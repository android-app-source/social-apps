.class public final enum Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2412483
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    .line 2412484
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->$VALUES:[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    .line 2412485
    new-instance v0, LX/Gzu;

    invoke-direct {v0}, LX/Gzu;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2412486
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;
    .locals 1

    .prologue
    .line 2412482
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;
    .locals 1

    .prologue
    .line 2412481
    sget-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->$VALUES:[Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2412478
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2412479
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2412480
    return-void
.end method
