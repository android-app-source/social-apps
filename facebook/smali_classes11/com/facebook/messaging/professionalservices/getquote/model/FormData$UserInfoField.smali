.class public final Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2412557
    new-instance v0, LX/Gzv;

    invoke-direct {v0}, LX/Gzv;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2412558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412559
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    .line 2412560
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412561
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    .line 2412562
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;Z)V
    .locals 0

    .prologue
    .line 2412551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412552
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    .line 2412553
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412554
    iput-boolean p3, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    .line 2412555
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2412556
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2412538
    if-ne p0, p1, :cond_1

    .line 2412539
    :cond_0
    :goto_0
    return v0

    .line 2412540
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2412541
    :cond_3
    check-cast p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2412542
    iget-boolean v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 2412543
    :cond_4
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2412544
    goto :goto_0

    .line 2412545
    :cond_6
    iget-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2412546
    :cond_7
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2412532
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2412533
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 2412534
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 2412535
    return v0

    :cond_1
    move v0, v1

    .line 2412536
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2412537
    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2412547
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2412548
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2412549
    iget-boolean v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2412550
    return-void
.end method
