.class public final Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2412487
    new-instance v0, LX/Gzt;

    invoke-direct {v0}, LX/Gzt;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2412488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412489
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    .line 2412490
    const-class v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    .line 2412491
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;)V
    .locals 0

    .prologue
    .line 2412492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412493
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    .line 2412494
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    .line 2412495
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2412496
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2412497
    if-ne p0, p1, :cond_1

    .line 2412498
    :cond_0
    :goto_0
    return v0

    .line 2412499
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2412500
    :cond_3
    check-cast p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2412501
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2412502
    goto :goto_0

    .line 2412503
    :cond_5
    iget-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2412504
    :cond_6
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2412505
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2412506
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 2412507
    return v0

    :cond_1
    move v0, v1

    .line 2412508
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2412509
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2412510
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2412511
    return-void
.end method
