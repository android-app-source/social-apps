.class public Lcom/facebook/messaging/professionalservices/getquote/model/FormData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2412563
    new-instance v0, LX/Gzq;

    invoke-direct {v0}, LX/Gzq;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2412621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412622
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    .line 2412623
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    .line 2412624
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    .line 2412625
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    .line 2412626
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    .line 2412627
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2412628
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2412629
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2412612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412613
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    .line 2412614
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    .line 2412615
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    .line 2412616
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    .line 2412617
    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    .line 2412618
    iput-object p4, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    .line 2412619
    iput-object p5, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    .line 2412620
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2412611
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2412596
    if-ne p0, p1, :cond_1

    .line 2412597
    :cond_0
    :goto_0
    return v0

    .line 2412598
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2412599
    :cond_3
    check-cast p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412600
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2412601
    goto :goto_0

    .line 2412602
    :cond_5
    iget-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2412603
    :cond_6
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2412604
    :cond_9
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2412605
    goto :goto_0

    .line 2412606
    :cond_b
    iget-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2412607
    :cond_c
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2412608
    goto :goto_0

    .line 2412609
    :cond_e
    iget-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    if-nez v2, :cond_d

    .line 2412610
    :cond_f
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    if-eqz v2, :cond_10

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_10
    iget-object v2, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()Lcom/facebook/messaging/professionalservices/getquote/model/FormData;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2412580
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2412581
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2412582
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2412583
    new-instance v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2412584
    iget-object v4, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2412585
    iget-object v6, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    move-object v0, v6

    .line 2412586
    invoke-direct {v3, v4, v0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;-><init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v5, v1

    .line 2412587
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2412588
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2412589
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2412590
    new-instance v2, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2412591
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2412592
    iget-object v6, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    move-object v6, v6

    .line 2412593
    iget-boolean v7, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    move v0, v7

    .line 2412594
    invoke-direct {v2, v3, v6, v0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;-><init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;Z)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v4, v1

    .line 2412595
    :cond_3
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2412570
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2412571
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 2412572
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 2412573
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 2412574
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 2412575
    return v0

    :cond_1
    move v0, v1

    .line 2412576
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2412577
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2412578
    goto :goto_2

    :cond_4
    move v0, v1

    .line 2412579
    goto :goto_3
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2412564
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2412565
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2412566
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2412567
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2412568
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2412569
    return-void
.end method
