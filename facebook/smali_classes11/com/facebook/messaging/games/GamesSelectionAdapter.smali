.class public Lcom/facebook/messaging/games/GamesSelectionAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/FET;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameApplicationFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/FEX;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2216333
    const-class v0, Lcom/facebook/messaging/games/GamesSelectionAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/games/GamesSelectionAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216334
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2216335
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2216336
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030784

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2216337
    new-instance v1, LX/FET;

    invoke-direct {v1, p0, v0}, LX/FET;-><init>(Lcom/facebook/messaging/games/GamesSelectionAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2216338
    check-cast p1, LX/FET;

    .line 2216339
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionAdapter;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2216340
    :goto_0
    return-void

    .line 2216341
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameApplicationFragmentModel;

    const/4 p0, 0x1

    .line 2216342
    iget-object v1, p1, LX/FET;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameApplicationFragmentModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216343
    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameApplicationFragmentModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2216344
    iget-object v1, p1, LX/FET;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2216345
    :goto_1
    new-instance v1, LX/FES;

    invoke-direct {v1, p1, v0, p2}, LX/FES;-><init>(LX/FET;Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameApplicationFragmentModel;I)V

    .line 2216346
    iget-object v2, p1, LX/FET;->m:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216347
    iget-object v2, p1, LX/FET;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216348
    goto :goto_0

    .line 2216349
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameApplicationFragmentModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v3, p1, LX/FET;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/messaging/games/GamesSelectionAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2216350
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionAdapter;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2216351
    const/4 v0, 0x0

    .line 2216352
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
