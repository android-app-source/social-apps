.class public Lcom/facebook/messaging/games/GamesSelectionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;
.implements LX/0f2;


# instance fields
.field public p:LX/67X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/6LN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/8Bs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/6LO;
    .annotation runtime Lcom/facebook/messaging/games/ForGamesBannerNotification;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/FEb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/8Br;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2216320
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/view/MenuItem;)V
    .locals 3

    .prologue
    .line 2216313
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    .line 2216314
    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setMaxWidth(I)V

    .line 2216315
    const v1, 0x7f083254

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/games/GamesSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 2216316
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d140c

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/games/GamesSelectionFragment;

    .line 2216317
    new-instance v2, LX/FER;

    invoke-direct {v2, p0, v1}, LX/FER;-><init>(Lcom/facebook/messaging/games/GamesSelectionActivity;Lcom/facebook/messaging/games/GamesSelectionFragment;)V

    .line 2216318
    iput-object v2, v0, Landroid/support/v7/widget/SearchView;->mOnQueryChangeListener:LX/3xK;

    .line 2216319
    return-void
.end method

.method private static a(Lcom/facebook/messaging/games/GamesSelectionActivity;LX/67X;LX/FEb;LX/6LN;LX/8Bs;LX/6LO;)V
    .locals 0

    .prologue
    .line 2216312
    iput-object p1, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->p:LX/67X;

    iput-object p2, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->t:LX/FEb;

    iput-object p3, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->q:LX/6LN;

    iput-object p4, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->r:LX/8Bs;

    iput-object p5, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->s:LX/6LO;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/games/GamesSelectionActivity;

    invoke-static {v5}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v1

    check-cast v1, LX/67X;

    invoke-static {v5}, LX/FEb;->b(LX/0QB;)LX/FEb;

    move-result-object v2

    check-cast v2, LX/FEb;

    invoke-static {v5}, LX/6LN;->b(LX/0QB;)LX/6LN;

    move-result-object v3

    check-cast v3, LX/6LN;

    const-class v4, LX/8Bs;

    invoke-interface {v5, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/8Bs;

    invoke-static {v5}, LX/FEU;->a(LX/0QB;)LX/FEU;

    move-result-object v5

    check-cast v5, LX/6LO;

    invoke-static/range {v0 .. v5}, Lcom/facebook/messaging/games/GamesSelectionActivity;->a(Lcom/facebook/messaging/games/GamesSelectionActivity;LX/67X;LX/FEb;LX/6LN;LX/8Bs;LX/6LO;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2216311
    const-string v0, "instant_game_list"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216306
    invoke-static {p0, p0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2216307
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->r:LX/8Bs;

    sget-object v1, LX/8Bx;->GAMES_SELECTION_ACTIVITY:LX/8Bx;

    invoke-virtual {v0, v1}, LX/8Bs;->a(LX/8Bx;)LX/8Br;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->u:LX/8Br;

    .line 2216308
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->q:LX/6LN;

    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->u:LX/8Br;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->s:LX/6LO;

    invoke-virtual {v0, v1, v2}, LX/6LN;->a(Ljava/util/Set;LX/6LO;)V

    .line 2216309
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->p:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2216310
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 2216295
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2216296
    instance-of v0, p1, Lcom/facebook/messaging/games/GamesSelectionFragment;

    if-eqz v0, :cond_0

    .line 2216297
    check-cast p1, Lcom/facebook/messaging/games/GamesSelectionFragment;

    invoke-virtual {p0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "thread_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2216298
    iput-object v0, p1, Lcom/facebook/messaging/games/GamesSelectionFragment;->k:Ljava/lang/String;

    .line 2216299
    move-object v1, p1

    .line 2216300
    invoke-virtual {p0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "entry_point"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FEh;

    .line 2216301
    iput-object v0, v1, Lcom/facebook/messaging/games/GamesSelectionFragment;->l:LX/FEh;

    .line 2216302
    move-object v0, v1

    .line 2216303
    new-instance v1, LX/FEQ;

    invoke-direct {v1, p0}, LX/FEQ;-><init>(Lcom/facebook/messaging/games/GamesSelectionActivity;)V

    .line 2216304
    iput-object v1, v0, Lcom/facebook/messaging/games/GamesSelectionFragment;->m:LX/FEP;

    .line 2216305
    :cond_0
    return-void
.end method

.method public final b()LX/3u1;
    .locals 1

    .prologue
    .line 2216294
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->p:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216287
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2216288
    const v0, 0x7f030782

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->setContentView(I)V

    .line 2216289
    invoke-virtual {p0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->b()LX/3u1;

    move-result-object v0

    const v1, 0x7f083241

    invoke-virtual {v0, v1}, LX/3u1;->b(I)V

    .line 2216290
    const v0, 0x7f0d0807

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2216291
    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->q:LX/6LN;

    .line 2216292
    iput-object v0, v1, LX/6LN;->h:Landroid/view/ViewGroup;

    .line 2216293
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    .line 2216279
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->t:LX/FEb;

    invoke-virtual {v0}, LX/FEb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->t:LX/FEb;

    .line 2216280
    iget-object v1, v0, LX/FEb;->b:LX/0Uh;

    const/16 v2, 0x158

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2216281
    if-eqz v0, :cond_0

    .line 2216282
    invoke-virtual {p0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110034

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2216283
    const v0, 0x7f0d323a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2216284
    invoke-direct {p0, v0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->a(Landroid/view/MenuItem;)V

    .line 2216285
    const/4 v0, 0x1

    .line 2216286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2216275
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2216276
    invoke-virtual {p0}, Lcom/facebook/messaging/games/GamesSelectionActivity;->finish()V

    .line 2216277
    const/4 v0, 0x1

    .line 2216278
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x5a556156

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216272
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2216273
    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->q:LX/6LN;

    invoke-virtual {v1}, LX/6LN;->b()V

    .line 2216274
    const/16 v1, 0x23

    const v2, 0x7bf55d3e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0xc8bd65a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216269
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2216270
    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionActivity;->q:LX/6LN;

    invoke-virtual {v1}, LX/6LN;->a()V

    .line 2216271
    const/16 v1, 0x23

    const v2, -0x4953267c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
