.class public Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;
.super Lcom/facebook/messaging/xma/ui/XMALinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:LX/FLv;

.field private final d:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2216777
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2216778
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216775
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2216776
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216767
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2216768
    const v0, 0x7f03095a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2216769
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->setOrientation(I)V

    .line 2216770
    new-instance v0, LX/FLv;

    invoke-direct {v0, p0}, LX/FLv;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->c:LX/FLv;

    .line 2216771
    const v0, 0x7f0d1805

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2216772
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->c:LX/FLv;

    iget-object v0, v0, LX/FLv;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2216773
    const v0, 0x7f0d1806

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->d:LX/4ob;

    .line 2216774
    return-void
.end method

.method private setCoverPhoto(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 2216759
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2216760
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2216761
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->c:LX/FLv;

    iget-object v0, v0, LX/FLv;->b:Lcom/facebook/messaging/xma/ui/XMALinearLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2216762
    const/4 v1, 0x3

    const v2, 0x7f0d1805

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2216763
    iget-object v1, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->c:LX/FLv;

    iget-object v1, v1, LX/FLv;->b:Lcom/facebook/messaging/xma/ui/XMALinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2216764
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2216765
    :goto_0
    return-void

    .line 2216766
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setProfilePicture(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2216755
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2216756
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->c:LX/FLv;

    iget-object v0, v0, LX/FLv;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2216757
    :goto_0
    return-void

    .line 2216758
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->c:LX/FLv;

    iget-object v0, v0, LX/FLv;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupMessageClickListener(Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V
    .locals 1

    .prologue
    .line 2216746
    new-instance v0, LX/FEx;

    invoke-direct {v0, p0, p1}, LX/FEx;-><init>(Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216747
    return-void
.end method


# virtual methods
.method public final a(LX/6ll;)V
    .locals 1
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216752
    invoke-super {p0, p1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a(LX/6ll;)V

    .line 2216753
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->setXMACallback(LX/6ll;)V

    .line 2216754
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2216748
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2216749
    iget-object v0, p0, Lcom/facebook/messaging/games/attachments/gameshare/GameShareView;->c:LX/FLv;

    iget-object v0, v0, LX/FLv;->b:Lcom/facebook/messaging/xma/ui/XMALinearLayout;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->setPadding(IIII)V

    .line 2216750
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->onMeasure(II)V

    .line 2216751
    return-void
.end method
