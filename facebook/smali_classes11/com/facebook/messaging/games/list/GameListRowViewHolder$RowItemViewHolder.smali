.class public final Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;
.super LX/FF7;
.source ""


# static fields
.field private static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final m:LX/FOK;

.field private final n:Landroid/view/View;

.field private final o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final p:Lcom/facebook/widget/text/BetterTextView;

.field private final q:Lcom/facebook/widget/text/BetterTextView;

.field private final r:Lcom/facebook/widget/text/BetterTextView;

.field private final s:Lcom/facebook/widget/text/BetterTextView;

.field private final t:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2216881
    const-class v0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/FOK;)V
    .locals 1

    .prologue
    .line 2216882
    invoke-direct {p0, p1}, LX/FF7;-><init>(Landroid/view/View;)V

    .line 2216883
    iput-object p1, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->n:Landroid/view/View;

    .line 2216884
    const v0, 0x7f0d13bf

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2216885
    const v0, 0x7f0d13c1

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 2216886
    const v0, 0x7f0d13c3

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 2216887
    const v0, 0x7f0d13c4

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->r:Lcom/facebook/widget/text/BetterTextView;

    .line 2216888
    const v0, 0x7f0d13c2

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->s:Lcom/facebook/widget/text/BetterTextView;

    .line 2216889
    const v0, 0x7f0d13c5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 2216890
    iput-object p2, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->m:LX/FOK;

    .line 2216891
    return-void
.end method

.method private a(Lcom/facebook/widget/text/BetterTextView;LX/FFH;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2216892
    if-eqz p2, :cond_0

    iget-object v0, p2, LX/FFH;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2216893
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2216894
    invoke-virtual {p1, v3, v3, v3, v3}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2216895
    :goto_0
    return-void

    .line 2216896
    :cond_1
    iget-object v0, p2, LX/FFH;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216897
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->m:LX/FOK;

    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p2, LX/FFH;->b:LX/0Px;

    invoke-virtual {v0, v1, p1, v2}, LX/FOK;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable$Callback;LX/0Px;)LX/FOI;

    move-result-object v0

    .line 2216898
    invoke-virtual {p1, v0, v3, v3, v3}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2216899
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/FFD;LX/FF9;)V
    .locals 3

    .prologue
    .line 2216900
    instance-of v0, p2, LX/FFE;

    if-nez v0, :cond_0

    .line 2216901
    :goto_0
    return-void

    .line 2216902
    :cond_0
    check-cast p2, LX/FFE;

    .line 2216903
    iget-object v0, p2, LX/FFE;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2216904
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2216905
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p2, LX/FFE;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216906
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->q:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p2, LX/FFE;->d:LX/FFH;

    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->a(Lcom/facebook/widget/text/BetterTextView;LX/FFH;)V

    .line 2216907
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->r:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p2, LX/FFE;->e:LX/FFH;

    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->a(Lcom/facebook/widget/text/BetterTextView;LX/FFH;)V

    .line 2216908
    iget-object v1, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->s:Lcom/facebook/widget/text/BetterTextView;

    iget-boolean v0, p2, LX/FFE;->f:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2216909
    iget-object v0, p2, LX/FFE;->a:Ljava/lang/String;

    .line 2216910
    new-instance v1, LX/FF6;

    invoke-direct {v1, p0, p3, v0, p1}, LX/FF6;-><init>(Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;LX/FF9;Ljava/lang/String;I)V

    .line 2216911
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216912
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->t:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2216913
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p2, LX/FFE;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    .line 2216914
    :cond_2
    const/16 v0, 0x8

    goto :goto_2
.end method
