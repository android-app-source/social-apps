.class public Lcom/facebook/messaging/games/GamesSelectionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:LX/FEZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/FEi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/FEb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/FEf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/FFC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/FEd;

.field private g:Landroid/os/Handler;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Landroid/support/v7/widget/RecyclerView;

.field public j:Landroid/widget/ProgressBar;

.field public k:Ljava/lang/String;

.field public l:LX/FEh;

.field public m:LX/FEP;

.field private n:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2216419
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2216420
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->o:Z

    .line 2216421
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->p:Z

    return-void
.end method

.method private static a(Lcom/facebook/messaging/games/GamesSelectionFragment;LX/FEZ;LX/FEi;LX/FEb;LX/FEf;LX/FFC;)V
    .locals 0

    .prologue
    .line 2216517
    iput-object p1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->a:LX/FEZ;

    iput-object p2, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->b:LX/FEi;

    iput-object p3, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->c:LX/FEb;

    iput-object p4, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->d:LX/FEf;

    iput-object p5, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->e:LX/FFC;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/games/GamesSelectionFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/games/GamesSelectionFragment;

    invoke-static {v5}, LX/FEZ;->b(LX/0QB;)LX/FEZ;

    move-result-object v1

    check-cast v1, LX/FEZ;

    invoke-static {v5}, LX/FEi;->a(LX/0QB;)LX/FEi;

    move-result-object v2

    check-cast v2, LX/FEi;

    invoke-static {v5}, LX/FEb;->b(LX/0QB;)LX/FEb;

    move-result-object v3

    check-cast v3, LX/FEb;

    const-class v4, LX/FEf;

    invoke-interface {v5, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/FEf;

    const-class v6, LX/FFC;

    invoke-interface {v5, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/FFC;

    invoke-static/range {v0 .. v5}, Lcom/facebook/messaging/games/GamesSelectionFragment;->a(Lcom/facebook/messaging/games/GamesSelectionFragment;LX/FEZ;LX/FEi;LX/FEb;LX/FEf;LX/FFC;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/games/GamesSelectionFragment;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2216491
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2216492
    sget-object v0, LX/FEm;->GAME_LIST_INDEX:LX/FEm;

    iget-object v0, v0, LX/FEm;->value:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2216493
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2216494
    :goto_0
    sget-object v3, LX/FEm;->GAME_LIST_IS_SEARCH_RESULT:LX/FEm;

    iget-object v3, v3, LX/FEm;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2216495
    sget-object v0, LX/FEm;->GAME_LIST_SEARCH_TERM:LX/FEm;

    iget-object v0, v0, LX/FEm;->value:Ljava/lang/String;

    invoke-interface {v2, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2216496
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->b:LX/FEi;

    .line 2216497
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    .line 2216498
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2216499
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2216500
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v4, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    goto :goto_1

    .line 2216501
    :cond_0
    iget-object v3, v0, LX/FEi;->b:LX/0if;

    sget-object v4, LX/FEi;->a:LX/0ih;

    sget-object p2, LX/FEj;->GAME_SELECT:LX/FEj;

    iget-object p2, p2, LX/FEj;->value:Ljava/lang/String;

    sget-object p3, LX/FEm;->GAME_ID:LX/FEm;

    iget-object p3, p3, LX/FEm;->value:Ljava/lang/String;

    .line 2216502
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object p3, v0

    .line 2216503
    invoke-virtual {v3, v4, p2, p3, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2216504
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->a:LX/FEZ;

    iget-object v2, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->l:LX/FEh;

    .line 2216505
    new-instance v4, Landroid/content/Intent;

    iget-object v5, v0, LX/FEZ;->b:Landroid/content/Context;

    const-class p2, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-direct {v4, v5, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2216506
    const-string v5, "app_id"

    invoke-virtual {v4, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2216507
    const-string v5, "source"

    invoke-virtual {v3}, LX/FEh;->toQuicksilverSource()LX/8TZ;

    move-result-object p2

    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2216508
    const-string v5, "source_context"

    sget-object p2, LX/8Ta;->Thread:LX/8Ta;

    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2216509
    const-string v5, "source_id"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2216510
    const-string v5, "funnel_definition"

    sget-object p2, LX/FEi;->a:LX/0ih;

    .line 2216511
    iget-object p1, p2, LX/0ih;->a:Ljava/lang/String;

    move-object p2, p1

    .line 2216512
    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2216513
    iget-object v5, v0, LX/FEZ;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object p2, v0, LX/FEZ;->b:Landroid/content/Context;

    invoke-interface {v5, v4, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2216514
    invoke-static {p0, v1}, Lcom/facebook/messaging/games/GamesSelectionFragment;->a$redex0(Lcom/facebook/messaging/games/GamesSelectionFragment;Z)V

    .line 2216515
    return-void

    .line 2216516
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/games/GamesSelectionFragment;Z)V
    .locals 1

    .prologue
    .line 2216485
    if-nez p1, :cond_0

    .line 2216486
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->b:LX/FEi;

    invoke-virtual {v0}, LX/FEi;->a()V

    .line 2216487
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->o:Z

    .line 2216488
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->m:LX/FEP;

    if-eqz v0, :cond_1

    .line 2216489
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->m:LX/FEP;

    invoke-interface {v0}, LX/FEP;->a()V

    .line 2216490
    :cond_1
    return-void
.end method

.method public static b$redex0(Lcom/facebook/messaging/games/GamesSelectionFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2216479
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->j:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2216480
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2216481
    iget-boolean v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->p:Z

    if-eqz v0, :cond_0

    .line 2216482
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->b:LX/FEi;

    sget-object v1, LX/FEj;->GAME_LIST_SHOW:LX/FEj;

    invoke-virtual {v0, v1}, LX/FEi;->a(LX/FEj;)V

    .line 2216483
    iput-boolean v2, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->p:Z

    .line 2216484
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/messaging/games/GamesSelectionFragment;)V
    .locals 3

    .prologue
    .line 2216477
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f083241

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08324b

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/FEY;

    invoke-direct {v2, p0}, LX/FEY;-><init>(Lcom/facebook/messaging/games/GamesSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2216478
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216473
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2216474
    const-class v0, Lcom/facebook/messaging/games/GamesSelectionFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/games/GamesSelectionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2216475
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->g:Landroid/os/Handler;

    .line 2216476
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2216471
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->f:LX/FEd;

    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->k:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, LX/FEd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216472
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2216466
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->n:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2216467
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->n:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2216468
    :cond_0
    new-instance v0, Lcom/facebook/messaging/games/GamesSelectionFragment$2;

    invoke-direct {v0, p0, p1}, Lcom/facebook/messaging/games/GamesSelectionFragment$2;-><init>(Lcom/facebook/messaging/games/GamesSelectionFragment;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->n:Ljava/lang/Runnable;

    .line 2216469
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->n:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    const v4, -0x3faea85b

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2216470
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x66d93172

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216461
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2216462
    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->b:LX/FEi;

    sget-object v2, LX/FEj;->GAME_LIST_FETCH:LX/FEj;

    invoke-virtual {v1, v2}, LX/FEi;->a(LX/FEj;)V

    .line 2216463
    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->j:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2216464
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/games/GamesSelectionFragment;->a(Ljava/lang/String;)V

    .line 2216465
    const/16 v1, 0x2b

    const v2, -0x69eb22c8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x180a16d5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2216426
    const v0, 0x7f030783

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2216427
    const v0, 0x7f0d140f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->i:Landroid/support/v7/widget/RecyclerView;

    .line 2216428
    const v0, 0x7f0d140e

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->j:Landroid/widget/ProgressBar;

    .line 2216429
    const v0, 0x7f0d140d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2216430
    iget-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->i:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2216431
    new-instance v0, LX/FEX;

    invoke-direct {v0, p0}, LX/FEX;-><init>(Lcom/facebook/messaging/games/GamesSelectionFragment;)V

    .line 2216432
    iget-object v3, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->c:LX/FEb;

    invoke-virtual {v3}, LX/FEb;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2216433
    iget-object v3, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->e:LX/FFC;

    iget-object v4, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->i:Landroid/support/v7/widget/RecyclerView;

    .line 2216434
    new-instance v5, LX/FFB;

    .line 2216435
    new-instance v8, LX/FF2;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-direct {v8, v6, v7}, LX/FF2;-><init>(LX/0tX;LX/1Ck;)V

    .line 2216436
    move-object v6, v8

    .line 2216437
    check-cast v6, LX/FF2;

    .line 2216438
    new-instance v8, LX/FF4;

    invoke-direct {v8}, LX/FF4;-><init>()V

    .line 2216439
    invoke-static {v3}, LX/FOK;->b(LX/0QB;)LX/FOK;

    move-result-object v7

    check-cast v7, LX/FOK;

    .line 2216440
    iput-object v7, v8, LX/FF4;->a:LX/FOK;

    .line 2216441
    move-object v7, v8

    .line 2216442
    check-cast v7, LX/FF4;

    .line 2216443
    new-instance v8, LX/FF3;

    invoke-direct {v8}, LX/FF3;-><init>()V

    .line 2216444
    move-object v8, v8

    .line 2216445
    move-object v8, v8

    .line 2216446
    check-cast v8, LX/FF3;

    move-object v9, v4

    move-object v10, v0

    invoke-direct/range {v5 .. v10}, LX/FFB;-><init>(LX/FF2;LX/FF4;LX/FF3;Landroid/support/v7/widget/RecyclerView;LX/FEX;)V

    .line 2216447
    move-object v0, v5

    .line 2216448
    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->f:LX/FEd;

    .line 2216449
    :goto_0
    const v0, -0x4d751a44

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2216450
    :cond_0
    iget-object v3, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->d:LX/FEf;

    iget-object v4, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->i:Landroid/support/v7/widget/RecyclerView;

    .line 2216451
    new-instance v7, LX/FEe;

    .line 2216452
    new-instance v5, Lcom/facebook/messaging/games/GamesSelectionAdapter;

    invoke-direct {v5}, Lcom/facebook/messaging/games/GamesSelectionAdapter;-><init>()V

    .line 2216453
    move-object v5, v5

    .line 2216454
    move-object v5, v5

    .line 2216455
    check-cast v5, Lcom/facebook/messaging/games/GamesSelectionAdapter;

    .line 2216456
    new-instance v9, LX/FEW;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-direct {v9, v6, v8}, LX/FEW;-><init>(LX/0tX;LX/1Ck;)V

    .line 2216457
    move-object v6, v9

    .line 2216458
    check-cast v6, LX/FEW;

    invoke-direct {v7, v5, v6, v4, v0}, LX/FEe;-><init>(Lcom/facebook/messaging/games/GamesSelectionAdapter;LX/FEW;Landroid/support/v7/widget/RecyclerView;LX/FEX;)V

    .line 2216459
    move-object v0, v7

    .line 2216460
    iput-object v0, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->f:LX/FEd;

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3946e6a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216422
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2216423
    iget-boolean v1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->o:Z

    if-nez v1, :cond_0

    .line 2216424
    iget-object v1, p0, Lcom/facebook/messaging/games/GamesSelectionFragment;->b:LX/FEi;

    invoke-virtual {v1}, LX/FEi;->a()V

    .line 2216425
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x22b80d56

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
