.class public Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2223786
    new-instance v0, LX/FJl;

    invoke-direct {v0}, LX/FJl;-><init>()V

    sput-object v0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FJm;)V
    .locals 1

    .prologue
    .line 2223787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223788
    iget-boolean v0, p1, LX/FJm;->a:Z

    move v0, v0

    .line 2223789
    iput-boolean v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->a:Z

    .line 2223790
    iget v0, p1, LX/FJm;->b:I

    move v0, v0

    .line 2223791
    iput v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->b:I

    .line 2223792
    iget-object v0, p1, LX/FJm;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2223793
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->c:Ljava/lang/String;

    .line 2223794
    iget-object v0, p1, LX/FJm;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2223795
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->d:Ljava/lang/String;

    .line 2223796
    iget-object v0, p1, LX/FJm;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2223797
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->e:Ljava/lang/String;

    .line 2223798
    iget-object v0, p1, LX/FJm;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2223799
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->f:Ljava/lang/String;

    .line 2223800
    iget-object v0, p1, LX/FJm;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2223801
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->g:Ljava/lang/String;

    .line 2223802
    iget-object v0, p1, LX/FJm;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2223803
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->h:Ljava/lang/String;

    .line 2223804
    iget-object v0, p1, LX/FJm;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2223805
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->i:Ljava/lang/String;

    .line 2223806
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2223807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223808
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->a:Z

    .line 2223809
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->b:I

    .line 2223810
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->c:Ljava/lang/String;

    .line 2223811
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->d:Ljava/lang/String;

    .line 2223812
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->e:Ljava/lang/String;

    .line 2223813
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->f:Ljava/lang/String;

    .line 2223814
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->g:Ljava/lang/String;

    .line 2223815
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->h:Ljava/lang/String;

    .line 2223816
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->i:Ljava/lang/String;

    .line 2223817
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2223818
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2223819
    iget-boolean v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2223820
    iget v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2223821
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223822
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223823
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223824
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223825
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223826
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223827
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223828
    return-void
.end method
