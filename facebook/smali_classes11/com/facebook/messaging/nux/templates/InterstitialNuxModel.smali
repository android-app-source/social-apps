.class public Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2223897
    new-instance v0, LX/FJr;

    invoke-direct {v0}, LX/FJr;-><init>()V

    sput-object v0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2223898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223899
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->a:Ljava/lang/String;

    .line 2223900
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->b:Ljava/lang/String;

    .line 2223901
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->c:Ljava/lang/String;

    .line 2223902
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->d:I

    .line 2223903
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->e:I

    .line 2223904
    return-void
.end method

.method public static newBuilder()LX/FJs;
    .locals 2

    .prologue
    .line 2223905
    new-instance v0, LX/FJs;

    invoke-direct {v0}, LX/FJs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2223906
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2223907
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223908
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223909
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2223910
    iget v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2223911
    iget v0, p0, Lcom/facebook/messaging/nux/templates/InterstitialNuxModel;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2223912
    return-void
.end method
