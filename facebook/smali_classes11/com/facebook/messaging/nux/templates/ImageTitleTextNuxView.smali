.class public Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/TextView;

.field public j:LX/FJq;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2223878
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2223879
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223880
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a:LX/0Ot;

    .line 2223881
    invoke-direct {p0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a()V

    .line 2223882
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223883
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2223884
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223885
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a:LX/0Ot;

    .line 2223886
    invoke-direct {p0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a()V

    .line 2223887
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223873
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2223874
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223875
    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a:LX/0Ot;

    .line 2223876
    invoke-direct {p0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a()V

    .line 2223877
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2223858
    const-class v0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2223859
    const v0, 0x7f0308d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2223860
    const v0, 0x106000b

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->setBackgroundResource(I)V

    .line 2223861
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->setOrientation(I)V

    .line 2223862
    const v0, 0x7f0d16ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->b:Landroid/view/View;

    .line 2223863
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->b:Landroid/view/View;

    new-instance v1, LX/FJn;

    invoke-direct {v1, p0}, LX/FJn;-><init>(Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2223864
    const v0, 0x7f0d16ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->c:Landroid/widget/ImageView;

    .line 2223865
    const v0, 0x7f0d16ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->d:Landroid/widget/TextView;

    .line 2223866
    const v0, 0x7f0d16f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->e:Landroid/widget/TextView;

    .line 2223867
    const v0, 0x7f0d16f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->f:Landroid/widget/TextView;

    .line 2223868
    const v0, 0x7f0d16f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->g:Landroid/widget/TextView;

    .line 2223869
    const v0, 0x7f0d16f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->h:Landroid/widget/Button;

    .line 2223870
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->h:Landroid/widget/Button;

    new-instance v1, LX/FJo;

    invoke-direct {v1, p0}, LX/FJo;-><init>(Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2223871
    const v0, 0x7f0d16f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->i:Landroid/widget/TextView;

    .line 2223872
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2223888
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2223889
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2223890
    return-void

    .line 2223891
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;

    const/16 v1, 0x27c5

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a:LX/0Ot;

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2223852
    iget-object v2, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2223853
    invoke-virtual {p0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b212a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2223854
    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v1, v0, v1, v2}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->setPadding(IIII)V

    .line 2223855
    return-void

    .line 2223856
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2223857
    goto :goto_1
.end method


# virtual methods
.method public setListener(LX/FJq;)V
    .locals 0
    .param p1    # LX/FJq;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223840
    iput-object p1, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->j:LX/FJq;

    .line 2223841
    return-void
.end method

.method public setModel(Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;)V
    .locals 2

    .prologue
    .line 2223842
    iget-boolean v0, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->a:Z

    invoke-direct {p0, v0}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Z)V

    .line 2223843
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->c:Landroid/widget/ImageView;

    iget v1, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2223844
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2223845
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2223846
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->f:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2223847
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->g:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2223848
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->h:Landroid/widget/Button;

    iget-object v1, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2223849
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->i:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2223850
    iget-object v0, p0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->i:Landroid/widget/TextView;

    new-instance v1, LX/FJp;

    invoke-direct {v1, p0, p1}, LX/FJp;-><init>(Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2223851
    return-void
.end method
