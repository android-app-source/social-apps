.class public Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;
.super LX/0te;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/os/Looper;

.field public c:LX/FK2;

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zr;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FJz;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FK7;",
            ">;"
        }
    .end annotation
.end field

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2224417
    const-class v0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;

    sput-object v0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2224409
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2224410
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2224411
    iput-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->d:LX/0Ot;

    .line 2224412
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2224413
    iput-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->e:LX/0Ot;

    .line 2224414
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2224415
    iput-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->f:LX/0Ot;

    .line 2224416
    return-void
.end method

.method public static synthetic a(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;Landroid/content/Intent;)LX/1mW;
    .locals 1

    .prologue
    .line 2224365
    invoke-virtual {p0, p1}, LX/0te;->pushViewerContext(Landroid/content/Intent;)LX/1mW;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;",
            "LX/0Ot",
            "<",
            "LX/0Zr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FJz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FK7;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2224408
    iput-object p1, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->d:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->e:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->f:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;

    const/16 v1, 0x274

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x28e8

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x28eb

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->a(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;)V
    .locals 1

    .prologue
    .line 2224405
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK7;

    invoke-virtual {v0}, LX/FK7;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJz;

    invoke-virtual {v0}, LX/FJz;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2224406
    iget v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->g:I

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->stopSelf(I)V

    .line 2224407
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 2224388
    iput p2, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->g:I

    .line 2224389
    if-nez p1, :cond_0

    .line 2224390
    sget-object v0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->a:Ljava/lang/Class;

    const-string v1, "Received a null intent"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2224391
    invoke-static {p0}, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->a$redex0(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;)V

    .line 2224392
    :goto_0
    return-void

    .line 2224393
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2224394
    const-string v1, "NotificationsPrefsService.SYNC_THREAD_FROM_CLIENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2224395
    const-string v0, "THREAD_KEY_STRING"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2224396
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK7;

    invoke-virtual {v0, v1}, LX/FK7;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2224397
    :cond_1
    :goto_1
    invoke-static {p0}, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->a$redex0(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;)V

    goto :goto_0

    .line 2224398
    :cond_2
    const-string v1, "NotificationsPrefsService.SYNC_THREAD_FROM_SERVER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2224399
    const-string v0, "THREAD_KEY_STRING"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2224400
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK7;

    invoke-virtual {v0, v1}, LX/FK7;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_1

    .line 2224401
    :cond_3
    const-string v1, "NotificationsPrefsService.SYNC_GLOBAL_FROM_CLIENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2224402
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJz;

    invoke-virtual {v0}, LX/FJz;->a()V

    goto :goto_1

    .line 2224403
    :cond_4
    const-string v1, "NotificationsPrefsService.SYNC_GLOBAL_FROM_SERVER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2224404
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJz;

    invoke-virtual {v0}, LX/FJz;->b()V

    goto :goto_1
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2224387
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0xe363737

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2224376
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2224377
    invoke-static {p0, p0}, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2224378
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zr;

    const-string v2, "NotificationPrefsService"

    invoke-virtual {v0, v2}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 2224379
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 2224380
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->b:Landroid/os/Looper;

    .line 2224381
    new-instance v0, LX/FK2;

    iget-object v2, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->b:Landroid/os/Looper;

    invoke-direct {v0, p0, v2}, LX/FK2;-><init>(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->c:LX/FK2;

    .line 2224382
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK7;

    new-instance v2, LX/FK0;

    invoke-direct {v2, p0}, LX/FK0;-><init>(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;)V

    .line 2224383
    iput-object v2, v0, LX/FK7;->l:LX/FK0;

    .line 2224384
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJz;

    new-instance v2, LX/FK1;

    invoke-direct {v2, p0}, LX/FK1;-><init>(Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;)V

    .line 2224385
    iput-object v2, v0, LX/FJz;->l:LX/FK1;

    .line 2224386
    const/16 v0, 0x25

    const v2, 0x113bf0b

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x7fa78b1b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2224371
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2224372
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 2224373
    iget-object v0, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK7;

    const/4 v2, 0x0

    .line 2224374
    iput-object v2, v0, LX/FK7;->l:LX/FK0;

    .line 2224375
    const/16 v0, 0x25

    const v2, -0x34b4706d    # -1.3340563E7f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x642929f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2224366
    iget-object v1, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->c:LX/FK2;

    invoke-virtual {v1}, LX/FK2;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 2224367
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 2224368
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2224369
    iget-object v2, p0, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;->c:LX/FK2;

    invoke-virtual {v2, v1}, LX/FK2;->sendMessage(Landroid/os/Message;)Z

    .line 2224370
    const/16 v1, 0x25

    const v2, -0x57f6ed07

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method
