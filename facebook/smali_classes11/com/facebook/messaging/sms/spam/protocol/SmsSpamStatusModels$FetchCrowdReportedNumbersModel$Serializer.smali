.class public final Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2232831
    const-class v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;

    new-instance v1, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2232832
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2232833
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2232834
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2232835
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2232836
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2232837
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2232838
    if-eqz v2, :cond_1

    .line 2232839
    const-string p0, "top_blocked_numbers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2232840
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2232841
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2232842
    if-eqz p0, :cond_0

    .line 2232843
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2232844
    invoke-static {v1, p0, p1, p2}, LX/FNd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2232845
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2232846
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2232847
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2232848
    check-cast p1, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel$Serializer;->a(Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;LX/0nX;LX/0my;)V

    return-void
.end method
