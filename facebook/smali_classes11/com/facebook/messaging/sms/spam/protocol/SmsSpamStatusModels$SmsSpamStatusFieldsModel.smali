.class public final Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xe7c2b8d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2233028
    const-class v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2233031
    const-class v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2233029
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2233030
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLBlockStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2233023
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    iput-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 2233024
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2233015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2233016
    invoke-direct {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2233017
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2233018
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2233019
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2233020
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2233021
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2233022
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2233025
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2233026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2233027
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2233013
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->f:Ljava/lang/String;

    .line 2233014
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2233010
    new-instance v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;-><init>()V

    .line 2233011
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2233012
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2233009
    const v0, 0x5553795c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2233008
    const v0, 0x1c4e6237

    return v0
.end method
