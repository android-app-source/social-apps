.class public final Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4494875f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2233071
    const-class v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2233095
    const-class v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2233093
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2233094
    return-void
.end method

.method private a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2233091
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    iput-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    .line 2233092
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2233085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2233086
    invoke-direct {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2233087
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2233088
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2233089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2233090
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2233077
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2233078
    invoke-direct {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2233079
    invoke-direct {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    .line 2233080
    invoke-direct {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2233081
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;

    .line 2233082
    iput-object v0, v1, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    .line 2233083
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2233084
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2233074
    new-instance v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;

    invoke-direct {v0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusMutationModel;-><init>()V

    .line 2233075
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2233076
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2233073
    const v0, 0x29d5ea0e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2233072
    const v0, 0xfde186e    # 2.19003E-29f

    return v0
.end method
