.class public final Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1d606a32
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2232963
    const-class v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2232964
    const-class v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2232965
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2232966
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2232967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2232968
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2232969
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2232970
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2232971
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2232972
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2232973
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2232974
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2232975
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    .line 2232976
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2232977
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;

    .line 2232978
    iput-object v0, v1, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    .line 2232979
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2232980
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2232981
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    iput-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    .line 2232982
    iget-object v0, p0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->e:Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2232983
    new-instance v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;

    invoke-direct {v0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;-><init>()V

    .line 2232984
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2232985
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2232986
    const v0, -0xfe8b1dd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2232987
    const v0, -0x6747e1ce

    return v0
.end method
