.class public Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/FMB;

.field private final c:LX/FMu;

.field private final d:LX/FMo;

.field private final e:LX/FMp;

.field private final f:LX/1rd;

.field private final g:LX/FN6;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FMB;LX/FMu;LX/FMo;LX/FMp;LX/1rd;LX/FN6;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/FMB;",
            "LX/FMu;",
            "LX/FMo;",
            "LX/FMp;",
            "LX/1rd;",
            "LX/FN6;",
            "LX/0Ot",
            "<",
            "LX/FNi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231026
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a:Landroid/content/Context;

    .line 2231027
    iput-object p2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->b:LX/FMB;

    .line 2231028
    iput-object p3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->c:LX/FMu;

    .line 2231029
    iput-object p4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->d:LX/FMo;

    .line 2231030
    iput-object p5, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->e:LX/FMp;

    .line 2231031
    iput-object p6, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->f:LX/1rd;

    .line 2231032
    iput-object p7, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->g:LX/FN6;

    .line 2231033
    iput-object p8, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->h:LX/0Ot;

    .line 2231034
    return-void
.end method

.method private a(LX/EdW;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2230967
    invoke-virtual {p1}, LX/EdW;->d()[B

    move-result-object v0

    .line 2230968
    if-eqz v0, :cond_1

    .line 2230969
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 2230970
    const-string v0, "ct_l"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2230971
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2230972
    sget-object v1, LX/2UG;->a:Landroid/net/Uri;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2230973
    if-eqz v0, :cond_1

    .line 2230974
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_0

    .line 2230975
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move v0, v6

    .line 2230976
    :goto_0
    return v0

    .line 2230977
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v7

    .line 2230978
    goto :goto_0

    .line 2230979
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2230980
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 2230981
    if-nez v0, :cond_0

    .line 2230982
    new-instance v0, LX/EdT;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No data in MMS: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2230983
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->f:LX/1rd;

    const-string v2, "subscription"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->g:LX/FN6;

    invoke-virtual {v3}, LX/FN6;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/1rd;->a(II)I

    move-result v3

    .line 2230984
    new-instance v1, LX/Edf;

    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->c:LX/FMu;

    invoke-virtual {v2, v3}, LX/FMu;->a(I)Z

    move-result v2

    invoke-direct {v1, v0, v2}, LX/Edf;-><init>([BZ)V

    .line 2230985
    invoke-virtual {v1}, LX/Edf;->a()LX/EdM;

    move-result-object v1

    .line 2230986
    if-nez v1, :cond_1

    .line 2230987
    new-instance v0, LX/EdT;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Recieved invalid message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2230988
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a:Landroid/content/Context;

    invoke-static {v0}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v0

    .line 2230989
    :try_start_0
    invoke-virtual {v1}, LX/EdM;->b()I

    move-result v2

    .line 2230990
    const/16 v4, 0x82

    if-ne v2, v4, :cond_8

    .line 2230991
    check-cast v1, LX/EdW;

    .line 2230992
    invoke-direct {p0, v1}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a(LX/EdW;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2230993
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->c:LX/FMu;

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2230994
    iget-object v10, v2, LX/FMu;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v11, LX/3Kr;->ac:LX/0Tn;

    iget-object v4, v2, LX/FMu;->d:LX/2Or;

    .line 2230995
    iget-object v12, v4, LX/2Or;->a:LX/0Uh;

    const/16 v13, 0x62c

    const/4 p1, 0x0

    invoke-virtual {v12, v13, p1}, LX/0Uh;->a(IZ)Z

    move-result v12

    move v4, v12

    .line 2230996
    if-nez v4, :cond_9

    move v4, v8

    :goto_0
    invoke-interface {v10, v11, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    .line 2230997
    if-eqz v4, :cond_a

    .line 2230998
    iget-object v4, v2, LX/FMu;->b:Landroid/content/Context;

    invoke-static {v4}, LX/FNi;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2230999
    iget-object v4, v2, LX/FMu;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v10, LX/3Kr;->ad:LX/0Tn;

    invoke-interface {v4, v10, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    .line 2231000
    iget-object v10, v2, LX/FMu;->e:Landroid/telephony/TelephonyManager;

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v10

    if-eqz v10, :cond_2

    if-eqz v4, :cond_a

    .line 2231001
    :cond_2
    :goto_1
    move v8, v8

    .line 2231002
    sget-object v2, LX/2UI;->a:Landroid/net/Uri;

    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->f:LX/1rd;

    invoke-virtual {v4, v3}, LX/1rd;->g(I)Ljava/lang/String;

    move-result-object v4

    if-nez v8, :cond_4

    :goto_2
    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/Edh;->a(LX/EdM;Landroid/net/Uri;ILjava/lang/String;ZLjava/util/Map;)Landroid/net/Uri;

    move-result-object v2

    .line 2231003
    if-nez v8, :cond_7

    .line 2231004
    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->b:LX/FMB;

    invoke-virtual {v4, v2}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v9

    .line 2231005
    if-eqz v9, :cond_6

    .line 2231006
    new-instance v2, LX/EdX;

    const/16 v4, 0x12

    invoke-virtual {v1}, LX/EdW;->g()[B

    move-result-object v5

    const/16 v6, 0x83

    invoke-direct {v2, v4, v5, v6}, LX/EdX;-><init>(I[BI)V

    .line 2231007
    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a:Landroid/content/Context;

    invoke-static {v4, v2}, LX/FMY;->a(Landroid/content/Context;LX/EdM;)Landroid/net/Uri;

    move-result-object v5

    .line 2231008
    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->c:LX/FMu;

    invoke-virtual {v2}, LX/FMu;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v1}, LX/EdW;->d()[B

    move-result-object v1

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>([B)V

    :goto_3
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a:Landroid/content/Context;

    invoke-static {v1, v5}, LX/FMj;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, LX/EdD;->a(ILandroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Z)V

    .line 2231009
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->d:LX/FMo;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2, v9, v3, v4}, LX/FMo;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/model/messages/Message;Landroid/net/Uri;Ljava/lang/Boolean;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2231010
    :cond_3
    :goto_4
    invoke-virtual {v0}, LX/Edh;->a()V

    .line 2231011
    return-void

    :cond_4
    move v5, v6

    .line 2231012
    goto :goto_2

    :cond_5
    move-object v6, v7

    .line 2231013
    goto :goto_3

    .line 2231014
    :cond_6
    :try_start_1
    const-string v1, "ProcessMmsReceivedAction"

    const-string v3, "Failed to load received message %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v1, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 2231015
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/Edh;->a()V

    throw v1

    .line 2231016
    :cond_7
    :try_start_2
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2231017
    const-string v5, "extra_uri"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2231018
    const-string v2, "extra_repersist_on_error"

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2231019
    const-string v2, "subscription"

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2231020
    const-string v2, "location_url"

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v1}, LX/EdW;->d()[B

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231021
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->e:LX/FMp;

    invoke-virtual {v1, v4}, LX/FMp;->a(Landroid/os/Bundle;)V

    goto :goto_4

    .line 2231022
    :cond_8
    const-string v1, "ProcessMmsReceivedAction"

    const-string v2, "Received unhandled PDU."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    :cond_9
    move v4, v9

    .line 2231023
    goto/16 :goto_0

    :cond_a
    move v8, v9

    .line 2231024
    goto/16 :goto_1
.end method
