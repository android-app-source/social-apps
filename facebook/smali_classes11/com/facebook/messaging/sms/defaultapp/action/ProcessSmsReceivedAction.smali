.class public Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0SG;

.field private final d:LX/FNf;

.field private final e:LX/FMB;

.field public final f:LX/FMo;

.field private final g:LX/FN4;

.field public final h:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2231346
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "address"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "protocol"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/FNf;LX/FMB;LX/FMo;LX/FN4;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231338
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b:Landroid/content/Context;

    .line 2231339
    iput-object p2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->c:LX/0SG;

    .line 2231340
    iput-object p3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->d:LX/FNf;

    .line 2231341
    iput-object p4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->e:LX/FMB;

    .line 2231342
    iput-object p5, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->f:LX/FMo;

    .line 2231343
    iput-object p6, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->g:LX/FN4;

    .line 2231344
    iput-object p7, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2231345
    return-void
.end method

.method public static a([Landroid/telephony/SmsMessage;)Landroid/content/ContentValues;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2231317
    aget-object v3, p0, v2

    .line 2231318
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2231319
    const-string v0, "address"

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231320
    const-string v0, "date_sent"

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2231321
    const-string v0, "protocol"

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getProtocolIdentifier()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231322
    const-string v0, "read"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231323
    const-string v0, "seen"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231324
    const-string v0, "subject"

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231325
    const-string v5, "reply_path_present"

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->isReplyPathPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231326
    const-string v0, "service_center"

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getServiceCenterAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231327
    array-length v0, p0

    if-ne v0, v1, :cond_1

    .line 2231328
    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v0

    .line 2231329
    :goto_1
    const-string v1, "body"

    const/16 v2, 0xc

    const/16 v3, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231330
    return-object v4

    :cond_0
    move v0, v2

    .line 2231331
    goto :goto_0

    .line 2231332
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2231333
    array-length v1, p0

    :goto_2
    if-ge v2, v1, :cond_2

    aget-object v3, p0, v2

    .line 2231334
    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2231335
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2231336
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/content/ContentValues;J)V
    .locals 4

    .prologue
    .line 2231314
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->d:LX/FNf;

    invoke-virtual {v0, p2, p3}, LX/FNf;->a(J)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 2231315
    const-string v2, "date"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2231316
    return-void
.end method

.method public static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/net/Uri;Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 2231309
    if-eqz p1, :cond_0

    .line 2231310
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->e:LX/FMB;

    invoke-virtual {v0, p1}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2231311
    if-eqz v0, :cond_0

    .line 2231312
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->f:LX/FMo;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3, p2}, LX/FMo;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/model/messages/Message;Landroid/net/Uri;Ljava/lang/Boolean;)V

    .line 2231313
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/content/ContentValues;I)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 2231286
    const-string v0, "address"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2231287
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2231288
    const-string v0, "Unknown"

    .line 2231289
    const-string v1, "address"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231290
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b:Landroid/content/Context;

    invoke-static {v1, v0}, LX/5e6;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2231291
    const-string v2, "thread_id"

    invoke-virtual {p1, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2231292
    if-ltz p2, :cond_1

    .line 2231293
    const-string v2, "sub_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231294
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p0, p1, v2, v3}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/content/ContentValues;J)V

    .line 2231295
    const-string v1, "reply_path_present"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2231296
    const-string v2, "service_center"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2231297
    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2231298
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->g:LX/FN4;

    .line 2231299
    iget-object v3, v1, LX/FN4;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v1, LX/FN4;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2231300
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2231301
    sget-object v1, LX/2UJ;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 2231302
    :cond_3
    iget-object v3, v1, LX/FN4;->a:LX/3Mo;

    .line 2231303
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2231304
    sget-object v5, LX/3MJ;->b:LX/0U1;

    .line 2231305
    iget-object p2, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p2

    .line 2231306
    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231307
    invoke-static {v3, v0, v4}, LX/3Mo;->a(LX/3Mo;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 2231308
    iget-object v3, v1, LX/FN4;->b:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private b([Landroid/telephony/SmsMessage;I)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 2231264
    invoke-static {p1}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a([Landroid/telephony/SmsMessage;)Landroid/content/ContentValues;

    move-result-object v7

    .line 2231265
    aget-object v0, p1, v4

    .line 2231266
    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v1

    .line 2231267
    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getProtocolIdentifier()I

    move-result v0

    .line 2231268
    const/4 v2, 0x2

    new-array v2, v2, [LX/0ux;

    const-string v3, "address"

    invoke-static {v3, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v2, v4

    const/4 v1, 0x1

    const-string v3, "protocol"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 2231269
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2231270
    :try_start_0
    sget-object v1, LX/2UJ;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2231271
    if-eqz v2, :cond_1

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2231272
    const-string v1, "_id"

    invoke-static {v2, v1}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2231273
    sget-object v1, LX/554;->a:Landroid/net/Uri;

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 2231274
    const-string v3, "thread_id"

    invoke-static {v2, v3}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2231275
    invoke-static {p0, v7, v4, v5}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/content/ContentValues;J)V

    .line 2231276
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v7, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2231277
    if-eqz v2, :cond_0

    .line 2231278
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v1

    .line 2231279
    :goto_0
    return-object v0

    .line 2231280
    :cond_1
    if-eqz v2, :cond_2

    .line 2231281
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2231282
    :cond_2
    invoke-static {p1}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a([Landroid/telephony/SmsMessage;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/content/ContentValues;I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2231283
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    .line 2231284
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2231285
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;
    .locals 8

    .prologue
    .line 2231262
    new-instance v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/FNf;->b(LX/0QB;)LX/FNf;

    move-result-object v3

    check-cast v3, LX/FNf;

    invoke-static {p0}, LX/FMB;->a(LX/0QB;)LX/FMB;

    move-result-object v4

    check-cast v4, LX/FMB;

    invoke-static {p0}, LX/FMo;->a(LX/0QB;)LX/FMo;

    move-result-object v5

    check-cast v5, LX/FMo;

    invoke-static {p0}, LX/FN4;->a(LX/0QB;)LX/FN4;

    move-result-object v6

    check-cast v6, LX/FN4;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;-><init>(Landroid/content/Context;LX/0SG;LX/FNf;LX/FMB;LX/FMo;LX/FN4;Lcom/facebook/content/SecureContextHelper;)V

    .line 2231263
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2231230
    const/4 v3, 0x0

    .line 2231231
    const-string v0, "pdus"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 2231232
    array-length v1, v0

    new-array v4, v1, [[B

    move v2, v3

    .line 2231233
    :goto_0
    array-length v1, v0

    if-ge v2, v1, :cond_0

    .line 2231234
    aget-object v1, v0, v2

    check-cast v1, [B

    check-cast v1, [B

    aput-object v1, v4, v2

    .line 2231235
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2231236
    :cond_0
    array-length v0, v4

    new-array v0, v0, [[B

    .line 2231237
    array-length v1, v0

    .line 2231238
    new-array v2, v1, [Landroid/telephony/SmsMessage;

    .line 2231239
    :goto_1
    if-ge v3, v1, :cond_1

    .line 2231240
    aget-object v6, v4, v3

    aput-object v6, v0, v3

    .line 2231241
    aget-object v6, v0, v3

    invoke-static {v6}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v6

    aput-object v6, v2, v3

    .line 2231242
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2231243
    :cond_1
    move-object v0, v2

    .line 2231244
    if-eqz v0, :cond_2

    array-length v1, v0

    if-nez v1, :cond_3

    .line 2231245
    :cond_2
    const-string v0, "ProcessSmsReceivedAction"

    const-string v1, "Received invalid message from intent: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2231246
    :goto_2
    return-void

    .line 2231247
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "subscription"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2231248
    aget-object v2, v0, v5

    .line 2231249
    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getMessageClass()Landroid/telephony/SmsMessage$MessageClass;

    move-result-object v3

    sget-object v4, Landroid/telephony/SmsMessage$MessageClass;->CLASS_0:Landroid/telephony/SmsMessage$MessageClass;

    if-ne v3, v4, :cond_4

    .line 2231250
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->f:LX/FMo;

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/FMo;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2231251
    :goto_3
    goto :goto_2

    .line 2231252
    :cond_4
    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->isReplace()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2231253
    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b([Landroid/telephony/SmsMessage;I)Landroid/net/Uri;

    move-result-object v0

    .line 2231254
    :goto_4
    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->isReplace()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2231255
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/net/Uri;Ljava/lang/Boolean;)V

    goto :goto_2

    .line 2231256
    :cond_5
    invoke-static {v0}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a([Landroid/telephony/SmsMessage;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/content/ContentValues;I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_4

    .line 2231257
    :cond_6
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b:Landroid/content/Context;

    const-class v4, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2231258
    const/high16 v3, 0x18000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2231259
    const-string v3, "sms_message"

    invoke-static {v0}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a([Landroid/telephony/SmsMessage;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2231260
    const-string v3, "subscription"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2231261
    iget-object v3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b:Landroid/content/Context;

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_3
.end method
