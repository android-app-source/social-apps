.class public Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0SG;

.field public final d:LX/FMX;

.field public final e:LX/FMu;

.field private final f:LX/FMB;

.field private final g:LX/FMo;

.field public final h:LX/1rd;

.field public final i:LX/FN6;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G5o;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2230966
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "resp_st"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "exp"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/FMX;LX/FMu;LX/FMB;LX/FMo;LX/1rd;LX/FN6;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/FMX;",
            "LX/FMu;",
            "LX/FMB;",
            "LX/FMo;",
            "LX/1rd;",
            "LX/FN6;",
            "LX/0Ot",
            "<",
            "LX/G5o;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2230873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2230874
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    .line 2230875
    iput-object p2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->c:LX/0SG;

    .line 2230876
    iput-object p3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->d:LX/FMX;

    .line 2230877
    iput-object p4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->e:LX/FMu;

    .line 2230878
    iput-object p5, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->f:LX/FMB;

    .line 2230879
    iput-object p6, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->g:LX/FMo;

    .line 2230880
    iput-object p7, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->h:LX/1rd;

    .line 2230881
    iput-object p8, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->i:LX/FN6;

    .line 2230882
    iput-object p9, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->j:LX/0Ot;

    .line 2230883
    iput-object p10, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->k:LX/0Ot;

    .line 2230884
    iput-object p11, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->l:LX/0Ot;

    .line 2230885
    return-void
.end method

.method public static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/EdW;Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2230886
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v0}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v0

    .line 2230887
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p2, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2230888
    sget-object v2, LX/2UI;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FNi;

    iget-object v3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-virtual {v1, v3}, LX/FNi;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v1, p1

    move v3, p3

    invoke-virtual/range {v0 .. v6}, LX/Edh;->a(LX/EdM;Landroid/net/Uri;ILjava/lang/String;ZLjava/util/Map;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/Edl;I)Landroid/net/Uri;
    .locals 7

    .prologue
    .line 2230889
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v0}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v0

    .line 2230890
    sget-object v2, LX/2UI;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->h:LX/1rd;

    invoke-virtual {v1, p2}, LX/1rd;->g(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p1

    move v3, p2

    invoke-virtual/range {v0 .. v6}, LX/Edh;->a(LX/EdM;Landroid/net/Uri;ILjava/lang/String;ZLjava/util/Map;)Landroid/net/Uri;

    move-result-object v0

    .line 2230891
    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/Edg;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2230892
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {}, LX/FMY;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    .line 2230893
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {}, LX/FMY;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v2

    .line 2230894
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2230895
    :try_start_1
    iget-object v0, p1, LX/Edg;->f:[B

    move-object v0, v0

    .line 2230896
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2230897
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 2230898
    new-instance v0, LX/G5p;

    const/4 v3, -0x1

    const/4 v4, -0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, LX/G5p;-><init>(Ljava/io/File;Ljava/io/File;IILX/60y;Z)V

    .line 2230899
    iget-object v3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G5o;

    invoke-virtual {v3, v0}, LX/G5o;->a(LX/G5p;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2230900
    const-wide/16 v4, 0x3c

    :try_start_3
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const v6, 0x42bee5b9

    invoke-static {v0, v4, v5, v3, v6}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2230901
    :try_start_4
    invoke-static {v2}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v0

    .line 2230902
    if-nez v0, :cond_1

    .line 2230903
    new-instance v0, LX/EdT;

    const-string v3, "Remux result not valid"

    invoke-direct {v0, v3}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2230904
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2230905
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    throw v0

    .line 2230906
    :catchall_1
    move-exception v0

    move-object v3, v4

    :goto_0
    if-eqz v3, :cond_0

    .line 2230907
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    :cond_0
    throw v0

    .line 2230908
    :catch_0
    move-exception v0

    .line 2230909
    new-instance v3, LX/EdT;

    invoke-direct {v3, v0}, LX/EdT;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 2230910
    :cond_1
    iput-object v0, p1, LX/Edg;->f:[B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2230911
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2230912
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 2230913
    return-void

    .line 2230914
    :catchall_2
    move-exception v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/Edl;)V
    .locals 5

    .prologue
    .line 2230915
    iget-object v0, p1, LX/EdV;->b:LX/EdY;

    move-object v1, v0

    .line 2230916
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, LX/EdY;->b()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2230917
    invoke-virtual {v1, v0}, LX/EdY;->a(I)LX/Edg;

    move-result-object v2

    .line 2230918
    new-instance v3, Ljava/lang/String;

    invoke-virtual {v2}, LX/Edg;->g()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    .line 2230919
    const-string v4, "video/mp4"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2230920
    :try_start_0
    invoke-static {p0, v2}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/Edg;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2230921
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2230922
    :catch_0
    move-exception v1

    .line 2230923
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30m;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 2230924
    const-string v3, "sms_takeover_mms_remux_failure"

    invoke-static {v0, v3}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2230925
    const-string v4, "error_msg"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230926
    invoke-static {v0, v3}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2230927
    throw v1

    .line 2230928
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2230929
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->f:LX/FMB;

    invoke-virtual {v0, p1}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2230930
    if-eqz v0, :cond_0

    .line 2230931
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->g:LX/FMo;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v0, p2, v3}, LX/FMo;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/model/messages/Message;Landroid/net/Uri;Ljava/lang/Boolean;)V

    .line 2230932
    :goto_0
    return-void

    .line 2230933
    :cond_0
    const-string v0, "ProcessMmsDownloadedAction"

    const-string v1, "Failed to load received message %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;Landroid/net/Uri;)[B
    .locals 6

    .prologue
    .line 2230934
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    .line 2230935
    :try_start_0
    invoke-static {v1}, LX/1t3;->b(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2230936
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2230937
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    return-object v0

    .line 2230938
    :catch_0
    move-exception v0

    .line 2230939
    :try_start_1
    const-string v2, "ProcessMmsDownloadedAction"

    const-string v3, "Error processing extracting downloaded MMS content: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2230940
    new-instance v2, LX/EdT;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230941
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2230942
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_1
    throw v0
.end method

.method public static b(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;Landroid/net/Uri;)LX/FMW;
    .locals 14

    .prologue
    const/4 v6, 0x0

    .line 2230943
    sget-object v7, LX/FMW;->NO_ERROR:LX/FMW;

    .line 2230944
    const-string v0, "_id"

    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2230945
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/2UI;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2230946
    if-eqz v1, :cond_5

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2230947
    const-string v0, "resp_st"

    invoke-static {v1, v0}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    const/16 v2, 0xe4

    if-ne v0, v2, :cond_4

    .line 2230948
    sget-object v0, LX/FMW;->MESSAGE_NOT_FOUND:LX/FMW;

    .line 2230949
    :goto_0
    const-string v2, "exp"

    invoke-static {v1, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 2230950
    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 2230951
    sget-object v0, LX/FMW;->MESSAGE_EXPIRED:LX/FMW;

    .line 2230952
    :cond_0
    sget-object v2, LX/FMW;->NO_ERROR:LX/FMW;

    if-eq v0, v2, :cond_1

    .line 2230953
    const-string v2, "thread_id"

    invoke-static {v1, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 2230954
    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2230955
    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->g:LX/FMo;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2230956
    invoke-static {p1}, LX/FMQ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    .line 2230957
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2230958
    const-string v9, "deleteMessagesParams"

    new-instance v11, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    invoke-static {v8}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v8

    sget-object v12, LX/6hi;->CLIENT_ONLY:LX/6hi;

    invoke-direct {v11, v8, v12, v2}, Lcom/facebook/messaging/service/model/DeleteMessagesParams;-><init>(LX/0Rf;LX/6hi;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v10, v9, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2230959
    iget-object v8, v4, LX/FMo;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0aG;

    const-string v9, "delete_messages"

    sget-object v11, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v13, 0x7858cff9

    move-object v12, v5

    invoke-static/range {v8 .. v13}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v8

    invoke-interface {v8}, LX/1MF;->start()LX/1ML;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2230960
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 2230961
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2230962
    :cond_2
    return-object v0

    .line 2230963
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    .line 2230964
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2230965
    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_4
    move-object v0, v7

    goto :goto_0

    :cond_5
    move-object v0, v7

    goto :goto_1
.end method
