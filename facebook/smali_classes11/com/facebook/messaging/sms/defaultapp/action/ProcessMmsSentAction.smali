.class public Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/FMB;

.field private final c:LX/FMo;

.field private final d:LX/FMw;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FMB;LX/FMo;LX/FMw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231036
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->a:Landroid/content/Context;

    .line 2231037
    iput-object p2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->b:LX/FMB;

    .line 2231038
    iput-object p3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->c:LX/FMo;

    .line 2231039
    iput-object p4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->d:LX/FMw;

    .line 2231040
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/os/Bundle;)Lcom/facebook/messaging/model/messages/Message;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2231041
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->b:LX/FMB;

    invoke-virtual {v0, p1}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2231042
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->REGULAR:LX/2uW;

    if-ne v0, v1, :cond_0

    .line 2231043
    const-string v0, "ProcessMmsSentAction"

    const-string v1, "MMS has already been sent and processed. This should never happen."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231044
    const/4 v0, 0x0

    .line 2231045
    :goto_0
    return-object v0

    .line 2231046
    :cond_0
    const-string v0, "mmssms_error_type"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FMM;

    .line 2231047
    const-string v1, "content_uri"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 2231048
    if-eqz v1, :cond_1

    .line 2231049
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->a:Landroid/content/Context;

    invoke-static {v2, v1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    .line 2231050
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2231051
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2231052
    :cond_1
    sget-object v1, LX/FMM;->NO_ERROR:LX/FMM;

    if-ne v0, v1, :cond_2

    .line 2231053
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->a:Landroid/content/Context;

    invoke-static {v1}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v1

    .line 2231054
    :try_start_0
    sget-object v2, Landroid/provider/Telephony$Mms$Sent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v2}, LX/Edh;->a(Landroid/net/Uri;Landroid/net/Uri;)Landroid/net/Uri;
    :try_end_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 2231055
    :cond_2
    :goto_1
    sget-object v1, LX/FMM;->NO_ERROR:LX/FMM;

    if-eq v0, v1, :cond_3

    .line 2231056
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->a:Landroid/content/Context;

    invoke-static {v1, p1}, LX/FMn;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2231057
    :cond_3
    invoke-static {p2}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(Landroid/os/Bundle;)Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    move-result-object v1

    .line 2231058
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->d:LX/FMw;

    .line 2231059
    iget-object v3, v1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2231060
    iget-wide v6, v1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    move-wide v4, v6

    .line 2231061
    invoke-virtual {v2, v3, v4, v5}, LX/FMw;->b(Ljava/lang/String;J)V

    .line 2231062
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->b:LX/FMB;

    invoke-virtual {v1, p1}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2231063
    if-eqz v1, :cond_4

    .line 2231064
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->c:LX/FMo;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0}, LX/FMo;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/model/messages/Message;LX/FMM;)V

    :goto_2
    move-object v0, v1

    .line 2231065
    goto :goto_0

    .line 2231066
    :catch_0
    sget-object v0, LX/FMM;->PROCESSING_ERROR:LX/FMM;

    goto :goto_1

    .line 2231067
    :cond_4
    const-string v0, "ProcessMmsSentAction"

    const-string v2, "Failed to load sent mms for notification: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method
