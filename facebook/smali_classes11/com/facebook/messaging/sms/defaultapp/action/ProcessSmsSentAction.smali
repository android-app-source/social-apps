.class public Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FMM;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/FMB;

.field private final c:LX/FMo;

.field private final d:LX/FMw;

.field private final e:LX/FMd;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "LX/FMt;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2Oq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2231352
    sget-object v0, LX/FMM;->GENERIC:LX/FMM;

    sget-object v1, LX/FMM;->NO_CONNECTION:LX/FMM;

    sget-object v2, LX/FMM;->CONNECTION_ERROR:LX/FMM;

    sget-object v3, LX/FMM;->NO_ERROR:LX/FMM;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->h:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/FMB;LX/FMo;LX/FMw;LX/FMd;LX/2Oq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231354
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->a:Landroid/content/Context;

    .line 2231355
    iput-object p2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->b:LX/FMB;

    .line 2231356
    iput-object p3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->c:LX/FMo;

    .line 2231357
    iput-object p4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->d:LX/FMw;

    .line 2231358
    iput-object p5, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->e:LX/FMd;

    .line 2231359
    iput-object p6, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->g:LX/2Oq;

    .line 2231360
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->f:Ljava/util/Map;

    .line 2231361
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/FMM;)LX/FMM;
    .locals 1

    .prologue
    .line 2231362
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMt;

    .line 2231363
    if-nez v0, :cond_0

    :goto_0
    return-object p2

    .line 2231364
    :cond_0
    iget-object p0, v0, LX/FMt;->e:LX/FMM;

    move-object p2, p0

    .line 2231365
    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;Landroid/os/Bundle;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 10
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2231366
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->g:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2231367
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->b:LX/FMB;

    invoke-virtual {v0, p1}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2231368
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v2, LX/2uW;->REGULAR:LX/2uW;

    if-ne v0, v2, :cond_0

    .line 2231369
    const-string v0, "ProcessSmsSentAction"

    const-string v2, "SMS has already been sentand processed. This should never happen."

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2231370
    :goto_0
    return-object v0

    .line 2231371
    :cond_0
    const-string v0, "mmssms_error_type"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FMM;

    .line 2231372
    const-string v2, "number_of_parts"

    invoke-virtual {p2, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 2231373
    if-eq v2, v6, :cond_5

    .line 2231374
    iget-object v3, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->f:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FMt;

    .line 2231375
    if-nez v3, :cond_1

    .line 2231376
    new-instance v3, LX/FMt;

    invoke-direct {v3, p1, v2}, LX/FMt;-><init>(Landroid/net/Uri;I)V

    .line 2231377
    iget-object v4, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->f:Ljava/util/Map;

    invoke-interface {v4, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2231378
    :cond_1
    move-object v2, v3

    .line 2231379
    iget v3, v2, LX/FMt;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, LX/FMt;->c:I

    .line 2231380
    iget-object v3, v2, LX/FMt;->e:LX/FMM;

    if-eq v0, v3, :cond_3

    .line 2231381
    iget v3, v2, LX/FMt;->c:I

    iget v4, v2, LX/FMt;->b:I

    if-eq v3, v4, :cond_b

    .line 2231382
    sget-object v3, LX/FMM;->NO_ERROR:LX/FMM;

    if-eq v0, v3, :cond_2

    .line 2231383
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/FMt;->d:Z

    .line 2231384
    :cond_2
    :goto_1
    iget-object v3, v2, LX/FMt;->e:LX/FMM;

    .line 2231385
    sget-object v4, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->h:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    sget-object v5, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->h:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    if-ge v4, v5, :cond_c

    :goto_2
    move-object v3, v0

    .line 2231386
    iput-object v3, v2, LX/FMt;->e:LX/FMM;

    .line 2231387
    :cond_3
    iget v0, v2, LX/FMt;->c:I

    iget v3, v2, LX/FMt;->b:I

    if-ge v0, v3, :cond_d

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2231388
    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2231389
    goto :goto_0

    .line 2231390
    :cond_4
    iget-object v0, v2, LX/FMt;->e:LX/FMM;

    move-object v0, v0

    .line 2231391
    const-string v1, "mmssms_error_type"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2231392
    :cond_5
    sget-object v1, LX/FMM;->NO_ERROR:LX/FMM;

    if-ne v0, v1, :cond_9

    .line 2231393
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->a:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-static {v1, p1, v2, v7}, LX/554;->a(Landroid/content/Context;Landroid/net/Uri;II)Z

    move-result v1

    .line 2231394
    if-nez v1, :cond_6

    .line 2231395
    const-string v1, "ProcessSmsSentAction"

    const-string v2, "Failed to move message to sent box: %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v7

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2231396
    :cond_6
    :goto_4
    invoke-static {p2}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(Landroid/os/Bundle;)Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    move-result-object v1

    .line 2231397
    if-eqz v1, :cond_7

    .line 2231398
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->d:LX/FMw;

    .line 2231399
    iget-object v3, v1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2231400
    iget-wide v8, v1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    move-wide v4, v8

    .line 2231401
    invoke-virtual {v2, v3, v4, v5}, LX/FMw;->b(Ljava/lang/String;J)V

    .line 2231402
    :cond_7
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->b:LX/FMB;

    invoke-virtual {v1, p1}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2231403
    if-eqz v1, :cond_a

    .line 2231404
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 2231405
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->e:LX/FMd;

    iget-object v3, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, LX/FMd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231406
    :cond_8
    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->c:LX/FMo;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0}, LX/FMo;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/model/messages/Message;LX/FMM;)V

    :goto_5
    move-object v0, v1

    .line 2231407
    goto/16 :goto_0

    .line 2231408
    :cond_9
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->a:Landroid/content/Context;

    const/4 v2, 0x5

    invoke-static {v1, p1, v2, v6}, LX/554;->a(Landroid/content/Context;Landroid/net/Uri;II)Z

    goto :goto_4

    .line 2231409
    :cond_a
    const-string v0, "ProcessSmsSentAction"

    const-string v2, "Failed to load sent sms for notification: %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v7

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 2231410
    :cond_b
    sget-object v3, LX/FMM;->NO_ERROR:LX/FMM;

    if-eq v0, v3, :cond_2

    .line 2231411
    const/4 v3, 0x0

    iput-boolean v3, v2, LX/FMt;->d:Z

    goto/16 :goto_1

    :cond_c
    move-object v0, v3

    goto/16 :goto_2

    :cond_d
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 2231412
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMt;

    iget-boolean v0, v0, LX/FMt;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2231413
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2231414
    return-void
.end method
