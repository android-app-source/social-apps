.class public Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/30m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/2EJ;

.field private final t:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/44w",
            "<",
            "Landroid/content/ContentValues;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2229703
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2229704
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->s:LX/2EJ;

    .line 2229705
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    return-void
.end method

.method private a(LX/44w;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/44w",
            "<",
            "Landroid/content/ContentValues;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2229706
    iget-object v0, p1, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, Landroid/content/ContentValues;

    .line 2229707
    iget-object v1, p1, LX/44w;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    .line 2229708
    new-instance v2, LX/31Y;

    invoke-direct {v2, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f080972

    new-instance v4, LX/FMV;

    invoke-direct {v4, p0, v0, v1}, LX/FMV;-><init>(Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;Landroid/content/ContentValues;Ljava/lang/Integer;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080973

    new-instance v3, LX/FMU;

    invoke-direct {v3, p0}, LX/FMU;-><init>(Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080974

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const-string v2, "body"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/FMT;

    invoke-direct {v1, p0}, LX/FMT;-><init>(Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->s:LX/2EJ;

    .line 2229709
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->s:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2229710
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;

    invoke-static {v2}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b(LX/0QB;)Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;

    invoke-static {v2}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2}, LX/30m;->a(LX/0QB;)LX/30m;

    move-result-object v2

    check-cast v2, LX/30m;

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->p:Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;

    iput-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->q:Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->r:LX/30m;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;)V
    .locals 1

    .prologue
    .line 2229711
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2229712
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 2229713
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2229714
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->finish()V

    .line 2229715
    :goto_0
    return-void

    .line 2229716
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->a(LX/44w;)V

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2229717
    const/4 v0, 0x0

    .line 2229718
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2229719
    const-string v0, "sms_message"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 2229720
    :cond_0
    const-string v1, "body"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2229721
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    new-instance v2, LX/44w;

    const-string v3, "subscription"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2229722
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2229723
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2229724
    invoke-direct {p0, p1}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->c(Landroid/content/Intent;)V

    .line 2229725
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2229726
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->requestWindowFeature(I)Z

    .line 2229727
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2229728
    invoke-static {p0, p0}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2229729
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2229730
    if-eqz v0, :cond_0

    .line 2229731
    invoke-direct {p0, v0}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->c(Landroid/content/Intent;)V

    .line 2229732
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2229733
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->finish()V

    .line 2229734
    :cond_1
    :goto_0
    return-void

    .line 2229735
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 2229736
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->a(LX/44w;)V

    goto :goto_0
.end method
