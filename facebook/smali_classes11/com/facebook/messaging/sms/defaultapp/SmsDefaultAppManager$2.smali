.class public final Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/FMK;

.field public final synthetic c:LX/2uq;


# direct methods
.method public constructor <init>(LX/2uq;Landroid/content/Context;LX/FMK;)V
    .locals 0

    .prologue
    .line 2230244
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->c:LX/2uq;

    iput-object p2, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->b:LX/FMK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2230245
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->a:Landroid/content/Context;

    invoke-static {v0}, LX/2uq;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2230246
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->c:LX/2uq;

    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->b:LX/FMK;

    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v2, v3}, LX/2uq;->a(Ljava/lang/Object;Landroid/content/Context;Z)Z

    .line 2230247
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->c:LX/2uq;

    iget-object v0, v0, LX/2uq;->e:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->f()V

    .line 2230248
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/0aY;->C:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2230249
    const-string v1, "default_sms"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2230250
    const-string v1, "sms_enabled"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2230251
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$2;->c:LX/2uq;

    iget-object v1, v1, LX/2uq;->p:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2230252
    :cond_0
    return-void
.end method
