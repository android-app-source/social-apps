.class public Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2229737
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2229738
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2229739
    invoke-static {p0, p0}, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2229740
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 2229741
    if-nez p1, :cond_3

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2229742
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const/4 v6, 0x0

    .line 2229743
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    const-string v5, "\\?"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2229744
    aget-object v5, v3, v6

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2229745
    const/4 v3, 0x0

    .line 2229746
    :goto_0
    move-object v3, v3

    .line 2229747
    const-string v0, "address"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 2229748
    :goto_1
    const-string v5, "android.intent.extra.EMAIL"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2229749
    :goto_2
    if-nez v3, :cond_8

    if-nez v0, :cond_0

    if-eqz v1, :cond_8

    .line 2229750
    :cond_0
    if-eqz v0, :cond_6

    .line 2229751
    const-string v0, "address"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2229752
    :goto_3
    const-string v1, "sms_body"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2229753
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2229754
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    .line 2229755
    if-nez v1, :cond_c

    .line 2229756
    :cond_1
    :goto_4
    move-object v1, v2

    .line 2229757
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2229758
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2229759
    :cond_2
    if-eqz v0, :cond_7

    .line 2229760
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.messaging.sms.COMPOSE_SMS"

    const/4 v4, 0x0

    const-class v5, LX/FMm;

    invoke-direct {v2, v3, v4, p0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2229761
    const-string v3, "addresses"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2229762
    const-string v0, "message"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2229763
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2229764
    :cond_3
    :goto_5
    invoke-virtual {p0}, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;->finish()V

    .line 2229765
    return-void

    :cond_4
    move v0, v2

    .line 2229766
    goto :goto_1

    :cond_5
    move v1, v2

    .line 2229767
    goto :goto_2

    .line 2229768
    :cond_6
    const-string v0, "android.intent.extra.EMAIL"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2229769
    :cond_7
    if-eqz v1, :cond_3

    .line 2229770
    new-instance v0, Landroid/content/Intent;

    sget-object v2, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2229771
    sget-object v2, LX/3RH;->r:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2229772
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2229773
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_5

    :cond_8
    move-object v0, v3

    goto :goto_3

    :cond_9
    aget-object v3, v3, v6

    .line 2229774
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2229775
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    array-length v8, v7

    const/4 v5, 0x0

    :goto_6
    if-ge v5, v8, :cond_b

    aget-char v9, v7, v5

    .line 2229776
    const/16 p1, 0xa

    invoke-static {v9, p1}, Ljava/lang/Character;->digit(CI)I

    move-result p1

    .line 2229777
    const/4 v0, -0x1

    if-eq p1, v0, :cond_a

    .line 2229778
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2229779
    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 2229780
    :cond_a
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 2229781
    :cond_b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v3, v5

    .line 2229782
    const/16 v5, 0x3b

    const/16 v6, 0x2c

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2229783
    :cond_c
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 2229784
    const-string v5, "?"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2229785
    const/16 v5, 0x3f

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2229786
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 2229787
    array-length v6, v5

    const/4 v3, 0x0

    :goto_8
    if-ge v3, v6, :cond_1

    aget-object v7, v5, v3

    .line 2229788
    const-string v8, "body="

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 2229789
    const/4 v8, 0x5

    :try_start_0
    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "UTF-8"

    invoke-static {v7, v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto/16 :goto_4

    .line 2229790
    :catch_0
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_8
.end method
