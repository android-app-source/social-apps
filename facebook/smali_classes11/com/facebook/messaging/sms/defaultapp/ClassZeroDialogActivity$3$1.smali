.class public final Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity$3$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/FMV;


# direct methods
.method public constructor <init>(LX/FMV;)V
    .locals 0

    .prologue
    .line 2229690
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity$3$1;->a:LX/FMV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2229691
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity$3$1;->a:LX/FMV;

    iget-object v0, v0, LX/FMV;->c:Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;

    iget-object v0, v0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->r:LX/30m;

    const-string v1, "action_save_class_zero_message"

    invoke-virtual {v0, v1}, LX/30m;->b(Ljava/lang/String;)V

    .line 2229692
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity$3$1;->a:LX/FMV;

    iget-object v0, v0, LX/FMV;->c:Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;

    iget-object v0, v0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity;->p:Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;

    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity$3$1;->a:LX/FMV;

    iget-object v1, v1, LX/FMV;->a:Landroid/content/ContentValues;

    iget-object v2, p0, Lcom/facebook/messaging/sms/defaultapp/ClassZeroDialogActivity$3$1;->a:LX/FMV;

    iget-object v2, v2, LX/FMV;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 p0, 0x1

    .line 2229693
    const-string v3, "read"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2229694
    const-string v3, "seen"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2229695
    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->b(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/content/ContentValues;I)Landroid/net/Uri;

    move-result-object v3

    .line 2229696
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;Landroid/net/Uri;Ljava/lang/Boolean;)V

    .line 2229697
    return-void
.end method
