.class public Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FMu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Or;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2231443
    const-class v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;

    const-string v1, "image_fetch_mms"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2231444
    const-string v0, "image/png"

    const-string v1, "image/jpeg"

    const-string v2, "image/jpg"

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231446
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231447
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->c:LX/0Ot;

    .line 2231448
    return-void
.end method

.method private static a(Lcom/facebook/ui/media/attachments/MediaResource;III)Z
    .locals 4

    .prologue
    .line 2231449
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2231450
    sget-object v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 2231451
    :goto_0
    move v0, v0

    .line 2231452
    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-gt v0, p2, :cond_0

    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-le v0, p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/HashSet;II)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/io/File;",
            ">;II)",
            "Lcom/facebook/ui/media/attachments/MediaResource;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 2231453
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->d:LX/FMu;

    .line 2231454
    invoke-static {v0, p4}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "maxImageWidth"

    const/16 v3, 0x280

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move v0, v1

    .line 2231455
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->d:LX/FMu;

    .line 2231456
    invoke-static {v1, p4}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "maxImageHeight"

    const/16 v4, 0x1e0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    move v1, v2

    .line 2231457
    invoke-static {p1, p3, v0, v1}, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->a(Lcom/facebook/ui/media/attachments/MediaResource;III)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2231458
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    .line 2231459
    iget v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-gt v3, v0, :cond_0

    iget v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-gt v3, v1, :cond_0

    const-string v3, "image/png"

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2231460
    :cond_0
    new-instance v3, LX/FMZ;

    invoke-direct {v3, v0, v1}, LX/FMZ;-><init>(II)V

    .line 2231461
    iput-object v3, v2, LX/1bX;->j:LX/33B;

    .line 2231462
    :cond_1
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 2231463
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    sget-object v2, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    invoke-static {v0}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    .line 2231464
    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2231465
    if-eqz v0, :cond_5

    :try_start_1
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_5

    .line 2231466
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lm;

    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2231467
    invoke-static {}, LX/FMY;->d()Landroid/net/Uri;

    move-result-object v1

    .line 2231468
    iget-object v3, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->b:Landroid/content/Context;

    invoke-static {v3, v1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 2231469
    new-instance v4, LX/5zn;

    invoke-direct {v4}, LX/5zn;-><init>()V

    invoke-virtual {v4, p1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v4

    .line 2231470
    iput-object v1, v4, LX/5zn;->b:Landroid/net/Uri;

    .line 2231471
    move-object v1, v4

    .line 2231472
    const-string v4, "image/jpeg"

    .line 2231473
    iput-object v4, v1, LX/5zn;->q:Ljava/lang/String;

    .line 2231474
    move-object v4, v1

    .line 2231475
    invoke-virtual {p2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2231476
    const/16 v1, 0x5f

    .line 2231477
    :cond_2
    new-instance v5, Ljava/io/FileOutputStream;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2231478
    :try_start_2
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2, v6, v1, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2231479
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 2231480
    add-int/lit8 v1, v1, -0xa

    .line 2231481
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    int-to-long v8, p3

    cmp-long v5, v6, v8

    if-lez v5, :cond_3

    const/4 v5, 0x5

    if-ge v1, v5, :cond_2

    .line 2231482
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->length()J

    .line 2231483
    invoke-virtual {v4}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    .line 2231484
    :try_start_4
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-object p1, v1

    .line 2231485
    :cond_4
    return-object p1

    .line 2231486
    :catchall_0
    move-exception v1

    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2231487
    :catchall_1
    move-exception v1

    :try_start_6
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 2231488
    :catch_0
    move-exception v0

    .line 2231489
    const-string v1, "MmsPhotoAttachmentHelper"

    const-string v2, "Failed creating photo uri for resource: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v4, v3, v10

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2231490
    new-instance v1, LX/EdT;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2231491
    :cond_5
    new-instance v1, LX/EdT;

    const-string v2, "Could not decode image to bitmap"

    invoke-direct {v1, v2}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1
.end method
