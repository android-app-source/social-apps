.class public Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:J

.field public d:I

.field public e:I

.field public f:Z

.field public g:LX/FMM;

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2231793
    new-instance v0, LX/FMz;

    invoke-direct {v0}, LX/FMz;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2231794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231795
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    .line 2231796
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    .line 2231797
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->c:J

    .line 2231798
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    .line 2231799
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->f:Z

    .line 2231800
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d:I

    .line 2231801
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/FMM;->valueOf(Ljava/lang/String;)LX/FMM;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    .line 2231802
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->h:I

    .line 2231803
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJIII)V
    .locals 2

    .prologue
    .line 2231777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231778
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    .line 2231779
    iput-wide p2, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    .line 2231780
    iput-wide p4, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->c:J

    .line 2231781
    iput p6, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d:I

    .line 2231782
    iput p7, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    .line 2231783
    sget-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    .line 2231784
    iput p8, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->h:I

    .line 2231785
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2231810
    const-string v0, "mmssms_custom_bundle"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2231811
    if-eqz v0, :cond_0

    .line 2231812
    const-string v1, "pending_send_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    .line 2231813
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 2

    .prologue
    .line 2231804
    const-string v0, "mmssms_custom_bundle"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2231805
    if-nez v0, :cond_0

    .line 2231806
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2231807
    const-string v1, "mmssms_custom_bundle"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2231808
    :cond_0
    const-string v1, "pending_send_message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2231809
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2231814
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/FMM;)V
    .locals 0

    .prologue
    .line 2231790
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    .line 2231791
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 2231792
    iget-wide v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2231789
    iget v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2231788
    const/4 v0, 0x0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2231787
    iget v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d:I

    return v0
.end method

.method public final i()LX/FMM;
    .locals 1

    .prologue
    .line 2231786
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2231776
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PendingSendMessage{mMessageId=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mThreadId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimestampMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRetryCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsExpired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMessageSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLastErrorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSubId ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2231767
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2231768
    iget-wide v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2231769
    iget-wide v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2231770
    iget v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2231771
    iget-boolean v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2231772
    iget v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2231773
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    invoke-virtual {v0}, LX/FMM;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2231774
    iget v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2231775
    return-void
.end method
