.class public Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ml;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8mv;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8jJ;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8jI;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3dt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2231647
    const-class v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;

    const-string v1, "image_fetch_mms"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2231648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231649
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231650
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->c:LX/0Ot;

    .line 2231651
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231652
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->d:LX/0Ot;

    .line 2231653
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231654
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->e:LX/0Ot;

    .line 2231655
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231656
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->f:LX/0Ot;

    .line 2231657
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231658
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->g:LX/0Ot;

    .line 2231659
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231660
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->h:LX/0Ot;

    .line 2231661
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231662
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->i:LX/0Ot;

    .line 2231663
    return-void
.end method

.method private static a(Landroid/graphics/Bitmap;Ljava/lang/String;)LX/Edg;
    .locals 4

    .prologue
    .line 2231664
    new-instance v0, LX/Edg;

    invoke-direct {v0}, LX/Edg;-><init>()V

    .line 2231665
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2231666
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2231667
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    move-object v1, v1

    .line 2231668
    iput-object v1, v0, LX/Edg;->f:[B

    .line 2231669
    const-string v1, "image/png"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Edg;->e([B)V

    .line 2231670
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sticker:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/FMe;->a(LX/Edg;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2231671
    return-object v0

    .line 2231672
    :catch_0
    move-exception v0

    .line 2231673
    const-string v1, "MmsStickerAttachmentHelper"

    const-string v2, "Failed creating photo pdu part for sticker"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2231674
    new-instance v1, LX/EdT;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Landroid/net/Uri;Ljava/lang/String;)LX/Edg;
    .locals 5

    .prologue
    .line 2231675
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2231676
    invoke-static {v0, p2}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)LX/Edg;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2231677
    :catch_0
    move-exception v0

    .line 2231678
    const-string v1, "MmsStickerAttachmentHelper"

    const-string v2, "Failed creating photo pdu part for resource: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2231679
    new-instance v1, LX/EdT;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Lcom/facebook/stickers/model/Sticker;)LX/Edg;
    .locals 4

    .prologue
    .line 2231680
    const/4 v1, 0x0

    .line 2231681
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 2231682
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    .line 2231683
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2231684
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v2, LX/FMx;

    invoke-direct {v2, p0, p1}, LX/FMx;-><init>(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Lcom/facebook/stickers/model/Sticker;)V

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2231685
    invoke-static {p0}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->b$redex0(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/1v3;->a(Ljava/util/concurrent/Future;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 2231686
    if-eqz v0, :cond_5

    .line 2231687
    iget-boolean v2, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v2

    .line 2231688
    if-eqz v0, :cond_5

    .line 2231689
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dt;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 2231690
    if-eqz v0, :cond_5

    .line 2231691
    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v2, :cond_3

    .line 2231692
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    .line 2231693
    :goto_1
    if-eqz v0, :cond_4

    .line 2231694
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Landroid/net/Uri;Ljava/lang/String;)LX/Edg;

    move-result-object v0

    return-object v0

    .line 2231695
    :cond_2
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2231696
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    goto :goto_0

    .line 2231697
    :cond_3
    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-eqz v2, :cond_5

    .line 2231698
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    goto :goto_1

    .line 2231699
    :cond_4
    new-instance v0, LX/EdT;

    const-string v1, "Unable to get image for sticker"

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method private static b(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Lcom/facebook/stickers/model/Sticker;)LX/Edg;
    .locals 4

    .prologue
    .line 2231700
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/8mv;->c(Lcom/facebook/stickers/model/Sticker;)LX/1bf;

    move-result-object v1

    .line 2231701
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    sget-object v2, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    invoke-static {v0}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    .line 2231702
    invoke-static {p0}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->b$redex0(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/1v3;->a(Ljava/util/concurrent/Future;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 2231703
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2231704
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lm;

    .line 2231705
    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2231706
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)LX/Edg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2231707
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    return-object v1

    .line 2231708
    :cond_0
    :try_start_1
    new-instance v1, LX/EdT;

    const-string v2, "Unable to download sticker image"

    invoke-direct {v1, v2}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231709
    :catchall_0
    move-exception v1

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1
.end method

.method public static b$redex0(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;)J
    .locals 8

    .prologue
    .line 2231710
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uq;

    .line 2231711
    iget-object v2, v0, LX/2Uq;->a:LX/0ad;

    sget-wide v4, LX/6jD;->w:J

    const-wide/32 v6, 0xea60

    invoke-interface {v2, v4, v5, v6, v7}, LX/0ad;->a(JJ)J

    move-result-wide v2

    move-wide v0, v2

    .line 2231712
    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/Edg;
    .locals 3

    .prologue
    .line 2231713
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dt;

    invoke-virtual {v0, p1}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 2231714
    if-nez v0, :cond_2

    .line 2231715
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8jI;

    invoke-virtual {v0, p1}, LX/8jI;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2231716
    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    move-object v1, v0

    .line 2231717
    :goto_0
    if-eqz v1, :cond_1

    .line 2231718
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ml;

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2231719
    :try_start_0
    invoke-static {p0, v1}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Lcom/facebook/stickers/model/Sticker;)LX/Edg;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 2231720
    :goto_1
    return-object v0

    .line 2231721
    :catch_0
    :goto_2
    invoke-static {p0, v1}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->b(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Lcom/facebook/stickers/model/Sticker;)LX/Edg;

    move-result-object v0

    goto :goto_1

    .line 2231722
    :cond_0
    invoke-static {p0, v1}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->b(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Lcom/facebook/stickers/model/Sticker;)LX/Edg;

    move-result-object v0

    goto :goto_1

    .line 2231723
    :cond_1
    new-instance v0, LX/EdT;

    const-string v1, "Unable to get sticker information"

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2231724
    :catch_1
    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method
