.class public final Lcom/facebook/messaging/sms/defaultapp/send/MmsVideoAttachmentHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:LX/FMy;


# direct methods
.method public constructor <init>(LX/FMy;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2231725
    iput-object p1, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsVideoAttachmentHelper$1;->b:LX/FMy;

    iput-object p2, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsVideoAttachmentHelper$1;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2231726
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2231727
    const-string v0, "mediaResource"

    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsVideoAttachmentHelper$1;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2231728
    const-string v0, "transcode"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2231729
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/MmsVideoAttachmentHelper$1;->b:LX/FMy;

    iget-object v0, v0, LX/FMy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "video_transcode"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, LX/FMy;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x6b63795d    # 2.7499928E26f

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method
