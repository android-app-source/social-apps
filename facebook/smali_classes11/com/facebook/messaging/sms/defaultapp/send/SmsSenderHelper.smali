.class public Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FMy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FMe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Or;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMu;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/SmsThreadManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232030
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232031
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->f:LX/0Ot;

    .line 2232032
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232033
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->g:LX/0Ot;

    .line 2232034
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232035
    iput-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->h:LX/0Ot;

    .line 2232036
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;
    .locals 11

    .prologue
    .line 2232037
    sget-object v0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->i:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    if-nez v0, :cond_1

    .line 2232038
    const-class v1, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    monitor-enter v1

    .line 2232039
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->i:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232040
    if-eqz v2, :cond_0

    .line 2232041
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232042
    new-instance v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    invoke-direct {v3}, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;-><init>()V

    .line 2232043
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    .line 2232044
    new-instance v8, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;

    invoke-direct {v8}, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;-><init>()V

    .line 2232045
    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    const/16 v6, 0xb99

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/FMu;->b(LX/0QB;)LX/FMu;

    move-result-object v6

    check-cast v6, LX/FMu;

    invoke-static {v0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v7

    check-cast v7, LX/2Or;

    .line 2232046
    iput-object v5, v8, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->b:Landroid/content/Context;

    iput-object v9, v8, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->c:LX/0Ot;

    iput-object v6, v8, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->d:LX/FMu;

    iput-object v7, v8, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->e:LX/2Or;

    .line 2232047
    move-object v5, v8

    .line 2232048
    check-cast v5, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;

    .line 2232049
    new-instance v6, LX/FMy;

    invoke-direct {v6}, LX/FMy;-><init>()V

    .line 2232050
    const/16 v7, 0x542

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x140f

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xd49

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/FMu;->b(LX/0QB;)LX/FMu;

    move-result-object v10

    check-cast v10, LX/FMu;

    const/16 p0, 0x2d7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2232051
    iput-object v7, v6, LX/FMy;->a:LX/0Ot;

    iput-object v8, v6, LX/FMy;->b:LX/0Ot;

    iput-object v9, v6, LX/FMy;->c:LX/0Ot;

    iput-object v10, v6, LX/FMy;->d:LX/FMu;

    iput-object p0, v6, LX/FMy;->e:LX/0Ot;

    .line 2232052
    move-object v6, v6

    .line 2232053
    check-cast v6, LX/FMy;

    .line 2232054
    new-instance v8, LX/FMe;

    invoke-static {v0}, LX/2Ma;->a(LX/0QB;)LX/2Ma;

    move-result-object v7

    check-cast v7, LX/2Ma;

    invoke-direct {v8, v7}, LX/FMe;-><init>(LX/2Ma;)V

    .line 2232055
    move-object v7, v8

    .line 2232056
    check-cast v7, LX/FMe;

    invoke-static {v0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v8

    check-cast v8, LX/2Or;

    const/16 v9, 0x2999

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xd9c

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p0, 0xb45

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2232057
    iput-object v4, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->a:Landroid/content/Context;

    iput-object v5, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->b:Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;

    iput-object v6, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->c:LX/FMy;

    iput-object v7, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->d:LX/FMe;

    iput-object v8, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->e:LX/2Or;

    iput-object v9, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->f:LX/0Ot;

    iput-object v10, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->g:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->h:LX/0Ot;

    .line 2232058
    move-object v0, v3

    .line 2232059
    sput-object v0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->i:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232060
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232061
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232062
    :cond_1
    sget-object v0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->i:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    return-object v0

    .line 2232063
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2232065
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2232066
    :goto_0
    return v0

    .line 2232067
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMu;

    iget v3, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    .line 2232068
    invoke-static {v0, v3}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "smsToMmsTextLengthThreshold"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    move v0, v4

    .line 2232069
    if-ltz v0, :cond_1

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v0, :cond_1

    move v0, v2

    .line 2232070
    goto :goto_0

    .line 2232071
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMu;

    iget v3, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    const/16 v9, 0x1e

    .line 2232072
    invoke-static {v0, v3}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "smsToMmsTextThreshold"

    invoke-virtual {v5, v6, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 2232073
    if-lez v5, :cond_4

    :goto_1
    move v0, v5

    .line 2232074
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v4, v0, 0x3f

    if-ge v3, v4, :cond_2

    move v0, v1

    .line 2232075
    goto :goto_0

    .line 2232076
    :cond_2
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v3

    .line 2232077
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2232078
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v5, v0, LX/FMu;->g:LX/0W3;

    sget-wide v7, LX/0X5;->hs:J

    invoke-interface {v5, v7, v8, v9}, LX/0W4;->a(JI)I

    move-result v5

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;ILjava/util/HashSet;)LX/EdY;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            "I",
            "Ljava/util/HashSet",
            "<",
            "Ljava/io/File;",
            ">;)",
            "LX/EdY;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2232079
    :try_start_0
    new-instance v5, LX/EdY;

    invoke-direct {v5}, LX/EdY;-><init>()V

    .line 2232080
    new-instance v6, LX/FN5;

    invoke-direct {v6}, LX/FN5;-><init>()V

    .line 2232081
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v7, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v4

    move v2, v4

    :goto_0
    if-ge v3, v8, :cond_1

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2232082
    invoke-static {v0}, LX/5zs;->from(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;

    move-result-object v1

    .line 2232083
    sget-object v9, LX/5zs;->HTTP:LX/5zs;

    if-eq v9, v1, :cond_0

    sget-object v9, LX/5zs;->HTTPS:LX/5zs;

    if-ne v9, v1, :cond_4

    .line 2232084
    :cond_0
    invoke-static {}, LX/FMY;->d()Landroid/net/Uri;

    move-result-object v1

    .line 2232085
    iget-object v9, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->a:Landroid/content/Context;

    invoke-static {v9, v1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;
    :try_end_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 2232086
    :try_start_1
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    new-instance v10, Ljava/net/URI;

    iget-object v11, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v10}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 2232087
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v10

    const-string v11, "MmsForwardTempDownload"

    .line 2232088
    iput-object v11, v10, LX/15E;->c:Ljava/lang/String;

    .line 2232089
    move-object v10, v10

    .line 2232090
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v11

    .line 2232091
    iput-object v11, v10, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2232092
    move-object v10, v10

    .line 2232093
    iput-object v1, v10, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2232094
    move-object v1, v10

    .line 2232095
    new-instance v10, LX/FN3;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v9}, LX/FN3;-><init>(Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;Ljava/io/File;)V

    .line 2232096
    iput-object v10, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2232097
    move-object v1, v1

    .line 2232098
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object v10

    .line 2232099
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v10}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    .line 2232100
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2232101
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2232102
    move-object v0, v0

    .line 2232103
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch LX/EdT; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v1, v0

    .line 2232104
    :goto_1
    :try_start_2
    sget-object v0, LX/FN2;->a:[I

    iget-object v9, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v9}, LX/2MK;->ordinal()I

    move-result v9

    aget v0, v0, v9

    packed-switch v0, :pswitch_data_0

    move v0, v2

    .line 2232105
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto/16 :goto_0

    .line 2232106
    :catch_0
    move-exception v0

    .line 2232107
    const-string v1, "SmsSenderHelper"

    const-string v2, "Incorrect URI on MediaResource when temporarily downloading to forward to SMS"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2232108
    new-instance v0, LX/EdT;

    const-string v1, "Incorrect URI on MediaResource when temporarily downloading to forward to SMS"

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch LX/EdT; {:try_start_2 .. :try_end_2} :catch_1

    .line 2232109
    :catch_1
    move-exception v0

    .line 2232110
    const-string v1, "SmsSenderHelper"

    const-string v2, "Queue mms failed."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2232111
    throw v0

    .line 2232112
    :catch_2
    move-exception v0

    .line 2232113
    :try_start_3
    const-string v1, "SmsSenderHelper"

    const-string v2, "Failed to temporarily download MediaResource to forward to SMS"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2232114
    new-instance v0, LX/EdT;

    const-string v1, "Failed to temporarily download MediaResource to forward to SMS"

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2232115
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->b:Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v9, v9, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    div-int v9, p2, v9

    iget v10, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    invoke-virtual {v0, v1, p3, v9, v10}, Lcom/facebook/messaging/sms/defaultapp/send/MmsPhotoAttachmentHelper;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/HashSet;II)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v1

    .line 2232116
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v0, "photo"

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2232117
    invoke-virtual {v6, v2}, LX/FN5;->a(Ljava/lang/String;)V

    .line 2232118
    iget-object v9, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-static {v9, v1, v2}, LX/FMe;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)LX/Edg;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/EdY;->a(LX/Edg;)Z

    goto :goto_2

    .line 2232119
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->c:LX/FMy;

    iget v9, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    invoke-virtual {v0, v1, v9}, LX/FMy;->a(Lcom/facebook/ui/media/attachments/MediaResource;I)Landroid/net/Uri;

    move-result-object v9

    .line 2232120
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v0, "video"

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2232121
    iget-wide v10, v1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-virtual {v6, v2, v10, v11}, LX/FN5;->a(Ljava/lang/String;J)V

    .line 2232122
    iget-object v1, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->d:LX/FMe;

    invoke-virtual {v1, v9, v2}, LX/FMe;->a(Landroid/net/Uri;Ljava/lang/String;)LX/Edg;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/EdY;->a(LX/Edg;)Z

    goto/16 :goto_2

    .line 2232123
    :pswitch_2
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v0, "audio"

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2232124
    iget-wide v10, v1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-virtual {v6, v2, v10, v11}, LX/FN5;->b(Ljava/lang/String;J)V

    .line 2232125
    iget-object v9, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->d:LX/FMe;

    invoke-virtual {v9, v1, v2}, LX/FMe;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)LX/Edg;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/EdY;->a(LX/Edg;)Z

    goto/16 :goto_2

    .line 2232126
    :pswitch_3
    new-instance v0, LX/EdT;

    const-string v1, "Other media resource type is not supported for mms"

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2232127
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2232128
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "text"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2232129
    const/4 v3, 0x1

    .line 2232130
    iput-boolean v3, v6, LX/FN5;->b:Z

    .line 2232131
    iget-object v1, v6, LX/FN5;->a:Ljava/util/List;

    const-string v2, "<par dur=\"5000ms\"><text src=\"%s\" region=\"Text\" /></par>"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2232132
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2232133
    new-instance v2, LX/Edg;

    invoke-direct {v2}, LX/Edg;-><init>()V

    .line 2232134
    const/16 v3, 0x6a

    invoke-virtual {v2, v3}, LX/Edg;->a(I)V

    .line 2232135
    const-string v3, "text/plain"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Edg;->e([B)V

    .line 2232136
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 2232137
    iput-object v3, v2, LX/Edg;->f:[B

    .line 2232138
    invoke-static {v2, v0}, LX/FMe;->a(LX/Edg;Ljava/lang/String;)V

    .line 2232139
    move-object v0, v2

    .line 2232140
    invoke-virtual {v5, v0}, LX/EdY;->a(LX/Edg;)Z

    .line 2232141
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2232142
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sticker:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2232143
    invoke-virtual {v6, v0}, LX/FN5;->a(Ljava/lang/String;)V

    .line 2232144
    :goto_3
    const/4 v1, 0x0

    invoke-virtual {v6}, LX/FN5;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2232145
    new-instance v3, LX/Edg;

    invoke-direct {v3}, LX/Edg;-><init>()V

    .line 2232146
    const-string v6, "application/smil"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, LX/Edg;->e([B)V

    .line 2232147
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 2232148
    iput-object v6, v3, LX/Edg;->f:[B

    .line 2232149
    invoke-static {v3, v0}, LX/FMe;->a(LX/Edg;Ljava/lang/String;)V

    .line 2232150
    move-object v0, v3

    .line 2232151
    invoke-virtual {v5, v1, v0}, LX/EdY;->a(ILX/Edg;)V

    .line 2232152
    return-object v5

    .line 2232153
    :cond_3
    const-string v0, "smil"
    :try_end_3
    .catch LX/EdT; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2232154
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v0, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 2232155
    :goto_0
    return v0

    .line 2232156
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->b(Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2232157
    goto :goto_0

    .line 2232158
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v0

    .line 2232159
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v1, :cond_3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method
