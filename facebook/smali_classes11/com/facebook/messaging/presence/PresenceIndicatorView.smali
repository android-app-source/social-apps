.class public Lcom/facebook/messaging/presence/PresenceIndicatorView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Landroid/widget/ImageView;

.field private c:I

.field private d:LX/FKA;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/FKA;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2224693
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2224694
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 2224724
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2224725
    sget-object v0, LX/FKA;->NONE:LX/FKA;

    iput-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->d:LX/FKA;

    .line 2224726
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->f:Ljava/util/Map;

    .line 2224727
    iput-boolean v5, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->g:Z

    .line 2224728
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setOrientation(I)V

    .line 2224729
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setGravity(I)V

    .line 2224730
    sget-object v0, LX/03r;->PresenceIndicatorView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2224731
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2224732
    sget-object v0, LX/FKA;->ONLINE:LX/FKA;

    const v2, 0x7f0212ec

    invoke-direct {p0, v0, v2}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a(LX/FKA;I)V

    .line 2224733
    sget-object v0, LX/FKA;->PUSHABLE:LX/FKA;

    const v2, 0x7f02128b

    invoke-direct {p0, v0, v2}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a(LX/FKA;I)V

    .line 2224734
    :cond_0
    new-instance v0, Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f0103d4

    invoke-direct {v0, p1, v6, v2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2224735
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2224736
    const/16 v0, 0x3

    const v2, 0x7f0104e8

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2224737
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1, v6, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->b:Landroid/widget/ImageView;

    .line 2224738
    const/16 v0, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 2224739
    if-ne v0, v5, :cond_2

    .line 2224740
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->addView(Landroid/view/View;)V

    .line 2224741
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->addView(Landroid/view/View;)V

    .line 2224742
    :goto_0
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 2224743
    if-ne v0, v4, :cond_1

    .line 2224744
    invoke-virtual {p0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0317

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2224745
    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setTextColor(I)V

    .line 2224746
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2224747
    return-void

    .line 2224748
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->addView(Landroid/view/View;)V

    .line 2224749
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2224710
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->b:Landroid/widget/ImageView;

    const v1, 0x7f021180

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2224711
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->d:LX/FKA;

    sget-object v1, LX/FKA;->AVAILABLE_ON_MOBILE:LX/FKA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->d:LX/FKA;

    sget-object v1, LX/FKA;->AVAILABLE_ON_WEB:LX/FKA;

    if-ne v0, v1, :cond_1

    .line 2224712
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2224713
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2224714
    :goto_0
    return-void

    .line 2224715
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->d:LX/FKA;

    sget-object v1, LX/FKA;->ONLINE:LX/FKA;

    if-ne v0, v1, :cond_2

    .line 2224716
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2224717
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2224718
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2224719
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2224720
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2224721
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setGravity(I)V

    .line 2224722
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2224723
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(LX/FKA;I)V
    .locals 2

    .prologue
    .line 2224708
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->f:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224709
    return-void
.end method


# virtual methods
.method public final a(LX/FKA;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2224704
    iput-object p1, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->d:LX/FKA;

    .line 2224705
    iput-object p2, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->e:Ljava/lang/String;

    .line 2224706
    invoke-direct {p0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a()V

    .line 2224707
    return-void
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 2224703
    iget v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->c:I

    return v0
.end method

.method public setShowIcon(Z)V
    .locals 0

    .prologue
    .line 2224700
    iput-boolean p1, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->g:Z

    .line 2224701
    invoke-direct {p0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a()V

    .line 2224702
    return-void
.end method

.method public setStatus(LX/FKA;)V
    .locals 1

    .prologue
    .line 2224698
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a(LX/FKA;Ljava/lang/String;)V

    .line 2224699
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 2224695
    iput p1, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->c:I

    .line 2224696
    iget-object v0, p0, Lcom/facebook/messaging/presence/PresenceIndicatorView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2224697
    return-void
.end method
