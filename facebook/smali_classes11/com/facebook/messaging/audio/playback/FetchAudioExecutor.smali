.class public Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;


# instance fields
.field public final b:LX/2Vw;

.field public final c:LX/FCr;

.field private final d:LX/FCm;

.field public final e:LX/44Z;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2210755
    const-class v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    sput-object v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Vw;LX/FCr;LX/44Z;LX/FCm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2210756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210757
    iput-object p1, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->b:LX/2Vw;

    .line 2210758
    iput-object p2, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->c:LX/FCr;

    .line 2210759
    iput-object p4, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->d:LX/FCm;

    .line 2210760
    iput-object p3, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->e:LX/44Z;

    .line 2210761
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;
    .locals 7

    .prologue
    .line 2210762
    sget-object v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->f:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    if-nez v0, :cond_1

    .line 2210763
    const-class v1, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    monitor-enter v1

    .line 2210764
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->f:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2210765
    if-eqz v2, :cond_0

    .line 2210766
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2210767
    new-instance p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    invoke-static {v0}, LX/2Vw;->a(LX/0QB;)LX/2Vw;

    move-result-object v3

    check-cast v3, LX/2Vw;

    invoke-static {v0}, LX/FCr;->a(LX/0QB;)LX/FCr;

    move-result-object v4

    check-cast v4, LX/FCr;

    invoke-static {v0}, LX/44Z;->a(LX/0QB;)LX/44Z;

    move-result-object v5

    check-cast v5, LX/44Z;

    invoke-static {v0}, LX/FCm;->a(LX/0QB;)LX/FCm;

    move-result-object v6

    check-cast v6, LX/FCm;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;-><init>(LX/2Vw;LX/FCr;LX/44Z;LX/FCm;)V

    .line 2210768
    move-object v0, p0

    .line 2210769
    sput-object v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->f:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210770
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2210771
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2210772
    :cond_1
    sget-object v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->f:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    return-object v0

    .line 2210773
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2210774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/FCv;)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 2210775
    invoke-virtual {p1}, LX/FCv;->b()LX/FCa;

    move-result-object v1

    .line 2210776
    iget-object v0, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->b:LX/2Vw;

    invoke-virtual {v0, v1}, LX/2Vx;->e(LX/2WG;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2210777
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fetching "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2210778
    iget-object v3, v1, LX/FCa;->a:Landroid/net/Uri;

    move-object v1, v3

    .line 2210779
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was failed recently. Waiting for retrying."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2210780
    :cond_0
    iget-object v0, p1, LX/FCv;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 2210781
    new-instance v2, LX/FCu;

    invoke-direct {v2, p0, v1}, LX/FCu;-><init>(Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/FCa;)V

    .line 2210782
    new-instance v3, LX/34X;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "audio_download"

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-direct {v3, v0, v2, v4}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2210783
    const-string v0, "Download audio"

    const v2, 0x9bd0cb2

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2210784
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->d:LX/FCm;

    invoke-virtual {v0, v3}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;
    :try_end_0
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2210785
    const v1, -0x79f749b8

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2210786
    :catch_0
    move-exception v0

    .line 2210787
    :try_start_1
    iget-object v2, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->b:LX/2Vw;

    const-wide/32 v4, 0x36ee80

    invoke-virtual {v2, v1, v4, v5}, LX/2Vx;->a(LX/2WG;J)V

    .line 2210788
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210789
    :catchall_0
    move-exception v0

    const v1, -0x7e59d8ca

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2210790
    :catch_1
    move-exception v0

    .line 2210791
    iget-object v2, p0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->b:LX/2Vw;

    const-wide/16 v4, 0x4e20

    invoke-virtual {v2, v1, v4, v5}, LX/2Vx;->a(LX/2WG;J)V

    .line 2210792
    throw v0
.end method
