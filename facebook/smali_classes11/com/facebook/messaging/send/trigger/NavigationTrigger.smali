.class public Lcom/facebook/messaging/send/trigger/NavigationTrigger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/send/trigger/NavigationTrigger;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2224914
    new-instance v0, LX/FKI;

    invoke-direct {v0}, LX/FKI;-><init>()V

    sput-object v0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2224943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224944
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a:Z

    .line 2224945
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    .line 2224946
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    .line 2224947
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->d:Ljava/lang/String;

    .line 2224948
    return-void

    .line 2224949
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2224936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224937
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224938
    iput-object p1, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    .line 2224939
    iput-object p2, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    .line 2224940
    iput-object p3, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->d:Ljava/lang/String;

    .line 2224941
    iput-boolean p4, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a:Z

    .line 2224942
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/messaging/send/trigger/NavigationTrigger;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2224935
    new-instance v0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v2, v2, v1}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2224928
    iget-boolean v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a:Z

    if-eqz v0, :cond_0

    .line 2224929
    iget-object v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    .line 2224930
    :goto_0
    return-object v0

    .line 2224931
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "2"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2224932
    iget-object v1, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2224933
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2224934
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2224927
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2224923
    instance-of v1, p1, Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    if-nez v1, :cond_1

    .line 2224924
    :cond_0
    :goto_0
    return v0

    .line 2224925
    :cond_1
    check-cast p1, Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    .line 2224926
    iget-boolean v1, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a:Z

    iget-boolean v2, p1, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2224922
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2224921
    invoke-virtual {p0}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2224915
    iget-boolean v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2224916
    iget-object v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2224917
    iget-object v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2224918
    iget-object v0, p0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2224919
    return-void

    .line 2224920
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
