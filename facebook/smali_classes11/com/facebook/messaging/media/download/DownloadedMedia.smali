.class public Lcom/facebook/messaging/media/download/DownloadedMedia;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/download/DownloadedMedia;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/FG4;

.field public final b:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2218197
    new-instance v0, LX/FG3;

    invoke-direct {v0}, LX/FG3;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/download/DownloadedMedia;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FG4;Landroid/net/Uri;)V
    .locals 1
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2218198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218199
    iput-object p1, p0, Lcom/facebook/messaging/media/download/DownloadedMedia;->a:LX/FG4;

    .line 2218200
    iput-object p2, p0, Lcom/facebook/messaging/media/download/DownloadedMedia;->b:Landroid/net/Uri;

    .line 2218201
    sget-object v0, LX/FG4;->FAILURE:LX/FG4;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/FG4;->NO_PERMISSION:LX/FG4;

    if-eq p1, v0, :cond_0

    .line 2218202
    const-string v0, "Must provide a URI for successful download"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218203
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2218204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218205
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FG4;

    iput-object v0, p0, Lcom/facebook/messaging/media/download/DownloadedMedia;->a:LX/FG4;

    .line 2218206
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/media/download/DownloadedMedia;->b:Landroid/net/Uri;

    .line 2218207
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2218208
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2218209
    iget-object v0, p0, Lcom/facebook/messaging/media/download/DownloadedMedia;->a:LX/FG4;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2218210
    iget-object v0, p0, Lcom/facebook/messaging/media/download/DownloadedMedia;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2218211
    return-void
.end method
