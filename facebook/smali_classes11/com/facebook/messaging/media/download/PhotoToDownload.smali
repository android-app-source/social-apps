.class public Lcom/facebook/messaging/media/download/PhotoToDownload;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/download/PhotoToDownload;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2218605
    new-instance v0, LX/FGD;

    invoke-direct {v0}, LX/FGD;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/download/PhotoToDownload;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2218606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218607
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    .line 2218608
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->b:Ljava/lang/String;

    .line 2218609
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2218610
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2218611
    invoke-direct {p0, p1, v0, v0}, Lcom/facebook/messaging/media/download/PhotoToDownload;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2218612
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2218613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218614
    if-nez p1, :cond_0

    .line 2218615
    const-string v0, "PhotoToDownload"

    const-string v1, "FBID is null."

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218616
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    .line 2218617
    iput-object p2, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->b:Ljava/lang/String;

    .line 2218618
    iput-object p3, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2218619
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2218620
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2218621
    iget-object v0, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2218622
    iget-object v0, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2218623
    iget-object v0, p0, Lcom/facebook/messaging/media/download/PhotoToDownload;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2218624
    return-void
.end method
