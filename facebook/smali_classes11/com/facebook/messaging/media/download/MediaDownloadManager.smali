.class public Lcom/facebook/messaging/media/download/MediaDownloadManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

.field private static volatile j:Lcom/facebook/messaging/media/download/MediaDownloadManager;


# instance fields
.field public final c:LX/2OS;

.field private final d:LX/0aG;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final i:LX/1Ml;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2218216
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->a:[Ljava/lang/String;

    .line 2218217
    new-instance v0, LX/2rN;

    invoke-direct {v0}, LX/2rN;-><init>()V

    sget-object v1, LX/0jt;->ONLY_SHOW_FOR_SETTINGS:LX/0jt;

    .line 2218218
    iput-object v1, v0, LX/2rN;->c:LX/0jt;

    .line 2218219
    move-object v0, v0

    .line 2218220
    iput-boolean v3, v0, LX/2rN;->d:Z

    .line 2218221
    move-object v0, v0

    .line 2218222
    invoke-virtual {v0}, LX/2rN;->e()Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->b:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/2OS;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/1Ml;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsPhotosAutoDownloadAvailable;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0aG;",
            "LX/2OS;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/1Ml;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2218223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218224
    iput-object p1, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->e:LX/0Or;

    .line 2218225
    iput-object p2, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2218226
    iput-object p3, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->d:LX/0aG;

    .line 2218227
    iput-object p4, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->c:LX/2OS;

    .line 2218228
    iput-object p6, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->f:Ljava/util/concurrent/ExecutorService;

    .line 2218229
    iput-object p5, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->g:Ljava/util/concurrent/ExecutorService;

    .line 2218230
    iput-object p7, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->i:LX/1Ml;

    .line 2218231
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/media/download/MediaDownloadManager;
    .locals 11

    .prologue
    .line 2218232
    sget-object v0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->j:Lcom/facebook/messaging/media/download/MediaDownloadManager;

    if-nez v0, :cond_1

    .line 2218233
    const-class v1, Lcom/facebook/messaging/media/download/MediaDownloadManager;

    monitor-enter v1

    .line 2218234
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->j:Lcom/facebook/messaging/media/download/MediaDownloadManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2218235
    if-eqz v2, :cond_0

    .line 2218236
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2218237
    new-instance v3, Lcom/facebook/messaging/media/download/MediaDownloadManager;

    const/16 v4, 0x14ed

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v6

    check-cast v6, LX/0aG;

    invoke-static {v0}, LX/2OS;->a(LX/0QB;)LX/2OS;

    move-result-object v7

    check-cast v7, LX/2OS;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v10

    check-cast v10, LX/1Ml;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/messaging/media/download/MediaDownloadManager;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/2OS;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/1Ml;)V

    .line 2218238
    move-object v0, v3

    .line 2218239
    sput-object v0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->j:Lcom/facebook/messaging/media/download/MediaDownloadManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2218240
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2218241
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2218242
    :cond_1
    sget-object v0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->j:Lcom/facebook/messaging/media/download/MediaDownloadManager;

    return-object v0

    .line 2218243
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2218244
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2218245
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/FG8;->c:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2218246
    :goto_0
    return v0

    .line 2218247
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2218248
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2218249
    const/4 v5, 0x0

    .line 2218250
    iget-object v7, v0, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    if-eqz v7, :cond_2

    iget-object v7, v0, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v5, 0x1

    :cond_2
    move v5, v5

    .line 2218251
    if-nez v5, :cond_3

    .line 2218252
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2218253
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2218254
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2218255
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 2218256
    goto :goto_0

    .line 2218257
    :cond_5
    iget-object v2, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->i:LX/1Ml;

    const-string v3, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v2, v3}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2218258
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/FG8;->c:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    move v0, v1

    .line 2218259
    goto :goto_0

    .line 2218260
    :cond_6
    new-instance v1, Lcom/facebook/messaging/media/download/DownloadPhotosParams;

    .line 2218261
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2218262
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_8

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 2218263
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->c:LX/2OS;

    invoke-virtual {v7, v2}, LX/2OS;->e(Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v2

    .line 2218264
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 2218265
    new-instance v8, Lcom/facebook/messaging/media/download/PhotoToDownload;

    iget-object p1, v2, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-direct {v8, p1}, Lcom/facebook/messaging/media/download/PhotoToDownload;-><init>(Ljava/lang/String;)V

    move-object v2, v8

    .line 2218266
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2218267
    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2218268
    :cond_8
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2218269
    sget-object v2, LX/FGC;->GALLERY:LX/FGC;

    invoke-direct {v1, v0, v2, v6}, Lcom/facebook/messaging/media/download/DownloadPhotosParams;-><init>(Ljava/util/List;LX/FGC;Z)V

    .line 2218270
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2218271
    const-string v0, "downloadPhotosParams"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2218272
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadManager;->d:LX/0aG;

    const-string v1, "photo_download"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "photo_auto_save"

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x57423862

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move v0, v6

    .line 2218273
    goto/16 :goto_0
.end method
