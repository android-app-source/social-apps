.class public Lcom/facebook/messaging/media/download/DownloadPhotosParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/download/DownloadPhotosParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/media/download/PhotoToDownload;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/FGC;

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2218170
    new-instance v0, LX/FG2;

    invoke-direct {v0}, LX/FG2;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2218171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218172
    const-class v0, Lcom/facebook/messaging/media/download/PhotoToDownload;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->a:LX/0Px;

    .line 2218173
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FGC;

    iput-object v0, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->b:LX/FGC;

    .line 2218174
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->c:Z

    .line 2218175
    return-void
.end method

.method public constructor <init>(Ljava/util/List;LX/FGC;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/media/download/PhotoToDownload;",
            ">;",
            "LX/FGC;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2218176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218177
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->a:LX/0Px;

    .line 2218178
    iput-object p2, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->b:LX/FGC;

    .line 2218179
    iput-boolean p3, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->c:Z

    .line 2218180
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2218181
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2218182
    iget-object v0, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2218183
    iget-object v0, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->b:LX/FGC;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2218184
    iget-boolean v0, p0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2218185
    return-void
.end method
