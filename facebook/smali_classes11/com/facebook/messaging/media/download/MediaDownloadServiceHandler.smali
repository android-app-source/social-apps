.class public Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:LX/0Zb;

.field public final e:LX/03V;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/43N;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final h:LX/1Er;

.field public final i:Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

.field public final j:LX/2Ms;

.field public final k:LX/0Uh;

.field public final l:LX/3el;

.field public final m:LX/2Nw;

.field private final n:Ljava/io/File;

.field private final o:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 2218571
    const-string v0, "\\.|:"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a:Ljava/util/regex/Pattern;

    .line 2218572
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 2218573
    sget-object v0, LX/1ld;->j:LX/1c9;

    if-nez v0, :cond_0

    .line 2218574
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2218575
    sget-object v1, LX/1ld;->a:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218576
    sget-object v1, LX/1ld;->b:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218577
    sget-object v1, LX/1ld;->c:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218578
    sget-object v1, LX/1ld;->d:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218579
    sget-object v1, LX/1ld;->e:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218580
    sget-object v1, LX/1ld;->f:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218581
    sget-object v1, LX/1ld;->g:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218582
    sget-object v1, LX/1ld;->h:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218583
    sget-object v1, LX/1ld;->i:LX/1lW;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218584
    invoke-static {v0}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    sput-object v0, LX/1ld;->j:LX/1c9;

    .line 2218585
    :cond_0
    sget-object v0, LX/1ld;->j:LX/1c9;

    move-object v0, v0

    .line 2218586
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lW;

    .line 2218587
    :try_start_0
    invoke-static {v0}, LX/1lW;->a(LX/1lW;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2218588
    :catch_0
    move-exception v1

    .line 2218589
    const-string v4, "MediaDownloadServiceHandler"

    const-string v5, "Unknown image format %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 2218590
    iget-object v8, v0, LX/1lW;->c:Ljava/lang/String;

    move-object v0, v8

    .line 2218591
    aput-object v0, v6, v7

    invoke-static {v4, v1, v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2218592
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 2218593
    sput-object v0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Zb;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/03V;LX/0Ot;Ljava/io/File;Ljava/io/File;LX/0Uh;LX/1Er;Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;LX/2Ms;LX/3el;LX/2Nw;)V
    .locals 0
    .param p6    # Ljava/io/File;
        .annotation runtime Lcom/facebook/messaging/annotations/PhotoDirectory;
        .end annotation
    .end param
    .param p7    # Ljava/io/File;
        .annotation runtime Lcom/facebook/messaging/annotations/VideoDirectory;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Zb;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/43N;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1Er;",
            "Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;",
            "LX/2Ms;",
            "LX/3el;",
            "LX/2Nw;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2218556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218557
    iput-object p1, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    .line 2218558
    iput-object p2, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->d:LX/0Zb;

    .line 2218559
    iput-object p4, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    .line 2218560
    iput-object p3, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->g:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2218561
    iput-object p5, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->f:LX/0Ot;

    .line 2218562
    iput-object p6, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->n:Ljava/io/File;

    .line 2218563
    iput-object p7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->o:Ljava/io/File;

    .line 2218564
    iput-object p9, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->h:LX/1Er;

    .line 2218565
    iput-object p10, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->i:Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    .line 2218566
    iput-object p11, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->j:LX/2Ms;

    .line 2218567
    iput-object p8, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->k:LX/0Uh;

    .line 2218568
    iput-object p12, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->l:LX/3el;

    .line 2218569
    iput-object p13, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->m:LX/2Nw;

    .line 2218570
    return-void
.end method

.method public static a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;LX/FGC;)Ljava/io/File;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2218551
    sget-object v1, LX/FG9;->a:[I

    invoke-virtual {p1}, LX/FGC;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2218552
    :cond_0
    :goto_0
    return-object v0

    .line 2218553
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->n:Ljava/io/File;

    .line 2218554
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move-object v0, v1

    .line 2218555
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 2218548
    invoke-static {p0}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->b(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;)Z

    move-result v0

    const-string v1, "Failed to create directory to save videos."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2218549
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "received_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2218550
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->o:Ljava/io/File;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;LX/FGC;)Ljava/io/File;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2218540
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "received_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2218541
    sget-object v2, LX/FG9;->a:[I

    invoke-virtual {p3}, LX/FGC;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2218542
    :goto_0
    return-object v0

    .line 2218543
    :pswitch_0
    invoke-static {p0, p3}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;LX/FGC;)Ljava/io/File;

    move-result-object v2

    .line 2218544
    if-nez v2, :cond_0

    .line 2218545
    iget-object v1, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v2, "MediaDownloadServiceHandler"

    const-string v3, "Failed to create directory to save photos."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2218546
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 2218547
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->h:LX/1Er;

    const-string v0, "_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->k:LX/0Uh;

    const/16 v4, 0x268

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_1
    invoke-virtual {v2, v1, v3, v0}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/io/File;)Ljava/io/File;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2218523
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2218524
    :try_start_0
    invoke-static {v0}, LX/1la;->a(Ljava/io/InputStream;)LX/1lW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2218525
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 2218526
    const/4 v0, 0x0

    .line 2218527
    :try_start_1
    iget-object v2, v1, LX/1lW;->b:Ljava/lang/String;

    move-object v0, v2
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2218528
    :goto_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1t3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2218529
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2218530
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2218531
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2218532
    invoke-virtual {p0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p0, v1

    .line 2218533
    :cond_0
    :goto_1
    return-object p0

    .line 2218534
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    throw v1

    .line 2218535
    :catch_0
    move-exception v2

    .line 2218536
    const-string v3, "MediaDownloadServiceHandler"

    const-string v4, "Unknown image format %s"

    new-array v5, v7, [Ljava/lang/Object;

    .line 2218537
    iget-object v8, v1, LX/1lW;->c:Ljava/lang/String;

    move-object v1, v8

    .line 2218538
    aput-object v1, v5, v6

    invoke-static {v3, v2, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2218539
    :cond_1
    const-string v0, "MediaDownloadServiceHandler"

    const-string v2, "Could not rename file %s to %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-static {v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Landroid/net/Uri;Ljava/io/File;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2218508
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 2218509
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2218510
    const/16 v0, 0x2000

    :try_start_2
    new-array v0, v0, [B

    .line 2218511
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    .line 2218512
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2218513
    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_1
    if-eqz v2, :cond_0

    .line 2218514
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 2218515
    :cond_0
    if-eqz v1, :cond_1

    .line 2218516
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2218517
    :cond_1
    :goto_2
    throw v0

    .line 2218518
    :cond_2
    if-eqz v3, :cond_3

    .line 2218519
    invoke-static {v3}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 2218520
    :cond_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 2218521
    :goto_3
    return-void

    :catch_0
    goto :goto_3

    :catch_1
    goto :goto_2

    .line 2218522
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method public static a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Landroid/net/Uri;Ljava/io/File;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2218482
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->k:LX/0Uh;

    const/16 v1, 0x13d

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2218483
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->l:LX/3el;

    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/3el;->a(LX/1bX;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    move-result-object v0

    .line 2218484
    invoke-static {v0}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    .line 2218485
    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    move-object v1, v0

    .line 2218486
    if-nez v1, :cond_0

    .line 2218487
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Closeable reference is null. uri="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2218488
    :cond_0
    new-instance v2, LX/1lZ;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-direct {v2, v0}, LX/1lZ;-><init>(LX/1FK;)V

    .line 2218489
    :try_start_0
    invoke-static {v2}, LX/1la;->a(Ljava/io/InputStream;)LX/1lW;

    move-result-object v0

    invoke-static {v0}, LX/1ld;->a(LX/1lW;)Z

    move-result v0

    invoke-static {p0, v2, p2, v0}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a$redex0(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/io/InputStream;Ljava/io/File;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2218490
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 2218491
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 2218492
    :goto_0
    return-void

    .line 2218493
    :catchall_0
    move-exception v0

    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 2218494
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 2218495
    :cond_1
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    new-instance v1, Ljava/net/URI;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 2218496
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    const-string v2, "MessengerPhotoDownload"

    .line 2218497
    iput-object v2, v1, LX/15E;->c:Ljava/lang/String;

    .line 2218498
    move-object v1, v1

    .line 2218499
    iput-object p3, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2218500
    move-object v1, v1

    .line 2218501
    iput-object v0, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2218502
    move-object v0, v1

    .line 2218503
    new-instance v1, LX/FGA;

    invoke-direct {v1, p0, p2}, LX/FGA;-><init>(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/io/File;)V

    .line 2218504
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2218505
    move-object v0, v0

    .line 2218506
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 2218507
    iget-object v1, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->g:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2218473
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->d:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2218474
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2218475
    const-string v1, "media_fbid"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2218476
    if-eqz p3, :cond_0

    .line 2218477
    const-string v1, "is_auto_download"

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2218478
    :cond_0
    if-eqz p4, :cond_1

    .line 2218479
    const-string v1, "download_media_caller_context"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2218480
    :cond_1
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2218481
    :cond_2
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/io/InputStream;Ljava/io/File;Z)V
    .locals 3

    .prologue
    .line 2218465
    if-eqz p3, :cond_0

    .line 2218466
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2218467
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/43N;

    const/16 v2, 0x5a

    invoke-virtual {v0, p1, v1, v2}, LX/43N;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2218468
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2218469
    :goto_0
    return-void

    .line 2218470
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0

    .line 2218471
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [LX/3AS;

    invoke-static {p2, v0}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v0

    .line 2218472
    invoke-virtual {v0, p1}, LX/3AU;->a(Ljava/io/InputStream;)J

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;
    .locals 14

    .prologue
    .line 2218300
    new-instance v0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x1808

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 2218301
    invoke-static {}, LX/FGB;->a()Ljava/io/File;

    move-result-object v6

    move-object v6, v6

    .line 2218302
    check-cast v6, Ljava/io/File;

    .line 2218303
    invoke-static {}, LX/FGF;->a()Ljava/io/File;

    move-result-object v7

    move-object v7, v7

    .line 2218304
    check-cast v7, Ljava/io/File;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v9

    check-cast v9, LX/1Er;

    invoke-static {p0}, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->b(LX/0QB;)Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    move-result-object v10

    check-cast v10, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    invoke-static {p0}, LX/2Ms;->b(LX/0QB;)LX/2Ms;

    move-result-object v11

    check-cast v11, LX/2Ms;

    invoke-static {p0}, LX/3el;->a(LX/0QB;)LX/3el;

    move-result-object v12

    check-cast v12, LX/3el;

    invoke-static {p0}, LX/2Nw;->b(LX/0QB;)LX/2Nw;

    move-result-object v13

    check-cast v13, LX/2Nw;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;-><init>(Landroid/content/Context;LX/0Zb;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/03V;LX/0Ot;Ljava/io/File;Ljava/io/File;LX/0Uh;LX/1Er;Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;LX/2Ms;LX/3el;LX/2Nw;)V

    .line 2218305
    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;)Z
    .locals 1

    .prologue
    .line 2218464
    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->o:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->o:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2218440
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2218441
    const-string v1, "saveMmsPhotoParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/download/SaveMmsPhotoParams;

    .line 2218442
    iget-object v0, v0, Lcom/facebook/messaging/media/download/SaveMmsPhotoParams;->b:Landroid/net/Uri;

    const/4 v6, 0x0

    .line 2218443
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    .line 2218444
    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x100

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".tmp"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 2218445
    sget-object v7, LX/FGC;->GALLERY:LX/FGC;

    invoke-static {p0, v7}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;LX/FGC;)Ljava/io/File;

    move-result-object v9

    .line 2218446
    if-nez v9, :cond_2

    .line 2218447
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v8, "MediaDownloadServiceHandler"

    const-string v9, "Failed to create directory to save photos."

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218448
    const/4 v7, 0x0

    .line 2218449
    :goto_0
    move-object v2, v7

    .line 2218450
    if-nez v2, :cond_0

    .line 2218451
    iget-object v2, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v3, "MediaDownloadServiceHandler"

    const-string v4, "Could not create photo file for saving"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218452
    new-instance v2, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v3, LX/FG4;->FAILURE:LX/FG4;

    invoke-direct {v2, v3, v6}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    .line 2218453
    :goto_1
    move-object v0, v2

    .line 2218454
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2218455
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2218456
    new-instance v2, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v3, LX/FG4;->PRE_EXISTING:LX/FG4;

    invoke-direct {v2, v3, v6}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto :goto_1

    .line 2218457
    :cond_1
    :try_start_0
    invoke-static {p0, v0, v2}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Landroid/net/Uri;Ljava/io/File;)V

    .line 2218458
    invoke-static {v2}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 2218459
    iget-object v2, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v4, v5, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2218460
    new-instance v2, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v4, LX/FG4;->DOWNLOADED:LX/FG4;

    invoke-direct {v2, v4, v3}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2218461
    :catch_0
    move-exception v2

    .line 2218462
    const-string v3, "MediaDownloadServiceHandler"

    const-string v4, "failed to save mms photo"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2218463
    new-instance v2, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v3, LX/FG4;->FAILURE:LX/FG4;

    invoke-direct {v2, v3, v6}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto :goto_1

    :cond_2
    new-instance v7, Ljava/io/File;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v9, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 2218306
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2218307
    const-string v1, "photo_download"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2218308
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2218309
    const-string v1, "downloadPhotosParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;

    .line 2218310
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2218311
    iget-object v4, v0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/download/PhotoToDownload;

    .line 2218312
    iget-object v6, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v6, v6

    .line 2218313
    const/4 v13, 0x0

    .line 2218314
    iget-object v7, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    if-nez v7, :cond_5

    .line 2218315
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v8, "MediaDownloadServiceHandler"

    const-string v9, "Called with no FBID."

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218316
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v8, LX/FG4;->FAILURE:LX/FG4;

    invoke-direct {v7, v8, v13}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    .line 2218317
    :goto_1
    move-object v1, v7

    .line 2218318
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2218319
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2218320
    :cond_0
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2218321
    :goto_2
    return-object v0

    .line 2218322
    :cond_1
    const-string v1, "video_download"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2218323
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2218324
    const-string v1, "video_attachment_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;

    .line 2218325
    invoke-static {p0}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->b(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 2218326
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2218327
    :goto_3
    move-object v0, v0

    .line 2218328
    goto :goto_2

    .line 2218329
    :cond_2
    const-string v1, "local_video_download"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2218330
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2218331
    const-string v1, "videoUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2218332
    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 2218333
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2218334
    new-instance v1, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v3, LX/FG4;->PRE_EXISTING:LX/FG4;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    .line 2218335
    :goto_4
    move-object v0, v1

    .line 2218336
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2218337
    goto :goto_2

    .line 2218338
    :cond_3
    const-string v1, "save_mms_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2218339
    invoke-direct {p0, p1}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_2

    .line 2218340
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2218341
    :cond_5
    iget-boolean v9, v0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->c:Z

    .line 2218342
    sget-object v7, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a:Ljava/util/regex/Pattern;

    iget-object v8, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2218343
    iget-object v8, v0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->b:LX/FGC;

    .line 2218344
    sget-object v10, LX/FGC;->TEMP:LX/FGC;

    if-ne v8, v10, :cond_6

    .line 2218345
    sget-object v8, LX/FGC;->GALLERY:LX/FGC;

    .line 2218346
    :cond_6
    sget-object v10, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->b:LX/0Rf;

    invoke-virtual {v10}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_e

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 2218347
    invoke-static {p0, v7, v10, v8}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;LX/FGC;)Ljava/io/File;

    move-result-object v10

    .line 2218348
    if-eqz v10, :cond_7

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2218349
    :goto_5
    move-object v8, v10

    .line 2218350
    if-eqz v8, :cond_8

    if-nez v9, :cond_8

    .line 2218351
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v9, LX/FG4;->PRE_EXISTING:LX/FG4;

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v7, v9, v8}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 2218352
    :cond_8
    const-string v8, "tmp"

    iget-object v10, v0, Lcom/facebook/messaging/media/download/DownloadPhotosParams;->b:LX/FGC;

    invoke-static {p0, v7, v8, v10}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;LX/FGC;)Ljava/io/File;

    move-result-object v7

    .line 2218353
    if-nez v7, :cond_9

    .line 2218354
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v8, "MediaDownloadServiceHandler"

    const-string v9, "Could not create photo file for saving"

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218355
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v8, LX/FG4;->FAILURE:LX/FG4;

    invoke-direct {v7, v8, v13}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 2218356
    :cond_9
    :try_start_0
    const-string v8, "messenger_save_photo_start"

    iget-object v10, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v12

    invoke-static {p0, v8, v10, v11, v12}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2218357
    iget-object v8, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->b:Ljava/lang/String;

    if-eqz v8, :cond_b

    .line 2218358
    iget-object v8, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->m:LX/2Nw;

    iget-object v10, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    iget-object v11, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v12, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-static {v10, v11, v12}, LX/6bn;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v10

    iget-object v11, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->b:Ljava/lang/String;

    invoke-virtual {v8, v10, v11}, LX/2Nw;->b(Landroid/net/Uri;Ljava/lang/String;)[B

    move-result-object v8

    .line 2218359
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2218360
    invoke-virtual {v10, v8}, Ljava/io/FileOutputStream;->write([B)V

    .line 2218361
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 2218362
    :goto_6
    invoke-static {v7}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    .line 2218363
    iget-object v8, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->k:LX/0Uh;

    const/16 v10, 0x268

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, LX/0Uh;->a(IZ)Z

    move-result v10

    .line 2218364
    if-eqz v10, :cond_d

    .line 2218365
    iget-object v8, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    invoke-static {v8, v7}, Lcom/facebook/content/FbFileProvider;->a(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    move-object v8, v7

    .line 2218366
    :goto_7
    new-instance v7, Landroid/content/Intent;

    const-string v11, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2218367
    invoke-virtual {v7, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2218368
    if-eqz v10, :cond_a

    .line 2218369
    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2218370
    :cond_a
    iget-object v10, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    invoke-virtual {v10, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2218371
    const-string v7, "messenger_save_photo_success"

    iget-object v10, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v12

    invoke-static {p0, v7, v10, v11, v12}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2218372
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v10, LX/FG4;->DOWNLOADED:LX/FG4;

    invoke-direct {v7, v10, v8}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_1

    .line 2218373
    :catch_0
    move-exception v7

    .line 2218374
    const-string v8, "MediaDownloadServiceHandler"

    const-string v10, "Unable to download to file "

    invoke-static {v8, v10, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2218375
    const-string v7, "messenger_save_photo_fail"

    iget-object v8, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v7, v8, v9, v10}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2218376
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v8, LX/FG4;->FAILURE:LX/FG4;

    invoke-direct {v7, v8, v13}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 2218377
    :cond_b
    :try_start_1
    iget-object v8, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->i:Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    iget-object v10, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    iget-object v11, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->j:LX/2Ms;

    .line 2218378
    invoke-virtual {v11}, LX/2Ms;->b()Z

    move-result v12

    if-eqz v12, :cond_f

    .line 2218379
    const/16 v12, 0x2710

    .line 2218380
    :goto_8
    move v11, v12

    .line 2218381
    invoke-virtual {v8, v10, v11, v6}, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->a(Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)Landroid/net/Uri;

    move-result-object v8

    .line 2218382
    if-nez v8, :cond_c

    .line 2218383
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v8, "MediaDownloadServiceHandler"

    const-string v10, "Could not retrieve URL of image"

    invoke-virtual {v7, v8, v10}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218384
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v8, LX/FG4;->FAILURE:LX/FG4;

    const/4 v10, 0x0

    invoke-direct {v7, v8, v10}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_1

    .line 2218385
    :catch_1
    move-exception v7

    .line 2218386
    const-string v8, "MediaDownloadServiceHandler"

    const-string v10, "Unable to download to file "

    invoke-static {v8, v10, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2218387
    const-string v7, "messenger_save_photo_fail"

    iget-object v8, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v7, v8, v9, v10}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2218388
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v8, LX/FG4;->FAILURE:LX/FG4;

    invoke-direct {v7, v8, v13}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 2218389
    :cond_c
    :try_start_2
    invoke-static {p0, v8, v7, v6}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Landroid/net/Uri;Ljava/io/File;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_6

    .line 2218390
    :catch_2
    move-exception v7

    .line 2218391
    const-string v8, "MediaDownloadServiceHandler"

    const-string v10, "Unable to download to file "

    invoke-static {v8, v10, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2218392
    const-string v7, "messenger_save_photo_fail"

    iget-object v8, v1, Lcom/facebook/messaging/media/download/PhotoToDownload;->a:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v7, v8, v9, v10}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2218393
    new-instance v7, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v8, LX/FG4;->FAILURE:LX/FG4;

    invoke-direct {v7, v8, v13}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 2218394
    :cond_d
    :try_start_3
    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v7

    move-object v8, v7

    goto/16 :goto_7

    :cond_e
    const/4 v10, 0x0

    goto/16 :goto_5

    :cond_f
    :try_start_4
    const/16 v12, 0x800

    goto :goto_8
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 2218395
    :cond_10
    iget-object v1, v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    .line 2218396
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/engine/VideoDataSource;

    .line 2218397
    iget-object v1, v1, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    .line 2218398
    invoke-static {v1}, LX/1H1;->f(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 2218399
    iget-object v3, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v4, "MediaDownloadServiceHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Called with non-http URI: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 2218400
    :cond_12
    iget-object v3, v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 2218401
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 2218402
    new-instance v0, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v1, LX/FG4;->PRE_EXISTING:LX/FG4;

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_3

    .line 2218403
    :cond_13
    iget-object v4, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, v4

    .line 2218404
    const/4 v5, 0x0

    const/4 v11, 0x0

    .line 2218405
    const-string v6, "messenger_save_video_start"

    iget-object v7, v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v6, v7, v11, v8}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2218406
    :try_start_5
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    new-instance v7, Ljava/net/URI;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 2218407
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v7

    const-string v8, "MessengerVideoDownload"

    .line 2218408
    iput-object v8, v7, LX/15E;->c:Ljava/lang/String;

    .line 2218409
    move-object v7, v7

    .line 2218410
    iput-object v4, v7, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2218411
    move-object v7, v7

    .line 2218412
    iput-object v6, v7, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2218413
    move-object v6, v7

    .line 2218414
    new-instance v7, LX/FGA;

    invoke-direct {v7, p0, v3}, LX/FGA;-><init>(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/io/File;)V

    .line 2218415
    iput-object v7, v6, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2218416
    move-object v6, v6

    .line 2218417
    invoke-virtual {v6}, LX/15E;->a()LX/15D;

    move-result-object v6

    .line 2218418
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->g:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v7, v6}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    .line 2218419
    const-string v6, "messenger_save_video_success"

    iget-object v7, v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v4}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v6, v7, v8, v9}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_5 .. :try_end_5} :catch_4

    .line 2218420
    const/4 v5, 0x1

    .line 2218421
    :goto_a
    move v1, v5

    .line 2218422
    if-eqz v1, :cond_11

    .line 2218423
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2218424
    iget-object v1, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v2, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2218425
    new-instance v0, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v1, LX/FG4;->DOWNLOADED:LX/FG4;

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_3

    .line 2218426
    :cond_14
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_3

    .line 2218427
    :catch_3
    move-exception v6

    .line 2218428
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v8, "MediaDownloadServiceHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unable to write to file "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v8, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218429
    const-string v6, "messenger_save_video_fail"

    iget-object v7, v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v6, v7, v11, v8}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_a

    .line 2218430
    :catch_4
    move-exception v6

    .line 2218431
    iget-object v7, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->e:LX/03V;

    const-string v8, "MediaDownloadServiceHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unable to write to file "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v8, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218432
    const-string v6, "messenger_save_video_fail"

    iget-object v7, v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v6, v7, v11, v8}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_a

    .line 2218433
    :cond_15
    :try_start_6
    invoke-static {p0, v0, v2}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->a(Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;Landroid/net/Uri;Ljava/io/File;)V

    .line 2218434
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 2218435
    iget-object v1, p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->c:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string p1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v3, p1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2218436
    new-instance v1, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v3, LX/FG4;->DOWNLOADED:LX/FG4;

    invoke-direct {v1, v3, v2}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_4

    .line 2218437
    :catch_5
    move-exception v1

    .line 2218438
    const-string v2, "MediaDownloadServiceHandler"

    const-string v3, "failed to save local video"

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2218439
    new-instance v1, Lcom/facebook/messaging/media/download/DownloadedMedia;

    sget-object v2, LX/FG4;->FAILURE:LX/FG4;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/facebook/messaging/media/download/DownloadedMedia;-><init>(LX/FG4;Landroid/net/Uri;)V

    goto/16 :goto_4
.end method
