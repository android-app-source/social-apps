.class public final Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/attachments/VideoAttachmentData;

.field public final synthetic b:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/messaging/attachments/VideoAttachmentData;)V
    .locals 0

    .prologue
    .line 2218656
    iput-object p1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->b:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iput-object p2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->a:Lcom/facebook/messaging/attachments/VideoAttachmentData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2218657
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->a:Lcom/facebook/messaging/attachments/VideoAttachmentData;

    invoke-virtual {v0}, Lcom/facebook/messaging/attachments/VideoAttachmentData;->a()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->a:Lcom/facebook/messaging/attachments/VideoAttachmentData;

    invoke-virtual {v0}, Lcom/facebook/messaging/attachments/VideoAttachmentData;->a()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-static {v0}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2218658
    :cond_0
    :goto_0
    return-void

    .line 2218659
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->a:Lcom/facebook/messaging/attachments/VideoAttachmentData;

    invoke-virtual {v0}, Lcom/facebook/messaging/attachments/VideoAttachmentData;->a()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    .line 2218660
    iget-object v1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->a:Lcom/facebook/messaging/attachments/VideoAttachmentData;

    iget-object v1, v1, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    .line 2218661
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->b:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v2, v2, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->m:LX/6eI;

    invoke-virtual {v2}, LX/6eI;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2218662
    new-instance v2, LX/36s;

    invoke-direct {v2, v0, v1}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 2218663
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;->b:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->k:LX/1Lg;

    sget-object v1, LX/1Li;->MISC:LX/1Li;

    invoke-virtual {v0, v1}, LX/1Lg;->a(LX/1Li;)LX/1M7;

    move-result-object v0

    .line 2218664
    const/4 v1, 0x1

    new-array v1, v1, [LX/36s;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, LX/1M7;->b([LX/36s;)V

    goto :goto_0
.end method
