.class public Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final o:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/2OS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/messaging/media/download/MediaDownloadManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/3dt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1Lg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/6gF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/6eI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/retry/MediaRetryQueue;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2218794
    const-class v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    sput-object v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->o:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2218790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218791
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2218792
    iput-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->p:LX/0Ot;

    .line 2218793
    return-void
.end method

.method private static a(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;LX/2OS;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0kb;Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/0Uh;LX/1HI;Lcom/facebook/messaging/media/download/MediaDownloadManager;LX/0ad;LX/3dt;LX/1Lg;LX/6gF;LX/6eI;Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;",
            "LX/2OS;",
            "LX/0aG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0kb;",
            "Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1HI;",
            "Lcom/facebook/messaging/media/download/MediaDownloadManager;",
            "LX/0ad;",
            "LX/3dt;",
            "LX/1Lg;",
            "LX/6gF;",
            "LX/6eI;",
            "Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/retry/MediaRetryQueue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2218684
    iput-object p1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a:LX/2OS;

    iput-object p2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->b:LX/0aG;

    iput-object p3, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->d:LX/0kb;

    iput-object p5, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->e:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    iput-object p6, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->f:LX/0Uh;

    iput-object p7, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->g:LX/1HI;

    iput-object p8, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->h:Lcom/facebook/messaging/media/download/MediaDownloadManager;

    iput-object p9, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    iput-object p10, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->j:LX/3dt;

    iput-object p11, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->k:LX/1Lg;

    iput-object p12, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->l:LX/6gF;

    iput-object p13, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->m:LX/6eI;

    iput-object p14, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->n:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    iput-object p15, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->p:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 5

    .prologue
    .line 2218766
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a:LX/2OS;

    invoke-virtual {v0, p1}, LX/2OS;->f(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/attachments/AudioAttachmentData;

    move-result-object v0

    .line 2218767
    if-nez v0, :cond_1

    .line 2218768
    :cond_0
    :goto_0
    return-void

    .line 2218769
    :cond_1
    iget-object v1, v0, Lcom/facebook/messaging/attachments/AudioAttachmentData;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2218770
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->m:LX/6eI;

    invoke-virtual {v2}, LX/6eI;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2218771
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->n:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    .line 2218772
    iget-object p0, v0, Lcom/facebook/messaging/attachments/AudioAttachmentData;->d:Ljava/lang/String;

    move-object v0, p0

    .line 2218773
    invoke-virtual {v2, v0, v1}, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 2218774
    :cond_2
    if-eqz v1, :cond_0

    .line 2218775
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->e:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    new-instance v2, LX/FCv;

    invoke-direct {v2, v1}, LX/FCv;-><init>(Landroid/net/Uri;)V

    .line 2218776
    invoke-virtual {v2}, LX/FCv;->b()LX/FCa;

    move-result-object v3

    .line 2218777
    iget-object v1, v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->e:LX/44Z;

    .line 2218778
    iget-object v4, v1, LX/44Z;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, v3}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/executors/KeyedExecutor$Task;

    .line 2218779
    if-eqz v4, :cond_5

    iget-object v4, v4, Lcom/facebook/common/executors/KeyedExecutor$Task;->c:LX/0Va;

    :goto_1
    move-object v1, v4

    .line 2218780
    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2218781
    :cond_3
    new-instance v1, LX/FCt;

    invoke-direct {v1, v0, v2}, LX/FCt;-><init>(Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/FCv;)V

    .line 2218782
    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "audio-message-"

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2218783
    iget-object p0, v3, LX/FCa;->a:Landroid/net/Uri;

    move-object p0, p0

    .line 2218784
    invoke-virtual {p0}, Landroid/net/Uri;->hashCode()I

    move-result p0

    rem-int/lit8 p0, p0, 0x3

    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result p0

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2218785
    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Create async task for downloading "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2218786
    iget-object p0, v2, LX/FCv;->a:Landroid/net/Uri;

    move-object p0, p0

    .line 2218787
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2218788
    iget-object p1, v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->e:LX/44Z;

    invoke-virtual {p1, v4, v3, v1, p0}, LX/44Z;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2218789
    :cond_4
    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2218764
    if-nez p0, :cond_1

    .line 2218765
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    sget-object v2, LX/6ek;->MONTAGE:LX/6ek;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ZLjava/lang/String;)V
    .locals 7

    .prologue
    .line 2218756
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2218757
    :cond_0
    :goto_0
    return-void

    .line 2218758
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/FGG;->a:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2218759
    const/4 v0, 0x1

    .line 2218760
    :goto_1
    move v0, v0

    .line 2218761
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    invoke-virtual {v0}, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2218762
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    invoke-static {p1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->a(LX/1bf;)V

    goto :goto_0

    .line 2218763
    :cond_2
    iget-object v6, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;-><init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;ZLcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)V

    const v1, 0x56f6b2e9

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->d:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;
    .locals 18

    .prologue
    .line 2218753
    new-instance v2, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    invoke-direct {v2}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;-><init>()V

    .line 2218754
    invoke-static/range {p0 .. p0}, LX/2OS;->a(LX/0QB;)LX/2OS;

    move-result-object v3

    check-cast v3, LX/2OS;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->a(LX/0QB;)Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v9

    check-cast v9, LX/1HI;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/media/download/MediaDownloadManager;->a(LX/0QB;)Lcom/facebook/messaging/media/download/MediaDownloadManager;

    move-result-object v10

    check-cast v10, Lcom/facebook/messaging/media/download/MediaDownloadManager;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v12

    check-cast v12, LX/3dt;

    invoke-static/range {p0 .. p0}, LX/1Lf;->a(LX/0QB;)LX/1Lg;

    move-result-object v13

    check-cast v13, LX/1Lg;

    invoke-static/range {p0 .. p0}, LX/6gF;->a(LX/0QB;)LX/6gF;

    move-result-object v14

    check-cast v14, LX/6gF;

    invoke-static/range {p0 .. p0}, LX/6eI;->a(LX/0QB;)LX/6eI;

    move-result-object v15

    check-cast v15, LX/6eI;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->a(LX/0QB;)Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    move-result-object v16

    check-cast v16, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    const/16 v17, 0x27f0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v2 .. v17}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;LX/2OS;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0kb;Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/0Uh;LX/1HI;Lcom/facebook/messaging/media/download/MediaDownloadManager;LX/0ad;LX/3dt;LX/1Lg;LX/6gF;LX/6eI;Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;LX/0Ot;)V

    .line 2218755
    return-object v2
.end method

.method public static d(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;ZZ)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2218743
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->j:LX/3dt;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2218744
    :cond_0
    :goto_0
    return-void

    .line 2218745
    :cond_1
    if-nez p3, :cond_2

    .line 2218746
    invoke-static {p1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/2N0;->g:S

    invoke-interface {v0, v1, v2, v7}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2218747
    :cond_2
    const-class v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    const-string v1, "media_prefetch"

    const-string v2, "sticker_prefetch"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    .line 2218748
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2218749
    const-string v0, "fetchStickersParams"

    new-instance v1, Lcom/facebook/stickers/service/FetchStickersParams;

    new-instance v3, Ljava/util/ArrayList;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sget-object v5, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v1, v3, v5}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2218750
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->b:LX/0aG;

    const-string v1, "fetch_stickers"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const v5, -0x4e532f41

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 2218751
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2218752
    new-instance v1, LX/FGH;

    invoke-direct {v1, p0, v4, p4, p2}, LX/FGH;-><init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/common/callercontext/CallerContext;ZLcom/facebook/messaging/model/messages/Message;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 2218685
    if-nez p2, :cond_1

    .line 2218686
    :cond_0
    :goto_0
    return-void

    .line 2218687
    :cond_1
    invoke-static {p1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 2218688
    iget-object v1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->l:LX/6gF;

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6gF;->b(Ljava/lang/String;)J

    move-result-wide v1

    .line 2218689
    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_7

    .line 2218690
    iget-object v1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->l:LX/6gF;

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    .line 2218691
    invoke-virtual {v1, v2}, LX/6gF;->b(Ljava/lang/String;)J

    move-result-wide v7

    .line 2218692
    const-wide/16 v9, 0x0

    cmp-long v6, v7, v9

    if-gtz v6, :cond_8

    .line 2218693
    :cond_2
    :goto_1
    move v1, v5

    .line 2218694
    if-nez v1, :cond_0

    .line 2218695
    :cond_3
    :goto_2
    const/4 v1, 0x0

    .line 2218696
    invoke-static {p2}, LX/2OS;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2218697
    :cond_4
    iget-object v1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a:LX/2OS;

    invoke-virtual {v1, p2}, LX/2OS;->g(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/attachments/VideoAttachmentData;

    move-result-object v1

    .line 2218698
    if-nez v1, :cond_11

    .line 2218699
    :cond_5
    :goto_3
    invoke-static {p0, p1, p2, v0, v0}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->d(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;ZZ)V

    .line 2218700
    invoke-static {p0, p2}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2218701
    const/4 v5, 0x0

    .line 2218702
    iget-object v1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a:LX/2OS;

    invoke-virtual {v1, p2}, LX/2OS;->g(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/attachments/VideoAttachmentData;

    move-result-object v1

    .line 2218703
    if-nez v1, :cond_13

    .line 2218704
    :cond_6
    :goto_4
    goto :goto_0

    .line 2218705
    :cond_7
    iget-object v1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->l:LX/6gF;

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    .line 2218706
    iget-object v5, v1, LX/6gF;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 2218707
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 2218708
    :goto_5
    goto :goto_2

    :cond_8
    iget-object v6, v1, LX/6gF;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v9

    cmp-long v6, v7, v9

    if-gez v6, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    .line 2218709
    :cond_9
    if-nez v0, :cond_a

    invoke-static {p1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/2N0;->f:S

    invoke-interface {v2, v3, v4, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2218710
    :cond_a
    invoke-static {p2}, LX/2OS;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    .line 2218711
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->f:LX/0Uh;

    const/16 v3, 0x169

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_b

    if-nez v4, :cond_b

    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->h:Lcom/facebook/messaging/media/download/MediaDownloadManager;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/media/download/MediaDownloadManager;->a(LX/0Px;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2218712
    :cond_b
    invoke-static {p2}, LX/2OS;->b(Lcom/facebook/messaging/model/messages/Message;)I

    move-result v5

    .line 2218713
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a:LX/2OS;

    invoke-virtual {v2, p2}, LX/2OS;->e(Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v6

    .line 2218714
    const-class v2, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    const-string v3, "media_prefetch"

    const-string v7, "image_prefetch"

    invoke-static {v2, v3, v7}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    .line 2218715
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v3, v1

    :goto_6
    if-ge v3, v8, :cond_4

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 2218716
    invoke-static {p1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2218717
    iget-object v2, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iget-object v2, v2, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    .line 2218718
    iget-object v9, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iget-object v9, v9, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    .line 2218719
    if-eqz v9, :cond_c

    .line 2218720
    iget-object v10, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-static {p0, v9, v7, v0, v10}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a$redex0(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ZLjava/lang/String;)V

    .line 2218721
    :cond_c
    :goto_7
    if-eqz v2, :cond_d

    .line 2218722
    iget-object v9, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-static {p0, v2, v7, v0, v9}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a$redex0(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ZLjava/lang/String;)V

    .line 2218723
    :cond_d
    if-eqz v4, :cond_e

    .line 2218724
    invoke-static {p1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v2, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    if-eqz v2, :cond_10

    .line 2218725
    iget-object v2, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iget-object v2, v2, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    .line 2218726
    :goto_8
    if-eqz v2, :cond_e

    .line 2218727
    iget-object v1, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-static {p0, v2, v7, v0, v1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a$redex0(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ZLjava/lang/String;)V

    .line 2218728
    :cond_e
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    .line 2218729
    :cond_f
    iget-object v2, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-static {v5, v2}, LX/6bv;->a(ILcom/facebook/messaging/attachments/ImageAttachmentUris;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_7

    .line 2218730
    :cond_10
    iget-object v2, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-static {v5, v2}, LX/6bv;->a(ILcom/facebook/messaging/attachments/ImageAttachmentUris;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_8

    .line 2218731
    :cond_11
    if-nez v0, :cond_12

    .line 2218732
    invoke-static {p1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v2

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/2N0;->i:S

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2218733
    :cond_12
    const-class v2, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    const-string v3, "media_prefetch"

    const-string v4, "video_cover_prefetch"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 2218734
    iget-object v3, v1, Lcom/facebook/messaging/attachments/VideoAttachmentData;->g:Landroid/net/Uri;

    iget-object v1, v1, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    invoke-static {p0, v3, v2, v0, v1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a$redex0(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ZLjava/lang/String;)V

    goto/16 :goto_3

    .line 2218735
    :cond_13
    if-nez v0, :cond_14

    .line 2218736
    invoke-static {p1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2218737
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/2N0;->i:S

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2218738
    :cond_14
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;

    invoke-direct {v3, p0, v1}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$2;-><init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/messaging/attachments/VideoAttachmentData;)V

    const v1, -0x79c56a6c

    invoke-static {v2, v3, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_4

    .line 2218739
    :cond_15
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/FGG;->b:S

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2218740
    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->d:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->v()Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/FGG;->c:S

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_14

    goto/16 :goto_4

    .line 2218741
    :cond_16
    invoke-static {v2}, LX/6gE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v7

    .line 2218742
    iget-object v8, v1, LX/6gF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    invoke-interface {v8, v7, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    goto/16 :goto_5
.end method
