.class public final Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;ZLcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2218672
    iput-object p1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iput-object p2, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->a:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->b:Z

    iput-object p4, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->c:Lcom/facebook/common/callercontext/CallerContext;

    iput-object p5, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2218673
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->m:LX/6eI;

    invoke-virtual {v0}, LX/6eI;->a()Z

    move-result v1

    .line 2218674
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    if-eqz v1, :cond_0

    sget-object v0, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 2218675
    :goto_0
    iput-object v0, v2, LX/1bX;->b:LX/1bY;

    .line 2218676
    move-object v0, v2

    .line 2218677
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 2218678
    iget-boolean v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->b:Z

    if-eqz v0, :cond_1

    .line 2218679
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->g:LX/1HI;

    iget-object v3, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 2218680
    :goto_1
    new-instance v3, LX/FGI;

    invoke-direct {v3, p0, v1, v2}, LX/FGI;-><init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;ZLX/1bf;)V

    iget-object v1, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v1, v1, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v3, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2218681
    return-void

    .line 2218682
    :cond_0
    sget-object v0, LX/1bY;->FULL_FETCH:LX/1bY;

    goto :goto_0

    .line 2218683
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->g:LX/1HI;

    iget-object v3, p0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    goto :goto_1
.end method
