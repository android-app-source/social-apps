.class public final Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/FH4;


# direct methods
.method public constructor <init>(LX/FH4;)V
    .locals 0

    .prologue
    .line 2219746
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;->a:LX/FH4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2219747
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;->a:LX/FH4;

    iget-object v0, v0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v1, :cond_1

    .line 2219748
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;->a:LX/FH4;

    iget-object v0, v0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->D:LX/2Nx;

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;->a:LX/FH4;

    iget-object v1, v1, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 2219749
    invoke-static {v0}, LX/2Nx;->a(LX/2Nx;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2219750
    :cond_0
    :goto_0
    return-void

    .line 2219751
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;->a:LX/FH4;

    iget-object v0, v0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 2219752
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;->a:LX/FH4;

    iget-object v0, v0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->E:LX/2OB;

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;->a:LX/FH4;

    iget-object v1, v1, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219753
    invoke-static {v0}, LX/2OB;->a(LX/2OB;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2219754
    :cond_2
    :goto_1
    goto :goto_0

    .line 2219755
    :cond_3
    iget-object v2, v0, LX/2Nx;->d:LX/0ad;

    sget-short v3, LX/FGN;->a:S

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2219756
    iget-object v2, v0, LX/2Nx;->a:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2219757
    :try_start_0
    invoke-static {v1}, LX/2Nx;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 2219758
    iget-object v3, v0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->removeItemSync(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2219759
    :catch_0
    move-exception v2

    .line 2219760
    iget-object v3, v0, LX/2Nx;->b:LX/03V;

    const-string p0, "ImageUploadCandidateStore_delete_original_image_failed"

    invoke-virtual {v3, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2219761
    :cond_4
    iget-object v2, v0, LX/2OB;->d:LX/0ad;

    sget-short v3, LX/FGN;->d:S

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2219762
    iget-object v2, v0, LX/2OB;->a:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2219763
    :try_start_1
    invoke-static {v1}, LX/2OB;->e(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v2

    .line 2219764
    iget-object v3, v0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->removeItemSync(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2219765
    :catch_1
    move-exception v2

    .line 2219766
    iget-object v3, v0, LX/2OB;->b:LX/03V;

    const-string p0, "VideoUploadCandidateStore_delete_original_video_failed"

    invoke-virtual {v3, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
