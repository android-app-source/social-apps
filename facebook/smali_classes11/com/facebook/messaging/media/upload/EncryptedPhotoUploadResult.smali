.class public Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:LX/FGR;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:[B
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2218968
    new-instance v0, LX/FGQ;

    invoke-direct {v0}, LX/FGQ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2218959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218960
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    .line 2218961
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->b:Landroid/net/Uri;

    .line 2218962
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/FGR;->valueOf(Ljava/lang/String;)LX/FGR;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->c:LX/FGR;

    .line 2218963
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->d:Ljava/lang/String;

    .line 2218964
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->e:[B

    .line 2218965
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->f:Ljava/lang/String;

    .line 2218966
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->g:Ljava/lang/Long;

    .line 2218967
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;LX/FGR;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2218950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218951
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    .line 2218952
    iput-object p2, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->b:Landroid/net/Uri;

    .line 2218953
    iput-object p3, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->c:LX/FGR;

    .line 2218954
    iput-object p4, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->d:Ljava/lang/String;

    .line 2218955
    iput-object p5, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->e:[B

    .line 2218956
    iput-object p6, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->f:Ljava/lang/String;

    .line 2218957
    iput-object p7, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->g:Ljava/lang/Long;

    .line 2218958
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;)Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;
    .locals 8

    .prologue
    .line 2218939
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218940
    new-instance v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;

    sget-object v3, LX/FGR;->Success:LX/FGR;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;-><init>(Ljava/lang/String;Landroid/net/Uri;LX/FGR;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2218949
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2218941
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2218942
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2218943
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->c:LX/FGR;

    invoke-virtual {v0}, LX/FGR;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2218944
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2218945
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->e:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2218946
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2218947
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->g:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2218948
    return-void
.end method
