.class public final Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/FI7;

.field public final synthetic b:Ljava/io/File;

.field public final synthetic c:J

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/FIE;

.field public final synthetic f:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic g:Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;LX/FI7;Ljava/io/File;JLjava/lang/String;LX/FIE;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 2221739
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->g:Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    iput-object p2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->a:LX/FI7;

    iput-object p3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->b:Ljava/io/File;

    iput-wide p4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->c:J

    iput-object p6, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->d:Ljava/lang/String;

    iput-object p7, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->e:LX/FIE;

    iput-object p8, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2221740
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->g:Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    iget-object v7, v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a:LX/18V;

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->g:Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    iget-object v8, v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->g:LX/FI8;

    new-instance v0, LX/FI6;

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->g:Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->a:LX/FI7;

    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->b:Ljava/io/File;

    iget-wide v4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->c:J

    iget-object v6, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, LX/FI6;-><init>(Ljava/lang/String;LX/FI7;Ljava/io/File;JLjava/lang/String;)V

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v8, v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2221741
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->e:LX/FIE;

    const/4 v1, 0x1

    .line 2221742
    iput-boolean v1, v0, LX/FIE;->c:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2221743
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 2221744
    :goto_0
    return-void

    .line 2221745
    :catch_0
    move-exception v0

    .line 2221746
    :try_start_1
    sget-object v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v2, "Upload failed at transfer stage with sessionId %s and streamId %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->g:Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    iget-object v5, v5, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->g:Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    iget-object v5, v5, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221747
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
