.class public Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final s:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/18V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2MM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/FIA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/FI8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/FI5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/FI3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2Me;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/ui/media/attachments/MediaResource;

.field private m:Ljava/lang/Long;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Z

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FIE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2221884
    const-class v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    sput-object v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    .line 2221885
    const-class v0, LX/FIC;

    const-string v1, "video_transcode_upload"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->s:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 1
    .param p1    # Lcom/facebook/ui/media/attachments/MediaResource;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221880
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    .line 2221881
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2221882
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    .line 2221883
    return-void
.end method

.method private a(Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;)LX/6bE;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 2221860
    const/4 v0, 0x0

    move v7, v8

    .line 2221861
    :goto_0
    if-ge v7, v9, :cond_2

    .line 2221862
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->c:LX/6bH;

    invoke-virtual {p0}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->c()Ljava/lang/String;

    move-result-object v1

    .line 2221863
    const/4 v2, 0x0

    .line 2221864
    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    if-nez v3, :cond_4

    .line 2221865
    :cond_0
    :goto_1
    move v2, v2

    .line 2221866
    if-nez v2, :cond_3

    .line 2221867
    const/4 v2, 0x0

    .line 2221868
    :goto_2
    move-object v2, v2

    .line 2221869
    const-string v6, "video_transcode_upload"

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, LX/6bH;->a(Ljava/lang/String;LX/2Mf;Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/MediaTranscodeParameters;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2221870
    const v1, -0x6250bae6

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bE;

    .line 2221871
    if-eqz v0, :cond_1

    .line 2221872
    iget-boolean v1, v0, LX/6bE;->a:Z

    move v1, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2221873
    if-nez v1, :cond_2

    .line 2221874
    :cond_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_0

    .line 2221875
    :catch_0
    move-exception v0

    .line 2221876
    sget-object v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v2, "Upload failed at transcoding stage with sessionId %s and streamId %s"

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v4, v3, v8

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2221877
    throw v0

    .line 2221878
    :cond_2
    return-object v0

    :cond_3
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->j:LX/2Me;

    goto :goto_2

    :cond_4
    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v3, v3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v4, LX/5zj;->MONTAGE:LX/5zj;

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v3, v3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v4, LX/5zj;->MONTAGE_FRONT:LX/5zj;

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v3, v3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v4, LX/5zj;->MONTAGE_BACK:LX/5zj;

    if-ne v3, v4, :cond_0

    :cond_5
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private a(LX/FIE;Ljava/util/concurrent/CountDownLatch;)V
    .locals 12

    .prologue
    .line 2221836
    iget-boolean v0, p1, LX/FIE;->c:Z

    move v0, v0

    .line 2221837
    if-eqz v0, :cond_0

    .line 2221838
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->m:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2221839
    iget-wide v10, p1, LX/FIE;->d:J

    move-wide v2, v10

    .line 2221840
    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->m:Ljava/lang/Long;

    .line 2221841
    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 2221842
    :goto_0
    return-void

    .line 2221843
    :cond_0
    sget-object v2, LX/FI7;->UnKnown:LX/FI7;

    .line 2221844
    iget-object v0, p1, LX/FIE;->a:LX/6bJ;

    move-object v0, v0

    .line 2221845
    iget-object v0, v0, LX/6bJ;->a:LX/6bI;

    sget-object v1, LX/6bI;->Audio:LX/6bI;

    if-ne v0, v1, :cond_2

    .line 2221846
    sget-object v2, LX/FI7;->Audio:LX/FI7;

    .line 2221847
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v6, v0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 2221848
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->m:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2221849
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->m:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2221850
    iget-wide v10, p1, LX/FIE;->d:J

    move-wide v8, v10

    .line 2221851
    add-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->m:Ljava/lang/Long;

    .line 2221852
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->b:LX/2MM;

    .line 2221853
    iget-object v1, p1, LX/FIE;->b:LX/6bE;

    move-object v1, v1

    .line 2221854
    iget-object v3, v1, LX/6bE;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v1, v3

    .line 2221855
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 2221856
    iget-object v9, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;

    move-object v1, p0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation$1;-><init>(Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;LX/FI7;Ljava/io/File;JLjava/lang/String;LX/FIE;Ljava/util/concurrent/CountDownLatch;)V

    const v1, -0x52fb5644

    invoke-static {v9, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 2221857
    :cond_2
    iget-object v0, p1, LX/FIE;->a:LX/6bJ;

    move-object v0, v0

    .line 2221858
    iget-object v0, v0, LX/6bJ;->a:LX/6bI;

    sget-object v1, LX/6bI;->Video:LX/6bI;

    if-ne v0, v1, :cond_1

    .line 2221859
    sget-object v2, LX/FI7;->Video:LX/FI7;

    goto :goto_1
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2221886
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->m:Ljava/lang/Long;

    .line 2221887
    iput-boolean v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->p:Z

    .line 2221888
    iput-boolean v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->q:Z

    .line 2221889
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/String;
    .locals 15
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2221763
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v0, :cond_0

    move v0, v3

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2221764
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/FID;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v5

    .line 2221765
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221766
    new-instance v6, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;

    invoke-direct {v6}, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;-><init>()V

    .line 2221767
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2221768
    new-instance v4, LX/6bM;

    invoke-direct {v4}, LX/6bM;-><init>()V

    iget-object v7, v0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    .line 2221769
    iput-object v7, v4, LX/6bM;->f:Landroid/graphics/RectF;

    .line 2221770
    move-object v4, v4

    .line 2221771
    iget v7, v0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    .line 2221772
    iput v7, v4, LX/6bM;->b:I

    .line 2221773
    move-object v4, v4

    .line 2221774
    iget v7, v0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    .line 2221775
    iput v7, v4, LX/6bM;->c:I

    .line 2221776
    move-object v4, v4

    .line 2221777
    invoke-static {v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v7

    .line 2221778
    iput-boolean v7, v4, LX/6bM;->a:Z

    .line 2221779
    move-object v4, v4

    .line 2221780
    invoke-virtual {v4}, LX/6bM;->g()Lcom/facebook/media/transcode/video/VideoEditConfig;

    move-result-object v4

    .line 2221781
    move-object v0, v4

    .line 2221782
    iput-object v0, v6, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    .line 2221783
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2221784
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->c:LX/6bH;

    invoke-virtual {v0, v5, v6}, LX/6bH;->a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;)Ljava/util/List;

    move-result-object v0

    .line 2221785
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bJ;

    .line 2221786
    iget-object v7, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    new-instance v8, LX/FIE;

    invoke-direct {v8, v0}, LX/FIE;-><init>(LX/6bJ;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2221787
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 2221788
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a:LX/18V;

    iget-object v4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->f:LX/FIA;

    new-instance v7, LX/FI9;

    iget-object v8, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p0}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->c()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, LX/FI9;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    const/4 v8, 0x0

    sget-object v9, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v7, v8, v9}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    .line 2221789
    :cond_2
    new-instance v7, Ljava/util/concurrent/CountDownLatch;

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 2221790
    invoke-direct {p0}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->m()V

    move v4, v2

    .line 2221791
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_7

    .line 2221792
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    .line 2221793
    iget-object v8, v0, LX/FIE;->b:LX/6bE;

    move-object v0, v8

    .line 2221794
    if-eqz v0, :cond_3

    iget-object v8, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->b:LX/2MM;

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    .line 2221795
    iget-object v9, v0, LX/FIE;->b:LX/6bE;

    move-object v0, v9

    .line 2221796
    iget-object v9, v0, LX/6bE;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v0, v9

    .line 2221797
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_6

    .line 2221798
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    .line 2221799
    iget-object v8, v0, LX/FIE;->a:LX/6bJ;

    move-object v0, v8

    .line 2221800
    invoke-direct {p0, v5, v0, v6}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a(Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;)LX/6bE;

    move-result-object v8

    .line 2221801
    if-eqz v8, :cond_4

    .line 2221802
    iget-boolean v0, v8, LX/6bE;->a:Z

    move v0, v0

    .line 2221803
    if-nez v0, :cond_5

    .line 2221804
    :cond_4
    sget-object v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v1, "Upload failed with at transcoding stage with sessionId %s and streamId %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2221805
    new-instance v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Segment transcode failed at %d segment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2221806
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    .line 2221807
    iput-object v8, v0, LX/FIE;->b:LX/6bE;

    .line 2221808
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->b:LX/2MM;

    .line 2221809
    iget-object v9, v8, LX/6bE;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v8, v9

    .line 2221810
    invoke-virtual {v8}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 2221811
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2221812
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 2221813
    iput-wide v8, v0, LX/FIE;->d:J

    .line 2221814
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    invoke-direct {p0, v0, v7}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a(LX/FIE;Ljava/util/concurrent/CountDownLatch;)V

    .line 2221815
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e:LX/0Xl;

    iget-object v8, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    add-int/lit8 v9, v4, 0x1

    int-to-double v10, v9

    iget-object v9, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    int-to-double v12, v9

    div-double/2addr v10, v12

    invoke-static {v8, v10, v11}, LX/FGn;->b(Lcom/facebook/ui/media/attachments/MediaResource;D)Landroid/content/Intent;

    move-result-object v8

    invoke-interface {v0, v8}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221816
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_2

    .line 2221817
    :cond_7
    const-wide/16 v4, 0x1e

    :try_start_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v4, v5, v0}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 2221818
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    .line 2221819
    iget-boolean v5, v0, LX/FIE;->c:Z

    move v0, v5

    .line 2221820
    if-nez v0, :cond_8

    move v0, v2

    .line 2221821
    :goto_3
    if-nez v0, :cond_9

    .line 2221822
    sget-object v0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v2, "Upload failed final waiting stage with sessionId %s and streamId %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    .line 2221823
    :goto_4
    monitor-exit p0

    return-object v0

    .line 2221824
    :cond_9
    :try_start_3
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e:LX/0Xl;

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v2}, LX/FGn;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2221825
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a:LX/18V;

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->h:LX/FI5;

    new-instance v3, LX/FI4;

    iget-object v4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, LX/FI4;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    sget-object v5, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3, v4, v5}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2221826
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e:LX/0Xl;

    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v3}, LX/FGn;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Landroid/content/Intent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2221827
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e:LX/0Xl;

    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v3}, LX/FGn;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Landroid/content/Intent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2221828
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->p:Z
    :try_end_3
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 2221829
    :catch_0
    move-exception v0

    .line 2221830
    :try_start_4
    sget-object v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v3, "Upload failed with at end stage with sessionId %s and streamId %s. Error: %s, Status Code: %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2221831
    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v2

    const/16 v3, 0x190

    if-ne v2, v3, :cond_a

    .line 2221832
    throw v0

    .line 2221833
    :catch_1
    move-exception v0

    .line 2221834
    sget-object v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v3, "Upload failed with at end stage with sessionId %s and streamId %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_a
    move-object v0, v1

    .line 2221835
    goto/16 :goto_4

    :cond_b
    move v0, v3

    goto/16 :goto_3
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2221760
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2221761
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    .line 2221762
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/FIE;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2221759
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->r:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final finalize()V
    .locals 7

    .prologue
    .line 2221748
    :try_start_0
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2221749
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221750
    iget-boolean v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->q:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->p:Z

    if-eqz v0, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2221751
    :cond_0
    :goto_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 2221752
    return-void

    .line 2221753
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0

    .line 2221754
    :cond_1
    :try_start_1
    const/4 v0, 0x0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iput-boolean v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->p:Z

    .line 2221755
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a:LX/18V;

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->i:LX/FI3;

    new-instance v2, LX/FI2;

    iget-object v3, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, LX/FI2;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2221756
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->q:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2221757
    :catch_0
    move-exception v0

    .line 2221758
    sget-object v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v2, "Cancel upload failed with sessionId %s and streamId %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
