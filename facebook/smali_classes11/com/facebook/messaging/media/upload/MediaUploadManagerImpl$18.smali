.class public final Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/6eR;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic c:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

.field public final synthetic d:LX/FHx;

.field public final synthetic e:Z

.field public final synthetic f:Z

.field public final synthetic g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)V
    .locals 0

    .prologue
    .line 2219620
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p4, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->c:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    iput-object p5, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->d:LX/FHx;

    iput-boolean p6, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->e:Z

    iput-boolean p7, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219621
    check-cast p1, LX/6eR;

    .line 2219622
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219623
    sget-object v0, LX/6eR;->VALID:LX/6eR;

    if-eq p1, v0, :cond_1

    .line 2219624
    const/4 v0, 0x0

    .line 2219625
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->PHOTO:LX/2MK;

    if-ne v2, v3, :cond_2

    .line 2219626
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->D:LX/2Nx;

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/2Nx;->c(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 2219627
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 2219628
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v1, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2219629
    iput-object v0, v1, LX/5zn;->b:Landroid/net/Uri;

    .line 2219630
    move-object v0, v1

    .line 2219631
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v1

    .line 2219632
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->c:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    iget-object v3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->d:LX/FHx;

    iget-boolean v4, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->e:Z

    iget-boolean v5, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->f:Z

    .line 2219633
    const/4 v10, 0x0

    .line 2219634
    invoke-static {v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v13

    .line 2219635
    iget-object v7, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v8, LX/2MK;->VIDEO:LX/2MK;

    if-ne v7, v8, :cond_3

    .line 2219636
    invoke-static {v0, v1, v10, v4}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;IZ)Z

    move-result v7

    .line 2219637
    if-nez v7, :cond_3

    .line 2219638
    iget-object v7, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    new-instance v8, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$19;

    invoke-direct {v8, v0, v13}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$19;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V

    const v9, 0x769c0097

    invoke-static {v7, v8, v9}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2219639
    const/4 v7, 0x0

    invoke-static {v7}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2219640
    :goto_1
    move-object v6, v7

    .line 2219641
    move-object v0, v6

    .line 2219642
    return-object v0

    .line 2219643
    :cond_2
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    if-ne v2, v3, :cond_0

    .line 2219644
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->E:LX/2OB;

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v2}, LX/2OB;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 2219645
    :cond_3
    sget-object v7, LX/FHx;->PHASE_ONE:LX/FHx;

    if-ne v3, v7, :cond_4

    .line 2219646
    sget-object v7, LX/FGa;->TRANSCODING:LX/FGa;

    invoke-static {v0, v13, v7}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FGa;)V

    .line 2219647
    :cond_4
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 2219648
    const-string v7, "mediaResource"

    invoke-virtual {v9, v7, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2219649
    if-eqz v5, :cond_5

    sget-object v7, LX/FHx;->PHASE_ONE:LX/FHx;

    if-ne v3, v7, :cond_5

    .line 2219650
    const-string v7, "trimEndToPreviousSyncPoint"

    const/4 v8, 0x1

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2219651
    :cond_5
    iget-object v7, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v8, LX/2MK;->VIDEO:LX/2MK;

    if-ne v7, v8, :cond_6

    .line 2219652
    iget-object v7, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->v:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/6eZ;

    .line 2219653
    const-string v8, "transcode"

    iget-boolean v7, v7, LX/6eZ;->a:Z

    invoke-virtual {v9, v8, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2219654
    const-string v7, "isOutOfSpace"

    invoke-virtual {v9, v7, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2219655
    const-string v7, "estimatedBytes"

    invoke-virtual {v9, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2219656
    const-string v8, "video_transcode"

    .line 2219657
    :goto_2
    iget-object v7, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->f:LX/0aG;

    sget-object v10, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v11, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v12, "media_transcode"

    invoke-static {v11, v12}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v11

    const v12, -0x70091384

    invoke-static/range {v7 .. v12}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    .line 2219658
    iget-object v8, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    invoke-virtual {v8, v13, v7}, LX/2MT;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/1ML;)V

    .line 2219659
    new-instance v8, LX/FGw;

    invoke-direct {v8, v0, v3, v13, v1}, LX/FGw;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/FHx;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;)V

    iget-object v9, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    goto :goto_1

    .line 2219660
    :cond_6
    const-string v7, "photoQuality"

    invoke-virtual {v9, v7, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2219661
    const-string v7, "phase"

    invoke-virtual {v3}, LX/FHx;->getIndex()I

    move-result v8

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2219662
    const-string v8, "photo_transcode"

    goto :goto_2
.end method
