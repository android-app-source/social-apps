.class public Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2222597
    new-instance v0, LX/FIk;

    invoke-direct {v0}, LX/FIk;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2222607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222608
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->a:Ljava/lang/String;

    .line 2222609
    iput-object p2, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->b:Ljava/lang/String;

    .line 2222610
    iput-object p3, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->c:Ljava/lang/String;

    .line 2222611
    iput p4, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->d:I

    .line 2222612
    return-void
.end method

.method public static a(LX/0lF;)Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;
    .locals 5

    .prologue
    .line 2222604
    const-string v0, "turn_server_ipv6"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "turn_server_ipv4"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2222605
    :goto_0
    new-instance v1, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;

    const-string v2, "turn_user_id"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "turn_password"

    invoke-virtual {p0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "turn_server_port"

    invoke-virtual {p0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v1

    .line 2222606
    :cond_0
    const-string v0, "turn_server_ipv6"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2222603
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2222598
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2222599
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2222600
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2222601
    iget v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2222602
    return-void
.end method
