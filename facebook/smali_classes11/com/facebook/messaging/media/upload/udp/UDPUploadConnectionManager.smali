.class public Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/FIW;

.field public final c:LX/14U;

.field public final d:LX/0TD;

.field private final e:LX/0aG;

.field public final f:LX/0SG;

.field public final g:Ljava/util/concurrent/atomic/AtomicLong;

.field public final h:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager$ServerConfigChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18V;LX/FIW;LX/0TD;LX/0aG;LX/0SG;)V
    .locals 4
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222638
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->a:LX/18V;

    .line 2222639
    iput-object p2, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->b:LX/FIW;

    .line 2222640
    iput-object p3, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->d:LX/0TD;

    .line 2222641
    iput-object p4, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->e:LX/0aG;

    .line 2222642
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->c:LX/14U;

    .line 2222643
    iput-object p5, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->f:LX/0SG;

    .line 2222644
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->g:Ljava/util/concurrent/atomic/AtomicLong;

    .line 2222645
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->h:Ljava/util/concurrent/atomic/AtomicReference;

    .line 2222646
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->i:Ljava/util/List;

    .line 2222647
    return-void
.end method

.method public static a(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Ljava/lang/String;Landroid/os/Parcelable;)LX/1ML;
    .locals 6

    .prologue
    .line 2222664
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2222665
    const-string v0, "udp_parcel_key"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2222666
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->e:LX/0aG;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x378c58fd    # -249500.05f

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 2222667
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2222668
    new-instance v1, LX/FIl;

    invoke-direct {v1, p0}, LX/FIl;-><init>(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2222669
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;
    .locals 9

    .prologue
    .line 2222670
    sget-object v0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->j:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    if-nez v0, :cond_1

    .line 2222671
    const-class v1, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    monitor-enter v1

    .line 2222672
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->j:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2222673
    if-eqz v2, :cond_0

    .line 2222674
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2222675
    new-instance v3, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {v0}, LX/FIW;->a(LX/0QB;)LX/FIW;

    move-result-object v5

    check-cast v5, LX/FIW;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;-><init>(LX/18V;LX/FIW;LX/0TD;LX/0aG;LX/0SG;)V

    .line 2222676
    move-object v0, v3

    .line 2222677
    sput-object v0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->j:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222678
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2222679
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2222680
    :cond_1
    sget-object v0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->j:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    return-object v0

    .line 2222681
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2222682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;)V
    .locals 5

    .prologue
    .line 2222648
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIh;

    .line 2222649
    iget-object v2, v0, LX/FIh;->d:LX/FIK;

    iget-object v3, p1, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/FIK;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222650
    :try_start_1
    iget-object v2, p1, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->c:Ljava/lang/String;

    iget v3, p1, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->d:I

    invoke-static {v0, v2, v3}, LX/FIh;->a(LX/FIh;Ljava/lang/String;I)V

    .line 2222651
    iget-object v2, v0, LX/FIh;->d:LX/FIK;

    invoke-virtual {v2}, LX/FIK;->b()Z
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222652
    :goto_1
    :try_start_2
    iget-object v2, v0, LX/FIh;->c:LX/1qI;

    invoke-virtual {v2}, LX/1qI;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2222653
    goto :goto_0

    .line 2222654
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2222655
    :cond_0
    monitor-exit p0

    return-void

    .line 2222656
    :catch_0
    move-exception v2

    .line 2222657
    const-class v3, LX/FIh;

    const-string v4, "Unable to update socket destination address"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2222658
    :catch_1
    move-exception v2

    .line 2222659
    const-class v3, LX/FIh;

    const-string v4, "Unable to send stun ping to new address"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static b(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;)V
    .locals 4

    .prologue
    .line 2222660
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 2222661
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->g:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2222662
    invoke-direct {p0, p1}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->a(Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;)V

    .line 2222663
    return-void
.end method

.method public static c(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;LX/FIn;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "LX/FIn;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2222622
    new-instance v0, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;

    iget v3, p3, LX/FIn;->b:I

    iget-wide v4, p3, LX/FIn;->a:J

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;IJ)V

    .line 2222623
    const-string v1, "udp_connection_upload_metadata"

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->a(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Ljava/lang/String;Landroid/os/Parcelable;)LX/1ML;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/FIh;)V
    .locals 1

    .prologue
    .line 2222624
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222625
    monitor-exit p0

    return-void

    .line 2222626
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;LX/FIn;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "LX/FIn;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2222627
    iget-wide v0, p3, LX/FIn;->a:J

    .line 2222628
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2222629
    new-instance v2, Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;

    invoke-direct {v2, v0, v1}, Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;-><init>(J)V

    .line 2222630
    iget-object v3, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->d:LX/0TD;

    new-instance v4, LX/FIm;

    invoke-direct {v4, p0, v2}, LX/FIm;-><init>(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2222631
    const v3, 0x5ff02d73

    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;

    .line 2222632
    invoke-static {p0, v2}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->b(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;)V

    .line 2222633
    :cond_0
    :goto_0
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->c(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;LX/FIn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2222634
    :cond_1
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2222635
    iget-object v4, p0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x1b7740

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 2222636
    const-string v2, "udp_connection_refresh_server_config"

    new-instance v3, Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;

    invoke-direct {v3, v0, v1}, Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;-><init>(J)V

    invoke-static {p0, v2, v3}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->a(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Ljava/lang/String;Landroid/os/Parcelable;)LX/1ML;

    goto :goto_0
.end method
