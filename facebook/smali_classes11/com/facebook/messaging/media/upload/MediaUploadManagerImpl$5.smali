.class public final Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2219740
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 2219741
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v1, :cond_1

    .line 2219742
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->D:LX/2Nx;

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2Nx;->a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2219743
    :cond_0
    :goto_0
    return-void

    .line 2219744
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 2219745
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->E:LX/2OB;

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/2OB;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
