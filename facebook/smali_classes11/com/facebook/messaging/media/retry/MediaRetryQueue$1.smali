.class public final Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/retry/MediaRetryQueue;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2218835
    iput-object p1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iput-object p2, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2218836
    iget-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iget-object v0, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->j:LX/6eI;

    invoke-virtual {v0}, LX/6eI;->a()Z

    move-result v0

    .line 2218837
    iget-object v1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->a:Landroid/net/Uri;

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    if-eqz v0, :cond_0

    sget-object v0, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 2218838
    :goto_0
    iput-object v0, v1, LX/1bX;->b:LX/1bY;

    .line 2218839
    move-object v0, v1

    .line 2218840
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2218841
    iget-object v1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iget-object v1, v1, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->f:LX/1HI;

    sget-object v2, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 2218842
    new-instance v1, LX/FGM;

    invoke-direct {v1, p0}, LX/FGM;-><init>(Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iget-object v2, v2, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2218843
    return-void

    .line 2218844
    :cond_0
    sget-object v0, LX/1bY;->FULL_FETCH:LX/1bY;

    goto :goto_0
.end method
