.class public final Lcom/facebook/messaging/media/retry/MediaRetryQueue$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/retry/MediaRetryQueue;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/retry/MediaRetryQueue;)V
    .locals 0

    .prologue
    .line 2218845
    iput-object p1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$2;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2218846
    iget-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$2;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    .line 2218847
    invoke-virtual {v0}, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2218848
    :cond_0
    :goto_0
    return-void

    .line 2218849
    :cond_1
    iget-object v1, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->a:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 2218850
    iget-object v1, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->g:LX/FGL;

    .line 2218851
    iget-object v2, v1, LX/FGL;->b:LX/0Yb;

    if-eqz v2, :cond_2

    .line 2218852
    iget-object v2, v1, LX/FGL;->b:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->c()V

    .line 2218853
    const/4 v2, 0x0

    iput-object v2, v1, LX/FGL;->b:LX/0Yb;

    .line 2218854
    :cond_2
    iget-object v1, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 2218855
    iget-object v2, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->b:LX/2MA;

    invoke-interface {v2}, LX/2MA;->c()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2218856
    iget-object v1, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->g:LX/FGL;

    invoke-virtual {v1, v0}, LX/FGL;->a(LX/0YZ;)V

    goto :goto_0

    .line 2218857
    :cond_3
    invoke-static {v1}, LX/1H1;->i(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FGK;

    invoke-virtual {v2}, LX/FGK;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2218858
    :cond_4
    iget-object v2, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2218859
    :cond_5
    iget-object v2, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->d:Ljava/util/concurrent/ExecutorService;

    new-instance p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;-><init>(Lcom/facebook/messaging/media/retry/MediaRetryQueue;Landroid/net/Uri;)V

    const v1, -0x6c0a5c05

    invoke-static {v2, p0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
.end method
