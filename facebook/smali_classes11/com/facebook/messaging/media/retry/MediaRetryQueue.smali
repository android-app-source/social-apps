.class public Lcom/facebook/messaging/media/retry/MediaRetryQueue;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0YZ;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final k:Lcom/facebook/common/callercontext/CallerContext;

.field private static final m:Ljava/lang/Object;


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2MA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/net/ConnectivityManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/FGL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/6eI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final l:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/net/Uri;",
            "LX/FGK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2218860
    const-class v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    const-string v1, "media_retry"

    const-string v2, "image_retry"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 2218861
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->m:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2218916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218917
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    .line 2218918
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/media/retry/MediaRetryQueue;
    .locals 7

    .prologue
    .line 2218889
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2218890
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2218891
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2218892
    if-nez v1, :cond_0

    .line 2218893
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2218894
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2218895
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2218896
    sget-object v1, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->m:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2218897
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2218898
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2218899
    :cond_1
    if-nez v1, :cond_4

    .line 2218900
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2218901
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2218902
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->b(LX/0QB;)Lcom/facebook/messaging/media/retry/MediaRetryQueue;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2218903
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2218904
    if-nez v1, :cond_2

    .line 2218905
    sget-object v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->m:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2218906
    :goto_1
    if-eqz v0, :cond_3

    .line 2218907
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2218908
    :goto_3
    check-cast v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2218909
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2218910
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2218911
    :catchall_1
    move-exception v0

    .line 2218912
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2218913
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2218914
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2218915
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->m:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/media/retry/MediaRetryQueue;
    .locals 11

    .prologue
    .line 2218880
    new-instance v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    invoke-direct {v0}, Lcom/facebook/messaging/media/retry/MediaRetryQueue;-><init>()V

    .line 2218881
    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/2M4;->a(LX/0QB;)LX/2MA;

    move-result-object v2

    check-cast v2, LX/2MA;

    invoke-static {p0}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v6

    check-cast v6, LX/1HI;

    .line 2218882
    new-instance v8, LX/FGL;

    invoke-direct {v8}, LX/FGL;-><init>()V

    .line 2218883
    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    .line 2218884
    iput-object v7, v8, LX/FGL;->a:LX/0Xl;

    .line 2218885
    move-object v7, v8

    .line 2218886
    check-cast v7, LX/FGL;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/6eI;->a(LX/0QB;)LX/6eI;

    move-result-object v10

    check-cast v10, LX/6eI;

    .line 2218887
    iput-object v1, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->a:LX/0Sh;

    iput-object v2, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->b:LX/2MA;

    iput-object v3, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->c:Landroid/net/ConnectivityManager;

    iput-object v4, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->e:LX/03V;

    iput-object v6, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->f:LX/1HI;

    iput-object v7, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->g:LX/FGL;

    iput-object v8, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->h:LX/0So;

    iput-object v9, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->i:LX/0ad;

    iput-object v10, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->j:LX/6eI;

    .line 2218888
    return-object v0
.end method


# virtual methods
.method public final a(LX/1bf;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 2218871
    iget-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2218872
    iget-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/FGJ;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2218873
    :goto_0
    return-void

    .line 2218874
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/FGJ;->c:I

    invoke-interface {v0, v1, v2, v6}, LX/0ad;->a(LX/0c0;II)I

    move-result v4

    .line 2218875
    const-wide/32 v0, 0x5265c00

    iget-object v2, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->i:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget v5, LX/FGJ;->d:I

    invoke-interface {v2, v3, v5, v6}, LX/0ad;->a(LX/0c0;II)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v0

    .line 2218876
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v0

    .line 2218877
    new-instance v0, LX/FGK;

    iget-object v5, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->h:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    add-long/2addr v2, v6

    iget-object v5, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->h:LX/0So;

    invoke-direct/range {v0 .. v5}, LX/FGK;-><init>(Landroid/net/Uri;JILX/0So;)V

    .line 2218878
    iget-object v2, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218879
    iget-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->g:LX/FGL;

    invoke-virtual {v0, p0}, LX/FGL;->a(LX/0YZ;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 2218870
    iget-object v0, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/FGJ;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    return v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x71dd3cee

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2218862
    iget-object v1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2218863
    iget-object v1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->e:LX/03V;

    const-string v2, "media_retry"

    const-string v3, "Media retry network receiver is wake up with empty queue.."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218864
    const/16 v1, 0x27

    const v2, 0x1698f2e6

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2218865
    :goto_0
    return-void

    .line 2218866
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->c:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2218867
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2218868
    iget-object v1, p0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/messaging/media/retry/MediaRetryQueue$2;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/media/retry/MediaRetryQueue$2;-><init>(Lcom/facebook/messaging/media/retry/MediaRetryQueue;)V

    const v3, -0x24f36480

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2218869
    :cond_1
    const v1, -0x7f8d48a5

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
