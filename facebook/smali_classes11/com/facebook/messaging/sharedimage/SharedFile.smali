.class public Lcom/facebook/messaging/sharedimage/SharedFile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/sharedimage/SharedFile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Landroid/net/Uri;

.field public final d:Lcom/facebook/ipc/media/data/MimeType;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2226212
    new-instance v0, LX/FLD;

    invoke-direct {v0}, LX/FLD;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sharedimage/SharedFile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2226213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->a:Ljava/lang/String;

    .line 2226215
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->b:I

    .line 2226216
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->c:Landroid/net/Uri;

    .line 2226217
    const-class v0, Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MimeType;

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->d:Lcom/facebook/ipc/media/data/MimeType;

    .line 2226218
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2226219
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2226220
    instance-of v1, p1, Lcom/facebook/messaging/sharedimage/SharedFile;

    if-eqz v1, :cond_0

    .line 2226221
    check-cast p1, Lcom/facebook/messaging/sharedimage/SharedFile;

    .line 2226222
    iget-object v1, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/sharedimage/SharedFile;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->b:I

    iget v2, p1, Lcom/facebook/messaging/sharedimage/SharedFile;->b:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->c:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/messaging/sharedimage/SharedFile;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->d:Lcom/facebook/ipc/media/data/MimeType;

    iget-object v2, p1, Lcom/facebook/messaging/sharedimage/SharedFile;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 2226223
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2226224
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->c:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->d:Lcom/facebook/ipc/media/data/MimeType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2226225
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2226226
    iget v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2226227
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2226228
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedFile;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2226229
    return-void
.end method
