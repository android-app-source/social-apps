.class public Lcom/facebook/messaging/sharedimage/SharedImage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/sharedimage/SharedImage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public c:Z

.field public d:Landroid/net/Uri;

.field public final e:Lcom/facebook/ui/media/attachments/MediaResource;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/facebook/messaging/model/messages/Message;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2226234
    new-instance v0, LX/FLF;

    invoke-direct {v0}, LX/FLF;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sharedimage/SharedImage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2226235
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/sharedimage/SharedImage;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2226236
    return-void
.end method

.method private constructor <init>(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1
    .param p6    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2226237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->c:Z

    .line 2226239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->d:Landroid/net/Uri;

    .line 2226240
    iput-object p1, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2226241
    iput-object p2, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->f:Ljava/lang/String;

    .line 2226242
    iput-object p3, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->g:Ljava/lang/String;

    .line 2226243
    iput-object p4, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->h:Ljava/lang/String;

    .line 2226244
    iput-object p5, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->i:Ljava/lang/String;

    .line 2226245
    iget v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    iput v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->a:I

    .line 2226246
    iget v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    iput v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->b:I

    .line 2226247
    iput-object p6, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->j:Lcom/facebook/messaging/model/messages/Message;

    .line 2226248
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2226249
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2226250
    instance-of v0, p1, Lcom/facebook/messaging/sharedimage/SharedImage;

    if-eqz v0, :cond_1

    .line 2226251
    check-cast p1, Lcom/facebook/messaging/sharedimage/SharedImage;

    .line 2226252
    iget-object v0, p1, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 2226253
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2226254
    iget-object v0, p1, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 2226255
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v1}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2226256
    :goto_0
    return v0

    .line 2226257
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 2226258
    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    iget-object v1, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 2226259
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2226260
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2226261
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2226262
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2226263
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2226264
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2226265
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/SharedImage;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2226266
    return-void
.end method
