.class public final Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2226397
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel;

    new-instance v1, Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2226398
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2226399
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2226400
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2226401
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2226402
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2226403
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2226404
    if-eqz v2, :cond_2

    .line 2226405
    const-string p0, "message_shared_media"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2226406
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2226407
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2226408
    if-eqz p0, :cond_1

    .line 2226409
    const-string v0, "page_info"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2226410
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2226411
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2226412
    if-eqz v0, :cond_0

    .line 2226413
    const-string v2, "start_cursor"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2226414
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2226415
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2226416
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2226417
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2226418
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2226419
    check-cast p1, Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel$Serializer;->a(Lcom/facebook/messaging/sharedimage/graphql/MediaFbIdToCursorQueryModels$FbIdToCursorQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
