.class public final Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2226852
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2226853
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2226850
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2226851
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2226821
    if-nez p1, :cond_0

    .line 2226822
    :goto_0
    return v1

    .line 2226823
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2226824
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2226825
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2226826
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2226827
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2226828
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2226829
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2226830
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2226831
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2226832
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2226833
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2226834
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2226835
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2226836
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2226837
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2226838
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2226839
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2226840
    :sswitch_3
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2226841
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2226842
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 2226843
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2226844
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2226845
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2226846
    :sswitch_4
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2226847
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2226848
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2226849
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x51ff9702 -> :sswitch_3
        -0x4a086c40 -> :sswitch_2
        -0x1cb41d3a -> :sswitch_4
        0x3bc9cff1 -> :sswitch_1
        0x6c62d788 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2226820
    new-instance v0, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2226817
    sparse-switch p0, :sswitch_data_0

    .line 2226818
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2226819
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x51ff9702 -> :sswitch_0
        -0x4a086c40 -> :sswitch_0
        -0x1cb41d3a -> :sswitch_0
        0x3bc9cff1 -> :sswitch_0
        0x6c62d788 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2226816
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2226814
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;->b(I)V

    .line 2226815
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2226809
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2226810
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2226811
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2226812
    iput p2, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;->b:I

    .line 2226813
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2226783
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2226808
    new-instance v0, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2226805
    iget v0, p0, LX/1vt;->c:I

    .line 2226806
    move v0, v0

    .line 2226807
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2226802
    iget v0, p0, LX/1vt;->c:I

    .line 2226803
    move v0, v0

    .line 2226804
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2226799
    iget v0, p0, LX/1vt;->b:I

    .line 2226800
    move v0, v0

    .line 2226801
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2226796
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2226797
    move-object v0, v0

    .line 2226798
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2226787
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2226788
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2226789
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2226790
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2226791
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2226792
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2226793
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2226794
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2226795
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2226784
    iget v0, p0, LX/1vt;->c:I

    .line 2226785
    move v0, v0

    .line 2226786
    return v0
.end method
