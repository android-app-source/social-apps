.class public final Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$SubsequentSharedPhotosModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2227070
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$SubsequentSharedPhotosModel;

    new-instance v1, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$SubsequentSharedPhotosModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$SubsequentSharedPhotosModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2227071
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2227072
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2227073
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2227074
    const/4 v2, 0x0

    .line 2227075
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2227076
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2227077
    :goto_0
    move v1, v2

    .line 2227078
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2227079
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2227080
    new-instance v1, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$SubsequentSharedPhotosModel;

    invoke-direct {v1}, Lcom/facebook/messaging/sharedimage/graphql/SharedImageHistoryQueryModels$SubsequentSharedPhotosModel;-><init>()V

    .line 2227081
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2227082
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2227083
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2227084
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2227085
    :cond_0
    return-object v1

    .line 2227086
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2227087
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2227088
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2227089
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2227090
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 2227091
    const-string v5, "imageCount"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2227092
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2227093
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_a

    .line 2227094
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2227095
    :goto_2
    move v3, v4

    .line 2227096
    goto :goto_1

    .line 2227097
    :cond_3
    const-string v5, "photoResult"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2227098
    invoke-static {p1, v0}, LX/FLZ;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 2227099
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2227100
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2227101
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2227102
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 2227103
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_8

    .line 2227104
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2227105
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2227106
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v7, :cond_6

    .line 2227107
    const-string p0, "count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2227108
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v5

    goto :goto_3

    .line 2227109
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2227110
    :cond_8
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2227111
    if-eqz v3, :cond_9

    .line 2227112
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 2227113
    :cond_9
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v3, v4

    move v6, v4

    goto :goto_3
.end method
