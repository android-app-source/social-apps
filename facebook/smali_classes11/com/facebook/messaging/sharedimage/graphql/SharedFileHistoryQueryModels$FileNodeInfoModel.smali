.class public final Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1169c43e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2226505
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2226504
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2226502
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2226503
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2226500
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->e:Ljava/lang/String;

    .line 2226501
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2226498
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->g:Ljava/lang/String;

    .line 2226499
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2226496
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->h:Ljava/lang/String;

    .line 2226497
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2226474
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2226475
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2226476
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2226477
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2226478
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2226479
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2226480
    const/4 v0, 0x1

    iget v3, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->f:I

    invoke-virtual {p1, v0, v3, v4}, LX/186;->a(III)V

    .line 2226481
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2226482
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2226483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2226484
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2226493
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2226494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2226495
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2226490
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2226491
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;->f:I

    .line 2226492
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2226487
    new-instance v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$FileNodeInfoModel;-><init>()V

    .line 2226488
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2226489
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2226486
    const v0, 0x4d0f9b13    # 1.50581552E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2226485
    const v0, 0x2f58b603

    return v0
.end method
