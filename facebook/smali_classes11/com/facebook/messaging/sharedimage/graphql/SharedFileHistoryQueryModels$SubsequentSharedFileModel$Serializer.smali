.class public final Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2226620
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;

    new-instance v1, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2226621
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2226622
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2226623
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2226624
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2226625
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2226626
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2226627
    if-eqz v2, :cond_0

    .line 2226628
    const-string p0, "FileCount"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2226629
    invoke-static {v1, v2, p1}, LX/FLM;->a(LX/15i;ILX/0nX;)V

    .line 2226630
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2226631
    if-eqz v2, :cond_1

    .line 2226632
    const-string p0, "FileResult"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2226633
    invoke-static {v1, v2, p1, p2}, LX/FLN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2226634
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2226635
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2226636
    check-cast p1, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Serializer;->a(Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;LX/0nX;LX/0my;)V

    return-void
.end method
