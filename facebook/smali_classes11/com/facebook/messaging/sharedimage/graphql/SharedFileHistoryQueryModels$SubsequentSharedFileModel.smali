.class public final Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x40e9c2a8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2226637
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2226662
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2226660
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2226661
    return-void
.end method

.method private a()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2226658
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->e:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->e:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    .line 2226659
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->e:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    return-object v0
.end method

.method private j()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2226656
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->f:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    iput-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->f:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    .line 2226657
    iget-object v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->f:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2226663
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2226664
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->a()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2226665
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->j()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2226666
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2226667
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2226668
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2226669
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2226670
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2226643
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2226644
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->a()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2226645
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->a()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    .line 2226646
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->a()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2226647
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;

    .line 2226648
    iput-object v0, v1, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->e:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    .line 2226649
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->j()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2226650
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->j()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    .line 2226651
    invoke-direct {p0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->j()Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2226652
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;

    .line 2226653
    iput-object v0, v1, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;->f:Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileResultModel;

    .line 2226654
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2226655
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2226640
    new-instance v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;

    invoke-direct {v0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel;-><init>()V

    .line 2226641
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2226642
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2226639
    const v0, -0x252786ed

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2226638
    const v0, -0x2c24372f

    return v0
.end method
