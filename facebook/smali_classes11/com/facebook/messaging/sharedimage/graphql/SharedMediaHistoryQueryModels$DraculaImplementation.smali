.class public final Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2227572
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2227573
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2227570
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2227571
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2227536
    if-nez p1, :cond_0

    .line 2227537
    :goto_0
    return v1

    .line 2227538
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2227539
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2227540
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2227541
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2227542
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2227543
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2227544
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2227545
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2227546
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2227547
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2227548
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2227549
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2227550
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2227551
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2227552
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2227553
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2227554
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2227555
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2227556
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2227557
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2227558
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2227559
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2227560
    :sswitch_4
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2227561
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2227562
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 2227563
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2227564
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2227565
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2227566
    :sswitch_5
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2227567
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2227568
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2227569
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x51619406 -> :sswitch_2
        -0x2b0a1819 -> :sswitch_0
        -0x6be20d6 -> :sswitch_3
        0x29cdfa9 -> :sswitch_4
        0x17659023 -> :sswitch_5
        0x6bb43105 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2227535
    new-instance v0, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2227532
    sparse-switch p0, :sswitch_data_0

    .line 2227533
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2227534
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x51619406 -> :sswitch_0
        -0x2b0a1819 -> :sswitch_0
        -0x6be20d6 -> :sswitch_0
        0x29cdfa9 -> :sswitch_0
        0x17659023 -> :sswitch_0
        0x6bb43105 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2227531
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2227529
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;->b(I)V

    .line 2227530
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2227574
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2227575
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2227576
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2227577
    iput p2, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;->b:I

    .line 2227578
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2227528
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2227503
    new-instance v0, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2227525
    iget v0, p0, LX/1vt;->c:I

    .line 2227526
    move v0, v0

    .line 2227527
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2227522
    iget v0, p0, LX/1vt;->c:I

    .line 2227523
    move v0, v0

    .line 2227524
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2227519
    iget v0, p0, LX/1vt;->b:I

    .line 2227520
    move v0, v0

    .line 2227521
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2227516
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2227517
    move-object v0, v0

    .line 2227518
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2227507
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2227508
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2227509
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2227510
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2227511
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2227512
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2227513
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2227514
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/sharedimage/graphql/SharedMediaHistoryQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2227515
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2227504
    iget v0, p0, LX/1vt;->c:I

    .line 2227505
    move v0, v0

    .line 2227506
    return v0
.end method
