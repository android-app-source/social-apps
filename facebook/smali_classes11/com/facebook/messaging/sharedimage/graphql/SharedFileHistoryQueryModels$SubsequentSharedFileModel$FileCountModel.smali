.class public final Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2226555
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2226556
    const-class v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2226557
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2226558
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2226559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2226560
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2226561
    iget v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 2226562
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2226563
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2226564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2226565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2226566
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2226567
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2226568
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;->e:I

    .line 2226569
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2226570
    new-instance v0, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;

    invoke-direct {v0}, Lcom/facebook/messaging/sharedimage/graphql/SharedFileHistoryQueryModels$SubsequentSharedFileModel$FileCountModel;-><init>()V

    .line 2226571
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2226572
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2226573
    const v0, 0x72bfac09

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2226574
    const v0, 0x6b20d116

    return v0
.end method
