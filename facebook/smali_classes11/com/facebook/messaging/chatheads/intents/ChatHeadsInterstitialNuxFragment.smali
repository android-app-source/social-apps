.class public Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;
.super Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Landroid/widget/ViewFlipper;

.field public r:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

.field private s:Lcom/facebook/resources/ui/FbTextView;

.field private t:Lcom/facebook/resources/ui/FbTextView;

.field private final u:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2212727
    invoke-direct {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;-><init>()V

    .line 2212728
    new-instance v0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment$1;-><init>(Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->u:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;)V
    .locals 3

    .prologue
    .line 2212729
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->o:LX/1Ml;

    invoke-virtual {v0}, LX/1Ml;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2212730
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dd;->d:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2212731
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2212732
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2212733
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 2212734
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const v2, 0x7f0e04fa

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 2212735
    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2212736
    invoke-static {p0}, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->a(Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;)V

    .line 2212737
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3d48ffc7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2212738
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2212739
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {v1}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object p1

    check-cast p1, LX/1Ml;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iput-object v4, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->n:Landroid/os/Handler;

    iput-object p1, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->o:LX/1Ml;

    iput-object v1, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->p:Lcom/facebook/content/SecureContextHelper;

    .line 2212740
    const/16 v1, 0x2b

    const v2, 0x59aaa199

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x224415d7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2212741
    const v1, 0x7f030280

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x38e6581f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x18a6aae4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2212742
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onPause()V

    .line 2212743
    iget-object v1, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->r:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2212744
    const/16 v1, 0x2b

    const v2, 0xf71509f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x397cca29

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2212745
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onResume()V

    .line 2212746
    iget-object v1, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->n:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->u:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    const v3, 0x30d384b8    # 1.5389992E-9f

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2212747
    const/16 v1, 0x2b

    const v2, 0x3c200bac

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2212748
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2212749
    const v0, 0x7f0d0921

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->q:Landroid/widget/ViewFlipper;

    .line 2212750
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->q:Landroid/widget/ViewFlipper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setAutoStart(Z)V

    .line 2212751
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->q:Landroid/widget/ViewFlipper;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setFlipInterval(I)V

    .line 2212752
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->q:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040028

    invoke-virtual {v0, v1, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 2212753
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->q:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040029

    invoke-virtual {v0, v1, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 2212754
    const v0, 0x7f0d0923

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    iput-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->r:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 2212755
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->r:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    const v2, 0x7f08018d

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setMessage(Landroid/text/Spanned;)V

    .line 2212756
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->r:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    sget-object v1, LX/D9U;->RIGHT:LX/D9U;

    invoke-virtual {v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setOrigin(LX/D9U;)V

    .line 2212757
    const v0, 0x7f0d0927

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 2212758
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->s:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/FDX;

    invoke-direct {v1, p0}, LX/FDX;-><init>(Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2212759
    const v0, 0x7f0d0928

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 2212760
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;->t:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/FDY;

    invoke-direct {v1, p0}, LX/FDY;-><init>(Lcom/facebook/messaging/chatheads/intents/ChatHeadsInterstitialNuxFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2212761
    return-void
.end method
