.class public Lcom/facebook/messaging/ui/name/ThreadNameView;
.super LX/2Wj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Wj",
        "<",
        "LX/FON;",
        ">;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/ui/name/NamesOnlyThreadNameViewComputer;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3L2;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/ui/name/DefaultThreadNameViewComputer;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3L2;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/3L2;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2235147
    invoke-direct {p0, p1}, LX/2Wj;-><init>(Landroid/content/Context;)V

    .line 2235148
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/ui/name/ThreadNameView;->a(Landroid/util/AttributeSet;)V

    .line 2235149
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2235168
    invoke-direct {p0, p1, p2}, LX/2Wj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2235169
    invoke-direct {p0, p2}, Lcom/facebook/messaging/ui/name/ThreadNameView;->a(Landroid/util/AttributeSet;)V

    .line 2235170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2235165
    invoke-direct {p0, p1, p2, p3}, LX/2Wj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2235166
    invoke-direct {p0, p2}, Lcom/facebook/messaging/ui/name/ThreadNameView;->a(Landroid/util/AttributeSet;)V

    .line 2235167
    return-void
.end method

.method private a(LX/FON;)Ljava/lang/CharSequence;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2235164
    iget-object v0, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->c:LX/3L2;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, LX/3L2;->a(LX/FON;I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 2235153
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/name/ThreadNameView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->ThreadNameView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2235154
    const/16 v1, 0x7

    sget-object v2, LX/3L4;->USE_THREAD_NAME_IF_AVAILABLE:LX/3L4;

    iget v2, v2, LX/3L4;->value:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 2235155
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2235156
    const-class v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/ui/name/ThreadNameView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2235157
    sget-object v0, LX/3L4;->USE_THREAD_NAME_IF_AVAILABLE:LX/3L4;

    iget v0, v0, LX/3L4;->value:I

    if-eq v1, v0, :cond_0

    .line 2235158
    sget-object v0, LX/3L4;->USE_PARTICIPANTS_NAMES_ONLY:LX/3L4;

    iget v0, v0, LX/3L4;->value:I

    if-ne v1, v0, :cond_0

    .line 2235159
    iget-object v0, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3L2;

    .line 2235160
    iput-object v0, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->c:LX/3L2;

    .line 2235161
    :goto_0
    return-void

    .line 2235162
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3L2;

    .line 2235163
    iput-object v0, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->c:LX/3L2;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/ui/name/ThreadNameView;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/ui/name/ThreadNameView;",
            "LX/0Or",
            "<",
            "LX/3L2;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3L2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2235152
    iput-object p1, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->b:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/ui/name/ThreadNameView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    const/16 v1, 0xde4

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xde3

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/ui/name/ThreadNameView;->a(Lcom/facebook/messaging/ui/name/ThreadNameView;LX/0Or;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2235151
    check-cast p1, LX/FON;

    invoke-direct {p0, p1}, Lcom/facebook/messaging/ui/name/ThreadNameView;->a(LX/FON;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getVariableTextLayoutComputer()LX/3L3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/3L3",
            "<",
            "LX/FON;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2235150
    iget-object v0, p0, Lcom/facebook/messaging/ui/name/ThreadNameView;->c:LX/3L2;

    return-object v0
.end method
