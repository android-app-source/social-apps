.class public Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/facebook/fbui/glyph/GlyphButton;

.field public c:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/FOQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2235127
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2235128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2235118
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2235119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2235120
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2235121
    const v0, 0x7f030b19

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2235122
    const v0, 0x7f0d1bf2

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->a:Landroid/widget/TextView;

    .line 2235123
    const v0, 0x7f0d0a7e

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2235124
    iget-object v0, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/FOP;

    invoke-direct {v1, p0}, LX/FOP;-><init>(Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2235125
    invoke-virtual {p0, p0}, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2235126
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x171dc149

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2235114
    iget-object v1, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->d:LX/FOQ;

    if-eqz v1, :cond_0

    .line 2235115
    iget-object v1, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->c:Lcom/facebook/user/model/User;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2235116
    iget-object v1, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->d:LX/FOQ;

    iget-object v2, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->c:Lcom/facebook/user/model/User;

    invoke-interface {v1, v2}, LX/FOQ;->a(Lcom/facebook/user/model/User;)V

    .line 2235117
    :cond_0
    const v1, -0x3ed7cc0d

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setListener(LX/FOQ;)V
    .locals 0
    .param p1    # LX/FOQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2235112
    iput-object p1, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->d:LX/FOQ;

    .line 2235113
    return-void
.end method

.method public setUser(Lcom/facebook/user/model/User;)V
    .locals 3

    .prologue
    .line 2235102
    iput-object p1, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->c:Lcom/facebook/user/model/User;

    .line 2235103
    iget-object v0, p1, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2235104
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2235105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2235106
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080f60

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2235107
    iget-object v1, p1, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2235108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2235109
    iget-object v1, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2235110
    :goto_0
    return-void

    .line 2235111
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/ui/name/MessengerUserBubbleView;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
