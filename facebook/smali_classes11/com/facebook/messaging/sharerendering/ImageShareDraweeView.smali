.class public Lcom/facebook/messaging/sharerendering/ImageShareDraweeView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2228201
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2228202
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2228199
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2228200
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2228187
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2228188
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2228192
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2228193
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 2228194
    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_1

    .line 2228195
    iget v1, p0, Lcom/facebook/messaging/sharerendering/ImageShareDraweeView;->c:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/facebook/messaging/sharerendering/ImageShareDraweeView;->c:I

    .line 2228196
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 2228197
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onMeasure(II)V

    .line 2228198
    return-void
.end method

.method public setImageWidthHint(I)V
    .locals 0

    .prologue
    .line 2228189
    iput p1, p0, Lcom/facebook/messaging/sharerendering/ImageShareDraweeView;->c:I

    .line 2228190
    invoke-virtual {p0}, Lcom/facebook/messaging/sharerendering/ImageShareDraweeView;->requestLayout()V

    .line 2228191
    return-void
.end method
