.class public Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/FEO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/CheckBox;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Lcom/facebook/messaging/event/sending/EventMessageParams;

.field private final h:LX/DgO;

.field public i:LX/FEF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2216048
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2216049
    new-instance v0, LX/FE1;

    invoke-direct {v0, p0}, LX/FE1;-><init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->h:LX/DgO;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    new-instance v0, LX/FEO;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object p0

    check-cast p0, LX/11S;

    invoke-direct {v0, v2, p0}, LX/FEO;-><init>(Landroid/content/Context;LX/11S;)V

    move-object v1, v0

    check-cast v1, LX/FEO;

    iput-object v1, p1, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->a:LX/FEO;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Lcom/facebook/android/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 2216050
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216051
    invoke-static {v0}, Lcom/facebook/messaging/event/sending/EventMessageParams;->h(Lcom/facebook/messaging/event/sending/EventMessageParams;)V

    .line 2216052
    iput-object p1, v0, Lcom/facebook/messaging/event/sending/EventMessageParams;->e:Lcom/facebook/android/maps/model/LatLng;

    .line 2216053
    sget-object v1, LX/Dgb;->PINNED_LOCATION:LX/Dgb;

    iput-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    .line 2216054
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c:Landroid/widget/TextView;

    invoke-static {p0, p1}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->b(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Lcom/facebook/android/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216055
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    if-eqz v0, :cond_0

    .line 2216056
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    iget-object v1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    invoke-virtual {v0, v1}, LX/FEF;->a(Lcom/facebook/messaging/event/sending/EventMessageParams;)V

    .line 2216057
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Lcom/facebook/android/maps/model/LatLng;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 2216058
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    cmpg-double v0, v0, v4

    if-gez v0, :cond_0

    const v0, 0x7f08326c

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2216059
    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    cmpg-double v0, v2, v4

    if-gez v0, :cond_1

    const v0, 0x7f08326e

    :goto_1
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2216060
    const v2, 0x7f08326f

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    iget-wide v4, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x3

    aput-object v0, v3, v1

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2216061
    :cond_0
    const v0, 0x7f08326b

    goto :goto_0

    .line 2216062
    :cond_1
    const v0, 0x7f08326d

    goto :goto_1
.end method

.method public static c(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2216063
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->a:LX/FEO;

    .line 2216064
    iget-object v1, v0, LX/FEO;->a:Landroid/content/Context;

    const v2, 0x7f083270

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object p0, v0, LX/FEO;->b:LX/11S;

    invoke-static {p0, p1}, LX/FEO;->c(LX/11S;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    iget-object p0, v0, LX/FEO;->b:LX/11S;

    invoke-static {p0, p1}, LX/FEO;->d(LX/11S;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2216065
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216066
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2216067
    const-class v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2216068
    if-eqz p1, :cond_0

    .line 2216069
    const-string v0, "group_event_creation_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/event/sending/EventMessageParams;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216070
    :goto_0
    return-void

    .line 2216071
    :cond_0
    new-instance v0, Lcom/facebook/messaging/event/sending/EventMessageParams;

    invoke-direct {v0}, Lcom/facebook/messaging/event/sending/EventMessageParams;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    goto :goto_0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2216072
    instance-of v0, p1, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    if-eqz v0, :cond_0

    .line 2216073
    check-cast p1, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->h:LX/DgO;

    .line 2216074
    iput-object v0, p1, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    .line 2216075
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2adb5f5d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216076
    const v1, 0x7f0304da

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4a19f839    # 2522638.2f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2216077
    const-string v0, "group_event_creation_params"

    iget-object v1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2216078
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216079
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2216080
    const v0, 0x7f0d0e1d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->b:Landroid/widget/TextView;

    .line 2216081
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->b:Landroid/widget/TextView;

    new-instance p1, LX/FE2;

    invoke-direct {p1, p0}, LX/FE2;-><init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2216082
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->b:Landroid/widget/TextView;

    new-instance p1, LX/FE3;

    invoke-direct {p1, p0}, LX/FE3;-><init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2216083
    const v0, 0x7f0d0e23

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c:Landroid/widget/TextView;

    .line 2216084
    const v0, 0x7f0d0e1f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->d:Landroid/widget/CheckBox;

    .line 2216085
    const v0, 0x7f0d0e20

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->e:Landroid/widget/TextView;

    .line 2216086
    const v0, 0x7f0d0e21

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->f:Landroid/widget/TextView;

    .line 2216087
    sget-object v0, LX/FE0;->a:[I

    iget-object p1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216088
    iget-object p2, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    move-object p1, p2

    .line 2216089
    invoke-virtual {p1}, LX/Dgb;->ordinal()I

    move-result p1

    aget v0, v0, p1

    packed-switch v0, :pswitch_data_0

    .line 2216090
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->d:Landroid/widget/CheckBox;

    iget-object p1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216091
    iget-boolean p2, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->b:Z

    move p1, p2

    .line 2216092
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2216093
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->d:Landroid/widget/CheckBox;

    new-instance p1, LX/FE4;

    invoke-direct {p1, p0}, LX/FE4;-><init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2216094
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216095
    iget-object p1, v0, Lcom/facebook/messaging/event/sending/EventMessageParams;->c:Ljava/util/Calendar;

    move-object v0, p1

    .line 2216096
    if-eqz v0, :cond_0

    .line 2216097
    iget-object p1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->e:Landroid/widget/TextView;

    invoke-static {p0, v0}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216098
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216099
    iget-object p1, v0, Lcom/facebook/messaging/event/sending/EventMessageParams;->d:Ljava/util/Calendar;

    move-object v0, p1

    .line 2216100
    if-eqz v0, :cond_1

    .line 2216101
    iget-object p1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->f:Landroid/widget/TextView;

    invoke-static {p0, v0}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216102
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c:Landroid/widget/TextView;

    new-instance p1, LX/FE5;

    invoke-direct {p1, p0}, LX/FE5;-><init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216103
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->e:Landroid/widget/TextView;

    new-instance p1, LX/FE6;

    invoke-direct {p1, p0}, LX/FE6;-><init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216104
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->f:Landroid/widget/TextView;

    new-instance p1, LX/FE7;

    invoke-direct {p1, p0}, LX/FE7;-><init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216105
    return-void

    .line 2216106
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216107
    iget-object p2, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->e:Lcom/facebook/android/maps/model/LatLng;

    move-object p1, p2

    .line 2216108
    invoke-static {p0, p1}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->b(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Lcom/facebook/android/maps/model/LatLng;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2216109
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216110
    iget-object p2, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->f:Lcom/facebook/messaging/location/sending/NearbyPlace;

    move-object p1, p2

    .line 2216111
    iget-object p1, p1, Lcom/facebook/messaging/location/sending/NearbyPlace;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
