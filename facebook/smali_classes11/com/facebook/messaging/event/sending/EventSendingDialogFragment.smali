.class public Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;
.super Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;
.source ""


# instance fields
.field public m:LX/476;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/FEG;

.field public o:Lcom/facebook/messaging/event/sending/EventMessageParams;

.field private p:LX/475;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2216173
    invoke-direct {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;-><init>()V

    .line 2216174
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7bf464f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216175
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2216176
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;

    const-class v1, LX/476;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/476;

    iput-object p1, p0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;->m:LX/476;

    .line 2216177
    const/16 v1, 0x2b

    const v2, 0x5d5c69c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x473a52ff

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216178
    const v1, 0x7f03051a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6921a436

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x656cbbd2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2216179
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onDestroy()V

    .line 2216180
    iget-object v1, p0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;->p:LX/475;

    invoke-virtual {v1}, LX/475;->b()V

    .line 2216181
    const/16 v1, 0x2b

    const v2, 0x52fcc45c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2216182
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;->m:LX/476;

    .line 2216183
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2216184
    invoke-virtual {v0, v1}, LX/476;->a(Landroid/view/View;)LX/475;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;->p:LX/475;

    .line 2216185
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;->p:LX/475;

    invoke-virtual {v0}, LX/475;->a()V

    .line 2216186
    const v0, 0x7f0d0e84

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2216187
    new-instance v1, LX/FED;

    invoke-direct {v1, p0}, LX/FED;-><init>(Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216188
    const v0, 0x7f0d0e86

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2216189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2216190
    new-instance v1, LX/FEE;

    invoke-direct {v1, p0}, LX/FEE;-><init>(Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216191
    new-instance v1, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;-><init>()V

    .line 2216192
    new-instance v2, LX/FEF;

    invoke-direct {v2, p0, v0}, LX/FEF;-><init>(Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;Landroid/view/View;)V

    .line 2216193
    iput-object v2, v1, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    .line 2216194
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d0e87

    const-string v3, "event_message_details_fragment"

    invoke-virtual {v0, v2, v1, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2216195
    return-void
.end method
