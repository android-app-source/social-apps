.class public Lcom/facebook/messaging/event/sending/EventMessageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/event/sending/EventMessageParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Ljava/util/Calendar;

.field public d:Ljava/util/Calendar;

.field public e:Lcom/facebook/android/maps/model/LatLng;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/messaging/location/sending/NearbyPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/Dgb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2216115
    new-instance v0, LX/FEB;

    invoke-direct {v0}, LX/FEB;-><init>()V

    sput-object v0, Lcom/facebook/messaging/event/sending/EventMessageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2216116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216117
    sget-object v0, LX/Dgb;->UNSET:LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    .line 2216118
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2216119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216120
    sget-object v0, LX/Dgb;->UNSET:LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    .line 2216121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->a:Ljava/lang/String;

    .line 2216122
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->b:Z

    .line 2216123
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->c:Ljava/util/Calendar;

    .line 2216124
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->d:Ljava/util/Calendar;

    .line 2216125
    const-class v0, LX/Dgb;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    .line 2216126
    const-class v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->e:Lcom/facebook/android/maps/model/LatLng;

    .line 2216127
    const-class v0, Lcom/facebook/messaging/location/sending/NearbyPlace;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/location/sending/NearbyPlace;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->f:Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2216128
    return-void
.end method

.method public static h(Lcom/facebook/messaging/event/sending/EventMessageParams;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2216129
    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->e:Lcom/facebook/android/maps/model/LatLng;

    .line 2216130
    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->f:Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2216131
    sget-object v0, LX/Dgb;->UNSET:LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    .line 2216132
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2216133
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2216134
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2216135
    iget-boolean v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2216136
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->c:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2216137
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->d:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2216138
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2216139
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->e:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2216140
    iget-object v0, p0, Lcom/facebook/messaging/event/sending/EventMessageParams;->f:Lcom/facebook/messaging/location/sending/NearbyPlace;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2216141
    return-void
.end method
