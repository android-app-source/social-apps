.class public Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public final b:LX/0Uh;

.field private final c:LX/3MV;

.field private final d:LX/3Mw;

.field public final e:LX/3My;

.field private final f:LX/6Po;

.field public final g:LX/0tX;

.field public final h:LX/6hN;

.field private final i:LX/03V;

.field private final j:LX/FFv;

.field private final k:LX/13Q;

.field private final l:LX/1BA;

.field private final m:LX/2Ht;

.field private final n:LX/3Mx;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0SI;

.field private final q:LX/1tL;

.field public final r:LX/3MW;

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/2Oi;

.field private final v:LX/3QK;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Uh;LX/3MV;LX/3Mw;LX/3My;LX/6Po;LX/0tX;LX/6hN;LX/03V;LX/FFv;LX/13Q;LX/1BA;LX/2Ht;LX/3Mx;LX/0Or;LX/0SI;LX/1tL;LX/3MW;LX/0Or;LX/0Or;LX/2Oi;LX/3QK;)V
    .locals 1
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .param p19    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/threads/annotations/IsThreadFetchWithoutUsersEnabledGk;
        .end annotation
    .end param
    .param p20    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/sync/annotations/IsFetchUserCustomTagEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/3MV;",
            "LX/3Mw;",
            "LX/3My;",
            "LX/6Po;",
            "LX/0tX;",
            "LX/6hN;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/FFv;",
            "LX/13Q;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/2Ht;",
            "LX/3Mx;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0SI;",
            "LX/1tL;",
            "LX/3MW;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Oi;",
            "LX/3QK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2234657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2234658
    iput-object p1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a:LX/0SG;

    .line 2234659
    iput-object p2, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b:LX/0Uh;

    .line 2234660
    iput-object p3, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->c:LX/3MV;

    .line 2234661
    iput-object p4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->d:LX/3Mw;

    .line 2234662
    iput-object p5, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->e:LX/3My;

    .line 2234663
    iput-object p6, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    .line 2234664
    iput-object p7, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    .line 2234665
    iput-object p8, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->h:LX/6hN;

    .line 2234666
    iput-object p9, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->i:LX/03V;

    .line 2234667
    iput-object p10, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->j:LX/FFv;

    .line 2234668
    iput-object p11, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->k:LX/13Q;

    .line 2234669
    iput-object p12, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->l:LX/1BA;

    .line 2234670
    iput-object p13, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->m:LX/2Ht;

    .line 2234671
    iput-object p14, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234672
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    .line 2234673
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    .line 2234674
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->q:LX/1tL;

    .line 2234675
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    .line 2234676
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->s:LX/0Or;

    .line 2234677
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->t:LX/0Or;

    .line 2234678
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->u:LX/2Oi;

    .line 2234679
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->v:LX/3QK;

    .line 2234680
    return-void
.end method

.method public static a(Ljava/util/Set;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMessageParams;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/4H3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234608
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2234609
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMessageParams;

    .line 2234610
    new-instance v3, LX/4H3;

    invoke-direct {v3}, LX/4H3;-><init>()V

    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4H3;->a(Ljava/lang/String;)LX/4H3;

    move-result-object v3

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/4H3;->b(Ljava/lang/String;)LX/4H3;

    move-result-object v0

    .line 2234611
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2234612
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234681
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2234682
    invoke-static {}, LX/5bZ;->a()LX/5bY;

    move-result-object v3

    .line 2234683
    const-string v2, "user_fbids"

    invoke-virtual {v3, v2, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2234684
    const-string v2, "exclude_email_addresses"

    invoke-static {v0}, LX/3MW;->a(LX/3MW;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v3, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2234685
    const-string p1, "include_customer_data"

    iget-object v2, v0, LX/3MW;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v3, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2234686
    invoke-virtual {v0, v3}, LX/3MW;->a(LX/0gW;)V

    .line 2234687
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2234688
    iget-object v3, v0, LX/3MW;->b:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 2234689
    iput-object v3, v2, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234690
    iput-object p2, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234691
    move-object v0, v2

    .line 2234692
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2234693
    const v1, 0xcee64b7

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v0}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    .line 2234694
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2234695
    :catch_0
    move-exception v0

    .line 2234696
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234697
    const-string v2, "failed_fetch_user_communication"

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2, v0, v3}, LX/3Mx;->a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 2234698
    throw v0
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;Ljava/util/List;)Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/service/model/FetchThreadListParams;",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;",
            ">;)",
            "Lcom/facebook/messaging/service/model/FetchThreadListResult;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2234699
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234700
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234701
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2234702
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234703
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v1, v1

    .line 2234704
    iget-object v2, v0, LX/3Mx;->a:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "null_thread_list_result_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Thread list returned from server was null"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2234705
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2234706
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->a()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v9

    .line 2234707
    new-instance v1, Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->m()I

    move-result v2

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->n()I

    move-result v3

    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/model/folders/FolderCounts;-><init>(IIJJ)V

    .line 2234708
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->c:LX/3MV;

    invoke-virtual {v0, p3}, LX/3MV;->a(Ljava/util/Collection;)LX/0P1;

    move-result-object v3

    .line 2234709
    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->d:LX/3Mw;

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v2

    if-ge v0, v2, :cond_1

    const/4 v0, 0x1

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v4, v5, v3, v2, v0}, LX/3Mw;->a(LX/0Px;LX/0P1;ZLcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v0

    .line 2234710
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v2

    sget-object v4, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234711
    iput-object v4, v2, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234712
    move-object v2, v2

    .line 2234713
    iget-object v4, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v4, v4

    .line 2234714
    iput-object v4, v2, LX/6iK;->b:LX/6ek;

    .line 2234715
    move-object v2, v2

    .line 2234716
    iput-object v0, v2, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2234717
    move-object v0, v2

    .line 2234718
    invoke-virtual {v3}, LX/0P1;->values()LX/0Py;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2234719
    iput-object v2, v0, LX/6iK;->d:Ljava/util/List;

    .line 2234720
    move-object v0, v0

    .line 2234721
    iput-object v1, v0, LX/6iK;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 2234722
    move-object v0, v0

    .line 2234723
    iput-object v9, v0, LX/6iK;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2234724
    move-object v0, v0

    .line 2234725
    iput-boolean v8, v0, LX/6iK;->i:Z

    .line 2234726
    move-object v0, v0

    .line 2234727
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 2234728
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 2234729
    iput-wide v2, v0, LX/6iK;->j:J

    .line 2234730
    move-object v0, v0

    .line 2234731
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2234732
    iput-wide v2, v0, LX/6iK;->l:J

    .line 2234733
    move-object v0, v0

    .line 2234734
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2234735
    return-object v0

    :cond_1
    move v2, v8

    .line 2234736
    goto :goto_0
.end method

.method private a(Ljava/util/Set;ILjava/lang/Boolean;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;
    .locals 7
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;I",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234613
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;ILjava/lang/Boolean;)LX/5ZA;

    move-result-object v2

    .line 2234614
    const/4 v1, 0x0

    .line 2234615
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b:LX/0Uh;

    const/16 v3, 0x163

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_3

    .line 2234616
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->m:LX/2Ht;

    invoke-virtual {v0, v2}, LX/2Ht;->a(LX/0gW;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2234617
    const-wide/16 v4, 0x1e

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const v6, -0x1b330a46

    invoke-static {v0, v4, v5, v3, v6}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2234618
    :goto_0
    if-nez v0, :cond_0

    .line 2234619
    :try_start_2
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2234620
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2234621
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234622
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234623
    invoke-static {p0, v0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234624
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->c(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2234625
    const v1, -0x79b2da8b

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2234626
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 2234627
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 2234628
    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->d:LX/3Mw;

    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v4, v0, v1}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2234629
    if-eqz v1, :cond_1

    .line 2234630
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 2234631
    :catch_0
    move-exception v0

    .line 2234632
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2234633
    const-string v2, "thread_keys"

    invoke-interface {p1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234634
    if-eqz p4, :cond_2

    .line 2234635
    const-string v2, "caller_context"

    invoke-virtual {p4}, Lcom/facebook/common/callercontext/CallerContext;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234636
    :cond_2
    iget-object v2, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234637
    const-string v3, "failed_fetch_threads_communication"

    invoke-static {v2, v3, v0, v1}, LX/3Mx;->a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 2234638
    throw v0

    .line 2234639
    :catch_1
    move-exception v0

    .line 2234640
    :try_start_3
    const-string v3, "GQLThreadQueryHelper"

    const-string v4, "Failed to fetch thread(s) over mqtt."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v0, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    move-object v0, v1

    goto/16 :goto_0

    .line 2234641
    :cond_4
    return-object v2
.end method

.method public static a(LX/0gW;LX/6ek;)V
    .locals 2

    .prologue
    .line 2234737
    sget-object v0, LX/6ek;->MONTAGE:LX/6ek;

    if-ne p1, v0, :cond_1

    .line 2234738
    const-string v0, "MONTAGE"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2234739
    :goto_0
    move-object v0, v0

    .line 2234740
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2234741
    const-string v1, "folder_tag"

    invoke-virtual {p0, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 2234742
    :cond_0
    return-void

    .line 2234743
    :cond_1
    sget-object v0, LX/6ek;->PENDING:LX/6ek;

    if-ne p1, v0, :cond_2

    .line 2234744
    const-string v0, "PENDING"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2234745
    :cond_2
    sget-object v0, LX/6ek;->OTHER:LX/6ek;

    if-ne p1, v0, :cond_3

    .line 2234746
    const-string v0, "OTHER"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2234747
    :cond_3
    sget-object v0, LX/6ek;->ARCHIVED:LX/6ek;

    if-ne p1, v0, :cond_4

    .line 2234748
    const-string v0, "ARCHIVED"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2234749
    :cond_4
    sget-object v0, LX/6ek;->SPAM:LX/6ek;

    if-ne p1, v0, :cond_5

    .line 2234750
    const-string v0, "SPAM"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2234751
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2234752
    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0gW;)V
    .locals 5

    .prologue
    .line 2234753
    const-string v0, "full_screen_height"

    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->h:LX/6hN;

    invoke-virtual {v1}, LX/6hN;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "full_screen_width"

    iget-object v2, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->h:LX/6hN;

    invoke-virtual {v2}, LX/6hN;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2234754
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->h:LX/6hN;

    invoke-virtual {v0}, LX/6hN;->h()I

    move-result v0

    .line 2234755
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->h:LX/6hN;

    invoke-virtual {v1}, LX/6hN;->g()I

    move-result v1

    .line 2234756
    iget-object v2, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->h:LX/6hN;

    invoke-virtual {v2}, LX/6hN;->f()I

    move-result v2

    .line 2234757
    const-string v3, "small_preview_width"

    mul-int/lit8 v4, v0, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "small_preview_height"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "medium_preview_width"

    mul-int/lit8 v4, v1, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "medium_preview_height"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "large_preview_width"

    mul-int/lit8 v3, v2, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "large_preview_height"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2234758
    return-void
.end method

.method public static a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V
    .locals 3

    .prologue
    .line 2234759
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->q:LX/1tL;

    invoke-virtual {v0}, LX/1tL;->a()Ljava/lang/String;

    move-result-object v0

    .line 2234760
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2234761
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-MSGR-Region"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2234762
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2234763
    iput-object v0, p1, LX/0zO;->f:LX/0Px;

    .line 2234764
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;)Z
    .locals 3

    .prologue
    .line 2234765
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b:LX/0Uh;

    const/16 v1, 0x4e7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private static b(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;
    .locals 8
    .param p1    # Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234766
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    sget-object v1, LX/FDy;->a:LX/6Pr;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fetchMoreMessages (GQL). "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2234767
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v3

    .line 2234768
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2234769
    invoke-static {}, LX/5ZC;->f()LX/5Z4;

    move-result-object v0

    .line 2234770
    const-string v1, "thread_id"

    .line 2234771
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v2

    .line 2234772
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "before_time_ms"

    .line 2234773
    iget-wide v6, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->c:J

    move-wide v4, v6

    .line 2234774
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "msg_count"

    .line 2234775
    iget v3, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->d:I

    move v3, v3

    .line 2234776
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2234777
    invoke-static {p0, v0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0gW;)V

    .line 2234778
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2234779
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2234780
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234781
    iput-object p2, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234782
    invoke-static {p0, v0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234783
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const v1, -0x681caff0

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2234784
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 2234785
    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2234786
    :catch_0
    move-exception v0

    .line 2234787
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234788
    const-string v2, "failed_fetch_more_messages_communication"

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2, v0, v3}, LX/3Mx;->a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 2234789
    throw v0
.end method

.method private static b(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;
    .locals 6
    .param p1    # Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234790
    :try_start_0
    iget-wide v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->c:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 2234791
    iget-object v2, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    sget-object v3, LX/FDy;->a:LX/6Pr;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fetchPinnedThreads (GQL) after time: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2234792
    invoke-static {}, LX/5ZC;->j()LX/5Z6;

    move-result-object v2

    .line 2234793
    const-string v3, "after_time_sec"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "thread_count"

    const/16 v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "include_message_info"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "include_full_user_info"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "exclude_email_addresses"

    invoke-static {p0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2234794
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2234795
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2234796
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234797
    iput-object p2, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234798
    invoke-static {p0, v0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234799
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const v1, 0x16a1ce6e

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2234800
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 2234801
    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2234802
    :catch_0
    move-exception v0

    .line 2234803
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234804
    const-string v2, "failed_fetch_pinned_threads_communication"

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2, v0, v3}, LX/3Mx;->a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 2234805
    throw v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;
    .locals 25

    .prologue
    .line 2234806
    new-instance v2, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v5

    check-cast v5, LX/3MV;

    invoke-static/range {p0 .. p0}, LX/3Mw;->a(LX/0QB;)LX/3Mw;

    move-result-object v6

    check-cast v6, LX/3Mw;

    invoke-static/range {p0 .. p0}, LX/3My;->a(LX/0QB;)LX/3My;

    move-result-object v7

    check-cast v7, LX/3My;

    invoke-static/range {p0 .. p0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v8

    check-cast v8, LX/6Po;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/6hN;->a(LX/0QB;)LX/6hN;

    move-result-object v10

    check-cast v10, LX/6hN;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static/range {p0 .. p0}, LX/FFv;->a(LX/0QB;)LX/FFv;

    move-result-object v12

    check-cast v12, LX/FFv;

    invoke-static/range {p0 .. p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v13

    check-cast v13, LX/13Q;

    invoke-static/range {p0 .. p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v14

    check-cast v14, LX/1BA;

    invoke-static/range {p0 .. p0}, LX/2Ht;->a(LX/0QB;)LX/2Ht;

    move-result-object v15

    check-cast v15, LX/2Ht;

    invoke-static/range {p0 .. p0}, LX/3Mx;->a(LX/0QB;)LX/3Mx;

    move-result-object v16

    check-cast v16, LX/3Mx;

    const/16 v17, 0x12cc

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v18

    check-cast v18, LX/0SI;

    invoke-static/range {p0 .. p0}, LX/1tL;->a(LX/0QB;)LX/1tL;

    move-result-object v19

    check-cast v19, LX/1tL;

    invoke-static/range {p0 .. p0}, LX/3MW;->a(LX/0QB;)LX/3MW;

    move-result-object v20

    check-cast v20, LX/3MW;

    const/16 v21, 0x1532

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    const/16 v22, 0x1530

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    invoke-static/range {p0 .. p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v23

    check-cast v23, LX/2Oi;

    invoke-static/range {p0 .. p0}, LX/3QK;->a(LX/0QB;)LX/3QK;

    move-result-object v24

    check-cast v24, LX/3QK;

    invoke-direct/range {v2 .. v24}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;-><init>(LX/0SG;LX/0Uh;LX/3MV;LX/3Mw;LX/3My;LX/6Po;LX/0tX;LX/6hN;LX/03V;LX/FFv;LX/13Q;LX/1BA;LX/2Ht;LX/3Mx;LX/0Or;LX/0SI;LX/1tL;LX/3MW;LX/0Or;LX/0Or;LX/2Oi;LX/3QK;)V

    .line 2234807
    return-object v2
.end method

.method private c(Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)LX/FOC;
    .locals 11
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadListParams;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/FOC;"
        }
    .end annotation

    .prologue
    .line 2234808
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2234809
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2234810
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2234811
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 2234812
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2234813
    :cond_0
    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v0, v4

    .line 2234814
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2234815
    :catch_0
    move-exception v0

    .line 2234816
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    invoke-virtual {v1, v0}, LX/3Mx;->b(Ljava/lang/Throwable;)V

    .line 2234817
    throw v0

    .line 2234818
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2234819
    iget-object v2, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    sget-object v3, LX/FDy;->a:LX/6Pr;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fetchThreadList (GQL) - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2234820
    new-instance v2, LX/0v6;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ThreadList(s) - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2234821
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2234822
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2234823
    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    move-object v7, v4

    .line 2234824
    invoke-static {}, LX/5ZC;->a()LX/5Z9;

    move-result-object v8

    .line 2234825
    const-string v4, "include_thread_info"

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v4, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v9, "thread_count"

    invoke-virtual {v0}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v9, "include_message_info"

    sget-object v10, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v9, "fetch_users_separately"

    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "include_customer_data"

    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->t:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v9, v10, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v4

    const-string v9, "exclude_email_addresses"

    invoke-static {p0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v4

    const-string v9, "include_booking_requests"

    iget-object v10, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b:LX/0Uh;

    const/16 p1, 0x249

    invoke-virtual {v10, p1, v6}, LX/0Uh;->a(IZ)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v9

    const-string v10, "filter_to_groups"

    sget-object v4, LX/6iH;->STANDARD_GROUP:LX/6iH;

    if-ne v7, v4, :cond_4

    move v4, v5

    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v10, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v9, "filter_to_rooms"

    sget-object v10, LX/6iH;->ROOM:LX/6iH;

    if-ne v7, v10, :cond_5

    :goto_3
    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v9, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2234826
    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    invoke-virtual {v4, v8}, LX/3MW;->a(LX/0gW;)V

    .line 2234827
    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v4, v4

    .line 2234828
    invoke-static {v8, v4}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(LX/0gW;LX/6ek;)V

    .line 2234829
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2234830
    iget-object v5, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v5}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    .line 2234831
    iput-object v5, v4, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234832
    iput-object p2, v4, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234833
    invoke-static {p0, v4}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234834
    move-object v4, v4

    .line 2234835
    invoke-virtual {v2, v4}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2234836
    const-string v6, "actor_id"

    sget-object v7, LX/4Zz;->ALL:LX/4Zz;

    sget-object v8, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {v4, v6, v7, v8}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v4

    .line 2234837
    iget-object v6, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    invoke-virtual {v6, v4, p2}, LX/3MW;->a(LX/4a1;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v4

    .line 2234838
    invoke-virtual {v2, v4}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2234839
    new-instance v6, Landroid/util/Pair;

    invoke-direct {v6, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2234840
    invoke-virtual {v1, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 2234841
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v0, v2}, LX/0tX;->a(LX/0v6;)V

    .line 2234842
    new-instance v3, LX/FOC;

    invoke-direct {v3}, LX/FOC;-><init>()V

    .line 2234843
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2234844
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/common/util/concurrent/ListenableFuture;

    const v5, -0xd4a95a7

    invoke-static {v2, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2234845
    iget-object v5, v2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v5

    .line 2234846
    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    const v5, -0x32014803    # -5.3418384E8f

    invoke-static {v0, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2234847
    iget-object v5, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v5

    .line 2234848
    check-cast v0, Ljava/util/List;

    .line 2234849
    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2234850
    iget-object v6, v3, LX/FOC;->a:Ljava/util/HashMap;

    invoke-virtual {v6, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2234851
    goto :goto_4

    .line 2234852
    :cond_3
    return-object v3

    :cond_4
    move v4, v6

    .line 2234853
    goto/16 :goto_2

    :cond_5
    move v5, v6

    goto/16 :goto_3
.end method

.method private static c(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Landroid/util/Pair;
    .locals 12
    .param p1    # Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2234854
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    sget-object v1, LX/FDy;->a:LX/6Pr;

    const-string v2, "fetchMoreThreads (GQL)"

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2234855
    new-instance v0, LX/0v6;

    const-string v1, "MoreThreads"

    invoke-direct {v0, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2234856
    invoke-static {}, LX/5ZC;->b()LX/5Z5;

    move-result-object v6

    .line 2234857
    const-string v7, "after_time_ms"

    .line 2234858
    iget-wide v10, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->c:J

    move-wide v8, v10

    .line 2234859
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "thread_count"

    .line 2234860
    iget v9, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v9, v9

    .line 2234861
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v8, "include_message_info"

    sget-object v9, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "include_full_user_info"

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "exclude_email_addresses"

    invoke-static {p0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2234862
    iget-object v7, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    invoke-virtual {v7, v6}, LX/3MW;->a(LX/0gW;)V

    .line 2234863
    iget-object v7, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v7, v7

    .line 2234864
    invoke-static {v6, v7}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(LX/0gW;LX/6ek;)V

    .line 2234865
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2234866
    iget-object v7, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v7}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v7

    .line 2234867
    iput-object v7, v6, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234868
    iput-object p2, v6, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234869
    invoke-static {p0, v6}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234870
    move-object v1, v6

    .line 2234871
    invoke-virtual {v0, v1}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2234872
    const-string v3, "actor_id"

    sget-object v4, LX/4Zz;->ALL:LX/4Zz;

    sget-object v5, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {v1, v3, v4, v5}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v1

    .line 2234873
    iget-object v3, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    invoke-virtual {v3, v1, p2}, LX/3MW;->a(LX/4a1;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    .line 2234874
    invoke-virtual {v0, v1}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2234875
    iget-object v3, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0v6;)V

    .line 2234876
    const v0, -0x6ce74d5

    invoke-static {v2, v0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2234877
    iget-object v2, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v2

    .line 2234878
    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;

    .line 2234879
    const v2, 0x2dda9bfa

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v1}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v1

    .line 2234880
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 2234881
    :catch_0
    move-exception v0

    .line 2234882
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    invoke-virtual {v1, v0}, LX/3Mx;->d(Ljava/lang/Throwable;)V

    .line 2234883
    throw v0
.end method

.method private static d(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;
    .locals 3
    .param p1    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMessageParams;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234642
    :try_start_0
    invoke-static {p1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;)LX/0Px;

    move-result-object v0

    .line 2234643
    invoke-static {}, LX/5ZC;->g()LX/5Z3;

    move-result-object v1

    .line 2234644
    const-string v2, "thread_msg_ids"

    invoke-virtual {v1, v2, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2234645
    invoke-static {p0, v1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0gW;)V

    .line 2234646
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2234647
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2234648
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234649
    iput-object p2, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234650
    invoke-static {p0, v0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234651
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->c(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2234652
    const v1, -0x698c8a09

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2234653
    :catch_0
    move-exception v0

    .line 2234654
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234655
    const-string v2, "failed_fetch_messages_communication"

    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2, v0, p0}, LX/3Mx;->a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 2234656
    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;Ljava/util/Set;)LX/0P1;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMessageParams;",
            ">;)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/service/model/FetchMessageResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234387
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 2234388
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;

    .line 2234389
    if-eqz v0, :cond_0

    .line 2234390
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->w()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2234391
    :cond_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 2234392
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMessageParams;

    .line 2234393
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->a:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;

    .line 2234394
    if-nez v1, :cond_2

    .line 2234395
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->k:LX/13Q;

    const-string v5, "gql_fetch_msg_fail"

    invoke-virtual {v1, v5}, LX/13Q;->a(Ljava/lang/String;)V

    .line 2234396
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->l:LX/1BA;

    const v5, 0x310015    # 4.499969E-39f

    invoke-virtual {v1, v5}, LX/1BA;->a(I)V

    .line 2234397
    goto :goto_1

    .line 2234398
    :cond_2
    :try_start_0
    iget-object v5, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2234399
    iget-object v7, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->e:LX/3My;

    iget-object v6, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    invoke-virtual {v7, v5, v1, v6}, LX/3My;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6

    .line 2234400
    new-instance v7, Lcom/facebook/messaging/service/model/FetchMessageResult;

    sget-object v8, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 2234401
    sget-object v9, LX/0SF;->a:LX/0SF;

    move-object v9, v9

    .line 2234402
    invoke-virtual {v9}, LX/0SF;->a()J

    move-result-wide v10

    invoke-direct {v7, v8, v6, v10, v11}, Lcom/facebook/messaging/service/model/FetchMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;J)V

    move-object v1, v7

    .line 2234403
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchMessageParams;->a:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2234404
    :catch_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->i:LX/03V;

    const-string v1, "message_fetch"

    const-string v5, "Failed to convert graphql message model"

    invoke-static {v1, v5}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_1

    .line 2234405
    :cond_3
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Set;ILcom/facebook/common/callercontext/CallerContext;Z)LX/0P1;
    .locals 18
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Z)",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/service/model/FetchThreadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234406
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 2234407
    const/4 v4, 0x0

    .line 2234408
    if-nez p4, :cond_6

    .line 2234409
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->j:LX/FFv;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/FFv;->a(Ljava/util/Set;)LX/0P1;

    move-result-object v4

    .line 2234410
    if-eqz v4, :cond_5

    .line 2234411
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    move-object v7, v4

    move-object v4, v5

    .line 2234412
    :goto_0
    invoke-static {}, LX/0SF;->b()LX/0SF;

    move-result-object v5

    invoke-virtual {v5}, LX/0SF;->a()J

    move-result-wide v10

    .line 2234413
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;ILjava/lang/Boolean;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;

    move-result-object v9

    .line 2234414
    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v12

    .line 2234415
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2234416
    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 2234417
    if-nez v5, :cond_1

    .line 2234418
    if-eqz v7, :cond_0

    .line 2234419
    invoke-virtual {v7, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Px;

    .line 2234420
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    invoke-static {v6, v5}, LX/FFv;->a(Lcom/facebook/user/model/User;LX/0Px;)LX/0Px;

    move-result-object v5

    .line 2234421
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v6

    sget-object v8, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {v6, v8}, LX/6iO;->a(Lcom/facebook/fbservice/results/DataFetchDisposition;)LX/6iO;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/6iO;->a(LX/0Px;)LX/6iO;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v14

    invoke-virtual {v5, v14, v15}, LX/6iO;->a(J)LX/6iO;

    move-result-object v5

    invoke-virtual {v5}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v5

    .line 2234422
    :goto_2
    invoke-virtual {v12, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2234423
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->v:LX/3QK;

    const-string v6, "fetchThreads"

    iget-object v5, v5, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v4, v6, v5}, LX/3QK;->a(Ljava/lang/String;Lcom/facebook/messaging/model/messages/MessagesCollection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2234424
    :catch_0
    move-exception v4

    .line 2234425
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    invoke-virtual {v5, v4}, LX/3Mx;->g(Ljava/lang/Throwable;)V

    .line 2234426
    throw v4

    .line 2234427
    :cond_1
    const/4 v6, 0x0

    .line 2234428
    if-eqz v7, :cond_4

    .line 2234429
    :try_start_1
    invoke-virtual {v7, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Px;

    move-object v8, v6

    .line 2234430
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->d:LX/3Mw;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    invoke-virtual {v14, v4, v5, v6, v8}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;Lcom/facebook/user/model/User;LX/0Px;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v5

    .line 2234431
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 2234432
    invoke-static {}, LX/0SF;->b()LX/0SF;

    move-result-object v6

    invoke-virtual {v6}, LX/0SF;->a()J

    move-result-wide v16

    sub-long v16, v16, v10

    .line 2234433
    if-eqz v7, :cond_2

    if-eqz v8, :cond_2

    .line 2234434
    sget-object v6, LX/6bf;->LOCAL:LX/6bf;

    invoke-virtual {v6}, LX/6bf;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2234435
    const-string v6, "local_duration"

    .line 2234436
    :goto_4
    const-string v15, "fetch_location"

    invoke-interface {v14, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234437
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v14, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234438
    iput-object v14, v5, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    goto :goto_2

    .line 2234439
    :cond_2
    sget-object v6, LX/6bf;->SERVER:LX/6bf;

    invoke-virtual {v6}, LX/6bf;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2234440
    const-string v6, "server_duration"

    goto :goto_4

    .line 2234441
    :cond_3
    invoke-virtual {v12}, LX/0P2;->b()LX/0P1;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    return-object v4

    :cond_4
    move-object v8, v6

    goto :goto_3

    :cond_5
    move-object v7, v4

    move-object v4, v5

    goto/16 :goto_0

    :cond_6
    move-object v7, v4

    move-object v4, v5

    goto/16 :goto_0
.end method

.method public final a(LX/0Px;ILjava/lang/Boolean;)LX/5ZA;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/Boolean;",
            ")",
            "LX/5ZA;"
        }
    .end annotation

    .prologue
    .line 2234442
    new-instance v0, LX/5ZA;

    invoke-direct {v0}, LX/5ZA;-><init>()V

    move-object v1, v0

    .line 2234443
    const-string v0, "thread_ids"

    invoke-virtual {v1, v0, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v0

    const-string v2, "include_message_info"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "msg_count"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "include_customer_data"

    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v2, "exclude_email_addresses"

    invoke-static {p0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v2, "include_booking_requests"

    iget-object v3, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b:LX/0Uh;

    const/16 v4, 0x249

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2234444
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2234445
    const-string v0, "include_full_user_info"

    invoke-virtual {p3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2234446
    :goto_0
    invoke-static {p0, v1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0gW;)V

    .line 2234447
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    invoke-virtual {v0, v1}, LX/3MW;->a(LX/0gW;)V

    .line 2234448
    return-object v1

    .line 2234449
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2234450
    const-string v0, "include_full_user_info"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_0

    .line 2234451
    :cond_1
    const-string v0, "fetch_users_separately"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;ILjava/lang/Boolean;)LX/5ZA;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;I",
            "Ljava/lang/Boolean;",
            ")",
            "LX/5ZA;"
        }
    .end annotation

    .prologue
    .line 2234452
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2234453
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2234454
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2234455
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(LX/0Px;ILjava/lang/Boolean;)LX/5ZA;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;
    .locals 6
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2234456
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    sget-object v1, LX/FDy;->a:LX/6Pr;

    const-string v2, "fetchMoreThreads deprecated (GQL)"

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2234457
    invoke-static {}, LX/5ZC;->b()LX/5Z5;

    move-result-object v0

    .line 2234458
    const-string v1, "after_time_ms"

    .line 2234459
    iget-wide v4, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->c:J

    move-wide v2, v4

    .line 2234460
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "thread_count"

    .line 2234461
    iget v3, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v3, v3

    .line 2234462
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "include_message_info"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "include_full_user_info"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "exclude_email_addresses"

    invoke-static {p0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2234463
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    invoke-virtual {v1, v0}, LX/3MW;->a(LX/0gW;)V

    .line 2234464
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v1, v1

    .line 2234465
    invoke-static {v0, v1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(LX/0gW;LX/6ek;)V

    .line 2234466
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2234467
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2234468
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234469
    iput-object p2, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234470
    invoke-static {p0, v0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234471
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const v1, 0x69ceb8f

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2234472
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 2234473
    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2234474
    return-object v0

    .line 2234475
    :catch_0
    move-exception v0

    .line 2234476
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    invoke-virtual {v1, v0}, LX/3Mx;->d(Ljava/lang/Throwable;)V

    .line 2234477
    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;
    .locals 4
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2234478
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    sget-object v1, LX/FDy;->a:LX/6Pr;

    const-string v2, "fetchThreadList deprecated (GQL)"

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2234479
    invoke-static {}, LX/5ZC;->a()LX/5Z9;

    move-result-object v0

    .line 2234480
    const-string v1, "include_thread_info"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "thread_count"

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "include_message_info"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "include_full_user_info"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "exclude_email_addresses"

    invoke-static {p0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "fetch_users_separately"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2234481
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->r:LX/3MW;

    invoke-virtual {v1, v0}, LX/3MW;->a(LX/0gW;)V

    .line 2234482
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v1, v1

    .line 2234483
    invoke-static {v0, v1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(LX/0gW;LX/6ek;)V

    .line 2234484
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2234485
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->p:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2234486
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2234487
    iput-object p2, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2234488
    invoke-static {p0, v0}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0zO;)V

    .line 2234489
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const v1, 0x7069b7dc

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2234490
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 2234491
    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2234492
    return-object v0

    .line 2234493
    :catch_0
    move-exception v0

    .line 2234494
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    invoke-virtual {v1, v0}, LX/3Mx;->b(Ljava/lang/Throwable;)V

    .line 2234495
    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
    .locals 3
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234496
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;

    move-result-object v0

    .line 2234497
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->d:LX/3Mw;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v1, v2, v0}, LX/3Mw;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2234498
    :catch_0
    move-exception v0

    .line 2234499
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234500
    iget-object v2, v1, LX/3Mx;->a:LX/03V;

    const-string p0, "failed_fetch_pinned_threads"

    invoke-virtual {v2, p0, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2234501
    throw v0
.end method

.method public final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;
    .locals 6

    .prologue
    .line 2234502
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->e:LX/3My;

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v1, p2, v2, v0}, LX/3My;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    .line 2234503
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234504
    sget-object v3, LX/0SF;->a:LX/0SF;

    move-object v3, v3

    .line 2234505
    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v4

    invoke-direct {v1, v2, v0, v4, v5}, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/messages/MessagesCollection;J)V

    return-object v1
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;
    .locals 2
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234506
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->b(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;

    move-result-object v0

    .line 2234507
    :try_start_0
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v1

    .line 2234508
    invoke-virtual {p0, v0, v1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2234509
    :catch_0
    move-exception v0

    .line 2234510
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234511
    iget-object p0, v1, LX/3Mx;->a:LX/03V;

    const-string p1, "failed_fetch_more_messages"

    invoke-virtual {p0, p1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2234512
    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 7
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2234513
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->f:LX/6Po;

    sget-object v1, LX/FDy;->a:LX/6Pr;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fetchThread (GQL). "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2234514
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v3, v3

    .line 2234515
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2234516
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v0, v0

    .line 2234517
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2234518
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Received FetchThreadParams with a legacy thread id: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2234519
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v3, v3

    .line 2234520
    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v0, v1, v3}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234521
    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 2234522
    iget v1, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v1, v1

    .line 2234523
    invoke-virtual {p0, v0, v1, p2, v4}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;ILcom/facebook/common/callercontext/CallerContext;Z)LX/0P1;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2234524
    if-nez v0, :cond_1

    .line 2234525
    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2234526
    iget-wide v4, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2234527
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->u:LX/2Oi;

    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2234528
    if-nez v1, :cond_0

    .line 2234529
    invoke-static {p0, v3, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    move-result-object v3

    .line 2234530
    if-eqz v3, :cond_0

    .line 2234531
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->c:LX/3MV;

    invoke-virtual {v1, v3}, LX/3MV;->a(LX/5Vu;)Lcom/facebook/user/model/User;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2234532
    :cond_0
    if-eqz v1, :cond_1

    .line 2234533
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v0

    sget-object v3, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234534
    iput-object v3, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234535
    move-object v0, v0

    .line 2234536
    iget-object v3, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2234537
    iput-object v1, v0, LX/6iO;->e:LX/0Px;

    .line 2234538
    move-object v0, v0

    .line 2234539
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 2234540
    iput-wide v4, v0, LX/6iO;->g:J

    .line 2234541
    move-object v0, v0

    .line 2234542
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2234543
    :cond_1
    if-nez v0, :cond_2

    .line 2234544
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234545
    iget-object v1, v0, LX/3Mx;->a:LX/03V;

    const-string v3, "fetch_thread_fail"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to fetch thread "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2234546
    sget-object v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2234547
    :cond_2
    return-object v0

    .line 2234548
    :catch_0
    move-exception v0

    .line 2234549
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234550
    iget-object v2, v1, LX/3Mx;->a:LX/03V;

    const-string v3, "failed_fetch_user"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2234551
    throw v0
.end method

.method public final a(Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/HashMap;
    .locals 7
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadListParams;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadListParams;",
            "Lcom/facebook/messaging/service/model/FetchThreadListResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234552
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->c(Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)LX/FOC;

    move-result-object v1

    .line 2234553
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2234554
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2234555
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234556
    iget-object v4, v0, LX/3Mx;->a:LX/03V;

    const-string v5, "null_logged_in_user"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Got a null ViewerContextUser, caller context:"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", LoggedInUser: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->c:LX/0WJ;

    invoke-virtual {v6}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", ViewerContextUserId: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v3, v0, LX/3Mx;->c:LX/0WJ;

    invoke-virtual {v3}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isMaskingCurrentCredentials: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->c:LX/0WJ;

    invoke-virtual {v6}, LX/0WJ;->g()Z

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", hasReportedAuthSyncError: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->c:LX/0WJ;

    invoke-virtual {v6}, LX/0WJ;->h()Z

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isLoggedIn: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->c:LX/0WJ;

    invoke-virtual {v6}, LX/0WJ;->b()Z

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isLoggingOut: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->c:LX/0WJ;

    invoke-virtual {v6}, LX/0WJ;->d()Z

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isAppBackgrounded: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->d:LX/0Uo;

    invoke-virtual {v6}, LX/0Uo;->j()Z

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isUserInApp: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->d:LX/0Uo;

    invoke-virtual {v6}, LX/0Uo;->l()Z

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isAppInitialized: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->d:LX/0Uo;

    invoke-virtual {v6}, LX/0Uo;->h()Z

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isApplicationFirstRunOnInstall: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->d:LX/0Uo;

    .line 2234557
    iget-boolean p2, v6, LX/0Uo;->U:Z

    move v6, p2

    .line 2234558
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isApplicationFirstRunOnUpgrade: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->d:LX/0Uo;

    .line 2234559
    iget-boolean p2, v6, LX/0Uo;->V:Z

    move v6, p2

    .line 2234560
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", isAppStartedInBackground: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, LX/3Mx;->d:LX/0Uo;

    invoke-virtual {v6}, LX/0Uo;->k()LX/03R;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2234561
    :cond_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2234562
    iget-object v4, v1, LX/FOC;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 2234563
    if-nez v4, :cond_4

    .line 2234564
    const/4 v4, 0x0

    .line 2234565
    :goto_2
    move-object v4, v4

    .line 2234566
    iget-object v5, v1, LX/FOC;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .line 2234567
    if-nez v5, :cond_5

    .line 2234568
    const/4 v5, 0x0

    .line 2234569
    :goto_3
    move-object v5, v5

    .line 2234570
    invoke-direct {p0, v0, v4, v5}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;Ljava/util/List;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v4

    .line 2234571
    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2234572
    :catch_0
    move-exception v0

    .line 2234573
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234574
    iget-object v2, v1, LX/3Mx;->a:LX/03V;

    const-string v3, "failed_fetch_thread_list"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2234575
    throw v0

    .line 2234576
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_4
    const-string v1, "Number of threadlist results should be equal to number of threadlist requests."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2234577
    return-object v2

    .line 2234578
    :cond_2
    const/4 v0, 0x0

    goto :goto_4

    .line 2234579
    :cond_3
    iget-object v3, v0, LX/3Mx;->c:LX/0WJ;

    invoke-virtual {v3}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 2234580
    iget-object p2, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, p2

    .line 2234581
    goto/16 :goto_0

    :cond_4
    :try_start_1
    iget-object v4, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;

    goto :goto_2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    goto :goto_3
.end method

.method public final b(Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)LX/0P1;
    .locals 2
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMessageParams;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/service/model/FetchMessageResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234582
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->d(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;

    move-result-object v0

    .line 2234583
    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Collection;Ljava/util/Set;)LX/0P1;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2234584
    :catch_0
    move-exception v0

    .line 2234585
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234586
    iget-object p0, v1, LX/3Mx;->a:LX/03V;

    const-string p1, "failed_fetch_messages"

    invoke-virtual {p0, p1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2234587
    throw v0
.end method

.method public final b(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;
    .locals 9
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2234588
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->c(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Landroid/util/Pair;

    move-result-object v1

    .line 2234589
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;

    .line 2234590
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/Collection;

    .line 2234591
    :try_start_0
    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    const-string v5, "Fetch-more-threads null response"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234592
    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    const-string v5, "Page info missing"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234593
    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->c:LX/3MV;

    invoke-virtual {v4, v1}, LX/3MV;->a(Ljava/util/Collection;)LX/0P1;

    move-result-object v5

    .line 2234594
    iget-object v4, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->d:LX/3Mw;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2234595
    iget v1, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v1, v1

    .line 2234596
    if-ge v0, v1, :cond_0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v4, v6, v5, v1, v0}, LX/3Mw;->a(LX/0Px;LX/0P1;ZLcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v4

    .line 2234597
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234598
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v3, v0

    .line 2234599
    invoke-virtual {v5}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    .line 2234600
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 2234601
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_0
    move v1, v3

    .line 2234602
    goto :goto_0

    .line 2234603
    :catch_0
    move-exception v0

    .line 2234604
    iget-object v1, p0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->n:LX/3Mx;

    .line 2234605
    iget-object v2, v1, LX/3Mx;->a:LX/03V;

    const-string v3, "failed_fetch_more_threads"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2234606
    throw v0
.end method

.method public final b(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 1
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234607
    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    return-object v0
.end method
