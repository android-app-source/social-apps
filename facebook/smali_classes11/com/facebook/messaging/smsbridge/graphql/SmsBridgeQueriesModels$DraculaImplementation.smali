.class public final Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2233579
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2233580
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2233577
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2233578
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2233505
    if-nez p1, :cond_0

    move v0, v1

    .line 2233506
    :goto_0
    return v0

    .line 2233507
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2233508
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2233509
    :sswitch_0
    const-class v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$MatchedSMSThreadsQueryModel$MatchedSmsParticipantsModel$EdgesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2233510
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2233511
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2233512
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2233513
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2233514
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2233515
    const v2, -0x1b8db423

    const/4 v5, 0x0

    .line 2233516
    if-nez v0, :cond_1

    move v3, v5

    .line 2233517
    :goto_1
    move v0, v3

    .line 2233518
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2233519
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2233520
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2233521
    :sswitch_2
    const-class v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$MatchedSMSThreadsQueryModel$MatchedSmsParticipantsModel$EdgesModel$NodeModel$GroupThreadsModel$GroupThreadsEdgesModel$GroupThreadsEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$MatchedSMSThreadsQueryModel$MatchedSmsParticipantsModel$EdgesModel$NodeModel$GroupThreadsModel$GroupThreadsEdgesModel$GroupThreadsEdgesNodeModel;

    .line 2233522
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2233523
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2233524
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2233525
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2233526
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2233527
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2233528
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2233529
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2233530
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2233531
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2233532
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2233533
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2233534
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 2233535
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 2233536
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2233537
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2233538
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 2233539
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2233540
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 2233541
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x1cf7af7f -> :sswitch_1
        -0x1b8db423 -> :sswitch_2
        -0x1e8b50f -> :sswitch_3
        0x7c4597b5 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2233576
    new-instance v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2233572
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2233573
    if-eqz v0, :cond_0

    .line 2233574
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2233575
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2233567
    if-eqz p0, :cond_0

    .line 2233568
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2233569
    if-eq v0, p0, :cond_0

    .line 2233570
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2233571
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2233551
    sparse-switch p2, :sswitch_data_0

    .line 2233552
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2233553
    :sswitch_0
    const-class v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$MatchedSMSThreadsQueryModel$MatchedSmsParticipantsModel$EdgesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2233554
    invoke-static {v0, p3}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 2233555
    :goto_0
    :sswitch_1
    return-void

    .line 2233556
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2233557
    const v1, -0x1b8db423

    .line 2233558
    if-eqz v0, :cond_0

    .line 2233559
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2233560
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2233561
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2233562
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2233563
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2233564
    :cond_0
    goto :goto_0

    .line 2233565
    :sswitch_3
    const-class v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$MatchedSMSThreadsQueryModel$MatchedSmsParticipantsModel$EdgesModel$NodeModel$GroupThreadsModel$GroupThreadsEdgesModel$GroupThreadsEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$MatchedSMSThreadsQueryModel$MatchedSmsParticipantsModel$EdgesModel$NodeModel$GroupThreadsModel$GroupThreadsEdgesModel$GroupThreadsEdgesNodeModel;

    .line 2233566
    invoke-static {v0, p3}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1cf7af7f -> :sswitch_2
        -0x1b8db423 -> :sswitch_3
        -0x1e8b50f -> :sswitch_1
        0x7c4597b5 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2233545
    if-eqz p1, :cond_0

    .line 2233546
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;

    move-result-object v1

    .line 2233547
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;

    .line 2233548
    if-eq v0, v1, :cond_0

    .line 2233549
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2233550
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2233544
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2233542
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2233543
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2233474
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2233475
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2233476
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a:LX/15i;

    .line 2233477
    iput p2, p0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->b:I

    .line 2233478
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2233479
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2233480
    new-instance v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2233481
    iget v0, p0, LX/1vt;->c:I

    .line 2233482
    move v0, v0

    .line 2233483
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2233484
    iget v0, p0, LX/1vt;->c:I

    .line 2233485
    move v0, v0

    .line 2233486
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2233487
    iget v0, p0, LX/1vt;->b:I

    .line 2233488
    move v0, v0

    .line 2233489
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2233490
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2233491
    move-object v0, v0

    .line 2233492
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2233493
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2233494
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2233495
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2233496
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2233497
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2233498
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2233499
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2233500
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2233501
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2233502
    iget v0, p0, LX/1vt;->c:I

    .line 2233503
    move v0, v0

    .line 2233504
    return v0
.end method
