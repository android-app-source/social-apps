.class public final Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2233437
    const-class v0, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel;

    new-instance v1, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2233438
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2233439
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2233440
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2233441
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2233442
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2233443
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2233444
    if-eqz p0, :cond_0

    .line 2233445
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2233446
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2233447
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2233448
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2233449
    check-cast p1, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel$Serializer;->a(Lcom/facebook/messaging/smsbridge/graphql/SmsBridgeMutationsModels$MatchedSMSBridgeThreadsMigrateMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
