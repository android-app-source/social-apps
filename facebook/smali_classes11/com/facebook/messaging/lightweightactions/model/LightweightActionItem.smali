.class public Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:LX/FFq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2217648
    new-instance v0, LX/FFp;

    invoke-direct {v0}, LX/FFp;-><init>()V

    sput-object v0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2217635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2217636
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->a:J

    .line 2217637
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->b:Ljava/lang/String;

    .line 2217638
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->c:I

    .line 2217639
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/FFq;->parseType(Ljava/lang/String;)LX/FFq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->d:LX/FFq;

    .line 2217640
    return-void
.end method

.method public static newBuilder()LX/FFo;
    .locals 1

    .prologue
    .line 2217647
    new-instance v0, LX/FFo;

    invoke-direct {v0}, LX/FFo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2217646
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2217641
    iget-wide v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2217642
    iget-object v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2217643
    iget v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2217644
    iget-object v0, p0, Lcom/facebook/messaging/lightweightactions/model/LightweightActionItem;->d:LX/FFq;

    invoke-virtual {v0}, LX/FFq;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2217645
    return-void
.end method
