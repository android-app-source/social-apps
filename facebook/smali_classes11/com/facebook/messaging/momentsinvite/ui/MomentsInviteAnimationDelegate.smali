.class public Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/FJ7;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1Ad;

.field private final c:LX/1Uo;

.field private final d:Lcom/facebook/drawee/view/GenericDraweeView;

.field private final e:I

.field public f:LX/FJF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2223258
    const-class v0, LX/FJ7;

    const-string v1, "thread_view_module"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/1Uo;Lcom/facebook/drawee/view/GenericDraweeView;I)V
    .locals 0

    .prologue
    .line 2223223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223224
    iput-object p1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->b:LX/1Ad;

    .line 2223225
    iput-object p2, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->c:LX/1Uo;

    .line 2223226
    iput-object p3, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    .line 2223227
    iput p4, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->e:I

    .line 2223228
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2223245
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2223246
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 2223247
    return-void
.end method

.method public final a(FFFFFZF)V
    .locals 2

    .prologue
    .line 2223248
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p4}, Lcom/facebook/drawee/view/GenericDraweeView;->setTranslationX(F)V

    .line 2223249
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p5}, Lcom/facebook/drawee/view/GenericDraweeView;->setTranslationY(F)V

    .line 2223250
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/GenericDraweeView;->setPivotX(F)V

    .line 2223251
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/view/GenericDraweeView;->setPivotY(F)V

    .line 2223252
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p3}, Lcom/facebook/drawee/view/GenericDraweeView;->setScaleX(F)V

    .line 2223253
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p3}, Lcom/facebook/drawee/view/GenericDraweeView;->setScaleY(F)V

    .line 2223254
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    if-eqz p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 2223255
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p7}, Lcom/facebook/drawee/view/GenericDraweeView;->setAlpha(F)V

    .line 2223256
    return-void

    .line 2223257
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 2223240
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/GenericDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2223241
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 2223242
    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 2223243
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/GenericDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2223244
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223229
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->e:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2223230
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    iget-object v2, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->c:LX/1Uo;

    .line 2223231
    iput-object v0, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2223232
    move-object v0, v2

    .line 2223233
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2223234
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->isAbsolute()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p1

    :goto_0
    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    new-instance v1, LX/FJC;

    invoke-direct {v1, p0, p1}, LX/FJC;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2223235
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2223236
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 2223237
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->d:Lcom/facebook/drawee/view/GenericDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setAlpha(F)V

    .line 2223238
    return-void

    .line 2223239
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
