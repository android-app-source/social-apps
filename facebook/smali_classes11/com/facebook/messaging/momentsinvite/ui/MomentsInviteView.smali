.class public Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/FJF;
.implements LX/6ls;


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6lr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FIz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FJT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/6gC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:Landroid/view/View;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/drawee/view/GenericDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private final o:[Landroid/widget/TextView;

.field private p:LX/FJB;

.field private final q:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2223491
    const-class v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2223508
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2223509
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223510
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f:LX/0Ot;

    .line 2223511
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223512
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    .line 2223513
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223514
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->h:LX/0Ot;

    .line 2223515
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    .line 2223516
    new-instance v0, LX/FJM;

    invoke-direct {v0, p0}, LX/FJM;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)V

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->q:Landroid/view/View$OnClickListener;

    .line 2223517
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->b()V

    .line 2223518
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223519
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2223520
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223521
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f:LX/0Ot;

    .line 2223522
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223523
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    .line 2223524
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223525
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->h:LX/0Ot;

    .line 2223526
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    .line 2223527
    new-instance v0, LX/FJM;

    invoke-direct {v0, p0}, LX/FJM;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)V

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->q:Landroid/view/View$OnClickListener;

    .line 2223528
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->b()V

    .line 2223529
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223530
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2223531
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223532
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f:LX/0Ot;

    .line 2223533
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223534
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    .line 2223535
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2223536
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->h:LX/0Ot;

    .line 2223537
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    .line 2223538
    new-instance v0, LX/FJM;

    invoke-direct {v0, p0}, LX/FJM;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)V

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->q:Landroid/view/View$OnClickListener;

    .line 2223539
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->b()V

    .line 2223540
    return-void
.end method

.method private static a(LX/6gC;)LX/0Px;
    .locals 5
    .param p0    # LX/6gC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6gC;",
            ")",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2223541
    if-nez p0, :cond_0

    .line 2223542
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2223543
    :goto_0
    return-object v0

    .line 2223544
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2223545
    iget-object v0, p0, LX/6gC;->c:LX/0Px;

    move-object v3, v0

    .line 2223546
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2223547
    invoke-static {v0, v2}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a(Ljava/lang/String;LX/0Pz;)V

    .line 2223548
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2223549
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;)V
    .locals 1

    .prologue
    .line 2223550
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->e()Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->cV_()Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->cU_()Ljava/lang/String;

    .line 2223551
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2223552
    new-instance v0, LX/FJQ;

    invoke-direct {v0, p0, p2}, LX/FJQ;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2223553
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2223554
    return-void
.end method

.method private static a(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;LX/1Ad;LX/1Uo;Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/6lr;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;",
            "LX/1Ad;",
            "LX/1Uo;",
            "Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;",
            "LX/6lr;",
            "LX/0Ot",
            "<",
            "LX/FIz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FJT;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2223555
    iput-object p1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->b:LX/1Uo;

    iput-object p3, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    iput-object p4, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->d:LX/6lr;

    iput-object p5, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->h:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    invoke-static {v7}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {v7}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v2

    check-cast v2, LX/1Uo;

    new-instance v5, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    invoke-static {v7}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-direct {v5, v3, v4}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;-><init>(LX/1HI;Ljava/util/concurrent/Executor;)V

    move-object v3, v5

    check-cast v3, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    invoke-static {v7}, LX/6lr;->a(LX/0QB;)LX/6lr;

    move-result-object v4

    check-cast v4, LX/6lr;

    const/16 v5, 0x2823

    invoke-static {v7, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2828

    invoke-static {v7, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v8, 0xac0

    invoke-static {v7, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;LX/1Ad;LX/1Uo;Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/6lr;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private static a(Ljava/lang/String;LX/0Pz;)V
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Pz",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2223556
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2223557
    :cond_0
    :goto_0
    return-void

    .line 2223558
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2223559
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2223560
    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2223561
    const-class v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2223562
    const v0, 0x7f030d59

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2223563
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->setOrientation(I)V

    .line 2223564
    invoke-virtual {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x43a00000    # 320.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->j:I

    .line 2223565
    const v0, 0x7f0d20fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->k:Landroid/view/View;

    .line 2223566
    const v0, 0x7f0d2100

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->l:Landroid/widget/TextView;

    .line 2223567
    const v0, 0x7f0d2101

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->m:Landroid/widget/TextView;

    .line 2223568
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    const v0, 0x7f0d2102

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 2223569
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    const v0, 0x7f0d2103

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    .line 2223570
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    const/4 v3, 0x2

    const v0, 0x7f0d2104

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    .line 2223571
    const v0, 0x7f0d20fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/GenericDraweeView;

    const v1, 0x7f0d20ff

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->n:LX/0Px;

    .line 2223572
    invoke-virtual {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a020f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2223573
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2223574
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->n:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->n:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/GenericDraweeView;

    .line 2223575
    new-instance v2, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;

    iget-object v6, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a:LX/1Ad;

    iget-object v7, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->b:LX/1Uo;

    invoke-direct {v2, v6, v7, v0, v3}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;-><init>(LX/1Ad;LX/1Uo;Lcom/facebook/drawee/view/GenericDraweeView;I)V

    .line 2223576
    iput-object p0, v2, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteAnimationDelegate;->f:LX/FJF;

    .line 2223577
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2223578
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2223579
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    .line 2223580
    iput-object p0, v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->d:LX/FJF;

    .line 2223581
    new-instance v0, LX/FJB;

    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/FJB;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/0Px;)V

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->p:LX/FJB;

    .line 2223582
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2223583
    return-void
.end method

.method public static c(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2223584
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    if-nez v1, :cond_0

    .line 2223585
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223586
    iget-object v2, v1, LX/6gC;->h:LX/0Px;

    move-object v1, v2

    .line 2223587
    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->h()LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/0RZ;->a(Ljava/util/Iterator;LX/0Rl;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    goto :goto_0
.end method

.method private c(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2223588
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223589
    iget-object v2, v0, LX/6gC;->c:LX/0Px;

    move-object v0, v2

    .line 2223590
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 2223591
    :goto_0
    return v0

    .line 2223592
    :cond_1
    int-to-double v2, p1

    const-wide v4, 0x3ffe666666666666L    # 1.9

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 2223593
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2223594
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v3, p1, :cond_2

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v3, v2, :cond_2

    move v0, v1

    .line 2223595
    goto :goto_0

    .line 2223596
    :cond_2
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 2223597
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2223598
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2223599
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->p:LX/FJB;

    .line 2223600
    iget-object v1, v0, LX/FJB;->e:Landroid/graphics/PointF;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/FJB;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    int-to-float v3, p1

    cmpl-float v1, v1, v3

    if-nez v1, :cond_3

    iget-object v1, v0, LX/FJB;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    int-to-float v3, v2

    cmpl-float v1, v1, v3

    if-nez v1, :cond_3

    .line 2223601
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2223602
    :cond_3
    new-instance v1, Landroid/graphics/PointF;

    int-to-float v3, p1

    int-to-float v4, v2

    invoke-direct {v1, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, LX/FJB;->e:Landroid/graphics/PointF;

    .line 2223603
    invoke-static {v0}, LX/FJB;->c(LX/FJB;)V

    goto :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2223492
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223493
    iget-object v1, v0, LX/6gC;->c:LX/0Px;

    move-object v0, v1

    .line 2223494
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2223495
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2223496
    :goto_0
    return-void

    .line 2223497
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2223498
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i()V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2223499
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223500
    iget-object v1, v0, LX/6gC;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2223501
    if-nez v0, :cond_1

    .line 2223502
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2223503
    :goto_0
    return-void

    .line 2223504
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2223505
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223506
    iget-object p0, v1, LX/6gC;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2223507
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2223394
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223395
    iget-object v1, v0, LX/6gC;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2223396
    if-nez v0, :cond_1

    .line 2223397
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2223398
    :goto_0
    return-void

    .line 2223399
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2223400
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223401
    iget-object p0, v1, LX/6gC;->b:Ljava/lang/String;

    move-object v1, p0

    .line 2223402
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private g()V
    .locals 7

    .prologue
    .line 2223403
    const/4 v0, 0x0

    .line 2223404
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    if-eqz v1, :cond_2

    .line 2223405
    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223406
    iget-object v2, v1, LX/6gC;->g:LX/0Px;

    move-object v1, v2

    .line 2223407
    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->h()LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0RZ;->b(Ljava/util/Iterator;LX/0Rl;)LX/0Rc;

    move-result-object v1

    move v2, v0

    .line 2223408
    :goto_0
    invoke-virtual {v1}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    array-length v0, v0

    if-ge v2, v0, :cond_0

    .line 2223409
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    aget-object v3, v0, v2

    invoke-virtual {v1}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    invoke-direct {p0, v3, v0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a(Landroid/widget/TextView;Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;)V

    .line 2223410
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2223411
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIz;

    iget-object v3, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FJT;

    invoke-virtual {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/FJT;->a(Landroid/content/Context;)Z

    move-result v1

    .line 2223412
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "moments_invite_impression"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2223413
    invoke-static {v3, v4}, LX/FIz;->a(LX/6gC;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2223414
    const-string v5, "share_id"

    .line 2223415
    iget-object v6, v3, LX/6gC;->f:Ljava/lang/String;

    move-object v6, v6

    .line 2223416
    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223417
    const-string v5, "invite_app_installed"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223418
    const-string v5, "invite_button_count"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223419
    iget-object v5, v0, LX/FIz;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2223420
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    .line 2223421
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->o:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2223422
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2223423
    :cond_1
    return-void

    :cond_2
    move v2, v0

    goto :goto_1
.end method

.method private h()LX/0Rl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rl",
            "<",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetInterfaces$MomentsAppInvitationActionLinkFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2223424
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJT;

    invoke-virtual {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FJT;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/FJN;

    invoke-direct {v0, p0}, LX/FJN;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/FJO;

    invoke-direct {v0, p0}, LX/FJO;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2223425
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->p:LX/FJB;

    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    invoke-static {v1}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->a(LX/6gC;)LX/0Px;

    move-result-object v1

    .line 2223426
    iget-object p0, v0, LX/FJB;->f:LX/0Px;

    invoke-static {p0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2223427
    :goto_0
    return-void

    .line 2223428
    :cond_0
    iput-object v1, v0, LX/FJB;->f:LX/0Px;

    .line 2223429
    invoke-static {v0}, LX/FJB;->c(LX/FJB;)V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 2223430
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->p:LX/FJB;

    .line 2223431
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/FJB;->g:Z

    .line 2223432
    invoke-static {v0}, LX/FJB;->f(LX/FJB;)V

    .line 2223433
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->d:LX/6lr;

    .line 2223434
    iget-object v1, v0, LX/6lr;->a:Ljava/util/WeakHashMap;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2223435
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 2223436
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->d:LX/6lr;

    .line 2223437
    iget-object v1, v0, LX/6lr;->a:Ljava/util/WeakHashMap;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2223438
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->p:LX/FJB;

    .line 2223439
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/FJB;->g:Z

    .line 2223440
    invoke-static {v0}, LX/FJB;->f(LX/FJB;)V

    .line 2223441
    return-void
.end method

.method private setModel(LX/6gC;)V
    .locals 0
    .param p1    # LX/6gC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223442
    iput-object p1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223443
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->e()V

    .line 2223444
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f()V

    .line 2223445
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g()V

    .line 2223446
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->d()V

    .line 2223447
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/Throwable;)V
    .locals 3
    .param p2    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223448
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 2223449
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIz;

    iget-object v1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223450
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "moments_invite_failed_to_load_image"

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2223451
    invoke-static {v1, v2}, LX/FIz;->a(LX/6gC;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2223452
    const-string p0, "image_uri"

    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223453
    const-string p0, "exception"

    invoke-virtual {v2, p0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223454
    iget-object p0, v0, LX/FIz;->a:LX/0Zb;

    invoke-interface {p0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2223455
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x361c8fbd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2223456
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2223457
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->j()V

    .line 2223458
    const/16 v1, 0x2d

    const v2, -0x127c56ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4311f9da

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2223459
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2223460
    invoke-direct {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->k()V

    .line 2223461
    const/16 v1, 0x2d

    const v2, 0x6dbb41db

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2223462
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 2223463
    iget v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->j:I

    invoke-virtual {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getMeasuredWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2223464
    invoke-direct {p0, v0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->c(I)Z

    move-result v1

    .line 2223465
    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getMeasuredWidth()I

    move-result v1

    if-le v1, v0, :cond_1

    .line 2223466
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->measure(II)V

    .line 2223467
    :cond_1
    return-void
.end method

.method public setModelFromShare(Lcom/facebook/messaging/model/share/Share;)V
    .locals 4
    .param p1    # Lcom/facebook/messaging/model/share/Share;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223468
    if-nez p1, :cond_0

    .line 2223469
    const/4 v0, 0x0

    .line 2223470
    :goto_0
    move-object v0, v0

    .line 2223471
    invoke-direct {p0, v0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->setModel(LX/6gC;)V

    .line 2223472
    return-void

    .line 2223473
    :cond_0
    invoke-static {}, LX/6gD;->newBuilder()LX/6gD;

    move-result-object v0

    .line 2223474
    iget-object v1, p1, Lcom/facebook/messaging/model/share/Share;->c:Ljava/lang/String;

    .line 2223475
    iput-object v1, v0, LX/6gD;->a:Ljava/lang/String;

    .line 2223476
    iget-object v1, p1, Lcom/facebook/messaging/model/share/Share;->e:Ljava/lang/String;

    .line 2223477
    iput-object v1, v0, LX/6gD;->b:Ljava/lang/String;

    .line 2223478
    iget-object v1, p1, Lcom/facebook/messaging/model/share/Share;->j:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/share/Share;->k:Ljava/lang/String;

    invoke-static {v1, v2, v3}, LX/FJD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2223479
    invoke-virtual {v0, v1}, LX/6gD;->b(LX/0Px;)LX/6gD;

    .line 2223480
    invoke-virtual {v0, v1}, LX/6gD;->a(LX/0Px;)LX/6gD;

    .line 2223481
    iget-object v1, p1, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    if-eqz v1, :cond_1

    .line 2223482
    iget-object v1, p1, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    .line 2223483
    iget-object v2, v1, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->a:LX/0Px;

    move-object v1, v2

    .line 2223484
    invoke-virtual {v0, v1}, LX/6gD;->a(Ljava/util/List;)LX/6gD;

    .line 2223485
    iget-object v1, p1, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    .line 2223486
    iget-object v2, v1, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2223487
    iput-object v1, v0, LX/6gD;->d:Ljava/lang/String;

    .line 2223488
    :cond_1
    invoke-virtual {v0}, LX/6gD;->i()LX/6gC;

    move-result-object v0

    goto :goto_0
.end method

.method public setModelFromXMA(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)V
    .locals 2
    .param p1    # Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223489
    invoke-virtual {p0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p1, v1, v0}, LX/FJD;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;Landroid/content/res/Resources;LX/0Uh;)LX/6gC;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->setModel(LX/6gC;)V

    .line 2223490
    return-void
.end method
