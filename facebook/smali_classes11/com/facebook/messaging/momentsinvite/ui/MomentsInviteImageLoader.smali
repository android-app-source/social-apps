.class public Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/1HI;

.field public final c:Ljava/util/concurrent/Executor;

.field public d:LX/FJF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2223334
    const-class v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    const-string v1, "thread_view_module"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2223335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223336
    iput-object p1, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->b:LX/1HI;

    .line 2223337
    iput-object p2, p0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->c:Ljava/util/concurrent/Executor;

    .line 2223338
    return-void
.end method
