.class public Lcom/facebook/messaging/localfetch/FetchUserUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2218080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218081
    iput-object p1, p0, Lcom/facebook/messaging/localfetch/FetchUserUtil;->a:LX/0aG;

    .line 2218082
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/localfetch/FetchUserUtil;
    .locals 1

    .prologue
    .line 2218083
    invoke-static {p0}, Lcom/facebook/messaging/localfetch/FetchUserUtil;->b(LX/0QB;)Lcom/facebook/messaging/localfetch/FetchUserUtil;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/localfetch/FetchUserUtil;Ljava/util/List;)LX/1MF;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/1MF;"
        }
    .end annotation

    .prologue
    .line 2218076
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2218077
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadUsersParams;

    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-direct {v0, v1, p1}, Lcom/facebook/messaging/service/model/FetchThreadUsersParams;-><init>(LX/0rS;Ljava/util/List;)V

    .line 2218078
    const-string v1, "FetchThreadUsersParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2218079
    iget-object v0, p0, Lcom/facebook/messaging/localfetch/FetchUserUtil;->a:LX/0aG;

    const-string v1, "fetch_users"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/localfetch/FetchUserUtil;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x66a9e619

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/localfetch/FetchUserUtil;
    .locals 2

    .prologue
    .line 2218074
    new-instance v1, Lcom/facebook/messaging/localfetch/FetchUserUtil;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/localfetch/FetchUserUtil;-><init>(LX/0aG;)V

    .line 2218075
    return-object v1
.end method
