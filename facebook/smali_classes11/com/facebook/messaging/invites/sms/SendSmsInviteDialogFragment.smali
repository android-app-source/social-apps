.class public Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# instance fields
.field public m:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FFZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2217499
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 2217500
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    iput-object p0, p1, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->m:LX/0Zb;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2217501
    const/4 v2, 0x0

    .line 2217502
    const v0, 0x7f080679

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2217503
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f080567

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2217504
    iget-object v0, p0, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/FFZ;

    .line 2217505
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.messenger.ACTION_SMS_INVITE_SENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2217506
    const-string v1, "com.facebook.messenger.EXTRA_SMS_ADDRESS"

    iget-object v4, v6, LX/FFZ;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2217507
    const-string v1, "com.facebook.messenger.EXTRA_SMS_BODY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2217508
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v1, v4, v0, v5}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 2217509
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 2217510
    iget-object v1, v6, LX/FFZ;->b:Ljava/lang/String;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 2217511
    iget-object v0, v6, LX/FFZ;->a:Lcom/facebook/user/model/User;

    .line 2217512
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2217513
    iget-object v1, v6, LX/FFZ;->b:Ljava/lang/String;

    .line 2217514
    iget-object v4, p0, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->m:LX/0Zb;

    const-string v5, "invite_via_sms_dialog_accept"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2217515
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2217516
    const-string v5, "SendSmsInviteDialogFragment"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2217517
    const-string v5, "invite_via_sms_user_id"

    invoke-virtual {v4, v5, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2217518
    const-string v5, "invite_via_sms_user_mobile"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2217519
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2217520
    :cond_0
    goto :goto_0

    .line 2217521
    :cond_1
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 2217522
    iget-object v0, p0, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->m:LX/0Zb;

    const-string v1, "invite_via_sms_dialog_cancel"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2217523
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2217524
    const-string v1, "SendSmsInviteDialogFragment"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2217525
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2217526
    :cond_0
    invoke-super {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->k()V

    .line 2217527
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x6e212e6f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2217528
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2217529
    const-class v0, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2217530
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2217531
    const-string v1, "user_to_invite_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2217532
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->n:Ljava/util/List;

    .line 2217533
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2217534
    invoke-static {v0}, LX/FFa;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v5

    .line 2217535
    if-eqz v5, :cond_0

    .line 2217536
    iget-object v6, p0, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->n:Ljava/util/List;

    new-instance v7, LX/FFZ;

    .line 2217537
    iget-object v8, v5, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v8, v8

    .line 2217538
    iget-object p1, v5, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    move-object v5, p1

    .line 2217539
    invoke-direct {v7, p0, v0, v8, v5}, LX/FFZ;-><init>(Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;Lcom/facebook/user/model/User;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2217540
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2217541
    :cond_1
    const/4 v6, 0x0

    .line 2217542
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2217543
    iget-object v0, p0, Lcom/facebook/messaging/invites/sms/SendSmsInviteDialogFragment;->n:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFZ;

    .line 2217544
    iget-object v1, v0, LX/FFZ;->a:Lcom/facebook/user/model/User;

    .line 2217545
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v1

    .line 2217546
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2217547
    iget-object v0, v0, LX/FFZ;->c:Ljava/lang/String;

    .line 2217548
    :goto_1
    new-instance v1, LX/6dy;

    const v4, 0x7f080675

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f080677

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v4, 0x7f080676

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2217549
    iput-object v0, v1, LX/6dy;->d:Ljava/lang/String;

    .line 2217550
    move-object v0, v1

    .line 2217551
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    move-object v0, v0

    .line 2217552
    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 2217553
    const v0, -0x6f351ee7

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
