.class public final Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2236099
    const-class v0, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;

    new-instance v1, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2236100
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2236101
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2236102
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2236103
    const/4 v2, 0x0

    .line 2236104
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2236105
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236106
    :goto_0
    move v1, v2

    .line 2236107
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2236108
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2236109
    new-instance v1, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;-><init>()V

    .line 2236110
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2236111
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2236112
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2236113
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2236114
    :cond_0
    return-object v1

    .line 2236115
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236116
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2236117
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2236118
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2236119
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2236120
    const-string v4, "message_threads"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2236121
    const/4 v3, 0x0

    .line 2236122
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2236123
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236124
    :goto_2
    move v1, v3

    .line 2236125
    goto :goto_1

    .line 2236126
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2236127
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2236128
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2236129
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236130
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2236131
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2236132
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2236133
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2236134
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2236135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2236136
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2236137
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2236138
    const/4 v5, 0x0

    .line 2236139
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_f

    .line 2236140
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236141
    :goto_5
    move v4, v5

    .line 2236142
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2236143
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2236144
    goto :goto_3

    .line 2236145
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2236146
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2236147
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 2236148
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236149
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_e

    .line 2236150
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2236151
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2236152
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_b

    if-eqz v8, :cond_b

    .line 2236153
    const-string v9, "former_participants"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 2236154
    const/4 v8, 0x0

    .line 2236155
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_14

    .line 2236156
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236157
    :goto_7
    move v7, v8

    .line 2236158
    goto :goto_6

    .line 2236159
    :cond_c
    const-string v9, "other_participants"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 2236160
    const/4 v8, 0x0

    .line 2236161
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_19

    .line 2236162
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236163
    :goto_8
    move v6, v8

    .line 2236164
    goto :goto_6

    .line 2236165
    :cond_d
    const-string v9, "updated_time_precise"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 2236166
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_6

    .line 2236167
    :cond_e
    const/4 v8, 0x3

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2236168
    invoke-virtual {v0, v5, v7}, LX/186;->b(II)V

    .line 2236169
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 2236170
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2236171
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_5

    :cond_f
    move v4, v5

    move v6, v5

    move v7, v5

    goto :goto_6

    .line 2236172
    :cond_10
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236173
    :cond_11
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_13

    .line 2236174
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2236175
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2236176
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_11

    if-eqz v9, :cond_11

    .line 2236177
    const-string p0, "nodes"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 2236178
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2236179
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, p0, :cond_12

    .line 2236180
    :goto_a
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, p0, :cond_12

    .line 2236181
    invoke-static {p1, v0}, LX/FOt;->b(LX/15w;LX/186;)I

    move-result v9

    .line 2236182
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 2236183
    :cond_12
    invoke-static {v7, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 2236184
    goto :goto_9

    .line 2236185
    :cond_13
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2236186
    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2236187
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_7

    :cond_14
    move v7, v8

    goto :goto_9

    .line 2236188
    :cond_15
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2236189
    :cond_16
    :goto_b
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_18

    .line 2236190
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2236191
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2236192
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_16

    if-eqz v9, :cond_16

    .line 2236193
    const-string p0, "nodes"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_15

    .line 2236194
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2236195
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, p0, :cond_17

    .line 2236196
    :goto_c
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, p0, :cond_17

    .line 2236197
    invoke-static {p1, v0}, LX/FOu;->b(LX/15w;LX/186;)I

    move-result v9

    .line 2236198
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 2236199
    :cond_17
    invoke-static {v6, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 2236200
    goto :goto_b

    .line 2236201
    :cond_18
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2236202
    invoke-virtual {v0, v8, v6}, LX/186;->b(II)V

    .line 2236203
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_8

    :cond_19
    move v6, v8

    goto :goto_b
.end method
