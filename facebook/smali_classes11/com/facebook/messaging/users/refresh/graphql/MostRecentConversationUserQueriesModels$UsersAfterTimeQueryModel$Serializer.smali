.class public final Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2236286
    const-class v0, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;

    new-instance v1, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2236287
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2236288
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2236289
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2236290
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2236291
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2236292
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2236293
    if-eqz v2, :cond_9

    .line 2236294
    const-string v3, "message_threads"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2236295
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2236296
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2236297
    if-eqz v3, :cond_8

    .line 2236298
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2236299
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2236300
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_7

    .line 2236301
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2236302
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2236303
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2236304
    if-eqz p0, :cond_2

    .line 2236305
    const-string v0, "former_participants"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2236306
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2236307
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2236308
    if-eqz v0, :cond_1

    .line 2236309
    const-string v2, "nodes"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2236310
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2236311
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v0}, LX/15i;->c(I)I

    move-result p0

    if-ge v2, p0, :cond_0

    .line 2236312
    invoke-virtual {v1, v0, v2}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/FOt;->a(LX/15i;ILX/0nX;)V

    .line 2236313
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2236314
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2236315
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2236316
    :cond_2
    const/4 p0, 0x1

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2236317
    if-eqz p0, :cond_5

    .line 2236318
    const-string v0, "other_participants"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2236319
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2236320
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2236321
    if-eqz v0, :cond_4

    .line 2236322
    const-string v2, "nodes"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2236323
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2236324
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1, v0}, LX/15i;->c(I)I

    move-result p0

    if-ge v2, p0, :cond_3

    .line 2236325
    invoke-virtual {v1, v0, v2}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/FOu;->a(LX/15i;ILX/0nX;)V

    .line 2236326
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2236327
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2236328
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2236329
    :cond_5
    const/4 p0, 0x2

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2236330
    if-eqz p0, :cond_6

    .line 2236331
    const-string v0, "updated_time_precise"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2236332
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2236333
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2236334
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 2236335
    :cond_7
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2236336
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2236337
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2236338
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2236339
    check-cast p1, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel$Serializer;->a(Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
