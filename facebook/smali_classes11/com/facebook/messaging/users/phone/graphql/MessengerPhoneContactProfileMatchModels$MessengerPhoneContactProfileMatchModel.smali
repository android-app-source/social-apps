.class public final Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5d748434
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2235713
    const-class v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2235712
    const-class v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2235710
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2235711
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2235704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2235705
    invoke-virtual {p0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2235706
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2235707
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2235708
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2235709
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2235714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2235715
    invoke-virtual {p0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2235716
    invoke-virtual {p0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    .line 2235717
    invoke-virtual {p0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2235718
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;

    .line 2235719
    iput-object v0, v1, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->e:Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    .line 2235720
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2235721
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileMatches"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2235702
    iget-object v0, p0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->e:Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    iput-object v0, p0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->e:Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    .line 2235703
    iget-object v0, p0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->e:Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2235699
    new-instance v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;

    invoke-direct {v0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;-><init>()V

    .line 2235700
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2235701
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2235698
    const v0, 0x15360753

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2235697
    const v0, 0x4833519a

    return v0
.end method
