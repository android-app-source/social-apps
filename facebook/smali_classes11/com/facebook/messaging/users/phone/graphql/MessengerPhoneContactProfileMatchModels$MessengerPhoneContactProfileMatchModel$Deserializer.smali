.class public final Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2235463
    const-class v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;

    new-instance v1, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2235464
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2235465
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2235466
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2235467
    invoke-static {p1, v0}, LX/FOm;->a(LX/15w;LX/186;)I

    move-result v1

    .line 2235468
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2235469
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2235470
    new-instance v1, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;

    invoke-direct {v1}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;-><init>()V

    .line 2235471
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2235472
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2235473
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2235474
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2235475
    :cond_0
    return-object v1
.end method
