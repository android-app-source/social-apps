.class public final Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:I

.field public final synthetic d:Z

.field public final synthetic e:LX/FOd;


# direct methods
.method public constructor <init>(LX/FOd;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 2235414
    iput-object p1, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->e:LX/FOd;

    iput-object p2, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->b:Ljava/lang/String;

    iput p4, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->c:I

    iput-boolean p5, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2235415
    iget-object v0, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->e:LX/FOd;

    iget-object v0, v0, LX/FOd;->f:LX/3MQ;

    iget-object v1, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->b:Ljava/lang/String;

    iget v3, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->c:I

    .line 2235416
    iget-object v4, v0, LX/3MQ;->d:LX/3Lx;

    invoke-virtual {v4, v1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2235417
    const/4 v5, 0x2

    new-array v5, v5, [LX/0ux;

    const/4 v6, 0x0

    sget-object v7, LX/3MR;->a:LX/0U1;

    .line 2235418
    iget-object v1, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v1

    .line 2235419
    invoke-static {v7, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    aput-object v4, v5, v6

    const/4 v4, 0x1

    sget-object v6, LX/3MR;->c:LX/0U1;

    .line 2235420
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2235421
    invoke-static {v6, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v5}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 2235422
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2235423
    sget-object v6, LX/3MR;->b:LX/0U1;

    .line 2235424
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2235425
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2235426
    sget-object v6, LX/3MR;->c:LX/0U1;

    .line 2235427
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2235428
    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2235429
    invoke-static {v0, v4, v5}, LX/3MQ;->a(LX/3MQ;LX/0ux;Landroid/content/ContentValues;)V

    .line 2235430
    iget-boolean v0, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->d:Z

    if-eqz v0, :cond_0

    .line 2235431
    iget-object v0, p0, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;->e:LX/FOd;

    iget-object v0, v0, LX/FOd;->c:LX/0Xl;

    sget-object v1, LX/0aY;->D:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 2235432
    :cond_0
    return-void
.end method
