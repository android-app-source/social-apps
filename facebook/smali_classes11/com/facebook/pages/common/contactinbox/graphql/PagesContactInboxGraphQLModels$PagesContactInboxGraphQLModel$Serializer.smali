.class public final Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2436777
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    new-instance v1, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2436778
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2436779
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2436780
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2436781
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2436782
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2436783
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2436784
    if-eqz v2, :cond_6

    .line 2436785
    const-string v3, "admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2436786
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2436787
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2436788
    if-eqz v3, :cond_5

    .line 2436789
    const-string v4, "page_contact_us_leads"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2436790
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2436791
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2436792
    if-eqz v4, :cond_1

    .line 2436793
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2436794
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2436795
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v4}, LX/15i;->c(I)I

    move-result v2

    if-ge p0, v2, :cond_0

    .line 2436796
    invoke-virtual {v1, v4, p0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/HBE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2436797
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2436798
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2436799
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2436800
    if-eqz v4, :cond_4

    .line 2436801
    const-string p0, "page_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2436802
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2436803
    const/4 p0, 0x0

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2436804
    if-eqz p0, :cond_2

    .line 2436805
    const-string v2, "end_cursor"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2436806
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2436807
    :cond_2
    const/4 p0, 0x1

    invoke-virtual {v1, v4, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2436808
    if-eqz p0, :cond_3

    .line 2436809
    const-string v2, "has_next_page"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2436810
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2436811
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2436812
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2436813
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2436814
    :cond_6
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2436815
    if-eqz v2, :cond_7

    .line 2436816
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2436817
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2436818
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2436819
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2436820
    check-cast p1, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel$Serializer;->a(Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
