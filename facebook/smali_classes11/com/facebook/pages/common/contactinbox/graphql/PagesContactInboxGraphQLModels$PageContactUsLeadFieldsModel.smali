.class public final Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x41081815
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2436689
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2436688
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2436686
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2436687
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 2436666
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2436667
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2436668
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2436669
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2436670
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2436671
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 2436672
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x6fb1e5d8

    invoke-static {v2, v1, v3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2436673
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2436674
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2436675
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2436676
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->f:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2436677
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2436678
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->h:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2436679
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2436680
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2436681
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2436682
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2436683
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2436684
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2436685
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2436656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2436657
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2436658
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6fb1e5d8

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2436659
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2436660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    .line 2436661
    iput v3, v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l:I

    .line 2436662
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2436663
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2436664
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2436665
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436655
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2436650
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2436651
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->f:J

    .line 2436652
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->h:J

    .line 2436653
    const/4 v0, 0x7

    const v1, 0x6fb1e5d8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l:I

    .line 2436654
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2436647
    new-instance v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;-><init>()V

    .line 2436648
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2436649
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2436628
    const v0, 0x5076c8a4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2436690
    const v0, 0x48da08eb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436645
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->e:Ljava/lang/String;

    .line 2436646
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 2436643
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2436644
    iget-wide v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->f:J

    return-wide v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436641
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->g:Ljava/lang/String;

    .line 2436642
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()J
    .locals 2

    .prologue
    .line 2436639
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2436640
    iget-wide v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->h:J

    return-wide v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436637
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->i:Ljava/lang/String;

    .line 2436638
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436635
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->j:Ljava/lang/String;

    .line 2436636
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436633
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->k:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->k:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    .line 2436634
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->k:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    return-object v0
.end method

.method public final q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoneNumber"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436631
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2436632
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436629
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m:Ljava/lang/String;

    .line 2436630
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m:Ljava/lang/String;

    return-object v0
.end method
