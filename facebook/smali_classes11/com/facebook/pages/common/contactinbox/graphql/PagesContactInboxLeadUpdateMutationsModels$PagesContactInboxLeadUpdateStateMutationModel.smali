.class public final Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xe5b76cb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2437098
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2437074
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2437096
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2437097
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2437090
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2437091
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->a()Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2437092
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2437093
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2437094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2437095
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2437082
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2437083
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->a()Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2437084
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->a()Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    .line 2437085
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->a()Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2437086
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;

    .line 2437087
    iput-object v0, v1, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->e:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    .line 2437088
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2437089
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2437080
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->e:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->e:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    .line 2437081
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->e:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2437077
    new-instance v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;-><init>()V

    .line 2437078
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2437079
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2437076
    const v0, 0x33512137

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2437075
    const v0, 0x632a88b1

    return v0
.end method
