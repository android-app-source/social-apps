.class public final Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3f256119
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2437057
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2437056
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2437054
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2437055
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2437040
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->e:Ljava/lang/String;

    .line 2437041
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2437046
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2437047
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2437048
    invoke-virtual {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->j()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2437049
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2437050
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2437051
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2437052
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2437053
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2437058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2437059
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2437060
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2437045
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2437042
    new-instance v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;-><init>()V

    .line 2437043
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2437044
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2437039
    const v0, 0x522fd1b4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2437038
    const v0, 0x48da08eb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2437036
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->f:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->f:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    .line 2437037
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->f:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    return-object v0
.end method
