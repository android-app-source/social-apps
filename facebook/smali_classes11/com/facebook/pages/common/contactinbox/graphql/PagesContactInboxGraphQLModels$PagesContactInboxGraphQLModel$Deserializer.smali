.class public final Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2436691
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    new-instance v1, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2436692
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2436774
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2436693
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2436694
    const/4 v2, 0x0

    .line 2436695
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2436696
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2436697
    :goto_0
    move v1, v2

    .line 2436698
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2436699
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2436700
    new-instance v1, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;-><init>()V

    .line 2436701
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2436702
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2436703
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2436704
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2436705
    :cond_0
    return-object v1

    .line 2436706
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2436707
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2436708
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2436709
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2436710
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 2436711
    const-string v5, "admin_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2436712
    const/4 v4, 0x0

    .line 2436713
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_9

    .line 2436714
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2436715
    :goto_2
    move v3, v4

    .line 2436716
    goto :goto_1

    .line 2436717
    :cond_3
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2436718
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2436719
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2436720
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2436721
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2436722
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 2436723
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2436724
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2436725
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2436726
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2436727
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 2436728
    const-string v6, "page_contact_us_leads"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2436729
    const/4 v5, 0x0

    .line 2436730
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_f

    .line 2436731
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2436732
    :goto_4
    move v3, v5

    .line 2436733
    goto :goto_3

    .line 2436734
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2436735
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 2436736
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_9
    move v3, v4

    goto :goto_3

    .line 2436737
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2436738
    :cond_b
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_e

    .line 2436739
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2436740
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2436741
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_b

    if-eqz v7, :cond_b

    .line 2436742
    const-string v8, "nodes"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 2436743
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2436744
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_c

    .line 2436745
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_c

    .line 2436746
    invoke-static {p1, v0}, LX/HBE;->b(LX/15w;LX/186;)I

    move-result v7

    .line 2436747
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2436748
    :cond_c
    invoke-static {v6, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 2436749
    goto :goto_5

    .line 2436750
    :cond_d
    const-string v8, "page_info"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2436751
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2436752
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v9, :cond_15

    .line 2436753
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2436754
    :goto_7
    move v3, v7

    .line 2436755
    goto :goto_5

    .line 2436756
    :cond_e
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2436757
    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 2436758
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v3}, LX/186;->b(II)V

    .line 2436759
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_4

    :cond_f
    move v3, v5

    move v6, v5

    goto :goto_5

    .line 2436760
    :cond_10
    const-string p0, "has_next_page"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 2436761
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v3

    move v9, v3

    move v3, v8

    .line 2436762
    :cond_11
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_13

    .line 2436763
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2436764
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2436765
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_11

    if-eqz v11, :cond_11

    .line 2436766
    const-string p0, "end_cursor"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_10

    .line 2436767
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_8

    .line 2436768
    :cond_12
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_8

    .line 2436769
    :cond_13
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2436770
    invoke-virtual {v0, v7, v10}, LX/186;->b(II)V

    .line 2436771
    if-eqz v3, :cond_14

    .line 2436772
    invoke-virtual {v0, v8, v9}, LX/186;->a(IZ)V

    .line 2436773
    :cond_14
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto :goto_7

    :cond_15
    move v3, v7

    move v9, v7

    move v10, v7

    goto :goto_8
.end method
