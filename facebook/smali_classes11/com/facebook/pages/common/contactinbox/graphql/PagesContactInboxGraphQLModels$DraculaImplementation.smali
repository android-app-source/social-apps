.class public final Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2436551
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2436552
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2436553
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2436554
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2436555
    if-nez p1, :cond_0

    .line 2436556
    :goto_0
    return v0

    .line 2436557
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2436558
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2436559
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2436560
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2436561
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2436562
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2436563
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2436564
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2436565
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2436566
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2436567
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2436568
    const v2, 0x53c76846

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2436569
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2436570
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2436571
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2436572
    :sswitch_2
    const-class v1, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 2436573
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2436574
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2436575
    const v3, 0x1e01ecd1

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2436576
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2436577
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2436578
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2436579
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2436580
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2436581
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2436582
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 2436583
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2436584
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2436585
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 2436586
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x112519ec -> :sswitch_1
        0x1e01ecd1 -> :sswitch_3
        0x53c76846 -> :sswitch_2
        0x6fb1e5d8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2436587
    new-instance v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2436604
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2436605
    if-eqz v0, :cond_0

    .line 2436606
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2436607
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2436588
    sparse-switch p2, :sswitch_data_0

    .line 2436589
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2436590
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2436591
    const v1, 0x53c76846

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2436592
    :goto_0
    :sswitch_1
    return-void

    .line 2436593
    :sswitch_2
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2436594
    invoke-static {v0, p3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 2436595
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2436596
    const v1, 0x1e01ecd1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x112519ec -> :sswitch_0
        0x1e01ecd1 -> :sswitch_1
        0x53c76846 -> :sswitch_2
        0x6fb1e5d8 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2436597
    if-eqz p1, :cond_0

    .line 2436598
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2436599
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;

    .line 2436600
    if-eq v0, v1, :cond_0

    .line 2436601
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2436602
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2436603
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2436549
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2436550
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2436518
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2436519
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2436520
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2436521
    iput p2, p0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->b:I

    .line 2436522
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2436548
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2436523
    new-instance v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2436524
    iget v0, p0, LX/1vt;->c:I

    .line 2436525
    move v0, v0

    .line 2436526
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2436527
    iget v0, p0, LX/1vt;->c:I

    .line 2436528
    move v0, v0

    .line 2436529
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2436530
    iget v0, p0, LX/1vt;->b:I

    .line 2436531
    move v0, v0

    .line 2436532
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436533
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2436534
    move-object v0, v0

    .line 2436535
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2436536
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2436537
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2436538
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2436539
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2436540
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2436541
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2436542
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2436543
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2436544
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2436545
    iget v0, p0, LX/1vt;->c:I

    .line 2436546
    move v0, v0

    .line 2436547
    return v0
.end method
