.class public Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;
.super Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HBP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HBN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private final n:Landroid/view/View$OnClickListener;

.field private final o:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2436433
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;-><init>()V

    .line 2436434
    new-instance v0, LX/HAv;

    invoke-direct {v0, p0}, LX/HAv;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->n:Landroid/view/View$OnClickListener;

    .line 2436435
    new-instance v0, LX/HAw;

    invoke-direct {v0, p0}, LX/HAw;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->o:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LX/HB4;Ljava/lang/String;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2436423
    sget-object v0, LX/HB3;->a:[I

    invoke-virtual {p3}, LX/HB4;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2436424
    const/4 v0, 0x0

    .line 2436425
    :goto_0
    return-object v0

    .line 2436426
    :pswitch_0
    const v0, 0x7f020850

    move v1, v0

    .line 2436427
    :goto_1
    const v0, 0x7f030ec8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2436428
    const v0, 0x7f0d2418

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2436429
    const v0, 0x7f0d2419

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    .line 2436430
    goto :goto_0

    .line 2436431
    :pswitch_1
    const v0, 0x7f020956

    move v1, v0

    .line 2436432
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 2436413
    const v0, 0x7f0d241a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomRelativeLayout;

    .line 2436414
    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->c:LX/HBP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m()J

    move-result-wide v4

    mul-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, LX/HBP;->a(Landroid/content/Context;J)I

    move-result v2

    .line 2436415
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0213c6

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 2436416
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, v3, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 2436417
    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2436418
    const v1, 0x7f0d241b

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v1, v3}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2436419
    const v1, 0x7f0d241c

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->c:LX/HBP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m()J

    move-result-wide v4

    mul-long/2addr v4, v6

    invoke-virtual {v2, v3, v4, v5}, LX/HBP;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436420
    const v1, 0x7f0d241d

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, LX/HB0;

    invoke-direct {v2, p0, v0}, LX/HB0;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;Lcom/facebook/widget/CustomRelativeLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2436421
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->setVisibility(I)V

    .line 2436422
    return-void
.end method

.method public static b(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v6, 0x0

    .line 2436336
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2436337
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2436338
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083699

    new-array v4, v12, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08369a

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->l:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->b:LX/11S;

    sget-object v6, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    iget-object v7, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v7}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->k()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-interface {v5, v6, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v12

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08369b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08369b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->m:Ljava/lang/String;

    .line 2436339
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->m:Ljava/lang/String;

    return-object v0
.end method

.method public static b$redex0(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;Ljava/lang/String;)V
    .locals 8
    .param p0    # Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;
        .annotation build Lcom/facebook/graphql/calls/PageLeadGenInfoState;
        .end annotation
    .end param

    .prologue
    .line 2436397
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2436398
    :goto_0
    return-void

    .line 2436399
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pages_contact_inbox_update_state_mutation_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2436400
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->d()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNREAD:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    if-eq v0, v1, :cond_2

    .line 2436401
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->k:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v1, "RESPONDED"

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    .line 2436402
    :goto_1
    iget-object v3, v0, LX/9XE;->a:LX/0Zb;

    sget-object v6, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TOGGLE_RESPONDED:LX/9X6;

    invoke-static {v6, v4, v5}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "result_in_responded"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v3, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2436403
    :goto_2
    new-instance v0, LX/4Hn;

    invoke-direct {v0}, LX/4Hn;-><init>()V

    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v1

    .line 2436404
    const-string v3, "id"

    invoke-virtual {v0, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436405
    move-object v0, v0

    .line 2436406
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436407
    move-object v0, v0

    .line 2436408
    new-instance v1, LX/HBG;

    invoke-direct {v1}, LX/HBG;-><init>()V

    move-object v3, v1

    .line 2436409
    const-string v1, "input"

    invoke-virtual {v3, v1, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2436410
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/HB2;

    invoke-direct {v3, p0}, LX/HB2;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    .line 2436411
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 2436412
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_from_UNREAD_to_READ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_2
.end method

.method private c()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2436392
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2436393
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2436394
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2436395
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2436396
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private d()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .locals 2

    .prologue
    .line 2436391
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->i:LX/HBN;

    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HBN;->a(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->i:LX/HBN;

    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HBN;->a(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2436378
    invoke-super {p0, p1}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2436379
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    const/16 v5, 0x455

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v6

    check-cast v6, LX/11S;

    invoke-static {v0}, LX/HBP;->b(LX/0QB;)LX/HBP;

    move-result-object v7

    check-cast v7, LX/HBP;

    const/16 v8, 0x12b1

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x542

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x12c4

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xafd

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p1, 0x2b68

    invoke-static {v0, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/HBN;->a(LX/0QB;)LX/HBN;

    move-result-object v0

    check-cast v0, LX/HBN;

    iput-object v5, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->a:LX/0Ot;

    iput-object v6, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->b:LX/11S;

    iput-object v7, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->c:LX/HBP;

    iput-object v8, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->d:LX/0Ot;

    iput-object v9, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->e:LX/0Ot;

    iput-object v10, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->f:LX/0Ot;

    iput-object v11, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->g:LX/0Ot;

    iput-object p1, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->h:LX/0Ot;

    iput-object v0, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->i:LX/HBN;

    .line 2436380
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2436381
    const-string v3, "extra_request_key"

    invoke-static {v0, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    .line 2436382
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2436383
    const-string v3, "extra_page_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->k:Ljava/lang/String;

    .line 2436384
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2436385
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2436386
    const-string v3, "extra_page_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->l:Ljava/lang/String;

    .line 2436387
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->l:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2436388
    return-void

    :cond_0
    move v0, v2

    .line 2436389
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2436390
    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x3075baf5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 2436351
    const v0, 0x7f030ec9

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2436352
    const v0, 0x7f0d241e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436353
    const v0, 0x7f0d2420

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436354
    const v0, 0x7f0d241f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->b:LX/11S;

    sget-object v5, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    iget-object v6, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->k()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-interface {v2, v5, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 2436355
    check-cast v0, Landroid/widget/LinearLayout;

    .line 2436356
    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->c:LX/HBP;

    iget-object v5, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-virtual {v2, v6, v7}, LX/HBP;->b(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2436357
    invoke-direct {p0, v1}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->a(Landroid/view/View;)V

    .line 2436358
    :cond_0
    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2436359
    sget-object v2, LX/HB4;->EMAIL:LX/HB4;

    iget-object v5, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1, v0, v2, v5}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LX/HB4;Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 2436360
    if-eqz v2, :cond_1

    .line 2436361
    iget-object v5, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2436362
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2436363
    :cond_1
    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2436364
    const v2, 0x7f030e38

    invoke-virtual {p1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2436365
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2436366
    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2436367
    sget-object v6, LX/HB4;->PHONE:LX/HB4;

    invoke-virtual {v5, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v0, v6, v2}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LX/HB4;Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 2436368
    if-eqz v2, :cond_3

    .line 2436369
    iget-object v5, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2436370
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2436371
    :cond_3
    const v2, 0x7f030ec7

    invoke-virtual {p1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 2436372
    const v2, 0x7f0d2416

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/SwitchCompat;

    .line 2436373
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->d()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->RESPONDED:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    if-ne v6, v7, :cond_4

    const/4 v3, 0x1

    :cond_4
    invoke-virtual {v2, v3}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2436374
    new-instance v3, LX/HAx;

    invoke-direct {v3, p0}, LX/HAx;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2436375
    const v2, 0x7f0d2417

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, LX/HAz;

    invoke-direct {v3, p0}, LX/HAz;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2436376
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2436377
    const v0, 0x364e8603

    invoke-static {v0, v4}, LX/02F;->f(II)V

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x26ea7d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2436344
    invoke-super {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->onStart()V

    .line 2436345
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2436346
    if-eqz v0, :cond_0

    .line 2436347
    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2436348
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2436349
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2436350
    :cond_0
    const/16 v0, 0x2b

    const v2, 0xb3a9ac5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2436340
    invoke-super {p0, p1, p2}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2436341
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->d()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNREAD:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    if-ne v0, v1, :cond_0

    .line 2436342
    const-string v0, "READ"

    invoke-static {p0, v0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->b$redex0(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;Ljava/lang/String;)V

    .line 2436343
    :cond_0
    return-void
.end method
