.class public Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;
.super Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HB8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HBP;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HBN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:J

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public j:Landroid/support/v4/widget/SwipeRefreshLayout;

.field public k:LX/1P0;

.field public l:Landroid/view/View;

.field public m:Lcom/facebook/pages/common/ui/PagesEmptyView;

.field public n:LX/HB7;

.field private o:Z

.field public p:Z

.field public q:Ljava/lang/String;

.field public r:Z

.field private final s:Landroid/view/View$OnClickListener;

.field public final t:LX/1OX;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2436257
    invoke-direct {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;-><init>()V

    .line 2436258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->o:Z

    .line 2436259
    iput-boolean v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->p:Z

    .line 2436260
    iput-boolean v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->r:Z

    .line 2436261
    new-instance v0, LX/HAq;

    invoke-direct {v0, p0}, LX/HAq;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->s:Landroid/view/View$OnClickListener;

    .line 2436262
    new-instance v0, LX/HAr;

    invoke-direct {v0, p0}, LX/HAr;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->t:LX/1OX;

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;Z)V
    .locals 4

    .prologue
    .line 2436249
    if-nez p1, :cond_0

    .line 2436250
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2436251
    iget-boolean v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->p:Z

    if-eqz v0, :cond_1

    .line 2436252
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/HB7;->b(Z)V

    .line 2436253
    :cond_0
    new-instance v1, LX/HAt;

    invoke-direct {v1, p0}, LX/HAt;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V

    .line 2436254
    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->a:LX/1Ck;

    if-eqz p1, :cond_2

    const-string v0, "pages_fetch_contact_inbox_request_messages_list"

    :goto_0
    new-instance v3, LX/HAu;

    invoke-direct {v3, p0, p1}, LX/HAu;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;Z)V

    invoke-virtual {v2, v0, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2436255
    :cond_1
    return-void

    .line 2436256
    :cond_2
    const-string v0, "pages_fetch_contact_inbox_more_request_messages"

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;I)V
    .locals 2

    .prologue
    .line 2436245
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2436246
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->m:Lcom/facebook/pages/common/ui/PagesEmptyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setVisibility(I)V

    .line 2436247
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->m:Lcom/facebook/pages/common/ui/PagesEmptyView;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setMessage(I)V

    .line 2436248
    return-void
.end method

.method public static b(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2436239
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->f:LX/HBN;

    invoke-virtual {v0}, LX/HBN;->b()V

    .line 2436240
    iput-boolean v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->p:Z

    .line 2436241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->q:Ljava/lang/String;

    .line 2436242
    iput-boolean v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->r:Z

    .line 2436243
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->a(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;Z)V

    .line 2436244
    return-void
.end method

.method public static e(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V
    .locals 1

    .prologue
    .line 2436237
    const v0, 0x7f083692

    invoke-static {p0, v0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->a$redex0(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;I)V

    .line 2436238
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2436263
    invoke-super {p0, p1}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2436264
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    const-class v5, LX/HB8;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HB8;

    const/16 v6, 0x2b68

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p1, 0x2b51

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/HBN;->a(LX/0QB;)LX/HBN;

    move-result-object v0

    check-cast v0, LX/HBN;

    iput-object v3, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->a:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->b:LX/0tX;

    iput-object v5, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->c:LX/HB8;

    iput-object v6, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->d:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->e:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->f:LX/HBN;

    .line 2436265
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2436266
    const-string v1, "arg_page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->g:J

    .line 2436267
    iget-wide v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2436268
    return-void

    .line 2436269
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2a

    const v1, 0x232e28d0

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2436211
    const v0, 0x7f030ec6

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2436212
    const v0, 0x7f0d2414

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2436213
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v3, LX/HAs;

    invoke-direct {v3, p0}, LX/HAs;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2436214
    const v0, 0x7f0d2412

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->l:Landroid/view/View;

    .line 2436215
    const v0, 0x7f0d2413

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/ui/PagesEmptyView;

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->m:Lcom/facebook/pages/common/ui/PagesEmptyView;

    .line 2436216
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->m:Lcom/facebook/pages/common/ui/PagesEmptyView;

    const v3, 0x7f020135

    invoke-virtual {v0, v3}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setImageResource(I)V

    .line 2436217
    const v0, 0x7f0d2415

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2436218
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->c:LX/HB8;

    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, LX/HB8;->a(Landroid/view/View$OnClickListener;)LX/HB7;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    .line 2436219
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->k:LX/1P0;

    .line 2436220
    iget-boolean v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->o:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->f:LX/HBN;

    .line 2436221
    iget-object v3, v0, LX/HBN;->a:Landroid/os/Bundle;

    move-object v0, v3

    .line 2436222
    if-eqz v0, :cond_1

    .line 2436223
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->l:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2436224
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->f:LX/HBN;

    .line 2436225
    iget-object v3, v0, LX/HBN;->a:Landroid/os/Bundle;

    move-object v0, v3

    .line 2436226
    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->k:LX/1P0;

    const-string v4, "state_layout_manager"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1OR;->a(Landroid/os/Parcelable;)V

    .line 2436227
    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    const-string v4, "state_item_list"

    invoke-static {v0, v4}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2436228
    iget-object v4, v3, LX/HB7;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2436229
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 2436230
    iget-object v6, v3, LX/HB7;->c:Ljava/util/List;

    check-cast v4, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2436231
    :cond_0
    invoke-virtual {v3}, LX/1OM;->notifyDataSetChanged()V

    .line 2436232
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->t:LX/1OX;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2436233
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2436234
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->k:LX/1P0;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2436235
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/62X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0117

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, LX/62X;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2436236
    const/16 v0, 0x2b

    const v3, -0x4acb5559

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6e3198ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2436208
    iget-object v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->f:LX/HBN;

    invoke-virtual {v1}, LX/HBN;->b()V

    .line 2436209
    invoke-super {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->onDestroy()V

    .line 2436210
    const/16 v1, 0x2b

    const v2, -0x64714822

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x52058f8a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2436196
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2436197
    const-string v2, "state_item_list"

    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    .line 2436198
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2436199
    iget-object v6, v3, LX/HB7;->c:Ljava/util/List;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2436200
    move-object v3, v5

    .line 2436201
    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2436202
    const-string v2, "state_layout_manager"

    iget-object v3, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->k:LX/1P0;

    invoke-virtual {v3}, LX/1OR;->f()Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2436203
    iget-object v2, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->f:LX/HBN;

    .line 2436204
    iput-object v1, v2, LX/HBN;->a:Landroid/os/Bundle;

    .line 2436205
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->o:Z

    .line 2436206
    invoke-super {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->onDestroyView()V

    .line 2436207
    const/16 v1, 0x2b

    const v2, -0x607796ae

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4d25438b    # 1.73291696E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2436189
    invoke-super {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->onStart()V

    .line 2436190
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2436191
    if-eqz v1, :cond_0

    .line 2436192
    const v2, 0x7f083691

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2436193
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2436194
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2436195
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x478c2ea2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2436183
    invoke-super {p0, p1, p2}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2436184
    iget-boolean v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->o:Z

    if-eqz v0, :cond_0

    .line 2436185
    invoke-static {p0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->b(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V

    .line 2436186
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2436187
    iget-object v0, p0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2436188
    :cond_0
    return-void
.end method
