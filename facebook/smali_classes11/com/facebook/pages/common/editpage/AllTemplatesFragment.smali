.class public Lcom/facebook/pages/common/editpage/AllTemplatesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HBr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:J

.field private e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public f:Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2437890
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;

    const/16 v2, 0xafd

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x12b1

    invoke-static {v1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class p0, LX/HBr;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/HBr;

    iput-object v2, p1, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->a:LX/0Ot;

    iput-object v3, p1, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->b:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->c:LX/HBr;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2437860
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2437861
    const-class v0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2437862
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2437863
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->d:J

    .line 2437864
    iget-wide v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2437865
    return-void

    .line 2437866
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1ce3c77e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2437889
    const v1, 0x7f0300d9

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x581e809d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x50ee764a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2437883
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2437884
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2437885
    if-eqz v0, :cond_0

    .line 2437886
    const v2, 0x7f0836d6

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2437887
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2437888
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x13e97d39

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2437867
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2437868
    const v0, 0x7f0d052a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2437869
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->c:LX/HBr;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    .line 2437870
    iget v3, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v3, v3

    .line 2437871
    iget-wide v4, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->d:J

    invoke-virtual/range {v0 .. v5}, LX/HBr;->a(Landroid/content/Context;LX/0gc;IJ)Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->f:Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;

    .line 2437872
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x1

    .line 2437873
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2437874
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->f:Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2437875
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2437876
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "fetch_pages_templates_data_request"

    .line 2437877
    new-instance v6, LX/HCY;

    invoke-direct {v6}, LX/HCY;-><init>()V

    move-object v6, v6

    .line 2437878
    const-string v7, "page_id"

    iget-wide v8, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "template_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/HCY;

    .line 2437879
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    .line 2437880
    iget-object v6, p0, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-virtual {v6, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v2, v6

    .line 2437881
    new-instance v3, LX/HBm;

    invoke-direct {v3, p0}, LX/HBm;-><init>(Lcom/facebook/pages/common/editpage/AllTemplatesFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2437882
    return-void
.end method
