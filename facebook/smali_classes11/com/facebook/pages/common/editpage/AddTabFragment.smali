.class public Lcom/facebook/pages/common/editpage/AddTabFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/HDI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/HDK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

.field public g:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field public h:Lcom/facebook/resources/ui/FbFrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2437809
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/common/editpage/AddTabFragment;

    invoke-static {p0}, LX/HDI;->a(LX/0QB;)LX/HDI;

    move-result-object v1

    check-cast v1, LX/HDI;

    const-class v2, LX/HDK;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/HDK;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v1, p1, Lcom/facebook/pages/common/editpage/AddTabFragment;->a:LX/HDI;

    iput-object v2, p1, Lcom/facebook/pages/common/editpage/AddTabFragment;->b:LX/HDK;

    iput-object p0, p1, Lcom/facebook/pages/common/editpage/AddTabFragment;->c:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2437840
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2437841
    const-class v0, Lcom/facebook/pages/common/editpage/AddTabFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/editpage/AddTabFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2437842
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2437843
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->d:J

    .line 2437844
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2437845
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->e:Ljava/lang/String;

    .line 2437846
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2437847
    const-string v1, "extra_addable_tabs_channel_data"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2437848
    iget-wide v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2437849
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2437850
    return-void

    .line 2437851
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x25ee4996

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2437839
    const v1, 0x7f0300a3

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5b79a919

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x733dee34

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2437833
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2437834
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2437835
    if-eqz v1, :cond_0

    .line 2437836
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2437837
    const v2, 0x7f0836b2

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2437838
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x536d2bb6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2437810
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2437811
    const v0, 0x7f0d04ac

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->g:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2437812
    const v0, 0x7f0d04ad

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->h:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 2437813
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->g:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    .line 2437814
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->a:LX/HDI;

    .line 2437815
    iget-object v1, v0, LX/HDI;->a:LX/0P1;

    move-object v2, v1

    .line 2437816
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2437817
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v5

    .line 2437818
    if-eqz v5, :cond_0

    .line 2437819
    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HDF;

    .line 2437820
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {v0}, LX/HDE;->c()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2437821
    iget-object v6, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->g:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2437822
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0300a4

    iget-object v9, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->g:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 p1, 0x0

    invoke-virtual {v7, v8, v9, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/fig/listitem/FigListItem;

    .line 2437823
    iget-object v8, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->c:LX/0wM;

    invoke-interface {v0}, LX/HDE;->d()I

    move-result v9

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a00a6

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v8, v9, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2437824
    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2437825
    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gA_()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gA_()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-virtual {v7, v8}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2437826
    new-instance v8, LX/HBl;

    invoke-direct {v8, p0, v0}, LX/HBl;-><init>(Lcom/facebook/pages/common/editpage/AddTabFragment;LX/HDF;)V

    invoke-virtual {v7, v8}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2437827
    move-object v0, v7

    .line 2437828
    invoke-virtual {v6, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2437829
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2437830
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AddTabFragment;->b:LX/HDK;

    invoke-virtual {v0, v5}, LX/HDK;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/HDJ;

    move-result-object v0

    goto :goto_1

    .line 2437831
    :cond_2
    return-void

    .line 2437832
    :cond_3
    const/4 v8, 0x0

    goto :goto_2
.end method
