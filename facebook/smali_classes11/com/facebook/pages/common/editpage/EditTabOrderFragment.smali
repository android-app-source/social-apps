.class public Lcom/facebook/pages/common/editpage/EditTabOrderFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/HCP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:J

.field private g:Ljava/lang/String;

.field public h:LX/HCg;

.field private i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private j:LX/HCO;

.field public k:LX/3xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2438413
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static c(Lcom/facebook/pages/common/editpage/EditTabOrderFragment;)V
    .locals 5

    .prologue
    .line 2438414
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->h:LX/HCg;

    invoke-interface {v0}, LX/HCg;->a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2438415
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->j:LX/HCO;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->h:LX/HCg;

    invoke-interface {v2}, LX/HCg;->a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->a()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2438416
    iget-object v2, v0, LX/HCO;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2438417
    iget-object v2, v0, LX/HCO;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2438418
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/HCO;->l:Z

    .line 2438419
    iget-object v2, v0, LX/HCO;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2438420
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v4, p0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v2, v4, :cond_0

    .line 2438421
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/HCO;->l:Z

    goto :goto_0

    .line 2438422
    :cond_2
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2438423
    :cond_3
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2438424
    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->j:LX/HCO;

    .line 2438425
    iget-boolean v2, v1, LX/HCO;->n:Z

    move v1, v2

    .line 2438426
    if-eqz v1, :cond_0

    .line 2438427
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2438428
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2438429
    const-string v3, "extra_updated_tab_order"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2438430
    const/4 v3, -0x1

    invoke-virtual {v1, v3, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2438431
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2438432
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2438433
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2438434
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    const-class v3, LX/HCP;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HCP;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v5, 0x259

    invoke-static {p1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xafd

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v0, 0x12b1

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->a:LX/HCP;

    iput-object v4, v2, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->b:LX/0Uh;

    iput-object v5, v2, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->d:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->e:LX/0Ot;

    .line 2438435
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2438436
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->f:J

    .line 2438437
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2438438
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->g:Ljava/lang/String;

    .line 2438439
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2438440
    const-string v1, "extra_reorder_tabs_data"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HCg;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->h:LX/HCg;

    .line 2438441
    iget-wide v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2438442
    return-void

    .line 2438443
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x604a0011

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2438444
    const v1, 0x7f030462

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x75b16fb7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xfc0e7e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2438445
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2438446
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2438447
    if-eqz v1, :cond_0

    .line 2438448
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2438449
    const v2, 0x7f0836b4

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2438450
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x34bcc7ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2438451
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2438452
    const v0, 0x7f0d0d31

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2438453
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->a:LX/HCP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->f:J

    invoke-virtual {v0, v1, v2, v3, p0}, LX/HCP;->a(Landroid/content/Context;JLcom/facebook/pages/common/editpage/EditTabOrderFragment;)LX/HCO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->j:LX/HCO;

    .line 2438454
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x1

    .line 2438455
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2438456
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->j:LX/HCO;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2438457
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2438458
    new-instance v0, LX/HCH;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->j:LX/HCO;

    invoke-direct {v0, v1}, LX/HCH;-><init>(LX/HCO;)V

    .line 2438459
    new-instance v1, LX/3xm;

    invoke-direct {v1, v0}, LX/3xm;-><init>(LX/3xj;)V

    iput-object v1, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->k:LX/3xm;

    .line 2438460
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->k:LX/3xm;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, LX/3xm;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 2438461
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->h:LX/HCg;

    if-eqz v0, :cond_0

    .line 2438462
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->c(Lcom/facebook/pages/common/editpage/EditTabOrderFragment;)V

    .line 2438463
    :goto_0
    return-void

    .line 2438464
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "fetch_pages_reorder_data_request"

    const/4 v5, 0x0

    .line 2438465
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2438466
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->values()[Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v7

    array-length v8, v7

    move v4, v5

    :goto_1
    if-ge v4, v8, :cond_2

    aget-object v9, v7, v4

    .line 2438467
    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v10

    const-string v11, "TAB_"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2438468
    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2438469
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2438470
    :cond_2
    new-instance v4, LX/HPt;

    invoke-direct {v4}, LX/HPt;-><init>()V

    move-object v4, v4

    .line 2438471
    const-string v7, "page_id"

    iget-wide v8, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v7, "fetch_action_tabs"

    iget-object v8, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->b:LX/0Uh;

    sget v9, LX/8Dm;->g:I

    invoke-virtual {v8, v9, v5}, LX/0Uh;->a(IZ)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v7, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_tab_navigation_edit_channel_context"

    new-instance v7, LX/4CV;

    invoke-direct {v7}, LX/4CV;-><init>()V

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4CV;->a(Ljava/lang/Boolean;)LX/4CV;

    move-result-object v7

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/4CV;->a(Ljava/util/List;)LX/4CV;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/HPt;

    .line 2438472
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2438473
    iget-object v4, p0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2438474
    new-instance v3, LX/HCG;

    invoke-direct {v3, p0}, LX/HCG;-><init>(Lcom/facebook/pages/common/editpage/EditTabOrderFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method
