.class public Lcom/facebook/pages/common/editpage/EditPageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field public a:LX/HBs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HCD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:J

.field private g:Landroid/widget/ViewFlipper;

.field private h:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private j:Landroid/widget/LinearLayout;

.field public k:LX/HCC;

.field private l:LX/1P0;

.field private m:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2438118
    const-class v0, Lcom/facebook/pages/common/editpage/EditPageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/editpage/EditPageFragment;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2438115
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2438116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->n:Z

    .line 2438117
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/pages/common/editpage/EditPageFragment;

    invoke-static {v4}, LX/HBs;->b(LX/0QB;)LX/HBs;

    move-result-object v1

    check-cast v1, LX/HBs;

    invoke-static {v4}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v4}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const-class p0, LX/HCD;

    invoke-interface {v4, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/HCD;

    iput-object v1, p1, Lcom/facebook/pages/common/editpage/EditPageFragment;->a:LX/HBs;

    iput-object v2, p1, Lcom/facebook/pages/common/editpage/EditPageFragment;->b:LX/03V;

    iput-object v3, p1, Lcom/facebook/pages/common/editpage/EditPageFragment;->c:LX/1Ck;

    iput-object v4, p1, Lcom/facebook/pages/common/editpage/EditPageFragment;->d:LX/HCD;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/editpage/EditPageFragment;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2438106
    iget-boolean v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->n:Z

    if-eqz v1, :cond_0

    .line 2438107
    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->g:Landroid/widget/ViewFlipper;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 2438108
    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->h:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2438109
    :goto_0
    return-void

    .line 2438110
    :cond_0
    if-nez p1, :cond_1

    .line 2438111
    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->h:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2438112
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->g:Landroid/widget/ViewFlipper;

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->h:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2438113
    iget-boolean p0, v2, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    move v2, p0

    .line 2438114
    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V
    .locals 15

    .prologue
    .line 2438086
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->n:Z

    .line 2438087
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->a$redex0(Lcom/facebook/pages/common/editpage/EditPageFragment;Z)V

    .line 2438088
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->c:LX/1Ck;

    sget-object v1, LX/HBx;->FETCH_EDIT_PAGE_QUERY:LX/HBx;

    iget-object v2, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->a:LX/HBs;

    iget-wide v4, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->f:J

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2438089
    const-wide/16 v10, 0x0

    cmp-long v6, v4, v10

    if-lez v6, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 2438090
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2438091
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->values()[Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v10

    array-length v11, v10

    move v6, v8

    :goto_1
    if-ge v6, v11, :cond_2

    aget-object v12, v10, v6

    .line 2438092
    invoke-virtual {v12}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v13

    const-string v14, "TAB_"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2438093
    invoke-virtual {v9, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2438094
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    move v6, v8

    .line 2438095
    goto :goto_0

    .line 2438096
    :cond_2
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 2438097
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2438098
    invoke-virtual {v9, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2438099
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v9, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2438100
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 2438101
    new-instance v10, LX/HCe;

    invoke-direct {v10}, LX/HCe;-><init>()V

    move-object v10, v10

    .line 2438102
    const-string v11, "page_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v10

    const-string v11, "fetch_action_tabs"

    iget-object v12, v2, LX/HBs;->c:LX/0Uh;

    sget v13, LX/8Dm;->g:I

    invoke-virtual {v12, v13, v8}, LX/0Uh;->a(IZ)Z

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v10

    const-string v11, "config_actions_enabled"

    iget-object v12, v2, LX/HBs;->c:LX/0Uh;

    sget v13, LX/8Dm;->i:I

    invoke-virtual {v12, v13, v8}, LX/0Uh;->a(IZ)Z

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v10

    const-string v11, "profile_tab_navigation_edit_channel_context"

    new-instance v12, LX/4CV;

    invoke-direct {v12}, LX/4CV;-><init>()V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v12, v13}, LX/4CV;->a(Ljava/lang/Boolean;)LX/4CV;

    move-result-object v12

    invoke-static {v9}, LX/HBs;->a(LX/0Px;)LX/0Px;

    move-result-object v9

    invoke-virtual {v12, v9}, LX/4CV;->a(Ljava/util/List;)LX/4CV;

    move-result-object v9

    invoke-virtual {v10, v11, v9}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v9

    const-string v10, "addable_tabs_channel_context"

    new-instance v11, LX/4CV;

    invoke-direct {v11}, LX/4CV;-><init>()V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v11, v7}, LX/4CV;->a(Ljava/lang/Boolean;)LX/4CV;

    move-result-object v7

    invoke-static {v6}, LX/HBs;->a(LX/0Px;)LX/0Px;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/4CV;->a(Ljava/util/List;)LX/4CV;

    move-result-object v6

    invoke-virtual {v9, v10, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v6

    const-string v7, "cta_icon_size"

    iget-object v9, v2, LX/HBs;->a:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0f8e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v7, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "cta_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v9

    invoke-virtual {v6, v7, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    const-string v7, "template_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v9

    invoke-virtual {v6, v7, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    const-string v7, "template_picker_enabled"

    iget-object v9, v2, LX/HBs;->c:LX/0Uh;

    sget v10, LX/8Dm;->k:I

    invoke-virtual {v9, v10, v8}, LX/0Uh;->a(IZ)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/HCe;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2438103
    iget-object v7, v2, LX/HBs;->b:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v2, v6

    .line 2438104
    new-instance v3, LX/HBw;

    invoke-direct {v3, p0}, LX/HBw;-><init>(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2438105
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    .line 2438119
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->m:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eqz v0, :cond_1

    .line 2438120
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2438121
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2438122
    const-string v2, "extra_deleted_tab_type"

    iget-object v3, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->m:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2438123
    if-eqz v0, :cond_0

    .line 2438124
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2438125
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2438126
    :cond_0
    const/4 v0, 0x1

    .line 2438127
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2438079
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2438080
    const-class v0, Lcom/facebook/pages/common/editpage/EditPageFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2438081
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2438082
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->f:J

    .line 2438083
    iget-wide v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2438084
    return-void

    .line 2438085
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2438043
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2438044
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2438045
    const/16 v0, 0x2783

    if-ne p1, v0, :cond_1

    if-eqz p3, :cond_1

    .line 2438046
    const-string v0, "extra_deleted_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2438047
    const-string v0, "extra_deleted_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->m:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2438048
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    .line 2438049
    :cond_0
    :goto_0
    return-void

    .line 2438050
    :cond_1
    const/16 v0, 0x2781

    if-ne p1, v0, :cond_2

    .line 2438051
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    goto :goto_0

    .line 2438052
    :cond_2
    const/16 v0, 0x2784

    if-ne p1, v0, :cond_3

    .line 2438053
    const-string v0, "extra_updated_tab_order"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2438054
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    goto :goto_0

    .line 2438055
    :cond_3
    const/16 v0, 0x2785

    if-ne p1, v0, :cond_4

    .line 2438056
    const-string v0, "extra_updated_actions"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2438057
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    goto :goto_0

    .line 2438058
    :cond_4
    const/16 v0, 0x2786

    if-ne p1, v0, :cond_5

    .line 2438059
    const-string v0, "extra_updated_page_template"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2438060
    new-instance v0, LX/6WI;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080018

    const/4 v2, 0x0

    .line 2438061
    iget-object p1, v0, LX/6WI;->a:LX/31a;

    iget-object p2, v0, LX/6WI;->a:LX/31a;

    iget-object p2, p2, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {p2, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    iput-object p2, p1, LX/31a;->o:Ljava/lang/CharSequence;

    .line 2438062
    iget-object p1, v0, LX/6WI;->a:LX/31a;

    iput-object v2, p1, LX/31a;->p:Landroid/content/DialogInterface$OnClickListener;

    .line 2438063
    move-object v0, v0

    .line 2438064
    const v1, 0x7f0836e1

    invoke-virtual {v0, v1}, LX/6WI;->a(I)LX/6WI;

    move-result-object v0

    invoke-virtual {v0}, LX/6WI;->a()LX/6WJ;

    move-result-object v0

    .line 2438065
    invoke-virtual {v0}, LX/6WJ;->show()V

    .line 2438066
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    goto :goto_0

    .line 2438067
    :cond_5
    const/16 v0, 0x2788

    if-ne p1, v0, :cond_6

    .line 2438068
    const-string v0, "extra_add_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2438069
    if-eqz v0, :cond_0

    .line 2438070
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    goto :goto_0

    .line 2438071
    :cond_6
    const/16 v0, 0x2789

    if-ne p1, v0, :cond_7

    .line 2438072
    const-string v0, "extra_add_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2438073
    if-eqz v0, :cond_0

    .line 2438074
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    goto/16 :goto_0

    .line 2438075
    :cond_7
    const/16 v0, 0x278a

    if-ne p1, v0, :cond_0

    .line 2438076
    const-string v0, "extra_deleted_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2438077
    if-eqz v0, :cond_0

    .line 2438078
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    goto/16 :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x11972e22

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2438042
    const v1, 0x7f030456

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x9af395f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x545baacf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2438039
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2438040
    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->c:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2438041
    const/16 v1, 0x2b

    const v2, -0x73ca13e9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5aa7f104

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2438033
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2438034
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2438035
    if-eqz v1, :cond_0

    .line 2438036
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2438037
    const v2, 0x7f0836ad

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2438038
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x50afd08c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2438017
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2438018
    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->d:LX/HCD;

    iget-wide v2, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->f:J

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, LX/HBt;

    invoke-direct {v5, p0}, LX/HBt;-><init>(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    .line 2438019
    iget v0, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v6, v0

    .line 2438020
    invoke-virtual/range {v1 .. v6}, LX/HCD;->a(JLandroid/content/Context;LX/HBt;I)LX/HCC;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->k:LX/HCC;

    .line 2438021
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v7, v2}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->l:LX/1P0;

    .line 2438022
    const v0, 0x7f0d0d22

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->g:Landroid/widget/ViewFlipper;

    .line 2438023
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->h:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2438024
    const v0, 0x7f0d0d23

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2438025
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->k:LX/HCC;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2438026
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->l:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2438027
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/62f;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v2, v7}, LX/62f;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2438028
    const v0, 0x7f0d0d24

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->j:Landroid/widget/LinearLayout;

    .line 2438029
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->h:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, LX/HBu;

    invoke-direct {v1, p0}, LX/HBu;-><init>(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2438030
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/EditPageFragment;->j:Landroid/widget/LinearLayout;

    new-instance v1, LX/HBv;

    invoke-direct {v1, p0}, LX/HBv;-><init>(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2438031
    invoke-static {p0}, Lcom/facebook/pages/common/editpage/EditPageFragment;->d(Lcom/facebook/pages/common/editpage/EditPageFragment;)V

    .line 2438032
    return-void
.end method
