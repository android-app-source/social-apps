.class public Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;

.field private static final m:Ljava/lang/String;


# instance fields
.field public final n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final o:Lcom/facebook/resources/ui/FbTextView;

.field public final p:Lcom/facebook/resources/ui/FbTextView;

.field public final q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2441248
    const-class v0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->m:Ljava/lang/String;

    .line 2441249
    const-class v0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;

    sget-object v1, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2441242
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2441243
    const v0, 0x7f0d0bcf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2441244
    const v0, 0x7f0d0bd1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2441245
    const v0, 0x7f0d0bd2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2441246
    const v0, 0x7f0d0bd0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2441247
    return-void
.end method
