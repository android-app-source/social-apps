.class public Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HBq;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/content/Context;

.field public c:LX/3Sb;

.field private final d:LX/23P;

.field private final e:LX/0gc;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private final g:I

.field private final h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2437927
    const-class v0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;

    const-class v1, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/23P;LX/0Ot;Landroid/content/Context;LX/0gc;IJ)V
    .locals 2
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/23P;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "Landroid/content/Context;",
            "LX/0gc;",
            "IJ)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2437969
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2437970
    new-instance v0, LX/4AD;

    invoke-direct {v0}, LX/4AD;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->c:LX/3Sb;

    .line 2437971
    iput-object p1, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->d:LX/23P;

    .line 2437972
    iput-object p3, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->b:Landroid/content/Context;

    .line 2437973
    iput-object p4, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->e:LX/0gc;

    .line 2437974
    iput-object p2, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->f:LX/0Ot;

    .line 2437975
    iput p5, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->g:I

    .line 2437976
    iput-wide p6, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->h:J

    .line 2437977
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;)V
    .locals 9

    .prologue
    .line 2437958
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9X7;->EDIT_TAP_TEMPLATE_MORE_INFO:LX/9X7;

    iget-wide v2, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->h:J

    const-string v5, "templates"

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LX/9XE;->a(LX/9X7;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Ljava/lang/String;)V

    .line 2437959
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->e:LX/0gc;

    iget v1, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->g:I

    iget-wide v2, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->h:J

    .line 2437960
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v4

    const v5, 0x7f0400d6

    const v6, 0x7f0400e1

    const v7, 0x7f0400d5

    const v8, 0x7f0400e2

    invoke-virtual {v4, v5, v6, v7, v8}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v4

    .line 2437961
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2437962
    const-string v6, "com.facebook.katana.profile.id"

    invoke-virtual {v5, v6, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2437963
    const-string v6, "template_type"

    invoke-virtual {v5, v6, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2437964
    new-instance v6, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    invoke-direct {v6}, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;-><init>()V

    .line 2437965
    invoke-virtual {v6, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2437966
    move-object v5, v6

    .line 2437967
    invoke-virtual {v4, v1, v5}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v4

    invoke-virtual {v4}, LX/0hH;->b()I

    .line 2437968
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2437954
    sget-object v0, LX/HBp;->HEADER:LX/HBp;

    invoke-virtual {v0}, LX/HBp;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2437955
    const v0, 0x7f0300d8

    .line 2437956
    :goto_0
    new-instance v1, LX/HBq;

    iget-object v2, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->b:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, p0, v0, p2}, LX/HBq;-><init>(Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;Landroid/view/View;I)V

    return-object v1

    .line 2437957
    :cond_0
    const v0, 0x7f03148c

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2437930
    check-cast p1, LX/HBq;

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 2437931
    iget v0, p1, LX/HBq;->r:I

    sget-object v1, LX/HBp;->HEADER:LX/HBp;

    invoke-virtual {v1}, LX/HBp;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2437932
    :goto_0
    return-void

    .line 2437933
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->c:LX/3Sb;

    add-int/lit8 v1, p2, -0x1

    invoke-interface {v0, v1}, LX/3Sb;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2437934
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v3, LX/HBo;

    invoke-direct {v3, p0, v1, v0}, LX/HBo;-><init>(Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;LX/15i;I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2437935
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    iget-object v3, p1, LX/HBq;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v4, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2437936
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    iget-object v3, p1, LX/HBq;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2437937
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2437938
    const-string v2, " "

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2437939
    iget-object v2, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0836d3

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2437940
    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2437941
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    iget-object v5, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a010f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sub-int v2, v5, v2

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x12

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2437942
    iget-object v2, p1, LX/HBq;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2437943
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2437944
    iget-object v0, p1, LX/HBq;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->d:LX/23P;

    iget-object v2, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0836d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2437945
    iget-object v0, p1, LX/HBq;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2437946
    iget-object v0, p1, LX/HBq;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2437947
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2437948
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2437949
    iget-object v0, p1, LX/HBq;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->d:LX/23P;

    iget-object v2, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0836d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2437950
    iget-object v0, p1, LX/HBq;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2437951
    iget-object v0, p1, LX/HBq;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2437952
    :cond_2
    iget-object v0, p1, LX/HBq;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2437953
    iget-object v0, p1, LX/HBq;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2437929
    if-gtz p1, :cond_0

    sget-object v0, LX/HBp;->HEADER:LX/HBp;

    invoke-virtual {v0}, LX/HBp;->ordinal()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HBp;->TEMPLATE:LX/HBp;

    invoke-virtual {v0}, LX/HBp;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2437928
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;->c:LX/3Sb;

    invoke-interface {v0}, LX/3Sb;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
