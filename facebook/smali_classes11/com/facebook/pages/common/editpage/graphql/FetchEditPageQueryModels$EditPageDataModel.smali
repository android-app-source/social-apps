.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/HCi;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1e942842
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439685
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439684
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2439768
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2439769
    return-void
.end method

.method private j()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439766
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    .line 2439767
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    return-object v0
.end method

.method private k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439764
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2439765
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommerceStore"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439762
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2439763
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439760
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->i:Ljava/lang/String;

    .line 2439761
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439758
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    .line 2439759
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    return-object v0
.end method

.method private o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439756
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2439757
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    return-object v0
.end method

.method private p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439754
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    .line 2439755
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2439735
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439736
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2439737
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2439738
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x27742c0e

    invoke-static {v3, v2, v4}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2439739
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2439740
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2439741
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2439742
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2439743
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2439744
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2439745
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2439746
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2439747
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2439748
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2439749
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2439750
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2439751
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2439752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439753
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2439700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439701
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2439702
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    .line 2439703
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2439704
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    .line 2439705
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    .line 2439706
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2439707
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2439708
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2439709
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    .line 2439710
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2439711
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2439712
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x27742c0e

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2439713
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2439714
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    .line 2439715
    iput v3, v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->g:I

    move-object v1, v0

    .line 2439716
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2439717
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    .line 2439718
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2439719
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    .line 2439720
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    .line 2439721
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2439722
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2439723
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2439724
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    .line 2439725
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->k:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2439726
    :cond_4
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2439727
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    .line 2439728
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2439729
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    .line 2439730
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->l:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    .line 2439731
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439732
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    .line 2439733
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move-object p0, v1

    .line 2439734
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2439699
    new-instance v0, LX/HCl;

    invoke-direct {v0, p1}, LX/HCl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439698
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2439694
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2439695
    const/4 v0, 0x2

    const v1, 0x27742c0e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->g:I

    .line 2439696
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;->h:Z

    .line 2439697
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2439692
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2439693
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2439691
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2439688
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;-><init>()V

    .line 2439689
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2439690
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2439687
    const v0, -0x25796d3a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2439686
    const v0, 0x25d6af

    return v0
.end method
