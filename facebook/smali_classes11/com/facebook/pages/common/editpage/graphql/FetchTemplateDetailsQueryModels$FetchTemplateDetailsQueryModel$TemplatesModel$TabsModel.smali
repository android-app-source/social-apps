.class public final Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x840de43
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440565
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440568
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2440566
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2440567
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2440544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440545
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->a()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2440546
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2440547
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2440548
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2440549
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2440550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440551
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2440552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440553
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->a()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2440554
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->a()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    .line 2440555
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->a()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2440556
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;

    .line 2440557
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    .line 2440558
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2440559
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    .line 2440560
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2440561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;

    .line 2440562
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    .line 2440563
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440564
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440542
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    .line 2440543
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->e:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2440539
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;-><init>()V

    .line 2440540
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2440541
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2440538
    const v0, -0x728505d7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2440537
    const v0, -0x53558405

    return v0
.end method

.method public final j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440535
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    .line 2440536
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    return-object v0
.end method
