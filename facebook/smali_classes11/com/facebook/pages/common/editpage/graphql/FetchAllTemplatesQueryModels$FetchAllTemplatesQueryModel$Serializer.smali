.class public final Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2438986
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;

    new-instance v1, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2438987
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2438988
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2438989
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2438990
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2438991
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2438992
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2438993
    if-eqz v2, :cond_0

    .line 2438994
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2438995
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2438996
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2438997
    if-eqz v2, :cond_2

    .line 2438998
    const-string p0, "templates"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2438999
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2439000
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 2439001
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/HCc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439002
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2439003
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2439004
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2439005
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2439006
    check-cast p1, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Serializer;->a(Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
