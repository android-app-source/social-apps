.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x37f6714b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439290
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439289
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2439287
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2439288
    return-void
.end method

.method private a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTemplates"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2439285
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0x42917d8

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;->e:LX/3Sb;

    .line 2439286
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2439279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439280
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 2439281
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2439282
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2439283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439284
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2439291
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439292
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2439293
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2439294
    if-eqz v1, :cond_0

    .line 2439295
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;

    .line 2439296
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;->e:LX/3Sb;

    .line 2439297
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439298
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2439278
    new-instance v0, LX/HCk;

    invoke-direct {v0, p1}, LX/HCk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2439276
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2439277
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2439275
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2439272
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel;-><init>()V

    .line 2439273
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2439274
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2439271
    const v0, -0x403b0b03

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2439270
    const v0, 0x25d6af

    return v0
.end method
