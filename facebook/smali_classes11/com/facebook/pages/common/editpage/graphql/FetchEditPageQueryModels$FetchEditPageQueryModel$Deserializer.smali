.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2439770
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    new-instance v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2439771
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2439835
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2439772
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2439773
    const/4 v11, 0x0

    .line 2439774
    const/4 v10, 0x0

    .line 2439775
    const/4 v9, 0x0

    .line 2439776
    const/4 v8, 0x0

    .line 2439777
    const/4 v7, 0x0

    .line 2439778
    const/4 v6, 0x0

    .line 2439779
    const/4 v5, 0x0

    .line 2439780
    const/4 v4, 0x0

    .line 2439781
    const/4 v3, 0x0

    .line 2439782
    const/4 v2, 0x0

    .line 2439783
    const/4 v1, 0x0

    .line 2439784
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 2439785
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2439786
    const/4 v1, 0x0

    .line 2439787
    :goto_0
    move v1, v1

    .line 2439788
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2439789
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2439790
    new-instance v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;-><init>()V

    .line 2439791
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2439792
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2439793
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2439794
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2439795
    :cond_0
    return-object v1

    .line 2439796
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2439797
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_d

    .line 2439798
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 2439799
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2439800
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 2439801
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2439802
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    goto :goto_1

    .line 2439803
    :cond_4
    const-string p0, "actionBarChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2439804
    invoke-static {p1, v0}, LX/HCq;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2439805
    :cond_5
    const-string p0, "addableTabsChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2439806
    invoke-static {p1, v0}, LX/HCr;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2439807
    :cond_6
    const-string p0, "commerce_store"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2439808
    invoke-static {p1, v0}, LX/HCs;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2439809
    :cond_7
    const-string p0, "is_service_page"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2439810
    const/4 v1, 0x1

    .line 2439811
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 2439812
    :cond_8
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2439813
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2439814
    :cond_9
    const-string p0, "primaryButtonsChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 2439815
    invoke-static {p1, v0}, LX/HCt;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2439816
    :cond_a
    const-string p0, "profileTabNavigationEditChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 2439817
    invoke-static {p1, v0}, LX/HPz;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2439818
    :cond_b
    const-string p0, "services_card"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 2439819
    invoke-static {p1, v0}, LX/HCu;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2439820
    :cond_c
    const-string p0, "templates"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 2439821
    invoke-static {p1, v0}, LX/HCp;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 2439822
    :cond_d
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2439823
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 2439824
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2439825
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 2439826
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2439827
    if-eqz v1, :cond_e

    .line 2439828
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 2439829
    :cond_e
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2439830
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2439831
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2439832
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2439833
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2439834
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
