.class public final Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x38679c5d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439040
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439026
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2439027
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2439028
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439029
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2439030
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2439031
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2439032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439033
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2439034
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 2439035
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2439036
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2439037
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2439038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439039
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2439016
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439017
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2439018
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2439019
    if-eqz v1, :cond_0

    .line 2439020
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;

    .line 2439021
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->f:LX/3Sb;

    .line 2439022
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439023
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTemplates"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2439024
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, -0x3aa90cab

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->f:LX/3Sb;

    .line 2439025
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2439015
    new-instance v0, LX/HCa;

    invoke-direct {v0, p1}, LX/HCa;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2439013
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2439014
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2439012
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2439007
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$FetchAllTemplatesQueryModel;-><init>()V

    .line 2439008
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2439009
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2439011
    const v0, -0x79abf092

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2439010
    const v0, 0x252222

    return v0
.end method
