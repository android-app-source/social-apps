.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2439501
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    new-instance v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2439502
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2439503
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2439504
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2439505
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2439506
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 2439507
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2439508
    :goto_0
    move v1, v2

    .line 2439509
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2439510
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2439511
    new-instance v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;-><init>()V

    .line 2439512
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2439513
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2439514
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2439515
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2439516
    :cond_0
    return-object v1

    .line 2439517
    :cond_1
    const-string p0, "is_service_page"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2439518
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v8, v1

    move v1, v3

    .line 2439519
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_a

    .line 2439520
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 2439521
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2439522
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 2439523
    const-string p0, "actionBarChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2439524
    invoke-static {p1, v0}, LX/HCq;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 2439525
    :cond_3
    const-string p0, "addableTabsChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2439526
    invoke-static {p1, v0}, LX/HCr;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2439527
    :cond_4
    const-string p0, "commerce_store"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2439528
    invoke-static {p1, v0}, LX/HCs;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2439529
    :cond_5
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2439530
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2439531
    :cond_6
    const-string p0, "primaryButtonsChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2439532
    invoke-static {p1, v0}, LX/HCt;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2439533
    :cond_7
    const-string p0, "profileTabNavigationEditChannel"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2439534
    invoke-static {p1, v0}, LX/HPz;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2439535
    :cond_8
    const-string p0, "services_card"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2439536
    invoke-static {p1, v0}, LX/HCu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2439537
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2439538
    :cond_a
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2439539
    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2439540
    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2439541
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2439542
    if-eqz v1, :cond_b

    .line 2439543
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 2439544
    :cond_b
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2439545
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2439546
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2439547
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2439548
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
