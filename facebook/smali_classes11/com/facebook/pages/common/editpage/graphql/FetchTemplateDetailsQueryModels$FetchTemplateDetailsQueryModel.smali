.class public final Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6e5693cb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440676
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440675
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2440673
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2440674
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440670
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2440671
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2440672
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2440657
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440658
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2440659
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2440660
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2440661
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2440662
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2440663
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440664
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2440668
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->f:Ljava/util/List;

    .line 2440669
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2440677
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440678
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2440679
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2440680
    if-eqz v1, :cond_0

    .line 2440681
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;

    .line 2440682
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->f:Ljava/util/List;

    .line 2440683
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440684
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2440667
    new-instance v0, LX/HCy;

    invoke-direct {v0, p1}, LX/HCy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2440665
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2440666
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2440656
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2440653
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;-><init>()V

    .line 2440654
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2440655
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2440652
    const v0, 0x5c03d64d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2440651
    const v0, 0x252222

    return v0
.end method
