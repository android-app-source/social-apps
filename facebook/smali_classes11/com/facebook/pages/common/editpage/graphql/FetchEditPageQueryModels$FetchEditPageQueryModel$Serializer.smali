.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2439838
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    new-instance v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2439839
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2439840
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2439841
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2439842
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2439843
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2439844
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2439845
    if-eqz v2, :cond_0

    .line 2439846
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439847
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2439848
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439849
    if-eqz v2, :cond_1

    .line 2439850
    const-string p0, "actionBarChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439851
    invoke-static {v1, v2, p1, p2}, LX/HCq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439852
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439853
    if-eqz v2, :cond_2

    .line 2439854
    const-string p0, "addableTabsChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439855
    invoke-static {v1, v2, p1, p2}, LX/HCr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439856
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439857
    if-eqz v2, :cond_3

    .line 2439858
    const-string p0, "commerce_store"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439859
    invoke-static {v1, v2, p1, p2}, LX/HCs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439860
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2439861
    if-eqz v2, :cond_4

    .line 2439862
    const-string p0, "is_service_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439863
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2439864
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2439865
    if-eqz v2, :cond_5

    .line 2439866
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439867
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2439868
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439869
    if-eqz v2, :cond_6

    .line 2439870
    const-string p0, "primaryButtonsChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439871
    invoke-static {v1, v2, p1, p2}, LX/HCt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439872
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439873
    if-eqz v2, :cond_7

    .line 2439874
    const-string p0, "profileTabNavigationEditChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439875
    invoke-static {v1, v2, p1, p2}, LX/HPz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439876
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439877
    if-eqz v2, :cond_8

    .line 2439878
    const-string p0, "services_card"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439879
    invoke-static {v1, v2, p1}, LX/HCu;->a(LX/15i;ILX/0nX;)V

    .line 2439880
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439881
    if-eqz v2, :cond_9

    .line 2439882
    const-string p0, "templates"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439883
    invoke-static {v1, v2, p1, p2}, LX/HCp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439884
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2439885
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2439886
    check-cast p1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Serializer;->a(Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
