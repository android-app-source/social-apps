.class public final Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440999
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440998
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2440996
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2440997
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440994
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;->e:Ljava/lang/String;

    .line 2440995
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2440976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440977
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2440978
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2440979
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2440980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440981
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2440991
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440993
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2441000
    new-instance v0, LX/HDA;

    invoke-direct {v0, p1}, LX/HDA;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440990
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2440988
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2440989
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2440987
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2440984
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel$PageModel;-><init>()V

    .line 2440985
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2440986
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2440983
    const v0, 0x71316a32

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2440982
    const v0, 0x25d6af

    return v0
.end method
