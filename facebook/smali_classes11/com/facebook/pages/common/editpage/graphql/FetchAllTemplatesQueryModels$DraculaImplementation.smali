.class public final Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2438889
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2438890
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2438945
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2438946
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 2438908
    if-nez p1, :cond_0

    .line 2438909
    :goto_0
    return v0

    .line 2438910
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2438911
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2438912
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2438913
    const v2, -0x498a2109

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2438914
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 2438915
    const v3, 0x1f0a349b

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2438916
    invoke-virtual {p0, p1, v9}, LX/15i;->b(II)Z

    move-result v3

    .line 2438917
    invoke-virtual {p0, p1, v10}, LX/15i;->b(II)Z

    move-result v4

    .line 2438918
    invoke-virtual {p0, p1, v11}, LX/15i;->p(II)I

    move-result v5

    .line 2438919
    const v6, 0x474e3a3a

    invoke-static {p0, v5, v6, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    .line 2438920
    const/4 v6, 0x5

    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v6

    .line 2438921
    invoke-virtual {p3, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2438922
    const/4 v7, 0x6

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2438923
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2438924
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2438925
    invoke-virtual {p3, v9, v3}, LX/186;->a(IZ)V

    .line 2438926
    invoke-virtual {p3, v10, v4}, LX/186;->a(IZ)V

    .line 2438927
    invoke-virtual {p3, v11, v5}, LX/186;->b(II)V

    .line 2438928
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 2438929
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2438930
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2438931
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2438932
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2438933
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2438934
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2438935
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2438936
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2438937
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2438938
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2438939
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2438940
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2438941
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2438942
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2438943
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2438944
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x498a2109 -> :sswitch_1
        -0x3aa90cab -> :sswitch_0
        0x1f0a349b -> :sswitch_2
        0x474e3a3a -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2438899
    if-nez p0, :cond_0

    move v0, v1

    .line 2438900
    :goto_0
    return v0

    .line 2438901
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2438902
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2438903
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2438904
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2438905
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2438906
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2438907
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2438892
    const/4 v7, 0x0

    .line 2438893
    const/4 v1, 0x0

    .line 2438894
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2438895
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2438896
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2438897
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2438898
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2438891
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2438880
    sparse-switch p2, :sswitch_data_0

    .line 2438881
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2438882
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2438883
    const v1, -0x498a2109

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2438884
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2438885
    const v1, 0x1f0a349b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2438886
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2438887
    const v1, 0x474e3a3a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2438888
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x498a2109 -> :sswitch_1
        -0x3aa90cab -> :sswitch_0
        0x1f0a349b -> :sswitch_1
        0x474e3a3a -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2438874
    if-eqz p1, :cond_0

    .line 2438875
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2438876
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;

    .line 2438877
    if-eq v0, v1, :cond_0

    .line 2438878
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2438879
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2438873
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2438947
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2438948
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2438868
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2438869
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2438870
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2438871
    iput p2, p0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->b:I

    .line 2438872
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2438867
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2438866
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2438842
    iget v0, p0, LX/1vt;->c:I

    .line 2438843
    move v0, v0

    .line 2438844
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2438863
    iget v0, p0, LX/1vt;->c:I

    .line 2438864
    move v0, v0

    .line 2438865
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2438860
    iget v0, p0, LX/1vt;->b:I

    .line 2438861
    move v0, v0

    .line 2438862
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2438857
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2438858
    move-object v0, v0

    .line 2438859
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2438848
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2438849
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2438850
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2438851
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2438852
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2438853
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2438854
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2438855
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchAllTemplatesQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2438856
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2438845
    iget v0, p0, LX/1vt;->c:I

    .line 2438846
    move v0, v0

    .line 2438847
    return v0
.end method
