.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2439602
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    new-instance v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2439603
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2439604
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2439605
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2439606
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2439607
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2439608
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439609
    if-eqz v2, :cond_0

    .line 2439610
    const-string p0, "actionBarChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439611
    invoke-static {v1, v2, p1, p2}, LX/HCq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439612
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439613
    if-eqz v2, :cond_1

    .line 2439614
    const-string p0, "addableTabsChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439615
    invoke-static {v1, v2, p1, p2}, LX/HCr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439616
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439617
    if-eqz v2, :cond_2

    .line 2439618
    const-string p0, "commerce_store"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439619
    invoke-static {v1, v2, p1, p2}, LX/HCs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439620
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2439621
    if-eqz v2, :cond_3

    .line 2439622
    const-string p0, "is_service_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439623
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2439624
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2439625
    if-eqz v2, :cond_4

    .line 2439626
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439627
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2439628
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439629
    if-eqz v2, :cond_5

    .line 2439630
    const-string p0, "primaryButtonsChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439631
    invoke-static {v1, v2, p1, p2}, LX/HCt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439632
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439633
    if-eqz v2, :cond_6

    .line 2439634
    const-string p0, "profileTabNavigationEditChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439635
    invoke-static {v1, v2, p1, p2}, LX/HPz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2439636
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2439637
    if-eqz v2, :cond_7

    .line 2439638
    const-string p0, "services_card"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439639
    invoke-static {v1, v2, p1}, LX/HCu;->a(LX/15i;ILX/0nX;)V

    .line 2439640
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2439641
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2439642
    check-cast p1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$Serializer;->a(Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
