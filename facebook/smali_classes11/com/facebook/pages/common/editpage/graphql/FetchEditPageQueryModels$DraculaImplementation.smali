.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2439400
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2439401
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2439402
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2439403
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2439369
    if-nez p1, :cond_0

    move v0, v1

    .line 2439370
    :goto_0
    return v0

    .line 2439371
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2439372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2439373
    :sswitch_0
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    .line 2439374
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2439375
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 2439376
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 2439377
    const v4, 0xe00fdd1

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 2439378
    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v4

    .line 2439379
    invoke-virtual {p3, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 2439380
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2439381
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2439382
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 2439383
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 2439384
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2439385
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2439386
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2439387
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2439388
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2439389
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2439390
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2439391
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2439392
    const v2, 0x76a94dc5

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2439393
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2439394
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2439395
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2439396
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v0

    .line 2439397
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2439398
    invoke-virtual {p3, v1, v0}, LX/186;->a(IZ)V

    .line 2439399
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x42917d8 -> :sswitch_0
        0xe00fdd1 -> :sswitch_1
        0x27742c0e -> :sswitch_2
        0x76a94dc5 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2439360
    if-nez p0, :cond_0

    move v0, v1

    .line 2439361
    :goto_0
    return v0

    .line 2439362
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2439363
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2439364
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2439365
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2439366
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2439367
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2439368
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2439353
    const/4 v7, 0x0

    .line 2439354
    const/4 v1, 0x0

    .line 2439355
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2439356
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2439357
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2439358
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2439359
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2439352
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2439347
    if-eqz p0, :cond_0

    .line 2439348
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2439349
    if-eq v0, p0, :cond_0

    .line 2439350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2439351
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2439338
    sparse-switch p2, :sswitch_data_0

    .line 2439339
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2439340
    :sswitch_0
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    .line 2439341
    invoke-static {v0, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2439342
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2439343
    const v1, 0xe00fdd1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2439344
    :goto_0
    :sswitch_1
    return-void

    .line 2439345
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2439346
    const v1, 0x76a94dc5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x42917d8 -> :sswitch_0
        0xe00fdd1 -> :sswitch_1
        0x27742c0e -> :sswitch_2
        0x76a94dc5 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2439332
    if-eqz p1, :cond_0

    .line 2439333
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2439334
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    .line 2439335
    if-eq v0, v1, :cond_0

    .line 2439336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2439337
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2439404
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2439330
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2439331
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2439325
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2439326
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2439327
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2439328
    iput p2, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->b:I

    .line 2439329
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2439324
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2439323
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2439320
    iget v0, p0, LX/1vt;->c:I

    .line 2439321
    move v0, v0

    .line 2439322
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2439317
    iget v0, p0, LX/1vt;->c:I

    .line 2439318
    move v0, v0

    .line 2439319
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2439314
    iget v0, p0, LX/1vt;->b:I

    .line 2439315
    move v0, v0

    .line 2439316
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439311
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2439312
    move-object v0, v0

    .line 2439313
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2439299
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2439300
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2439301
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2439302
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2439303
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2439304
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2439305
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2439306
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2439307
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2439308
    iget v0, p0, LX/1vt;->c:I

    .line 2439309
    move v0, v0

    .line 2439310
    return v0
.end method
