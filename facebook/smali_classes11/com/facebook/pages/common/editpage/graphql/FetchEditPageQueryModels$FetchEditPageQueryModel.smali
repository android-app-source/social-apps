.class public final Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/HCi;
.implements LX/HCh;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x799f496a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439958
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2439948
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2439949
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2439950
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439951
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2439952
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2439953
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439954
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    .line 2439955
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    return-object v0
.end method

.method private l()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439956
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2439957
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommerceStore"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439988
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2439989
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439959
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    .line 2439960
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    return-object v0
.end method

.method private o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439961
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2439962
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    return-object v0
.end method

.method private p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439963
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    .line 2439964
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 2439965
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439966
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2439967
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2439968
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2439969
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x27742c0e

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2439970
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2439971
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2439972
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2439973
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2439974
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->mc_()LX/2uF;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v8

    .line 2439975
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2439976
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 2439977
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2439978
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2439979
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2439980
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2439981
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2439982
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2439983
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2439984
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2439985
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2439986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439987
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2439907
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2439908
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2439909
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    .line 2439910
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2439911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2439912
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    .line 2439913
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2439914
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2439915
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2439916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2439917
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2439918
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2439919
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x27742c0e

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2439920
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2439921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2439922
    iput v3, v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->h:I

    move-object v1, v0

    .line 2439923
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2439924
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    .line 2439925
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2439926
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2439927
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    .line 2439928
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2439929
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2439930
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2439931
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2439932
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2439933
    :cond_4
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2439934
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    .line 2439935
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->p()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2439936
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2439937
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->m:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ServicesCardModel;

    .line 2439938
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->mc_()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2439939
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->mc_()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 2439940
    if-eqz v2, :cond_6

    .line 2439941
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2439942
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n:LX/3Sb;

    move-object v1, v0

    .line 2439943
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2439944
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    .line 2439945
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move-object p0, v1

    .line 2439946
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2439947
    new-instance v0, LX/HCm;

    invoke-direct {v0, p1}, LX/HCm;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439887
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->o()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2439888
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2439889
    const/4 v0, 0x3

    const v1, 0x27742c0e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->h:I

    .line 2439890
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->i:Z

    .line 2439891
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2439892
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2439893
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2439894
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2439895
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;-><init>()V

    .line 2439896
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2439897
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439906
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439898
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->l()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439899
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->j:Ljava/lang/String;

    .line 2439900
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2439901
    const v0, -0x18bc5234

    return v0
.end method

.method public final synthetic e()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2439902
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2439903
    const v0, 0x252222

    return v0
.end method

.method public final mc_()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTemplates"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2439904
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x9

    const v4, -0x42917d8

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n:LX/3Sb;

    .line 2439905
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->n:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method
