.class public final Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2d3e5715
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440570
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2440569
    const-class v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2440649
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2440650
    return-void
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440647
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 2440648
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2440627
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440628
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2440629
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2440630
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2440631
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2440632
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->n()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 2440633
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->o()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2440634
    invoke-direct {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->p()Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2440635
    const/16 v7, 0x9

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2440636
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2440637
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2440638
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2440639
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2440640
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2440641
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2440642
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2440643
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2440644
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2440645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440646
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2440625
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->e:Ljava/util/List;

    .line 2440626
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2440592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2440593
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2440594
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2440595
    if-eqz v1, :cond_6

    .line 2440596
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    .line 2440597
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2440598
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2440599
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    .line 2440600
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2440601
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    .line 2440602
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    .line 2440603
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2440604
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    .line 2440605
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2440606
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    .line 2440607
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    .line 2440608
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2440609
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    .line 2440610
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2440611
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    .line 2440612
    iput-object v0, v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    .line 2440613
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2440614
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2440615
    if-eqz v2, :cond_3

    .line 2440616
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    .line 2440617
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k:Ljava/util/List;

    move-object v1, v0

    .line 2440618
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2440619
    invoke-virtual {p0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2440620
    if-eqz v2, :cond_4

    .line 2440621
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    .line 2440622
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->l:Ljava/util/List;

    move-object v1, v0

    .line 2440623
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2440624
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2440588
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2440589
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->h:Z

    .line 2440590
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->i:Z

    .line 2440591
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2440585
    new-instance v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;-><init>()V

    .line 2440586
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2440587
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2440584
    const v0, -0x4ab6cb23

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2440583
    const v0, 0x49241c03

    return v0
.end method

.method public final j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440581
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    .line 2440582
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->f:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440579
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    .line 2440580
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->g:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2440577
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2440578
    iget-boolean v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->h:Z

    return v0
.end method

.method public final m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2440575
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    .line 2440576
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j:Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2440573
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k:Ljava/util/List;

    .line 2440574
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2440571
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->l:Ljava/util/List;

    .line 2440572
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
