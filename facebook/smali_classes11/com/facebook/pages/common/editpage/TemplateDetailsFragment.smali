.class public Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/H9E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:J

.field public g:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public i:Lcom/facebook/resources/ui/FbTextView;

.field public j:Lcom/facebook/resources/ui/FbTextView;

.field public k:Landroid/widget/LinearLayout;

.field public l:Landroid/widget/LinearLayout;

.field public m:Landroid/widget/LinearLayout;

.field public n:Lcom/facebook/fig/button/FigButton;

.field public o:Lcom/facebook/fig/button/FigButton;

.field public p:LX/H9D;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2438828
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;LX/0Px;Landroid/widget/LinearLayout;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;",
            ">;",
            "Landroid/widget/LinearLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2438770
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2438771
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_5

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 2438772
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ADD_BUTTON:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v1, v4, :cond_1

    .line 2438773
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gA_()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gA_()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, ""

    .line 2438774
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->p()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->p()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2438775
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->p()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1, p2}, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;Ljava/lang/String;Ljava/lang/String;Landroid/widget/LinearLayout;)V

    .line 2438776
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2438777
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gA_()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2438778
    :cond_3
    const/4 v5, -0x1

    .line 2438779
    iget-object v4, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->p:LX/H9D;

    if-nez v4, :cond_4

    .line 2438780
    iget-object v4, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->c:LX/H9E;

    .line 2438781
    iget-object v6, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v6, v6

    .line 2438782
    invoke-virtual {v4, v6}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->p:LX/H9D;

    .line 2438783
    :cond_4
    instance-of v4, v0, LX/9Y8;

    if-eqz v4, :cond_8

    .line 2438784
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    .line 2438785
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v4, v6, :cond_7

    move v4, v5

    .line 2438786
    :goto_3
    move v0, v4

    .line 2438787
    const/4 v4, -0x1

    if-eq v0, v4, :cond_1

    .line 2438788
    new-instance v4, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2438789
    invoke-virtual {v4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2438790
    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2438791
    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2438792
    invoke-virtual {p2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2438793
    goto :goto_2

    .line 2438794
    :cond_5
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_6

    .line 2438795
    new-instance v0, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2438796
    const v1, 0x7f0836e4

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2438797
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2438798
    :cond_6
    return-void

    .line 2438799
    :cond_7
    invoke-static {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v6

    .line 2438800
    new-instance v7, LX/9YA;

    invoke-direct {v7}, LX/9YA;-><init>()V

    .line 2438801
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2438802
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2438803
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->t()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2438804
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->u()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->d:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    .line 2438805
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->e:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    .line 2438806
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->v()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2438807
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->w()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    .line 2438808
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->x()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    .line 2438809
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->y()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    .line 2438810
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->j:Ljava/lang/String;

    .line 2438811
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->z()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2438812
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->l:Ljava/lang/String;

    .line 2438813
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->m:Ljava/lang/String;

    .line 2438814
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->A()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->n:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    .line 2438815
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->o:Ljava/lang/String;

    .line 2438816
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->r()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->p:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2438817
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, LX/9YA;->q:Ljava/lang/String;

    .line 2438818
    move-object v6, v7

    .line 2438819
    iput-object v4, v6, LX/9YA;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2438820
    move-object v4, v6

    .line 2438821
    invoke-virtual {v4}, LX/9YA;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    .line 2438822
    iget-object v6, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->p:LX/H9D;

    invoke-virtual {v6, v4}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v4

    .line 2438823
    instance-of v6, v4, LX/H8Z;

    if-eqz v6, :cond_8

    .line 2438824
    check-cast v4, LX/H8Z;

    invoke-interface {v4}, LX/H8Z;->a()LX/HA7;

    move-result-object v4

    .line 2438825
    iget v5, v4, LX/HA7;->b:I

    move v4, v5

    .line 2438826
    goto/16 :goto_3

    :cond_8
    move v4, v5

    .line 2438827
    goto/16 :goto_3
.end method

.method public static a(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;Ljava/lang/String;Ljava/lang/String;Landroid/widget/LinearLayout;)V
    .locals 2

    .prologue
    .line 2438735
    new-instance v0, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2438736
    invoke-virtual {v0, p1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2438737
    invoke-virtual {v0, p2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2438738
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2438739
    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2438740
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;LX/9X7;)V
    .locals 6

    .prologue
    .line 2438829
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-wide v2, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->f:J

    iget-object v4, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->g:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v5, "templates"

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/9XE;->a(LX/9X7;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Ljava/lang/String;)V

    .line 2438830
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2438759
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2438760
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v4, p0

    check-cast v4, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    const/16 v5, 0xafd

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x12b1

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, LX/H9E;

    invoke-interface {p1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/H9E;

    const/16 v8, 0x259

    invoke-static {p1, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v0, 0x2b68

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v5, v4, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a:LX/0Ot;

    iput-object v6, v4, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->b:LX/0Ot;

    iput-object v7, v4, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->c:LX/H9E;

    iput-object v8, v4, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->d:LX/0Ot;

    iput-object p1, v4, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->e:LX/0Ot;

    .line 2438761
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2438762
    const-string v3, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->f:J

    .line 2438763
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2438764
    const-string v3, "template_type"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->g:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 2438765
    iget-wide v4, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->f:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2438766
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->g:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->g:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    if-eq v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 2438767
    return-void

    :cond_0
    move v0, v2

    .line 2438768
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2438769
    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x49687ebe

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2438758
    const v1, 0x7f03148d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6310a574

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2438741
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2438742
    const v0, 0x7f0d2eb9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2438743
    const v0, 0x7f0d2ebc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2438744
    const v0, 0x7f0d2ebd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2438745
    const v0, 0x7f0d2ebe

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->k:Landroid/widget/LinearLayout;

    .line 2438746
    const v0, 0x7f0d2ebf

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->l:Landroid/widget/LinearLayout;

    .line 2438747
    const v0, 0x7f0d2ec0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->m:Landroid/widget/LinearLayout;

    .line 2438748
    const v0, 0x7f0d2ec1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->n:Lcom/facebook/fig/button/FigButton;

    .line 2438749
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->n:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/HCT;

    invoke-direct {v1, p0}, LX/HCT;-><init>(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2438750
    const v0, 0x7f0d2ec2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->o:Lcom/facebook/fig/button/FigButton;

    .line 2438751
    iget-object v0, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "fetch_template_datains_request"

    .line 2438752
    new-instance v4, LX/HCw;

    invoke-direct {v4}, LX/HCw;-><init>()V

    move-object v4, v4

    .line 2438753
    const-string v5, "page_id"

    iget-wide v6, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "template_type"

    iget-object v6, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->g:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v5, "template_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v5, "cta_icon_size"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0f8e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "cta_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/HCw;

    .line 2438754
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2438755
    iget-object v4, p0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2438756
    new-instance v3, LX/HCU;

    invoke-direct {v3, p0}, LX/HCU;-><init>(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2438757
    return-void
.end method
