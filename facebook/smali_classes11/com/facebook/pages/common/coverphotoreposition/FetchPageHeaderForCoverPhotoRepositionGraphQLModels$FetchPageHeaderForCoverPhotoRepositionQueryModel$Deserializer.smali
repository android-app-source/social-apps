.class public final Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2437491
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;

    new-instance v1, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2437492
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2437493
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2437494
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2437495
    const/4 v2, 0x0

    .line 2437496
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 2437497
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2437498
    :goto_0
    move v1, v2

    .line 2437499
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2437500
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2437501
    new-instance v1, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;-><init>()V

    .line 2437502
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2437503
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2437504
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2437505
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2437506
    :cond_0
    return-object v1

    .line 2437507
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2437508
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2437509
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2437510
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2437511
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, p0, :cond_2

    if-eqz v5, :cond_2

    .line 2437512
    const-string v6, "category_names"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2437513
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2437514
    :cond_3
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2437515
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2437516
    :cond_4
    const-string v6, "profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2437517
    const/4 v5, 0x0

    .line 2437518
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_a

    .line 2437519
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2437520
    :goto_2
    move v1, v5

    .line 2437521
    goto :goto_1

    .line 2437522
    :cond_5
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2437523
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2437524
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2437525
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2437526
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1

    .line 2437527
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2437528
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_9

    .line 2437529
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2437530
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2437531
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_8

    if-eqz v6, :cond_8

    .line 2437532
    const-string p0, "uri"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2437533
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 2437534
    :cond_9
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2437535
    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2437536
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_a
    move v1, v5

    goto :goto_3
.end method
