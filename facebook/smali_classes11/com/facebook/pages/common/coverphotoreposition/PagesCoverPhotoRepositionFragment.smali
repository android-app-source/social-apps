.class public Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/BP1;


# static fields
.field private static final n:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:LX/434;

.field private c:J

.field public d:Ljava/lang/String;

.field public e:LX/5SB;

.field public f:LX/BPz;

.field public g:LX/03V;

.field public h:LX/2Qx;

.field public i:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation
.end field

.field public j:LX/0tX;

.field public k:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private l:Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

.field private m:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2437690
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    sput-object v0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->n:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2437691
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2437692
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->c:J

    .line 2437693
    return-void
.end method

.method public static a(JLjava/lang/String;J)Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;
    .locals 3

    .prologue
    .line 2437694
    new-instance v0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;-><init>()V

    .line 2437695
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2437696
    const-string v2, "cover_photo_id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2437697
    const-string v2, "cover_photo_uri"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2437698
    const-string v2, "page_id"

    invoke-virtual {v1, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2437699
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2437700
    return-object v0
.end method

.method public static b(Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;)V
    .locals 2

    .prologue
    .line 2437701
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->l:Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->f:LX/BPz;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->a(LX/BPz;)Z

    .line 2437702
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;
    .locals 11

    .prologue
    .line 2437703
    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->e:LX/5SB;

    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v2, -0x1

    .line 2437704
    :goto_0
    new-instance v1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    iget-object v4, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->m:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-virtual {v5}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getNormalizedCropBounds()Landroid/graphics/RectF;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->b:LX/434;

    iget-wide v7, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->c:J

    invoke-direct/range {v1 .. v8}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;-><init>(JLjava/lang/String;Landroid/graphics/RectF;LX/434;J)V

    return-object v1

    .line 2437705
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->e:LX/5SB;

    .line 2437706
    iget-wide v9, v1, LX/5SB;->b:J

    move-wide v2, v9

    .line 2437707
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 2437708
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2437709
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2437710
    const-string v1, "cover_photo_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->a:Ljava/lang/String;

    .line 2437711
    const-string v1, "cover_photo_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->c:J

    .line 2437712
    const-string v1, "page_id"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-nez v1, :cond_2

    move-object v0, v4

    :goto_0
    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->d:Ljava/lang/String;

    .line 2437713
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2437714
    :cond_0
    sget-object v0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->n:Ljava/lang/Class;

    const-string v1, "Missing required arguments."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2437715
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2437716
    :cond_1
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    move-result-object v5

    check-cast v5, LX/2Qx;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v3, v2, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->g:LX/03V;

    iput-object v5, v2, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->h:LX/2Qx;

    iput-object v6, v2, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->i:Ljava/lang/String;

    iput-object v7, v2, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->j:LX/0tX;

    iput-object v0, v2, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->k:Ljava/util/concurrent/ExecutorService;

    .line 2437717
    :try_start_0
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->i:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2437718
    :goto_1
    if-eqz p1, :cond_3

    .line 2437719
    const-string v2, "fragment_uuid"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/ParcelUuid;

    move-object v5, v2

    .line 2437720
    :goto_2
    iget-object v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object v6, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, LX/HBS;->a(JJLjava/lang/String;Landroid/os/ParcelUuid;Ljava/lang/String;LX/8A4;)LX/HBS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->e:LX/5SB;

    .line 2437721
    new-instance v0, LX/HBZ;

    invoke-direct {v0}, LX/HBZ;-><init>()V

    move-object v0, v0

    .line 2437722
    const-string v1, "page_id"

    iget-object v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2437723
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2437724
    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->j:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2437725
    new-instance v1, LX/HBe;

    invoke-direct {v1, p0}, LX/HBe;-><init>(Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;)V

    iget-object v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->k:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2437726
    return-void

    .line 2437727
    :cond_2
    const-string v1, "page_id"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2437728
    :catch_0
    const-wide/16 v0, -0x1

    .line 2437729
    iget-object v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->g:LX/03V;

    const-string v3, "timeline_invalid_meuser"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "logged in user: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2437730
    :cond_3
    new-instance v5, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-direct {v5, v2}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    goto :goto_2
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2437731
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->l:Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->f:LX/BPz;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->a(LX/BPz;)Z

    .line 2437732
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3935e463

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2437733
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->a:Ljava/lang/String;

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->b:LX/434;

    .line 2437734
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->b:LX/434;

    invoke-static {v0}, LX/8K7;->a(LX/434;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2437735
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0815be

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2437736
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2437737
    if-eqz v0, :cond_0

    .line 2437738
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 2437739
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2437740
    :cond_0
    new-instance v0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    iget-object v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->a:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->l:Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    .line 2437741
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->f:LX/BPz;

    if-eqz v0, :cond_1

    .line 2437742
    invoke-static {p0}, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->b(Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;)V

    .line 2437743
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->l:Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    const v2, 0x7f0d22cc

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->m:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    .line 2437744
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->l:Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    const/16 v2, 0x2b

    const v3, 0x4a8b0ca8    # 4556372.0f

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a308230

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2437745
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2437746
    const/16 v1, 0x2b

    const v2, 0x2a0931dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x622ca871

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2437747
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2437748
    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->m:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a()V

    .line 2437749
    const/16 v1, 0x2b

    const v2, 0x1af8594c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2437750
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2437751
    const-string v0, "fragment_uuid"

    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->e:LX/5SB;

    .line 2437752
    iget-object p0, v1, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v1, p0

    .line 2437753
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2437754
    return-void
.end method
