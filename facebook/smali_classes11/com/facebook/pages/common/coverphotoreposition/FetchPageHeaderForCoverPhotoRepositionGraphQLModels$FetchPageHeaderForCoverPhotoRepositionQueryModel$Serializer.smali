.class public final Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2437539
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;

    new-instance v1, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2437540
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2437541
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2437542
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2437543
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2437544
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2437545
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2437546
    if-eqz v2, :cond_0

    .line 2437547
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2437548
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2437549
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2437550
    if-eqz v2, :cond_1

    .line 2437551
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2437552
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2437553
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2437554
    if-eqz v2, :cond_3

    .line 2437555
    const-string p0, "profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2437556
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2437557
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2437558
    if-eqz p0, :cond_2

    .line 2437559
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2437560
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2437561
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2437562
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2437563
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2437564
    check-cast p1, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Serializer;->a(Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
