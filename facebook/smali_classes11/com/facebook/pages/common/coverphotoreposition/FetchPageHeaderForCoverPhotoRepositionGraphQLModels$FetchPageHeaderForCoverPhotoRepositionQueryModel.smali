.class public final Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7676d608
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2437603
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2437602
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2437600
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2437601
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2437590
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2437591
    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 2437592
    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2437593
    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x7b0d57f0

    invoke-static {v3, v2, v4}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2437594
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2437595
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2437596
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2437597
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2437598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2437599
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2437588
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->e:Ljava/util/List;

    .line 2437589
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2437578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2437579
    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2437580
    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7b0d57f0

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2437581
    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2437582
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;

    .line 2437583
    iput v3, v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->g:I

    .line 2437584
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2437585
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2437586
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2437587
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2437577
    new-instance v0, LX/HBb;

    invoke-direct {v0, p1}, LX/HBb;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2437604
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2437605
    const/4 v0, 0x2

    const v1, -0x7b0d57f0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->g:I

    .line 2437606
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2437575
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2437576
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2437574
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2437571
    new-instance v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;-><init>()V

    .line 2437572
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2437573
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2437570
    const v0, -0x379ec27e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2437569
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2437567
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->f:Ljava/lang/String;

    .line 2437568
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2437565
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2437566
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
