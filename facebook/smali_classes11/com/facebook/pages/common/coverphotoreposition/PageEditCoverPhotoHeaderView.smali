.class public Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static a:D

.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/0PO;

.field private final c:Ljava/lang/String;

.field public d:LX/1Ad;

.field public f:LX/0hB;

.field public g:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2437611
    const-wide v0, 0x3ffc7ae147ae147bL    # 1.78

    sput-wide v0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->a:D

    .line 2437612
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    const-string v1, "pages_identity"

    const-string v2, "profile_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2437613
    invoke-direct {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2437614
    iput-object p1, p0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->c:Ljava/lang/String;

    .line 2437615
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2437616
    const v0, 0x7f030e46

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2437617
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object v1, p1, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->f:LX/0hB;

    iput-object v2, p1, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->g:Landroid/content/res/Resources;

    iput-object p0, p1, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->d:LX/1Ad;

    return-void
.end method


# virtual methods
.method public final a(LX/BPz;)Z
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2437618
    iget-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->f:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v2

    .line 2437619
    int-to-double v4, v2

    sget-wide v6, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->a:D

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    .line 2437620
    const v0, 0x7f0d22cc

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    .line 2437621
    iget-object v5, p0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->c:Ljava/lang/String;

    invoke-virtual {v0, v5, v2, v4}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a(Ljava/lang/String;II)V

    .line 2437622
    const v0, 0x7f0d22cb

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2437623
    iget-object v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->g:Landroid/content/res/Resources;

    const v4, 0x7f021396

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2437624
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 2437625
    const v0, 0x7f0d22ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 2437626
    iget-boolean v0, p1, LX/BPz;->a:Z

    move v0, v0

    .line 2437627
    iget-object v2, p1, LX/BPz;->e:Ljava/lang/String;

    move-object v2, v2

    .line 2437628
    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v10, v2, v3, v1, v4}, LX/EQR;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const v4, 0x7f021a24

    const v5, 0x7f021afe

    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0d94

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0dcb

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sget-object v9, LX/03R;->UNSET:LX/03R;

    invoke-static {v9}, LX/52d;->a(Ljava/lang/Object;)LX/0Or;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2437629
    const-string v0, " \u00b7 "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->b:LX/0PO;

    .line 2437630
    const v0, 0x7f0d22cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2437631
    iget-object v1, p1, LX/BPz;->f:LX/0Px;

    move-object v1, v1

    .line 2437632
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2437633
    iget-object v2, p0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->b:LX/0PO;

    invoke-virtual {v2, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 2437634
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2437635
    :cond_0
    const v0, 0x7f0d22cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2437636
    iget-object v1, p0, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->d:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v2, Lcom/facebook/pages/common/coverphotoreposition/PageEditCoverPhotoHeaderView;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    .line 2437637
    iget-object v2, p1, LX/BPz;->c:Lcom/facebook/graphql/model/GraphQLImage;

    move-object v2, v2

    .line 2437638
    if-eqz v2, :cond_1

    .line 2437639
    iget-object v2, p1, LX/BPz;->c:Lcom/facebook/graphql/model/GraphQLImage;

    move-object v2, v2

    .line 2437640
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v2, v2

    .line 2437641
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    .line 2437642
    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2437643
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
