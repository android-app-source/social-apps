.class public Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/ui/titlebar/Fb4aTitleBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2434799
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2434800
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;->p:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2434801
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;->p:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    if-nez v0, :cond_0

    .line 2434802
    :goto_0
    return-void

    .line 2434803
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;->p:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f082f2c

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2434804
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;->p:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/HA1;

    invoke-direct {v1, p0}, LX/HA1;-><init>(Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;)V
    .locals 0

    .prologue
    .line 2434805
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2434806
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2434807
    new-instance v0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;-><init>()V

    .line 2434808
    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2434809
    const v1, 0x7f030037

    invoke-virtual {p0, v1}, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;->setContentView(I)V

    .line 2434810
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageActivity;->a()V

    .line 2434811
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d03b1

    const-string v3, "chromeless:content:fragment:tag"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2434812
    return-void
.end method
