.class public Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2434813
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;-><init>()V

    .line 2434814
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2434815
    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;->a:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;

    const/16 p0, 0x3096

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2434816
    const-string v0, "pages_page_promote"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2434817
    const-class v0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2434818
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2434819
    const-string v1, "page_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;->i:J

    .line 2434820
    iget-wide v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Page Id Not Found"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2434821
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2434822
    return-void

    .line 2434823
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()LX/2jY;
    .locals 6

    .prologue
    .line 2434824
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfW;

    const-string v1, "ANDROID_PAGE_PROMOTE"

    new-instance v2, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v2}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-wide v4, p0, Lcom/facebook/pages/common/actionchannel/actions/promotepage/NativePromotePageFragment;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2434825
    iput-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2434826
    move-object v2, v2

    .line 2434827
    invoke-virtual {v0, v1, v2}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    return-object v0
.end method
