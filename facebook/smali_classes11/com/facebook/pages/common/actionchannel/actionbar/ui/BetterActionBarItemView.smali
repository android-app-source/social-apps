.class public Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/widget/LinearLayout;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field public d:Lcom/facebook/fbui/glyph/GlyphView;

.field public e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2433318
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2433319
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->a(Landroid/content/Context;)V

    .line 2433320
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2433321
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2433322
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->a(Landroid/content/Context;)V

    .line 2433323
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2433324
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2433325
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->a(Landroid/content/Context;)V

    .line 2433326
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2433327
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->a:Landroid/content/Context;

    .line 2433328
    const v0, 0x7f030196

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2433329
    const v0, 0x7f0d06e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->b:Landroid/widget/LinearLayout;

    .line 2433330
    const v0, 0x7f0d06ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2433331
    const v0, 0x7f0d06eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2433332
    const v0, 0x7f0d06ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2433333
    return-void
.end method


# virtual methods
.method public final a(LX/HA7;Landroid/view/View$OnClickListener;LX/HA6;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2433334
    if-eqz p1, :cond_0

    .line 2433335
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2433336
    iget v1, p1, LX/HA7;->d:I

    move v1, v1

    .line 2433337
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2433338
    iget-object v0, p1, LX/HA7;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2433339
    if-eqz v0, :cond_1

    .line 2433340
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2433341
    iget-object v1, p1, LX/HA7;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2433342
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2433343
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2433344
    if-eqz p2, :cond_2

    .line 2433345
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->b:Landroid/widget/LinearLayout;

    invoke-static {v0}, LX/8FY;->a(Landroid/view/View;)V

    .line 2433346
    :goto_1
    invoke-static {p3}, LX/H8X;->a(LX/HA6;)I

    move-result v0

    .line 2433347
    if-lez v0, :cond_3

    .line 2433348
    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2433349
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2433350
    sget-object v2, LX/HA6;->EDITABLE:LX/HA6;

    if-ne p3, v2, :cond_4

    .line 2433351
    const v2, 0x7f0a008b

    .line 2433352
    :goto_2
    move v2, v2

    .line 2433353
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2433354
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2433355
    :cond_0
    :goto_3
    return-void

    .line 2433356
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2433357
    iget v1, p1, LX/HA7;->b:I

    move v1, v1

    .line 2433358
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0

    .line 2433359
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2433360
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 2433361
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_3

    .line 2433362
    :cond_4
    sget-object v2, LX/HA6;->INCOMPLETE:LX/HA6;

    if-ne p3, v2, :cond_5

    .line 2433363
    const v2, 0x7f0a0095

    goto :goto_2

    .line 2433364
    :cond_5
    const v2, 0x7f0a0099

    goto :goto_2
.end method
