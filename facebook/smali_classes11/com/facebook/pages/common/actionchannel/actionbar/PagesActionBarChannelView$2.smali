.class public final Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/H8M;


# direct methods
.method public constructor <init>(LX/H8M;)V
    .locals 0

    .prologue
    .line 2433124
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;->a:LX/H8M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2433125
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;->a:LX/H8M;

    iget-object v0, v0, LX/H8M;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2433126
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;->a:LX/H8M;

    iget-object v1, v1, LX/H8M;->i:Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2433127
    new-instance v3, LX/0hs;

    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;->a:LX/H8M;

    iget-object v1, v1, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v1}, Lcom/facebook/fig/actionbar/FigActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x2

    invoke-direct {v3, v1, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2433128
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v3, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2433129
    const/4 v1, -0x1

    .line 2433130
    iput v1, v3, LX/0hs;->t:I

    .line 2433131
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v3, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2433132
    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;->a:LX/H8M;

    iget-object v4, v1, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/facebook/fig/actionbar/FigActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2433133
    if-eqz v1, :cond_0

    .line 2433134
    invoke-virtual {v3, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2433135
    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;->a:LX/H8M;

    iget-object v1, v1, LX/H8M;->i:Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2433136
    :cond_1
    return-void
.end method
