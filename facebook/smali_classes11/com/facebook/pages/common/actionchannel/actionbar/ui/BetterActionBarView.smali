.class public Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2433384
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2433385
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->a(Landroid/content/Context;)V

    .line 2433386
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2433365
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2433366
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->a(Landroid/content/Context;)V

    .line 2433367
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2433381
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2433382
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->a(Landroid/content/Context;)V

    .line 2433383
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2433387
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->a:Landroid/content/Context;

    .line 2433388
    const v0, 0x7f030197

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2433389
    const v0, 0x7f0d06ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;

    const v1, 0x7f0d06ee

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;

    const v2, 0x7f0d06ef

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;

    const v3, 0x7f0d06f0

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->b:LX/0Px;

    .line 2433390
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/H8O;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2433369
    invoke-virtual {p1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v2

    .line 2433370
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v3

    .line 2433371
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2433372
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;

    .line 2433373
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H8O;

    .line 2433374
    iget-object v4, v1, LX/H8O;->a:LX/HA7;

    move-object v4, v4

    .line 2433375
    iget-object v5, v1, LX/H8O;->b:Landroid/view/View$OnClickListener;

    move-object v5, v5

    .line 2433376
    iget-object p0, v1, LX/H8O;->c:LX/HA6;

    move-object v1, p0

    .line 2433377
    invoke-virtual {v0, v4, v5, v1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->a(LX/HA7;Landroid/view/View$OnClickListener;LX/HA6;)V

    goto :goto_0

    .line 2433378
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2433379
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarItemView;->setVisibility(I)V

    goto :goto_1

    .line 2433380
    :cond_1
    return-void
.end method

.method public getNumberItemViews()I
    .locals 1

    .prologue
    .line 2433368
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
