.class public final Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2435208
    const-class v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel;

    new-instance v1, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2435209
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2435210
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2435211
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2435212
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2435213
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2435214
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2435215
    if-eqz v2, :cond_0

    .line 2435216
    const-string p0, "user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2435217
    invoke-static {v1, v2, p1}, LX/HAS;->a(LX/15i;ILX/0nX;)V

    .line 2435218
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2435219
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2435220
    check-cast p1, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$Serializer;->a(Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
