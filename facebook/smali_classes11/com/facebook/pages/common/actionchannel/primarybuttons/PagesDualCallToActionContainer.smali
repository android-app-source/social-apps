.class public Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/H8L;


# static fields
.field private static final k:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static final l:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/H9E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/CYM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HAF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/HDT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Landroid/content/Context;

.field private n:LX/H9D;

.field private o:Landroid/os/ParcelUuid;

.field private final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/HAH;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/fig/button/FigButton;",
            "LX/HAM;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2435115
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ACTIONBAR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->k:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 2435116
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->l:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2435118
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2435119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->p:Ljava/util/ArrayList;

    .line 2435120
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Landroid/content/Context;)V

    .line 2435121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2435122
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2435123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->p:Ljava/util/ArrayList;

    .line 2435124
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Landroid/content/Context;)V

    .line 2435125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2435126
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2435127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->p:Ljava/util/ArrayList;

    .line 2435128
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Landroid/content/Context;)V

    .line 2435129
    return-void
.end method

.method private a(JLcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;I)Landroid/view/View$OnClickListener;
    .locals 7

    .prologue
    .line 2435130
    sget-object v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->l:LX/0Rf;

    invoke-virtual {p3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2435131
    new-instance v0, LX/HAJ;

    invoke-direct {v0, p0}, LX/HAJ;-><init>(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;)V

    .line 2435132
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/HAK;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/HAK;-><init>(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;JLcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2435155
    const-class v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2435156
    const v0, 0x7f030ecb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2435157
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->setOrientation(I)V

    .line 2435158
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->m:Landroid/content/Context;

    .line 2435159
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->c:LX/H9E;

    invoke-virtual {v0, p0}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->n:LX/H9D;

    .line 2435160
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->g:LX/CYM;

    .line 2435161
    iget-object p1, v0, LX/CYM;->e:LX/10U;

    move-object v0, p1

    .line 2435162
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 2435163
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->q:Ljava/util/HashMap;

    .line 2435164
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->c()V

    .line 2435165
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2435133
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    sget-object v1, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->k:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3kd;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kd;

    .line 2435134
    if-nez v0, :cond_0

    .line 2435135
    :goto_0
    return-void

    .line 2435136
    :cond_0
    new-instance v1, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2435137
    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 2435138
    const/4 v2, -0x1

    .line 2435139
    iput v2, v1, LX/0hs;->t:I

    .line 2435140
    const v2, 0x7f08182a

    invoke-virtual {v1, v2}, LX/0hs;->b(I)V

    .line 2435141
    invoke-virtual {v1, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2435142
    new-instance v2, LX/HAL;

    invoke-direct {v2, p0, v0}, LX/HAL;-><init>(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;LX/3kd;)V

    .line 2435143
    iput-object v2, v1, LX/0ht;->H:LX/2dD;

    .line 2435144
    goto :goto_0
.end method

.method private a(Lcom/facebook/fig/button/FigButton;LX/H8Z;LX/HAM;)V
    .locals 1

    .prologue
    .line 2435145
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->i:LX/HAF;

    invoke-virtual {v0, p2}, LX/HAF;->a(LX/H8Y;)V

    .line 2435146
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->q:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2435147
    return-void
.end method

.method private static a(Lcom/facebook/fig/button/FigButton;LX/HA6;LX/H8Z;)V
    .locals 1

    .prologue
    .line 2435148
    invoke-interface {p2}, LX/H8Z;->a()LX/HA7;

    move-result-object v0

    .line 2435149
    iget p2, v0, LX/HA7;->b:I

    move v0, p2

    .line 2435150
    invoke-virtual {p0, v0}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2435151
    invoke-static {p1}, LX/H8X;->a(LX/HA6;)I

    move-result v0

    .line 2435152
    if-lez v0, :cond_0

    .line 2435153
    invoke-virtual {p0, v0}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2435154
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;LX/0Ot;LX/0Ot;LX/H9E;LX/0Uh;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/CYM;LX/0Ot;LX/HAF;LX/HDT;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/H9E;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/CYM;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/HAF;",
            "LX/HDT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2435117
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->c:LX/H9E;

    iput-object p4, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->d:LX/0Uh;

    iput-object p5, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->e:LX/17Y;

    iput-object p6, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object p7, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->g:LX/CYM;

    iput-object p8, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->h:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->i:LX/HAF;

    iput-object p10, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->j:LX/HDT;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    const/16 v1, 0x259

    invoke-static {v10, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xbd2

    invoke-static {v10, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const-class v3, LX/H9E;

    invoke-interface {v10, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/H9E;

    invoke-static {v10}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v10}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    new-instance v8, LX/CYM;

    invoke-static {v10}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v7

    check-cast v7, LX/CXj;

    invoke-direct {v8, v7}, LX/CYM;-><init>(LX/CXj;)V

    move-object v7, v8

    check-cast v7, LX/CYM;

    const/16 v8, 0x12c4

    invoke-static {v10, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v10}, LX/HAF;->a(LX/0QB;)LX/HAF;

    move-result-object v9

    check-cast v9, LX/HAF;

    invoke-static {v10}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v10

    check-cast v10, LX/HDT;

    invoke-static/range {v0 .. v10}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;LX/0Ot;LX/0Ot;LX/H9E;LX/0Uh;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/CYM;LX/0Ot;LX/HAF;LX/HDT;)V

    return-void
.end method

.method private a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2435033
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, -0x7ceb8589

    if-ne v2, v3, :cond_2

    .line 2435034
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2435035
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2435036
    goto :goto_0

    .line 2435037
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->n:LX/H9D;

    invoke-virtual {v2, p1}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2435038
    invoke-static {p0}, LX/HAF;->a(LX/H8L;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->r:LX/0Px;

    .line 2435039
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->r:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->r:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8E;

    .line 2435040
    iget-object v3, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->j:LX/HDT;

    invoke-virtual {v3, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2435041
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2435042
    :cond_0
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2435043
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HAM;

    .line 2435044
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->i:LX/HAF;

    iget-object v0, v0, LX/HAM;->b:LX/H8Z;

    invoke-virtual {v2, v0}, LX/HAF;->b(LX/H8Y;)V

    goto :goto_0

    .line 2435045
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2435046
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2435047
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2435048
    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->q:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HAM;

    .line 2435049
    iget-object v3, v1, LX/HAM;->a:LX/HA6;

    iget-object v1, v1, LX/HAM;->b:LX/H8Z;

    invoke-static {v0, v3, v1}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Lcom/facebook/fig/button/FigButton;LX/HA6;LX/H8Z;)V

    goto :goto_0

    .line 2435050
    :cond_0
    return-void
.end method

.method public final a(JLX/0Px;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2435051
    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0064

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 2435052
    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->removeAllViews()V

    .line 2435053
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->d()V

    .line 2435054
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->g:LX/CYM;

    invoke-virtual {v2}, LX/CYM;->a()V

    .line 2435055
    const/4 v7, 0x0

    :goto_0
    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v2

    if-ge v7, v2, :cond_9

    .line 2435056
    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 2435057
    invoke-direct {p0, v9}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2435058
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v4, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2435059
    const/16 v2, 0x88

    .line 2435060
    if-nez v7, :cond_0

    .line 2435061
    const/16 v2, 0x18

    .line 2435062
    :cond_0
    invoke-virtual {v3, v10, v10, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2435063
    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, -0x7ceb8589

    if-ne v4, v5, :cond_5

    .line 2435064
    new-instance v11, LX/HAH;

    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v11, v4}, LX/HAH;-><init>(Landroid/content/Context;)V

    .line 2435065
    invoke-virtual {p0, v11}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->addView(Landroid/view/View;)V

    .line 2435066
    invoke-virtual {v11, v3}, LX/HAH;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2435067
    invoke-virtual {v11, v2}, LX/HAH;->setButtonType(I)V

    .line 2435068
    move/from16 v0, p5

    invoke-static {v9, v0}, LX/H8X;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;Z)LX/HA6;

    move-result-object v2

    invoke-static {v2}, LX/H8X;->a(LX/HA6;)I

    move-result v2

    invoke-virtual {v11, v2}, LX/HAH;->setButtonRes(I)V

    .line 2435069
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->d:LX/0Uh;

    sget v3, LX/8Dm;->i:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->d:LX/0Uh;

    sget v3, LX/8Dm;->l:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v3, 0x1

    .line 2435070
    :goto_1
    new-instance v2, LX/CY6;

    invoke-direct {v2}, LX/CY6;-><init>()V

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/CY6;->a(J)LX/CY6;

    move-result-object v2

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/CY6;->a(Ljava/lang/String;)LX/CY6;

    move-result-object v2

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/CY6;->a(LX/0Px;)LX/CY6;

    move-result-object v2

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/CY6;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)LX/CY6;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, LX/CY6;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;)LX/CY6;

    move-result-object v12

    new-instance v2, LX/CYE;

    const-string v4, "PRIMARY_BUTTONS"

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v5

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9}, LX/H8X;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Z

    move-result v8

    invoke-direct/range {v2 .. v8}, LX/CYE;-><init>(ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;IZ)V

    invoke-virtual {v12, v2}, LX/CY6;->a(LX/CYE;)LX/CY6;

    move-result-object v3

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v3, v2}, LX/CY6;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)LX/CY6;

    move-result-object v2

    invoke-virtual {v2}, LX/CY6;->a()LX/CY7;

    move-result-object v2

    invoke-virtual {v11, v2}, LX/HAH;->a(LX/CY7;)V

    .line 2435071
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->o:Landroid/os/ParcelUuid;

    if-eqz v2, :cond_1

    .line 2435072
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->o:Landroid/os/ParcelUuid;

    invoke-virtual {v11, v2}, LX/HAH;->setLoggingUuid(Landroid/os/ParcelUuid;)V

    .line 2435073
    :cond_1
    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2435074
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->g:LX/CYM;

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->n()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/CYM;->a(Z)V

    .line 2435075
    :cond_2
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2435076
    :cond_3
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 2435077
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 2435078
    :cond_5
    new-instance v4, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;)V

    .line 2435079
    invoke-virtual {p0, v4}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->addView(Landroid/view/View;)V

    .line 2435080
    invoke-virtual {v4, v3}, Lcom/facebook/fig/button/FigButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2435081
    invoke-virtual {v4, v2}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2435082
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->n:LX/H9D;

    invoke-virtual {v2, v9}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v2

    check-cast v2, LX/H8Z;

    .line 2435083
    new-instance v5, LX/HAM;

    sget-object v3, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->l:LX/0Rf;

    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v3, LX/HA6;->LOCKED:LX/HA6;

    :goto_3
    invoke-direct {v5, p0, v3, v2}, LX/HAM;-><init>(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;LX/HA6;LX/H8Z;)V

    .line 2435084
    invoke-direct {p0, v4, v2, v5}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Lcom/facebook/fig/button/FigButton;LX/H8Z;LX/HAM;)V

    .line 2435085
    iget-object v3, v5, LX/HAM;->a:LX/HA6;

    iget-object v5, v5, LX/HAM;->b:LX/H8Z;

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Lcom/facebook/fig/button/FigButton;LX/HA6;LX/H8Z;)V

    .line 2435086
    if-eqz p5, :cond_7

    .line 2435087
    invoke-direct {p0, p1, p2, v9, v7}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(JLcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;I)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 2435088
    invoke-virtual {v4, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2435089
    :goto_4
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/facebook/fig/button/FigButton;->setClickable(Z)V

    goto :goto_2

    .line 2435090
    :cond_6
    move/from16 v0, p5

    invoke-static {v9, v0}, LX/H8X;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;Z)LX/HA6;

    move-result-object v3

    goto :goto_3

    .line 2435091
    :cond_7
    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v3, v5, :cond_8

    .line 2435092
    invoke-direct {p0, v4}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(Landroid/view/View;)V

    .line 2435093
    :cond_8
    new-instance v3, LX/HAI;

    invoke-direct {v3, p0, v2}, LX/HAI;-><init>(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;LX/H8Z;)V

    invoke-virtual {v4, v3}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    .line 2435094
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getChildCount()I

    move-result v2

    if-nez v2, :cond_a

    .line 2435095
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->setVisibility(I)V

    .line 2435096
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-class v3, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "No valid actions to render on CTA bar"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2435097
    :goto_5
    return-void

    .line 2435098
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 2435099
    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0060

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_5
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2435100
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->r:LX/0Px;

    if-eqz v0, :cond_1

    .line 2435101
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->r:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->r:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8E;

    .line 2435102
    iget-object v3, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->j:LX/HDT;

    invoke-virtual {v3, v0}, LX/0b4;->b(LX/0b2;)Z

    .line 2435103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2435104
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->r:LX/0Px;

    .line 2435105
    :cond_1
    return-void
.end method

.method public getSupportedActionTypes()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2435106
    sget-object v0, LX/H9D;->a:LX/0Px;

    move-object v0, v0

    .line 2435107
    return-object v0
.end method

.method public setLoggingUuid(Landroid/os/ParcelUuid;)V
    .locals 4

    .prologue
    .line 2435108
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->o:Landroid/os/ParcelUuid;

    .line 2435109
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->g:LX/CYM;

    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->o:Landroid/os/ParcelUuid;

    .line 2435110
    iput-object v1, v0, LX/CYM;->b:Landroid/os/ParcelUuid;

    .line 2435111
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HAH;

    .line 2435112
    iget-object v3, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->o:Landroid/os/ParcelUuid;

    invoke-virtual {v0, v3}, LX/HAH;->setLoggingUuid(Landroid/os/ParcelUuid;)V

    .line 2435113
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2435114
    :cond_0
    return-void
.end method
