.class public final Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2435267
    const-class v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2435266
    const-class v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2435264
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2435265
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435262
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;->e:Ljava/lang/String;

    .line 2435263
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2435256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2435257
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2435258
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2435259
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2435260
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2435261
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2435253
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2435254
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2435255
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2435252
    new-instance v0, LX/HAQ;

    invoke-direct {v0, p1}, LX/HAQ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435251
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2435243
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2435244
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2435250
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2435247
    new-instance v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel$UserModel;-><init>()V

    .line 2435248
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2435249
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2435246
    const v0, -0x46e62d8d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2435245
    const v0, 0x285feb

    return v0
.end method
