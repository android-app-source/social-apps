.class public Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/H8L;


# instance fields
.field public a:LX/HDT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/H9E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HAF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/fig/button/FigButton;

.field private g:Landroid/os/ParcelUuid;

.field private h:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/H8Z;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/H9D;

.field private j:LX/CXx;

.field private k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2435381
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2435382
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f()V

    .line 2435383
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2435378
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2435379
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f()V

    .line 2435380
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2435375
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2435376
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f()V

    .line 2435377
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;LX/HDT;LX/H9E;LX/CXj;LX/HAF;LX/03V;)V
    .locals 0

    .prologue
    .line 2435374
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a:LX/HDT;

    iput-object p2, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->b:LX/H9E;

    iput-object p3, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->c:LX/CXj;

    iput-object p4, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->d:LX/HAF;

    iput-object p5, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->e:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-static {v5}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v1

    check-cast v1, LX/HDT;

    const-class v2, LX/H9E;

    invoke-interface {v5, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/H9E;

    invoke-static {v5}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v3

    check-cast v3, LX/CXj;

    invoke-static {v5}, LX/HAF;->a(LX/0QB;)LX/HAF;

    move-result-object v4

    check-cast v4, LX/HAF;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {v0 .. v5}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;LX/HDT;LX/H9E;LX/CXj;LX/HAF;LX/03V;)V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2435328
    invoke-static {p0}, LX/HAF;->a(LX/H8L;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->k:LX/0Px;

    .line 2435329
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->k:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->k:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8E;

    .line 2435330
    iget-object v3, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a:LX/HDT;

    invoke-virtual {v3, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2435331
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2435332
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->getTabDataSubscriber()LX/CXx;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->j:LX/CXx;

    .line 2435333
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->c:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->j:LX/CXx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2435334
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2435384
    const-class v0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2435385
    const v0, 0x7f030eec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2435386
    const v0, 0x7f0d245d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    .line 2435387
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->h:Ljava/util/LinkedHashMap;

    .line 2435388
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->b:LX/H9E;

    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->i:LX/H9D;

    .line 2435389
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2435370
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8Z;

    .line 2435371
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->d:LX/HAF;

    invoke-virtual {v2, v0}, LX/HAF;->b(LX/H8Y;)V

    goto :goto_0

    .line 2435372
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 2435373
    return-void
.end method

.method public static setActionBarChannelItems(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2435362
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->g()V

    .line 2435363
    if-nez p1, :cond_1

    .line 2435364
    :cond_0
    return-void

    .line 2435365
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2435366
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->i:LX/H9D;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v2, v0}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v0

    check-cast v0, LX/H8Z;

    .line 2435367
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->h:Ljava/util/LinkedHashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2435368
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->d:LX/HAF;

    invoke-virtual {v2, v0}, LX/HAF;->a(LX/H8Y;)V

    .line 2435369
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2435344
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2435345
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->h:Ljava/util/LinkedHashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8Z;

    .line 2435346
    invoke-interface {v0}, LX/H8Z;->a()LX/HA7;

    move-result-object v1

    .line 2435347
    iget-object v2, v1, LX/HA7;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2435348
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2435349
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    .line 2435350
    iget-object v3, v1, LX/HA7;->c:Ljava/lang/String;

    move-object v1, v3

    .line 2435351
    invoke-virtual {v2, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2435352
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1, v4}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2435353
    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/HAV;

    invoke-direct {v2, p0, v0}, LX/HAV;-><init>(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;LX/H8Z;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2435354
    :goto_1
    return-void

    .line 2435355
    :cond_0
    iget v2, v1, LX/HA7;->b:I

    move v2, v2

    .line 2435356
    if-lez v2, :cond_1

    .line 2435357
    iget-object v2, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    .line 2435358
    iget v3, v1, LX/HA7;->b:I

    move v1, v3

    .line 2435359
    invoke-virtual {v2, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    goto :goto_0

    .line 2435360
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->e:LX/03V;

    const-class v2, LX/H8M;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "No valid text string or resource available for page action."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2435361
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2435341
    iput-object p1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->g:Landroid/os/ParcelUuid;

    .line 2435342
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->e()V

    .line 2435343
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2435338
    invoke-direct {p0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->g()V

    .line 2435339
    iget-object v0, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->f:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2435340
    return-void
.end method

.method public getSupportedActionTypes()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2435336
    sget-object v0, LX/H9D;->a:LX/0Px;

    move-object v0, v0

    .line 2435337
    return-object v0
.end method

.method public getTabDataSubscriber()LX/CXx;
    .locals 2

    .prologue
    .line 2435335
    new-instance v0, LX/HAU;

    iget-object v1, p0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->g:Landroid/os/ParcelUuid;

    invoke-direct {v0, p0, v1}, LX/HAU;-><init>(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;Landroid/os/ParcelUuid;)V

    return-object v0
.end method
