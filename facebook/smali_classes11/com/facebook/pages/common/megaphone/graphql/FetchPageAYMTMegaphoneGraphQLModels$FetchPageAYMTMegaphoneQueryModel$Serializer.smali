.class public final Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2442359
    const-class v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;

    new-instance v1, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2442360
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2442358
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2442361
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2442362
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2442363
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2442364
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2442365
    if-eqz v2, :cond_0

    .line 2442366
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2442367
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2442368
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2442369
    if-eqz v2, :cond_1

    .line 2442370
    const-string p0, "aymt_megaphone_channel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2442371
    invoke-static {v1, v2, p1, p2}, LX/HEe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2442372
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2442373
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2442357
    check-cast p1, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$Serializer;->a(Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
