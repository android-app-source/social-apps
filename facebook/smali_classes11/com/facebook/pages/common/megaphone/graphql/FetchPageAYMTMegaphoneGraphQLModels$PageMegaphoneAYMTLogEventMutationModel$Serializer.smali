.class public final Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2442475
    const-class v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel;

    new-instance v1, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2442476
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2442477
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2442478
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2442479
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2442480
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2442481
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2442482
    if-eqz v2, :cond_0

    .line 2442483
    const-string p0, "aymt_channel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2442484
    invoke-static {v1, v2, p1}, LX/HEf;->a(LX/15i;ILX/0nX;)V

    .line 2442485
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2442486
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2442487
    check-cast p1, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel$Serializer;->a(Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$PageMegaphoneAYMTLogEventMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
