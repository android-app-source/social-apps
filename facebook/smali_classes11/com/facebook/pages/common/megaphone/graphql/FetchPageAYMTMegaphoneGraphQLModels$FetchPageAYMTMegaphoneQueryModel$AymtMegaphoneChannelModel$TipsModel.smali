.class public final Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x671434b6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2442262
    const-class v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2442263
    const-class v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2442264
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2442265
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2442266
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2442267
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2442268
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2442269
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2442270
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2442271
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x7bac47cc

    invoke-static {v5, v4, v6}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2442272
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2442273
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2442274
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2442275
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2442276
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2442277
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2442278
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2442279
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2442280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2442281
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2442283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2442284
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2442285
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7bac47cc

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2442286
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2442287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    .line 2442288
    iput v3, v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->i:I

    .line 2442289
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2442290
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2442291
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2442292
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442282
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2442293
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2442294
    const/4 v0, 0x4

    const v1, 0x7bac47cc

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->i:I

    .line 2442295
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2442245
    new-instance v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;-><init>()V

    .line 2442246
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2442247
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2442261
    const v0, 0x25fd25a1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2442260
    const v0, 0xab8e43c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442258
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->e:Ljava/lang/String;

    .line 2442259
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442256
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->f:Ljava/lang/String;

    .line 2442257
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442254
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->g:Ljava/lang/String;

    .line 2442255
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442252
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->h:Ljava/lang/String;

    .line 2442253
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442250
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2442251
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442248
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->j:Ljava/lang/String;

    .line 2442249
    iget-object v0, p0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->j:Ljava/lang/String;

    return-object v0
.end method
