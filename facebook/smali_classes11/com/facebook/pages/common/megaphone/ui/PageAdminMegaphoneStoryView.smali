.class public Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;
.super Lcom/facebook/fbui/widget/megaphone/Megaphone;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2442653
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;-><init>(Landroid/content/Context;)V

    .line 2442654
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2442655
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2442656
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2442657
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2442658
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2442659
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2442660
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2442661
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2442662
    const v0, 0x7f02137e

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;->setBackgroundResource(I)V

    .line 2442663
    sget-object v0, LX/03r;->PageAdminMegaphoneStoryView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2442664
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 2442665
    if-lez v1, :cond_0

    .line 2442666
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitleMaxLines(I)V

    .line 2442667
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 2442668
    if-lez v1, :cond_1

    .line 2442669
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitleMaxLines(I)V

    .line 2442670
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2442671
    return-void
.end method
