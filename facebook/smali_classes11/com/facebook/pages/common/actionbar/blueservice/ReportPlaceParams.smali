.class public Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433121
    new-instance v0, LX/H8C;

    invoke-direct {v0}, LX/H8C;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2433115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->a:Ljava/lang/String;

    .line 2433117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->b:Ljava/lang/String;

    .line 2433118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->c:Ljava/lang/String;

    .line 2433119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->d:Ljava/lang/String;

    .line 2433120
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2433103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433104
    iput-object p1, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->a:Ljava/lang/String;

    .line 2433105
    iput-object p2, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->b:Ljava/lang/String;

    .line 2433106
    iput-object p3, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->c:Ljava/lang/String;

    .line 2433107
    iput-object p4, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->d:Ljava/lang/String;

    .line 2433108
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2433114
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2433109
    iget-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2433110
    iget-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2433111
    iget-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2433112
    iget-object v0, p0, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2433113
    return-void
.end method
