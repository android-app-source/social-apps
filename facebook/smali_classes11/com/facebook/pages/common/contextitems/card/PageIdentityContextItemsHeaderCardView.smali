.class public Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Bi8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dt5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field public k:Landroid/os/ParcelUuid;

.field public l:LX/HBY;

.field private m:LX/CXz;

.field private n:LX/CY1;

.field private o:LX/CXm;

.field private p:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

.field private q:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2437340
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->PLACE_CITY_QUESTION:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->PLACE_STREET_ADDRESS_QUESTION:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->PLACE_ZIP_CODE_QUESTION:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->h:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2437341
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2437342
    invoke-direct {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->c()V

    .line 2437343
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2437344
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2437345
    invoke-direct {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->c()V

    .line 2437346
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2437347
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2437348
    invoke-direct {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->c()V

    .line 2437349
    return-void
.end method

.method private a(LX/HBY;Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)LX/HBY;
    .locals 12
    .param p1    # LX/HBY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2437350
    if-eqz p1, :cond_0

    .line 2437351
    iget-object v0, p1, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437352
    if-nez v0, :cond_1

    :cond_0
    move-object v1, p1

    .line 2437353
    :goto_0
    return-object v1

    .line 2437354
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2437355
    iget-object v0, p1, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437356
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    .line 2437357
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v5

    .line 2437358
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v6

    if-ne p2, v6, :cond_2

    .line 2437359
    new-instance v6, LX/5Nm;

    invoke-direct {v6}, LX/5Nm;-><init>()V

    .line 2437360
    new-instance v6, LX/5Nm;

    invoke-direct {v6}, LX/5Nm;-><init>()V

    .line 2437361
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->a()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->a:Ljava/lang/String;

    .line 2437362
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->l()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->b:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;

    .line 2437363
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->c()LX/0Px;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->c:LX/0Px;

    .line 2437364
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->d:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    .line 2437365
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->e()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->e:Ljava/lang/String;

    .line 2437366
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->ak_()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->f:Ljava/lang/String;

    .line 2437367
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2437368
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2437369
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->k()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/5Nm;->i:Ljava/lang/String;

    .line 2437370
    move-object v5, v6

    .line 2437371
    invoke-direct {p0, p3}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a(Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    .line 2437372
    iput-object v6, v5, LX/5Nm;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2437373
    move-object v5, v5

    .line 2437374
    invoke-virtual {v5}, LX/5Nm;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v5

    .line 2437375
    new-instance v6, LX/5Np;

    invoke-direct {v6}, LX/5Np;-><init>()V

    .line 2437376
    new-instance v6, LX/5Np;

    invoke-direct {v6}, LX/5Np;-><init>()V

    .line 2437377
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v7

    iput-object v7, v6, LX/5Np;->a:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 2437378
    move-object v0, v6

    .line 2437379
    iput-object v5, v0, LX/5Np;->a:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 2437380
    move-object v0, v0

    .line 2437381
    invoke-virtual {v0}, LX/5Np;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    move-result-object v0

    .line 2437382
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2437383
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2437384
    :cond_2
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2437385
    :cond_3
    new-instance v0, LX/5Nq;

    invoke-direct {v0}, LX/5Nq;-><init>()V

    .line 2437386
    iget-object v0, p1, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437387
    invoke-static {v0}, LX/5Nq;->a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;)LX/5Nq;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2437388
    iput-object v1, v0, LX/5Nq;->a:LX/0Px;

    .line 2437389
    move-object v0, v0

    .line 2437390
    invoke-virtual {v0}, LX/5Nq;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v6

    .line 2437391
    new-instance v1, LX/HBY;

    .line 2437392
    iget-wide v10, p1, LX/HBY;->a:J

    move-wide v2, v10

    .line 2437393
    iget-object v0, p1, LX/HBY;->b:Ljava/lang/String;

    move-object v4, v0

    .line 2437394
    iget-object v0, p1, LX/HBY;->c:Ljava/lang/String;

    move-object v5, v0

    .line 2437395
    iget-object v0, p1, LX/HBY;->e:LX/0Px;

    move-object v7, v0

    .line 2437396
    iget-object v0, p1, LX/HBY;->f:Lcom/facebook/graphql/model/GraphQLRating;

    move-object v8, v0

    .line 2437397
    iget-object v0, p1, LX/HBY;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-object v9, v0

    .line 2437398
    invoke-direct/range {v1 .. v9}, LX/HBY;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;LX/0Px;Lcom/facebook/graphql/model/GraphQLRating;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;)V

    goto/16 :goto_0
.end method

.method public static synthetic a(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;LX/HBY;Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)LX/HBY;
    .locals 1

    .prologue
    .line 2437399
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a(LX/HBY;Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)LX/HBY;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3

    .prologue
    .line 2437400
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    if-ne p1, v0, :cond_0

    .line 2437401
    new-instance v0, LX/4ae;

    invoke-direct {v0}, LX/4ae;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081827

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2437402
    iput-object v1, v0, LX/4ae;->a:Ljava/lang/String;

    .line 2437403
    move-object v0, v0

    .line 2437404
    invoke-virtual {v0}, LX/4ae;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    .line 2437405
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4ae;

    invoke-direct {v0}, LX/4ae;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081827

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2437406
    iput-object v1, v0, LX/4ae;->a:Ljava/lang/String;

    .line 2437407
    move-object v0, v0

    .line 2437408
    invoke-virtual {v0}, LX/4ae;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryInterfaces$ContextItemsConnectionFragment$Edges;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2437409
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    .line 2437410
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2437411
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v0

    .line 2437412
    sget-object v3, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->h:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2437413
    invoke-direct {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->e()V

    .line 2437414
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2437415
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->PAGE_EVENTS_CALENDAR_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-ne v0, v3, :cond_0

    .line 2437416
    invoke-direct {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->f()V

    goto :goto_1

    .line 2437417
    :cond_2
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;LX/Bi8;LX/CXj;LX/0Ot;LX/0Ot;LX/0Ot;LX/0kL;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;",
            "LX/Bi8;",
            "LX/CXj;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dt5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;",
            "LX/0kL;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2437418
    iput-object p1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a:LX/Bi8;

    iput-object p2, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    iput-object p3, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->f:LX/0kL;

    iput-object p7, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->g:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-static {v7}, LX/Bi8;->b(LX/0QB;)LX/Bi8;

    move-result-object v1

    check-cast v1, LX/Bi8;

    invoke-static {v7}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v2

    check-cast v2, LX/CXj;

    const/16 v3, 0x259

    invoke-static {v7, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2c39

    invoke-static {v7, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf07

    invoke-static {v7, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v7}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    const/16 v8, 0x1430

    invoke-static {v7, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;LX/Bi8;LX/CXj;LX/0Ot;LX/0Ot;LX/0Ot;LX/0kL;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 2437419
    new-instance v1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;

    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    invoke-virtual {v0}, LX/HBY;->a()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437420
    iget-object v4, v0, LX/HBY;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2437421
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437422
    iget-object v5, v0, LX/HBY;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2437423
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437424
    iget-object v6, v0, LX/HBY;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-object v6, v6

    .line 2437425
    const/4 v7, 0x0

    move-object v8, p3

    move v10, v9

    invoke-direct/range {v1 .. v10}, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;-><init>(JLjava/lang/String;Ljava/lang/String;LX/1k1;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;ZZ)V

    .line 2437426
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->k:Landroid/os/ParcelUuid;

    .line 2437427
    iput-object v0, v1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->j:Landroid/os/ParcelUuid;

    .line 2437428
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dt5;

    invoke-interface {v0, p1, p2, v1}, LX/Dt5;->a(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V

    .line 2437429
    return-void
.end method

.method public static b(LX/HBY;Ljava/util/List;)LX/HBY;
    .locals 12
    .param p0    # LX/HBY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HBY;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;",
            ">;)",
            "LX/HBY;"
        }
    .end annotation

    .prologue
    .line 2437316
    if-eqz p0, :cond_0

    .line 2437317
    iget-object v0, p0, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437318
    if-nez v0, :cond_1

    :cond_0
    move-object v1, p0

    .line 2437319
    :goto_0
    return-object v1

    .line 2437320
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2437321
    iget-object v0, p0, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437322
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    .line 2437323
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2437324
    :cond_2
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2437325
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2437326
    :cond_4
    new-instance v0, LX/5Nq;

    invoke-direct {v0}, LX/5Nq;-><init>()V

    .line 2437327
    iget-object v0, p0, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437328
    invoke-static {v0}, LX/5Nq;->a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;)LX/5Nq;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2437329
    iput-object v1, v0, LX/5Nq;->a:LX/0Px;

    .line 2437330
    move-object v0, v0

    .line 2437331
    invoke-virtual {v0}, LX/5Nq;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v6

    .line 2437332
    new-instance v1, LX/HBY;

    .line 2437333
    iget-wide v10, p0, LX/HBY;->a:J

    move-wide v2, v10

    .line 2437334
    iget-object v0, p0, LX/HBY;->b:Ljava/lang/String;

    move-object v4, v0

    .line 2437335
    iget-object v0, p0, LX/HBY;->c:Ljava/lang/String;

    move-object v5, v0

    .line 2437336
    iget-object v0, p0, LX/HBY;->e:LX/0Px;

    move-object v7, v0

    .line 2437337
    iget-object v0, p0, LX/HBY;->f:Lcom/facebook/graphql/model/GraphQLRating;

    move-object v8, v0

    .line 2437338
    iget-object v0, p0, LX/HBY;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-object v9, v0

    .line 2437339
    invoke-direct/range {v1 .. v9}, LX/HBY;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;LX/0Px;Lcom/facebook/graphql/model/GraphQLRating;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;)V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2437306
    const-class v0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2437307
    const v0, 0x7f030e5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2437308
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->setOrientation(I)V

    .line 2437309
    const v0, 0x7f0d2330

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->p:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    .line 2437310
    const v0, 0x7f0d2331

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->q:Landroid/view/View;

    .line 2437311
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->p:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a:LX/Bi8;

    invoke-virtual {v0, v1}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->setAdapter(LX/Bi8;)V

    .line 2437312
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->p:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    const-string v1, "newsfeed"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2437313
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->p:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    new-instance v1, LX/HBU;

    invoke-direct {v1, p0}, LX/HBU;-><init>(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;)V

    .line 2437314
    iput-object v1, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->g:LX/Bi2;

    .line 2437315
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    .line 2437300
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->m:LX/CXz;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2437301
    new-instance v0, LX/HBV;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437302
    iget-wide v4, v1, LX/HBY;->a:J

    move-wide v2, v4

    .line 2437303
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/HBV;-><init>(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;Ljava/lang/Long;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->m:LX/CXz;

    .line 2437304
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->m:LX/CXz;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2437305
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 2437254
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->n:LX/CY1;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2437255
    new-instance v0, LX/HBW;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437256
    iget-wide v4, v1, LX/HBY;->a:J

    move-wide v2, v4

    .line 2437257
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/HBW;-><init>(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;Ljava/lang/Long;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->n:LX/CY1;

    .line 2437258
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->n:LX/CY1;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2437259
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 2437294
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->o:LX/CXm;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2437295
    new-instance v0, LX/HBX;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437296
    iget-wide v4, v1, LX/HBY;->a:J

    move-wide v2, v4

    .line 2437297
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/HBX;-><init>(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;Ljava/lang/Long;)V

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->o:LX/CXm;

    .line 2437298
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->o:LX/CXm;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2437299
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2437292
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->p:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a()V

    .line 2437293
    return-void
.end method

.method public final a(LX/HBY;)V
    .locals 6

    .prologue
    .line 2437274
    iput-object p1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437275
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->i:Z

    .line 2437276
    new-instance v0, LX/Bi6;

    .line 2437277
    iget-wide v4, p1, LX/HBY;->a:J

    move-wide v2, v4

    .line 2437278
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437279
    iget-object v3, v2, LX/HBY;->f:Lcom/facebook/graphql/model/GraphQLRating;

    move-object v2, v3

    .line 2437280
    invoke-direct {v0, v1, v2}, LX/Bi6;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLRating;)V

    .line 2437281
    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a:LX/Bi8;

    .line 2437282
    iget-object v2, p1, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v2, v2

    .line 2437283
    sget-object v3, LX/Bi3;->PAGE_HEADER:LX/Bi3;

    invoke-virtual {v1, v2, v3, v0}, LX/Bi8;->a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;LX/Bi3;LX/Bi6;)V

    .line 2437284
    invoke-direct {p0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->d()V

    .line 2437285
    iget-object v0, p1, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437286
    if-eqz v0, :cond_0

    .line 2437287
    iget-object v0, p1, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437288
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2437289
    iget-object v0, p1, LX/HBY;->d:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-object v0, v0

    .line 2437290
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a(LX/0Px;)V

    .line 2437291
    :cond_0
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 2437267
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2437268
    iget-boolean v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->j:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->i:Z

    if-eqz v0, :cond_1

    .line 2437269
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->k:Landroid/os/ParcelUuid;

    if-nez v0, :cond_0

    .line 2437270
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UUID in context heaeder view is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2437271
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->b:LX/CXj;

    new-instance v1, LX/CXq;

    iget-object v2, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->k:Landroid/os/ParcelUuid;

    sget-object v3, LX/CXp;->CONTEXT_ITEMS_DISPATCH_DRAW:LX/CXp;

    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/CXq;-><init>(Landroid/os/ParcelUuid;LX/CXp;LX/0am;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2437272
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->j:Z

    .line 2437273
    :cond_1
    return-void
.end method

.method public setContainerBorderVisibility(Z)V
    .locals 2

    .prologue
    .line 2437264
    iget-object v1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->q:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2437265
    return-void

    .line 2437266
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setPageFragmentUuid(Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2437262
    iput-object p1, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->k:Landroid/os/ParcelUuid;

    .line 2437263
    return-void
.end method

.method public setParentFragment(Lcom/facebook/base/fragment/FbFragment;)V
    .locals 1

    .prologue
    .line 2437260
    check-cast p1, LX/HOo;

    invoke-interface {p1}, LX/HOo;->k()Landroid/os/ParcelUuid;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->k:Landroid/os/ParcelUuid;

    .line 2437261
    return-void
.end method
