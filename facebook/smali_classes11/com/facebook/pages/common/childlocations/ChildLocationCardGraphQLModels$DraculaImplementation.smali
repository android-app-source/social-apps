.class public final Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2435722
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2435723
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2435724
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2435725
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2435726
    if-nez p1, :cond_0

    .line 2435727
    :goto_0
    return v0

    .line 2435728
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2435729
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2435730
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2435731
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2435732
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2435733
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2435734
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2435735
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2435736
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2435737
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x147097ca
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2435705
    new-instance v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2435739
    packed-switch p0, :pswitch_data_0

    .line 2435740
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2435741
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x147097ca
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2435738
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2435742
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->b(I)V

    .line 2435743
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2435716
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2435717
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2435718
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2435719
    iput p2, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->b:I

    .line 2435720
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2435721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2435715
    new-instance v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2435712
    iget v0, p0, LX/1vt;->c:I

    .line 2435713
    move v0, v0

    .line 2435714
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2435709
    iget v0, p0, LX/1vt;->c:I

    .line 2435710
    move v0, v0

    .line 2435711
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2435706
    iget v0, p0, LX/1vt;->b:I

    .line 2435707
    move v0, v0

    .line 2435708
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435702
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2435703
    move-object v0, v0

    .line 2435704
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2435693
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2435694
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2435695
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2435696
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2435697
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2435698
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2435699
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2435700
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2435701
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2435690
    iget v0, p0, LX/1vt;->c:I

    .line 2435691
    move v0, v0

    .line 2435692
    return v0
.end method
