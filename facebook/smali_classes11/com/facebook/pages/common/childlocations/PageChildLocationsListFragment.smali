.class public Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field public static final y:[I


# instance fields
.field public A:I

.field public a:LX/HAk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0y2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field public k:Landroid/location/Location;

.field public l:I

.field public m:I

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public o:Landroid/widget/ListView;

.field public p:LX/HAj;

.field private q:LX/E8s;

.field private r:LX/E8t;

.field public s:Landroid/view/View;

.field public t:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private u:Z

.field public v:I

.field private w:I

.field private x:I

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2436061
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->y:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2436055
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2436056
    iput-boolean v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->u:Z

    .line 2436057
    iput v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->x:I

    .line 2436058
    iput-boolean v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->z:Z

    .line 2436059
    iput v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->A:I

    .line 2436060
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Landroid/location/Location;JLjava/lang/String;Z)Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;",
            ">;",
            "Landroid/location/Location;",
            "J",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;"
        }
    .end annotation

    .prologue
    .line 2436046
    new-instance v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;-><init>()V

    .line 2436047
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2436048
    const-string v2, "extra_child_locations"

    invoke-static {v1, v2, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2436049
    const-string v2, "extra_page_user_location"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2436050
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2436051
    const-string v2, "profile_name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436052
    const-string v2, "extra_is_inside_page_surface_tab"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2436053
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2436054
    return-object v0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 2436040
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->n:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->n:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2436041
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    invoke-virtual {v0}, LX/HAj;->clear()V

    .line 2436042
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->n:LX/0Px;

    invoke-virtual {v0, v1}, LX/HAj;->addAll(Ljava/util/Collection;)V

    .line 2436043
    :goto_0
    return-void

    .line 2436044
    :cond_0
    new-instance v0, LX/HAm;

    invoke-direct {v0, p0}, LX/HAm;-><init>(Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;)V

    .line 2436045
    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->c:LX/1Ck;

    sget-object v2, LX/HAo;->FETCH_CHILD_LOCATIONS:LX/HAo;

    new-instance v3, LX/HAn;

    invoke-direct {v3, p0}, LX/HAn;-><init>(Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2436027
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    if-nez v0, :cond_2

    .line 2436028
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->r:LX/E8t;

    if-nez v0, :cond_1

    .line 2436029
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    .line 2436030
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    const/4 v1, 0x1

    .line 2436031
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2436032
    :cond_0
    :goto_1
    return-void

    .line 2436033
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->r:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    .line 2436034
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->r:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2436035
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->r:LX/E8t;

    if-eqz v0, :cond_0

    .line 2436036
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->r:LX/E8t;

    .line 2436037
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2436038
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->r:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2436039
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2436022
    iget v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->x:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->x:I

    if-le p1, v0, :cond_1

    .line 2436023
    :cond_0
    :goto_0
    return-void

    .line 2436024
    :cond_1
    iput p1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->x:I

    .line 2436025
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2436026
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->s:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->x:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2436021
    const-string v0, "page_child_locations_list_activity"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2435963
    iput p1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->w:I

    .line 2435964
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    iget v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->w:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2435965
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2436018
    iput-object p1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->r:LX/E8t;

    .line 2436019
    invoke-direct {p0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->e()V

    .line 2436020
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2436008
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2436009
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    const-class v3, LX/HAk;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HAk;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v4

    check-cast v4, LX/0zG;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v8

    check-cast v8, LX/0rq;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object p1

    check-cast p1, LX/0y2;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    iput-object v3, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->a:LX/HAk;

    iput-object v4, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->b:LX/0zG;

    iput-object v5, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->c:LX/1Ck;

    iput-object v6, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->d:LX/03V;

    iput-object v7, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->e:LX/0tX;

    iput-object v8, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->f:LX/0rq;

    iput-object p1, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->g:LX/0y2;

    iput-object v0, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->h:LX/0hB;

    .line 2436010
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->l:I

    .line 2436011
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->m:I

    .line 2436012
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2436013
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->i:Ljava/lang/String;

    .line 2436014
    const-string v0, "profile_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->j:Ljava/lang/String;

    .line 2436015
    const-string v0, "extra_page_user_location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->k:Landroid/location/Location;

    .line 2436016
    const-string v0, "extra_is_inside_page_surface_tab"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->u:Z

    .line 2436017
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2436006
    iput-object p1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->t:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2436007
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2436004
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->a(Z)V

    .line 2436005
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x4234366e

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2435997
    const v0, 0x7f030038

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2435998
    const v0, 0x7f0d03b2

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2435999
    const v1, 0x7f0d03b3

    invoke-static {v3, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    .line 2436000
    iget-boolean v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->u:Z

    if-eqz v1, :cond_0

    .line 2436001
    invoke-virtual {v3, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2436002
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0062

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v5, v1, v5, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2436003
    :cond_0
    const/16 v0, 0x2b

    const v1, 0x17378e64

    invoke-static {v6, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x2fc1c5f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2435988
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2435989
    iget-boolean v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2435990
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2435991
    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->b:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2435992
    if-eqz v0, :cond_1

    .line 2435993
    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2435994
    :cond_0
    :goto_0
    const v0, 0x6a874256

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    .line 2435995
    :cond_1
    if-eqz v1, :cond_0

    .line 2435996
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->j:Ljava/lang/String;

    invoke-interface {v1, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2435966
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2435967
    iget-boolean v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->u:Z

    if-eqz v0, :cond_0

    .line 2435968
    invoke-direct {p0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->e()V

    .line 2435969
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 2435970
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->q:LX/E8s;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 2435971
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->s:Landroid/view/View;

    .line 2435972
    iget v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->x:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->E_(I)V

    .line 2435973
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 2435974
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    new-instance v1, LX/HAl;

    invoke-direct {v1, p0}, LX/HAl;-><init>(Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2435975
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2435976
    const-string v1, "extra_child_locations"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 2435977
    iget-object v2, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->a:LX/HAk;

    if-eqz v1, :cond_3

    move-object v0, v1

    .line 2435978
    :goto_0
    new-instance p2, LX/HAj;

    const-class v3, Landroid/content/Context;

    invoke-interface {v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v2}, LX/HSx;->b(LX/0QB;)LX/HSx;

    move-result-object v4

    check-cast v4, LX/CSL;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p2, v3, v0, v4, p1}, LX/HAj;-><init>(Landroid/content/Context;Ljava/util/List;LX/CSL;Lcom/facebook/content/SecureContextHelper;)V

    .line 2435979
    move-object v0, p2

    .line 2435980
    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    .line 2435981
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2435982
    iget-boolean v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->u:Z

    if-eqz v0, :cond_1

    .line 2435983
    iget v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->w:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->a(I)V

    .line 2435984
    :cond_1
    if-nez v1, :cond_2

    .line 2435985
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->a(Z)V

    .line 2435986
    :cond_2
    return-void

    .line 2435987
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method
