.class public Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2436069
    const-class v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2436070
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2436071
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2436062
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2436063
    const v0, 0x7f030e6a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2436064
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->setOrientation(I)V

    .line 2436065
    const v0, 0x7f0d0abc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2436066
    const v0, 0x7f0d0abd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->c:Landroid/widget/TextView;

    .line 2436067
    const v0, 0x7f0d2349

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->d:Landroid/widget/TextView;

    .line 2436068
    return-void
.end method
