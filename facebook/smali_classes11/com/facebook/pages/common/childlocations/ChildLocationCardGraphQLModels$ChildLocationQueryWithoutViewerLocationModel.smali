.class public final Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x245d6edb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2435689
    const-class v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2435688
    const-class v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2435686
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2435687
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2435680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2435681
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2435682
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2435683
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2435684
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2435685
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2435672
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2435673
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2435674
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    .line 2435675
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2435676
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;

    .line 2435677
    iput-object v0, v1, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->e:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    .line 2435678
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2435679
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2435671
    new-instance v0, LX/HAd;

    invoke-direct {v0, p1}, LX/HAd;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getChildLocations"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435669
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->e:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->e:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    .line 2435670
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->e:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2435667
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2435668
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2435666
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2435661
    new-instance v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;-><init>()V

    .line 2435662
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2435663
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2435665
    const v0, -0x66841616

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2435664
    const v0, 0x25d6af

    return v0
.end method
