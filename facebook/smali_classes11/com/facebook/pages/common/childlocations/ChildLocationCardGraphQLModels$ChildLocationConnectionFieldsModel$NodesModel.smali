.class public final Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x729ab4d8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2435514
    const-class v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2435513
    const-class v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2435511
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2435512
    return-void
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435509
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2435510
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2435495
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2435496
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x147097ca

    invoke-static {v1, v0, v2}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2435497
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2435498
    invoke-direct {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2435499
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2435500
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2435501
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2435502
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2435503
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2435504
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2435505
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2435506
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2435507
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2435508
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2435475
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2435476
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2435477
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x147097ca

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2435478
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2435479
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    .line 2435480
    iput v3, v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->e:I

    move-object v1, v0

    .line 2435481
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2435482
    invoke-direct {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2435483
    invoke-direct {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2435484
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    .line 2435485
    iput-object v0, v1, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2435486
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2435487
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2435488
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2435489
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    .line 2435490
    iput-object v0, v1, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2435491
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2435492
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2435493
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2435494
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2435474
    new-instance v0, LX/HAb;

    invoke-direct {v0, p1}, LX/HAb;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435473
    invoke-virtual {p0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2435515
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2435516
    const/4 v0, 0x0

    const v1, 0x147097ca

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->e:I

    .line 2435517
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2435457
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2435458
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2435460
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2435461
    new-instance v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;-><init>()V

    .line 2435462
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2435463
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2435464
    const v0, 0x703f8a69

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2435459
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2435465
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2435466
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435467
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->f:Ljava/lang/String;

    .line 2435468
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435469
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->h:Ljava/lang/String;

    .line 2435470
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2435471
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2435472
    iget-object v0, p0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
