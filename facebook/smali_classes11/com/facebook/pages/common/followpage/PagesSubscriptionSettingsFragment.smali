.class public Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:LX/HDw;

.field public d:LX/HE1;

.field public e:J

.field public f:LX/HE3;

.field private g:LX/HE3;

.field private h:LX/HE3;

.field private i:LX/HE3;

.field public j:Lcom/facebook/resources/ui/FbTextView;

.field public k:Lcom/facebook/widget/SwitchCompat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2441477
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2441478
    new-instance v0, LX/HDw;

    invoke-direct {v0, p0}, LX/HDw;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;)V

    move-object v0, v0

    .line 2441479
    iput-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->c:LX/HDw;

    .line 2441480
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    const/16 v2, 0x12b1

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0xafd

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->b:LX/0Ot;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 1

    .prologue
    .line 2441472
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iput-boolean p1, v0, LX/HE1;->a:Z

    .line 2441473
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iput-object p2, v0, LX/HE1;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2441474
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iput-object p3, v0, LX/HE1;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2441475
    invoke-direct {p0}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->k()V

    .line 2441476
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 2441427
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->k:Lcom/facebook/widget/SwitchCompat;

    iget-object v1, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-boolean v1, v1, LX/HE1;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2441428
    sget-object v0, LX/HE0;->a:[I

    iget-object v1, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v1, v1, LX/HE1;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2441429
    :goto_0
    return-void

    .line 2441430
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->i:LX/HE3;

    invoke-virtual {v0}, LX/HE3;->a()V

    goto :goto_0

    .line 2441431
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->h:LX/HE3;

    invoke-virtual {v0}, LX/HE3;->a()V

    goto :goto_0

    .line 2441432
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->g:LX/HE3;

    invoke-virtual {v0}, LX/HE3;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    .line 2441464
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2441465
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2441466
    const-string v2, "subscribe_status"

    iget-object v3, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v3, v3, LX/HE1;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2441467
    const-string v2, "secondary_subscribe_status"

    iget-object v3, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v3, v3, LX/HE1;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2441468
    const-string v2, "notification_status"

    iget-object v3, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-boolean v3, v3, LX/HE1;->a:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2441469
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2441470
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2441471
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2441452
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2441453
    const-class v0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2441454
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2441455
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->e:J

    .line 2441456
    new-instance v2, LX/HE1;

    .line 2441457
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2441458
    const-string v1, "notification_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 2441459
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2441460
    const-string v1, "secondary_subscribe_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2441461
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2441462
    const-string v4, "subscribe_status"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {v2, v3, v0, v1}, LX/HE1;-><init>(ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    iput-object v2, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    .line 2441463
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5528270b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2441451
    const v1, 0x7f030eeb

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0xf49894f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x42e6924c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2441448
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2441449
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2441450
    const/16 v0, 0x2b

    const v2, -0x6a4a9d86

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6cca736d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2441442
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2441443
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2441444
    if-eqz v1, :cond_0

    .line 2441445
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2441446
    const v2, 0x7f0836e5

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2441447
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x614d5c25

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v11, 0x1

    .line 2441433
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2441434
    const v0, 0x7f0d245a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->k:Lcom/facebook/widget/SwitchCompat;

    .line 2441435
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->k:Lcom/facebook/widget/SwitchCompat;

    new-instance v1, LX/HDv;

    invoke-direct {v1, p0}, LX/HDv;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441436
    new-instance v0, LX/HE3;

    const v1, 0x7f0d2c01

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f0d2c02

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    const v1, 0x7f0d2c03

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    const/4 v5, 0x0

    const v6, 0x7f08157c

    const v7, 0x7f081584

    const v8, 0x7f020099

    const v9, 0x7f020098

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNFOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, LX/HE3;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIIILcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->i:LX/HE3;

    .line 2441437
    new-instance v0, LX/HE3;

    const v1, 0x7f0d2bfe

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f0d2bff

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    const v1, 0x7f0d2c00

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    const v6, 0x7f081580

    const v7, 0x7f081585

    const v8, 0x7f020095

    const v9, 0x7f020094

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-object v1, p0

    move v5, v11

    invoke-direct/range {v0 .. v10}, LX/HE3;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIIILcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->h:LX/HE3;

    .line 2441438
    new-instance v0, LX/HE3;

    const v1, 0x7f0d2bfb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f0d2bfc

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    const v1, 0x7f0d2bfd

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    const v6, 0x7f08157f

    const v7, 0x7f081587

    const v8, 0x7f020097

    const v9, 0x7f020096

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-object v1, p0

    move v5, v11

    invoke-direct/range {v0 .. v10}, LX/HE3;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIIILcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->g:LX/HE3;

    .line 2441439
    const v0, 0x7f0d245c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2441440
    invoke-direct {p0}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->k()V

    .line 2441441
    return-void
.end method
