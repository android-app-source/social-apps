.class public final Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x9de4879
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2441739
    const-class v0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2441740
    const-class v0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2441751
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2441752
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2441741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2441742
    invoke-virtual {p0}, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;->j()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2441743
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2441744
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 2441745
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2441746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2441747
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2441748
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2441749
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2441750
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2441735
    new-instance v0, LX/HED;

    invoke-direct {v0, p1}, LX/HED;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2441736
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2441737
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;->e:Z

    .line 2441738
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2441726
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2441727
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2441723
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2441724
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2441725
    iget-boolean v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2441732
    new-instance v0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;-><init>()V

    .line 2441733
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2441734
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2441728
    const v0, 0x3be47a1b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2441729
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2441730
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2441731
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageSubscribeNotificationMutationModels$PageSubscribeNotificationMutationModel$PageModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method
