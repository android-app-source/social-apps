.class public final Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2289c30c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2441574
    const-class v0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2441575
    const-class v0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2441576
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2441577
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 2441586
    iput-object p1, p0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->e:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2441587
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2441588
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2441589
    if-eqz v0, :cond_0

    .line 2441590
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2441591
    :cond_0
    return-void

    .line 2441592
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2441578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2441579
    invoke-virtual {p0}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->a()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2441580
    invoke-virtual {p0}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->j()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2441581
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2441582
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2441583
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2441584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2441585
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2441593
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2441594
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2441595
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2441573
    new-instance v0, LX/HE7;

    invoke-direct {v0, p1}, LX/HE7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2441571
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->e:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->e:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2441572
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->e:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2441555
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2441556
    invoke-virtual {p0}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->a()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2441557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2441558
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2441559
    :goto_0
    return-void

    .line 2441560
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2441568
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2441569
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2441570
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2441565
    new-instance v0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;-><init>()V

    .line 2441566
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2441567
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2441564
    const v0, 0x54112a78

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2441563
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2441561
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2441562
    iget-object v0, p0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method
