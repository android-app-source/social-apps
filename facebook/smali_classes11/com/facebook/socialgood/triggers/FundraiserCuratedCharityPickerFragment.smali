.class public Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/BOa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Fob;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/17Y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/Long;

.field private l:Landroid/os/ResultReceiver;

.field public m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2289700
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-static {p0}, LX/BOa;->a(LX/0QB;)LX/BOa;

    move-result-object v2

    check-cast v2, LX/BOa;

    new-instance v5, LX/Fob;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v5, v3}, LX/Fob;-><init>(Landroid/content/Context;)V

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    iput-object v3, v5, LX/Fob;->a:Landroid/content/Context;

    iput-object v4, v5, LX/Fob;->b:Landroid/view/LayoutInflater;

    move-object v3, v5

    check-cast v3, LX/Fob;

    new-instance v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    invoke-direct {v0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;-><init>()V

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, LX/0Tf;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, LX/0Tf;

    iput-object v4, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->b:LX/0tX;

    iput-object v5, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->c:LX/0Tf;

    iput-object p1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->d:LX/0Tf;

    move-object v4, v0

    check-cast v4, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p0

    check-cast p0, LX/17Y;

    iput-object v2, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    iput-object v3, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    iput-object v4, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iput-object v5, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->e:LX/17Y;

    return-void
.end method

.method public static a(J)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2289696
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2289697
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2289698
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p0

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2289699
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;Ljava/lang/String;Ljava/lang/String;ZLX/BOS;)V
    .locals 5

    .prologue
    .line 2289664
    const-string v0, "fundraiser_for_story_upsell"

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2289665
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2289666
    const-string v1, "fundraiser_charity_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289667
    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->l:Landroid/os/ResultReceiver;

    if-eqz v1, :cond_0

    .line 2289668
    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->l:Landroid/os/ResultReceiver;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 2289669
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2289670
    :cond_1
    :goto_0
    return-void

    .line 2289671
    :cond_2
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->e:LX/17Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->gX:Ljava/lang/String;

    const-string v3, "fundraiser_create_promo"

    invoke-static {v2, p1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2289672
    if-eqz v0, :cond_1

    .line 2289673
    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    .line 2289674
    invoke-static {v1, p1, p2, p4, p3}, LX/BOa;->c(LX/BOa;Ljava/lang/String;Ljava/lang/String;LX/BOS;Z)Ljava/util/Map;

    move-result-object v2

    .line 2289675
    iget-object v3, v1, LX/BOa;->a:LX/0Zb;

    const-string v4, "fundraiser_create_promo_select_charity"

    invoke-static {v1, v4, v2}, LX/BOa;->b(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289676
    new-instance v1, LX/Fm9;

    invoke-direct {v1}, LX/Fm9;-><init>()V

    .line 2289677
    iput-object p1, v1, LX/Fm9;->b:Ljava/lang/String;

    .line 2289678
    move-object v1, v1

    .line 2289679
    iput-object p2, v1, LX/Fm9;->d:Ljava/lang/String;

    .line 2289680
    move-object v1, v1

    .line 2289681
    iput-boolean p3, v1, LX/Fm9;->c:Z

    .line 2289682
    move-object v1, v1

    .line 2289683
    iget-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->i:Ljava/lang/String;

    .line 2289684
    iput-object v2, v1, LX/Fm9;->f:Ljava/lang/String;

    .line 2289685
    move-object v1, v1

    .line 2289686
    iget-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->j:Ljava/lang/String;

    .line 2289687
    iput-object v2, v1, LX/Fm9;->g:Ljava/lang/String;

    .line 2289688
    move-object v1, v1

    .line 2289689
    iget-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->k:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 2289690
    iget-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->k:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2289691
    iput-wide v2, v1, LX/Fm9;->h:J

    .line 2289692
    :cond_3
    const-string v2, "fundraiser_model"

    invoke-virtual {v1}, LX/Fm9;->a()Lcom/facebook/socialgood/model/Fundraiser;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2289693
    const-string v1, "promo_id"

    iget-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2289694
    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2289695
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2289554
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2289555
    const-class v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2289556
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2289557
    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->g:Ljava/lang/String;

    .line 2289558
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2289559
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->h:Ljava/lang/String;

    .line 2289560
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2289561
    const-string v1, "result_receiver"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ResultReceiver;

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->l:Landroid/os/ResultReceiver;

    .line 2289562
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->g:Ljava/lang/String;

    .line 2289563
    iput-object v1, v0, LX/BOa;->d:Ljava/lang/String;

    .line 2289564
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->h:Ljava/lang/String;

    .line 2289565
    iput-object v1, v0, LX/BOa;->b:Ljava/lang/String;

    .line 2289566
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    .line 2289567
    iget-object v1, v0, LX/BOa;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2289568
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2289569
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2289570
    iput-object v1, v0, LX/BOa;->c:Ljava/lang/String;

    .line 2289571
    :cond_0
    if-eqz p1, :cond_2

    .line 2289572
    const-string v2, "extra_prefill_title"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->i:Ljava/lang/String;

    .line 2289573
    const-string v2, "extra_prefill_description"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->j:Ljava/lang/String;

    .line 2289574
    const-string v2, "extra_prefill_end_time"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->k:Ljava/lang/Long;

    .line 2289575
    const-string v2, "extra_search_category"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2289576
    iget-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-ne v2, v3, :cond_1

    .line 2289577
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2289578
    :cond_1
    const-string v2, "extra_search_category_translated_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->n:Ljava/lang/String;

    .line 2289579
    :cond_2
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->g:Ljava/lang/String;

    .line 2289580
    iput-object v1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->h:Ljava/lang/String;

    .line 2289581
    iput-object p0, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->i:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    .line 2289582
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    .line 2289583
    new-instance v1, LX/Fmq;

    invoke-direct {v1}, LX/Fmq;-><init>()V

    move-object v1, v1

    .line 2289584
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2289585
    new-instance v2, LX/Fmq;

    invoke-direct {v2}, LX/Fmq;-><init>()V

    const-string v3, "id"

    iget-object p1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    .line 2289586
    iget-object v3, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v3

    .line 2289587
    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2289588
    sget-object v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289589
    iput-object v2, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289590
    iget-object v2, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/Foe;

    sget-object v3, LX/Fod;->HIGHLIGHTED_CHARITY:LX/Fod;

    invoke-direct {v2, v0, v3}, LX/Foe;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;LX/Fod;)V

    iget-object v3, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->d:LX/0Tf;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2289591
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    invoke-virtual {v0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a()V

    .line 2289592
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    .line 2289593
    new-instance v1, LX/Fms;

    invoke-direct {v1}, LX/Fms;-><init>()V

    move-object v1, v1

    .line 2289594
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2289595
    new-instance v2, LX/Fms;

    invoke-direct {v2}, LX/Fms;-><init>()V

    const-string v3, "id"

    iget-object p1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    .line 2289596
    iget-object v3, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v3

    .line 2289597
    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2289598
    sget-object v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289599
    iput-object v2, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289600
    iget-object v2, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/Foe;

    sget-object v3, LX/Fod;->TITLES:LX/Fod;

    invoke-direct {v2, v0, v3}, LX/Foe;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;LX/Fod;)V

    iget-object v3, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->d:LX/0Tf;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2289601
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    .line 2289602
    new-instance v1, LX/Fmr;

    invoke-direct {v1}, LX/Fmr;-><init>()V

    move-object v1, v1

    .line 2289603
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2289604
    new-instance v2, LX/Fmr;

    invoke-direct {v2}, LX/Fmr;-><init>()V

    const-string v3, "id"

    iget-object p1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    .line 2289605
    iget-object v3, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v3

    .line 2289606
    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2289607
    sget-object v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289608
    iput-object v2, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289609
    iget-object v2, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/Foe;

    sget-object v3, LX/Fod;->PREFILL_FIELDS:LX/Fod;

    invoke-direct {v2, v0, v3}, LX/Foe;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;LX/Fod;)V

    iget-object v3, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->c:LX/0Tf;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2289610
    const-string v0, "fundraiser_for_story_upsell"

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->o:Z

    .line 2289611
    iget-boolean v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->o:Z

    if-eqz v0, :cond_3

    .line 2289612
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    .line 2289613
    invoke-static {}, LX/FnA;->a()LX/Fn9;

    move-result-object v1

    .line 2289614
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2289615
    sget-object v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289616
    iput-object v2, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2289617
    iget-object v2, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/Foe;

    sget-object v3, LX/Fod;->DAF_DISCLOSURE:LX/Fod;

    invoke-direct {v2, v0, v3}, LX/Foe;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;LX/Fod;)V

    iget-object v3, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->d:LX/0Tf;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2289618
    :cond_3
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    .line 2289619
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2289620
    return-void
.end method

.method public final a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2289647
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;

    .line 2289648
    if-eqz v0, :cond_0

    .line 2289649
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2289650
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->b(Ljava/lang/String;)V

    .line 2289651
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    if-eqz v0, :cond_1

    .line 2289652
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    new-instance v1, LX/Foa;

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Foa;-><init>(Ljava/lang/String;)V

    .line 2289653
    iget-object v2, v1, LX/Foa;->a:LX/FoZ;

    move-object v2, v2

    .line 2289654
    sget-object v3, LX/FoZ;->SUBTITLE:LX/FoZ;

    if-ne v2, v3, :cond_1

    .line 2289655
    iget-object v2, v0, LX/Fob;->d:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2289656
    const v2, -0x55e48649

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2289657
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2289658
    if-eqz v2, :cond_2

    const-class v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-virtual {v1, v2, v5, v0, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2289659
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-virtual {v1, v2, v5, v0, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2289660
    invoke-virtual {v1, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->n:Ljava/lang/String;

    .line 2289661
    :cond_2
    return-void

    .line 2289662
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083331

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2289663
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2289642
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2289643
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x82

    if-ne p1, v0, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2289644
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2289645
    const-string v1, "charity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "charity_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "is_daf_charity"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sget-object v3, LX/BOS;->CHARITY_FROM_SEARCH:LX/BOS;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a$redex0(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;Ljava/lang/String;Ljava/lang/String;ZLX/BOS;)V

    .line 2289646
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x25b0649f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2289641
    const v1, 0x7f030750

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x53c91b16

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2289632
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2289633
    const-string v0, "extra_prefill_title"

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289634
    const-string v0, "extra_prefill_description"

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289635
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->k:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2289636
    const-string v0, "extra_prefill_end_time"

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2289637
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-eqz v0, :cond_1

    .line 2289638
    const-string v0, "extra_search_category"

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289639
    :cond_1
    const-string v0, "extra_search_category_translated_name"

    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289640
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2289621
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2289622
    const v0, 0x7f0d137e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2289623
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_0

    .line 2289624
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    new-instance p1, LX/FoT;

    invoke-direct {p1, p0}, LX/FoT;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;)V

    .line 2289625
    iput-object p1, v0, LX/Fob;->e:LX/FoT;

    .line 2289626
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object p1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2289627
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/FoU;

    invoke-direct {p1, p0}, LX/FoU;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2289628
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/FoV;

    invoke-direct {p1, p0}, LX/FoV;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2289629
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    .line 2289630
    iget-object p0, v0, LX/BOa;->a:LX/0Zb;

    const-string p1, "fundraiser_create_promo_begin"

    const/4 p2, 0x0

    invoke-static {v0, p1, p2}, LX/BOa;->b(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289631
    return-void
.end method
