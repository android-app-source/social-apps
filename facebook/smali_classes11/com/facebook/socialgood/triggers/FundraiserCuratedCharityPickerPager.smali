.class public Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2289994
    const-class v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2289995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2289996
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->g:Z

    .line 2289997
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2289998
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->g:Z

    if-nez v0, :cond_1

    .line 2289999
    :cond_0
    :goto_0
    return-void

    .line 2290000
    :cond_1
    new-instance v0, LX/Fmp;

    invoke-direct {v0}, LX/Fmp;-><init>()V

    move-object v0, v0

    .line 2290001
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2290002
    new-instance v1, LX/0w7;

    invoke-direct {v1}, LX/0w7;-><init>()V

    .line 2290003
    const-string v2, "id"

    iget-object v3, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    .line 2290004
    const-string v2, "first"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;

    .line 2290005
    const-string v2, "page_cursor"

    iget-object v3, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    .line 2290006
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2290007
    sget-object v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2290008
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2290009
    iget-object v1, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2290010
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Foe;

    sget-object v2, LX/Fod;->CHARITIES:LX/Fod;

    invoke-direct {v1, p0, v2}, LX/Foe;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;LX/Fod;)V

    iget-object v2, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->d:LX/0Tf;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
