.class public Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private p:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2289523
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2289519
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2289520
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->p:LX/0h5;

    .line 2289521
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->p:LX/0h5;

    new-instance v1, LX/FoS;

    invoke-direct {v1, p0}, LX/FoS;-><init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2289522
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2289524
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2289525
    const v0, 0x7f0400c8

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->overridePendingTransition(II)V

    .line 2289526
    const v0, 0x7f03074f

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->setContentView(I)V

    .line 2289527
    new-instance v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-direct {v0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;-><init>()V

    .line 2289528
    invoke-virtual {p0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2289529
    invoke-direct {p0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->a()V

    .line 2289530
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d137d

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2289531
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2289517
    iget-object v0, p0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->p:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2289518
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2289514
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2289515
    const v0, 0x7f0400c8

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->overridePendingTransition(II)V

    .line 2289516
    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2289511
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 2289512
    const v0, 0x7f0400c8

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerActivity;->overridePendingTransition(II)V

    .line 2289513
    return-void
.end method
