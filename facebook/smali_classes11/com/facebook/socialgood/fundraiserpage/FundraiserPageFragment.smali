.class public Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/63S;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;

.field private static final k:Z


# instance fields
.field public A:LX/1Kf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/Fl8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private C:LX/0hI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public D:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

.field public F:Landroid/widget/TextView;

.field private G:Landroid/view/View;

.field public H:Ljava/lang/String;

.field public I:Ljava/lang/String;

.field public J:Ljava/lang/String;

.field public K:LX/BOc;

.field public L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

.field public M:Z

.field public N:Z

.field private O:LX/8FZ;

.field private P:I

.field private Q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private R:LX/0Yb;

.field private S:LX/Fl9;

.field public T:Landroid/view/ViewStub;

.field private U:Z

.field public V:LX/0g7;

.field public W:LX/63Q;

.field public X:LX/0h5;

.field private final Y:LX/CgB;

.field private final Z:LX/Cg9;

.field public volatile i:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private p:LX/CfW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0zG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/17W;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0sX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public v:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private w:LX/1vi;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private x:LX/E94;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private y:LX/Fku;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private z:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2279399
    const-class v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    .line 2279400
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->k:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2279513
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    .line 2279514
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279515
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->m:LX/0Ot;

    .line 2279516
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279517
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->r:LX/0Ot;

    .line 2279518
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279519
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->u:LX/0Ot;

    .line 2279520
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->M:Z

    .line 2279521
    new-instance v0, LX/Fkw;

    invoke-direct {v0, p0}, LX/Fkw;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Y:LX/CgB;

    .line 2279522
    new-instance v0, LX/Fky;

    invoke-direct {v0, p0}, LX/Fky;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Z:LX/Cg9;

    .line 2279523
    return-void
.end method

.method public static R(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V
    .locals 4

    .prologue
    .line 2279524
    new-instance v0, LX/FnY;

    invoke-direct {v0}, LX/FnY;-><init>()V

    move-object v0, v0

    .line 2279525
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2279526
    new-instance v1, LX/FnY;

    invoke-direct {v1}, LX/FnY;-><init>()V

    const-string v2, "campaign_id"

    iget-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->t:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2279527
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2279528
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2279529
    sget-object v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    .line 2279530
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2279531
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->o:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    move-object v0, v0

    .line 2279532
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2279533
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Fl4;

    invoke-direct {v1, p0}, LX/Fl4;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->n:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2279534
    return-void
.end method

.method public static V(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2279535
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->getDonateCallToActionButton()Landroid/view/View;

    move-result-object v0

    .line 2279536
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->x()Ljava/lang/String;

    move-result-object v1

    .line 2279537
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2279538
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2279539
    :cond_0
    :goto_0
    return-void

    .line 2279540
    :cond_1
    new-instance v4, LX/Fl1;

    invoke-direct {v4, p0, v1}, LX/Fl1;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;)V

    .line 2279541
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2279542
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2279543
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->T:Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    if-nez v0, :cond_0

    .line 2279544
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->D:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/FkF;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    move v0, v0

    .line 2279545
    if-eqz v0, :cond_0

    .line 2279546
    sget-boolean v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->C:LX/0hI;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v5

    .line 2279547
    :cond_2
    new-instance v0, LX/Fl9;

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->v:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->q:LX/0zG;

    iget-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->T:Landroid/view/ViewStub;

    invoke-direct/range {v0 .. v5}, LX/Fl9;-><init>(Landroid/content/Context;LX/0zG;Landroid/view/ViewStub;Landroid/view/View$OnClickListener;I)V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    goto :goto_0
.end method

.method private W()V
    .locals 1

    .prologue
    .line 2279548
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    if-eqz v0, :cond_0

    .line 2279549
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->b()V

    .line 2279550
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kK_()V

    .line 2279551
    return-void
.end method

.method private static a(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;LX/0Or;LX/0Zb;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/CfW;LX/0zG;LX/0Ot;LX/17W;LX/0sX;LX/0Ot;Landroid/content/Context;LX/1vi;LX/E94;LX/Fku;LX/0Xl;LX/1Kf;LX/Fl8;LX/0hI;LX/0ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;",
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/CfW;",
            "LX/0zG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/17W;",
            "LX/0sX;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "Landroid/content/Context;",
            "LX/1vi;",
            "LX/E94;",
            "LX/Fku;",
            "LX/0Xl;",
            "LX/1Kf;",
            "LX/Fl8;",
            "LX/0hI;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2279552
    iput-object p1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->i:LX/0Or;

    iput-object p2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->l:LX/0Zb;

    iput-object p3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->m:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->n:Ljava/util/concurrent/ExecutorService;

    iput-object p5, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->o:LX/0tX;

    iput-object p6, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->p:LX/CfW;

    iput-object p7, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->q:LX/0zG;

    iput-object p8, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->r:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->s:LX/17W;

    iput-object p10, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->t:LX/0sX;

    iput-object p11, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->u:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->v:Landroid/content/Context;

    iput-object p13, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->w:LX/1vi;

    iput-object p14, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->x:LX/E94;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->y:LX/Fku;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->z:LX/0Xl;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->A:LX/1Kf;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->B:LX/Fl8;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->C:LX/0hI;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->D:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 22

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v21

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    const/16 v2, 0x19c6

    move-object/from16 v0, v21

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static/range {v21 .. v21}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const/16 v4, 0x259

    move-object/from16 v0, v21

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {v21 .. v21}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v21 .. v21}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static/range {v21 .. v21}, LX/CfW;->a(LX/0QB;)LX/CfW;

    move-result-object v7

    check-cast v7, LX/CfW;

    invoke-static/range {v21 .. v21}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v8

    check-cast v8, LX/0zG;

    const/16 v9, 0x455

    move-object/from16 v0, v21

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v21 .. v21}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v10

    check-cast v10, LX/17W;

    invoke-static/range {v21 .. v21}, LX/0sX;->a(LX/0QB;)LX/0sX;

    move-result-object v11

    check-cast v11, LX/0sX;

    const/16 v12, 0xc49

    move-object/from16 v0, v21

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const-class v13, Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-interface {v0, v13}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Context;

    invoke-static/range {v21 .. v21}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v14

    check-cast v14, LX/1vi;

    const-class v15, LX/E94;

    move-object/from16 v0, v21

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/E94;

    invoke-static/range {v21 .. v21}, LX/Fku;->a(LX/0QB;)LX/Fku;

    move-result-object v16

    check-cast v16, LX/Fku;

    invoke-static/range {v21 .. v21}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v17

    check-cast v17, LX/0Xl;

    invoke-static/range {v21 .. v21}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v18

    check-cast v18, LX/1Kf;

    invoke-static/range {v21 .. v21}, LX/Fl8;->a(LX/0QB;)LX/Fl8;

    move-result-object v19

    check-cast v19, LX/Fl8;

    invoke-static/range {v21 .. v21}, LX/0hI;->a(LX/0QB;)LX/0hI;

    move-result-object v20

    check-cast v20, LX/0hI;

    invoke-static/range {v21 .. v21}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v21

    check-cast v21, LX/0ad;

    invoke-static/range {v1 .. v21}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->a(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;LX/0Or;LX/0Zb;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/CfW;LX/0zG;LX/0Ot;LX/17W;LX/0sX;LX/0Ot;Landroid/content/Context;LX/1vi;LX/E94;LX/Fku;LX/0Xl;LX/1Kf;LX/Fl8;LX/0hI;LX/0ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2279553
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fundraiser_page_fragment_error"

    invoke-virtual {v0, v1, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2279554
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->W()V

    .line 2279555
    return-void
.end method

.method public static d(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2279608
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fundraiser_page_fragment_error"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279609
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->W()V

    .line 2279610
    return-void
.end method


# virtual methods
.method public final G()LX/1SX;
    .locals 1

    .prologue
    .line 2279556
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->y:LX/Fku;

    return-object v0
.end method

.method public final H()LX/1PT;
    .locals 1

    .prologue
    .line 2279557
    new-instance v0, LX/Fl0;

    invoke-direct {v0, p0}, LX/Fl0;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    return-object v0
.end method

.method public final O()V
    .locals 4

    .prologue
    .line 2279558
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->P:I

    .line 2279559
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    if-eqz v0, :cond_0

    .line 2279560
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-virtual {v1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->getDonateCallToActionButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b21f4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 2279561
    iput v1, v0, LX/Fl9;->b:I

    .line 2279562
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;)LX/E8m;
    .locals 3

    .prologue
    .line 2279563
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->x:LX/E94;

    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->H()LX/1PT;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->G()LX/1SX;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2, p0}, LX/E94;->a(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;)LX/E93;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/widget/FrameLayout;
    .locals 3

    .prologue
    .line 2279564
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0b2d

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2279565
    invoke-super {p0, v0, p2}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/widget/FrameLayout;

    move-result-object v0

    .line 2279566
    const v1, 0x7f0d13a7

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    iput-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    .line 2279567
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-virtual {p0, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(LX/E8t;)V

    .line 2279568
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-virtual {v1}, LX/E8t;->a()V

    .line 2279569
    const v1, 0x7f0d13ad

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->F:Landroid/widget/TextView;

    .line 2279570
    const v1, 0x7f0d13a8

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    iput-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->T:Landroid/view/ViewStub;

    .line 2279571
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    if-eqz v1, :cond_0

    .line 2279572
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    iget-object p1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->J:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;Ljava/lang/String;)V

    .line 2279573
    invoke-static {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->V(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    .line 2279574
    :cond_0
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2279512
    const-string v0, "social_good"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279575
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2279576
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2279577
    if-eqz p1, :cond_1

    .line 2279578
    const-string v0, "post_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->H:Ljava/lang/String;

    .line 2279579
    const-string v0, "should_fetch_fundraiser_data"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->M:Z

    .line 2279580
    const-string v0, "has_logged_view"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->N:Z

    .line 2279581
    const-string v0, "fundraiser_data_model"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2279582
    const-string v0, "action_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BOc;->fromString(Ljava/lang/String;)LX/BOc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->K:LX/BOc;

    .line 2279583
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2279584
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    .line 2279585
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2279586
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->J:Ljava/lang/String;

    .line 2279587
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2279588
    const-string v0, "No campaign ID passed in"

    invoke-static {p0, v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->d(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;)V

    .line 2279589
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2279590
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->K:LX/BOc;

    if-nez v0, :cond_2

    .line 2279591
    :goto_1
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->O:LX/8FZ;

    .line 2279592
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->O:LX/8FZ;

    .line 2279593
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2279594
    return-void

    .line 2279595
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2279596
    const-string v1, "post_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->H:Ljava/lang/String;

    .line 2279597
    iput-boolean v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->M:Z

    .line 2279598
    iput-boolean v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->N:Z

    .line 2279599
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2279600
    const-string v1, "action_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BOc;->fromString(Ljava/lang/String;)LX/BOc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->K:LX/BOc;

    goto :goto_0

    .line 2279601
    :cond_2
    const/4 v0, 0x0

    .line 2279602
    sget-object v1, LX/Fl3;->a:[I

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->K:LX/BOc;

    invoke-virtual {v2}, LX/BOc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move-object v1, v0

    .line 2279603
    :goto_2
    if-eqz v1, :cond_3

    .line 2279604
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->v:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2279605
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->v:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2279606
    :cond_3
    sget-object v0, LX/BOc;->NONE:LX/BOc;

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->K:LX/BOc;

    goto :goto_1

    .line 2279607
    :pswitch_0
    sget-object v0, LX/0ax;->gO:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    const-string v2, "native_page_action"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    .line 2279491
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->D()I

    move-result v0

    .line 2279492
    if-eqz v0, :cond_1

    .line 2279493
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    iget v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->P:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->setTranslationY(F)V

    .line 2279494
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->p()V

    .line 2279495
    :cond_0
    :goto_0
    return-void

    .line 2279496
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->G:Landroid/view/View;

    if-nez v0, :cond_2

    .line 2279497
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->G:Landroid/view/View;

    .line 2279498
    :cond_2
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->G:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2279499
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->G:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2279500
    if-nez v0, :cond_3

    .line 2279501
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2279502
    :goto_1
    iget v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->P:I

    neg-int v1, v1

    .line 2279503
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2279504
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->setTranslationY(F)V

    .line 2279505
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    if-eqz v1, :cond_0

    .line 2279506
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    .line 2279507
    iget v2, v1, LX/Fl9;->b:I

    add-int/2addr v2, v0

    iget p0, v1, LX/Fl9;->d:I

    sub-int/2addr v2, p0

    if-gez v2, :cond_4

    .line 2279508
    invoke-static {v1}, LX/Fl9;->b(LX/Fl9;)Landroid/widget/FrameLayout;

    move-result-object v2

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2279509
    :goto_2
    goto :goto_0

    .line 2279510
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->p()V

    goto :goto_1

    .line 2279511
    :cond_4
    invoke-static {v1}, LX/Fl9;->b(LX/Fl9;)Landroid/widget/FrameLayout;

    move-result-object v2

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2279487
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->M:Z

    .line 2279488
    invoke-static {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    .line 2279489
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->e()V

    .line 2279490
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2279486
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->D()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2279485
    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->G:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2279483
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->U:Z

    .line 2279484
    return-void
.end method

.method public final lZ_()I
    .locals 1

    .prologue
    .line 2279482
    const v0, 0x7f03075e

    return v0
.end method

.method public final mK_()LX/63R;
    .locals 1

    .prologue
    .line 2279477
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    if-nez v0, :cond_0

    .line 2279478
    const/4 v0, 0x0

    .line 2279479
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    .line 2279480
    iget-object p0, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->c:Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    move-object v0, p0

    .line 2279481
    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2279468
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2279469
    const/16 v0, 0x82

    if-ne p1, v0, :cond_1

    .line 2279470
    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->e()V

    .line 2279471
    :cond_0
    :goto_0
    return-void

    .line 2279472
    :cond_1
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_2

    .line 2279473
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2279474
    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2279475
    :cond_2
    const/16 v0, 0x1bc

    if-ne p1, v0, :cond_0

    .line 2279476
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x60d9ac89

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2279462
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onDestroy()V

    .line 2279463
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->w:LX/1vi;

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Y:LX/CgB;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2279464
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->w:LX/1vi;

    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Z:LX/Cg9;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2279465
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R:LX/0Yb;

    if-eqz v1, :cond_0

    .line 2279466
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2279467
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1c93013

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x1df88dc5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2279445
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    if-eqz v1, :cond_0

    .line 2279446
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-virtual {v1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->getDonateCallToActionButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2279447
    :cond_0
    iput-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->G:Landroid/view/View;

    .line 2279448
    iput-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    .line 2279449
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    .line 2279450
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2279451
    :cond_1
    iput-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->F:Landroid/widget/TextView;

    .line 2279452
    iput-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->T:Landroid/view/ViewStub;

    .line 2279453
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    if-eqz v1, :cond_3

    .line 2279454
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    const/4 v5, 0x0

    .line 2279455
    iget-object v2, v1, LX/Fl9;->c:Lcom/facebook/resources/ui/FbButton;

    if-eqz v2, :cond_2

    .line 2279456
    iget-object v2, v1, LX/Fl9;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, v5}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2279457
    iput-object v5, v1, LX/Fl9;->c:Lcom/facebook/resources/ui/FbButton;

    .line 2279458
    :cond_2
    iput-object v5, v1, LX/Fl9;->a:Landroid/widget/FrameLayout;

    .line 2279459
    iput-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->S:LX/Fl9;

    .line 2279460
    :cond_3
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onDestroyView()V

    .line 2279461
    const/16 v1, 0x2b

    const v2, -0x659151dc

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2279438
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2279439
    const-string v0, "post_id"

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279440
    const-string v0, "action_type"

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->K:LX/BOc;

    invoke-virtual {v1}, LX/BOc;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279441
    const-string v0, "should_fetch_fundraiser_data"

    iget-boolean v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->M:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2279442
    const-string v0, "has_logged_view"

    iget-boolean v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2279443
    const-string v0, "fundraiser_data_model"

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2279444
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x479ee8a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2279433
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onStart()V

    .line 2279434
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2279435
    if-eqz v0, :cond_0

    .line 2279436
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2279437
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x655250dc

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279409
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2279410
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2279411
    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-static {v0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2279412
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2279413
    if-eqz v0, :cond_4

    .line 2279414
    :cond_3
    invoke-static {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    .line 2279415
    :cond_4
    const v0, 0x102000a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2279416
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 2279417
    move-object v0, v1

    .line 2279418
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->V:LX/0g7;

    .line 2279419
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->q:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->U:Z

    if-nez v0, :cond_5

    .line 2279420
    const-class v3, LX/63U;

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/63U;

    .line 2279421
    iget-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->W:LX/63Q;

    if-nez v3, :cond_5

    if-eqz v7, :cond_5

    .line 2279422
    new-instance v3, LX/63Q;

    invoke-direct {v3}, LX/63Q;-><init>()V

    iput-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->W:LX/63Q;

    .line 2279423
    iget-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->q:LX/0zG;

    invoke-interface {v3}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0h5;

    iput-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->X:LX/0h5;

    .line 2279424
    iget-object v3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->W:LX/63Q;

    iget-object v5, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->X:LX/0h5;

    check-cast v5, LX/63T;

    iget-object v6, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->V:LX/0g7;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v4, p0

    invoke-virtual/range {v3 .. v9}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    .line 2279425
    :cond_5
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->w:LX/1vi;

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Y:LX/CgB;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2279426
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->w:LX/1vi;

    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->Z:LX/Cg9;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2279427
    new-instance v0, LX/Fkz;

    invoke-direct {v0, p0}, LX/Fkz;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    .line 2279428
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R:LX/0Yb;

    if-eqz v1, :cond_6

    .line 2279429
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2279430
    :cond_6
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->z:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R:LX/0Yb;

    .line 2279431
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2279432
    return-void

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public final v()LX/2jY;
    .locals 5

    .prologue
    .line 2279401
    const-wide/16 v0, 0x0

    .line 2279402
    :try_start_0
    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2279403
    :goto_0
    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->p:LX/CfW;

    const-string v3, "ANDROID_FUNDRAISER_PAGE"

    new-instance v4, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v4}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2279404
    iput-object v0, v4, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2279405
    move-object v0, v4

    .line 2279406
    invoke-virtual {v2, v3, v0}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    return-object v0

    .line 2279407
    :catch_0
    move-exception v2

    .line 2279408
    const-string v3, "Could not parse campaign id"

    invoke-static {p0, v3, v2}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->a$redex0(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
