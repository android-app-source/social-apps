.class public Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;
.super LX/E8t;
.source ""


# instance fields
.field public b:LX/FkP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2279646
    invoke-direct {p0, p1}, LX/E8t;-><init>(Landroid/content/Context;)V

    .line 2279647
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->e()V

    .line 2279648
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279618
    invoke-direct {p0, p1, p2}, LX/E8t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2279619
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->e()V

    .line 2279620
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279643
    invoke-direct {p0, p1, p2, p3}, LX/E8t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2279644
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->e()V

    .line 2279645
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-static {v0}, LX/FkP;->b(LX/0QB;)LX/FkP;

    move-result-object v0

    check-cast v0, LX/FkP;

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->b:LX/FkP;

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2279639
    const-class v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2279640
    const v0, 0x7f03075f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2279641
    const v0, 0x7f0d13ab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->c:Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    .line 2279642
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 2279633
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279634
    new-instance v1, LX/FlJ;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p1 .. p1}, LX/Fkv;->f(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p1 .. p1}, LX/Fkv;->h(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->l()Z

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {p1 .. p1}, LX/Fkv;->n(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p1 .. p1}, LX/Fkv;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->x()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    invoke-static/range {p1 .. p1}, LX/Fkv;->o(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v11

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->b:LX/FkP;

    invoke-virtual {v6}, LX/FkP;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static/range {p1 .. p1}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v12, 0x1

    :goto_0
    invoke-static/range {p1 .. p1}, LX/Fkv;->q(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    invoke-static/range {p1 .. p1}, LX/Fkv;->t(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;

    move-result-object v14

    invoke-static/range {p1 .. p1}, LX/Fkv;->u(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v15

    invoke-static/range {p1 .. p1}, LX/Fkv;->r(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v16

    invoke-static/range {p1 .. p1}, LX/Fkv;->s(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v17

    move-object/from16 v6, p2

    invoke-direct/range {v1 .. v17}, LX/FlJ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZZLjava/lang/String;Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;ZZ)V

    .line 2279635
    const v2, 0x7f0d13ae

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

    invoke-static/range {p1 .. p1}, LX/Fkv;->p(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a(LX/FlJ;Z)V

    .line 2279636
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->c:Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279637
    return-void

    .line 2279638
    :cond_0
    const/4 v12, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2279623
    const v0, 0x7f0d13aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2279624
    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2279625
    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2279626
    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2279627
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2279628
    const v0, 0x7f0d13ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2279629
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->c:Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    invoke-virtual {v0, v3}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->setVisibility(I)V

    .line 2279630
    const v0, 0x7f0d13ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2279631
    const v0, 0x7f0d13af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2279632
    return-void
.end method

.method public getCoverHeaderView()Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;
    .locals 1

    .prologue
    .line 2279622
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->c:Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    return-object v0
.end method

.method public getDonateCallToActionButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 2279621
    const v0, 0x7f0d13ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
