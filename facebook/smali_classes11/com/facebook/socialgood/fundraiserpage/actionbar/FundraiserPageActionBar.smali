.class public Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;
.super LX/AhO;
.source ""


# instance fields
.field public a:LX/FlI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/FlB;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/FlJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2279807
    invoke-direct {p0, p1}, LX/AhO;-><init>(Landroid/content/Context;)V

    .line 2279808
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    .line 2279809
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a()V

    .line 2279810
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2279803
    invoke-direct {p0, p1, p2}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2279804
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    .line 2279805
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a()V

    .line 2279806
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2279799
    invoke-direct {p0, p1, p2, p3}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2279800
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    .line 2279801
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a()V

    .line 2279802
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2279789
    const-class v0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2279790
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->setGravity(I)V

    .line 2279791
    const v0, 0x7f021903

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->setBackgroundResource(I)V

    .line 2279792
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a:LX/FlI;

    .line 2279793
    iput-object v0, p0, LX/AhO;->b:LX/AhN;

    .line 2279794
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a:LX/FlI;

    .line 2279795
    iput-object p0, v0, LX/FlI;->p:Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

    .line 2279796
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/AhO;->setMaxNumOfVisibleButtons(I)V

    .line 2279797
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, v1}, LX/AhO;->a(ZZI)V

    .line 2279798
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

    invoke-static {v0}, LX/FlI;->b(LX/0QB;)LX/FlI;

    move-result-object v0

    check-cast v0, LX/FlI;

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a:LX/FlI;

    return-void
.end method

.method private static a(LX/FlB;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2279786
    sget-object v1, LX/FlA;->a:[I

    invoke-virtual {p0}, LX/FlB;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2279787
    :goto_0
    :pswitch_0
    return v0

    .line 2279788
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(LX/FlB;LX/FlJ;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2279766
    iget-object v2, p1, LX/FlJ;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2279767
    sget-object v3, LX/FlA;->a:[I

    invoke-virtual {p0}, LX/FlB;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 2279768
    :cond_0
    :goto_0
    return v0

    .line 2279769
    :pswitch_0
    iget-boolean v3, p1, LX/FlJ;->d:Z

    move v3, v3

    .line 2279770
    if-eqz v3, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 2279771
    :pswitch_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2279772
    iget-object v2, p1, LX/FlJ;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2279773
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2279774
    :pswitch_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2279775
    iget-object v2, p1, LX/FlJ;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2279776
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 2279777
    :pswitch_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2279778
    :pswitch_4
    iget-object v2, p1, LX/FlJ;->g:Ljava/lang/String;

    move-object v2, v2

    .line 2279779
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2279780
    :pswitch_5
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2279781
    iget-boolean v2, p1, LX/FlJ;->o:Z

    move v2, v2

    .line 2279782
    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 2279783
    :pswitch_6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2279784
    iget-boolean v2, p1, LX/FlJ;->p:Z

    move v2, v2

    .line 2279785
    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static a(LX/FlB;ZZZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2279760
    sget-object v2, LX/FlA;->a:[I

    invoke-virtual {p0}, LX/FlB;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move p1, v1

    .line 2279761
    :goto_0
    :pswitch_0
    return p1

    :pswitch_1
    move p1, v0

    .line 2279762
    goto :goto_0

    .line 2279763
    :pswitch_2
    if-nez p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    goto :goto_0

    :pswitch_3
    move p1, p2

    .line 2279764
    goto :goto_0

    :pswitch_4
    move p1, p3

    .line 2279765
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/FlJ;Z)V
    .locals 1

    .prologue
    .line 2279756
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    if-nez v0, :cond_0

    .line 2279757
    iput-object p1, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279758
    :cond_0
    invoke-virtual {p0, p2}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->setupActionBar(Z)V

    .line 2279759
    return-void
.end method

.method public final bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2279755
    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .prologue
    .line 2279752
    invoke-super {p0}, LX/AhO;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    .line 2279753
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2279754
    return-object v0
.end method

.method public getActionBarParameters()LX/FlJ;
    .locals 1

    .prologue
    .line 2279751
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    return-object v0
.end method

.method public setupActionBar(Z)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 2279695
    invoke-virtual {p0}, LX/AhO;->b()V

    .line 2279696
    invoke-virtual {p0}, LX/AhO;->clear()V

    .line 2279697
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a:LX/FlI;

    .line 2279698
    iput-boolean p1, v0, LX/FlI;->q:Z

    .line 2279699
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279700
    iget-boolean v1, v0, LX/FlJ;->o:Z

    move v3, v1

    .line 2279701
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    .line 2279702
    if-eqz v3, :cond_0

    .line 2279703
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    sget-object v1, LX/FlB;->EDIT_FUNDRAISER:LX/FlB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279704
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    sget-object v1, LX/FlB;->INVITE:LX/FlB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279705
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    sget-object v1, LX/FlB;->SHARE:LX/FlB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279706
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279707
    iget-object v1, v0, LX/FlJ;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2279708
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2279709
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    sget-object v1, LX/FlB;->GO_TO_PAGE:LX/FlB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279710
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279711
    iget-object v1, v0, LX/FlJ;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2279712
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2279713
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    sget-object v1, LX/FlB;->COPY_LINK:LX/FlB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279714
    :cond_2
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279715
    iget-boolean v1, v0, LX/FlJ;->j:Z

    move v0, v1

    .line 2279716
    if-eqz v0, :cond_3

    .line 2279717
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    sget-object v1, LX/FlB;->REPORT_FUNDRAISER:LX/FlB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279718
    :cond_3
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279719
    iget-boolean v1, v0, LX/FlJ;->k:Z

    move v0, v1

    .line 2279720
    if-eqz v0, :cond_4

    .line 2279721
    if-eqz p1, :cond_8

    .line 2279722
    if-eqz v3, :cond_7

    sget-object v0, LX/FlB;->UNFOLLOW_FUNDRAISER:LX/FlB;

    .line 2279723
    :goto_0
    if-eqz v3, :cond_a

    .line 2279724
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279725
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279726
    iget-boolean v1, v0, LX/FlJ;->p:Z

    move v0, v1

    .line 2279727
    if-eqz v0, :cond_5

    .line 2279728
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    sget-object v1, LX/FlB;->DELETE_FUNDRAISER:LX/FlB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279729
    :cond_5
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_c

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FlB;

    .line 2279730
    invoke-virtual {v0}, LX/FlB;->ordinal()I

    move-result v5

    invoke-virtual {v0}, LX/FlB;->getTitleResId()I

    move-result v6

    invoke-virtual {p0, v2, v5, v2, v6}, LX/AhO;->a(IIII)LX/3qv;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    invoke-static {v0, v6}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a(LX/FlB;LX/FlJ;)Z

    move-result v6

    invoke-interface {v5, v6}, LX/3qv;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v5

    invoke-static {v0}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a(LX/FlB;)Z

    move-result v6

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279731
    iget-boolean v8, v6, LX/FlJ;->p:Z

    move v6, v8

    .line 2279732
    invoke-static {v0, p1, v3, v6}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->a(LX/FlB;ZZZ)Z

    move-result v6

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v5

    invoke-virtual {v0}, LX/FlB;->getIconResId()I

    move-result v6

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 2279733
    invoke-virtual {v0}, LX/FlB;->isOverflow()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2279734
    invoke-static {v5, v2}, LX/3rl;->a(Landroid/view/MenuItem;I)Z

    .line 2279735
    :goto_3
    sget-object v6, LX/FlB;->FOLLOWING:LX/FlB;

    if-ne v0, v6, :cond_6

    .line 2279736
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 2279737
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2279738
    :cond_7
    sget-object v0, LX/FlB;->FOLLOWING:LX/FlB;

    goto :goto_0

    .line 2279739
    :cond_8
    if-eqz v3, :cond_9

    sget-object v0, LX/FlB;->FOLLOW_FUNDRAISER:LX/FlB;

    goto :goto_0

    :cond_9
    sget-object v0, LX/FlB;->FOLLOW:LX/FlB;

    goto :goto_0

    .line 2279740
    :cond_a
    iget-object v1, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 2279741
    :cond_b
    const/4 v6, 0x2

    invoke-static {v5, v6}, LX/3rl;->a(Landroid/view/MenuItem;I)Z

    goto :goto_3

    .line 2279742
    :cond_c
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279743
    iget-boolean v1, v0, LX/FlJ;->i:Z

    move v0, v1

    .line 2279744
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    .line 2279745
    iget-boolean v1, v0, LX/FlJ;->l:Z

    move v0, v1

    .line 2279746
    if-eqz v0, :cond_d

    .line 2279747
    invoke-virtual {p0, v7, v7, v2}, LX/AhO;->a(ZZI)V

    .line 2279748
    :cond_d
    invoke-virtual {p0}, LX/AhO;->d()V

    .line 2279749
    invoke-virtual {p0, v2}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->setVisibility(I)V

    .line 2279750
    return-void
.end method
