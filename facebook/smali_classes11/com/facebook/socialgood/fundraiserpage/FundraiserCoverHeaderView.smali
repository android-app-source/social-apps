.class public Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;
.super LX/Ban;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;

.field private static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private o:LX/Bas;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/1Uf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2279055
    const-class v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    const-string v1, "social_good"

    const-string v2, "cover_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->m:Lcom/facebook/common/callercontext/CallerContext;

    .line 2279056
    const-class v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    const-string v1, "social_good"

    const-string v2, "profile_picture"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2279066
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2279067
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279068
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->p:LX/0Ot;

    .line 2279069
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279070
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->q:LX/0Ot;

    .line 2279071
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279072
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->s:LX/0Ot;

    .line 2279073
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->b()V

    .line 2279074
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2279057
    invoke-direct {p0, p1, p2, p3}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2279058
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279059
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->p:LX/0Ot;

    .line 2279060
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279061
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->q:LX/0Ot;

    .line 2279062
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279063
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->s:LX/0Ot;

    .line 2279064
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->b()V

    .line 2279065
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 2279046
    invoke-direct {p0, p1}, LX/Ban;-><init>(Landroid/content/Context;)V

    .line 2279047
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279048
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->p:LX/0Ot;

    .line 2279049
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279050
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->q:LX/0Ot;

    .line 2279051
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2279052
    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->s:LX/0Ot;

    .line 2279053
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->b()V

    .line 2279054
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 2279042
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-static {v0}, LX/Fkv;->i(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v1

    .line 2279043
    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2279044
    new-instance v0, LX/Fkm;

    invoke-direct {v0, p0, v1, p1}, LX/Fkm;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;Ljava/lang/String;)V

    .line 2279045
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2279038
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object v1, LX/74S;->FUNDRAISER_COVER_PHOTO:LX/74S;

    invoke-interface {v0, p1, p2, p4, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(JLjava/lang/String;LX/74S;)Landroid/content/Intent;

    move-result-object v1

    .line 2279039
    if-eqz v1, :cond_0

    .line 2279040
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2279041
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;LX/Bas;LX/0Ot;LX/0Ot;LX/1Uf;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;",
            "LX/Bas;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/1Uf;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2279037
    iput-object p1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->o:LX/Bas;

    iput-object p2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->p:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->q:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->r:LX/1Uf;

    iput-object p5, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->s:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    invoke-static {v5}, LX/Bas;->b(LX/0QB;)LX/Bas;

    move-result-object v1

    check-cast v1, LX/Bas;

    const/16 v2, 0xbc6

    invoke-static {v5, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {v5, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v5}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    const/16 v6, 0xbc6

    invoke-static {v5, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->a(Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;LX/Bas;LX/0Ot;LX/0Ot;LX/1Uf;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2279032
    const/4 v0, 0x0

    .line 2279033
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel$AlbumModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel$AlbumModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel$AlbumModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2279034
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel$AlbumModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel$AlbumModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/photos/data/model/PhotoSet;->c(J)Ljava/lang/String;

    move-result-object v0

    .line 2279035
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3, p2, v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 2279036
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2279023
    const-class v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2279024
    sget-object v0, LX/Bam;->NARROW:LX/Bam;

    iput-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->d:LX/Bam;

    .line 2279025
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2279026
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setVisibility(I)V

    .line 2279027
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0886

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setMinimumHeight(I)V

    .line 2279028
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2279029
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2279030
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2279031
    return-void
.end method

.method private g()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2278999
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2279000
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    const/4 v5, 0x0

    .line 2279001
    invoke-static {v0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279002
    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2279003
    sget-object v4, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v0}, LX/Fkv;->f(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2279004
    :goto_0
    move-object v0, v4

    .line 2279005
    if-eqz v0, :cond_0

    .line 2279006
    new-instance v8, LX/Fkl;

    invoke-direct {v8, p0, v0}, LX/Fkl;-><init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;Ljava/lang/String;)V

    .line 2279007
    :goto_1
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->o:LX/Bas;

    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    const/4 v4, 0x1

    sget-object v7, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0}, LX/Ban;->getProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-result-object v10

    move v5, v3

    move v6, v3

    move-object v9, v1

    invoke-virtual/range {v0 .. v10}, LX/Bas;->a(LX/1bf;LX/1bf;ZZZZLcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)V

    .line 2279008
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    invoke-virtual {v0, v3}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setVisibility(I)V

    .line 2279009
    return-void

    :cond_0
    move-object v8, v1

    goto :goto_1

    .line 2279010
    :cond_1
    invoke-static {v0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2279011
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279012
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2279013
    invoke-static {v0}, LX/Fkv;->w(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I

    move-result v4

    const v6, 0x285feb

    if-ne v4, v6, :cond_6

    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 2279014
    if-eqz v4, :cond_3

    .line 2279015
    sget-object v4, LX/0ax;->bE:Ljava/lang/String;

    .line 2279016
    :goto_4
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2279017
    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 2279018
    :cond_3
    invoke-static {v0}, LX/Fkv;->w(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I

    move-result v4

    const v6, 0x25d6af

    if-ne v4, v6, :cond_7

    const/4 v4, 0x1

    :goto_5
    move v4, v4

    .line 2279019
    if-eqz v4, :cond_4

    .line 2279020
    sget-object v4, LX/0ax;->aE:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object v4, v5

    .line 2279021
    goto :goto_0

    :cond_5
    move-object v4, v5

    .line 2279022
    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    goto :goto_5
.end method

.method private h()V
    .locals 15

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 2278978
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    const/4 v1, 0x0

    .line 2278979
    invoke-static {v0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2278980
    invoke-static {v0}, LX/Fkv;->i(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v2

    .line 2278981
    if-eqz v2, :cond_0

    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2278982
    :cond_0
    :goto_0
    move-object v5, v1

    .line 2278983
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-static {v0}, LX/Fkv;->k(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2278984
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2278985
    invoke-static {v0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2278986
    invoke-static {v0}, LX/Fkv;->i(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v2

    .line 2278987
    if-eqz v2, :cond_1

    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2278988
    :cond_1
    const/4 v1, 0x0

    .line 2278989
    :goto_1
    move-object v9, v1

    .line 2278990
    invoke-direct {p0, v5}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->a(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v11

    .line 2278991
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2278992
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    invoke-static {v5}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    sget-object v10, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->m:Lcom/facebook/common/callercontext/CallerContext;

    move v8, v7

    move-object v12, v4

    move v13, v7

    move v14, v3

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2278993
    return-void

    .line 2278994
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2278995
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2278996
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2278997
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v9

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    goto :goto_1

    .line 2278998
    :cond_4
    new-array v1, v10, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    goto :goto_1
.end method

.method private i()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2278965
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2278966
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v2, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2278967
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2278968
    iget-object v2, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->r:LX/1Uf;

    invoke-static {v0}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v4, v3}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v4

    .line 2278969
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2278970
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2278971
    invoke-static {v0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2278972
    invoke-static {v0}, LX/Fkv;->A(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v2

    .line 2278973
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 2278974
    if-eqz v0, :cond_0

    .line 2278975
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const v4, 0x7f021a25

    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0b21cd

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0b21ce

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    move v5, v1

    move-object v9, v3

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2278976
    :goto_1
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2278977
    return-void

    :cond_0
    move-object v0, v4

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)I
    .locals 2

    .prologue
    .line 2278949
    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0429

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V
    .locals 2

    .prologue
    .line 2278957
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2278958
    iput-object p1, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2278959
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v1, 0x7f0e0b2b

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2278960
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v1, 0x7f0e0b2c

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleTextAppearance(I)V

    .line 2278961
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->g()V

    .line 2278962
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->h()V

    .line 2278963
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->i()V

    .line 2278964
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2278950
    invoke-super {p0, p1}, LX/Ban;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2278951
    invoke-virtual {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->b:I

    .line 2278952
    iget-object v0, p0, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->t:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    if-eqz v0, :cond_0

    .line 2278953
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->g()V

    .line 2278954
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->h()V

    .line 2278955
    invoke-direct {p0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserCoverHeaderView;->i()V

    .line 2278956
    :cond_0
    return-void
.end method
