.class public Lcom/facebook/socialgood/model/Fundraiser;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/socialgood/model/Fundraiser;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:J

.field public i:I

.field public j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2282079
    new-instance v0, LX/Fm8;

    invoke-direct {v0}, LX/Fm8;-><init>()V

    sput-object v0, Lcom/facebook/socialgood/model/Fundraiser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Fm9;)V
    .locals 2

    .prologue
    .line 2282080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282081
    iget-object v0, p1, LX/Fm9;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->a:Ljava/lang/String;

    .line 2282082
    iget-object v0, p1, LX/Fm9;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->b:Ljava/lang/String;

    .line 2282083
    iget-boolean v0, p1, LX/Fm9;->c:Z

    iput-boolean v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->c:Z

    .line 2282084
    iget-object v0, p1, LX/Fm9;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->d:Ljava/lang/String;

    .line 2282085
    iget-object v0, p1, LX/Fm9;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2282086
    iget-object v0, p1, LX/Fm9;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->f:Ljava/lang/String;

    .line 2282087
    iget-object v0, p1, LX/Fm9;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->g:Ljava/lang/String;

    .line 2282088
    iget-wide v0, p1, LX/Fm9;->h:J

    iput-wide v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->h:J

    .line 2282089
    iget v0, p1, LX/Fm9;->i:I

    iput v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->i:I

    .line 2282090
    iget-object v0, p1, LX/Fm9;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->j:Ljava/lang/String;

    .line 2282091
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2282092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282093
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/model/Fundraiser;->a:Ljava/lang/String;

    .line 2282094
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/model/Fundraiser;->b:Ljava/lang/String;

    .line 2282095
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->c:Z

    .line 2282096
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->d:Ljava/lang/String;

    .line 2282097
    const-class v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2282098
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->f:Ljava/lang/String;

    .line 2282099
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->g:Ljava/lang/String;

    .line 2282100
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->h:J

    .line 2282101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->i:I

    .line 2282102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->j:Ljava/lang/String;

    .line 2282103
    return-void

    .line 2282104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2282105
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2282106
    iget-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2282107
    iget-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2282108
    iget-boolean v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2282109
    iget-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2282110
    iget-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2282111
    iget-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2282112
    iget-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2282113
    iget-wide v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2282114
    iget v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2282115
    iget-object v0, p0, Lcom/facebook/socialgood/model/Fundraiser;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2282116
    return-void

    :cond_0
    move v0, v1

    .line 2282117
    goto :goto_0
.end method
