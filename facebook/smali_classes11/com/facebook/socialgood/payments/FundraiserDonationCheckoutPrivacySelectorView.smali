.class public Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;
.super LX/6E7;
.source ""


# instance fields
.field public a:Lcom/facebook/privacy/ui/PrivacyOptionView;

.field public b:Landroid/widget/TextView;

.field public c:LX/8RJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/6qh;

.field public e:Lcom/facebook/privacy/model/SelectablePrivacyData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2282286
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 2282287
    invoke-direct {p0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a()V

    .line 2282288
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2282289
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2282290
    invoke-direct {p0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a()V

    .line 2282291
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2282267
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2282268
    invoke-direct {p0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a()V

    .line 2282269
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2282281
    const-class v0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2282282
    const v0, 0x7f030756

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2282283
    const v0, 0x7f0d1383

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/ui/PrivacyOptionView;

    iput-object v0, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a:Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 2282284
    const v0, 0x7f0d1384

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->b:Landroid/widget/TextView;

    .line 2282285
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2282292
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eq v0, v1, :cond_0

    .line 2282293
    iget-object v0, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2282294
    :goto_0
    return-void

    .line 2282295
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-static {v0}, LX/8RJ;->a(LX/0QB;)LX/8RJ;

    move-result-object v0

    check-cast v0, LX/8RJ;

    iput-object v0, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->c:LX/8RJ;

    return-void
.end method


# virtual methods
.method public setPaymentsComponentCallback(LX/6qh;)V
    .locals 0

    .prologue
    .line 2282279
    iput-object p1, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->d:LX/6qh;

    .line 2282280
    return-void
.end method

.method public setPrivacyData(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 2

    .prologue
    .line 2282270
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2282271
    iput-object p1, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->e:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2282272
    iget-object v0, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a:Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 2282273
    iget-object v1, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v1

    .line 2282274
    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setPrivacyOption(LX/1oT;)V

    .line 2282275
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 2282276
    invoke-direct {p0, v0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2282277
    iget-object v0, p0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->a:Lcom/facebook/privacy/ui/PrivacyOptionView;

    new-instance v1, LX/FmE;

    invoke-direct {v1, p0}, LX/FmE;-><init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2282278
    return-void
.end method
