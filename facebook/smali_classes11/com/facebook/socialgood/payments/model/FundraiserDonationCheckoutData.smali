.class public Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2282354
    new-instance v0, LX/FmN;

    invoke-direct {v0}, LX/FmN;-><init>()V

    sput-object v0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FmO;)V
    .locals 1

    .prologue
    .line 2282355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282356
    iget-object v0, p1, LX/FmO;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2282357
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2282358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282359
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 2282360
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2282361
    :goto_0
    return-void

    .line 2282362
    :cond_0
    sget-object v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;)LX/FmO;
    .locals 2

    .prologue
    .line 2282363
    new-instance v0, LX/FmO;

    invoke-direct {v0, p0}, LX/FmO;-><init>(Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2282364
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2282365
    if-ne p0, p1, :cond_1

    .line 2282366
    :cond_0
    :goto_0
    return v0

    .line 2282367
    :cond_1
    instance-of v2, p1, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    if-nez v2, :cond_2

    move v0, v1

    .line 2282368
    goto :goto_0

    .line 2282369
    :cond_2
    check-cast p1, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    .line 2282370
    iget-object v2, p0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, p1, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2282371
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2282372
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2282373
    iget-object v0, p0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_0

    .line 2282374
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2282375
    :goto_0
    return-void

    .line 2282376
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2282377
    iget-object v0, p0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/privacy/model/SelectablePrivacyData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
