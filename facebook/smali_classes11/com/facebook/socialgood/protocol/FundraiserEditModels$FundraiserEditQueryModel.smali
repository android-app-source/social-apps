.class public final Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x9ee25a8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2285766
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2285767
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2285768
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2285769
    return-void
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285770
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2285771
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2285772
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 2285773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2285774
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->q()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2285775
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2285776
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2285777
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x50c277df

    invoke-static {v4, v3, v5}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserEditModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2285778
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2285779
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2285780
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2285781
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2285782
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2285783
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2285784
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2285785
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->h:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2285786
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2285787
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2285788
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2285789
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2285790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2285791
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2285792
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2285793
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2285794
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    .line 2285795
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2285796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    .line 2285797
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    .line 2285798
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2285799
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x50c277df

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserEditModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2285800
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2285801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    .line 2285802
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->i:I

    move-object v1, v0

    .line 2285803
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2285804
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2285805
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2285806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    .line 2285807
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2285808
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2285809
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2285810
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2285811
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2285812
    new-instance v0, LX/FnO;

    invoke-direct {v0, p1}, LX/FnO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285813
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2285762
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2285763
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->h:J

    .line 2285764
    const/4 v0, 0x4

    const v1, -0x50c277df

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->i:I

    .line 2285765
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2285814
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2285815
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2285742
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2285743
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;-><init>()V

    .line 2285744
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2285745
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2285746
    const v0, 0x7ff6f35d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2285747
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285748
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    .line 2285749
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285750
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->g:Ljava/lang/String;

    .line 2285751
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 2285752
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2285753
    iget-wide v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->h:J

    return-wide v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGoalAmount"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285754
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2285755
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285756
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2285757
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285758
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->k:Ljava/lang/String;

    .line 2285759
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285760
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->l:Ljava/lang/String;

    .line 2285761
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->l:Ljava/lang/String;

    return-object v0
.end method
