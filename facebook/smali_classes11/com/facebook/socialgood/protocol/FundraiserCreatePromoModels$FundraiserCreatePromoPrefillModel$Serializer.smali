.class public final Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2284564
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2284565
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2284566
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2284567
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2284568
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2284569
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2284570
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2284571
    if-eqz v2, :cond_0

    .line 2284572
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284573
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2284574
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2284575
    if-eqz v2, :cond_1

    .line 2284576
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284577
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284578
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2284579
    if-eqz v2, :cond_2

    .line 2284580
    const-string v3, "prefill_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284581
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284582
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2284583
    cmp-long v4, v2, v4

    if-eqz v4, :cond_3

    .line 2284584
    const-string v4, "prefill_end_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284585
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2284586
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2284587
    if-eqz v2, :cond_4

    .line 2284588
    const-string v3, "prefill_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284589
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284590
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2284591
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2284592
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;LX/0nX;LX/0my;)V

    return-void
.end method
