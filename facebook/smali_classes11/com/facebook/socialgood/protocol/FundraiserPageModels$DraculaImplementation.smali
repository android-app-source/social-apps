.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2286084
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2286085
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2286082
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2286083
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2286045
    if-nez p1, :cond_0

    .line 2286046
    :goto_0
    return v1

    .line 2286047
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2286048
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2286049
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2286050
    const v2, 0x408055c2

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2286051
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2286052
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2286053
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2286054
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 2286055
    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 2286056
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2286057
    :sswitch_1
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2286058
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2286059
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2286060
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2286061
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2286062
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2286063
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2286064
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2286065
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2286066
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2286067
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2286068
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2286069
    const v2, -0x70377a68

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2286070
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2286071
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2286072
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2286073
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 2286074
    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 2286075
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2286076
    :sswitch_4
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2286077
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2286078
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2286079
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2286080
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2286081
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x70377a68 -> :sswitch_4
        -0x1a58da36 -> :sswitch_0
        0x2d3cacc1 -> :sswitch_2
        0x361e8a0d -> :sswitch_3
        0x408055c2 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2286044
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2286039
    if-eqz p0, :cond_0

    .line 2286040
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2286041
    if-eq v0, p0, :cond_0

    .line 2286042
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2286043
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2286028
    sparse-switch p2, :sswitch_data_0

    .line 2286029
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2286030
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2286031
    const v1, 0x408055c2

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2286032
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2286033
    invoke-static {v0, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2286034
    :goto_0
    :sswitch_1
    return-void

    .line 2286035
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2286036
    const v1, -0x70377a68

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2286037
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2286038
    invoke-static {v0, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x70377a68 -> :sswitch_1
        -0x1a58da36 -> :sswitch_0
        0x2d3cacc1 -> :sswitch_1
        0x361e8a0d -> :sswitch_2
        0x408055c2 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2286022
    if-eqz p1, :cond_0

    .line 2286023
    invoke-static {p0, p1, p2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v1

    .line 2286024
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    .line 2286025
    if-eq v0, v1, :cond_0

    .line 2286026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2286027
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2286021
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2286086
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2286087
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2286016
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2286017
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2286018
    :cond_0
    iput-object p1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a:LX/15i;

    .line 2286019
    iput p2, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->b:I

    .line 2286020
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2286015
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2286014
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2286011
    iget v0, p0, LX/1vt;->c:I

    .line 2286012
    move v0, v0

    .line 2286013
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2286008
    iget v0, p0, LX/1vt;->c:I

    .line 2286009
    move v0, v0

    .line 2286010
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2286005
    iget v0, p0, LX/1vt;->b:I

    .line 2286006
    move v0, v0

    .line 2286007
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286002
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2286003
    move-object v0, v0

    .line 2286004
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2285993
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2285994
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2285995
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2285996
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2285997
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2285998
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2285999
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2286000
    invoke-static {v3, v9, v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2286001
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2285990
    iget v0, p0, LX/1vt;->c:I

    .line 2285991
    move v0, v0

    .line 2285992
    return v0
.end method
