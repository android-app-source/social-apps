.class public final Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2282777
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2282778
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2282746
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2282747
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2282748
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2282749
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2282750
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2282751
    if-eqz v2, :cond_0

    .line 2282752
    const-string p0, "beneficiary_selector_prompt"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2282753
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2282754
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2282755
    if-eqz v2, :cond_1

    .line 2282756
    const-string p0, "friend_beneficiary"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2282757
    invoke-static {v1, v2, p1, p2}, LX/FmV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2282758
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2282759
    if-eqz v2, :cond_2

    .line 2282760
    const-string p0, "hearts_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2282761
    invoke-static {v1, v2, p1}, LX/FmY;->a(LX/15i;ILX/0nX;)V

    .line 2282762
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2282763
    if-eqz v2, :cond_3

    .line 2282764
    const-string p0, "nonprofit_beneficiary"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2282765
    invoke-static {v1, v2, p1, p2}, LX/FmV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2282766
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2282767
    if-eqz v2, :cond_4

    .line 2282768
    const-string p0, "other_beneficiary_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2282769
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2282770
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2282771
    if-eqz v2, :cond_5

    .line 2282772
    const-string p0, "viewer_beneficiary"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2282773
    invoke-static {v1, v2, p1, p2}, LX/FmV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2282774
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2282775
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2282776
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
