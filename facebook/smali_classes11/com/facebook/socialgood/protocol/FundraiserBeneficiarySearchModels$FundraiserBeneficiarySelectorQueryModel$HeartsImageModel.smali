.class public final Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282745
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282734
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2282735
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2282736
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2282725
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2282726
    iget v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2282737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282738
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2282739
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2282740
    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 2282741
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2282742
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 2282743
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282744
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2282727
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282729
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2282730
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2282731
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->e:I

    .line 2282732
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->g:I

    .line 2282733
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2282718
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;-><init>()V

    .line 2282719
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2282720
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2282717
    const v0, 0x31551a72

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2282716
    const v0, 0x437b93b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282721
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->f:Ljava/lang/String;

    .line 2282722
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 2282723
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2282724
    iget v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->g:I

    return v0
.end method
