.class public final Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2285232
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2285233
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2285234
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2285235
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2285236
    const/4 v2, 0x0

    .line 2285237
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2285238
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2285239
    :goto_0
    move v1, v2

    .line 2285240
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2285241
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2285242
    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;-><init>()V

    .line 2285243
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2285244
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2285245
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2285246
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2285247
    :cond_0
    return-object v1

    .line 2285248
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2285249
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_4

    .line 2285250
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2285251
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2285252
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v4, :cond_2

    .line 2285253
    const-string p0, "client_mutation_id"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2285254
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2285255
    :cond_3
    const-string p0, "fundraiser"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2285256
    invoke-static {p1, v0}, LX/FnF;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 2285257
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2285258
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2285259
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2285260
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1
.end method
