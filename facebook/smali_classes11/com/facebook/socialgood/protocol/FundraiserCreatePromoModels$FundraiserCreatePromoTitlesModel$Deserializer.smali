.class public final Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2284637
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2284638
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2284639
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2284640
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2284641
    const/4 v2, 0x0

    .line 2284642
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 2284643
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2284644
    :goto_0
    move v1, v2

    .line 2284645
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2284646
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2284647
    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;-><init>()V

    .line 2284648
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2284649
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2284650
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2284651
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2284652
    :cond_0
    return-object v1

    .line 2284653
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2284654
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 2284655
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2284656
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2284657
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_2

    if-eqz v7, :cond_2

    .line 2284658
    const-string v8, "__type__"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "__typename"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2284659
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 2284660
    :cond_4
    const-string v8, "charity_category_wrapper"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2284661
    const/4 v7, 0x0

    .line 2284662
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_e

    .line 2284663
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2284664
    :goto_2
    move v5, v7

    .line 2284665
    goto :goto_1

    .line 2284666
    :cond_5
    const-string v8, "dialog_subtitle"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2284667
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2284668
    :cond_6
    const-string v8, "dialog_title"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2284669
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2284670
    :cond_7
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2284671
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2284672
    :cond_8
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2284673
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2284674
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2284675
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2284676
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2284677
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2284678
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1

    .line 2284679
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2284680
    :cond_b
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_d

    .line 2284681
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2284682
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2284683
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v9, :cond_b

    .line 2284684
    const-string p0, "category_translated_name"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 2284685
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 2284686
    :cond_c
    const-string p0, "category_value"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 2284687
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_3

    .line 2284688
    :cond_d
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2284689
    invoke-virtual {v0, v7, v8}, LX/186;->b(II)V

    .line 2284690
    const/4 v7, 0x1

    invoke-virtual {v0, v7, v5}, LX/186;->b(II)V

    .line 2284691
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_e
    move v5, v7

    move v8, v7

    goto :goto_3
.end method
