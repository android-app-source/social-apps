.class public final Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x867fcbc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282838
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282837
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2282835
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2282836
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2282819
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282820
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2282821
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2282822
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2282823
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2282824
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2282825
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2282826
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2282827
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2282828
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2282829
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2282830
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2282831
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2282832
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2282833
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282834
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2282796
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282797
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2282798
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282799
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2282800
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    .line 2282801
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282802
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2282803
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    .line 2282804
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2282805
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    .line 2282806
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    .line 2282807
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2282808
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282809
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2282810
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    .line 2282811
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282812
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2282813
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282814
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2282815
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    .line 2282816
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282817
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282818
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282794
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->e:Ljava/lang/String;

    .line 2282795
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2282791
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;-><init>()V

    .line 2282792
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2282793
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2282779
    const v0, -0x7c4fc25b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2282790
    const v0, -0x52706947

    return v0
.end method

.method public final j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282788
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282789
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->f:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282786
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    .line 2282787
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282784
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282785
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282782
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->i:Ljava/lang/String;

    .line 2282783
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282780
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282781
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    return-object v0
.end method
