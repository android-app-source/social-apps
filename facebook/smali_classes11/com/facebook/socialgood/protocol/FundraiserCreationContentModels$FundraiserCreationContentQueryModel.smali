.class public final Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2a883698
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2285138
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2285137
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2285135
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2285136
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2285114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2285115
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2285116
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2285117
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2285118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2285119
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2285127
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2285128
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2285129
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2285130
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2285131
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2285132
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2285133
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2285134
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2285125
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2285126
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2285122
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;-><init>()V

    .line 2285123
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2285124
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2285121
    const v0, -0x6ded2c03

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2285120
    const v0, 0x5d9b23ef

    return v0
.end method
