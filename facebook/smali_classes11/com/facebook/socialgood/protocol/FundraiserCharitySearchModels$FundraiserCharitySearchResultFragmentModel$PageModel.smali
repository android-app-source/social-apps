.class public final Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x576dd3de
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2283664
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2283663
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2283661
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2283662
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2283655
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2283656
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2283657
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2283658
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2283659
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2283660
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2283652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2283653
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2283654
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2283651
    new-instance v0, LX/Fmg;

    invoke-direct {v0, p1}, LX/Fmg;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2283647
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2283648
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->e:Z

    .line 2283649
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->f:Z

    .line 2283650
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2283645
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2283646
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2283644
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2283642
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2283643
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2283635
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;-><init>()V

    .line 2283636
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2283637
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2283641
    const v0, -0xbc1a9d2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2283640
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2283638
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2283639
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->f:Z

    return v0
.end method
