.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2287379
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2287380
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2287296
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2287297
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2287298
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2287299
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2287300
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2287301
    if-eqz v2, :cond_0

    .line 2287302
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287303
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2287304
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2287305
    if-eqz v2, :cond_1

    .line 2287306
    const-string p0, "campaign_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287307
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2287308
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287309
    if-eqz v2, :cond_2

    .line 2287310
    const-string p0, "can_invite_to_campaign"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287311
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287312
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287313
    if-eqz v2, :cond_3

    .line 2287314
    const-string p0, "can_viewer_delete"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287315
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287316
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287317
    if-eqz v2, :cond_4

    .line 2287318
    const-string p0, "can_viewer_edit"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287319
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287320
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287321
    if-eqz v2, :cond_5

    .line 2287322
    const-string p0, "can_viewer_post"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287323
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287324
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287325
    if-eqz v2, :cond_6

    .line 2287326
    const-string p0, "can_viewer_report"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287327
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287328
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287329
    if-eqz v2, :cond_7

    .line 2287330
    const-string p0, "charity_interface"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287331
    invoke-static {v1, v2, p1, p2}, LX/Fnv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287332
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287333
    if-eqz v2, :cond_8

    .line 2287334
    const-string p0, "focused_cover_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287335
    invoke-static {v1, v2, p1, p2}, LX/Fnx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287336
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287337
    if-eqz v2, :cond_9

    .line 2287338
    const-string p0, "fundraiser_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287339
    invoke-static {v1, v2, p1, p2}, LX/Fns;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287340
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287341
    if-eqz v2, :cond_a

    .line 2287342
    const-string p0, "fundraiser_page_subtitle"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287343
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287344
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2287345
    if-eqz v2, :cond_b

    .line 2287346
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287347
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2287348
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287349
    if-eqz v2, :cond_c

    .line 2287350
    const-string p0, "invited_you_to_donate_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287351
    invoke-static {v1, v2, p1}, LX/Fnt;->a(LX/15i;ILX/0nX;)V

    .line 2287352
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287353
    if-eqz v2, :cond_d

    .line 2287354
    const-string p0, "is_viewer_following"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287355
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287356
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287357
    if-eqz v2, :cond_e

    .line 2287358
    const-string p0, "logo_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287359
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2287360
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2287361
    if-eqz v2, :cond_f

    .line 2287362
    const-string p0, "mobile_donate_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287363
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2287364
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287365
    if-eqz v2, :cond_10

    .line 2287366
    const-string p0, "owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287367
    invoke-static {v1, v2, p1, p2}, LX/Fnq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287368
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287369
    if-eqz v2, :cond_11

    .line 2287370
    const-string p0, "privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287371
    invoke-static {v1, v2, p1, p2}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287372
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2287373
    if-eqz v2, :cond_12

    .line 2287374
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287375
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2287376
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2287377
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2287378
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
