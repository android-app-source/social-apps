.class public final Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2284258
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2284259
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2284257
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2284239
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2284240
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2284241
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2284242
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2284243
    if-eqz v2, :cond_0

    .line 2284244
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284245
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2284246
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2284247
    if-eqz v2, :cond_1

    .line 2284248
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284249
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284250
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2284251
    if-eqz v2, :cond_2

    .line 2284252
    const-string p0, "other_charities"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284253
    invoke-static {v1, v2, p1, p2}, LX/Fn1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2284254
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2284255
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2284256
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;LX/0nX;LX/0my;)V

    return-void
.end method
