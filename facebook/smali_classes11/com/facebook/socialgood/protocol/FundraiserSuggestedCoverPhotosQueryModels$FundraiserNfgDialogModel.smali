.class public final Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x69482a49
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2289045
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2289044
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2289007
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2289008
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2289036
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2289037
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2289038
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x51029a0e

    invoke-static {v2, v1, v3}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2289039
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2289040
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2289041
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2289042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2289043
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2289021
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2289022
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2289023
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2289024
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2289025
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;

    .line 2289026
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2289027
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2289028
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x51029a0e

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2289029
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2289030
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;

    .line 2289031
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->f:I

    move-object v1, v0

    .line 2289032
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2289033
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2289034
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2289035
    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2289019
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2289020
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2289016
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2289017
    const/4 v0, 0x1

    const v1, 0x51029a0e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->f:I

    .line 2289018
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2289013
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;-><init>()V

    .line 2289014
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2289015
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2289012
    const v0, -0x6cb3c2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2289011
    const v0, 0x7fba55ef

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNfgLogo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2289009
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2289010
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
