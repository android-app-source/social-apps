.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4e0d3453
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2287280
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2287279
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2287277
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2287278
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287274
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2287275
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2287276
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFocusedCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287272
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287273
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287227
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->l:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->l:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2287228
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->l:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2287257
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2287258
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2287259
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x361e8a0d

    invoke-static {v2, v1, v3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2287260
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->k()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2287261
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2287262
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2287263
    const/4 v0, 0x1

    iget-boolean v3, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->f:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2287264
    const/4 v0, 0x2

    iget-boolean v3, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->g:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2287265
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->h:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2287266
    const/4 v0, 0x4

    iget-boolean v3, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->i:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2287267
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2287268
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2287269
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2287270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2287271
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2287242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2287243
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2287244
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x361e8a0d

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2287245
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2287246
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;

    .line 2287247
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->j:I

    move-object v1, v0

    .line 2287248
    :cond_0
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->k()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2287249
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->k()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2287250
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->k()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2287251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;

    .line 2287252
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->l:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2287253
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2287254
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2287255
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2287256
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2287234
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2287235
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->f:Z

    .line 2287236
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->g:Z

    .line 2287237
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->h:Z

    .line 2287238
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->i:Z

    .line 2287239
    const/4 v0, 0x5

    const v1, 0x361e8a0d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->j:I

    .line 2287240
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;->k:Z

    .line 2287241
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2287231
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;-><init>()V

    .line 2287232
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2287233
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2287230
    const v0, 0x3e642dde

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2287229
    const v0, 0xb2664ac

    return v0
.end method
