.class public final Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x394546bc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2288713
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2288648
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2288711
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2288712
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2288696
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2288697
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2288698
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2288699
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x2eb47749

    invoke-static {v3, v2, v4}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2288700
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2288701
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2288702
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2288703
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2288704
    const/4 v0, 0x1

    iget-boolean v5, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->f:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 2288705
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2288706
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2288707
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2288708
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2288709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2288710
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2288676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2288677
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2288678
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    .line 2288679
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2288680
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    .line 2288681
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    .line 2288682
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2288683
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2eb47749

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2288684
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2288685
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    .line 2288686
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->h:I

    move-object v1, v0

    .line 2288687
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2288688
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    .line 2288689
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2288690
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    .line 2288691
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->i:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    .line 2288692
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2288693
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2288694
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2288695
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2288675
    new-instance v0, LX/Fo4;

    invoke-direct {v0, p1}, LX/Fo4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288674
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2288670
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2288671
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->f:Z

    .line 2288672
    const/4 v0, 0x3

    const v1, -0x2eb47749

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->h:I

    .line 2288673
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2288668
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2288669
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2288667
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2288664
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;-><init>()V

    .line 2288665
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2288666
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2288663
    const v0, 0x6291836d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2288662
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288659
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2288660
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2288661
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2288657
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2288658
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->f:Z

    return v0
.end method

.method public final l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288655
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    .line 2288656
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFullWidthPostDonationImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288653
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2288654
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288651
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->i:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->i:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    .line 2288652
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->i:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288649
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->j:Ljava/lang/String;

    .line 2288650
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->j:Ljava/lang/String;

    return-object v0
.end method
