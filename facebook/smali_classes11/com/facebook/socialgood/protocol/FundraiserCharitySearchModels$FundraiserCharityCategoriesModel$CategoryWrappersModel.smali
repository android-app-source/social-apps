.class public final Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x126bdde2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2283334
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2283335
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2283336
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2283337
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2283338
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2283339
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2283340
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->j()Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2283341
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2283342
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2283343
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2283344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2283345
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2283346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2283347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2283348
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283349
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->e:Ljava/lang/String;

    .line 2283350
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2283351
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;-><init>()V

    .line 2283352
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2283353
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2283354
    const v0, 0x4cbfd96f    # 1.00584312E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2283355
    const v0, 0x6e6cea30

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283356
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->f:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->f:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2283357
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->f:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    return-object v0
.end method
