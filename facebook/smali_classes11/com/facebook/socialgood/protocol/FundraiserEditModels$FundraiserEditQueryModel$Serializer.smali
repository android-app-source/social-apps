.class public final Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2285691
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2285692
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2285693
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2285694
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2285695
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2285696
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2285697
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2285698
    if-eqz v2, :cond_0

    .line 2285699
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285700
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2285701
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2285702
    if-eqz v2, :cond_1

    .line 2285703
    const-string v3, "charity_interface"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285704
    invoke-static {v1, v2, p1}, LX/FnR;->a(LX/15i;ILX/0nX;)V

    .line 2285705
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2285706
    if-eqz v2, :cond_2

    .line 2285707
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285708
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2285709
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2285710
    cmp-long v4, v2, v4

    if-eqz v4, :cond_3

    .line 2285711
    const-string v4, "end_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285712
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2285713
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2285714
    if-eqz v2, :cond_6

    .line 2285715
    const-string v3, "goal_amount"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285716
    const/4 v3, 0x0

    .line 2285717
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2285718
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 2285719
    if-eqz v3, :cond_4

    .line 2285720
    const-string v4, "amount_in_hundredths"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285721
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 2285722
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2285723
    if-eqz v3, :cond_5

    .line 2285724
    const-string v4, "currency"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285725
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2285726
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2285727
    :cond_6
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2285728
    if-eqz v2, :cond_7

    .line 2285729
    const-string v3, "header_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285730
    invoke-static {v1, v2, p1, p2}, LX/Fnl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2285731
    :cond_7
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2285732
    if-eqz v2, :cond_8

    .line 2285733
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285734
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2285735
    :cond_8
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2285736
    if-eqz v2, :cond_9

    .line 2285737
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285738
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2285739
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2285740
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2285741
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
