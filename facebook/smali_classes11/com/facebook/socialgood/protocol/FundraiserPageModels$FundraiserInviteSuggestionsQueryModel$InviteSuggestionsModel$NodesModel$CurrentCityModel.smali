.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x104a4aa6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2286402
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2286401
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2286399
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2286400
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2286393
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2286394
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2286395
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2286396
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2286397
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2286398
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2286381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2286382
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2286383
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2286392
    new-instance v0, LX/Fnd;

    invoke-direct {v0, p1}, LX/Fnd;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286403
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;->e:Ljava/lang/String;

    .line 2286404
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2286390
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2286391
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2286389
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2286386
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;-><init>()V

    .line 2286387
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2286388
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2286385
    const v0, -0x25042c93

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2286384
    const v0, 0x25d6af

    return v0
.end method
