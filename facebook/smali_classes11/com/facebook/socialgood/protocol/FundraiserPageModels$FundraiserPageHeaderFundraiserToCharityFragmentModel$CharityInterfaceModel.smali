.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3a97533b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2287067
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2287068
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2287072
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2287073
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287069
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2287070
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2287071
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287057
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->f:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->f:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    .line 2287058
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->f:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2287059
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2287060
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2287061
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2287062
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2287063
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2287064
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2287065
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2287066
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2287049
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2287050
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2287051
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    .line 2287052
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2287053
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    .line 2287054
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->f:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    .line 2287055
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2287056
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287048
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2287045
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;-><init>()V

    .line 2287046
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2287047
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2287044
    const v0, -0x5f63aedc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2287043
    const v0, -0x70ba2a88

    return v0
.end method
