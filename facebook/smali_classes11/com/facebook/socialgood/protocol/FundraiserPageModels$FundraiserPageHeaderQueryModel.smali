.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/Fnb;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x75261a4c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2287414
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2287415
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2287416
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2287417
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287418
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w:Ljava/lang/String;

    .line 2287419
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 2287420
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2287421
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2287422
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2287423
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2287424
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x361e8a0d

    invoke-static {v4, v3, v5}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2287425
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2287426
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2287427
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2287428
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, 0x2d3cacc1

    invoke-static {v8, v7, v9}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2287429
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2287430
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->x()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2287431
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v10

    invoke-static {p1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2287432
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->z()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v11

    invoke-static {p1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2287433
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->A()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p1, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2287434
    const/16 v13, 0x13

    invoke-virtual {p1, v13}, LX/186;->c(I)V

    .line 2287435
    const/4 v13, 0x0

    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 2287436
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2287437
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2287438
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2287439
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2287440
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2287441
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2287442
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2287443
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2287444
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2287445
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2287446
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2287447
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2287448
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2287449
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2287450
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2287451
    const/16 v0, 0x10

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2287452
    const/16 v0, 0x11

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2287453
    const/16 v0, 0x12

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 2287454
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2287455
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2287456
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2287457
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2287458
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    .line 2287459
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2287460
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287461
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->l:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    .line 2287462
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2287463
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x361e8a0d

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2287464
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2287465
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287466
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->m:I

    move-object v1, v0

    .line 2287467
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2287468
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    .line 2287469
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2287470
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287471
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->n:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    .line 2287472
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2287473
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2287474
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2287475
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287476
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2287477
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2287478
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2d3cacc1

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPageModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2287479
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2287480
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287481
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q:I

    move-object v1, v0

    .line 2287482
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2287483
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2287484
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2287485
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287486
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2287487
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2287488
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    .line 2287489
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2287490
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287491
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    .line 2287492
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->z()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2287493
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->z()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2287494
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->z()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 2287495
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2287496
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->v:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2287497
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2287498
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 2287499
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2287500
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_8
    move-object p0, v1

    .line 2287501
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2287502
    new-instance v0, LX/Fni;

    invoke-direct {v0, p1}, LX/Fni;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287523
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2287503
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2287504
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->g:Z

    .line 2287505
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->h:Z

    .line 2287506
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->i:Z

    .line 2287507
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->j:Z

    .line 2287508
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k:Z

    .line 2287509
    const/16 v0, 0x8

    const v1, 0x361e8a0d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->m:I

    .line 2287510
    const/16 v0, 0xc

    const v1, 0x2d3cacc1

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q:I

    .line 2287511
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r:Z

    .line 2287512
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2287513
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2287514
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2287515
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2287516
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;-><init>()V

    .line 2287517
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2287518
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2287519
    const v0, -0x3285b9d0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2287520
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287411
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2287412
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2287413
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287521
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->f:Ljava/lang/String;

    .line 2287522
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2287381
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287382
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->g:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2287385
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287386
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->h:Z

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 2287387
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287388
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->i:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2287383
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287384
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k:Z

    return v0
.end method

.method public final p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287389
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->l:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->l:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    .line 2287390
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->l:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    return-object v0
.end method

.method public final q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFocusedCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287391
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287392
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFundraiserPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287393
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->n:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->n:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    .line 2287394
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->n:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287395
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2287396
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287397
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p:Ljava/lang/String;

    .line 2287398
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final u()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInvitedYouToDonateText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287399
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287400
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 2287401
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2287402
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r:Z

    return v0
.end method

.method public final w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287403
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2287404
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287405
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t:Ljava/lang/String;

    .line 2287406
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287407
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    .line 2287408
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    return-object v0
.end method

.method public final z()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2287409
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->v:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->v:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2287410
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->v:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    return-object v0
.end method
