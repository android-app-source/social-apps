.class public final Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2288228
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2288229
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2288230
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2288231
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2288232
    if-nez p1, :cond_0

    .line 2288233
    :goto_0
    return v0

    .line 2288234
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2288235
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2288236
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2288237
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2288238
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2288239
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 2288240
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2288241
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2288242
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2288243
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 2288244
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2eb47749
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2288249
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2288245
    packed-switch p0, :pswitch_data_0

    .line 2288246
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2288247
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x2eb47749
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2288248
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2288221
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->b(I)V

    .line 2288222
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2288223
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2288224
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2288225
    :cond_0
    iput-object p1, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a:LX/15i;

    .line 2288226
    iput p2, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->b:I

    .line 2288227
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2288220
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2288219
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2288216
    iget v0, p0, LX/1vt;->c:I

    .line 2288217
    move v0, v0

    .line 2288218
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2288213
    iget v0, p0, LX/1vt;->c:I

    .line 2288214
    move v0, v0

    .line 2288215
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2288210
    iget v0, p0, LX/1vt;->b:I

    .line 2288211
    move v0, v0

    .line 2288212
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288207
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2288208
    move-object v0, v0

    .line 2288209
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2288198
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2288199
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2288200
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2288201
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2288202
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2288203
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2288204
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2288205
    invoke-static {v3, v9, v2}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2288206
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2288195
    iget v0, p0, LX/1vt;->c:I

    .line 2288196
    move v0, v0

    .line 2288197
    return v0
.end method
