.class public final Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2288617
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2288618
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2288616
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2288619
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2288620
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2288621
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2288622
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2288623
    if-eqz v2, :cond_0

    .line 2288624
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2288625
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2288626
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2288627
    if-eqz v2, :cond_1

    .line 2288628
    const-string p0, "can_invite_to_campaign"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2288629
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2288630
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2288631
    if-eqz v2, :cond_2

    .line 2288632
    const-string p0, "charity_interface"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2288633
    invoke-static {v1, v2, p1, p2}, LX/Fo8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2288634
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2288635
    if-eqz v2, :cond_3

    .line 2288636
    const-string p0, "full_width_post_donation_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2288637
    invoke-static {v1, v2, p1}, LX/Fo9;->a(LX/15i;ILX/0nX;)V

    .line 2288638
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2288639
    if-eqz v2, :cond_4

    .line 2288640
    const-string p0, "fundraiser_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2288641
    invoke-static {v1, v2, p1}, LX/Fo6;->a(LX/15i;ILX/0nX;)V

    .line 2288642
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2288643
    if-eqz v2, :cond_5

    .line 2288644
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2288645
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2288646
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2288647
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2288615
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
