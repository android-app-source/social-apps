.class public final Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2284094
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2284095
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2284092
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2284093
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2284058
    if-nez p1, :cond_0

    move v0, v1

    .line 2284059
    :goto_0
    return v0

    .line 2284060
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2284061
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2284062
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2284063
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2284064
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 2284065
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 2284066
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2284067
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2284068
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2284069
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2284070
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 2284071
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 2284072
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2284073
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2284074
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2284075
    const v2, -0x47db548e

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2284076
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2284077
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2284078
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2284079
    :sswitch_2
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2284080
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2284081
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2284082
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2284083
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2284084
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2284085
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2284086
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v2

    .line 2284087
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2284088
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2284089
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2284090
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2284091
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x47db548e -> :sswitch_2
        -0x1c75ea54 -> :sswitch_0
        0x3c86e7d9 -> :sswitch_1
        0x59171c9d -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2284057
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2284028
    if-eqz p0, :cond_0

    .line 2284029
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2284030
    if-eq v0, p0, :cond_0

    .line 2284031
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2284032
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2284050
    sparse-switch p2, :sswitch_data_0

    .line 2284051
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2284052
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2284053
    const v1, -0x47db548e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2284054
    :goto_0
    :sswitch_1
    return-void

    .line 2284055
    :sswitch_2
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2284056
    invoke-static {v0, p3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x47db548e -> :sswitch_2
        -0x1c75ea54 -> :sswitch_1
        0x3c86e7d9 -> :sswitch_0
        0x59171c9d -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2284044
    if-eqz p1, :cond_0

    .line 2284045
    invoke-static {p0, p1, p2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;

    move-result-object v1

    .line 2284046
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;

    .line 2284047
    if-eq v0, v1, :cond_0

    .line 2284048
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2284049
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2284043
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2284041
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2284042
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2284096
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2284097
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2284098
    :cond_0
    iput-object p1, p0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a:LX/15i;

    .line 2284099
    iput p2, p0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->b:I

    .line 2284100
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2284040
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2284039
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2284036
    iget v0, p0, LX/1vt;->c:I

    .line 2284037
    move v0, v0

    .line 2284038
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2284033
    iget v0, p0, LX/1vt;->c:I

    .line 2284034
    move v0, v0

    .line 2284035
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2284025
    iget v0, p0, LX/1vt;->b:I

    .line 2284026
    move v0, v0

    .line 2284027
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2284022
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2284023
    move-object v0, v0

    .line 2284024
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2284013
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2284014
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2284015
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2284016
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2284017
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2284018
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2284019
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2284020
    invoke-static {v3, v9, v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2284021
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2284010
    iget v0, p0, LX/1vt;->c:I

    .line 2284011
    move v0, v0

    .line 2284012
    return v0
.end method
