.class public final Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5e4489bc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282986
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282985
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2282983
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2282984
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2282971
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282972
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2282973
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2282974
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2282975
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2282976
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2282977
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2282978
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2282979
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2282980
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2282981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282982
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2282958
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282959
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2282960
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    .line 2282961
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2282962
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;

    .line 2282963
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    .line 2282964
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2282965
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    .line 2282966
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2282967
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;

    .line 2282968
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    .line 2282969
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282970
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2282957
    new-instance v0, LX/FmS;

    invoke-direct {v0, p1}, LX/FmS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282956
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2282954
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2282955
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2282987
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2282951
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;-><init>()V

    .line 2282952
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2282953
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2282950
    const v0, 0x11ad830f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2282949
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282947
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    .line 2282948
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282945
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->f:Ljava/lang/String;

    .line 2282946
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282943
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->g:Ljava/lang/String;

    .line 2282944
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282941
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    .line 2282942
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->h:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    return-object v0
.end method
