.class public final Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x335d2b8b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282475
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282488
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2282486
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2282487
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2282476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282477
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2282478
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2282479
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2282480
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2282481
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2282482
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2282483
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2282484
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282485
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2282467
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282468
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2282469
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    .line 2282470
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2282471
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    .line 2282472
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    .line 2282473
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282474
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282489
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    .line 2282490
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->e:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2282464
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;-><init>()V

    .line 2282465
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2282466
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2282458
    const v0, 0x20481118

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2282463
    const v0, -0x65d97e94

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282461
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->f:Ljava/lang/String;

    .line 2282462
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282459
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->g:Ljava/lang/String;

    .line 2282460
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method
