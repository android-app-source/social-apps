.class public final Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2288403
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2288390
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2288404
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2288405
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2288409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2288410
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2288411
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2288412
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2288413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2288414
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2288406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2288407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2288408
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2288401
    new-instance v0, LX/Fo3;

    invoke-direct {v0, p1}, LX/Fo3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288402
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2288399
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2288400
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2288398
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2288395
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;-><init>()V

    .line 2288396
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2288397
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2288394
    const v0, 0x7d8910f2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2288393
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288391
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    .line 2288392
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    return-object v0
.end method
