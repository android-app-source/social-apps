.class public final Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6e8f03b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2283693
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2283688
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2283694
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2283695
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2283696
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2283697
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2283698
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x18373e3e

    invoke-static {v2, v1, v3}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2283699
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2283700
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2283701
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2283702
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2283703
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2283704
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2283705
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2283706
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2283707
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2283708
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2283709
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2283710
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2283711
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2283712
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2283713
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2283714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2283715
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2283716
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x18373e3e

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2283717
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2283718
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2283719
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->f:I

    move-object v1, v0

    .line 2283720
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2283721
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    .line 2283722
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2283723
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2283724
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->j:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    .line 2283725
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2283726
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2283727
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2283728
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283729
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2283730
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2283731
    const/4 v0, 0x1

    const v1, -0x18373e3e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->f:I

    .line 2283732
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2283689
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;-><init>()V

    .line 2283690
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2283691
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2283692
    const v0, -0x664efbea

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2283672
    const v0, -0x70ba2a88

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283673
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2283674
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2283675
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCharityPickerSubtitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283676
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2283677
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283678
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->g:Ljava/lang/String;

    .line 2283679
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283680
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->h:Ljava/lang/String;

    .line 2283681
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283682
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->i:Ljava/lang/String;

    .line 2283683
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283684
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->j:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->j:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    .line 2283685
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->j:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2283686
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k:Ljava/lang/String;

    .line 2283687
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method
