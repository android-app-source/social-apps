.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2287186
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2287187
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2287188
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2287189
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2287190
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2287191
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2287192
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2287193
    if-eqz v2, :cond_0

    .line 2287194
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287195
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2287196
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287197
    if-eqz v2, :cond_1

    .line 2287198
    const-string p0, "can_viewer_delete"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287199
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287200
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287201
    if-eqz v2, :cond_2

    .line 2287202
    const-string p0, "can_viewer_edit"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287203
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287204
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287205
    if-eqz v2, :cond_3

    .line 2287206
    const-string p0, "can_viewer_post"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287207
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287208
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287209
    if-eqz v2, :cond_4

    .line 2287210
    const-string p0, "can_viewer_report"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287211
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287212
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287213
    if-eqz v2, :cond_5

    .line 2287214
    const-string p0, "focused_cover_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287215
    invoke-static {v1, v2, p1, p2}, LX/Fnx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287216
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2287217
    if-eqz v2, :cond_6

    .line 2287218
    const-string p0, "is_viewer_following"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287219
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2287220
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287221
    if-eqz v2, :cond_7

    .line 2287222
    const-string p0, "privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287223
    invoke-static {v1, v2, p1, p2}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287224
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2287225
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2287226
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserWithPresenceFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
