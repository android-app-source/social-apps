.class public final Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2288841
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2288842
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2288877
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2288878
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2288879
    if-nez p1, :cond_0

    .line 2288880
    :goto_0
    return v1

    .line 2288881
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2288882
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2288883
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2288884
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2288885
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2288886
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2288887
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2288888
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2288889
    const v2, -0x453ac4e

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2288890
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    .line 2288891
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2288892
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2288893
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 2288894
    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 2288895
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2288896
    :sswitch_2
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2288897
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2288898
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2288899
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2288900
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2288901
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2288902
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2288903
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2288904
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2288905
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2288906
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x52201e93 -> :sswitch_3
        -0x453ac4e -> :sswitch_2
        0x51029a0e -> :sswitch_0
        0x5b4e2271 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2288907
    if-nez p0, :cond_0

    move v0, v1

    .line 2288908
    :goto_0
    return v0

    .line 2288909
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2288910
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2288911
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2288912
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2288913
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2288914
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2288915
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2288916
    const/4 v7, 0x0

    .line 2288917
    const/4 v1, 0x0

    .line 2288918
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2288919
    invoke-static {v2, v3, v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2288920
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2288921
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2288922
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2288928
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2288923
    if-eqz p0, :cond_0

    .line 2288924
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2288925
    if-eq v0, p0, :cond_0

    .line 2288926
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2288927
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2288935
    sparse-switch p2, :sswitch_data_0

    .line 2288936
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2288937
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2288938
    const v1, -0x453ac4e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2288939
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    .line 2288940
    invoke-static {v0, p3}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2288941
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x52201e93 -> :sswitch_1
        -0x453ac4e -> :sswitch_1
        0x51029a0e -> :sswitch_1
        0x5b4e2271 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2288929
    if-eqz p1, :cond_0

    .line 2288930
    invoke-static {p0, p1, p2}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2288931
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    .line 2288932
    if-eq v0, v1, :cond_0

    .line 2288933
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2288934
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2288874
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2288875
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2288876
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2288869
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2288870
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2288871
    :cond_0
    iput-object p1, p0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2288872
    iput p2, p0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->b:I

    .line 2288873
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2288868
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2288867
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2288864
    iget v0, p0, LX/1vt;->c:I

    .line 2288865
    move v0, v0

    .line 2288866
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2288861
    iget v0, p0, LX/1vt;->c:I

    .line 2288862
    move v0, v0

    .line 2288863
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2288858
    iget v0, p0, LX/1vt;->b:I

    .line 2288859
    move v0, v0

    .line 2288860
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288855
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2288856
    move-object v0, v0

    .line 2288857
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2288846
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2288847
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2288848
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2288849
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2288850
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2288851
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2288852
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2288853
    invoke-static {v3, v9, v2}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2288854
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2288843
    iget v0, p0, LX/1vt;->c:I

    .line 2288844
    move v0, v0

    .line 2288845
    return v0
.end method
