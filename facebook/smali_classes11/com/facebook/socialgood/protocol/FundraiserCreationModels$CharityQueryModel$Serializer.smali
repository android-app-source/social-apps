.class public final Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2285178
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2285179
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2285177
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2285181
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2285182
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 2285183
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2285184
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 2285185
    if-eqz p0, :cond_0

    .line 2285186
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285187
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2285188
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2285189
    if-eqz p0, :cond_1

    .line 2285190
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285191
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2285192
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2285193
    if-eqz p0, :cond_2

    .line 2285194
    const-string p2, "name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2285195
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2285196
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2285197
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2285180
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
