.class public final Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2284694
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2284695
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2284732
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2284697
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2284698
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2284699
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2284700
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2284701
    if-eqz v2, :cond_0

    .line 2284702
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284703
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2284704
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2284705
    if-eqz v2, :cond_3

    .line 2284706
    const-string v3, "charity_category_wrapper"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284707
    const/4 p2, 0x1

    .line 2284708
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2284709
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2284710
    if-eqz v3, :cond_1

    .line 2284711
    const-string p0, "category_translated_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284712
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284713
    :cond_1
    invoke-virtual {v1, v2, p2}, LX/15i;->g(II)I

    move-result v3

    .line 2284714
    if-eqz v3, :cond_2

    .line 2284715
    const-string v3, "category_value"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284716
    invoke-virtual {v1, v2, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284717
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2284718
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2284719
    if-eqz v2, :cond_4

    .line 2284720
    const-string v3, "dialog_subtitle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284721
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284722
    :cond_4
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2284723
    if-eqz v2, :cond_5

    .line 2284724
    const-string v3, "dialog_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284725
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284726
    :cond_5
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2284727
    if-eqz v2, :cond_6

    .line 2284728
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284729
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2284730
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2284731
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2284696
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;LX/0nX;LX/0my;)V

    return-void
.end method
