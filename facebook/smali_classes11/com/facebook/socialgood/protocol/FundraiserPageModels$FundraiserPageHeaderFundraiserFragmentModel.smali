.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x14a6b9f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2286827
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2286828
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2286825
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2286826
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286822
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2286823
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2286824
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286820
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->f:Ljava/lang/String;

    .line 2286821
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286818
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2286819
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286816
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->i:Ljava/lang/String;

    .line 2286817
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286829
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2286830
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286814
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->k:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->k:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    .line 2286815
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->k:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2286797
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2286798
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2286799
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2286800
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2286801
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2286802
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2286803
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2286804
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2286805
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2286806
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2286807
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2286808
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2286809
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2286810
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2286811
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2286812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2286813
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2286779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2286780
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2286781
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2286782
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2286783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;

    .line 2286784
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2286785
    :cond_0
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2286786
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2286787
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2286788
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;

    .line 2286789
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2286790
    :cond_1
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2286791
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    .line 2286792
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2286793
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;

    .line 2286794
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->k:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    .line 2286795
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2286796
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2286778
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2286775
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2286776
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;->g:Z

    .line 2286777
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2286772
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;-><init>()V

    .line 2286773
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2286774
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2286770
    const v0, 0x5aa77a02

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2286771
    const v0, -0x5be312d5

    return v0
.end method
