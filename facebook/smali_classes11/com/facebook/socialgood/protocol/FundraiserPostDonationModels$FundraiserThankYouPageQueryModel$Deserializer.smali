.class public final Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2288571
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2288572
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2288573
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2288574
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2288575
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2288576
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 2288577
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2288578
    :goto_0
    move v1, v2

    .line 2288579
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2288580
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2288581
    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;-><init>()V

    .line 2288582
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2288583
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2288584
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2288585
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2288586
    :cond_0
    return-object v1

    .line 2288587
    :cond_1
    const-string p0, "can_invite_to_campaign"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2288588
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v8, v1

    move v1, v3

    .line 2288589
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_9

    .line 2288590
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2288591
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2288592
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 2288593
    const-string p0, "__type__"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2288594
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 2288595
    :cond_4
    const-string p0, "charity_interface"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2288596
    invoke-static {p1, v0}, LX/Fo8;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2288597
    :cond_5
    const-string p0, "full_width_post_donation_image"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2288598
    invoke-static {p1, v0}, LX/Fo9;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2288599
    :cond_6
    const-string p0, "fundraiser_page"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2288600
    invoke-static {p1, v0}, LX/Fo6;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2288601
    :cond_7
    const-string p0, "id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2288602
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2288603
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2288604
    :cond_9
    const/4 v10, 0x6

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2288605
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2288606
    if-eqz v1, :cond_a

    .line 2288607
    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 2288608
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2288609
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2288610
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2288611
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2288612
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
