.class public final Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x24602173
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2288570
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2288569
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2288567
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2288568
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288564
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2288565
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2288566
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288562
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    .line 2288563
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFullWidthPostDonationImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288560
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2288561
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288558
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->i:Ljava/lang/String;

    .line 2288559
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2288545
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2288546
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2288547
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2288548
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x2eb47749

    invoke-static {v3, v2, v4}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2288549
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2288550
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2288551
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2288552
    const/4 v0, 0x1

    iget-boolean v4, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->f:Z

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 2288553
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2288554
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2288555
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2288556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2288557
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2288530
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2288531
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2288532
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    .line 2288533
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->k()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2288534
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;

    .line 2288535
    iput-object v0, v1, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->g:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    .line 2288536
    :cond_0
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2288537
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2eb47749

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2288538
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2288539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;

    .line 2288540
    iput v3, v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->h:I

    move-object v1, v0

    .line 2288541
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2288542
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2288543
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2288544
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2288520
    invoke-direct {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2288526
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2288527
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->f:Z

    .line 2288528
    const/4 v0, 0x3

    const v1, -0x2eb47749

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;->h:I

    .line 2288529
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2288523
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel;-><init>()V

    .line 2288524
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2288525
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2288522
    const v0, 0x2de2ece0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2288521
    const v0, -0x7e2f964e

    return v0
.end method
