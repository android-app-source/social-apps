.class public final Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x778473f6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282572
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2282575
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2282573
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2282574
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2282563
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282564
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2282565
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2282566
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2282567
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2282568
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282569
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2282557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2282558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2282559
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282570
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->e:Ljava/lang/String;

    .line 2282571
    iget-object v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2282560
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2282561
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->f:Z

    .line 2282562
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2282554
    new-instance v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;-><init>()V

    .line 2282555
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2282556
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2282553
    const v0, -0x62fc065c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2282552
    const v0, 0x370fbffd

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2282550
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2282551
    iget-boolean v0, p0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->f:Z

    return v0
.end method
