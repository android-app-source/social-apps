.class public final Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2286735
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;

    new-instance v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2286736
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2286734
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2286737
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2286738
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2286739
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2286740
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2286741
    if-eqz v2, :cond_0

    .line 2286742
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2286743
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2286744
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2286745
    if-eqz v2, :cond_1

    .line 2286746
    const-string p0, "campaign_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2286747
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2286748
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2286749
    if-eqz v2, :cond_2

    .line 2286750
    const-string p0, "can_invite_to_campaign"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2286751
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2286752
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2286753
    if-eqz v2, :cond_3

    .line 2286754
    const-string p0, "fundraiser_page_subtitle"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2286755
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2286756
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2286757
    if-eqz v2, :cond_4

    .line 2286758
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2286759
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2286760
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2286761
    if-eqz v2, :cond_5

    .line 2286762
    const-string p0, "logo_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2286763
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2286764
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2286765
    if-eqz v2, :cond_6

    .line 2286766
    const-string p0, "owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2286767
    invoke-static {v1, v2, p1, p2}, LX/Fnq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2286768
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2286769
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2286733
    check-cast p1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$Serializer;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
