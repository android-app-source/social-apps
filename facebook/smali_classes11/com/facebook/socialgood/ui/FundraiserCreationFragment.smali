.class public Lcom/facebook/socialgood/ui/FundraiserCreationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/FkL;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->FUNDRAISER_CREATION_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field public D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

.field public E:Z

.field private F:Z

.field public b:LX/0zG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/FkM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/17Y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/BOa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/FpN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/FkP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/fig/textinput/FigEditText;

.field public n:Lcom/facebook/fig/textinput/FigEditText;

.field public o:Landroid/support/design/widget/TextInputLayout;

.field public p:Landroid/widget/EditText;

.field public q:Lcom/facebook/fig/textinput/FigEditText;

.field public r:Landroid/support/design/widget/TextInputLayout;

.field public s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

.field public t:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public u:LX/1ZF;

.field public v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

.field public w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

.field public x:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public y:Lcom/facebook/socialgood/model/Fundraiser;

.field private z:LX/Fp7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2291006
    const-class v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2291011
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2291012
    return-void
.end method

.method private static a(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2291008
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2291009
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->h:LX/03V;

    const-string v1, "fundraiser_creation_fragment_validation"

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2291010
    :cond_0
    return-void
.end method

.method public static a(Landroid/text/Editable;)Z
    .locals 1

    .prologue
    .line 2291007
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2290778
    if-eqz p1, :cond_1

    .line 2290779
    const-string v0, "fundraiser_charity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2290780
    const-string v0, "source"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->A:Ljava/lang/String;

    .line 2290781
    const-string v0, "cover_photo_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290782
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    const-string v1, "suggested_cover_photos"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2290783
    iput-object v1, v0, LX/FpN;->f:Ljava/util/ArrayList;

    .line 2290784
    const-string v0, "fundraiser_campaign_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->C:Ljava/lang/String;

    .line 2290785
    const-string v0, "fundraiser_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/model/Fundraiser;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290786
    const-string v0, "is_daf_charity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    .line 2290787
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2290788
    const-string v1, "finish_after_creation_enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->F:Z

    .line 2290789
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/Fp7;->CREATE:LX/Fp7;

    :goto_1
    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->z:LX/Fp7;

    .line 2290790
    return-void

    .line 2290791
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2290792
    const-string v1, "fundraiser_charity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2290793
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2290794
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->A:Ljava/lang/String;

    .line 2290795
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2290796
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->C:Ljava/lang/String;

    .line 2290797
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fundraiser_model"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/model/Fundraiser;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290798
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    if-nez v0, :cond_0

    .line 2290799
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2290800
    new-instance v2, LX/Fn6;

    invoke-direct {v2}, LX/Fn6;-><init>()V

    move-object v2, v2

    .line 2290801
    const-string v3, "id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/Fn6;

    .line 2290802
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2290803
    iget-object v3, v0, LX/FkM;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/FkI;

    invoke-direct {v3, v0}, LX/FkI;-><init>(LX/FkM;)V

    iget-object p1, v0, LX/FkM;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2290804
    goto :goto_0

    .line 2290805
    :cond_2
    sget-object v0, LX/Fp7;->EDIT:LX/Fp7;

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2291001
    invoke-static {p1, p2}, LX/BOb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2291002
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->e:LX/17Y;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->f:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2291003
    if-eqz v0, :cond_0

    .line 2291004
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->f:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2291005
    :cond_0
    return-void
.end method

.method public static d(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V
    .locals 3

    .prologue
    .line 2290994
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->t:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-nez v0, :cond_1

    .line 2290995
    :cond_0
    :goto_0
    return-void

    .line 2290996
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->t:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2290997
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c006e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 2290998
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Landroid/text/Editable;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-gt v2, v1, :cond_2

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Landroid/text/Editable;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2290999
    iput-boolean v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 2291000
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->t:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static q(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)LX/2vO;
    .locals 10

    .prologue
    .line 2290910
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2290911
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    instance-of v0, v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    check-cast v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v0

    .line 2290912
    :goto_0
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2290913
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2290914
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v1

    .line 2290915
    if-nez v1, :cond_5

    const/4 v1, 0x0

    .line 2290916
    :goto_1
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2290917
    const-string v6, "promo_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2290918
    const-string v6, "title"

    invoke-static {p0, v6, v2}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2290919
    const-string v6, "description"

    invoke-static {p0, v6, v0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2290920
    const-string v6, "currency"

    invoke-static {p0, v6, v3}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2290921
    const-string v6, "charity_id"

    iget-object v7, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-static {p0, v6, v7}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2290922
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 2290923
    new-instance v6, LX/4F4;

    invoke-direct {v6}, LX/4F4;-><init>()V

    .line 2290924
    const-string v7, "title"

    invoke-virtual {v6, v7, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290925
    move-object v2, v6

    .line 2290926
    const-string v6, "description"

    invoke-virtual {v2, v6, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290927
    move-object v0, v2

    .line 2290928
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2290929
    const-string v6, "charity_id"

    invoke-virtual {v0, v6, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290930
    move-object v0, v0

    .line 2290931
    const-string v2, "currency"

    invoke-virtual {v0, v2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290932
    move-object v0, v0

    .line 2290933
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2290934
    const-string v2, "end_time"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2290935
    move-object v0, v0

    .line 2290936
    const-string v1, "goal_amount"

    invoke-virtual {v0, v1, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290937
    move-object v0, v0

    .line 2290938
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->A:Ljava/lang/String;

    .line 2290939
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290940
    move-object v0, v0

    .line 2290941
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2290942
    iget-object v2, v1, LX/BOa;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2290943
    const-string v2, "session_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290944
    move-object v0, v0

    .line 2290945
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v1, :cond_1

    .line 2290946
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290947
    iget-object v2, v1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2290948
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2290949
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290950
    iget-object v2, v1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2290951
    const-string v2, "cover_photo_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290952
    :cond_0
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e()LX/4IT;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2290953
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e()LX/4IT;

    move-result-object v1

    .line 2290954
    const-string v2, "cover_photo_focus"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2290955
    :cond_1
    if-eqz v5, :cond_2

    .line 2290956
    const-string v1, "promo_id"

    invoke-virtual {v0, v1, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290957
    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2290958
    const-string v2, "promotional_source"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2290959
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2290960
    const-string v2, "promotional_source"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2290961
    const-string v2, "promotional_source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290962
    :cond_3
    :goto_2
    return-object v0

    .line 2290963
    :cond_4
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2290964
    :cond_5
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v1, v6

    goto/16 :goto_1

    .line 2290965
    :cond_6
    const-string v5, "fundraiser_campaign_id"

    iget-object v6, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->C:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2290966
    new-instance v5, LX/4F6;

    invoke-direct {v5}, LX/4F6;-><init>()V

    iget-object v6, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->C:Ljava/lang/String;

    .line 2290967
    const-string v7, "fundraiser_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290968
    move-object v5, v5

    .line 2290969
    const-string v6, "title"

    invoke-virtual {v5, v6, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290970
    move-object v2, v5

    .line 2290971
    const-string v5, "description"

    invoke-virtual {v2, v5, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290972
    move-object v0, v2

    .line 2290973
    const-string v2, "goal_amount"

    invoke-virtual {v0, v2, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290974
    move-object v0, v0

    .line 2290975
    const-string v2, "currency"

    invoke-virtual {v0, v2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290976
    move-object v0, v0

    .line 2290977
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2290978
    const-string v2, "end_time"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2290979
    move-object v0, v0

    .line 2290980
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->A:Ljava/lang/String;

    .line 2290981
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290982
    move-object v0, v0

    .line 2290983
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2290984
    iget-object v2, v1, LX/BOa;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2290985
    const-string v2, "session_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290986
    move-object v0, v0

    .line 2290987
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290988
    iget-object v2, v1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2290989
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2290990
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290991
    iget-object v2, v1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2290992
    const-string v2, "cover_photo_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290993
    goto :goto_2
.end method

.method public static s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z
    .locals 2

    .prologue
    .line 2290909
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->z:LX/Fp7;

    sget-object v1, LX/Fp7;->EDIT:LX/Fp7;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2290840
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    if-nez v0, :cond_1

    .line 2290841
    :cond_0
    :goto_0
    return-void

    .line 2290842
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290843
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2290844
    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2290845
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    invoke-virtual {v0}, LX/FpN;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2290846
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/FpN;->a(Ljava/lang/String;)V

    .line 2290847
    :cond_2
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290848
    iget-boolean v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->c:Z

    move v0, v1

    .line 2290849
    iput-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    .line 2290850
    iget-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    if-eqz v0, :cond_3

    .line 2290851
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/FpN;->b(Ljava/lang/String;)V

    .line 2290852
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2290853
    :cond_3
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290854
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2290855
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2290856
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290857
    iget-object v3, v1, Lcom/facebook/socialgood/model/Fundraiser;->d:Ljava/lang/String;

    move-object v1, v3

    .line 2290858
    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290859
    :cond_4
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-virtual {v0, v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->setVisibility(I)V

    .line 2290860
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v0, :cond_a

    .line 2290861
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    sget-object v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2290862
    :goto_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290863
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->f:Ljava/lang/String;

    move-object v0, v1

    .line 2290864
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2290865
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290866
    iget-object v2, v1, Lcom/facebook/socialgood/model/Fundraiser;->f:Ljava/lang/String;

    move-object v1, v2

    .line 2290867
    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290868
    :cond_5
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290869
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2290870
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2290871
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->k:LX/FkP;

    invoke-virtual {v0}, LX/FkP;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290872
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2290873
    :goto_2
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290874
    :cond_6
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290875
    iget v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->i:I

    move v0, v1

    .line 2290876
    if-eqz v0, :cond_7

    .line 2290877
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290878
    iget v2, v1, Lcom/facebook/socialgood/model/Fundraiser;->i:I

    move v1, v2

    .line 2290879
    div-int/lit8 v1, v1, 0x64

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290880
    :cond_7
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290881
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->j:Ljava/lang/String;

    move-object v0, v1

    .line 2290882
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2290883
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290884
    iget-object v2, v1, Lcom/facebook/socialgood/model/Fundraiser;->j:Ljava/lang/String;

    move-object v1, v2

    .line 2290885
    invoke-virtual {v0, v1}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->setText(Ljava/lang/CharSequence;)V

    .line 2290886
    :cond_8
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290887
    iget-wide v4, v0, Lcom/facebook/socialgood/model/Fundraiser;->h:J

    move-wide v0, v4

    .line 2290888
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_d

    .line 2290889
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2290890
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a()V

    .line 2290891
    :cond_9
    :goto_3
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2290892
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setTextColor(I)V

    .line 2290893
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290894
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->a()V

    goto/16 :goto_0

    .line 2290895
    :cond_a
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290896
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    move-object v0, v1

    .line 2290897
    if-eqz v0, :cond_b

    .line 2290898
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290899
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    move-object v0, v1

    .line 2290900
    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290901
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    sget-object v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_1

    .line 2290902
    :cond_b
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a()V

    goto/16 :goto_1

    .line 2290903
    :cond_c
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290904
    iget-object v1, v0, Lcom/facebook/socialgood/model/Fundraiser;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2290905
    invoke-static {v0}, LX/8oj;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 2290906
    :cond_d
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290907
    iget-wide v4, v1, Lcom/facebook/socialgood/model/Fundraiser;->h:J

    move-wide v2, v4

    .line 2290908
    invoke-virtual {v0, v2, v3}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->setEndTime(J)V

    goto :goto_3
.end method


# virtual methods
.method public final S_()Z
    .locals 10

    .prologue
    .line 2290808
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2290809
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2290810
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2290811
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2290812
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290813
    iget-object v6, v5, Lcom/facebook/socialgood/model/Fundraiser;->f:Ljava/lang/String;

    move-object v5, v6

    .line 2290814
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2290815
    :cond_0
    :goto_0
    move v0, v2

    .line 2290816
    if-eqz v0, :cond_1

    .line 2290817
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, LX/Fp6;

    invoke-direct {v1, p0}, LX/Fp6;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    .line 2290818
    new-instance v2, LX/0ju;

    invoke-direct {v2, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f083328

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f083329

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f08332a

    new-instance v4, LX/FpI;

    invoke-direct {v4, v1, v0}, LX/FpI;-><init>(LX/Fp6;Landroid/app/Activity;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f08332b

    new-instance v4, LX/FpH;

    invoke-direct {v4}, LX/FpH;-><init>()V

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 2290819
    const/4 v0, 0x1

    .line 2290820
    :goto_1
    return v0

    .line 2290821
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    invoke-virtual {v0}, LX/BOa;->h()V

    .line 2290822
    const/4 v0, 0x0

    goto :goto_1

    .line 2290823
    :cond_2
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290824
    iget-object v6, v5, Lcom/facebook/socialgood/model/Fundraiser;->g:Ljava/lang/String;

    move-object v5, v6

    .line 2290825
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2290826
    :cond_3
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    iget-object v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290827
    iget-object v6, v5, Lcom/facebook/socialgood/model/Fundraiser;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    move-object v5, v6

    .line 2290828
    invoke-virtual {v4, v5}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2290829
    :cond_4
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290830
    iget v6, v5, Lcom/facebook/socialgood/model/Fundraiser;->i:I

    move v5, v6

    .line 2290831
    div-int/lit8 v5, v5, 0x64

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2290832
    :cond_5
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v4}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v4}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290833
    iget-object v6, v5, Lcom/facebook/socialgood/model/Fundraiser;->j:Ljava/lang/String;

    move-object v5, v6

    .line 2290834
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2290835
    :cond_6
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    iget-object v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2290836
    iget-wide v8, v5, Lcom/facebook/socialgood/model/Fundraiser;->h:J

    move-wide v6, v8

    .line 2290837
    invoke-virtual {v4, v6, v7}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a(J)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 2290838
    goto/16 :goto_0

    .line 2290839
    :cond_7
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "200"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    iget-object v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-virtual {v5}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getDefaultEndTime()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a(Ljava/util/Calendar;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2290806
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2290807
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2291013
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    invoke-static {v0}, LX/FkM;->b(LX/0QB;)LX/FkM;

    move-result-object v4

    check-cast v4, LX/FkM;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v6

    check-cast v6, LX/17Y;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/BOa;->a(LX/0QB;)LX/BOa;

    move-result-object v8

    check-cast v8, LX/BOa;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/FpN;->a(LX/0QB;)LX/FpN;

    move-result-object v10

    check-cast v10, LX/FpN;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v11

    check-cast v11, LX/0wM;

    invoke-static {v0}, LX/FkP;->b(LX/0QB;)LX/FkP;

    move-result-object v12

    check-cast v12, LX/FkP;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b:LX/0zG;

    iput-object v4, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    iput-object v5, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->e:LX/17Y;

    iput-object v7, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->f:Landroid/content/Context;

    iput-object v8, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    iput-object v9, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->h:LX/03V;

    iput-object v10, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    iput-object v11, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->j:LX/0wM;

    iput-object v12, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->k:LX/FkP;

    iput-object v0, v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->l:LX/0ad;

    .line 2291014
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    .line 2291015
    iput-object p0, v0, LX/FkM;->c:LX/FkL;

    .line 2291016
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    .line 2291017
    iput-object p0, v0, LX/FkM;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    .line 2291018
    invoke-direct {p0, p1}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b(Landroid/os/Bundle;)V

    .line 2291019
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2291020
    iget-object v1, v0, LX/BOa;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2291021
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2291022
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2291023
    iput-object v1, v0, LX/BOa;->c:Ljava/lang/String;

    .line 2291024
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->A:Ljava/lang/String;

    .line 2291025
    iput-object v1, v0, LX/BOa;->b:Ljava/lang/String;

    .line 2291026
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    .line 2291027
    iput-object p0, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    .line 2291028
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v1, v0, LX/FpN;->e:Landroid/util/DisplayMetrics;

    .line 2291029
    iget-object v1, v0, LX/FpN;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, v0, LX/FpN;->e:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2291030
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2291031
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    .line 2291032
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2291033
    return-void
.end method

.method public final a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2290571
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2290572
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2290573
    :goto_0
    return-void

    .line 2290574
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2290575
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V

    .line 2290576
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2290577
    :goto_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_0

    .line 2290578
    :catch_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2290579
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    invoke-virtual {v0}, LX/FkM;->a()V

    .line 2290580
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_2

    .line 2290581
    check-cast p1, LX/4Ua;

    .line 2290582
    iget-object v0, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 2290583
    if-eqz v0, :cond_1

    iget v3, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    const v4, 0x1dfbcc

    if-ne v3, v4, :cond_1

    .line 2290584
    iget-object v0, v0, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    .line 2290585
    iget-object v3, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->r:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v3, v0}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 2290586
    iget-object v3, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->r:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v3, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 2290587
    iget-object v3, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    invoke-virtual {v3, v2, v0}, LX/BOa;->a(ILjava/lang/String;)V

    move v0, v1

    .line 2290588
    :goto_0
    iget-object v3, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-virtual {v3}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->c()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2290589
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f083316

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2290590
    iget-object v3, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-virtual {v3, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a(Ljava/lang/String;)V

    .line 2290591
    iget-object v3, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    invoke-virtual {v3, v2, v0}, LX/BOa;->a(ILjava/lang/String;)V

    move v0, v1

    .line 2290592
    :goto_1
    if-nez v0, :cond_0

    .line 2290593
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f083325

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2290594
    :cond_0
    return-void

    .line 2290595
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->r:Landroid/support/design/widget/TextInputLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 2290596
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->r:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2290597
    :cond_3
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-virtual {v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->b()V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2290598
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290599
    iget-boolean v1, v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->d:Z

    move v0, v1

    .line 2290600
    if-nez v0, :cond_1

    .line 2290601
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290602
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a()V

    .line 2290603
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    const/4 v1, 0x0

    .line 2290604
    iput-boolean v1, v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->i:Z

    .line 2290605
    return-void
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2290606
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    invoke-virtual {v0}, LX/FkM;->a()V

    .line 2290607
    if-eqz p1, :cond_0

    .line 2290608
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290609
    if-eqz v0, :cond_0

    .line 2290610
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290611
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel$FundraiserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->F:Z

    if-nez v0, :cond_0

    .line 2290612
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290613
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel$FundraiserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel$FundraiserModel;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "after_creation"

    invoke-direct {p0, v0, v1}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290614
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2290615
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2290616
    return-void
.end method

.method public final c(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2290617
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    invoke-virtual {v0}, LX/FkM;->a()V

    .line 2290618
    if-eqz p1, :cond_0

    .line 2290619
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290620
    if-eqz v0, :cond_0

    .line 2290621
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290622
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel;->a()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel$FundraiserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2290623
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290624
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel;->a()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel$FundraiserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditMutationModel$FundraiserModel;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "after_edit"

    invoke-direct {p0, v0, v1}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290625
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2290626
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2290627
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2290628
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2290629
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2290630
    sparse-switch p1, :sswitch_data_0

    .line 2290631
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->d(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    .line 2290632
    return-void

    .line 2290633
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2290634
    iget-object v1, v0, LX/BOa;->a:LX/0Zb;

    const-string v2, "fundraiser_creation_changed_cover_photo"

    const/4 p1, 0x0

    invoke-static {v0, v2, p1}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290635
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2290636
    if-eqz v0, :cond_3

    .line 2290637
    const-string v1, "original_image_item"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    .line 2290638
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2290639
    new-instance v2, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;-><init>(Landroid/net/Uri;)V

    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290640
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v1, v1

    .line 2290641
    if-eqz v1, :cond_1

    .line 2290642
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    new-instance v2, Landroid/graphics/PointF;

    .line 2290643
    iget-object p1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object p1, p1

    .line 2290644
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result p1

    .line 2290645
    iget-object p3, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v0, p3

    .line 2290646
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-direct {v2, p1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2290647
    iput-object v2, v1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    .line 2290648
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    sget-object v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2290649
    goto :goto_0

    .line 2290650
    :sswitch_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const/4 p1, 0x0

    .line 2290651
    const-string v1, "charity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2290652
    const-string v2, "charity_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2290653
    const-string v3, "charity_chosen_enum"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, LX/BOS;

    .line 2290654
    const-string v4, "is_daf_charity"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    .line 2290655
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4, v2}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290656
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2290657
    iget-object v4, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    iget-boolean v5, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    .line 2290658
    invoke-static {v4, v1, v2, v3, v5}, LX/BOa;->c(LX/BOa;Ljava/lang/String;Ljava/lang/String;LX/BOS;Z)Ljava/util/Map;

    move-result-object p2

    .line 2290659
    iget-object p3, v4, LX/BOa;->a:LX/0Zb;

    const-string v0, "fundraiser_creation_changed_charity"

    invoke-static {v4, v0, p2}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p3, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290660
    :cond_2
    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2290661
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/FpN;->a(Ljava/lang/String;)V

    .line 2290662
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-virtual {v1, p1}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->setVisibility(I)V

    .line 2290663
    iget-boolean v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    if-eqz v1, :cond_4

    .line 2290664
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/FpN;->b(Ljava/lang/String;)V

    .line 2290665
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2290666
    :goto_2
    goto/16 :goto_0

    .line 2290667
    :sswitch_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2290668
    const-string v1, "currency"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2290669
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2290670
    new-instance v3, LX/BOX;

    invoke-direct {v3, v1, v2, v0}, LX/BOX;-><init>(LX/BOa;Ljava/lang/String;Ljava/lang/String;)V

    .line 2290671
    iget-object v4, v1, LX/BOa;->a:LX/0Zb;

    const-string v5, "fundraiser_creation_changed_currency"

    invoke-static {v1, v5, v3}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290672
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v1, v0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2290673
    :cond_3
    const-string v0, "cover_photo_model"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    goto/16 :goto_1

    .line 2290674
    :cond_4
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x82 -> :sswitch_1
        0x378 -> :sswitch_0
        0x7c8 -> :sswitch_2
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3fc60524

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290675
    const v1, 0x7f03074a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2ed719e1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0xb2a61e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290676
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2290677
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    if-eqz v1, :cond_0

    .line 2290678
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290679
    :cond_0
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    if-eqz v1, :cond_1

    .line 2290680
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    .line 2290681
    iput-object v2, v1, LX/FkM;->c:LX/FkL;

    .line 2290682
    :cond_1
    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    .line 2290683
    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    .line 2290684
    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    .line 2290685
    const/16 v1, 0x2b

    const v2, -0x6468b4f5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a13c88b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290686
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2290687
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2290688
    const/16 v1, 0x2b

    const v2, -0x35c5f437

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2290689
    :goto_0
    return-void

    .line 2290690
    :cond_0
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->d(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    .line 2290691
    const v1, 0x76497e2f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2290692
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2290693
    const-string v0, "fundraiser_charity_id"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290694
    const-string v0, "source"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290695
    const-string v0, "cover_photo_model"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2290696
    const-string v0, "suggested_cover_photos"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    .line 2290697
    iget-object v2, v1, LX/FpN;->f:Ljava/util/ArrayList;

    move-object v1, v2

    .line 2290698
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2290699
    const-string v0, "fundraiser_campaign_id"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290700
    const-string v0, "fundraiser_model"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2290701
    const-string v0, "is_daf_charity"

    iget-boolean v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2290702
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x626df5ae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290703
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2290704
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2290705
    new-instance v2, LX/Fp4;

    invoke-direct {v2, p0}, LX/Fp4;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    invoke-interface {v1, v2}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2290706
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    .line 2290707
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    if-nez v1, :cond_0

    .line 2290708
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x5ce8ee62

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2290709
    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f083311

    :goto_1
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2290710
    iput-object v1, v2, LX/108;->g:Ljava/lang/String;

    .line 2290711
    move-object v1, v2

    .line 2290712
    const/4 v2, -0x2

    .line 2290713
    iput v2, v1, LX/108;->h:I

    .line 2290714
    move-object v1, v1

    .line 2290715
    const/4 v2, 0x0

    .line 2290716
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2290717
    move-object v1, v1

    .line 2290718
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->t:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2290719
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->t:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2290720
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    new-instance v2, LX/Fp5;

    invoke-direct {v2, p0}, LX/Fp5;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2290721
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f083313    # 1.810402E38f

    :goto_2
    invoke-interface {v2, v1}, LX/1ZF;->x_(I)V

    .line 2290722
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->u:LX/1ZF;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    goto :goto_0

    .line 2290723
    :cond_1
    const v1, 0x7f083310

    goto :goto_1

    .line 2290724
    :cond_2
    const v1, 0x7f083312

    goto :goto_2
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2290725
    new-instance v0, LX/Foz;

    invoke-direct {v0, p0}, LX/Foz;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    .line 2290726
    const p1, 0x7f0d1371

    invoke-virtual {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/textinput/FigEditText;

    iput-object p1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    .line 2290727
    iget-object p1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    new-instance p2, LX/Fp0;

    invoke-direct {p2, p0}, LX/Fp0;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    invoke-virtual {p1, p2}, Lcom/facebook/fig/textinput/FigEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290728
    const p1, 0x7f0d1374

    invoke-virtual {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/textinput/FigEditText;

    iput-object p1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    .line 2290729
    iget-object p1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {p1, v0}, Lcom/facebook/fig/textinput/FigEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2290730
    iget-object p1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->n:Lcom/facebook/fig/textinput/FigEditText;

    new-instance p2, LX/Fp1;

    invoke-direct {p2, p0}, LX/Fp1;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    invoke-virtual {p1, p2}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2290731
    const/4 p2, 0x0

    .line 2290732
    const v1, 0x7f0d1376

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2290733
    const v2, 0x7f0d1375

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/TextInputLayout;

    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->o:Landroid/support/design/widget/TextInputLayout;

    .line 2290734
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->k:LX/FkP;

    invoke-virtual {v2}, LX/FkP;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f03074b

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2290735
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    .line 2290736
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2290737
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    new-instance v2, LX/Fp2;

    invoke-direct {v2, p0}, LX/Fp2;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2290738
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->l:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-char v3, LX/FkF;->a:C

    const-string p1, ""

    invoke-interface {v1, v2, v3, p1}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2290739
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2290740
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->o:Landroid/support/design/widget/TextInputLayout;

    .line 2290741
    iput-boolean p2, v2, Landroid/support/design/widget/TextInputLayout;->r:Z

    .line 2290742
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290743
    :cond_0
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-virtual {v1, p2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2290744
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    instance-of v1, v1, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    if-eqz v1, :cond_1

    .line 2290745
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0a04

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 2290746
    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->f:Landroid/content/Context;

    const v3, 0x7f020847

    invoke-static {v2, v3}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v2}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2290747
    invoke-static {v2, v1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 2290748
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->p:Landroid/widget/EditText;

    invoke-static {v1, v2}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2290749
    :cond_1
    const/4 p1, 0x0

    .line 2290750
    const v0, 0x7f0d1379

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    .line 2290751
    const v0, 0x7f0d1378

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->r:Landroid/support/design/widget/TextInputLayout;

    .line 2290752
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->r:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    .line 2290753
    iput-boolean v1, v0, Landroid/support/design/widget/TextInputLayout;->r:Z

    .line 2290754
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    const-string v1, "200"

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290755
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->j:LX/0wM;

    const v2, 0x7f020a12

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, p1, p1, v1, p1}, Lcom/facebook/fig/textinput/FigEditText;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2290756
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q:Lcom/facebook/fig/textinput/FigEditText;

    new-instance v1, LX/Fp3;

    invoke-direct {v1, p0}, LX/Fp3;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2290757
    const v0, 0x7f0d137a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    .line 2290758
    const v0, 0x7f0d137b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->w:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    .line 2290759
    const v0, 0x7f0d1373

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    .line 2290760
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->setVisibility(I)V

    .line 2290761
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    .line 2290762
    iput-object p0, v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->j:Landroid/support/v4/app/Fragment;

    .line 2290763
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v0, :cond_5

    .line 2290764
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    sget-object v2, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2290765
    :goto_2
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    invoke-virtual {v1}, LX/FpN;->b()Z

    move-result v1

    .line 2290766
    iput-boolean v1, v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->i:Z

    .line 2290767
    const v0, 0x7f0d1372

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2290768
    iget-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->E:Z

    if-eqz v0, :cond_2

    .line 2290769
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->i:LX/FpN;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/FpN;->b(Ljava/lang/String;)V

    .line 2290770
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->x:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2290771
    :cond_2
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2290772
    iget-object v1, v0, LX/BOa;->a:LX/0Zb;

    const-string v2, "fundraiser_creation_view"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290773
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->t(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    .line 2290774
    return-void

    .line 2290775
    :cond_3
    const v2, 0x7f03074c

    goto/16 :goto_0

    .line 2290776
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2290777
    :cond_5
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a()V

    goto :goto_2
.end method
