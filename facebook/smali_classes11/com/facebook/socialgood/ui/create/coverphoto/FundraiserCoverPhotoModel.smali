.class public Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field private b:Landroid/net/Uri;

.field private c:Landroid/net/Uri;

.field public d:Z

.field public e:Landroid/graphics/PointF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2291425
    new-instance v0, LX/FpK;

    invoke-direct {v0}, LX/FpK;-><init>()V

    sput-object v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2291426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2291427
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291428
    iput-object v1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    .line 2291429
    iput-object v1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b:Landroid/net/Uri;

    .line 2291430
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->c:Landroid/net/Uri;

    .line 2291431
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->d:Z

    .line 2291432
    iput-object v1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    .line 2291433
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2291387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2291388
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    .line 2291389
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b:Landroid/net/Uri;

    .line 2291390
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->c:Landroid/net/Uri;

    .line 2291391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->d:Z

    .line 2291392
    const-class v0, Landroid/graphics/PointF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    .line 2291393
    return-void

    .line 2291394
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2291416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2291417
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291418
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291419
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    .line 2291420
    iput-object p2, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b:Landroid/net/Uri;

    .line 2291421
    iput-object v1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->c:Landroid/net/Uri;

    .line 2291422
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->d:Z

    .line 2291423
    iput-object v1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    .line 2291424
    return-void
.end method


# virtual methods
.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2291415
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->c:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2291434
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/4IT;
    .locals 4

    .prologue
    .line 2291409
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    if-nez v0, :cond_0

    .line 2291410
    const/4 v0, 0x0

    .line 2291411
    :goto_0
    return-object v0

    .line 2291412
    :cond_0
    new-instance v0, LX/4IT;

    invoke-direct {v0}, LX/4IT;-><init>()V

    .line 2291413
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4IT;->a(Ljava/lang/Double;)LX/4IT;

    .line 2291414
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4IT;->b(Ljava/lang/Double;)LX/4IT;

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2291403
    if-ne p0, p1, :cond_0

    .line 2291404
    const/4 v0, 0x1

    .line 2291405
    :goto_0
    return v0

    .line 2291406
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 2291407
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2291408
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v0

    check-cast p1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {p1}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2291402
    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2291395
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2291396
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2291397
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2291398
    iget-boolean v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2291399
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2291400
    return-void

    :cond_0
    move v0, v1

    .line 2291401
    goto :goto_0
.end method
