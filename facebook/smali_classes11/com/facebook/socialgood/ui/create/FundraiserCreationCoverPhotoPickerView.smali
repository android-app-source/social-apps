.class public Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Landroid/graphics/PointF;


# instance fields
.field private b:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/17Y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/FpN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Lcom/facebook/fbui/glyph/GlyphView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Landroid/widget/ProgressBar;

.field public i:Z

.field public j:Landroid/support/v4/app/Fragment;

.field private k:Landroid/view/View$OnClickListener;

.field private l:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 2291137
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a:Landroid/graphics/PointF;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2291134
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2291135
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->b()V

    .line 2291136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2291131
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2291132
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->b()V

    .line 2291133
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2291128
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2291129
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->b()V

    .line 2291130
    return-void
.end method

.method private static a(Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/FpN;)V
    .locals 0

    .prologue
    .line 2291127
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->c:LX/17Y;

    iput-object p3, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->d:LX/FpN;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {v2}, LX/FpN;->a(LX/0QB;)LX/FpN;

    move-result-object v2

    check-cast v2, LX/FpN;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/FpN;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2291116
    const-class v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2291117
    invoke-virtual {p0, p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2291118
    const v0, 0x7f030747

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2291119
    const v0, 0x7f0d136a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2291120
    const v0, 0x7f0d1368

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2291121
    const v0, 0x7f0d1369

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2291122
    const v0, 0x7f0d1367

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->h:Landroid/widget/ProgressBar;

    .line 2291123
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    .line 2291124
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->i()V

    .line 2291125
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e()V

    .line 2291126
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2291112
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->f()V

    .line 2291113
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->h()V

    .line 2291114
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->h:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2291115
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2291138
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2291139
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2291140
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2291110
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->h:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2291111
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2291108
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2291109
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    .line 2291104
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08331e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08331f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2291105
    new-instance v1, LX/FpA;

    invoke-direct {v1, p0, v0}, LX/FpA;-><init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;[Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->k:Landroid/view/View$OnClickListener;

    .line 2291106
    new-instance v0, LX/FpB;

    invoke-direct {v0, p0}, LX/FpB;-><init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;)V

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->l:Landroid/view/View$OnClickListener;

    .line 2291107
    return-void
.end method

.method public static j(Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;)V
    .locals 4

    .prologue
    .line 2291097
    sget-object v0, LX/0ax;->hc:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2291098
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->c:LX/17Y;

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2291099
    const-string v1, "suggested_photos"

    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->d:LX/FpN;

    .line 2291100
    iget-object v3, v2, LX/FpN;->f:Ljava/util/ArrayList;

    move-object v2, v3

    .line 2291101
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2291102
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x378

    iget-object v3, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->j:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2291103
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2291092
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->g()V

    .line 2291093
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->h()V

    .line 2291094
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2291095
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2291096
    return-void
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 2291086
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->FUNDRAISER_CREATION:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->o()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->LAUNCH_FUNDRAISER_COVER_PHOTO_CROPPER:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2291087
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2291088
    const-string v2, "extra_should_merge_camera_roll"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2291089
    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2291090
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->j:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1, p2, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2291091
    return-void
.end method

.method public final a(Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2

    .prologue
    .line 2291076
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->g()V

    .line 2291077
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->f()V

    .line 2291078
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2291079
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2291080
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2291081
    iget-object v1, p1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    move-object v1, v1

    .line 2291082
    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a:Landroid/graphics/PointF;

    :goto_0
    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 2291083
    return-void

    .line 2291084
    :cond_0
    iget-object v1, p1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    move-object v1, v1

    .line 2291085
    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x786214e2

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291068
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2291069
    const v1, -0x5a0f77ea

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2291070
    :goto_0
    return-void

    .line 2291071
    :cond_0
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->i:Z

    if-eqz v1, :cond_1

    .line 2291072
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->k:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2291073
    const v1, 0x4bcf4ec

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2291074
    :cond_1
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->l:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2291075
    const v1, -0x36f7ad19

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setParentFragment(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 2291066
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->j:Landroid/support/v4/app/Fragment;

    .line 2291067
    return-void
.end method
