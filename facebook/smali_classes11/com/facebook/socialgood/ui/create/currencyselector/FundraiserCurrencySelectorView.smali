.class public Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;
.super Lcom/facebook/fig/textinput/FigEditText;
.source ""


# instance fields
.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2291643
    invoke-direct {p0, p1}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;)V

    .line 2291644
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291645
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b:LX/0Ot;

    .line 2291646
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291647
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->c:LX/0Ot;

    .line 2291648
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291649
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->d:LX/0Ot;

    .line 2291650
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b()V

    .line 2291651
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2291670
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2291671
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291672
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b:LX/0Ot;

    .line 2291673
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291674
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->c:LX/0Ot;

    .line 2291675
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291676
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->d:LX/0Ot;

    .line 2291677
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b()V

    .line 2291678
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2291661
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2291662
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291663
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b:LX/0Ot;

    .line 2291664
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291665
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->c:LX/0Ot;

    .line 2291666
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291667
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->d:LX/0Ot;

    .line 2291668
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b()V

    .line 2291669
    return-void
.end method

.method private static a(Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;",
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2291660
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->c:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->d:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    const/16 v1, 0x5c8

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xc49

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->a(Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2291655
    const-class v0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2291656
    new-instance v0, LX/FpU;

    invoke-direct {v0, p0}, LX/FpU;-><init>(Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2291657
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v1, 0x7f020a12

    const v2, -0xb4b0aa

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v3, v3, v0, v3}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2291658
    const-string v0, "USD"

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->setText(Ljava/lang/CharSequence;)V

    .line 2291659
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2291652
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->setEnabled(Z)V

    .line 2291653
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v1, 0x7f020a12

    const v2, -0x23211d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v3, v3, v0, v3}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2291654
    return-void
.end method
