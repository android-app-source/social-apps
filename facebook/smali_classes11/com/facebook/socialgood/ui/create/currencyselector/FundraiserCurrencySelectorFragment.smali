.class public Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->FUNDRAISER_CURRENCY_SELECTOR_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/FpT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/widget/listview/BetterListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2291543
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;

    new-instance v0, LX/FpT;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    invoke-direct {v0, v2, p0}, LX/FpT;-><init>(Landroid/content/Context;LX/0wM;)V

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v2, v0, LX/FpT;->a:Landroid/content/Context;

    iput-object p0, v0, LX/FpT;->b:LX/0wM;

    move-object v1, v0

    check-cast v1, LX/FpT;

    iput-object v1, p1, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->a:LX/FpT;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2291571
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2291572
    const-class v0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2291573
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    .line 2291574
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2291575
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x42675946

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291576
    const v1, 0x7f030754

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x7cb933e1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x64d8db31

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291565
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2291566
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2291567
    if-nez v1, :cond_0

    .line 2291568
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x68a767fd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2291569
    :cond_0
    const v2, 0x7f083323

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2291570
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2291544
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2291545
    const v0, 0x7f0d1381

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 2291546
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_3

    .line 2291547
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->a:LX/FpT;

    .line 2291548
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2291549
    const-string v2, "currency"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2291550
    iput-object v1, v0, LX/FpT;->d:Ljava/lang/String;

    .line 2291551
    const/4 v4, 0x0

    .line 2291552
    sget-object v2, LX/5fx;->b:LX/0Px;

    move-object v5, v2

    .line 2291553
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2291554
    iget-object p1, v0, LX/FpT;->a:Landroid/content/Context;

    invoke-static {p1, v2}, LX/5fw;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2291555
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 2291556
    iget-object p2, v0, LX/FpT;->d:Ljava/lang/String;

    if-eqz p2, :cond_1

    iget-object p2, v0, LX/FpT;->d:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 2291557
    iget-object p2, v0, LX/FpT;->c:Ljava/util/ArrayList;

    new-instance v1, LX/FpS;

    invoke-direct {v1, v2, p1}, LX/FpS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2291558
    iget-object v2, v0, LX/FpT;->c:Ljava/util/ArrayList;

    const/4 p1, 0x1

    new-instance p2, LX/FpS;

    invoke-direct {p2}, LX/FpS;-><init>()V

    invoke-virtual {v2, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2291559
    :cond_0
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2291560
    :cond_1
    iget-object p2, v0, LX/FpT;->c:Ljava/util/ArrayList;

    new-instance v1, LX/FpS;

    invoke-direct {v1, v2, p1}, LX/FpS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2291561
    :cond_2
    const v2, 0x734ef564

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2291562
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->a:LX/FpT;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2291563
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/FpO;

    invoke-direct {v1, p0}, LX/FpO;-><init>(Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2291564
    :cond_3
    return-void
.end method
