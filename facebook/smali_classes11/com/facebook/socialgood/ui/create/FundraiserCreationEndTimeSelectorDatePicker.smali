.class public Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;
.super Lcom/facebook/fig/textinput/FigEditText;
.source ""

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/FpD;

.field public e:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2291285
    invoke-direct {p0, p1}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;)V

    .line 2291286
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291287
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->b:LX/0Ot;

    .line 2291288
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    .line 2291289
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->c()V

    .line 2291290
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2291291
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2291292
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291293
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->b:LX/0Ot;

    .line 2291294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    .line 2291295
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->c()V

    .line 2291296
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2291298
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2291299
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2291300
    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->b:LX/0Ot;

    .line 2291301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    .line 2291302
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->c()V

    .line 2291303
    return-void
.end method

.method private static a(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;LX/0Ot;LX/0wM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;",
            "LX/0Ot",
            "<",
            "LX/11S;",
            ">;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2291297
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->b:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->c:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    const/16 v1, 0x2e4

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {p0, v1, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->a(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;LX/0Ot;LX/0wM;)V

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2291279
    const-class v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2291280
    invoke-virtual {p0, p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2291281
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2291282
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->d:LX/FpD;

    if-eqz v0, :cond_0

    .line 2291283
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->d:LX/FpD;

    invoke-interface {v0}, LX/FpD;->a()V

    .line 2291284
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2291276
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setEnabled(Z)V

    .line 2291277
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->c:LX/0wM;

    const v1, 0x7f020a12

    const v2, -0x23211d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v3, v3, v0, v3}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2291278
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2291273
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setEnabled(Z)V

    .line 2291274
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->c:LX/0wM;

    const v1, 0x7f020a12

    const v2, -0xb4b0aa

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v3, v3, v0, v3}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2291275
    return-void
.end method

.method public getEndDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 2291272
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x2

    const v0, 0x50e1576a

    invoke-static {v7, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2291269
    new-instance v0, LX/D97;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e08bd

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    const/4 v5, 0x5

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, LX/D97;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 2291270
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 2291271
    const v0, 0x6203b3a1

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 1

    .prologue
    .line 2291263
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 2291264
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2291265
    invoke-virtual {v0, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 2291266
    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setDate(Ljava/util/Calendar;)V

    .line 2291267
    :cond_0
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->d()V

    .line 2291268
    return-void
.end method

.method public setDate(Ljava/util/Calendar;)V
    .locals 4

    .prologue
    .line 2291260
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    .line 2291261
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    sget-object v1, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setText(Ljava/lang/CharSequence;)V

    .line 2291262
    return-void
.end method

.method public setOnCalendarDatePickedListener(LX/FpD;)V
    .locals 0

    .prologue
    .line 2291258
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->d:LX/FpD;

    .line 2291259
    return-void
.end method
