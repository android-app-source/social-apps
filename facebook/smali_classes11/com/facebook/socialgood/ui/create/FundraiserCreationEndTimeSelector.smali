.class public Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/BOa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:Ljava/util/Calendar;

.field private d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

.field private e:Landroid/support/design/widget/TextInputLayout;

.field private f:Lcom/facebook/events/ui/date/TimePickerView;

.field private g:Landroid/support/design/widget/TextInputLayout;

.field private h:Lcom/facebook/widget/SwitchCompat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2291228
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2291229
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d()V

    .line 2291230
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2291231
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2291232
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d()V

    .line 2291233
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2291234
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2291235
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d()V

    .line 2291236
    return-void
.end method

.method private static a(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;LX/0wM;LX/BOa;)V
    .locals 0

    .prologue
    .line 2291237
    iput-object p1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->b:LX/BOa;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/BOa;->a(LX/0QB;)LX/BOa;

    move-result-object v1

    check-cast v1, LX/BOa;

    invoke-static {p0, v0, v1}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;LX/0wM;LX/BOa;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;LX/BOT;)V
    .locals 4

    .prologue
    .line 2291238
    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2291239
    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 2291240
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->b:LX/BOa;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, LX/BOa;->a(Ljava/lang/String;LX/BOT;Z)V

    .line 2291241
    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2291155
    const-class v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2291156
    const v0, 0x7f030749

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2291157
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->e()V

    .line 2291158
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->h()V

    .line 2291159
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->i()V

    .line 2291160
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2291242
    const v0, 0x7f0d136b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->h:Lcom/facebook/widget/SwitchCompat;

    .line 2291243
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->h:Lcom/facebook/widget/SwitchCompat;

    new-instance v1, LX/FpC;

    invoke-direct {v1, p0}, LX/FpC;-><init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2291244
    return-void
.end method

.method public static f(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;)V
    .locals 1

    .prologue
    .line 2291245
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->a()V

    .line 2291246
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->j()V

    .line 2291247
    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->b()V

    .line 2291248
    return-void
.end method

.method public static g(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;)V
    .locals 1

    .prologue
    .line 2291249
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->b()V

    .line 2291250
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->k()V

    .line 2291251
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2291220
    const v0, 0x7f0d136e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    .line 2291221
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getDefaultEndTime()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setDate(Ljava/util/Calendar;)V

    .line 2291222
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    new-instance v1, LX/FpE;

    invoke-direct {v1, p0}, LX/FpE;-><init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;)V

    .line 2291223
    iput-object v1, v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->d:LX/FpD;

    .line 2291224
    const v0, 0x7f0d136d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->e:Landroid/support/design/widget/TextInputLayout;

    .line 2291225
    const v0, 0x7f0d136f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->g:Landroid/support/design/widget/TextInputLayout;

    .line 2291226
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->b()V

    .line 2291227
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2291252
    const v0, 0x7f0d1370

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/TimePickerView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 2291253
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getDefaultEndTime()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 2291254
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    new-instance v1, LX/FpF;

    invoke-direct {v1, p0}, LX/FpF;-><init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;)V

    .line 2291255
    iput-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 2291256
    invoke-direct {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->k()V

    .line 2291257
    return-void
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2291216
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setEnabled(Z)V

    .line 2291217
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTextColor(I)V

    .line 2291218
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a:LX/0wM;

    const v2, 0x7f020a12

    const v3, -0x23211d

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v4, v4, v1, v4}, Lcom/facebook/events/ui/date/TimePickerView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2291219
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2291212
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setEnabled(Z)V

    .line 2291213
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTextColor(I)V

    .line 2291214
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a:LX/0wM;

    const v2, 0x7f020a12

    const v3, -0xb4b0aa

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v4, v4, v1, v4}, Lcom/facebook/events/ui/date/TimePickerView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2291215
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2291209
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->h:Lcom/facebook/widget/SwitchCompat;

    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->h:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2291210
    return-void

    .line 2291211
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2291204
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->e:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 2291205
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->e:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 2291206
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->g:Landroid/support/design/widget/TextInputLayout;

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 2291207
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->g:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 2291208
    return-void
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 2291196
    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2291197
    const/4 v0, 0x1

    .line 2291198
    :goto_0
    return v0

    .line 2291199
    :cond_0
    cmp-long v0, p1, v2

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2291200
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2291201
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2291202
    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->a(Ljava/util/Calendar;)Z

    move-result v0

    goto :goto_0

    .line 2291203
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/Calendar;)Z
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/16 v6, 0x9

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v0, 0x1

    .line 2291190
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2291191
    :cond_0
    :goto_0
    return v0

    .line 2291192
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2291193
    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v1

    .line 2291194
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 2291195
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2291185
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->e:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 2291186
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->e:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 2291187
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->g:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 2291188
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->g:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 2291189
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2291184
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->getSelectedEndTime()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultEndTime()Ljava/util/Calendar;
    .locals 3

    .prologue
    .line 2291180
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->c:Ljava/util/Calendar;

    if-nez v0, :cond_0

    .line 2291181
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->c:Ljava/util/Calendar;

    .line 2291182
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->c:Ljava/util/Calendar;

    const/4 v1, 0x5

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2291183
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->c:Ljava/util/Calendar;

    return-object v0
.end method

.method public getSelectedEndTime()Ljava/util/Calendar;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v4, 0x1

    .line 2291166
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->h:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2291167
    :goto_0
    return-object v0

    .line 2291168
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2291169
    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    .line 2291170
    iget-object v3, v2, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->e:Ljava/util/Calendar;

    move-object v3, v3

    .line 2291171
    if-nez v3, :cond_2

    move-object v0, v1

    .line 2291172
    goto :goto_0

    .line 2291173
    :cond_2
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 2291174
    iget-object v2, v1, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v1, v2

    .line 2291175
    if-nez v1, :cond_3

    .line 2291176
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0

    .line 2291177
    :cond_3
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 2291178
    iget-object v2, v1, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v5, v2

    .line 2291179
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0xb

    invoke-virtual {v5, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    goto :goto_0
.end method

.method public setEndTime(J)V
    .locals 5

    .prologue
    .line 2291161
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2291162
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2291163
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->d:Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;

    invoke-virtual {v1, v0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelectorDatePicker;->setDate(Ljava/util/Calendar;)V

    .line 2291164
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationEndTimeSelector;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 2291165
    return-void
.end method
