.class public Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->FUNDRAISER_CREATION_SUGGESTED_COVER_PHOTO_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field private a:Lcom/facebook/widget/listview/BetterListView;

.field public b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

.field public c:Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2291327
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;

    new-instance v1, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;

    invoke-direct {v1}, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;-><init>()V

    move-object v1, v1

    move-object v1, v1

    check-cast v1, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->c:Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;

    iput-object p0, p1, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->d:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2291328
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2291329
    const-class v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2291330
    if-eqz p1, :cond_0

    .line 2291331
    const-string v0, "selected_cover_photo"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2291332
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 2291333
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2291334
    if-ne p2, v4, :cond_0

    .line 2291335
    packed-switch p1, :pswitch_data_0

    .line 2291336
    :cond_0
    :goto_0
    return-void

    .line 2291337
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v0, :cond_0

    .line 2291338
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2291339
    if-eqz v0, :cond_1

    .line 2291340
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v1, v1

    .line 2291341
    if-eqz v1, :cond_1

    .line 2291342
    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    new-instance v2, Landroid/graphics/PointF;

    .line 2291343
    iget-object v3, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v3, v3

    .line 2291344
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    .line 2291345
    iget-object p1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v0, p1

    .line 2291346
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2291347
    iput-object v2, v1, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->e:Landroid/graphics/PointF;

    .line 2291348
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2291349
    const-string v1, "cover_photo_model"

    iget-object v2, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2291350
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2291351
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x999b2b9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291352
    const v1, 0x7f03074d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x7a95ed53

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2291353
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2291354
    const-string v0, "selected_cover_photo"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2291355
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6136d6d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291356
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2291357
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2291358
    if-nez v1, :cond_0

    .line 2291359
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x2725d1c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2291360
    :cond_0
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2291361
    const v2, 0x7f083321

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2291362
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2291363
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->c:Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;

    .line 2291364
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2291365
    const-string p1, "suggested_photos"

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    move-object v1, v1

    .line 2291366
    if-eqz v1, :cond_0

    .line 2291367
    iput-object v1, v0, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->b:Ljava/util/List;

    .line 2291368
    :cond_0
    const v0, 0x7f0d137c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 2291369
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->c:Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2291370
    iget-object v0, p0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/FpG;

    invoke-direct {v1, p0}, LX/FpG;-><init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2291371
    return-void
.end method
