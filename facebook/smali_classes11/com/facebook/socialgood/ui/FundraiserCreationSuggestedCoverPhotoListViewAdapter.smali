.class public Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2291045
    const-class v0, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2291042
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2291043
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->b:Ljava/util/List;

    .line 2291044
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;
    .locals 1

    .prologue
    .line 2291041
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2291046
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2291040
    invoke-virtual {p0, p1}, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->a(I)Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2291039
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2291034
    if-nez p2, :cond_0

    .line 2291035
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03074e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 2291036
    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2291037
    invoke-virtual {p0, p1}, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->a(I)Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2291038
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method
