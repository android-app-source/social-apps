.class public Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Fon;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Foq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Foy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/BOa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field private f:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

.field public g:Landroid/os/ResultReceiver;

.field private h:Ljava/lang/String;

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Fon;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Fon;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/widget/listview/BetterListView;

.field public m:Lcom/facebook/fig/textinput/FigEditText;

.field public n:Lcom/facebook/resources/ui/FbTextView;

.field public o:Lcom/facebook/fbui/glyph/GlyphView;

.field public p:Lcom/facebook/fbui/glyph/GlyphView;

.field public q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/Runnable;

.field public final u:Landroid/os/Handler;

.field public final v:Landroid/view/View$OnClickListener;

.field public final w:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2290275
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2290276
    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2290277
    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->r:Ljava/lang/String;

    .line 2290278
    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    .line 2290279
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->u:Landroid/os/Handler;

    .line 2290280
    new-instance v0, LX/Fog;

    invoke-direct {v0, p0}, LX/Fog;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->v:Landroid/view/View$OnClickListener;

    .line 2290281
    new-instance v0, LX/Foh;

    invoke-direct {v0, p0}, LX/Foh;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->w:Landroid/view/View$OnClickListener;

    .line 2290282
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    new-instance v0, LX/Foq;

    invoke-direct {v0}, LX/Foq;-><init>()V

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iput-object v1, v0, LX/Foq;->a:Landroid/view/LayoutInflater;

    iput-object v2, v0, LX/Foq;->b:Landroid/content/Context;

    move-object v1, v0

    check-cast v1, LX/Foq;

    invoke-static {p0}, LX/Foy;->b(LX/0QB;)LX/Foy;

    move-result-object v2

    check-cast v2, LX/Foy;

    invoke-static {p0}, LX/BOa;->a(LX/0QB;)LX/BOa;

    move-result-object p0

    check-cast p0, LX/BOa;

    iput-object v1, p1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    iput-object v2, p1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    iput-object p0, p1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->c:LX/BOa;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2290269
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Foy;->a(Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;Ljava/lang/String;)V

    .line 2290270
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->r:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->append(Ljava/lang/CharSequence;)V

    .line 2290271
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2290272
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/textinput/FigEditText;->setCursorVisible(Z)V

    .line 2290273
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->o$redex0(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290274
    return-void
.end method

.method public static o$redex0(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 2

    .prologue
    .line 2290263
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2290264
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2290265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    .line 2290266
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2290267
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    sget-object v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Foq;->a(Ljava/util/ArrayList;)V

    .line 2290268
    return-void
.end method

.method public static p(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2290253
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    invoke-virtual {v0}, LX/Foy;->a()V

    .line 2290254
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Foq;->a(Ljava/util/ArrayList;)V

    .line 2290255
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2290256
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2290257
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2290258
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2290259
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setCursorVisible(Z)V

    .line 2290260
    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2290261
    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    .line 2290262
    return-void
.end method

.method public static q(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2290243
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    invoke-virtual {v0}, LX/Foy;->a()V

    .line 2290244
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    sget-object v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Foq;->a(Ljava/util/ArrayList;)V

    .line 2290245
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2290246
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    invoke-virtual {v0, v2}, LX/Foy;->a(Ljava/lang/String;)V

    .line 2290247
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2290248
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2290249
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setCursorVisible(Z)V

    .line 2290250
    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2290251
    iput-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    .line 2290252
    return-void
.end method

.method public static s(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 4

    .prologue
    .line 2290283
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2290284
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->c:LX/BOa;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2290285
    new-instance v2, LX/BOV;

    invoke-direct {v2, v0, v1}, LX/BOV;-><init>(LX/BOa;Ljava/lang/String;)V

    .line 2290286
    iget-object v3, v0, LX/BOa;->a:LX/0Zb;

    const-string p0, "fundraiser_creation_searched_charities"

    invoke-static {v0, p0, v2}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290287
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2290241
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290242
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2290211
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2290212
    const-class v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v1, p0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2290213
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    .line 2290214
    iput-object p0, v1, LX/Foy;->e:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    .line 2290215
    if-nez p1, :cond_0

    .line 2290216
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v1

    .line 2290217
    :cond_0
    if-eqz p1, :cond_1

    .line 2290218
    const-string v1, "extra_charity_search_category"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2290219
    const-string v1, "extra_charity_search_category_translated_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->r:Ljava/lang/String;

    .line 2290220
    const-string v1, "extra_charity_search_page_cursor"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    .line 2290221
    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2290222
    if-eqz v1, :cond_6

    .line 2290223
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2290224
    const-string v2, "source"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->h:Ljava/lang/String;

    .line 2290225
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2290226
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->c:LX/BOa;

    iget-object v2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->h:Ljava/lang/String;

    .line 2290227
    iput-object v2, v1, LX/BOa;->b:Ljava/lang/String;

    .line 2290228
    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2290229
    const-string v2, "extra_should_show_daf_disclosure"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 2290230
    const-string v2, "fundraiser_for_story_upsell"

    iget-object v3, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    :cond_4
    iput-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->e:Z

    .line 2290231
    iget-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->e:Z

    if-eqz v0, :cond_5

    .line 2290232
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2290233
    const-string v1, "extra_daf_disclosure_model"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->f:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2290234
    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2290235
    const-string v1, "result_receiver"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ResultReceiver;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->g:Landroid/os/ResultReceiver;

    .line 2290236
    :cond_6
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->c:LX/BOa;

    .line 2290237
    iget-object v1, v0, LX/BOa;->a:LX/0Zb;

    const-string v2, "fundraiser_creation_open_charity_picker"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290238
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    .line 2290239
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2290240
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x320d872e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290157
    const v1, 0x7f030746

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x47018373

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1159308

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290208
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2290209
    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    invoke-virtual {v1}, LX/Foy;->a()V

    .line 2290210
    const/16 v1, 0x2b

    const v2, 0x57be5b74    # 4.18599995E14f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2290201
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2290202
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-eqz v0, :cond_0

    .line 2290203
    const-string v0, "extra_charity_search_category"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290204
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2290205
    const-string v0, "extra_charity_search_category_translated_name"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290206
    :cond_1
    const-string v0, "extra_charity_search_page_cursor"

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290207
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2290158
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2290159
    const v0, 0x7f0d1366

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->l:Lcom/facebook/widget/listview/BetterListView;

    .line 2290160
    const v0, 0x7f0d1363

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    .line 2290161
    const v0, 0x7f0d1365

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2290162
    const v0, 0x7f0d1362

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->o:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2290163
    const v0, 0x7f0d1364

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2290164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->k:Ljava/util/ArrayList;

    .line 2290165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->i:Ljava/util/ArrayList;

    .line 2290166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2290167
    sput-object v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->j:Ljava/util/ArrayList;

    new-instance v1, LX/Fon;

    sget-object p1, LX/Foo;->LOADER:LX/Foo;

    invoke-direct {v1, p1}, LX/Fon;-><init>(LX/Foo;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2290168
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    if-eqz v0, :cond_0

    .line 2290169
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->v:Landroid/view/View$OnClickListener;

    iget-object p1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->w:Landroid/view/View$OnClickListener;

    iget-boolean p2, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->e:Z

    .line 2290170
    iput-object v1, v0, LX/Foq;->d:Landroid/view/View$OnClickListener;

    .line 2290171
    iput-object p1, v0, LX/Foq;->e:Landroid/view/View$OnClickListener;

    .line 2290172
    iput-boolean p2, v0, LX/Foq;->i:Z

    .line 2290173
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    sget-object v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Foq;->a(Ljava/util/ArrayList;)V

    .line 2290174
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->l:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_1

    .line 2290175
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->l:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2290176
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->l:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/Foi;

    invoke-direct {v1, p0}, LX/Foi;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2290177
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    if-eqz v0, :cond_2

    .line 2290178
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    new-instance v1, LX/Foj;

    invoke-direct {v1, p0}, LX/Foj;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2290179
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    new-instance v1, LX/Fok;

    invoke-direct {v1, p0}, LX/Fok;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290180
    :cond_2
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->o:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_3

    .line 2290181
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->o:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Fol;

    invoke-direct {v1, p0}, LX/Fol;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290182
    :cond_3
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_4

    .line 2290183
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Fom;

    invoke-direct {v1, p0}, LX/Fom;-><init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290184
    :cond_4
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    .line 2290185
    new-instance v1, LX/Fmd;

    invoke-direct {v1}, LX/Fmd;-><init>()V

    move-object v1, v1

    .line 2290186
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2290187
    iget-object p1, v0, LX/Foy;->c:LX/0tX;

    invoke-virtual {p1, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2290188
    new-instance p1, LX/Fov;

    invoke-direct {p1, v0}, LX/Fov;-><init>(LX/Foy;)V

    iget-object p2, v0, LX/Foy;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2290189
    iget-boolean v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->e:Z

    if-eqz v0, :cond_5

    .line 2290190
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->f:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    if-eqz v0, :cond_6

    .line 2290191
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    iget-object v1, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->f:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    invoke-virtual {v0, v1}, LX/Foq;->a(Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;)V

    .line 2290192
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2290193
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b$redex0(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290194
    :goto_1
    return-void

    .line 2290195
    :cond_6
    iget-object v0, p0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    .line 2290196
    invoke-static {}, LX/FnA;->a()LX/Fn9;

    move-result-object v1

    .line 2290197
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2290198
    iget-object p1, v0, LX/Foy;->c:LX/0tX;

    invoke-virtual {p1, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance p1, LX/Fou;

    invoke-direct {p1, v0}, LX/Fou;-><init>(LX/Foy;)V

    iget-object p2, v0, LX/Foy;->b:LX/0Tf;

    invoke-static {v1, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2290199
    goto :goto_0

    .line 2290200
    :cond_7
    invoke-static {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    goto :goto_1
.end method
