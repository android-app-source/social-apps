.class public Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2290024
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2290017
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2290018
    const v0, 0x7f0400d9

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchActivity;->overridePendingTransition(II)V

    .line 2290019
    const v0, 0x7f030745

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchActivity;->setContentView(I)V

    .line 2290020
    new-instance v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-direct {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;-><init>()V

    .line 2290021
    invoke-virtual {p0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2290022
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1360

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2290023
    return-void
.end method
