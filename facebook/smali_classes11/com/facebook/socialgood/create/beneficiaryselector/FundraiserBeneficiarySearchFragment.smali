.class public Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->FUNDRAISER_BENEFICIARY_SEARCH:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/Fka;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Fkf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/Fkk;

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/FkY;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/FkY;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Lcom/facebook/ui/search/SearchEditText;

.field public h:Ljava/lang/String;

.field public final i:Landroid/os/Handler;

.field public final j:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2278727
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2278728
    new-instance v0, LX/FkU;

    invoke-direct {v0, p0}, LX/FkU;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)V

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->e:Ljava/util/ArrayList;

    .line 2278729
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->i:Landroid/os/Handler;

    .line 2278730
    new-instance v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment$2;

    invoke-direct {v0, p0}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment$2;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)V

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->j:Ljava/lang/Runnable;

    .line 2278731
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;

    new-instance v0, LX/Fka;

    invoke-static {v2}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const-class v3, Landroid/content/Context;

    invoke-interface {v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v0, v1, v3}, LX/Fka;-><init>(Landroid/view/LayoutInflater;Landroid/content/Context;)V

    move-object v1, v0

    check-cast v1, LX/Fka;

    new-instance v0, LX/Fkf;

    invoke-direct {v0}, LX/Fkf;-><init>()V

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v2}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    iput-object v3, v0, LX/Fkf;->a:LX/0tX;

    iput-object p0, v0, LX/Fkf;->b:LX/1Ck;

    move-object v2, v0

    check-cast v2, LX/Fkf;

    iput-object v1, p1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a:LX/Fka;

    iput-object v2, p1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->b:LX/Fkf;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2278710
    sget-object v0, LX/FkX;->a:[I

    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->c:LX/Fkk;

    invoke-virtual {v1}, LX/Fkk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2278711
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized beneficiary type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->c:LX/Fkk;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2278712
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->b:LX/Fkf;

    .line 2278713
    invoke-static {}, LX/Fmf;->a()LX/Fme;

    move-result-object v1

    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "page_cursor"

    invoke-virtual {v1, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2278714
    move-object v1, v1

    .line 2278715
    if-nez p2, :cond_0

    .line 2278716
    iget-object v2, v0, LX/Fkf;->b:LX/1Ck;

    const-string v3, "beneficiary_search"

    iget-object p0, v0, LX/Fkf;->a:LX/0tX;

    invoke-virtual {p0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    invoke-static {v0, p1}, LX/Fkf;->b(LX/Fkf;Ljava/lang/String;)LX/0Ve;

    move-result-object p0

    invoke-virtual {v2, v3, v1, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2278717
    :goto_0
    return-void

    .line 2278718
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->b:LX/Fkf;

    .line 2278719
    new-instance v1, LX/FmP;

    invoke-direct {v1}, LX/FmP;-><init>()V

    move-object v1, v1

    .line 2278720
    const-string v2, "search_query"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "page_cursor"

    invoke-virtual {v1, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2278721
    move-object v1, v1

    .line 2278722
    if-nez p2, :cond_1

    .line 2278723
    iget-object v2, v0, LX/Fkf;->b:LX/1Ck;

    const-string v3, "beneficiary_search"

    iget-object p0, v0, LX/Fkf;->a:LX/0tX;

    invoke-virtual {p0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    invoke-static {v0, p1}, LX/Fkf;->a(LX/Fkf;Ljava/lang/String;)LX/0Ve;

    move-result-object p0

    invoke-virtual {v2, v3, v1, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2278724
    :goto_1
    goto :goto_0

    .line 2278725
    :cond_0
    iget-object v2, v0, LX/Fkf;->b:LX/1Ck;

    const-string v3, "beneficiary_search"

    new-instance p0, LX/Fkd;

    invoke-direct {p0, v0, v1}, LX/Fkd;-><init>(LX/Fkf;LX/0zO;)V

    invoke-static {v0, p1}, LX/Fkf;->b(LX/Fkf;Ljava/lang/String;)LX/0Ve;

    move-result-object v1

    invoke-virtual {v2, v3, p0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0

    .line 2278726
    :cond_1
    iget-object v2, v0, LX/Fkf;->b:LX/1Ck;

    const-string v3, "beneficiary_search"

    new-instance p0, LX/Fkb;

    invoke-direct {p0, v0, v1}, LX/Fkb;-><init>(LX/Fkf;LX/0zO;)V

    invoke-static {v0, p1}, LX/Fkf;->a(LX/Fkf;Ljava/lang/String;)LX/0Ve;

    move-result-object v1

    invoke-virtual {v2, v3, p0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static e(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2278706
    sget-object v0, LX/FkX;->a:[I

    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->c:LX/Fkk;

    invoke-virtual {v1}, LX/Fkk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2278707
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized beneficiary type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->c:LX/Fkk;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2278708
    :pswitch_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083336

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2278709
    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083335

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static k(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)V
    .locals 2

    .prologue
    .line 2278702
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->h:Ljava/lang/String;

    .line 2278703
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2278704
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a:LX/Fka;

    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Fka;->a(Ljava/util/ArrayList;)V

    .line 2278705
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2278687
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2278688
    const-class v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2278689
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2278690
    const-string v1, "beneficiary_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Fkk;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->c:LX/Fkk;

    .line 2278691
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->c:LX/Fkk;

    if-nez v0, :cond_0

    .line 2278692
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Beneficiary type cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2278693
    :cond_0
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2278694
    if-nez v0, :cond_1

    .line 2278695
    :goto_0
    return-void

    .line 2278696
    :cond_1
    new-instance v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;-><init>(Landroid/content/Context;)V

    .line 2278697
    iget-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v2, v2

    .line 2278698
    iput-object v2, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    .line 2278699
    iget-object v2, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-static {p0}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->e(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2278700
    invoke-interface {v0, v1}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2278701
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;ZLjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/FkY;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2278664
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2278665
    :goto_0
    return-void

    .line 2278666
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2278667
    iput-object p4, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->h:Ljava/lang/String;

    .line 2278668
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a:LX/Fka;

    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p3}, LX/Fka;->a(Ljava/util/ArrayList;Z)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6a18a511

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2278686
    const v1, 0x7f030741

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x863c085

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x8e28a0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2278683
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2278684
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2278685
    const/16 v1, 0x2b

    const v2, -0x742b9c48

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2278669
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2278670
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->b:LX/Fkf;

    .line 2278671
    iput-object p0, v0, LX/Fkf;->c:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;

    .line 2278672
    const v0, 0x7f0d1357

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2278673
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->d:Ljava/util/ArrayList;

    .line 2278674
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_0

    .line 2278675
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a:LX/Fka;

    iget-object p1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, LX/Fka;->a(Ljava/util/ArrayList;)V

    .line 2278676
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object p1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a:LX/Fka;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2278677
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/FkV;

    invoke-direct {p1, p0}, LX/FkV;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2278678
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    if-eqz v0, :cond_1

    .line 2278679
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->g:Lcom/facebook/ui/search/SearchEditText;

    new-instance p1, LX/FkW;

    invoke-direct {p1, p0}, LX/FkW;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2278680
    :cond_1
    invoke-static {p0}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->k(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;)V

    .line 2278681
    invoke-static {p0, v1, v1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a$redex0(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2278682
    return-void
.end method
