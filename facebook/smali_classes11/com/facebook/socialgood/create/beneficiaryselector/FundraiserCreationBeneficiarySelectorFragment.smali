.class public Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/17Y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/fig/listitem/FigListItem;

.field public i:Lcom/facebook/fig/listitem/FigListItem;

.field public j:Lcom/facebook/fig/listitem/FigListItem;

.field public k:Lcom/facebook/fig/listitem/FigListItem;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2278928
    const-class v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2278935
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2278936
    return-void
.end method

.method public static a(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;LX/Fkk;)V
    .locals 1

    .prologue
    .line 2278929
    invoke-virtual {p2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2278930
    invoke-virtual {p2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2278931
    invoke-virtual {p2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2278932
    sget-object v0, LX/Fkk;->FRIEND:LX/Fkk;

    if-eq p3, v0, :cond_0

    sget-object v0, LX/Fkk;->NONPROFIT:LX/Fkk;

    if-ne p3, v0, :cond_1

    .line 2278933
    :cond_0
    new-instance v0, LX/Fki;

    invoke-direct {v0, p0, p3}, LX/Fki;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;LX/Fkk;)V

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2278934
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2278937
    if-nez p0, :cond_1

    .line 2278938
    :cond_0
    :goto_0
    return v0

    .line 2278939
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel$BeneficiaryImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2278940
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2278910
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2278911
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, LX/0Tf;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p1

    check-cast p1, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v2, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->b:LX/0Tf;

    iput-object v3, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->c:LX/0tX;

    iput-object p1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->d:LX/17Y;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2278912
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7c1880f7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2278913
    const v1, 0x7f030744

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x335470b1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2278914
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2278915
    new-instance v0, LX/FmQ;

    invoke-direct {v0}, LX/FmQ;-><init>()V

    move-object v0, v0

    .line 2278916
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2278917
    new-instance v1, LX/FmQ;

    invoke-direct {v1}, LX/FmQ;-><init>()V

    const-string p1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p2

    invoke-virtual {v1, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    .line 2278918
    iget-object p1, v1, LX/0gW;->e:LX/0w7;

    move-object v1, p1

    .line 2278919
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2278920
    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/Fkj;

    invoke-direct {v1, p0}, LX/Fkj;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;)V

    iget-object p1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->b:LX/0Tf;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2278921
    const v0, 0x7f0d135a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2278922
    const v0, 0x7f0d135b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2278923
    const v0, 0x7f0d135c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->h:Lcom/facebook/fig/listitem/FigListItem;

    .line 2278924
    const v0, 0x7f0d135d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->i:Lcom/facebook/fig/listitem/FigListItem;

    .line 2278925
    const v0, 0x7f0d135e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->j:Lcom/facebook/fig/listitem/FigListItem;

    .line 2278926
    const v0, 0x7f0d135f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->k:Lcom/facebook/fig/listitem/FigListItem;

    .line 2278927
    return-void
.end method
