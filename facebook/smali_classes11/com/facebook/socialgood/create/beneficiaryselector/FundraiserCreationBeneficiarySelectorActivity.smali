.class public Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2278858
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2278859
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2278860
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;->p:LX/0h5;

    .line 2278861
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;->p:LX/0h5;

    const v1, 0x7f083334

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2278862
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;->p:LX/0h5;

    new-instance v1, LX/Fkg;

    invoke-direct {v1, p0}, LX/Fkg;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2278863
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2278864
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2278865
    const v0, 0x7f030743

    invoke-virtual {p0, v0}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;->setContentView(I)V

    .line 2278866
    invoke-direct {p0}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;->a()V

    .line 2278867
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1359

    new-instance v2, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    invoke-direct {v2}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2278868
    return-void
.end method
