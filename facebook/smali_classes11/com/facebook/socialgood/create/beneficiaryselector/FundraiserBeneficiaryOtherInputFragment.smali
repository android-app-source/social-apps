.class public Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->FUNDRAISER_BENEFICIARY_OTHER_INPUT_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:Landroid/view/inputmethod/InputMethodManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:I

.field public d:LX/1ZF;

.field public e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public f:Lcom/facebook/fig/textinput/FigEditText;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/widget/FbImageView;

.field public i:LX/0hs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2278621
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;

    invoke-static {v2}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    const-class p0, Landroid/content/Context;

    invoke-interface {v2, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iput-object v1, p1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2278584
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2278585
    const-class v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2278586
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->d:LX/1ZF;

    .line 2278587
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->d:LX/1ZF;

    if-nez v0, :cond_0

    .line 2278588
    :goto_0
    return-void

    .line 2278589
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->d:LX/1ZF;

    const v1, 0x7f083337

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2278590
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f083338

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2278591
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2278592
    move-object v0, v0

    .line 2278593
    const/4 v1, -0x2

    .line 2278594
    iput v1, v0, LX/108;->h:I

    .line 2278595
    move-object v0, v0

    .line 2278596
    const/4 v1, 0x0

    .line 2278597
    iput-boolean v1, v0, LX/108;->d:Z

    .line 2278598
    move-object v0, v0

    .line 2278599
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2278600
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->d:LX/1ZF;

    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2278601
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->d:LX/1ZF;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1e005e38

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2278620
    const v1, 0x7f030740

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x9f7e8d1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2278602
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2278603
    const v0, 0x7f0d1354

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->f:Lcom/facebook/fig/textinput/FigEditText;

    .line 2278604
    const v0, 0x7f0d1355

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2278605
    const v0, 0x7f0d1356

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->h:Lcom/facebook/widget/FbImageView;

    .line 2278606
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->c:I

    .line 2278607
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2278608
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 2278609
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->f:Lcom/facebook/fig/textinput/FigEditText;

    new-instance v1, LX/FkR;

    invoke-direct {v1, p0}, LX/FkR;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2278610
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0165

    iget v3, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->c:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget p2, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->c:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v4, p1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2278611
    new-instance v0, LX/0hs;

    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->b:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->i:LX/0hs;

    .line 2278612
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->i:LX/0hs;

    const/4 v1, -0x1

    .line 2278613
    iput v1, v0, LX/0hs;->t:I

    .line 2278614
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->i:LX/0hs;

    iget-object v1, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->h:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2278615
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->i:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2278616
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->i:LX/0hs;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08333a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2278617
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->i:LX/0hs;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08333b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2278618
    iget-object v0, p0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->h:Lcom/facebook/widget/FbImageView;

    new-instance v1, LX/FkS;

    invoke-direct {v1, p0}, LX/FkS;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2278619
    return-void
.end method
