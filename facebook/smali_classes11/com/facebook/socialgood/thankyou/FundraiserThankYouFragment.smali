.class public Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->FUNDRAISER_THANK_YOU_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public j:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B35;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/BOb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public r:Lcom/facebook/resources/ui/FbTextView;

.field public s:Lcom/facebook/resources/ui/FbTextView;

.field public t:Lcom/facebook/fig/button/FigButton;

.field public u:Lcom/facebook/fig/button/FigButton;

.field public v:LX/1ZF;

.field private w:LX/8FZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2289502
    const-class v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2289488
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2289489
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2289490
    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->b:LX/0Ot;

    .line 2289491
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2289492
    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->d:LX/0Ot;

    .line 2289493
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2289494
    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->g:LX/0Ot;

    .line 2289495
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2289496
    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->h:LX/0Ot;

    .line 2289497
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2289498
    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->i:LX/0Ot;

    .line 2289499
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2289500
    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->k:LX/0Ot;

    .line 2289501
    return-void
.end method

.method public static a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2289480
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "post_donation_page_fragment_error"

    invoke-virtual {v0, v1, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2289481
    const/16 p1, 0x8

    .line 2289482
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0832ff

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2289483
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->s:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f083300

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2289484
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2289485
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->u:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2289486
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v1, "https://s-static.intern.facebook.com/rsrc.php/v2/yV/r/gAig2BXVLYf.png"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object p1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2289487
    return-void
.end method

.method public static a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2289441
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2289442
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->u:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2289443
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0832ff

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2289444
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->s:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f083300

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2289445
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    .line 2289446
    invoke-static {v0}, LX/FoJ;->b(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2289447
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->o()Ljava/lang/String;

    move-result-object v1

    .line 2289448
    :goto_0
    move-object v0, v1

    .line 2289449
    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->o:Ljava/lang/String;

    .line 2289450
    const/16 v3, 0x8

    .line 2289451
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->p:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x4fa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2289452
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f083305

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2289453
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/FoK;

    invoke-direct {v1, p0}, LX/FoK;-><init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2289454
    :goto_1
    if-eqz p3, :cond_9

    .line 2289455
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->u:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f08001e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2289456
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->u:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/FoO;

    invoke-direct {v1, p0}, LX/FoO;-><init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2289457
    :goto_2
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2289458
    return-void

    .line 2289459
    :cond_0
    invoke-static {v0}, LX/FoJ;->a(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;)I

    move-result v1

    const v2, 0x5e1f75b

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2289460
    if-eqz v1, :cond_2

    .line 2289461
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2289462
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2289463
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;->a()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2289464
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel;->a()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2289465
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    .line 2289466
    :cond_4
    if-eqz p3, :cond_6

    .line 2289467
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2289468
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    goto :goto_1

    .line 2289469
    :cond_5
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f083303

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2289470
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/FoL;

    invoke-direct {v1, p0}, LX/FoL;-><init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 2289471
    :cond_6
    if-eqz p2, :cond_7

    .line 2289472
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f083301

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2289473
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/FoM;

    invoke-direct {v1, p0}, LX/FoM;-><init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 2289474
    :cond_7
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2289475
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 2289476
    :cond_8
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f083302

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2289477
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/FoN;

    invoke-direct {v1, p0}, LX/FoN;-><init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 2289478
    :cond_9
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->u:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f083304

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2289479
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->u:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/FoP;

    invoke-direct {v1, p0}, LX/FoP;-><init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2
.end method

.method public static l(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V
    .locals 3

    .prologue
    .line 2289435
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    const-string v1, "native_thank_you_page"

    invoke-static {v0, v1}, LX/BOb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2289436
    if-eqz v1, :cond_0

    .line 2289437
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->j:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2289438
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->j:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2289439
    :cond_0
    invoke-static {p0}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->q(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    .line 2289440
    return-void
.end method

.method public static q(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V
    .locals 2

    .prologue
    .line 2289432
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2289433
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2289434
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2289420
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    const/16 v3, 0xac0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    const/16 v8, 0xc49

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x455

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3be

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, Landroid/content/Context;

    invoke-interface {v0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    const/16 v12, 0x24de

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/BOb;->b(LX/0QB;)LX/BOb;

    move-result-object v0

    check-cast v0, LX/BOb;

    iput-object v3, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->b:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->c:LX/0Zb;

    iput-object v5, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->d:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->e:LX/0tX;

    iput-object v7, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->f:Ljava/util/concurrent/ExecutorService;

    iput-object v8, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->g:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->h:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->i:LX/0Ot;

    iput-object v11, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->j:Landroid/content/Context;

    iput-object v12, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->k:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->l:LX/BOb;

    .line 2289421
    if-eqz p1, :cond_0

    .line 2289422
    const-string v0, "fundraiser_thank_you_data_model"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    .line 2289423
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    .line 2289424
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "overlay_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->p:Ljava/lang/String;

    .line 2289425
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2289426
    const-string v0, "No campaign ID passed in"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2289427
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2289428
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->w:LX/8FZ;

    .line 2289429
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->w:LX/8FZ;

    .line 2289430
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2289431
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2289415
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2289416
    const/16 v0, 0x2766

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2767

    if-ne p1, v0, :cond_2

    .line 2289417
    :cond_0
    invoke-static {p0}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->l(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    .line 2289418
    :cond_1
    :goto_0
    return-void

    .line 2289419
    :cond_2
    invoke-static {p0}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->q(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x76499be4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2289388
    const v1, 0x7f030763

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x21f4b5f8

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2289412
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2289413
    const-string v0, "fundraiser_thank_you_data_model"

    iget-object v1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2289414
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3555be0c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2289407
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2289408
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->v:LX/1ZF;

    .line 2289409
    iget-object v1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->v:LX/1ZF;

    if-nez v1, :cond_0

    .line 2289410
    :goto_0
    const/16 v1, 0x2b

    const v2, 0xa3cd87f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2289411
    :cond_0
    iget-object v1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->v:LX/1ZF;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2289389
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2289390
    const v0, 0x7f0d13b4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2289391
    const v0, 0x7f0d13b5

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 2289392
    const v0, 0x7f0d13b6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 2289393
    const v0, 0x7f0d13b7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->t:Lcom/facebook/fig/button/FigButton;

    .line 2289394
    const v0, 0x7f0d13b8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->u:Lcom/facebook/fig/button/FigButton;

    .line 2289395
    new-instance v0, LX/Fo1;

    invoke-direct {v0}, LX/Fo1;-><init>()V

    move-object v0, v0

    .line 2289396
    const-string v1, "campaign_id"

    iget-object p1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2289397
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2289398
    iget-object v1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    move-object v0, v0

    .line 2289399
    new-instance v1, LX/FoQ;

    invoke-direct {v1, p0}, LX/FoQ;-><init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V

    iget-object p1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2289400
    iget-object v0, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->c:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    .line 2289401
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_view_thank_you"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "fundraiser_page"

    .line 2289402
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2289403
    move-object p0, p0

    .line 2289404
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v1, p0

    .line 2289405
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289406
    return-void
.end method
