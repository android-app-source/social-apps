.class public Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/B9y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/Fli;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:Z

.field public j:LX/1ZF;

.field public k:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2281787
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2281788
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2281789
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->e:LX/0Ot;

    .line 2281790
    return-void
.end method

.method public static n(Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2281791
    iget v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083341

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083342

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2281792
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2281793
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v2, p0

    check-cast v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    invoke-static {v7}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v3

    check-cast v3, LX/B9y;

    invoke-static {v7}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v7}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v7}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    const/16 v0, 0x259

    invoke-static {v7, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    iput-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->a:LX/B9y;

    iput-object v4, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->c:LX/0tX;

    iput-object v6, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->d:LX/0Zb;

    iput-object v7, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->e:LX/0Ot;

    .line 2281794
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2281795
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2281796
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->g:Ljava/lang/String;

    .line 2281797
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    .line 2281798
    const/4 v0, 0x2

    new-array v0, v0, [LX/Flu;

    const/4 v1, 0x0

    sget-object v2, LX/Flu;->DONATED:LX/Flu;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/Flu;->NOT_DONATED:LX/Flu;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 2281799
    new-instance v1, LX/Fli;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->g:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, v4}, LX/Fli;-><init>(LX/0gc;Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->f:LX/Fli;

    .line 2281800
    return-void

    .line 2281801
    :cond_0
    const-string v0, "SELECTED_GUEST_COUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2281802
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->f:LX/Fli;

    invoke-virtual {v0}, LX/Fli;->f()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    .line 2281803
    iget v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2281804
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2281805
    const/16 v0, 0x71

    if-ne p1, v0, :cond_0

    .line 2281806
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2281807
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->g:Ljava/lang/String;

    iget v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    .line 2281808
    new-instance v2, LX/4F8;

    invoke-direct {v2}, LX/4F8;-><init>()V

    .line 2281809
    const-string v3, "fundraiser_campaign_id"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281810
    move-object v2, v2

    .line 2281811
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2281812
    const-string v4, "number_updated"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2281813
    move-object v2, v2

    .line 2281814
    new-instance v3, LX/FlL;

    invoke-direct {v3}, LX/FlL;-><init>()V

    move-object v3, v3

    .line 2281815
    const-string v4, "0"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/FlL;

    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2281816
    iget-object v3, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v1, v2

    .line 2281817
    new-instance v2, LX/Fly;

    iget-object v3, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->g:Ljava/lang/String;

    iget v4, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    iget-object v5, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->d:LX/0Zb;

    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v2, v3, v4, v5, v0}, LX/Fly;-><init>(Ljava/lang/String;ILX/0Zb;LX/03V;)V

    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2281818
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->d:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->g:Ljava/lang/String;

    .line 2281819
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "fundraiser_page_personal_message_send"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "fundraiser_page"

    .line 2281820
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2281821
    move-object v2, v2

    .line 2281822
    const-string v3, "fundraiser_campaign_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v1, v2

    .line 2281823
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2281824
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2281825
    :cond_0
    return-void

    .line 2281826
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->d:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->g:Ljava/lang/String;

    .line 2281827
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "fundraiser_page_personal_message_failure"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "fundraiser_page"

    .line 2281828
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2281829
    move-object v2, v2

    .line 2281830
    const-string v3, "fundraiser_campaign_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v1, v2

    .line 2281831
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x257909a2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2281832
    const v1, 0x7f03075b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x3358c73a    # -8.7672368E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2281833
    const-string v0, "SELECTED_GUEST_COUNT"

    iget v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2281834
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2281835
    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x406a5b25

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2281836
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2281837
    const/4 v5, 0x1

    .line 2281838
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    .line 2281839
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    if-nez v1, :cond_0

    .line 2281840
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x7813adb9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2281841
    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2281842
    iput v5, v1, LX/108;->a:I

    .line 2281843
    move-object v1, v1

    .line 2281844
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f080029

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2281845
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2281846
    move-object v1, v1

    .line 2281847
    const/4 v2, -0x2

    .line 2281848
    iput v2, v1, LX/108;->h:I

    .line 2281849
    move-object v1, v1

    .line 2281850
    iget-boolean v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->i:Z

    .line 2281851
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2281852
    move-object v1, v1

    .line 2281853
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->k:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2281854
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->k:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2281855
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    new-instance v2, LX/Flx;

    invoke-direct {v2, p0}, LX/Flx;-><init>(Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2281856
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    invoke-static {p0}, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->n(Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2281857
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    invoke-interface {v1, v5}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2281858
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2281859
    const v0, 0x7f0d139d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2281860
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->f:LX/Fli;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2281861
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->f:LX/Fli;

    .line 2281862
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v2, v1, LX/Fli;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 2281863
    iget-object v4, v1, LX/Fli;->b:[Lcom/facebook/base/fragment/FbFragment;

    iget-object v5, v1, LX/Fli;->e:Ljava/lang/String;

    iget-object v2, v1, LX/Fli;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Flu;

    .line 2281864
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2281865
    const-string p1, "FUNDRAISER_GUEST_LIST_TAB"

    invoke-virtual {v2}, LX/Flu;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281866
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {v6, p1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281867
    new-instance p1, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;

    invoke-direct {p1}, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;-><init>()V

    .line 2281868
    invoke-virtual {p1, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2281869
    move-object v2, p1

    .line 2281870
    aput-object v2, v4, v3

    .line 2281871
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2281872
    :cond_0
    const v1, 0x7f0d139c

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2281873
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2281874
    const v0, 0x7f0d139e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->l:Landroid/widget/TextView;

    .line 2281875
    return-void
.end method
