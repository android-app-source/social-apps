.class public abstract Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/FlW;


# instance fields
.field public a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/FlQ;

.field public c:Landroid/view/View;

.field public d:Lcom/facebook/widget/listview/BetterListView;

.field public e:Landroid/widget/TextView;

.field public f:Z

.field private g:Ljava/lang/String;

.field public h:LX/Flu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2280159
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2280160
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;

    new-instance v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    invoke-direct {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;-><init>()V

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, LX/0Tf;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    const/16 v6, 0x259

    invoke-static {v1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x15e7

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v0, Landroid/content/Context;

    invoke-interface {v1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b:LX/0tX;

    iput-object v4, v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->c:LX/0Tf;

    iput-object v5, v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->d:LX/0Zb;

    iput-object v6, v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->e:LX/0Ot;

    iput-object p0, v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->f:LX/0Or;

    iput-object v0, v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->g:Landroid/content/Context;

    move-object v1, v2

    check-cast v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    iput-object v1, p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2280189
    iget-boolean v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->f:Z

    if-eqz v0, :cond_0

    .line 2280190
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2280191
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2280192
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2280193
    :goto_0
    return-void

    .line 2280194
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2280195
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->b:LX/FlQ;

    invoke-virtual {v0}, LX/FlQ;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 2280196
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2280197
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2280198
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2280199
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Flv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2280183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->f:Z

    .line 2280184
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->b:LX/FlQ;

    .line 2280185
    iget-object v1, v0, LX/FlQ;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2280186
    const v1, -0x13ae2936

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2280187
    invoke-direct {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d()V

    .line 2280188
    return-void
.end method

.method public a(LX/Flv;)V
    .locals 0

    .prologue
    .line 2280182
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2280200
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2280201
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2280202
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->c()LX/FlQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->b:LX/FlQ;

    .line 2280203
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2280204
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2280205
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->g:Ljava/lang/String;

    .line 2280206
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2280207
    const-string v1, "FUNDRAISER_GUEST_LIST_TAB"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Flu;->fromString(Ljava/lang/String;)LX/Flu;

    move-result-object v0

    move-object v0, v0

    .line 2280208
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->h:LX/Flu;

    .line 2280209
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->h:LX/Flu;

    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->b()Z

    move-result v3

    .line 2280210
    iput-object v1, v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->k:Ljava/lang/String;

    .line 2280211
    iput-object v2, v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->l:LX/Flu;

    .line 2280212
    iput-object p0, v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->m:LX/FlW;

    .line 2280213
    iput-boolean v3, v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->n:Z

    .line 2280214
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    invoke-virtual {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b()V

    .line 2280215
    return-void
.end method

.method public abstract b()Z
.end method

.method public abstract c()LX/FlQ;
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x64686c75

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2280181
    const v1, 0x7f03075a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x54123c53

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2280161
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2280162
    const v0, 0x7f0d139b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->c:Landroid/view/View;

    .line 2280163
    const v0, 0x7f0d1399

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    .line 2280164
    const v0, 0x7f0d139a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->e:Landroid/widget/TextView;

    .line 2280165
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->e:Landroid/widget/TextView;

    .line 2280166
    sget-object v1, LX/FlV;->a:[I

    iget-object p1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->h:LX/Flu;

    invoke-virtual {p1}, LX/Flu;->ordinal()I

    move-result p1

    aget v1, v1, p1

    packed-switch v1, :pswitch_data_0

    .line 2280167
    const v1, 0x7f083346

    :goto_0
    move v1, v1

    .line 2280168
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2280169
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->b:LX/FlQ;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2280170
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/FlT;

    invoke-direct {v1, p0}, LX/FlT;-><init>(Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2280171
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    invoke-virtual {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b()V

    .line 2280172
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    .line 2280173
    iget-object v1, v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    sget-object p1, LX/FlY;->INITIAL:LX/FlY;

    if-ne v1, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2280174
    iput-boolean v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->f:Z

    .line 2280175
    invoke-direct {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d()V

    .line 2280176
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/FlU;

    invoke-direct {v1, p0}, LX/FlU;-><init>(Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2280177
    return-void

    .line 2280178
    :pswitch_0
    const v1, 0x7f083343

    goto :goto_0

    .line 2280179
    :pswitch_1
    const v1, 0x7f083344

    goto :goto_0

    .line 2280180
    :pswitch_2
    const v1, 0x7f083345

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
