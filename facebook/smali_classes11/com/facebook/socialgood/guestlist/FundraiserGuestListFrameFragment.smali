.class public Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0zG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Fli;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Flu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2280366
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2280367
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object p0

    check-cast p0, LX/0zG;

    iput-object p0, p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->a:LX/0zG;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2280368
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2280369
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2280370
    const/4 v0, 0x3

    new-array v0, v0, [LX/Flu;

    const/4 v1, 0x0

    sget-object v2, LX/Flu;->DONATED:LX/Flu;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/Flu;->INVITED:LX/Flu;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/Flu;->SHARED:LX/Flu;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->c:Ljava/util/List;

    .line 2280371
    new-instance v0, LX/Fli;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->c:Ljava/util/List;

    .line 2280372
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2280373
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {v4, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 2280374
    invoke-direct {v0, v1, v2, v3, v4}, LX/Fli;-><init>(LX/0gc;Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->b:LX/Fli;

    .line 2280375
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xb35e47b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2280376
    const v1, 0x7f03075b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x751cf6c8

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7985d68a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2280377
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2280378
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2280379
    if-eqz v0, :cond_0

    .line 2280380
    const v2, 0x7f083340

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 2280381
    instance-of v2, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    if-eqz v2, :cond_0

    .line 2280382
    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2280383
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x4e355bc2    # 7.6067238E8f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2280384
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2280385
    const v0, 0x7f0d139d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2280386
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->b:LX/Fli;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2280387
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->b:LX/Fli;

    .line 2280388
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v2, v1, LX/Fli;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 2280389
    iget-object v4, v1, LX/Fli;->b:[Lcom/facebook/base/fragment/FbFragment;

    iget-object v5, v1, LX/Fli;->e:Ljava/lang/String;

    iget-object v2, v1, LX/Fli;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Flu;

    .line 2280390
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2280391
    const-string p1, "FUNDRAISER_GUEST_LIST_TAB"

    invoke-virtual {v2}, LX/Flu;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2280392
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {v6, p1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2280393
    new-instance p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;

    invoke-direct {p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;-><init>()V

    .line 2280394
    invoke-virtual {p1, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2280395
    move-object v2, p1

    .line 2280396
    aput-object v2, v4, v3

    .line 2280397
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2280398
    :cond_0
    const v1, 0x7f0d139c

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2280399
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2280400
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFrameFragment;->c:Ljava/util/List;

    .line 2280401
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2280402
    const-string v3, "extra_view_fundraiser_supporters_connection_type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    .line 2280403
    if-eqz v2, :cond_1

    .line 2280404
    sget-object v3, LX/Fla;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2280405
    :cond_1
    sget-object v2, LX/Flu;->DONATED:LX/Flu;

    :goto_1
    move-object v2, v2

    .line 2280406
    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2280407
    return-void

    .line 2280408
    :pswitch_0
    sget-object v2, LX/Flu;->DONATED:LX/Flu;

    goto :goto_1

    .line 2280409
    :pswitch_1
    sget-object v2, LX/Flu;->INVITED:LX/Flu;

    goto :goto_1

    .line 2280410
    :pswitch_2
    sget-object v2, LX/Flu;->SHARED:LX/Flu;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
