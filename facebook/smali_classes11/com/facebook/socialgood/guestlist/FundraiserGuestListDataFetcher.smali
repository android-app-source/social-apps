.class public Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/FlY;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:LX/Flu;

.field public m:LX/FlW;

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2280225
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2280334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2280335
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280336
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->e:LX/0Ot;

    .line 2280337
    iput-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2280338
    sget-object v0, LX/FlY;->INITIAL:LX/FlY;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    .line 2280339
    iput-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->j:Ljava/lang/String;

    .line 2280340
    return-void
.end method

.method public static a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Flv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2280332
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->m:LX/FlW;

    invoke-interface {v0, p1}, LX/FlW;->a(LX/0Px;)V

    .line 2280333
    return-void
.end method

.method public static a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2280331
    iget-boolean v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->n:Z

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2280328
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2280329
    const/4 v0, 0x0

    .line 2280330
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 6

    .prologue
    .line 2280300
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 2280301
    :cond_0
    :goto_0
    return-void

    .line 2280302
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    sget-object v1, LX/FlY;->COMPLETE:LX/FlY;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    sget-object v1, LX/FlY;->ERROR:LX/FlY;

    if-eq v0, v1, :cond_0

    .line 2280303
    sget-object v0, LX/FlX;->a:[I

    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->l:LX/Flu;

    invoke-virtual {v1}, LX/Flu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2280304
    const/4 v0, 0x0

    :goto_1
    move-object v0, v0

    .line 2280305
    if-eqz v0, :cond_3

    .line 2280306
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2280307
    new-instance v1, LX/0w7;

    invoke-direct {v1}, LX/0w7;-><init>()V

    .line 2280308
    const-string v2, "campaignID"

    iget-object v3, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    move-result-object v2

    const-string v3, "numConnections"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;

    move-result-object v2

    const-string v3, "profile_image_size"

    .line 2280309
    iget-object v4, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->g:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b21fa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    move v4, v4

    .line 2280310
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;

    .line 2280311
    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->j:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2280312
    const-string v2, "afterCursor"

    iget-object v3, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    .line 2280313
    :cond_2
    move-object v1, v1

    .line 2280314
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2280315
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2280316
    move-object v0, v0

    .line 2280317
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2280318
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->c:LX/0Tf;

    invoke-static {v0, p0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2280319
    :cond_3
    sget-object v0, LX/FlY;->COMPLETE:LX/FlY;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    goto :goto_0

    .line 2280320
    :pswitch_0
    new-instance v0, LX/FlM;

    invoke-direct {v0}, LX/FlM;-><init>()V

    move-object v0, v0

    .line 2280321
    goto :goto_1

    .line 2280322
    :pswitch_1
    new-instance v0, LX/FlO;

    invoke-direct {v0}, LX/FlO;-><init>()V

    move-object v0, v0

    .line 2280323
    goto :goto_1

    .line 2280324
    :pswitch_2
    new-instance v0, LX/FlK;

    invoke-direct {v0}, LX/FlK;-><init>()V

    move-object v0, v0

    .line 2280325
    goto :goto_1

    .line 2280326
    :pswitch_3
    new-instance v0, LX/FlN;

    invoke-direct {v0}, LX/FlN;-><init>()V

    move-object v0, v0

    .line 2280327
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2280293
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->k:Ljava/lang/String;

    invoke-static {v0}, LX/BOe;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2280294
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->l:LX/Flu;

    if-eqz v1, :cond_0

    .line 2280295
    const-string v1, "guest_list_tab_load_failure"

    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->l:LX/Flu;

    invoke-virtual {v2}, LX/Flu;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2280296
    :cond_0
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2280297
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "guest_list_tab_load_failure"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2280298
    sget-object v0, LX/FlY;->ERROR:LX/FlY;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    .line 2280299
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2280226
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2280227
    if-eqz p1, :cond_0

    .line 2280228
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2280229
    if-nez v0, :cond_2

    .line 2280230
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->k:Ljava/lang/String;

    invoke-static {v0}, LX/BOe;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2280231
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->l:LX/Flu;

    if-eqz v1, :cond_1

    .line 2280232
    const-string v1, "guest_list_tab_load_failure"

    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->l:LX/Flu;

    invoke-virtual {v2}, LX/Flu;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2280233
    :cond_1
    iget-object v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2280234
    sget-object v0, LX/FlY;->ERROR:LX/FlY;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    .line 2280235
    :goto_0
    return-void

    .line 2280236
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2280237
    sget-object v1, LX/FlX;->a:[I

    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->l:LX/Flu;

    invoke-virtual {v2}, LX/Flu;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2280238
    const/4 v1, 0x0

    :goto_1
    move-object v0, v1

    .line 2280239
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->j:Ljava/lang/String;

    .line 2280240
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->j:Ljava/lang/String;

    if-nez v0, :cond_3

    sget-object v0, LX/FlY;->COMPLETE:LX/FlY;

    :goto_2
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->i:LX/FlY;

    .line 2280241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2280242
    :cond_3
    sget-object v0, LX/FlY;->PAGING:LX/FlY;

    goto :goto_2

    .line 2280243
    :pswitch_0
    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel;

    .line 2280244
    invoke-virtual {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel;

    move-result-object v1

    const/4 v3, 0x0

    .line 2280245
    if-nez v1, :cond_4

    move-object v2, v3

    .line 2280246
    :goto_3
    move-object v1, v2

    .line 2280247
    goto :goto_1

    .line 2280248
    :pswitch_1
    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;

    .line 2280249
    invoke-virtual {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    move-result-object v1

    const/4 v3, 0x0

    .line 2280250
    if-nez v1, :cond_8

    move-object v2, v3

    .line 2280251
    :goto_4
    move-object v1, v2

    .line 2280252
    goto :goto_1

    .line 2280253
    :pswitch_2
    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel;

    .line 2280254
    invoke-virtual {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel;

    move-result-object v1

    const/4 v3, 0x0

    .line 2280255
    if-nez v1, :cond_c

    move-object v2, v3

    .line 2280256
    :goto_5
    move-object v1, v2

    .line 2280257
    goto :goto_1

    .line 2280258
    :pswitch_3
    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel;

    .line 2280259
    invoke-virtual {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel;

    move-result-object v1

    const/4 v3, 0x0

    .line 2280260
    if-nez v1, :cond_10

    move-object v2, v3

    .line 2280261
    :goto_6
    move-object v1, v2

    .line 2280262
    goto :goto_1

    .line 2280263
    :cond_4
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2280264
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    .line 2280265
    const/4 v2, 0x0

    move v4, v2

    :goto_7
    if-ge v4, v7, :cond_6

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;

    .line 2280266
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->n()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 2280267
    new-instance p1, LX/Flv;

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result v2

    invoke-direct {p1, v0, v2}, LX/Flv;-><init>(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;Z)V

    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2280268
    :cond_5
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_7

    .line 2280269
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;LX/0Px;)V

    .line 2280270
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-nez v2, :cond_7

    move-object v2, v3

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserInvitedUsersConnectionQueryModel$FundraiserInvitedUsersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 2280271
    :cond_8
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2280272
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_8
    if-ge v4, v7, :cond_a

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2280273
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->n()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_9

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_9

    .line 2280274
    new-instance p1, LX/Flv;

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p1, v2, v0}, LX/Flv;-><init>(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;Z)V

    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2280275
    :cond_9
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_8

    .line 2280276
    :cond_a
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;LX/0Px;)V

    .line 2280277
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-nez v2, :cond_b

    move-object v2, v3

    goto/16 :goto_4

    :cond_b
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 2280278
    :cond_c
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2280279
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_9
    if-ge v4, v7, :cond_e

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;

    .line 2280280
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    if-eqz p1, :cond_d

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->n()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_d

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_d

    .line 2280281
    new-instance p1, LX/Flv;

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result v2

    invoke-direct {p1, v0, v2}, LX/Flv;-><init>(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;Z)V

    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2280282
    :cond_d
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_9

    .line 2280283
    :cond_e
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;LX/0Px;)V

    .line 2280284
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-nez v2, :cond_f

    move-object v2, v3

    goto/16 :goto_5

    :cond_f
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 2280285
    :cond_10
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2280286
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    .line 2280287
    const/4 v2, 0x0

    move v4, v2

    :goto_a
    if-ge v4, v7, :cond_12

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;

    .line 2280288
    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    if-eqz p1, :cond_11

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->n()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_11

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_11

    .line 2280289
    new-instance p1, LX/Flv;

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->b(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;Ljava/lang/String;)Z

    move-result v2

    invoke-direct {p1, v0, v2}, LX/Flv;-><init>(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;Z)V

    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2280290
    :cond_11
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_a

    .line 2280291
    :cond_12
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListDataFetcher;LX/0Px;)V

    .line 2280292
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-nez v2, :cond_13

    move-object v2, v3

    goto/16 :goto_6

    :cond_13
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
