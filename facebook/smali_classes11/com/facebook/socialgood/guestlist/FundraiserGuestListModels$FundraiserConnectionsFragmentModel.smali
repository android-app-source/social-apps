.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x839dbbd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2280543
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2280542
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2280540
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2280541
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2280525
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2280526
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2280527
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2280528
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x4329d4ea

    invoke-static {v3, v2, v4}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2280529
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2280530
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2280531
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2280532
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2280533
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2280534
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2280535
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2280536
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2280537
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2280538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2280539
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2280510
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2280511
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2280512
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4329d4ea

    invoke-static {v2, v0, v3}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2280513
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2280514
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2280515
    iput v3, v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->h:I

    move-object v1, v0

    .line 2280516
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2280517
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2280518
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2280519
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2280520
    iput-object v0, v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2280521
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2280522
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2280523
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2280524
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2280509
    new-instance v0, LX/Flb;

    invoke-direct {v0, p1}, LX/Flb;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280508
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2280504
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2280505
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->g:Z

    .line 2280506
    const/4 v0, 0x3

    const v1, 0x4329d4ea

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->h:I

    .line 2280507
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2280502
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2280503
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2280501
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2280483
    new-instance v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;-><init>()V

    .line 2280484
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2280485
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2280500
    const v0, -0x27a4d1ae

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2280499
    const v0, 0x50c72189

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280496
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2280497
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2280498
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280494
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->f:Ljava/lang/String;

    .line 2280495
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2280492
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2280493
    iget-boolean v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->g:Z

    return v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMutualFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280490
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2280491
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280488
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->i:Ljava/lang/String;

    .line 2280489
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280486
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2280487
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
