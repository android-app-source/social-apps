.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2280769
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel;

    new-instance v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2280770
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2280772
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2280773
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2280774
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2280775
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2280776
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2280777
    if-eqz p0, :cond_0

    .line 2280778
    const-string p2, "__typename"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2280779
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2280780
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2280781
    if-eqz p0, :cond_1

    .line 2280782
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2280783
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2280784
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2280785
    if-eqz p0, :cond_2

    .line 2280786
    const-string p2, "client_subscription_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2280787
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2280788
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2280789
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2280771
    check-cast p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel$Serializer;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
