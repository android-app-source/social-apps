.class public Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;
.super Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;
.source ""


# instance fields
.field public g:LX/FlR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2280341
    invoke-direct {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;-><init>()V

    .line 2280342
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280343
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->h:LX/0Ot;

    .line 2280344
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280345
    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->i:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;

    new-instance v3, LX/FlR;

    invoke-direct {v3}, LX/FlR;-><init>()V

    const-class v1, Landroid/content/Context;

    invoke-interface {v2, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, v3, LX/FlR;->b:Landroid/content/Context;

    move-object v1, v3

    check-cast v1, LX/FlR;

    const/16 v3, 0x2eb

    invoke-static {v2, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0xb19

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->g:LX/FlR;

    iput-object v3, p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->h:LX/0Ot;

    iput-object v2, p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->i:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(LX/Flv;)V
    .locals 3

    .prologue
    .line 2280351
    invoke-virtual {p1}, LX/Flv;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2280352
    iget-object v0, p1, LX/Flv;->a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-object v0, v0

    .line 2280353
    if-nez v0, :cond_1

    .line 2280354
    :cond_0
    :goto_0
    return-void

    .line 2280355
    :cond_1
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nG;

    .line 2280356
    iget-object v1, p1, LX/Flv;->a:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-object v1, v1

    .line 2280357
    invoke-virtual {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p1}, LX/Flv;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2280358
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2280359
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2280348
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2280349
    invoke-super {p0, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2280350
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2280347
    const/4 v0, 0x1

    return v0
.end method

.method public final c()LX/FlQ;
    .locals 1

    .prologue
    .line 2280346
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListFragment;->g:LX/FlR;

    return-object v0
.end method
