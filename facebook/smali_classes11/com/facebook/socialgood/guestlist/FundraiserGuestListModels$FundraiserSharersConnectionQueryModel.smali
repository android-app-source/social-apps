.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2f6794f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2281350
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2281349
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2281347
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2281348
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2281344
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2281345
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2281346
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2281342
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->g:Ljava/lang/String;

    .line 2281343
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2281332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2281333
    invoke-direct {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2281334
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2281335
    invoke-direct {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2281336
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2281337
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2281338
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2281339
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2281340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2281341
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2281324
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2281325
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2281326
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    .line 2281327
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2281328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;

    .line 2281329
    iput-object v0, v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->f:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    .line 2281330
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2281331
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2281351
    new-instance v0, LX/Flf;

    invoke-direct {v0, p1}, LX/Flf;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2281323
    invoke-direct {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2281321
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2281322
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2281320
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2281317
    new-instance v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;-><init>()V

    .line 2281318
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2281319
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2281316
    const v0, -0x163f1c5e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2281315
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFundraiserSharers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2281313
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->f:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->f:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    .line 2281314
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;->f:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$FundraiserSharersModel;

    return-object v0
.end method
