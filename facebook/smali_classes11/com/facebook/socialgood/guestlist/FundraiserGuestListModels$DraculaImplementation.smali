.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2280454
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2280455
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2280452
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2280453
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2280444
    if-nez p1, :cond_0

    .line 2280445
    :goto_0
    return v0

    .line 2280446
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2280447
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2280448
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2280449
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2280450
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2280451
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4329d4ea
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2280443
    new-instance v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2280440
    packed-switch p0, :pswitch_data_0

    .line 2280441
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2280442
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x4329d4ea
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2280439
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2280437
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->b(I)V

    .line 2280438
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2280456
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2280457
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2280458
    :cond_0
    iput-object p1, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->a:LX/15i;

    .line 2280459
    iput p2, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->b:I

    .line 2280460
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2280415
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2280411
    new-instance v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2280412
    iget v0, p0, LX/1vt;->c:I

    .line 2280413
    move v0, v0

    .line 2280414
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2280434
    iget v0, p0, LX/1vt;->c:I

    .line 2280435
    move v0, v0

    .line 2280436
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2280416
    iget v0, p0, LX/1vt;->b:I

    .line 2280417
    move v0, v0

    .line 2280418
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280419
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2280420
    move-object v0, v0

    .line 2280421
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2280422
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2280423
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2280424
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2280425
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2280426
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2280427
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2280428
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2280429
    invoke-static {v3, v9, v2}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2280430
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2280431
    iget v0, p0, LX/1vt;->c:I

    .line 2280432
    move v0, v0

    .line 2280433
    return v0
.end method
