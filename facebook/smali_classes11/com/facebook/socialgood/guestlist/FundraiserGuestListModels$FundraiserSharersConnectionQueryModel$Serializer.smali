.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2281311
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;

    new-instance v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2281312
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2281310
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2281292
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2281293
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2281294
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2281295
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2281296
    if-eqz v2, :cond_0

    .line 2281297
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2281298
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2281299
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2281300
    if-eqz v2, :cond_1

    .line 2281301
    const-string p0, "fundraiser_sharers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2281302
    invoke-static {v1, v2, p1, p2}, LX/Flr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2281303
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2281304
    if-eqz v2, :cond_2

    .line 2281305
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2281306
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2281307
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2281308
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2281309
    check-cast p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel$Serializer;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserSharersConnectionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
