.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7f2911c9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2280616
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2280631
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2280632
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2280633
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2280617
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2280618
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2280619
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2280620
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2280621
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2280622
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2280623
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2280624
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2280625
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2280626
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2280627
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;

    .line 2280628
    iput-object v0, v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2280629
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2280630
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2280609
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2280610
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2280611
    new-instance v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$DonorsModel$EdgesModel;-><init>()V

    .line 2280612
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2280613
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2280614
    const v0, 0xa30227b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2280615
    const v0, -0x88eb7d9

    return v0
.end method
