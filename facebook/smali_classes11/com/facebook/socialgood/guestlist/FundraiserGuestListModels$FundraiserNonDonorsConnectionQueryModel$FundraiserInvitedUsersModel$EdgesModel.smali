.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7f2911c9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2281100
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2281099
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2281076
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2281077
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2281093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2281094
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2281095
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2281096
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2281097
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2281098
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2281085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2281086
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2281087
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2281088
    invoke-virtual {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2281089
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;

    .line 2281090
    iput-object v0, v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2281091
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2281092
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2281083
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    .line 2281084
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;->e:Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserConnectionsFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2281080
    new-instance v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserNonDonorsConnectionQueryModel$FundraiserInvitedUsersModel$EdgesModel;-><init>()V

    .line 2281081
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2281082
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2281079
    const v0, 0x3bf28c8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2281078
    const v0, 0x16c9fe11

    return v0
.end method
