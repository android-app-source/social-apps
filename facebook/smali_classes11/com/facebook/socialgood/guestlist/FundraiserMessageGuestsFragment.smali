.class public Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;
.super Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;
.source ""

# interfaces
.implements LX/FlW;


# instance fields
.field public g:LX/Flz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2281706
    invoke-direct {p0}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;-><init>()V

    .line 2281707
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->i:Ljava/util/Set;

    .line 2281708
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;

    new-instance v0, LX/Flz;

    const-class p0, Landroid/content/Context;

    invoke-interface {v1, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-direct {v0, p0}, LX/Flz;-><init>(Landroid/content/Context;)V

    const-class p0, Landroid/content/Context;

    invoke-interface {v1, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    iput-object p0, v0, LX/Flz;->b:Landroid/content/Context;

    move-object v1, v0

    check-cast v1, LX/Flz;

    iput-object v1, p1, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->g:LX/Flz;

    return-void
.end method


# virtual methods
.method public final a(LX/Flv;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2281709
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->i:Ljava/util/Set;

    invoke-virtual {p1}, LX/Flv;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2281710
    :goto_0
    iget-object v3, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->h:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    invoke-virtual {v3}, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2281711
    if-eqz v0, :cond_1

    .line 2281712
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 2281713
    goto :goto_0

    .line 2281714
    :cond_1
    iget-object v3, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->g:LX/Flz;

    .line 2281715
    iput-boolean v2, v3, LX/Flz;->c:Z

    .line 2281716
    :cond_2
    iget-boolean v2, p1, LX/Flv;->b:Z

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p1, LX/Flv;->b:Z

    .line 2281717
    if-eqz v0, :cond_4

    .line 2281718
    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->i:Ljava/util/Set;

    invoke-virtual {p1}, LX/Flv;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2281719
    :goto_3
    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->h:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    .line 2281720
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->f:LX/Fli;

    invoke-virtual {v3}, LX/Fli;->f()LX/0Rf;

    move-result-object v3

    invoke-virtual {v3}, LX/0Rf;->size()I

    move-result v3

    iput v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    .line 2281721
    iget v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->h:I

    if-lez v3, :cond_6

    const/4 v3, 0x1

    :goto_4
    iput-boolean v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->i:Z

    .line 2281722
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->k:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    iget-boolean v4, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->i:Z

    .line 2281723
    iput-boolean v4, v3, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 2281724
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    iget-object v4, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->k:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v3, v4}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2281725
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->j:LX/1ZF;

    invoke-static {v2}, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->n(Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2281726
    const/16 p1, 0x1e

    const/4 v8, 0x0

    .line 2281727
    invoke-virtual {v2}, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->b()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2281728
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2281729
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0167

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, p1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2281730
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0899

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2281731
    :goto_5
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->h:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2281732
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->g:LX/Flz;

    .line 2281733
    iput-boolean v1, v0, LX/Flz;->c:Z

    .line 2281734
    :cond_3
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->g:LX/Flz;

    const v1, -0x13a0a0ac

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_1

    .line 2281735
    :cond_4
    iget-object v2, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->i:Ljava/util/Set;

    invoke-virtual {p1}, LX/Flv;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 2281736
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2281737
    :cond_6
    const/4 v3, 0x0

    goto :goto_4

    .line 2281738
    :cond_7
    iget-object v3, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->l:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2281739
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;

    invoke-static {v0, p0}, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2281740
    invoke-super {p0, p1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2281741
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2281742
    instance-of v1, v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    const-string v2, "Parent fragment must implement FundraiserMessageGuestsListener"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2281743
    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    iput-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->h:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    .line 2281744
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2281745
    const/4 v0, 0x0

    return v0
.end method

.method public final c()LX/FlQ;
    .locals 1

    .prologue
    .line 2281746
    iget-object v0, p0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->g:LX/Flz;

    return-object v0
.end method
