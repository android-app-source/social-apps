.class public final Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2280695
    const-class v0, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel;

    new-instance v1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2280696
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2280697
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2280678
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2280679
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2280680
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2280681
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2280682
    if-eqz v2, :cond_0

    .line 2280683
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2280684
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2280685
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2280686
    if-eqz v2, :cond_1

    .line 2280687
    const-string p0, "donors"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2280688
    invoke-static {v1, v2, p1, p2}, LX/Flm;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2280689
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2280690
    if-eqz v2, :cond_2

    .line 2280691
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2280692
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2280693
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2280694
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2280677
    check-cast p1, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel$Serializer;->a(Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserDonorsConnectionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
