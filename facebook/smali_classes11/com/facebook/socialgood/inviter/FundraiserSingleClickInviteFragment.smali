.class public Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;
.super Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->FUNDRAISER_SINGLE_CLICK_INVITE_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public f:LX/Fm2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2282059
    invoke-direct {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;-><init>()V

    .line 2282060
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2282061
    iput-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->g:LX/0Ot;

    .line 2282062
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2282063
    iput-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->h:LX/0Ot;

    .line 2282064
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2282048
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

    invoke-static {v0}, LX/Fm2;->b(LX/0QB;)LX/Fm2;

    move-result-object v3

    check-cast v3, LX/Fm2;

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xbc

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v3, v2, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->f:LX/Fm2;

    iput-object v4, v2, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->g:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->h:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->i:LX/0kL;

    iput-object v0, v2, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->j:Ljava/util/concurrent/ExecutorService;

    .line 2282049
    invoke-super {p0, p1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a(Landroid/os/Bundle;)V

    .line 2282050
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->c:LX/BXz;

    const/4 v1, 0x1

    .line 2282051
    iput-boolean v1, v0, LX/BXz;->a:Z

    .line 2282052
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2282053
    if-eqz v0, :cond_0

    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2282054
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->k:Ljava/lang/String;

    .line 2282055
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->l:Ljava/lang/String;

    .line 2282056
    const-string v1, "referral_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->m:Ljava/lang/String;

    .line 2282057
    :goto_0
    return-void

    .line 2282058
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "no_campaign_id"

    const-string v2, "Entering single click invite page with no campaign ID"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;)V
    .locals 6

    .prologue
    .line 2282044
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->f:LX/Fm2;

    iget-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->k:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->l:Ljava/lang/String;

    .line 2282045
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v1, v4, v3}, LX/Fm2;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2282046
    new-instance v1, LX/Fm6;

    invoke-direct {v1, p0, p1}, LX/Fm6;-><init>(Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;)V

    iget-object v2, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2282047
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2282030
    invoke-super {p0, p1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a(Ljava/lang/Throwable;)V

    .line 2282031
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iget-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->l:Ljava/lang/String;

    .line 2282032
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_single_click_invite_fetch_data_failure"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "fundraiser_single_click_invite"

    .line 2282033
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2282034
    move-object p0, p0

    .line 2282035
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "source"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v1, p0

    .line 2282036
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2282037
    return-void
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2282065
    const-string v0, "suggested_section_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2282066
    const v0, 0x7f08334a

    .line 2282067
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2282043
    const-string v0, "suggested_section_id"

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2282042
    const/4 v0, 0x0

    return v0
.end method

.method public final k()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2282041
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->f:LX/Fm2;

    iget-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Fm2;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2282038
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2282039
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iget-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->m:Ljava/lang/String;

    invoke-static {v1, v2, v3}, LX/BOe;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2282040
    return-void
.end method
