.class public Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""


# instance fields
.field private A:Ljava/lang/String;

.field public u:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:LX/Fm2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2281967
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    .line 2281968
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2281969
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/Fm2;->b(LX/0QB;)LX/Fm2;

    move-result-object v0

    check-cast v0, LX/Fm2;

    iput-object v3, v2, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->u:LX/0Zb;

    iput-object v4, v2, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->v:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v2, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->w:Ljava/util/concurrent/ExecutorService;

    iput-object v6, v2, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->x:LX/0kL;

    iput-object v0, v2, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->y:LX/Fm2;

    .line 2281970
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 2281971
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2281972
    if-eqz v0, :cond_0

    .line 2281973
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->z:Ljava/lang/String;

    .line 2281974
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->A:Ljava/lang/String;

    .line 2281975
    :cond_0
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->u:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->A:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/BOe;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2281976
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2281977
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2281978
    const-string v0, "suggested_section_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2281979
    const v0, 0x7f08334a

    .line 2281980
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2281981
    const/4 v0, 0x1

    return v0
.end method

.method public final p()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2281982
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->y:LX/Fm2;

    iget-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Fm2;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2281983
    const-string v0, "suggested_section_id"

    return-object v0
.end method

.method public final y()V
    .locals 4

    .prologue
    .line 2281984
    iget-object v0, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->y:LX/Fm2;

    iget-object v1, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->z:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/Fm2;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/Fm3;

    invoke-direct {v1, p0}, LX/Fm3;-><init>(Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;)V

    iget-object v2, p0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->w:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2281985
    return-void
.end method
