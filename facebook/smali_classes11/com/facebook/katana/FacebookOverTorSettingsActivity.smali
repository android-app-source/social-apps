.class public Lcom/facebook/katana/FacebookOverTorSettingsActivity;
.super Lcom/facebook/http/onion/ui/FbTorSettingsActivity;
.source ""


# instance fields
.field private b:LX/CIm;

.field private c:LX/3Py;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2400210
    invoke-direct {p0}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;-><init>()V

    return-void
.end method

.method private a(LX/CIm;LX/3Py;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2400207
    iput-object p1, p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->b:LX/CIm;

    .line 2400208
    iput-object p2, p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->c:LX/3Py;

    .line 2400209
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;

    invoke-static {v1}, LX/CIm;->b(LX/0QB;)LX/CIm;

    move-result-object v0

    check-cast v0, LX/CIm;

    invoke-static {v1}, LX/3Py;->b(LX/0QB;)LX/3Py;

    move-result-object v1

    check-cast v1, LX/3Py;

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->a(LX/CIm;LX/3Py;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2400205
    invoke-static {}, LX/0sG;->a()V

    .line 2400206
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2400211
    invoke-super {p0, p1}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->a(Landroid/os/Bundle;)V

    .line 2400212
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->requestWindowFeature(I)Z

    .line 2400213
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2400199
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->c:LX/3Py;

    .line 2400200
    iget-object v1, v0, LX/3Py;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0hM;->k:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v1, v2, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 2400201
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2400202
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->c:LX/3Py;

    invoke-virtual {v1, v0}, LX/3Py;->a(Z)V

    .line 2400203
    return-void

    .line 2400204
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2400195
    invoke-static {p0, p0}, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2400196
    iget-object v0, p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->b:LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2400197
    invoke-super {p0, p1}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->c(Landroid/os/Bundle;)V

    .line 2400198
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x286e065a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2400191
    invoke-super {p0}, Lcom/facebook/http/onion/ui/FbTorSettingsActivity;->onStart()V

    .line 2400192
    iget-object v1, p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->b:LX/CIm;

    invoke-virtual {v1, p0}, LX/CIm;->b(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2400193
    iget-object v1, p0, Lcom/facebook/katana/FacebookOverTorSettingsActivity;->b:LX/CIm;

    const v2, 0x7f083442

    invoke-virtual {v1, v2}, LX/CIm;->a(I)V

    .line 2400194
    const/16 v1, 0x23

    const v2, -0x776e2f18

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
