.class public Lcom/facebook/katana/model/FacebookSessionInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/katana/model/FacebookSessionInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2208858
    const-class v0, Lcom/facebook/katana/model/FacebookSessionInfo;

    new-instance v1, Lcom/facebook/katana/model/FacebookSessionInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/katana/model/FacebookSessionInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2208859
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2208860
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/model/FacebookSessionInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2208861
    if-nez p0, :cond_0

    .line 2208862
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2208863
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2208864
    invoke-static {p0, p1, p2}, Lcom/facebook/katana/model/FacebookSessionInfoSerializer;->b(Lcom/facebook/katana/model/FacebookSessionInfo;LX/0nX;LX/0my;)V

    .line 2208865
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2208866
    return-void
.end method

.method private static b(Lcom/facebook/katana/model/FacebookSessionInfo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2208867
    const-string v0, "username"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->username:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208868
    const-string v0, "session_key"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionKey:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208869
    const-string v0, "secret"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionSecret:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208870
    const-string v0, "access_token"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->oAuthToken:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208871
    const-string v0, "uid"

    iget-wide v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->userId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2208872
    const-string v0, "machine_id"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->machineID:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208873
    const-string v0, "error_data"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->errorData:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208874
    const-string v0, "filter"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->mFilterKey:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208875
    const-string v0, "profile"

    iget-object v1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->mMyself:Lcom/facebook/ipc/model/FacebookUser;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2208876
    const-string v0, "session_cookies"

    invoke-virtual {p0}, Lcom/facebook/katana/model/FacebookSessionInfo;->getSessionCookies()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2208877
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2208878
    check-cast p1, Lcom/facebook/katana/model/FacebookSessionInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/katana/model/FacebookSessionInfoSerializer;->a(Lcom/facebook/katana/model/FacebookSessionInfo;LX/0nX;LX/0my;)V

    return-void
.end method
