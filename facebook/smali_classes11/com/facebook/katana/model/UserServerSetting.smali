.class public Lcom/facebook/katana/model/UserServerSetting;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/model/UserServerSettingDeserializer;
.end annotation


# instance fields
.field public final mProjectName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "project"
    .end annotation
.end field

.field public final mSettingName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "setting"
    .end annotation
.end field

.field public final mValue:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "value"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2208906
    const-class v0, Lcom/facebook/katana/model/UserServerSettingDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2208907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208908
    iput-object v0, p0, Lcom/facebook/katana/model/UserServerSetting;->mProjectName:Ljava/lang/String;

    .line 2208909
    iput-object v0, p0, Lcom/facebook/katana/model/UserServerSetting;->mSettingName:Ljava/lang/String;

    .line 2208910
    iput-object v0, p0, Lcom/facebook/katana/model/UserServerSetting;->mValue:Ljava/lang/String;

    .line 2208911
    return-void
.end method
