.class public final Lcom/facebook/katana/model/FacebookApiException$ServerExceptionData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/model/FacebookApiException_ServerExceptionDataDeserializer;
.end annotation


# instance fields
.field public final mErrorCode:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_code"
    .end annotation
.end field

.field public final mErrorData:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_data"
    .end annotation
.end field

.field public final mErrorMsg:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_msg"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2208795
    const-class v0, Lcom/facebook/katana/model/FacebookApiException_ServerExceptionDataDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2208796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208797
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/katana/model/FacebookApiException$ServerExceptionData;->mErrorCode:I

    .line 2208798
    iput-object v1, p0, Lcom/facebook/katana/model/FacebookApiException$ServerExceptionData;->mErrorMsg:Ljava/lang/String;

    .line 2208799
    iput-object v1, p0, Lcom/facebook/katana/model/FacebookApiException$ServerExceptionData;->mErrorData:Ljava/lang/String;

    .line 2208800
    return-void
.end method
