.class public Lcom/facebook/katana/model/NewsFeedToggleOption;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/model/NewsFeedToggleOptionDeserializer;
.end annotation


# instance fields
.field public final script:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "script"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2208883
    const-class v0, Lcom/facebook/katana/model/NewsFeedToggleOptionDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2208879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208880
    iput-object v0, p0, Lcom/facebook/katana/model/NewsFeedToggleOption;->title:Ljava/lang/String;

    .line 2208881
    iput-object v0, p0, Lcom/facebook/katana/model/NewsFeedToggleOption;->script:Ljava/lang/String;

    .line 2208882
    return-void
.end method
