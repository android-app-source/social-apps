.class public Lcom/facebook/katana/activity/faceweb/ActionSheetButton;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/activity/faceweb/ActionSheetButtonDeserializer;
.end annotation


# instance fields
.field public final callback:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "callback"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2403000
    const-class v0, Lcom/facebook/katana/activity/faceweb/ActionSheetButtonDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2402995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2402996
    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/ActionSheetButton;->title:Ljava/lang/String;

    .line 2402997
    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/ActionSheetButton;->type:Ljava/lang/String;

    .line 2402998
    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/ActionSheetButton;->callback:Ljava/lang/String;

    .line 2402999
    return-void
.end method
