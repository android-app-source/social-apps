.class public Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# static fields
.field private static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/model/NewsFeedToggleOption;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0lp;

.field public m:Lcom/facebook/webview/FacebookWebView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2404743
    const-class v0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    sput-object v0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->j:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2404744
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 2404745
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    .line 2404746
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 2404747
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2404748
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404749
    const-string v1, "feed_filter_dismiss_script"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2404750
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404751
    const-string v1, "feed_filter_selected_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 2404752
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404753
    const-string v1, "feed_filter_buttons"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2404754
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->l:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 2404755
    new-instance v1, LX/Gv3;

    invoke-direct {v1, p0}, LX/Gv3;-><init>(Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;)V

    invoke-virtual {v0, v1}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2404756
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2404757
    new-instance v0, LX/0ju;

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f082a4f

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f082a4f

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080036

    new-instance v2, LX/Gv4;

    invoke-direct {v2, p0}, LX/Gv4;-><init>(Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2404758
    :goto_1
    return-object v0

    .line 2404759
    :catch_0
    move-exception v0

    .line 2404760
    sget-object v1, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->j:Ljava/lang/Class;

    const-string v5, "received bad faceweb data"

    invoke-static {v1, v5, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2404761
    :catch_1
    move-exception v0

    .line 2404762
    sget-object v1, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->j:Ljava/lang/Class;

    const-string v5, "received bad faceweb data"

    invoke-static {v1, v5, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2404763
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/CharSequence;

    .line 2404764
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2404765
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/model/NewsFeedToggleOption;

    iget-object v0, v0, Lcom/facebook/katana/model/NewsFeedToggleOption;->title:Ljava/lang/String;

    aput-object v0, v5, v1

    .line 2404766
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2404767
    :cond_1
    new-instance v0, LX/0ju;

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    new-instance v1, LX/Gv5;

    invoke-direct {v1, p0, v4, v3}, LX/Gv5;-><init>(Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;ILjava/lang/String;)V

    invoke-virtual {v0, v5, v1}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 2404768
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    goto :goto_1
.end method
