.class public Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# instance fields
.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/activity/faceweb/ActionSheetButton;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0lp;

.field public l:LX/03V;

.field public m:Lcom/facebook/webview/FacebookWebView;


# direct methods
.method public constructor <init>(LX/0lp;)V
    .locals 1

    .prologue
    .line 2404700
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 2404701
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    .line 2404702
    iput-object p1, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->k:LX/0lp;

    .line 2404703
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 2404704
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404705
    const-string v1, "action_sheet_buttons"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2404706
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->k:LX/0lp;

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 2404707
    new-instance v2, LX/Guz;

    invoke-direct {v2, p0}, LX/Guz;-><init>(Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;)V

    invoke-virtual {v0, v2}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2404708
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2404709
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f082a4f

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f082a4f

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080036

    new-instance v2, LX/Gv0;

    invoke-direct {v2, p0}, LX/Gv0;-><init>(Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2404710
    :goto_1
    return-object v0

    .line 2404711
    :catch_0
    move-exception v0

    .line 2404712
    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->l:LX/03V;

    const-string v3, "JSON"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not parse JSON:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2404713
    :catch_1
    move-exception v0

    .line 2404714
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->l:LX/03V;

    const-string v2, "JSON"

    const-string v3, "IOError in JSON parser"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2404715
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/CharSequence;

    .line 2404716
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2404717
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/faceweb/ActionSheetButton;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/ActionSheetButton;->title:Ljava/lang/String;

    aput-object v0, v2, v1

    .line 2404718
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2404719
    :cond_1
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    new-instance v1, LX/Gv1;

    invoke-direct {v1, p0}, LX/Gv1;-><init>(Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;)V

    invoke-virtual {v0, v2, v1}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 2404720
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2404721
    const-string v2, "action_sheet_hide_cancel"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2404722
    const v1, 0x7f083205

    new-instance v2, LX/Gv2;

    invoke-direct {v2, p0}, LX/Gv2;-><init>(Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2404723
    :cond_2
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d4c4099

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2404724
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2404725
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    iput-object v1, p0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->l:LX/03V;

    .line 2404726
    const/16 v1, 0x2b

    const v2, -0x5002e48a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
