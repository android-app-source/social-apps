.class public Lcom/facebook/katana/activity/faceweb/FacewebAssassin;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Gu1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/os/Handler;

.field private final c:LX/Gtz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2403071
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;LX/Gtz;)V
    .locals 0

    .prologue
    .line 2403031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2403032
    iput-object p1, p0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->b:Landroid/os/Handler;

    .line 2403033
    iput-object p2, p0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->c:LX/Gtz;

    .line 2403034
    return-void
.end method

.method public static a(Landroid/os/Handler;LX/Gtz;I)Lcom/facebook/katana/activity/faceweb/FacewebAssassin;
    .locals 4

    .prologue
    .line 2403067
    sget-object v0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v0}, LX/Gtz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2403068
    new-instance v0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    invoke-direct {v0, p0, p1}, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;-><init>(Landroid/os/Handler;LX/Gtz;)V

    .line 2403069
    int-to-long v2, p2

    const v1, 0x336a11e4

    invoke-static {p0, v0, v2, v3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2403070
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V
    .locals 3

    .prologue
    .line 2403064
    sget-object v1, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    monitor-enter v1

    .line 2403065
    :try_start_0
    sget-object v0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    new-instance v2, LX/Gu1;

    invoke-direct {v2, p0}, LX/Gu1;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2403066
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static final b(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V
    .locals 3

    .prologue
    .line 2403057
    sget-object v1, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    monitor-enter v1

    .line 2403058
    :try_start_0
    sget-object v0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2403059
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2403060
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gu1;

    iget-object v0, v0, LX/Gu1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403061
    if-eqz v0, :cond_1

    if-ne v0, p0, :cond_0

    .line 2403062
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2403063
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 2403035
    const/4 v0, 0x0

    .line 2403036
    sget-object v2, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    monitor-enter v2

    .line 2403037
    :try_start_0
    sget-object v1, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    .line 2403038
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 2403039
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gu1;

    .line 2403040
    iget-object v4, v0, LX/Gu1;->a:Ljava/lang/ref/WeakReference;

    if-eqz v4, :cond_4

    .line 2403041
    iget-object v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->c:LX/Gtz;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v8, v0, LX/Gu1;->b:J

    sub-long/2addr v6, v8

    invoke-interface {v4, v6, v7}, LX/Gtz;->a(J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2403042
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move-object v1, v0

    .line 2403043
    goto :goto_0

    .line 2403044
    :cond_1
    monitor-exit v2

    .line 2403045
    if-nez v1, :cond_3

    .line 2403046
    :cond_2
    :goto_2
    return-void

    .line 2403047
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2403048
    :cond_3
    iget-object v0, v1, LX/Gu1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403049
    if-eqz v0, :cond_2

    .line 2403050
    invoke-virtual {v0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->k()V

    .line 2403051
    sget-object v1, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    monitor-enter v1

    .line 2403052
    :try_start_1
    sget-object v0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2403053
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2403054
    const/4 v1, 0x3

    if-le v0, v1, :cond_2

    .line 2403055
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->b:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    const v1, -0x1e5ad370

    invoke-static {v0, p0, v2, v3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_2

    .line 2403056
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
