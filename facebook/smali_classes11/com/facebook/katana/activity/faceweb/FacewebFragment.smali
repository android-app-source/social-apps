.class public Lcom/facebook/katana/activity/faceweb/FacewebFragment;
.super Lcom/facebook/katana/fragment/BaseFacebookFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fk;


# static fields
.field public static final ae:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final af:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/regex/Pattern;


# instance fields
.field public A:Landroid/webkit/ValueCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/webkit/ValueCallback",
            "<[",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/lang/String;

.field private C:Landroid/view/View$OnClickListener;

.field public D:LX/GxZ;

.field private E:J

.field public F:J

.field public G:LX/0lp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public H:LX/GuQ;

.field public I:LX/GuR;

.field public J:Ljava/lang/String;

.field private K:LX/GuK;

.field public L:Z

.field public M:Z

.field private N:Lcom/facebook/katana/service/AppSession;

.field public O:Ljava/lang/String;

.field private P:Z

.field public Q:Ljava/lang/String;

.field public R:LX/0So;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public S:Z

.field public T:LX/0hx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public U:LX/0kv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public V:LX/0zF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public W:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public X:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public Y:LX/1ay;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public Z:LX/1Kf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public aa:LX/Guy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ab:[Lorg/json/JSONObject;

.field private ac:J

.field public ad:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ag:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ah:Landroid/view/View;

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/17d;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final j:Landroid/os/Handler;

.field public k:I

.field private final l:LX/GuY;

.field private final m:LX/Gub;

.field private final n:LX/Guj;

.field private final o:LX/Gum;

.field private final p:LX/Gun;

.field public q:J

.field public r:LX/GxF;

.field public s:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/F9m;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2U8;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2DR;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17X;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/katana/urimap/IntentHandlerUtil;",
            ">;"
        }
    .end annotation
.end field

.field public z:Landroid/webkit/ValueCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2404373
    const-string v0, "/groups/[^/]+/?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->i:Ljava/util/regex/Pattern;

    .line 2404374
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2404375
    sput-object v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ae:Ljava/util/Set;

    const-string v1, "fw_photo_uploaded"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2404376
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->af:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 2404377
    invoke-direct {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;-><init>()V

    .line 2404378
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    .line 2404379
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->k:I

    .line 2404380
    new-instance v0, LX/GuY;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LX/GuY;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->l:LX/GuY;

    .line 2404381
    new-instance v0, LX/Gub;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LX/Gub;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->m:LX/Gub;

    .line 2404382
    new-instance v0, LX/Guj;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LX/Guj;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->n:LX/Guj;

    .line 2404383
    new-instance v0, LX/Gum;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LX/Gum;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->o:LX/Gum;

    .line 2404384
    new-instance v0, LX/Gun;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LX/Gun;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->p:LX/Gun;

    .line 2404385
    iput-wide v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->q:J

    .line 2404386
    iput-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404387
    iput-wide v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->E:J

    .line 2404388
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    .line 2404389
    sget-object v0, LX/GuQ;->TITLE_BUTTON:LX/GuQ;

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->H:LX/GuQ;

    .line 2404390
    iput-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    .line 2404391
    iput-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    .line 2404392
    iput-wide v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ac:J

    .line 2404393
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ad:Ljava/util/Map;

    .line 2404394
    return-void
.end method

.method private A()Z
    .locals 3

    .prologue
    .line 2404395
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404396
    const-string v1, "arg_is_checkpoint"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static synthetic A(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z
    .locals 1

    .prologue
    .line 2404397
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->C()Z

    move-result v0

    return v0
.end method

.method public static B(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2404398
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2404399
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2404400
    const-string v2, "arg_is_blocking_checkpoint"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private C()Z
    .locals 3

    .prologue
    .line 2404275
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404276
    const-string v1, "faceweb_nfx"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static D(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2404401
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->t()Landroid/content/Intent;

    move-result-object v1

    .line 2404402
    if-nez v1, :cond_0

    .line 2404403
    :goto_0
    return v0

    :cond_0
    const-string v2, "faceweb_help_center"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 2404404
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 2404405
    :goto_0
    return-wide v0

    .line 2404406
    :catch_0
    :goto_1
    const-wide/16 v0, -0x1

    goto :goto_0

    :catch_1
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;ZZ)Lcom/facebook/katana/activity/faceweb/FacewebFragment;
    .locals 3

    .prologue
    .line 2404407
    new-instance v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-direct {v0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;-><init>()V

    .line 2404408
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2404409
    const-string v2, "mobile_page"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404410
    const-string v2, "faceweb_modal"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404411
    const-string v2, "titlebar_with_modal_done"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404412
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2404413
    return-object v0
.end method

.method public static a(Ljava/lang/String;ZZZZZLjava/lang/String;)Lcom/facebook/katana/activity/faceweb/FacewebFragment;
    .locals 3
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2404414
    new-instance v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-direct {v0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;-><init>()V

    .line 2404415
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2404416
    const-string v2, "mobile_page"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404417
    const-string v2, "faceweb_modal"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404418
    const-string v2, "titlebar_with_modal_done"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404419
    const-string v2, "uri_unhandled_report_category_name"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404420
    const-string v2, "arg_is_checkpoint"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404421
    const-string v2, "arg_is_blocking_checkpoint"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404422
    const-string v2, "faceweb_nfx"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404423
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2404424
    return-object v0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 8

    .prologue
    .line 2404425
    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    .line 2404426
    const-string v0, "profiles"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2404427
    const-string v0, "profiles"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v1

    .line 2404428
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 2404429
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 2404430
    :try_start_0
    const-string v0, "action"

    const-string v4, "didPickFriends"

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2404431
    array-length v4, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-wide v6, v1, v0

    .line 2404432
    invoke-virtual {v3, v6, v7}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    .line 2404433
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2404434
    :cond_0
    const-string v0, "pickedFriends"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2404435
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2404436
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2404437
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2404438
    const/4 v1, 0x2

    .line 2404439
    invoke-virtual {p0, v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a(I)Landroid/support/v4/app/DialogFragment;

    move-result-object v2

    .line 2404440
    const-string v3, "Cannot create dialog for %s. Check onCreateDialogFragment(int) method"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2404441
    invoke-static {}, LX/0sL;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2404442
    invoke-static {v3, v2}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2404443
    :cond_1
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v3, v3

    .line 2404444
    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v4

    .line 2404445
    invoke-static {v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->g(I)Ljava/lang/String;

    move-result-object v5

    .line 2404446
    invoke-virtual {v3, v5}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 2404447
    if-eqz v3, :cond_2

    .line 2404448
    invoke-virtual {v4, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2404449
    :cond_2
    const/16 v3, 0x1001

    invoke-virtual {v4, v3}, LX/0hH;->a(I)LX/0hH;

    .line 2404450
    const/4 v3, 0x1

    invoke-virtual {v2, v4, v5, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 2404451
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    .line 2404452
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0hx;->a(Z)V

    .line 2404453
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    new-instance v3, LX/Guc;

    invoke-direct {v3, p0}, LX/Guc;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;Ljava/util/List;LX/BWL;)V

    .line 2404454
    :cond_3
    :goto_2
    return-void

    .line 2404455
    :catch_0
    move-exception v0

    .line 2404456
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v1

    const-string v3, "inconceivable exception"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2404457
    :cond_4
    const/4 v0, 0x6

    .line 2404458
    invoke-virtual {p0, v0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a(I)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    .line 2404459
    const-string v2, "Cannot create dialog for %s. Check onCreateDialogFragment(int) method"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2404460
    invoke-static {}, LX/0sL;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2404461
    invoke-static {v2, v1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2404462
    :cond_5
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 2404463
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    .line 2404464
    invoke-static {v0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->g(I)Ljava/lang/String;

    move-result-object v4

    .line 2404465
    invoke-virtual {v2, v4}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2404466
    if-eqz v2, :cond_6

    .line 2404467
    invoke-virtual {v3, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2404468
    :cond_6
    const/16 v2, 0x1001

    invoke-virtual {v3, v2}, LX/0hH;->a(I)LX/0hH;

    .line 2404469
    const/4 v2, 0x1

    invoke-virtual {v1, v3, v4, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 2404470
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    const-string v1, "FacewebError"

    const-string v2, "Invite friend callback unset."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private a(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 2404471
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "setToolbarSegments"

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->m:LX/Gub;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404472
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showCommentPublisher"

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->n:LX/Guj;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404473
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showCheckinComposer"

    new-instance v2, LX/Guh;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/Guh;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404474
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showFriendPicker"

    new-instance v2, LX/Guk;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/Guk;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404475
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "addNativeEventListener"

    new-instance v2, LX/GuE;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuE;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404476
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showMsgComposer"

    new-instance v2, LX/Gul;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/Gul;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404477
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "callCell"

    new-instance v2, LX/GuG;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    const-string v4, "android.intent.action.DIAL"

    const-string v5, "tel:"

    invoke-direct {v2, p0, v3, v4, v5}, LX/GuG;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404478
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "callSMS"

    new-instance v2, LX/GuG;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    const-string v4, "android.intent.action.VIEW"

    const-string v5, "sms:"

    invoke-direct {v2, p0, v3, v4, v5}, LX/GuG;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404479
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showActionSheet"

    new-instance v2, LX/Gud;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/Gud;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404480
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "uploadPhoto"

    new-instance v2, LX/Guu;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/Guu;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404481
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showStatusComposer"

    new-instance v2, LX/Guo;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/Guo;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404482
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showShareComposer"

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->p:LX/Gun;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404483
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showMsgReplyPublisher"

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->o:LX/Gum;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404484
    new-instance v0, LX/Guv;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LX/Guv;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    .line 2404485
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v2, "pageLoading"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404486
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v2, "pageLoaded"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404487
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "setNavBarButton"

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->l:LX/GuY;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404488
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "setActionMenu"

    new-instance v2, LX/GuS;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuS;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404489
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showPickerView"

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->K:LX/GuK;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404490
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "enablePullToRefresh"

    new-instance v2, LX/GuA;

    invoke-direct {v2, p0}, LX/GuA;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404491
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "close"

    new-instance v2, LX/GuI;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuI;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404492
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "back"

    new-instance v2, LX/GuH;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuH;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404493
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, LX/GxF;->setScrollBarStyle(I)V

    .line 2404494
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "setNavBarHidden"

    new-instance v2, LX/GuZ;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuZ;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404495
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "dismissModalDialog"

    new-instance v2, LX/GuJ;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuJ;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404496
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "showAlert"

    new-instance v2, LX/Gug;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/Gug;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404497
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "openInNewWebView"

    new-instance v2, LX/GuP;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuP;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404498
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "hideSoftKeyboard"

    new-instance v2, LX/GuO;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuO;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404499
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "nativethirdparty"

    new-instance v2, LX/BWZ;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, v3}, LX/BWZ;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404500
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "getDevicePhoneNumber"

    new-instance v2, LX/GuL;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuL;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404501
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "backgroundConfirmationAddPendingContactpoint"

    new-instance v2, LX/GuF;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuF;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404502
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "getEmailAddresses"

    new-instance v2, LX/GuM;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuM;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404503
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "getInstallerData"

    new-instance v2, LX/GuN;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuN;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404504
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "refreshZeroToken"

    new-instance v2, LX/GuB;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, LX/GuB;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404505
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const-string v1, "switchToDialtone"

    new-instance v2, LX/GuC;

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v2, p0, v3, p1}, LX/GuC;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404506
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->g()V

    .line 2404507
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2404509
    const/4 v0, 0x2

    .line 2404510
    invoke-virtual {p0, v0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a(I)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    .line 2404511
    const-string v2, "Cannot create dialog for %s. Check onCreateDialogFragment(int) method"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2404512
    invoke-static {}, LX/0sL;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2404513
    invoke-static {v2, v1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2404514
    :cond_0
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 2404515
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    .line 2404516
    invoke-static {v0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->g(I)Ljava/lang/String;

    move-result-object v4

    .line 2404517
    invoke-virtual {v2, v4}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2404518
    if-eqz v2, :cond_1

    .line 2404519
    invoke-virtual {v3, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2404520
    :cond_1
    const/16 v2, 0x1001

    invoke-virtual {v3, v2}, LX/0hH;->a(I)LX/0hH;

    .line 2404521
    const/4 v2, 0x1

    invoke-virtual {v1, v3, v4, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 2404522
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    .line 2404523
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0hx;->a(Z)V

    .line 2404524
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2404525
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    .line 2404526
    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2404527
    new-instance v2, LX/Gu3;

    invoke-direct {v2, p0}, LX/Gu3;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2404528
    return-void
.end method

.method private static a(Lcom/facebook/katana/activity/faceweb/FacewebFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/17d;LX/03V;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0lp;LX/0So;LX/0hx;LX/0kv;LX/0zF;Ljava/util/concurrent/ScheduledExecutorService;LX/0Xl;LX/1ay;LX/1Kf;LX/Guy;Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/activity/faceweb/FacewebFragment;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/17d;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/F9m;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2U8;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2DR;",
            ">;",
            "LX/0Or",
            "<",
            "LX/17X;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/katana/urimap/IntentHandlerUtil;",
            ">;",
            "LX/0lp;",
            "LX/0So;",
            "LX/0hx;",
            "LX/0kv;",
            "LX/0zF;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Xl;",
            "LX/1ay;",
            "LX/1Kf;",
            "LX/Guy;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2404508
    iput-object p1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->g:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->h:LX/17d;

    iput-object p9, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    iput-object p10, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->t:LX/0Or;

    iput-object p11, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->u:LX/0Or;

    iput-object p12, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->v:LX/0Or;

    iput-object p13, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->w:LX/0Or;

    iput-object p14, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->x:LX/0Or;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->y:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->G:LX/0lp;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->U:LX/0kv;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->V:LX/0zF;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->W:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->X:LX/0Xl;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Y:LX/1ay;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Z:LX/1Kf;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->aa:LX/Guy;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 29

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v28

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/16 v3, 0x2fd

    move-object/from16 v0, v28

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x271

    move-object/from16 v0, v28

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x19c6

    move-object/from16 v0, v28

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xc49

    move-object/from16 v0, v28

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x455

    move-object/from16 v0, v28

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1a1

    move-object/from16 v0, v28

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x97

    move-object/from16 v0, v28

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v28 .. v28}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v10

    check-cast v10, LX/17d;

    invoke-static/range {v28 .. v28}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    const/16 v12, 0x24d5

    move-object/from16 v0, v28

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x2ba

    move-object/from16 v0, v28

    invoke-static {v0, v13}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x3ea

    move-object/from16 v0, v28

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0xc18

    move-object/from16 v0, v28

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v16, 0xc49

    move-object/from16 v0, v28

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0xc4b

    move-object/from16 v0, v28

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {v28 .. v28}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v18

    check-cast v18, LX/0lp;

    invoke-static/range {v28 .. v28}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v19

    check-cast v19, LX/0So;

    invoke-static/range {v28 .. v28}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v20

    check-cast v20, LX/0hx;

    invoke-static/range {v28 .. v28}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v21

    check-cast v21, LX/0kv;

    invoke-static/range {v28 .. v28}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v22

    check-cast v22, LX/0zF;

    invoke-static/range {v28 .. v28}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v23

    check-cast v23, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {v28 .. v28}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v24

    check-cast v24, LX/0Xl;

    invoke-static/range {v28 .. v28}, LX/1ay;->a(LX/0QB;)LX/1ay;

    move-result-object v25

    check-cast v25, LX/1ay;

    invoke-static/range {v28 .. v28}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v26

    check-cast v26, LX/1Kf;

    invoke-static/range {v28 .. v28}, LX/Guy;->a(LX/0QB;)LX/Guy;

    move-result-object v27

    check-cast v27, LX/Guy;

    invoke-static/range {v28 .. v28}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v28

    check-cast v28, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {v2 .. v28}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Lcom/facebook/katana/activity/faceweb/FacewebFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/17d;LX/03V;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0lp;LX/0So;LX/0hx;LX/0kv;LX/0zF;Ljava/util/concurrent/ScheduledExecutorService;LX/0Xl;LX/1ay;LX/1Kf;LX/Guy;Lcom/facebook/performancelogger/PerformanceLogger;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Lcom/facebook/ui/titlebar/Fb4aTitleBar;LX/1ZF;)V
    .locals 5

    .prologue
    .line 2404620
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2404621
    invoke-virtual {p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->d()V

    .line 2404622
    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->B(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2404623
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->C:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    .line 2404624
    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2404625
    :cond_0
    :goto_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f0209aa

    .line 2404626
    iput v1, v0, LX/108;->i:I

    .line 2404627
    move-object v0, v0

    .line 2404628
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2404629
    new-instance v1, LX/Gu5;

    invoke-direct {v1, p0}, LX/Gu5;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2404630
    if-eqz p2, :cond_2

    .line 2404631
    const-string v2, ""

    invoke-interface {p2, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2404632
    invoke-interface {p2, v0}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2404633
    new-instance v0, LX/Gu6;

    invoke-direct {v0, p0, v1}, LX/Gu6;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;LX/107;)V

    .line 2404634
    invoke-interface {p2, v0}, LX/1ZF;->a(LX/63W;)V

    .line 2404635
    :goto_1
    return-void

    .line 2404636
    :cond_1
    new-instance v0, LX/Gu4;

    invoke-direct {v0, p0, p2, p1}, LX/Gu4;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;LX/1ZF;Lcom/facebook/ui/titlebar/Fb4aTitleBar;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->C:Landroid/view/View$OnClickListener;

    .line 2404637
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment$13;

    invoke-direct {v1, p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment$13;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    const-wide/16 v2, 0xbb8

    const v4, -0x7276b566

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 2404638
    :cond_2
    const-string v2, ""

    invoke-virtual {p1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2404639
    invoke-virtual {p1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2404640
    invoke-virtual {p1, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    goto :goto_1
.end method

.method public static synthetic b(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z
    .locals 1

    .prologue
    .line 2404614
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A()Z

    move-result v0

    return v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 2404615
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404616
    const-string v1, "mobile_page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2404617
    if-eqz v0, :cond_0

    const-string v1, "/events/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2404618
    :cond_0
    :goto_0
    return-void

    .line 2404619
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Z:LX/1Kf;

    const/16 v1, 0x6dc

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Kf;->a(ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    .line 2404602
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2404603
    invoke-static {v0}, LX/0sL;->a(Ljava/lang/Object;)V

    .line 2404604
    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2404605
    if-eqz v1, :cond_1

    iget-boolean v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->P:Z

    if-nez v2, :cond_1

    .line 2404606
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    iget v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->k:I

    if-eq v2, v3, :cond_0

    .line 2404607
    iget v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->k:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2404608
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 2404609
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->l:LX/GuY;

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1, v0, v2}, LX/GuY;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V

    .line 2404610
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->m:LX/Gub;

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1, v0, v2}, LX/GuX;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V

    .line 2404611
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->n:LX/Guj;

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1, v0, v2}, LX/GuX;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V

    .line 2404612
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->o:LX/Gum;

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1, v0, v2}, LX/GuX;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V

    .line 2404613
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 2404591
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404592
    const-string v1, "no_title"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2404593
    :cond_0
    :goto_0
    return-void

    .line 2404594
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->V:LX/0zF;

    .line 2404595
    iget-object v1, v0, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v1, v1

    .line 2404596
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2404597
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2404598
    if-eqz v0, :cond_2

    .line 2404599
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->B:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_0

    .line 2404600
    :cond_2
    if-eqz v1, :cond_0

    .line 2404601
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private r()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2404535
    iget-wide v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ac:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ac:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x493e0

    cmp-long v0, v4, v6

    if-lez v0, :cond_9

    move v0, v1

    .line 2404536
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v5

    .line 2404537
    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    if-nez v3, :cond_a

    .line 2404538
    new-instance v3, LX/GxZ;

    invoke-direct {v3, v5}, LX/GxZ;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404539
    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    new-instance v4, LX/Gu8;

    invoke-direct {v4, p0}, LX/Gu8;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2404540
    iput-object v4, v3, LX/GxZ;->q:LX/Gu8;

    .line 2404541
    move v3, v1

    move v4, v2

    .line 2404542
    :goto_1
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->y()V

    .line 2404543
    if-eqz v3, :cond_0

    .line 2404544
    invoke-direct {p0, v5}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Landroid/app/Activity;)V

    .line 2404545
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->C()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2404546
    iget-object v5, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->aa:LX/Guy;

    .line 2404547
    iget-object v6, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v6, v6

    .line 2404548
    iget-object v7, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404549
    iput-object v6, v5, LX/Guy;->j:LX/0gc;

    .line 2404550
    const-string v8, "addUniversalFeedbackToken"

    invoke-virtual {v7, v8, v5}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2404551
    :cond_0
    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404552
    iget-object v5, v4, LX/GxZ;->y:LX/GxY;

    sget-object v6, LX/GxY;->CONTENT_STATE_WEBVIEW:LX/GxY;

    if-eq v5, v6, :cond_d

    iget-object v5, v4, LX/GxZ;->y:LX/GxY;

    sget-object v6, LX/GxY;->CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

    if-eq v5, v6, :cond_d

    const/4 v5, 0x1

    :goto_2
    move v4, v5

    .line 2404553
    if-eqz v4, :cond_b

    move v4, v1

    .line 2404554
    :goto_3
    if-nez v3, :cond_1

    if-nez v0, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    move v2, v1

    .line 2404555
    :cond_2
    if-eqz v2, :cond_c

    .line 2404556
    if-eqz v4, :cond_3

    .line 2404557
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    sget-object v2, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    invoke-virtual {v0, v2}, LX/GxZ;->a(LX/GxY;)V

    .line 2404558
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    invoke-virtual {v0, v1}, LX/0hx;->a(Z)V

    .line 2404559
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->N:Lcom/facebook/katana/service/AppSession;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 2404560
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/katana/service/AppSession;->b()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v3

    if-nez v3, :cond_e

    .line 2404561
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240001

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FacewebChromeLoad."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2404562
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x0

    .line 2404563
    invoke-static {v0}, LX/2yq;->a(Landroid/net/Uri;)LX/2yq;

    move-result-object v3

    .line 2404564
    if-nez v3, :cond_f

    .line 2404565
    :cond_5
    :goto_5
    move v0, v2

    .line 2404566
    if-eqz v0, :cond_6

    .line 2404567
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 2404568
    new-instance v2, Lcom/facebook/katana/activity/faceweb/FacewebFragment$3;

    invoke-direct {v2, p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment$3;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    const-wide/16 v4, 0x2710

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 2404569
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 2404570
    new-instance v3, LX/Gu9;

    invoke-direct {v3, p0, v0, v2}, LX/Gu9;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Ljava/util/Timer;LX/0gc;)V

    invoke-virtual {v2, v3}, LX/0gc;->a(LX/0fN;)V

    .line 2404571
    :cond_6
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    const-string v2, ".*home\\.php.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2404572
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    const-string v2, "fb4a_displaying_faceweb_feed"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "href: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", intent extras: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404573
    :cond_7
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/GxF;->a(Ljava/lang/String;Z)V

    .line 2404574
    :cond_8
    :goto_6
    return-void

    :cond_9
    move v0, v2

    .line 2404575
    goto/16 :goto_0

    :cond_a
    move v3, v2

    move v4, v1

    .line 2404576
    goto/16 :goto_1

    :cond_b
    move v4, v2

    .line 2404577
    goto/16 :goto_3

    .line 2404578
    :cond_c
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-eqz v0, :cond_8

    .line 2404579
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 2404580
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2404581
    invoke-static {v0}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2404582
    if-eqz v0, :cond_8

    .line 2404583
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacewebPageSession:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2404584
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240006

    invoke-interface {v1, v2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;)V

    goto :goto_6

    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2404585
    :cond_e
    invoke-virtual {v0}, Lcom/facebook/katana/service/AppSession;->b()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/katana/model/FacebookSessionInfo;->getSessionCookies()Ljava/util/List;

    move-result-object v3

    .line 2404586
    if-eqz v3, :cond_4

    .line 2404587
    const-string v4, "https://%s/"

    .line 2404588
    invoke-static {v2, v4}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2404589
    invoke-static {v2, v4, v3}, Lcom/facebook/webview/FacebookWebView;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2404590
    invoke-virtual {v0}, Lcom/facebook/katana/service/AppSession;->b()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/katana/model/FacebookSessionInfo;->b()V

    goto/16 :goto_4

    :cond_f
    invoke-static {v3}, LX/2yp;->b(LX/2yq;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v3}, LX/2yp;->a(LX/2yq;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v2, 0x1

    goto/16 :goto_5
.end method

.method public static s(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V
    .locals 1

    .prologue
    .line 2404529
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-nez v0, :cond_1

    .line 2404530
    :cond_0
    :goto_0
    return-void

    .line 2404531
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->B:Ljava/lang/String;

    .line 2404532
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2404533
    if-eqz v0, :cond_0

    .line 2404534
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->q()V

    goto :goto_0
.end method

.method private t()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2404365
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2404366
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2404367
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2404368
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2404369
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->t()Landroid/content/Intent;

    move-result-object v0

    .line 2404370
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2404371
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2404372
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 2404070
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    if-eqz v0, :cond_0

    .line 2404071
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    invoke-virtual {v0}, LX/GxZ;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2404072
    if-eqz v0, :cond_0

    .line 2404073
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2404074
    :cond_0
    return-void
.end method

.method private w()V
    .locals 3

    .prologue
    .line 2404075
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404076
    iget-object v1, v0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, v0, LX/GxZ;->B:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 2404077
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->onResume()V

    .line 2404078
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 2404079
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->onPause()V

    .line 2404080
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404081
    iget-object v1, v0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object p0, v0, LX/GxZ;->B:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 2404082
    return-void
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 2404083
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2404084
    const v1, 0x7f0d11fc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2404085
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    invoke-virtual {v1}, LX/GxZ;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2404086
    if-nez v1, :cond_1

    .line 2404087
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2404088
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404089
    iget-object v1, v0, LX/GxZ;->m:LX/GxF;

    move-object v0, v1

    .line 2404090
    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404091
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    new-instance v1, LX/GuD;

    invoke-direct {v1, p0}, LX/GuD;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/webview/FacebookWebView;->setFileChooserChromeClient(LX/BWI;)V

    .line 2404092
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Q:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2404093
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    new-instance v1, LX/Gu2;

    invoke-direct {v1, p0}, LX/Gu2;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2404094
    iput-object v1, v0, LX/GxF;->D:LX/Gu2;

    .line 2404095
    :cond_0
    return-void

    .line 2404096
    :cond_1
    if-ne v1, v0, :cond_3

    const/4 v0, 0x1

    .line 2404097
    :goto_1
    invoke-static {}, LX/0sL;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2404098
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 2404099
    :cond_2
    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static z(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V
    .locals 2

    .prologue
    .line 2404100
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->V:LX/0zF;

    .line 2404101
    iget-object v1, v0, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v1

    .line 2404102
    if-eqz v0, :cond_0

    .line 2404103
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2404104
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/DialogFragment;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2404105
    packed-switch p1, :pswitch_data_0

    .line 2404106
    invoke-super {p0, p1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a(I)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2404107
    :pswitch_0
    const v0, 0x7f083159

    .line 2404108
    new-instance v1, Lcom/facebook/katana/fragment/dialog/AlertDialogFragment;

    invoke-direct {v1}, Lcom/facebook/katana/fragment/dialog/AlertDialogFragment;-><init>()V

    .line 2404109
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2404110
    const-string v3, "message_res_id"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2404111
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2404112
    move-object v0, v1

    .line 2404113
    goto :goto_0

    .line 2404114
    :pswitch_1
    const/4 v0, -0x1

    .line 2404115
    if-ne p1, v4, :cond_1

    .line 2404116
    const v0, 0x7f083155

    .line 2404117
    :cond_0
    :goto_1
    if-ne p1, v4, :cond_3

    .line 2404118
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    invoke-virtual {v1, v3}, LX/0hx;->a(Z)V

    .line 2404119
    :goto_2
    invoke-static {v0, v3, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    goto :goto_0

    .line 2404120
    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    .line 2404121
    const v0, 0x7f083154

    goto :goto_1

    .line 2404122
    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 2404123
    const v0, 0x7f083153

    goto :goto_1

    .line 2404124
    :cond_3
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    invoke-virtual {v1, v2}, LX/0hx;->a(Z)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2404125
    const-string v0, "faceweb_view"

    return-object v0
.end method

.method public final a(LX/HeR;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2404126
    iget v0, p1, LX/HeR;->c:I

    move v0, v0

    .line 2404127
    sparse-switch v0, :sswitch_data_0

    .line 2404128
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 2404129
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 2404130
    add-int/lit16 v1, v0, 0x7d0

    .line 2404131
    iget v2, p1, LX/HeR;->c:I

    move v2, v2

    .line 2404132
    if-ne v2, v1, :cond_1

    .line 2404133
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    aget-object v2, v2, v0

    const-string v3, "callback"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2404134
    invoke-virtual {v1, v2, v4}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2404135
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2404136
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    sget-object v1, LX/CIp;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404137
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    invoke-virtual {v0}, LX/GxZ;->c()V

    goto :goto_0

    .line 2404138
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    sget-object v1, LX/CIp;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404139
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Faceweb href: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 2404140
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Faceweb href: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2404141
    :sswitch_2
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    sget-object v1, LX/CIp;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404142
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    .line 2404143
    iget-object v1, v0, LX/GuR;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2404144
    if-eqz v0, :cond_0

    .line 2404145
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    .line 2404146
    iget-object v2, v1, LX/GuR;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2404147
    invoke-virtual {v0, v1, v4}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2404148
    goto :goto_0

    .line 2404149
    :cond_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x834 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2404150
    invoke-super {p0, p1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a(Landroid/os/Bundle;)V

    .line 2404151
    const-class v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {v0, p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2404152
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x240008

    const-string v3, "FWFragmentCreate"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2404153
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0030

    const-string v2, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 2404154
    if-nez p1, :cond_0

    .line 2404155
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->o()V

    .line 2404156
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2404157
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    .line 2404158
    if-nez v0, :cond_0

    .line 2404159
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404160
    const-string v1, "mobile_page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2404161
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2404162
    const-string v0, "/home.php"

    .line 2404163
    :cond_0
    const-string v1, "mobile_page"

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2404164
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2404165
    :goto_0
    return-void

    .line 2404166
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17X;

    .line 2404167
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2404168
    if-nez v0, :cond_1

    .line 2404169
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2404170
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2404171
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->g:LX/0Ot;

    invoke-static {v1, v0, v2}, LX/17d;->a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;)V

    .line 2404172
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2404173
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-eqz v0, :cond_0

    .line 2404174
    const-string v0, "FacewebUrl"

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    .line 2404175
    :goto_0
    return-object v0

    .line 2404176
    :cond_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2404177
    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2404178
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->v()V

    .line 2404179
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404180
    iget-object v2, v0, LX/GxZ;->m:LX/GxF;

    move-object v0, v2

    .line 2404181
    if-eqz v0, :cond_0

    .line 2404182
    invoke-virtual {v0}, LX/GxF;->destroy()V

    .line 2404183
    :cond_0
    iput-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D:LX/GxZ;

    .line 2404184
    iput-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404185
    return-void
.end method

.method public final lW_()V
    .locals 6

    .prologue
    const/16 v4, 0x64

    const/4 v2, 0x0

    .line 2404186
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->d()LX/2A2;

    move-result-object v0

    .line 2404187
    instance-of v1, v0, LX/0l1;

    if-nez v1, :cond_1

    .line 2404188
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Activity is not an instance of CustomMenuHandler, not adding menu options"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404189
    :cond_0
    return-void

    .line 2404190
    :cond_1
    check-cast v0, LX/0l1;

    .line 2404191
    iget-boolean v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->M:Z

    if-eqz v1, :cond_2

    .line 2404192
    const v1, 0x7f083158

    const v3, 0x7f021442

    invoke-virtual {v0, v4, v1, v3}, LX/0l1;->a(III)V

    .line 2404193
    iget-boolean v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->S:Z

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v4, v1}, LX/0l1;->a(IZ)V

    .line 2404194
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    .line 2404195
    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v3, v1, :cond_3

    .line 2404196
    const/16 v1, 0x65

    const-string v3, "Faceweb!"

    const v4, 0x7f020d12

    invoke-virtual {v0, v1, v3, v4}, LX/0l1;->a(ILjava/lang/String;I)V

    .line 2404197
    :cond_3
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    if-eqz v1, :cond_4

    .line 2404198
    const/16 v1, 0x834

    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    .line 2404199
    iget-object v4, v3, LX/GuR;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2404200
    iget-object v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    .line 2404201
    iget v5, v4, LX/GuR;->a:I

    move v4, v5

    .line 2404202
    invoke-virtual {v0, v1, v3, v4}, LX/0l1;->a(ILjava/lang/String;I)V

    .line 2404203
    :cond_4
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    if-eqz v1, :cond_0

    .line 2404204
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    array-length v1, v1

    if-ge v2, v1, :cond_0

    .line 2404205
    add-int/lit16 v1, v2, 0x7d0

    .line 2404206
    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    aget-object v3, v3, v2

    .line 2404207
    const-string v4, "icon"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2404208
    const-string v4, "title"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "icon"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v1, v4, v3}, LX/0l1;->a(ILjava/lang/String;I)V

    .line 2404209
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v1, v2

    .line 2404210
    goto :goto_0

    .line 2404211
    :cond_6
    const-string v4, "title"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f021444

    invoke-virtual {v0, v1, v3, v4}, LX/0l1;->a(ILjava/lang/String;I)V

    goto :goto_2
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2404212
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-eqz v0, :cond_0

    .line 2404213
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->d()V

    .line 2404214
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2a

    const v3, 0x78f944e2

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2404215
    invoke-super {p0, p1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2404216
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->N:Lcom/facebook/katana/service/AppSession;

    .line 2404217
    if-eqz p1, :cond_0

    .line 2404218
    const-string v0, "PROFILE_ID"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->E:J

    .line 2404219
    const-string v0, "publisher_callback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    .line 2404220
    const-string v0, "native_event_listener_keys"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2404221
    const-string v0, "native_event_listener_values"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2404222
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v0, v5, :cond_0

    move v0, v1

    .line 2404223
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 2404224
    iget-object v5, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ad:Ljava/util/Map;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2404225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2404226
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404227
    const-string v3, "mobile_page"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    .line 2404228
    const-string v3, "parent_control_title_bar"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->P:Z

    .line 2404229
    const-string v3, "uri_unhandled_report_category_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Q:Ljava/lang/String;

    .line 2404230
    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2404231
    const-string v3, "/home.php"

    iput-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    .line 2404232
    :cond_1
    iget-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x240001

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FacewebChromeLoad."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->O:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2404233
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->d()LX/2A2;

    move-result-object v3

    .line 2404234
    const-string v4, "faceweb_modal"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    .line 2404235
    iput v8, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->k:I

    .line 2404236
    :cond_2
    const-string v1, "hide_drop_shadow"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2404237
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2404238
    if-eqz v0, :cond_3

    .line 2404239
    const v1, 0x7f0d11fd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2404240
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2404241
    :cond_3
    new-instance v0, LX/GuK;

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LX/GuK;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->K:LX/GuK;

    .line 2404242
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ac:J

    .line 2404243
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    new-instance v1, LX/Gu7;

    invoke-direct {v1, p0}, LX/Gu7;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    const v3, 0x88b8

    invoke-static {v0, v1, v3}, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a(Landroid/os/Handler;LX/Gtz;I)Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    .line 2404244
    const v0, -0x3801a05a

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2404245
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2404246
    if-nez p2, :cond_3

    .line 2404247
    const/16 v0, 0xb

    if-ne p1, v0, :cond_1

    .line 2404248
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->p:LX/Gun;

    invoke-virtual {v0}, LX/Gun;->a()V

    .line 2404249
    :cond_0
    :goto_0
    return-void

    .line 2404250
    :cond_1
    const/16 v0, 0xc

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_2

    .line 2404251
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z:Landroid/webkit/ValueCallback;

    invoke-interface {v0, v1}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 2404252
    iput-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z:Landroid/webkit/ValueCallback;

    goto :goto_0

    .line 2404253
    :cond_2
    const/16 v0, 0xd

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_0

    .line 2404254
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    invoke-interface {v0, v1}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 2404255
    iput-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    goto :goto_0

    .line 2404256
    :cond_3
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 2404257
    :sswitch_0
    const-string v0, "is_uploading_media"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2404258
    if-nez v0, :cond_0

    .line 2404259
    invoke-direct {p0, p3}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 2404260
    :sswitch_1
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2404261
    invoke-direct {p0, p3}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Landroid/content/Intent;)V

    .line 2404262
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->p:LX/Gun;

    invoke-virtual {v0}, LX/Gun;->a()V

    goto :goto_0

    .line 2404263
    :sswitch_2
    invoke-direct {p0, p2, p3}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 2404264
    :sswitch_3
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_0

    .line 2404265
    if-eqz p3, :cond_5

    const/4 v0, -0x1

    if-eq p2, v0, :cond_6

    :cond_5
    move-object v0, v1

    .line 2404266
    :goto_1
    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z:Landroid/webkit/ValueCallback;

    invoke-interface {v2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 2404267
    iput-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z:Landroid/webkit/ValueCallback;

    goto :goto_0

    .line 2404268
    :cond_6
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 2404269
    :sswitch_4
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_0

    .line 2404270
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    invoke-static {p2, p3}, Landroid/webkit/WebChromeClient$FileChooserParams;->parseResult(ILandroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 2404271
    iput-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xb -> :sswitch_1
        0xc -> :sswitch_3
        0xd -> :sswitch_4
        0x28 -> :sswitch_2
        0x32 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1f63fc02

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2404272
    const v1, 0x7f03068f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2404273
    const v2, 0x7f0d11fc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ah:Landroid/view/View;

    .line 2404274
    const/16 v2, 0x2b

    const v3, 0x5647739a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a2fd5f6

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2404277
    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->b(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2404278
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-eqz v1, :cond_1

    .line 2404279
    iget v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->k:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 2404280
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->V:LX/0zF;

    .line 2404281
    iget-object v2, v1, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v1, v2

    .line 2404282
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    .line 2404283
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setVisibility(I)V

    .line 2404284
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404285
    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->j:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/katana/activity/faceweb/FacewebFragment$8;

    invoke-direct {v3, p0, v1}, Lcom/facebook/katana/activity/faceweb/FacewebFragment$8;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;LX/GxF;)V

    const-wide/16 v4, 0x7530

    const v1, -0x42c427f6

    invoke-static {v2, v3, v4, v5, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2404286
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404287
    iput-object v7, v1, LX/GxF;->D:LX/Gu2;

    .line 2404288
    iput-object v7, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404289
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->v()V

    .line 2404290
    :cond_1
    invoke-super {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->onDestroy()V

    .line 2404291
    const/16 v1, 0x2b

    const v2, -0x3b7d9855

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x67101883

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2404292
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->v()V

    .line 2404293
    iput-object v3, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ah:Landroid/view/View;

    .line 2404294
    invoke-super {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->onDestroyView()V

    .line 2404295
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->V:LX/0zF;

    .line 2404296
    iget-object v2, v0, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v2

    .line 2404297
    iget-boolean v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->P:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 2404298
    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->b()V

    .line 2404299
    invoke-virtual {v0, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2404300
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2404301
    const-string v2, "titlebar_with_modal_done"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2404302
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2404303
    if-eqz v0, :cond_1

    .line 2404304
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2404305
    :cond_1
    const/16 v0, 0x2b

    const v2, 0x335959a1

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x48aa90cd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2404306
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 2404307
    if-eqz v1, :cond_0

    .line 2404308
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    const-string v2, "news_feed_implementation"

    invoke-virtual {v1, v2}, LX/03V;->a(Ljava/lang/String;)V

    .line 2404309
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240008

    const-string v3, "FWFragmentCreate"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2404310
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ac:J

    .line 2404311
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-eqz v1, :cond_3

    .line 2404312
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 2404313
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2404314
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2404315
    if-eqz v1, :cond_1

    .line 2404316
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FacewebPageSession:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2404317
    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0x240006

    invoke-interface {v2, v3, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2404318
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->stopLoading()V

    .line 2404319
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 2404320
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->x()V

    .line 2404321
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->freeMemory()V

    .line 2404322
    :cond_3
    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2404323
    invoke-super {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->onPause()V

    .line 2404324
    const/16 v1, 0x2b

    const v2, 0x7fbb371b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xfeadd49

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2404325
    invoke-super {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->onResume()V

    .line 2404326
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 2404327
    if-eqz v1, :cond_0

    .line 2404328
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    const-string v2, "news_feed_implementation"

    const-string v3, "faceweb"

    invoke-virtual {v1, v2, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404329
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ag:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240008

    const-string v3, "FWFragmentCreate"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2404330
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2404331
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/katana/service/AppSession;->b(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->N:Lcom/facebook/katana/service/AppSession;

    if-nez v2, :cond_1

    .line 2404332
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2404333
    const/16 v1, 0x2b

    const v2, 0x3abbce2f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2404334
    :goto_0
    return-void

    .line 2404335
    :cond_1
    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->b(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2404336
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r()V

    .line 2404337
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404338
    iget-boolean v2, v1, LX/GxF;->C:Z

    move v1, v2

    .line 2404339
    if-eqz v1, :cond_2

    .line 2404340
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2404341
    const v1, -0xe723029

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0

    .line 2404342
    :cond_2
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->p()V

    .line 2404343
    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->f()V

    .line 2404344
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_3

    .line 2404345
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->w()V

    .line 2404346
    :cond_3
    iget-wide v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ac:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    .line 2404347
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ac:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 2404348
    const-string v1, "(function() { if (window.fwDidEnterForeground) { fwDidEnterForeground(%d, %s); } })()"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "true"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2404349
    iget-object v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const/4 v3, 0x0

    .line 2404350
    invoke-virtual {v2, v1, v3}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2404351
    :cond_4
    invoke-direct {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->q()V

    .line 2404352
    const v1, 0x6dc9100e

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2404353
    invoke-super {p0, p1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2404354
    const-string v0, "PROFILE_ID"

    iget-wide v2, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->E:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2404355
    const-string v0, "save_active_state"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2404356
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2404357
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2404358
    iget-object v0, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ad:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2404359
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2404360
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2404361
    :cond_0
    const-string v0, "native_event_listener_keys"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2404362
    const-string v0, "native_event_listener_values"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2404363
    const-string v0, "publisher_callback"

    iget-object v1, p0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404364
    return-void
.end method
