.class public Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private q:LX/Dxc;

.field private r:Ljava/lang/String;

.field private s:Lcom/facebook/graphql/model/GraphQLAlbum;

.field private t:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field private u:LX/74S;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2208325
    const-class v0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;

    const-string v1, "photos_album"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2208324
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2208289
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2208290
    const-string v1, "set_token"

    iget-object v2, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208291
    const-string v1, "fullscreen_gallery_source"

    iget-object v2, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->u:LX/74S;

    invoke-virtual {v2}, LX/74S;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208292
    const-string v1, "extra_photo_tab_mode_params"

    iget-object v2, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->t:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2208293
    const-string v1, "photo_set_grid_source"

    const-string v2, "source_photo_album"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208294
    iget-object v1, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->s:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_0

    .line 2208295
    const-string v1, "extra_album_selected"

    iget-object v2, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->s:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2208296
    :cond_0
    const-string v1, "extra_caller_context"

    sget-object v2, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2208297
    new-instance v1, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    invoke-direct {v1}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;-><init>()V

    .line 2208298
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2208299
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2208300
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 2208301
    const v3, 0x7f0d002f

    invoke-virtual {v2, v3, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2208302
    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2208303
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2208304
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2208323
    sget-object v0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2208307
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2208308
    const v0, 0x7f030f57

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->setContentView(I)V

    .line 2208309
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v0

    .line 2208310
    invoke-static {v0}, LX/Dxc;->b(LX/0QB;)LX/Dxc;

    move-result-object v0

    check-cast v0, LX/Dxc;

    iput-object v0, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->q:LX/Dxc;

    .line 2208311
    invoke-virtual {p0}, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2208312
    const-string v0, "set_token"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->r:Ljava/lang/String;

    .line 2208313
    const-string v0, "fullscreen_gallery_source"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "fullscreen_gallery_source"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/74S;->valueOf(Ljava/lang/String;)LX/74S;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->u:LX/74S;

    .line 2208314
    const-string v0, "extra_photo_tab_mode_params"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->t:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2208315
    const-string v0, "extra_album_selected"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->s:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2208316
    invoke-direct {p0}, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->b()V

    .line 2208317
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2208318
    const v1, 0x7f083209

    invoke-virtual {p0, v1}, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2208319
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2208320
    new-instance v1, LX/FAs;

    invoke-direct {v1, p0}, LX/FAs;-><init>(Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2208321
    return-void

    .line 2208322
    :cond_0
    sget-object v0, LX/74S;->UNKNOWN:LX/74S;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2208305
    iget-object v0, p0, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;->q:LX/Dxc;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/Dxc;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    .line 2208306
    return-void
.end method
