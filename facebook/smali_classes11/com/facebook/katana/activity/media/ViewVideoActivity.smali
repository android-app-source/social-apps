.class public Lcom/facebook/katana/activity/media/ViewVideoActivity;
.super Lcom/facebook/katana/activity/BaseFacebookActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2208271
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2208272
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b(Landroid/os/Bundle;)V

    .line 2208273
    invoke-virtual {p0}, Lcom/facebook/katana/activity/media/ViewVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2208274
    const-string v1, "href"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2208275
    if-eqz v0, :cond_1

    .line 2208276
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2208277
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2208278
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "https"

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2208279
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2208280
    :cond_0
    const-string v2, "video/*"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2208281
    invoke-virtual {p0}, Lcom/facebook/katana/activity/media/ViewVideoActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2208282
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2208283
    invoke-virtual {p0, v1}, Lcom/facebook/katana/activity/media/ViewVideoActivity;->startActivity(Landroid/content/Intent;)V

    .line 2208284
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/katana/activity/media/ViewVideoActivity;->finish()V

    .line 2208285
    return-void
.end method
