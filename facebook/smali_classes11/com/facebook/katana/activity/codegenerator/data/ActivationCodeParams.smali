.class public Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2208005
    new-instance v0, LX/FAh;

    invoke-direct {v0}, LX/FAh;-><init>()V

    sput-object v0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2208006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208007
    iput-wide p1, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->a:J

    .line 2208008
    iput-object p3, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->b:Ljava/lang/String;

    .line 2208009
    iput-object p4, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->c:Ljava/lang/String;

    .line 2208010
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2207995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2207996
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->a:J

    .line 2207997
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->b:Ljava/lang/String;

    .line 2207998
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->c:Ljava/lang/String;

    .line 2207999
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2208004
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2208000
    iget-wide v0, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2208001
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208002
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208003
    return-void
.end method
