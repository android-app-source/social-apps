.class public Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2208080
    new-instance v0, LX/FAl;

    invoke-direct {v0}, LX/FAl;-><init>()V

    sput-object v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2208068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208069
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->a:Ljava/lang/String;

    .line 2208070
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->b:Ljava/lang/String;

    .line 2208071
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 p1, 0x1

    .line 2208072
    if-ne v0, p1, :cond_0

    :goto_0
    move v0, p1

    .line 2208073
    iput-boolean v0, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->c:Z

    .line 2208074
    return-void

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2208075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208076
    iput-object p1, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->a:Ljava/lang/String;

    .line 2208077
    iput-object p2, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->b:Ljava/lang/String;

    .line 2208078
    iput-boolean p3, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->c:Z

    .line 2208079
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2208067
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2208061
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208062
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208063
    iget-boolean v0, p0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->c:Z

    .line 2208064
    if-eqz v0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2208065
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2208066
    return-void

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
