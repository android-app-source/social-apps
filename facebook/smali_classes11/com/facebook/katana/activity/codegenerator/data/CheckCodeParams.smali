.class public Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2208014
    new-instance v0, LX/FAi;

    invoke-direct {v0}, LX/FAi;-><init>()V

    sput-object v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2208015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208016
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->a:Ljava/lang/String;

    .line 2208017
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->b:Ljava/lang/String;

    .line 2208018
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->c:Ljava/lang/String;

    .line 2208019
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2208020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208021
    iput-object p1, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->a:Ljava/lang/String;

    .line 2208022
    iput-object p2, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->b:Ljava/lang/String;

    .line 2208023
    iput-object p3, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->c:Ljava/lang/String;

    .line 2208024
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2208025
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2208026
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208027
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208028
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208029
    return-void
.end method
