.class public Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2208033
    new-instance v0, LX/FAj;

    invoke-direct {v0}, LX/FAj;-><init>()V

    sput-object v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2208042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208043
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->a:Ljava/lang/String;

    .line 2208044
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->b:Ljava/lang/String;

    .line 2208045
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2208038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208039
    iput-object p1, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->a:Ljava/lang/String;

    .line 2208040
    iput-object p2, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->b:Ljava/lang/String;

    .line 2208041
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2208037
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2208034
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208035
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2208036
    return-void
.end method
