.class public Lcom/facebook/katana/activity/photos/PhotosTabActivity;
.super Lcom/facebook/katana/activity/BaseFacebookActivity;
.source ""

# interfaces
.implements LX/0hc;
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements LX/0f2;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public A:LX/74I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FAu;",
            ">;"
        }
    .end annotation
.end field

.field private D:Landroid/support/v4/view/ViewPager;

.field private E:Ljava/lang/Long;

.field private F:Ljava/lang/Long;

.field private G:Ljava/lang/String;

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Z

.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2TK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/73w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2208444
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;-><init>()V

    .line 2208445
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    return-void
.end method

.method private static a(Lcom/facebook/katana/activity/photos/PhotosTabActivity;Lcom/facebook/content/SecureContextHelper;LX/2TK;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/73w;LX/74I;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/activity/photos/PhotosTabActivity;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2TK;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dxb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/74I;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2208442
    iput-object p1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->q:LX/2TK;

    iput-object p3, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->r:LX/0Or;

    iput-object p4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->s:LX/0Or;

    iput-object p5, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->u:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->v:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->w:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->x:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->y:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->z:LX/73w;

    iput-object p12, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->A:LX/74I;

    iput-object p13, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->B:Ljava/lang/Boolean;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    invoke-static {v13}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v13}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v2

    check-cast v2, LX/2TK;

    const/16 v3, 0x12cb

    invoke-static {v13, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x15e7

    invoke-static {v13, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x19c6

    invoke-static {v13, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2dfd

    invoke-static {v13, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {v13, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2eac

    invoke-static {v13, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2eab

    invoke-static {v13, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2eaa

    invoke-static {v13, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v13}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v11

    check-cast v11, LX/73w;

    invoke-static {v13}, LX/74I;->a(LX/0QB;)LX/74I;

    move-result-object v12

    check-cast v12, LX/74I;

    invoke-static {v13}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static/range {v0 .. v13}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->a(Lcom/facebook/katana/activity/photos/PhotosTabActivity;Lcom/facebook/content/SecureContextHelper;LX/2TK;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/73w;LX/74I;Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2208446
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->C:Ljava/util/List;

    new-instance v1, LX/FAu;

    invoke-direct {v1, p1, p2, p3, p4}, LX/FAu;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2208447
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2208417
    new-instance v1, LX/9lP;

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->F:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->F:Ljava/lang/Long;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->L:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->M:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, LX/9lP;-><init>(JJLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2208418
    new-instance v0, LX/74W;

    invoke-virtual {v1}, LX/9lP;->g()LX/9lQ;

    move-result-object v1

    invoke-virtual {v1}, LX/9lQ;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->I:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, LX/74W;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208419
    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->z:LX/73w;

    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->N:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/73w;->a(Ljava/lang/String;)V

    .line 2208420
    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->z:LX/73w;

    invoke-virtual {v1, v0}, LX/73w;->a(LX/74W;)V

    .line 2208421
    iput-object p1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->I:Ljava/lang/String;

    .line 2208422
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->n()V

    .line 2208423
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2208424
    const-string v0, "sync"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->q:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2208425
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->m()V

    .line 2208426
    :cond_0
    :goto_1
    return-void

    .line 2208427
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    goto :goto_0

    .line 2208428
    :cond_2
    const-string v0, "albums"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "sync"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2208429
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f020072

    .line 2208430
    iput v1, v0, LX/108;->i:I

    .line 2208431
    move-object v0, v0

    .line 2208432
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_1

    .line 2208433
    :cond_3
    const-string v0, "albums"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2208434
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f02034b

    .line 2208435
    iput v1, v0, LX/108;->i:I

    .line 2208436
    move-object v0, v0

    .line 2208437
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_1

    .line 2208438
    :cond_4
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f080017

    invoke-virtual {p0, v1}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2208439
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2208440
    move-object v0, v0

    .line 2208441
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_1
.end method

.method private c(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2208448
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->C:Ljava/util/List;

    .line 2208449
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-nez v0, :cond_0

    .line 2208450
    sget-object v0, LX/5SD;->VIEWING_MODE:LX/5SD;

    iget-object v3, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2208451
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2208452
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2208453
    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "set_token"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2208454
    if-nez v3, :cond_4

    .line 2208455
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v3, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Asked to show suggested photos but provided no campaign ID"

    invoke-virtual {v0, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208456
    :cond_1
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2208457
    const-string v3, "set_token"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/facebook/photos/data/model/PhotoSet;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208458
    const-string v3, "extra_photo_tab_mode_params"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2208459
    const-string v3, "photo_set_grid_source"

    const-string v4, "source_photos_tab"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208460
    const-string v3, "userId"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208461
    const-string v3, "userName"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208462
    const-string v3, "friendship_status"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->L:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208463
    const-string v3, "subscribe_status"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->M:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208464
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->r()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2208465
    const-string v3, "photos_of"

    const v4, 0x7f083209

    const-class v5, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-direct {p0, v3, v4, v5, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->a(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;)V

    .line 2208466
    :cond_2
    iget-object v3, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-static {v0, v3, v2, v4}, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->b(Landroid/os/Bundle;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 2208467
    const-string v3, "photo_uploads"

    const v4, 0x7f081280

    const-class v5, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;

    invoke-direct {p0, v3, v4, v5, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->a(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;)V

    .line 2208468
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2208469
    const-string v3, "extra_photo_tab_mode_params"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2208470
    const-string v3, "disable_adding_photos_to_albums"

    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "disable_adding_photos_to_albums"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2208471
    const-string v3, "owner_id"

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2208472
    const-string v3, "albums"

    const v4, 0x7f081281

    const-class v5, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0, v3, v4, v5, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->a(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;)V

    .line 2208473
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2208474
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2208475
    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "nux_ref"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2208476
    if-eqz v2, :cond_3

    .line 2208477
    const-string v3, "nux_ref"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208478
    :cond_3
    const-string v2, "sync"

    const v3, 0x7f081282

    const-class v4, Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0, v2, v3, v4, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->a(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;)V

    .line 2208479
    :goto_1
    const v0, 0x7f0d2511

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2208480
    new-instance v2, LX/FAv;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->C:Ljava/util/List;

    invoke-direct {v2, v3, p0, v4}, LX/FAv;-><init>(LX/0gc;Landroid/content/Context;Ljava/util/List;)V

    .line 2208481
    iget-object v3, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2208482
    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2208483
    iput-object p0, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2208484
    if-eqz p1, :cond_6

    .line 2208485
    invoke-direct {p0, p1}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->d(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 2208486
    if-eqz v0, :cond_5

    .line 2208487
    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2208488
    :goto_2
    return-void

    .line 2208489
    :cond_4
    const-string v4, "campaign_id"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208490
    const-string v4, "isDefaultLandingPage"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2208491
    const-string v4, "pandora_instance_id"

    new-instance v5, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    invoke-direct {v5, v3}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2208492
    const-string v3, "callerContext"

    const-class v4, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2208493
    const-string v3, "campaign"

    const v4, 0x7f081285

    const-class v5, Lcom/facebook/photos/pandora/ui/PandoraCampaignMediaSetFragment;

    invoke-direct {p0, v3, v4, v5, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->a(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2208494
    :cond_5
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v2, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "non existent tab tabToShow=\"%s\" syncTabAdded=\"%s\""

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v3, p1, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2208495
    :cond_6
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->B_(I)V

    goto :goto_2

    :cond_7
    move v1, v2

    goto/16 :goto_1
.end method

.method private d(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2208496
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->C:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2208497
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->C:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FAu;

    iget-object v0, v0, LX/FAu;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2208498
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2208499
    :goto_1
    return-object v0

    .line 2208500
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2208501
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2208502
    sget-object v0, LX/5SD;->EDIT_PROFILE_PIC:LX/5SD;

    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2208503
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2208504
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f020bf6

    .line 2208505
    iput v1, v0, LX/108;->i:I

    .line 2208506
    move-object v0, v0

    .line 2208507
    const v1, 0x7f083208

    invoke-virtual {p0, v1}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2208508
    iput-object v1, v0, LX/108;->j:Ljava/lang/String;

    .line 2208509
    move-object v0, v0

    .line 2208510
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2208511
    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 2208512
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2208513
    return-void
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 2208514
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2208515
    iget-boolean v1, v0, LX/Dxa;->a:Z

    move v0, v1

    .line 2208516
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2208517
    iget-boolean v1, v0, LX/Dxa;->b:Z

    move v0, v1

    .line 2208518
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 2208519
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->B:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->q:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Z
    .locals 3

    .prologue
    .line 2208520
    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_suggested_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private r()Z
    .locals 1

    .prologue
    .line 2208443
    iget-boolean v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->O:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()Z
    .locals 4

    .prologue
    .line 2208329
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 1

    .prologue
    .line 2208330
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->C:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FAu;

    iget-object v0, v0, LX/FAu;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->b(Ljava/lang/String;)V

    .line 2208331
    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2208332
    const-string v0, "photos_tabs"

    return-object v0
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2208333
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2208334
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->a(Landroid/content/Intent;)V

    .line 2208335
    const-string v0, "tab_to_show"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2208336
    if-nez v0, :cond_0

    .line 2208337
    iput v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    .line 2208338
    :goto_0
    return-void

    .line 2208339
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->d(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 2208340
    if-nez v0, :cond_1

    .line 2208341
    iput v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    goto :goto_0

    .line 2208342
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    goto :goto_0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2208343
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2208344
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b(Landroid/os/Bundle;)V

    .line 2208345
    invoke-static {p0, p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2208346
    const v0, 0x7f030f55

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->setContentView(I)V

    .line 2208347
    const v0, 0x7f0d2512

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->D:Landroid/support/v4/view/ViewPager;

    .line 2208348
    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2208349
    const-string v2, "owner_id"

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    .line 2208350
    const-string v2, "viewer_id"

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->F:Ljava/lang/Long;

    .line 2208351
    const-string v0, "friendship_status"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->L:Ljava/lang/String;

    .line 2208352
    const-string v0, "subscribe_status"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->M:Ljava/lang/String;

    .line 2208353
    const-string v0, "session_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->N:Ljava/lang/String;

    .line 2208354
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->N:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2208355
    invoke-static {}, LX/74I;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->N:Ljava/lang/String;

    .line 2208356
    const-string v0, "session_id"

    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->N:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2208357
    :cond_0
    const-string v0, "profile_name"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->G:Ljava/lang/String;

    .line 2208358
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->G:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->E:Ljava/lang/Long;

    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2208359
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->G:Ljava/lang/String;

    .line 2208360
    :cond_1
    const-string v0, "extra_photo_tab_mode_params"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2208361
    const-string v0, "hide_photos_of_tab"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->O:Z

    .line 2208362
    const-string v0, "extra_photo_title_text"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->K:Ljava/lang/String;

    .line 2208363
    const-string v0, "edit_profile_pic"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2208364
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->l()V

    .line 2208365
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    invoke-virtual {v0, v1}, LX/Dxa;->a(Landroid/content/Intent;)V

    .line 2208366
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2208367
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->o()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->G:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2208368
    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2208369
    :cond_3
    :goto_0
    invoke-virtual {v0, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2208370
    invoke-virtual {v0, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 2208371
    new-instance v2, LX/FAt;

    invoke-direct {v2, p0}, LX/FAt;-><init>(Lcom/facebook/katana/activity/photos/PhotosTabActivity;)V

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2208372
    const-string v0, "tab_to_show"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->c(Ljava/lang/String;)V

    .line 2208373
    return-void

    .line 2208374
    :cond_4
    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v2}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v2}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2208375
    :cond_5
    const v2, 0x7f081558

    invoke-virtual {p0, v2}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2208376
    invoke-virtual {v0, v7}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setShowDividers(Z)V

    goto :goto_0

    .line 2208377
    :cond_6
    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->K:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2208378
    iget-object v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->K:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2208379
    invoke-virtual {v0, v7}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setShowDividers(Z)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2208380
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2208381
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2208382
    :goto_0
    return-void

    .line 2208383
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxc;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/Dxc;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2208384
    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->finish()V

    goto :goto_0

    .line 2208385
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2208386
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2208387
    const-string v1, "is_uploading_media"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2208388
    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2208389
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_0
    .end packed-switch
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2208390
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2208391
    const-string v0, "mTabToShowOnResume"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    .line 2208392
    const-string v0, "extra_photo_tab_mode_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2208393
    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    const/16 v0, 0x22

    const v1, 0x72f94b5f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2208394
    invoke-super {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onResume()V

    .line 2208395
    iget v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    if-eq v1, v3, :cond_0

    .line 2208396
    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->D:Landroid/support/v4/view/ViewPager;

    iget v2, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2208397
    iput v3, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    .line 2208398
    :cond_0
    const/16 v1, 0x23

    const v2, 0x6a8c76c0

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2208399
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    .line 2208400
    const-string v0, "mTabToShowOnResume"

    iget v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->H:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2208401
    const-string v0, "extra_photo_tab_mode_params"

    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->J:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2208402
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2208403
    return-void
.end method

.method public final onTabChanged(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2208404
    invoke-direct {p0, p1}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->b(Ljava/lang/String;)V

    .line 2208405
    return-void
.end method

.method public titleBarPrimaryActionClickHandler(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2208406
    invoke-direct {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2208407
    invoke-virtual {p0}, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->finish()V

    .line 2208408
    :cond_0
    :goto_0
    return-void

    .line 2208409
    :cond_1
    const-string v0, "sync"

    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2208410
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2208411
    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2208412
    :cond_2
    const-string v0, "albums"

    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2208413
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9at;

    sget-object v1, LX/9au;->ALBUMSTAB:LX/9au;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v0

    .line 2208414
    iget-object v1, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2208415
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->I:Ljava/lang/String;

    const-string v1, "albums"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2208416
    iget-object v0, p0, Lcom/facebook/katana/activity/photos/PhotosTabActivity;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxb;

    invoke-virtual {v0, p0}, LX/Dxb;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method
