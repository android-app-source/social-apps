.class public Lcom/facebook/katana/InternSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# static fields
.field private static final aj:LX/0Tn;

.field private static final ak:LX/0Tn;

.field private static final al:LX/0Tn;

.field private static final am:LX/0Tn;


# instance fields
.field public A:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/44G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Y9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/D39;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/HYU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/FAK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/17X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0Or;
    .annotation runtime Lcom/facebook/aldrin/status/annotations/IsAldrinEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/GiU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/CIm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/3y7;
    .annotation runtime Lcom/facebook/abtest/qe/annotations/ForQuickExperiment;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/3y7;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/ForQuickPromotion;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/3y7;
    .annotation runtime Lcom/facebook/selfupdate/annotations/ForSelfUpdate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/31K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:LX/3y7;
    .annotation runtime Lcom/facebook/search/prefs/ForGraphSearch;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:LX/3y7;
    .annotation runtime Lcom/facebook/feed/annotations/ForNewsfeed;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public X:LX/3y7;
    .annotation runtime Lcom/facebook/greetingcards/create/ForGreetingCards;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/2l5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Z:LX/AP0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public a:LX/1Bf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:LX/7Wg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ab:LX/Bhy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ac:LX/Gvj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ad:LX/1ER;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ae:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public af:LX/D3X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ag:LX/D3Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ah:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ai:LX/FmM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public an:LX/4oq;

.field private ao:Landroid/preference/PreferenceScreen;

.field private final ap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0l2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6Uq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0dC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/2Gc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2Gq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2Bz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/3Q7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1Mj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/6ZG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/GaL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/94q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/contactlogs/ContactLogsUploadRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/EjM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/4mE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0YI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/7Tx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/Gaw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/Hhn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2400930
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "reset"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/katana/InternSettingsActivity;->aj:LX/0Tn;

    .line 2400931
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "rolodex"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/katana/InternSettingsActivity;->ak:LX/0Tn;

    .line 2400932
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "device_stat_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/katana/InternSettingsActivity;->al:LX/0Tn;

    .line 2400933
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "process_stat_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/katana/InternSettingsActivity;->am:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2400934
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 2400935
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ap:Ljava/util/List;

    .line 2400936
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2400937
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private a()Landroid/preference/Preference;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2400938
    invoke-virtual {p0}, Lcom/facebook/katana/InternSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 2400939
    const-string v0, "Push Registration"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400940
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400941
    const-string v2, "Device ID"

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400942
    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->g:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400943
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400944
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400945
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v3, "mqttlite_notif"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {v2, v0}, LX/4oi;->a(LX/0Tn;)V

    .line 2400946
    const v0, 0x7f080f21

    invoke-virtual {v2, v0}, LX/4oi;->setTitle(I)V

    .line 2400947
    const v0, 0x7f080f22

    invoke-virtual {v2, v0}, LX/4oi;->setSummary(I)V

    .line 2400948
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400949
    const-string v0, "mqtt_debug"

    const/4 v3, 0x4

    invoke-virtual {p0, v0, v3}, Lcom/facebook/katana/InternSettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2400950
    new-instance v3, LX/Gt2;

    invoke-direct {v3, p0, v0}, LX/Gt2;-><init>(Lcom/facebook/katana/InternSettingsActivity;Landroid/content/SharedPreferences;)V

    invoke-virtual {v2, v3}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2400951
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400952
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400953
    const v2, 0x7f080f1e

    invoke-virtual {v0, v2}, LX/4ok;->setTitle(I)V

    .line 2400954
    const v2, 0x7f080f1f

    invoke-virtual {v0, v2}, LX/4ok;->setSummary(I)V

    .line 2400955
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400956
    new-instance v2, LX/Gt3;

    invoke-direct {v2, p0}, LX/Gt3;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v2}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2400957
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400958
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400959
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400960
    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceCategory;)V

    .line 2400961
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->h:LX/2Gc;

    invoke-virtual {v0}, LX/2Gc;->a()Ljava/util/Set;

    move-result-object v0

    .line 2400962
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ge;

    .line 2400963
    new-instance v3, Landroid/preference/PreferenceCategory;

    invoke-direct {v3, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400964
    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400965
    iget-object v4, p0, Lcom/facebook/katana/InternSettingsActivity;->i:LX/2Gq;

    invoke-virtual {v4, v0}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/katana/InternSettingsActivity;->j:LX/2Bz;

    invoke-direct {p0, v3, v0, v4, v5}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceCategory;LX/2Ge;LX/2H0;LX/2Bz;)Landroid/preference/Preference;

    goto :goto_0

    .line 2400966
    :cond_0
    return-object v1
.end method

.method private a(Landroid/preference/PreferenceCategory;LX/2Ge;LX/2H0;LX/2Bz;)Landroid/preference/Preference;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2400967
    invoke-virtual {p2}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400968
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400969
    const-string v1, "Token"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400970
    invoke-virtual {p3}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400971
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400972
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400973
    const-string v1, "Last Token Request Time"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400974
    invoke-virtual {p3}, LX/2H0;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/katana/InternSettingsActivity;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400975
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400976
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400977
    const-string v1, "Last FBServer Register Time"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400978
    invoke-virtual {p3}, LX/2H0;->o()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/katana/InternSettingsActivity;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400979
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400980
    new-instance v0, LX/4or;

    invoke-direct {v0, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2400981
    const-string v1, "Register/Unregister"

    invoke-virtual {v0, v1}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400982
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Manually register/unregister "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400983
    invoke-virtual {p2}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2400984
    new-array v1, v8, [Ljava/lang/CharSequence;

    const-string v2, "Clear Preference"

    aput-object v2, v1, v4

    const-string v2, "Register"

    aput-object v2, v1, v5

    const-string v2, "Ensure Registration"

    aput-object v2, v1, v6

    const-string v2, "Unregister"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2400985
    new-array v1, v8, [Ljava/lang/CharSequence;

    const-string v2, "clear"

    aput-object v2, v1, v4

    const-string v2, "register"

    aput-object v2, v1, v5

    const-string v2, "ensure"

    aput-object v2, v1, v6

    const-string v2, "unregister"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2400986
    new-instance v1, LX/Gt4;

    invoke-direct {v1, p0, p4, p2, p3}, LX/Gt4;-><init>(Lcom/facebook/katana/InternSettingsActivity;LX/2Bz;LX/2Ge;LX/2H0;)V

    invoke-virtual {v0, v1}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2400987
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400988
    return-object p1
.end method

.method private static a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2400989
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 2400990
    const-string v0, ""

    .line 2400991
    :goto_0
    return-object v0

    .line 2400992
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 2400993
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MMM d, hh:mm:ss a z"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2400994
    invoke-virtual {v1, v0}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/preference/PreferenceCategory;)V
    .locals 5

    .prologue
    .line 2400995
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    sget-object v1, LX/01p;->f:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2400996
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    sget-object v2, LX/01p;->j:LX/01q;

    invoke-static {v1, v2}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2400997
    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 2400998
    const-string v2, "shared_status"

    const-string v3, "DISABLED"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2400999
    const-string v3, "leader_package"

    const-string v4, "unset"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2401000
    const-string v3, "Shared Experiment"

    invoke-virtual {p1, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401001
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401002
    const-string v4, "Experiment Status"

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401003
    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401004
    invoke-virtual {p1, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401005
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401006
    const-string v3, "Leader Package"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401007
    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401008
    invoke-virtual {p1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401009
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2401010
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401011
    const-string v2, "Registered Packages"

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401012
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401013
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401014
    :cond_0
    return-void
.end method

.method private a(Landroid/preference/PreferenceGroup;)V
    .locals 3

    .prologue
    .line 2401015
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401016
    const-string v1, "Override A Gk"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401017
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2401018
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401019
    return-void
.end method

.method private a(Landroid/preference/PreferenceGroup;LX/3y7;)V
    .locals 3

    .prologue
    .line 2401020
    invoke-interface {p2, p0}, LX/3y7;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 2401021
    instance-of v1, v0, LX/3yY;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, LX/3yY;

    invoke-interface {v1}, LX/3yY;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2401022
    :cond_1
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 2401023
    :cond_2
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;)V
    .locals 13

    .prologue
    .line 2401024
    new-instance v0, LX/Gsy;

    invoke-direct {v0, p0, p0}, LX/Gsy;-><init>(Lcom/facebook/katana/InternSettingsActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    .line 2401025
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    sget-object v1, LX/1CA;->s:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oq;->a(LX/0Tn;)V

    .line 2401026
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/4oq;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401027
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    const-string v1, "Filter settings"

    invoke-virtual {v0, v1}, LX/4oq;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401028
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    const-string v1, "Filter Internal Settings"

    invoke-virtual {v0, v1}, LX/4oq;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401029
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    const-string v1, "Quickly find required internal setting"

    invoke-virtual {v0, v1}, LX/4oq;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401030
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/4oq;->setText(Ljava/lang/String;)V

    .line 2401031
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "Leave empty to show all"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2401032
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2401033
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 2401034
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 2401035
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, LX/Gt9;

    invoke-direct {v1, p0}, LX/Gt9;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2401036
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    new-instance v1, LX/GtJ;

    invoke-direct {v1, p0}, LX/GtJ;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4oq;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401037
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401038
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401039
    sget-object v1, LX/0dU;->j:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2401040
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401041
    const v1, 0x7f0835f9

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(I)V

    .line 2401042
    const v1, 0x7f0835fa

    invoke-virtual {v0, v1}, LX/4ok;->setSummary(I)V

    .line 2401043
    move-object v1, v0

    .line 2401044
    new-instance v0, LX/GtR;

    invoke-direct {v0, p0}, LX/GtR;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v1, v0}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401045
    new-instance v0, LX/4or;

    invoke-direct {v0, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2401046
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401047
    new-instance v3, LX/4ok;

    invoke-direct {v3, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401048
    new-instance v4, LX/4or;

    invoke-direct {v4, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2401049
    new-instance v5, LX/4oq;

    invoke-direct {v5, p0}, LX/4oq;-><init>(Landroid/content/Context;)V

    .line 2401050
    sget-object v6, LX/0dU;->r:LX/0Tn;

    invoke-virtual {v5, v6}, LX/4oq;->a(LX/0Tn;)V

    .line 2401051
    const-string v6, "facebook.com"

    invoke-virtual {v5, v6}, LX/4oq;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401052
    const-string v6, "Sandbox"

    invoke-virtual {v5, v6}, LX/4oq;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401053
    const-string v6, "e.g., beta, prod, your-unix-name.sb, blank=facebook.com"

    invoke-virtual {v5, v6}, LX/4oq;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401054
    const-string v6, "Sandbox"

    invoke-virtual {v5, v6}, LX/4oq;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401055
    invoke-virtual {v5}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v6

    .line 2401056
    const-string v7, "e.g., beta, latest, intern, prod, dev, facebook.com"

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2401057
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2401058
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 2401059
    new-instance v6, LX/GtS;

    invoke-direct {v6, p0, v1, v4}, LX/GtS;-><init>(Lcom/facebook/katana/InternSettingsActivity;LX/4oi;LX/4or;)V

    invoke-virtual {v5, v6}, LX/4oq;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401060
    invoke-virtual {p1, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401061
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401062
    iget-object v6, p0, Lcom/facebook/katana/InternSettingsActivity;->ah:LX/0Uh;

    const/16 v7, 0x10

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2401063
    new-instance v6, LX/4ok;

    invoke-direct {v6, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401064
    sget-object v7, LX/0dU;->k:LX/0Tn;

    invoke-virtual {v6, v7}, LX/4oi;->a(LX/0Tn;)V

    .line 2401065
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401066
    const v7, 0x7f0835fb

    invoke-virtual {v6, v7}, LX/4ok;->setTitle(I)V

    .line 2401067
    const v7, 0x7f0835fc

    invoke-virtual {v6, v7}, LX/4ok;->setSummary(I)V

    .line 2401068
    move-object v6, v6

    .line 2401069
    new-instance v7, LX/GtT;

    invoke-direct {v7, p0}, LX/GtT;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v6, v7}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401070
    invoke-virtual {p1, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401071
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401072
    iget-object v6, p0, Lcom/facebook/katana/InternSettingsActivity;->R:LX/3y7;

    invoke-direct {p0, p1, v6}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceGroup;LX/3y7;)V

    .line 2401073
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceGroup;)V

    .line 2401074
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->b(Landroid/preference/PreferenceGroup;)V

    .line 2401075
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->e(Landroid/preference/PreferenceGroup;)V

    .line 2401076
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->f(Landroid/preference/PreferenceGroup;)V

    .line 2401077
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const-string v8, "Reload Bookmarks"

    aput-object v8, v6, v7

    invoke-virtual {v0, v6}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401078
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const-string v8, "bookmarks"

    aput-object v8, v6, v7

    invoke-virtual {v0, v6}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401079
    const-string v6, "Bookmarks menu"

    invoke-virtual {v0, v6}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401080
    const-string v6, "Bookmarks menu control"

    invoke-virtual {v0, v6}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401081
    const-string v6, "Reloads Bookmarks navigation data"

    invoke-virtual {v0, v6}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401082
    const-string v6, "Reload"

    invoke-virtual {v0, v6}, LX/4or;->setPositiveButtonText(Ljava/lang/CharSequence;)V

    .line 2401083
    const-string v6, "Cancel"

    invoke-virtual {v0, v6}, LX/4or;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2401084
    const-string v6, "navbar"

    invoke-virtual {v0, v6}, LX/4or;->setKey(Ljava/lang/String;)V

    .line 2401085
    new-instance v6, LX/GtU;

    invoke-direct {v6, p0}, LX/GtU;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v6}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401086
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401087
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401088
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401089
    const v6, 0x7f080e8d

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 2401090
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401091
    iget-object v6, p0, Lcom/facebook/katana/InternSettingsActivity;->aa:LX/7Wg;

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401092
    iget-object v6, p0, Lcom/facebook/katana/InternSettingsActivity;->z:LX/Hhn;

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401093
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ab:LX/Bhy;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401094
    new-instance v0, LX/5NL;

    invoke-direct {v0, p0}, LX/5NL;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401095
    new-instance v0, LX/5NK;

    invoke-direct {v0, p0}, LX/5NK;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401096
    new-instance v0, LX/5NN;

    invoke-direct {v0, p0}, LX/5NN;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401097
    new-instance v0, LX/5NP;

    invoke-direct {v0, p0}, LX/5NP;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401098
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401099
    new-instance v6, Landroid/preference/PreferenceCategory;

    invoke-direct {v6, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401100
    const-string v0, "Video"

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401101
    invoke-virtual {p1, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401102
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401103
    sget-object v7, LX/1CA;->g:LX/0Tn;

    invoke-virtual {v0, v7}, LX/4oi;->a(LX/0Tn;)V

    .line 2401104
    const-string v7, "Display Inline Video Specs"

    invoke-virtual {v0, v7}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401105
    const-string v7, "Display metadata of the playing video"

    invoke-virtual {v0, v7}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401106
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401107
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401108
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401109
    sget-object v7, LX/ESk;->a:LX/0Tn;

    invoke-virtual {v0, v7}, LX/4oi;->a(LX/0Tn;)V

    .line 2401110
    const-string v7, "Display Debug Autoplay Layer"

    invoke-virtual {v0, v7}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401111
    const-string v7, "Display layer with autoplay debugging info in Video Home"

    invoke-virtual {v0, v7}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401112
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401113
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401114
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401115
    sget-object v7, LX/19c;->a:LX/0Tn;

    invoke-virtual {v0, v7}, LX/4oi;->a(LX/0Tn;)V

    .line 2401116
    const-string v7, "Enable Video Player Debugging Logs"

    invoke-virtual {v0, v7}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401117
    const-string v7, "Enable debugging logs for video player"

    invoke-virtual {v0, v7}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401118
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401119
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401120
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401121
    sget-object v7, LX/1CA;->i:LX/0Tn;

    invoke-virtual {v0, v7}, LX/4oi;->a(LX/0Tn;)V

    .line 2401122
    const-string v7, "Play inline videos unmuted"

    invoke-virtual {v0, v7}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401123
    const-string v7, "Set all inline FB videos to unmuted state"

    invoke-virtual {v0, v7}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401124
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401125
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401126
    invoke-static {}, LX/1CB;->values()[LX/1CB;

    move-result-object v7

    .line 2401127
    array-length v0, v7

    new-array v8, v0, [Ljava/lang/CharSequence;

    .line 2401128
    const/4 v0, 0x0

    :goto_0
    array-length v9, v7

    if-ge v0, v9, :cond_1

    .line 2401129
    aget-object v9, v7, v0

    invoke-virtual {v9}, LX/1CB;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v0

    .line 2401130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2401131
    :cond_1
    new-instance v0, LX/4ot;

    invoke-direct {v0, p0}, LX/4ot;-><init>(Landroid/content/Context;)V

    .line 2401132
    sget-object v7, LX/1CA;->j:LX/0Tn;

    invoke-virtual {v0, v7}, LX/4or;->a(LX/0Tn;)V

    .line 2401133
    const-string v7, "Logging level"

    invoke-virtual {v0, v7}, LX/4ot;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401134
    const-string v7, "Adjust video logging verbosity level"

    invoke-virtual {v0, v7}, LX/4ot;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401135
    sget-object v7, LX/1CB;->NONE:LX/1CB;

    invoke-virtual {v7}, LX/1CB;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4ot;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401136
    new-instance v7, LX/GtV;

    invoke-direct {v7, p0}, LX/GtV;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v7}, LX/4ot;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401137
    invoke-virtual {v0, v8}, LX/4ot;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401138
    invoke-virtual {v0, v8}, LX/4ot;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401139
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401140
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401141
    const-string v7, "Video Home"

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401142
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401143
    new-instance v7, LX/4ok;

    invoke-direct {v7, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401144
    sget-object v8, LX/1CA;->k:LX/0Tn;

    invoke-virtual {v7, v8}, LX/4oi;->a(LX/0Tn;)V

    .line 2401145
    const-string v8, "Videohome force prefetching"

    invoke-virtual {v7, v8}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401146
    const-string v8, "Forces data prefetching"

    invoke-virtual {v7, v8}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401147
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401148
    invoke-virtual {v0, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401149
    new-instance v7, LX/EVX;

    invoke-direct {v7, p0}, LX/EVX;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401150
    new-instance v7, LX/4ok;

    invoke-direct {v7, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401151
    sget-object v8, LX/1CA;->l:LX/0Tn;

    invoke-virtual {v7, v8}, LX/4oi;->a(LX/0Tn;)V

    .line 2401152
    const-string v8, "Videohome data fetching toast"

    invoke-virtual {v7, v8}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401153
    const-string v8, "Show toasts about data fetching status"

    invoke-virtual {v7, v8}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401154
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401155
    invoke-virtual {v0, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401156
    new-instance v7, LX/4ok;

    invoke-direct {v7, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401157
    sget-object v8, LX/1CA;->m:LX/0Tn;

    invoke-virtual {v7, v8}, LX/4oi;->a(LX/0Tn;)V

    .line 2401158
    const-string v8, "Videohome Debug Overlay"

    invoke-virtual {v7, v8}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401159
    const-string v8, "long press on Videohome and see debug info"

    invoke-virtual {v7, v8}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401160
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401161
    invoke-virtual {v0, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401162
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401163
    sget-object v7, LX/2sa;->a:LX/0Tn;

    invoke-virtual {v0, v7}, LX/4oi;->a(LX/0Tn;)V

    .line 2401164
    const-string v7, "Show VH session status with push"

    invoke-virtual {v0, v7}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401165
    const-string v7, "Display VH session status using system tray notification"

    invoke-virtual {v0, v7}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401166
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401167
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401168
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401169
    new-instance v0, LX/D5I;

    invoke-direct {v0, p0}, LX/D5I;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401170
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401171
    new-instance v0, LX/D7J;

    invoke-direct {v0, p0}, LX/D7J;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401172
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401173
    new-instance v0, LX/7Pv;

    invoke-direct {v0, p0}, LX/7Pv;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401174
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401175
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401176
    const-string v6, "Groups"

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401177
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401178
    new-instance v6, Landroid/preference/Preference;

    invoke-direct {v6, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401179
    const-string v7, "Discover Landings"

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401180
    const-string v7, "Open Discover Landings Page"

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401181
    new-instance v7, LX/GtW;

    invoke-direct {v7, p0}, LX/GtW;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401182
    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401183
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401184
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401185
    const-string v6, "In-App Browser"

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401186
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401187
    sget-object v6, LX/1CA;->w:LX/0Tn;

    invoke-virtual {v2, v6}, LX/4oi;->a(LX/0Tn;)V

    .line 2401188
    const-string v6, "Enable Web Contents Debugging"

    invoke-virtual {v2, v6}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401189
    const-string v6, "Enable remote debugging with Chrome"

    invoke-virtual {v2, v6}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401190
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401191
    new-instance v6, LX/Gsp;

    invoke-direct {v6, p0}, LX/Gsp;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v2, v6}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401192
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401193
    sget-object v2, LX/1CA;->x:LX/0Tn;

    invoke-virtual {v3, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2401194
    const-string v2, "Enable Debug Overlay"

    invoke-virtual {v3, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401195
    const-string v2, "Enable in-app browser debug overlay"

    invoke-virtual {v3, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401196
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401197
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401198
    new-instance v2, LX/D3b;

    invoke-direct {v2, p0}, LX/D3b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401199
    new-instance v2, LX/D3c;

    invoke-direct {v2, p0}, LX/D3c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401200
    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->af:LX/D3X;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401201
    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->ag:LX/D3Z;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401202
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->h(Landroid/preference/PreferenceGroup;)V

    .line 2401203
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401204
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401205
    const-string v2, "Contacts"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401206
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401207
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401208
    const-string v3, "Force upload call logs"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401209
    const-string v3, "Call, MMS and SMS Log Upload"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401210
    new-instance v3, LX/Gsq;

    invoke-direct {v3, p0}, LX/Gsq;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401211
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401212
    const-string v6, "Force upload contacts"

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401213
    const-string v6, "Phonebook Contacts Upload"

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401214
    new-instance v6, LX/Gsr;

    invoke-direct {v6, p0}, LX/Gsr;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401215
    new-instance v6, Landroid/preference/Preference;

    invoke-direct {v6, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401216
    const-string v7, "Force DELTA upload contacts"

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401217
    const-string v7, "Phonebook Contacts DELTA Upload"

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401218
    new-instance v7, LX/Gss;

    invoke-direct {v7, p0}, LX/Gss;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401219
    new-instance v7, LX/8wa;

    invoke-direct {v7, p0}, LX/8wa;-><init>(Landroid/content/Context;)V

    .line 2401220
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401221
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401222
    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401223
    invoke-virtual {v0, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401224
    invoke-direct {p0}, Lcom/facebook/katana/InternSettingsActivity;->b()Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401225
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401226
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401227
    const-string v2, "Growth NUX"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401228
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401229
    new-instance v2, LX/F9Y;

    invoke-direct {v2, p0}, LX/F9Y;-><init>(Landroid/content/Context;)V

    .line 2401230
    const/4 v12, 0x1

    const/4 v6, 0x0

    .line 2401231
    invoke-static {v2}, LX/F9Y;->b(LX/F9Y;)LX/0Px;

    move-result-object v7

    .line 2401232
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2401233
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2401234
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v10

    move v3, v6

    .line 2401235
    :goto_1
    if-ge v3, v10, :cond_2

    .line 2401236
    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2401237
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2401238
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2401239
    :cond_2
    const-string v3, "Show NUX step"

    invoke-virtual {v2, v3}, LX/F9Y;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401240
    const-string v3, "Show a single NUX step"

    invoke-virtual {v2, v3}, LX/F9Y;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401241
    const-class v3, LX/F9Y;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/F9Y;->setKey(Ljava/lang/String;)V

    .line 2401242
    invoke-virtual {v2, v6}, LX/F9Y;->setPersistent(Z)V

    .line 2401243
    if-nez v10, :cond_9

    .line 2401244
    new-array v3, v12, [Ljava/lang/CharSequence;

    const-string v7, "There are no nux steps to show."

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, LX/F9Y;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401245
    new-array v3, v12, [Ljava/lang/CharSequence;

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, LX/F9Y;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401246
    :goto_2
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401247
    new-instance v2, LX/F9W;

    invoke-direct {v2, p0}, LX/F9W;-><init>(Landroid/content/Context;)V

    .line 2401248
    const-string v3, "Reset NUX status"

    invoke-virtual {v2, v3}, LX/F9W;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401249
    const-string v3, "Force current user into NUX eligibility for non-production sandbox"

    invoke-virtual {v2, v3}, LX/F9W;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401250
    const-class v3, LX/F9W;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/F9W;->setKey(Ljava/lang/String;)V

    .line 2401251
    new-instance v3, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$1;

    invoke-direct {v3, v2}, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$1;-><init>(LX/F9W;)V

    invoke-virtual {v2, v3}, LX/F9W;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401252
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401253
    new-instance v2, LX/F9U;

    invoke-direct {v2, p0}, LX/F9U;-><init>(Landroid/content/Context;)V

    .line 2401254
    invoke-virtual {v2}, LX/F9U;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2401255
    const-string v6, "Launch NUX"

    invoke-virtual {v2, v6}, LX/F9U;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401256
    const-string v6, "Launch this user\'s currently eligible NUX steps"

    invoke-virtual {v2, v6}, LX/F9U;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401257
    const-class v6, LX/F9U;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/F9U;->setKey(Ljava/lang/String;)V

    .line 2401258
    new-instance v6, LX/F9T;

    invoke-direct {v6, v2, v3}, LX/F9T;-><init>(LX/F9U;Landroid/content/Context;)V

    invoke-virtual {v2, v6}, LX/F9U;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401259
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401260
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401261
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401262
    const-string v2, "MQTT"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401263
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401264
    new-instance v2, LX/4ot;

    invoke-direct {v2, p0}, LX/4ot;-><init>(Landroid/content/Context;)V

    .line 2401265
    sget-object v3, LX/1p6;->b:LX/0Tn;

    invoke-virtual {v3}, LX/0To;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4ot;->setKey(Ljava/lang/String;)V

    .line 2401266
    const-string v3, "MQTT Server tier"

    invoke-virtual {v2, v3}, LX/4ot;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401267
    const-string v3, "default"

    invoke-virtual {v2, v3}, LX/4ot;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401268
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const-string v7, "default"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string v7, "sandbox"

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, LX/4ot;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401269
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const-string v7, "default"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string v7, "sandbox"

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, LX/4ot;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401270
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401271
    new-instance v2, LX/4on;

    invoke-direct {v2, p0}, LX/4on;-><init>(Landroid/content/Context;)V

    .line 2401272
    sget-object v3, LX/1p6;->c:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4om;->a(LX/0Tn;)V

    .line 2401273
    const-string v3, "MQTT Sandbox"

    invoke-virtual {v2, v3}, LX/4on;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401274
    const-string v3, "MQTT Sandbox"

    invoke-virtual {v2, v3}, LX/4on;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401275
    invoke-virtual {v2}, LX/4on;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    const-string v6, "user.devNN.prn2.facebook.com:8883"

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2401276
    invoke-virtual {v2}, LX/4on;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2401277
    invoke-virtual {v2}, LX/4on;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 2401278
    invoke-virtual {v2}, LX/4on;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 2401279
    if-nez v3, :cond_a

    .line 2401280
    :goto_3
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401281
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401282
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->g(Landroid/preference/PreferenceScreen;)V

    .line 2401283
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401284
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401285
    const-string v2, "FACEWEB"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401286
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401287
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string v6, "Clear cookies"

    aput-object v6, v2, v3

    const/4 v3, 0x1

    const-string v6, "Clear cache"

    aput-object v6, v2, v3

    invoke-virtual {v4, v2}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401288
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string v6, "cookies"

    aput-object v6, v2, v3

    const/4 v3, 0x1

    const-string v6, "cache"

    aput-object v6, v2, v3

    invoke-virtual {v4, v2}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401289
    const-string v2, "Reset webviews"

    invoke-virtual {v4, v2}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401290
    const-string v2, "Webview control"

    invoke-virtual {v4, v2}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401291
    const-string v2, "Clears webview cookies or caches"

    invoke-virtual {v4, v2}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401292
    const-string v2, "Clear"

    invoke-virtual {v4, v2}, LX/4or;->setPositiveButtonText(Ljava/lang/CharSequence;)V

    .line 2401293
    const-string v2, "Cancel"

    invoke-virtual {v4, v2}, LX/4or;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2401294
    sget-object v2, Lcom/facebook/katana/InternSettingsActivity;->aj:LX/0Tn;

    invoke-virtual {v4, v2}, LX/4or;->a(LX/0Tn;)V

    .line 2401295
    new-instance v2, LX/Gst;

    invoke-direct {v2, p0}, LX/Gst;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v4, v2}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401296
    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401297
    new-instance v2, LX/4om;

    invoke-direct {v2, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2401298
    sget-object v3, LX/0dU;->s:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4om;->a(LX/0Tn;)V

    .line 2401299
    const-string v3, ""

    invoke-virtual {v2, v3}, LX/4om;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401300
    const-string v3, "Weinre Server"

    invoke-virtual {v2, v3}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401301
    const-string v3, "Set weinre server hostname and port number "

    invoke-virtual {v2, v3}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401302
    const-string v3, "Weinre Server"

    invoke-virtual {v2, v3}, LX/4om;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401303
    invoke-virtual {v2}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    .line 2401304
    const-string v4, "(e.g., 172.16.108.12:8081)"

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2401305
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2401306
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401307
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401308
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401309
    const-string v2, "UI Performance"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401310
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401311
    new-instance v2, LX/Gb2;

    invoke-direct {v2, p0}, LX/Gb2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401312
    new-instance v2, LX/Gan;

    invoke-direct {v2, p0}, LX/Gan;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401313
    new-instance v2, LX/Gb0;

    invoke-direct {v2, p0}, LX/Gb0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401314
    new-instance v2, LX/Gax;

    invoke-direct {v2, p0}, LX/Gax;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401315
    new-instance v2, LX/Gay;

    invoke-direct {v2, p0}, LX/Gay;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401316
    new-instance v2, LX/Gaz;

    invoke-direct {v2, p0}, LX/Gaz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401317
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401318
    sget-object v3, LX/6Un;->b:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401319
    const-string v3, "Enable Runtime UI Linter"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401320
    const-string v3, "If enabled, the runtime UI linter will periodically check the UI for errors."

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401321
    sget-object v3, LX/6Un;->c:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401322
    new-instance v3, LX/Gsu;

    invoke-direct {v3, p0}, LX/Gsu;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v2, v3}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401323
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401324
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401325
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401326
    const-string v2, "HTTP Prefs"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401327
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401328
    const/4 v6, 0x1

    .line 2401329
    new-instance v2, LX/4on;

    invoke-direct {v2, p0}, LX/4on;-><init>(Landroid/content/Context;)V

    .line 2401330
    sget-object v3, LX/0dU;->l:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4om;->a(LX/0Tn;)V

    .line 2401331
    const v3, 0x7f0835fd

    invoke-virtual {v2, v3}, LX/4on;->setTitle(I)V

    .line 2401332
    const v3, 0x7f0835fe

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2401333
    iput-object v3, v2, LX/4on;->b:Ljava/lang/CharSequence;

    .line 2401334
    invoke-static {v2}, LX/4on;->b(LX/4on;)V

    .line 2401335
    const v3, 0x7f0835ff

    invoke-virtual {v2, v3}, LX/4on;->setDialogTitle(I)V

    .line 2401336
    invoke-virtual {v2}, LX/4on;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    const v4, 0x7f083600

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 2401337
    invoke-virtual {v2}, LX/4on;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2401338
    invoke-virtual {v2}, LX/4on;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 2401339
    move-object v2, v2

    .line 2401340
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401341
    new-instance v2, LX/FAL;

    iget-object v3, p0, Lcom/facebook/katana/InternSettingsActivity;->H:LX/FAK;

    invoke-direct {v2, p0, v3}, LX/FAL;-><init>(Landroid/content/Context;LX/FAK;)V

    .line 2401342
    invoke-virtual {v2}, LX/4ot;->getKey()Ljava/lang/String;

    move-result-object v4

    .line 2401343
    if-nez v4, :cond_b

    .line 2401344
    :goto_4
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401345
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401346
    sget-object v3, LX/0dU;->b:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401347
    const-string v3, "PHP Request profiling"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401348
    const-string v3, "Enable PHP Profiling on all requests"

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401349
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401350
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401351
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401352
    sget-object v3, LX/0dU;->c:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401353
    const-string v3, "Wirehog Request Profiling"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401354
    const-string v3, "Enable Wirehog Profiling on all requests"

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401355
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401356
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401357
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401358
    sget-object v3, LX/0dU;->d:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401359
    const-string v3, "Teak Request Profiling"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401360
    const-string v3, "Enable Teak Profiling on all requests"

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401361
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401362
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401363
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401364
    sget-object v3, LX/0dU;->e:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401365
    const-string v3, "FB Request Tracing"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401366
    const-string v3, "Enable FB Tracing on all requests"

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401367
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401368
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401369
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401370
    sget-object v3, LX/0dU;->f:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401371
    const-string v3, "Artillery Tracing"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401372
    const-string v3, "Enable Artillery tracing on all requests"

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401373
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401374
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401375
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401376
    sget-object v3, LX/0dU;->g:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401377
    const-string v3, "Show requests queue"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401378
    const-string v3, "Restart may be required"

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401379
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401380
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401381
    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->ad:LX/1ER;

    invoke-virtual {v2}, LX/1ER;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2401382
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401383
    sget-object v3, LX/0dU;->m:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401384
    const-string v3, "Enable 2G Empathy"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401385
    const-string v3, "Simulates 2G network timeouts and errors"

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401386
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401387
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401388
    :cond_3
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401389
    sget-object v3, LX/0dU;->n:LX/0Tn;

    invoke-virtual {v2, v3}, LX/4oi;->a(LX/0Tn;)V

    .line 2401390
    const-string v3, "Print liger trace events in logcat"

    invoke-virtual {v2, v3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401391
    const-string v3, "Turn on to print all Liger trace events in logcat."

    invoke-virtual {v2, v3}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401392
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401393
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401394
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401395
    new-instance v2, Lcom/facebook/katana/InternSettingsActivity$16;

    invoke-direct {v2, p0, v0, v1, v5}, Lcom/facebook/katana/InternSettingsActivity$16;-><init>(Lcom/facebook/katana/InternSettingsActivity;LX/4oi;LX/4oi;LX/4oq;)V

    .line 2401396
    sget-object v1, LX/0l2;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2401397
    const-string v1, "Monkey mode"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401398
    new-instance v1, LX/Gsv;

    invoke-direct {v1, p0, v2}, LX/Gsv;-><init>(Lcom/facebook/katana/InternSettingsActivity;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401399
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401400
    const-string v1, "Disables Logout, Crash, Report Bug, and this preference."

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401401
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401402
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401403
    sget-object v1, Lcom/facebook/performancelogger/PerformanceLogger;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2401404
    const-string v1, "Show PerfMarker in LogCat"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401405
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->A:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1}, Lcom/facebook/performancelogger/PerformanceLogger;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401406
    const-string v1, "Collect all PerfMarkers and report timestamp and elapsed time to LogCat"

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401407
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401408
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401409
    sget-object v1, LX/0id;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2401410
    const-string v1, "Display Perf Numbers On Screen"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401411
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401412
    const-string v1, "Display real time perf numbers on screen. Restart app to make it takes effect."

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401413
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401414
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401415
    sget-object v1, LX/44G;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2401416
    const v1, 0x7f080f26

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(I)V

    .line 2401417
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401418
    new-instance v1, LX/Gsw;

    invoke-direct {v1, p0}, LX/Gsw;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401419
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401420
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401421
    sget-object v1, LX/14A;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2401422
    const v1, 0x7f080f27

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(I)V

    .line 2401423
    const-string v1, "Show navigation events when there is a page transition"

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401424
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401425
    new-instance v0, LX/4or;

    invoke-direct {v0, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2401426
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0uQ;->f:LX/0Tn;

    const-wide/16 v4, -0x1

    invoke-interface {v1, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 2401427
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const-string v5, "1 second"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "5 seconds"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "30 seconds"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "1 minute"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "2 minutes"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "Unset"

    aput-object v5, v3, v4

    .line 2401428
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "5"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "30"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "60"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "120"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "-1"

    aput-object v6, v4, v5

    .line 2401429
    invoke-virtual {v0, v3}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401430
    invoke-virtual {v0, v4}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401431
    invoke-virtual {v0, v1}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401432
    sget-object v1, Lcom/facebook/katana/InternSettingsActivity;->al:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4or;->a(LX/0Tn;)V

    .line 2401433
    const-string v1, "Device Status Event Interval"

    invoke-virtual {v0, v1}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401434
    const-string v1, "Time between Device Status analytics events"

    invoke-virtual {v0, v1}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401435
    new-instance v1, LX/Gsx;

    invoke-direct {v1, p0}, LX/Gsx;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401436
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401437
    new-instance v0, LX/4or;

    invoke-direct {v0, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2401438
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0uQ;->b:LX/0Tn;

    const-wide/16 v4, -0x1

    invoke-interface {v1, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 2401439
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const-string v5, "1 second"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "5 seconds"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "30 seconds"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "1 minute"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "2 minutes"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "Unset"

    aput-object v5, v3, v4

    .line 2401440
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "5"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "30"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "60"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "120"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "-1"

    aput-object v6, v4, v5

    .line 2401441
    invoke-virtual {v0, v3}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401442
    invoke-virtual {v0, v4}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401443
    invoke-virtual {v0, v1}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401444
    sget-object v1, Lcom/facebook/katana/InternSettingsActivity;->am:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4or;->a(LX/0Tn;)V

    .line 2401445
    const-string v1, "Process Status Event Interval"

    invoke-virtual {v0, v1}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401446
    const-string v1, "Time between Process Status analytics events"

    invoke-virtual {v0, v1}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401447
    new-instance v1, LX/Gsz;

    invoke-direct {v1, p0}, LX/Gsz;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401448
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401449
    new-instance v0, LX/8wc;

    invoke-direct {v0, p0}, LX/8wc;-><init>(Landroid/content/Context;)V

    .line 2401450
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401451
    new-instance v0, LX/4ot;

    invoke-direct {v0, p0}, LX/4ot;-><init>(Landroid/content/Context;)V

    .line 2401452
    sget-object v1, LX/0eJ;->c:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ot;->setKey(Ljava/lang/String;)V

    .line 2401453
    const v1, 0x7f083455

    invoke-virtual {v0, v1}, LX/4ot;->setTitle(I)V

    .line 2401454
    const-string v1, "-1"

    invoke-virtual {v0, v1}, LX/4ot;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401455
    const v1, 0x7f100049

    invoke-virtual {v0, v1}, LX/4ot;->setEntries(I)V

    .line 2401456
    const v1, 0x7f10004a

    invoke-virtual {v0, v1}, LX/4ot;->setEntryValues(I)V

    .line 2401457
    move-object v0, v0

    .line 2401458
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401459
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401460
    sget-object v1, LX/0eJ;->e:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setKey(Ljava/lang/String;)V

    .line 2401461
    const v1, 0x7f083459

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(I)V

    .line 2401462
    move-object v0, v0

    .line 2401463
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401464
    new-instance v0, LX/8wY;

    invoke-direct {v0, p0}, LX/8wY;-><init>(Landroid/content/Context;)V

    .line 2401465
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401466
    new-instance v0, LX/Bhe;

    invoke-direct {v0, p0}, LX/Bhe;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401467
    new-instance v0, LX/7mt;

    invoke-direct {v0, p0}, LX/7mt;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401468
    new-instance v0, LX/7mp;

    invoke-direct {v0, p0}, LX/7mp;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401469
    new-instance v0, LX/4or;

    invoke-direct {v0, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2401470
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string v4, "Upload"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "Download"

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401471
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string v4, "upload"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "download"

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401472
    const-string v1, "Contacts menu"

    invoke-virtual {v0, v1}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401473
    const-string v1, "Contacts (Rolodex) control"

    invoke-virtual {v0, v1}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401474
    const-string v1, "Trigger contacts uploads/downloads"

    invoke-virtual {v0, v1}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401475
    const-string v1, "Start"

    invoke-virtual {v0, v1}, LX/4or;->setPositiveButtonText(Ljava/lang/CharSequence;)V

    .line 2401476
    const-string v1, "Cancel"

    invoke-virtual {v0, v1}, LX/4or;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2401477
    sget-object v1, Lcom/facebook/katana/InternSettingsActivity;->ak:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4or;->a(LX/0Tn;)V

    .line 2401478
    new-instance v1, LX/Gt0;

    invoke-direct {v1, p0}, LX/Gt0;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401479
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401480
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->x:LX/7Tx;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401481
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->c:LX/0l2;

    invoke-virtual {v0}, LX/0l2;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2401482
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 2401483
    :cond_4
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401484
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ac:LX/Gvj;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401485
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401486
    new-instance v0, LX/BQN;

    invoke-direct {v0, p0}, LX/BQN;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401487
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401488
    new-instance v0, LX/Gvl;

    invoke-direct {v0, p0}, LX/Gvl;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401489
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401490
    new-instance v0, LX/GbB;

    invoke-direct {v0, p0}, LX/GbB;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401491
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401492
    new-instance v0, LX/GTU;

    invoke-direct {v0, p0}, LX/GTU;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401493
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401494
    new-instance v0, LX/Gsm;

    invoke-direct {v0, p0}, LX/Gsm;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401495
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401496
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->S:LX/3y7;

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceGroup;LX/3y7;)V

    .line 2401497
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401498
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->Z:LX/AP0;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401499
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401500
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->T:LX/3y7;

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceGroup;LX/3y7;)V

    .line 2401501
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->U:LX/31K;

    .line 2401502
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401503
    const-string v2, "New SelfUpdate"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401504
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401505
    new-instance v2, LX/4ok;

    invoke-direct {v2, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401506
    const-string v3, "AppUpdateLib Logging"

    invoke-virtual {v2, v3}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401507
    const-string v3, "Log tag in logcat: AppUpdateLib"

    invoke-virtual {v2, v3}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401508
    sget-boolean v3, LX/EeM;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401509
    new-instance v3, LX/Hbz;

    invoke-direct {v3, v0}, LX/Hbz;-><init>(LX/31K;)V

    invoke-virtual {v2, v3}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401510
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401511
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401512
    const-string v3, "Start SelfUpdate Now"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401513
    const-string v3, "Resets timeouts and starts a new selfupdate immediately."

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401514
    new-instance v3, LX/Hc0;

    invoke-direct {v3, v0, p0, v2}, LX/Hc0;-><init>(LX/31K;Landroid/app/Activity;Landroid/preference/Preference;)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401515
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401516
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401517
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401518
    const-string v1, "Memory"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401519
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401520
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 2401521
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2401522
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401523
    const v4, 0x7f083456

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(I)V

    .line 2401524
    const v4, 0x7f083457

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401525
    new-instance v4, LX/Gat;

    invoke-direct {v4, v1, v2, p0}, LX/Gat;-><init>(Ljava/lang/String;Landroid/content/res/Resources;Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401526
    move-object v1, v3

    .line 2401527
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401528
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401529
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401530
    const-string v1, "Composer Nux"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401531
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401532
    new-instance v1, LX/ASA;

    invoke-direct {v1, p0}, LX/ASA;-><init>(Landroid/content/Context;)V

    .line 2401533
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401534
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401535
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->V:LX/3y7;

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceGroup;LX/3y7;)V

    .line 2401536
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401537
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401538
    const-string v1, "Composer Frames Nux"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401539
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401540
    new-instance v1, LX/9db;

    invoke-direct {v1, p0}, LX/9db;-><init>(Landroid/content/Context;)V

    .line 2401541
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401542
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401543
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401544
    const-string v1, "GraphQL"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401545
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401546
    new-instance v1, LX/Gif;

    invoke-direct {v1, p0}, LX/Gif;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401547
    new-instance v1, LX/Gih;

    invoke-direct {v1, p0}, LX/Gih;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401548
    new-instance v1, LX/Gij;

    invoke-direct {v1, p0}, LX/Gij;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401549
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401550
    new-instance v0, LX/GzL;

    invoke-direct {v0, p0}, LX/GzL;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401551
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401552
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401553
    const-string v1, "URI Widget"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401554
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401555
    new-instance v1, LX/Gvm;

    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->e:LX/17W;

    iget-object v3, p0, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v1, p0, v2, v3}, LX/Gvm;-><init>(Landroid/content/Context;LX/17W;Lcom/facebook/content/SecureContextHelper;)V

    .line 2401556
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401557
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401558
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->W:LX/3y7;

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceGroup;LX/3y7;)V

    .line 2401559
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401560
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->X:LX/3y7;

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceGroup;LX/3y7;)V

    .line 2401561
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401562
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401563
    const-string v1, "Rapid Feedback - Internal"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401564
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401565
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->F:LX/HYU;

    const-string v2, "Test/Debug Surveys with their IDs"

    invoke-virtual {v1, v2}, LX/HYU;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401566
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->F:LX/HYU;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401567
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401568
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401569
    const-string v1, "Security Checkup - Internal"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401570
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401571
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401572
    const-string v2, "Security Checkup"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401573
    const-string v2, "Launch Security Checkup for test/debug"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401574
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2401575
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401576
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401577
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401578
    const-string v1, "Platform"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401579
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401580
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401581
    sget-object v2, LX/2QS;->c:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2401582
    const-string v2, "Reset Manifest refresh per Action"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401583
    const-string v2, "Discards the cached manifest for each dialog invocation"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401584
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401585
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401586
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401587
    sget-object v2, LX/2QS;->d:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2401588
    const-string v2, "Disable Manifest Refresh"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401589
    const-string v2, "Stops the manifest from refreshing without a dialog invocation (happens on cold start)"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401590
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401591
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401592
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401593
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401594
    const-string v1, "Privacy"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401595
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401596
    new-instance v1, LX/8Q1;

    invoke-direct {v1, p0}, LX/8Q1;-><init>(Landroid/content/Context;)V

    .line 2401597
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401598
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401599
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401600
    const-string v1, "Sounds"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401601
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401602
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401603
    sget-object v2, LX/13d;->b:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2401604
    const v2, 0x7f083444

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(I)V

    .line 2401605
    const v2, 0x7f083445

    invoke-virtual {v1, v2}, LX/4oi;->setSummaryOn(I)V

    .line 2401606
    const v2, 0x7f083446

    invoke-virtual {v1, v2}, LX/4oi;->setSummaryOff(I)V

    .line 2401607
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401608
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401609
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401610
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401611
    const-string v1, "Search"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401612
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401613
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401614
    const-string v2, "Debug Settings"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401615
    const-string v2, "Control search settings"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401616
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2401617
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401618
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401619
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401620
    const-string v1, "Location"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401621
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401622
    new-instance v3, LX/4or;

    invoke-direct {v3, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2401623
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->n:LX/6ZG;

    .line 2401624
    iget-object v1, v0, LX/6ZG;->a:LX/1wY;

    iget-object v2, v0, LX/6ZG;->b:LX/03V;

    .line 2401625
    invoke-static {v1, v2}, LX/2vc;->b(LX/1wY;LX/03V;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 2401626
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2401627
    invoke-static {}, LX/2ve;->values()[LX/2ve;

    move-result-object v6

    array-length v7, v6

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v7, :cond_6

    aget-object v8, v6, v4

    .line 2401628
    sget-object v0, LX/2ve;->GOOGLE_PLAY_PREF:LX/2ve;

    if-eq v8, v0, :cond_5

    .line 2401629
    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2401630
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 2401631
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2401632
    :goto_6
    move-object v1, v4

    .line 2401633
    move-object v1, v1

    .line 2401634
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2sj;->b:LX/0Tn;

    sget-object v4, LX/2ve;->DEFAULT:LX/2ve;

    iget v4, v4, LX/2ve;->key:I

    invoke-interface {v0, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    .line 2401635
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    .line 2401636
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v6, v0, [Ljava/lang/String;

    .line 2401637
    const/4 v0, 0x0

    .line 2401638
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v0

    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ve;

    .line 2401639
    iget-object v2, v0, LX/2ve;->name:Ljava/lang/String;

    aput-object v2, v5, v1

    .line 2401640
    add-int/lit8 v2, v1, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, v0, LX/2ve;->key:I

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    move v1, v2

    .line 2401641
    goto :goto_7

    .line 2401642
    :cond_7
    invoke-virtual {v3, v5}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401643
    invoke-virtual {v3, v6}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401644
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401645
    const-string v0, "Location Implementation"

    invoke-virtual {v3, v0}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401646
    const-string v0, "Choice of Standard or Mock Location Implementations for testing purposes (Restart is recommended)"

    invoke-virtual {v3, v0}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401647
    new-instance v0, LX/Gt1;

    invoke-direct {v0, p0}, LX/Gt1;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v3, v0}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401648
    invoke-virtual {p1, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401649
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->y:LX/Gaw;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401650
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401651
    new-instance v0, LX/FUz;

    invoke-direct {v0, p0}, LX/FUz;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401652
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401653
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401654
    const-string v1, "Fundraisers"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401655
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401656
    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->i(Landroid/preference/PreferenceGroup;)V

    .line 2401657
    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->j(Landroid/preference/PreferenceGroup;)V

    .line 2401658
    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->k(Landroid/preference/PreferenceGroup;)V

    .line 2401659
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401660
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->l(Landroid/preference/PreferenceGroup;)V

    .line 2401661
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401662
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401663
    const-string v1, "Method Tracing"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401664
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401665
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401666
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->v:LX/4mE;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401667
    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->c(Landroid/preference/PreferenceGroup;)V

    .line 2401668
    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->d(Landroid/preference/PreferenceGroup;)V

    .line 2401669
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401670
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->g(Landroid/preference/PreferenceGroup;)V

    .line 2401671
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401672
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->c(Landroid/preference/PreferenceScreen;)V

    .line 2401673
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401674
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->L:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2401675
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401676
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->d(Landroid/preference/PreferenceScreen;)V

    .line 2401677
    :cond_8
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->e(Landroid/preference/PreferenceScreen;)V

    .line 2401678
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401679
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->f(Landroid/preference/PreferenceScreen;)V

    .line 2401680
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401681
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->h(Landroid/preference/PreferenceScreen;)V

    .line 2401682
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401683
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->j(Landroid/preference/PreferenceScreen;)V

    .line 2401684
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401685
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->k(Landroid/preference/PreferenceScreen;)V

    .line 2401686
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->l(Landroid/preference/PreferenceScreen;)V

    .line 2401687
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceScreen;)V

    .line 2401688
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->n(Landroid/preference/PreferenceGroup;)V

    .line 2401689
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401690
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->b(Landroid/preference/PreferenceScreen;)V

    .line 2401691
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2401692
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->i(Landroid/preference/PreferenceScreen;)V

    .line 2401693
    invoke-direct {p0}, Lcom/facebook/katana/InternSettingsActivity;->d()V

    .line 2401694
    return-void

    .line 2401695
    :cond_9
    new-array v3, v10, [Ljava/lang/CharSequence;

    invoke-interface {v8, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, LX/F9Y;->setEntries([Ljava/lang/CharSequence;)V

    .line 2401696
    new-array v3, v10, [Ljava/lang/CharSequence;

    invoke-interface {v9, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, LX/F9Y;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2401697
    invoke-virtual {v2}, LX/F9Y;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2401698
    new-instance v6, LX/F9X;

    invoke-direct {v6, v2, v3}, LX/F9X;-><init>(LX/F9Y;Landroid/content/Context;)V

    .line 2401699
    invoke-virtual {v2, v6}, LX/F9Y;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401700
    goto/16 :goto_2

    .line 2401701
    :cond_a
    iget-object v6, v2, LX/4on;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v7, v2, LX/4on;->a:LX/0dN;

    invoke-interface {v6, v3, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/String;LX/0dN;)V

    goto/16 :goto_3

    .line 2401702
    :cond_b
    iget-object v3, v2, LX/4ot;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2401703
    iget-object v6, v2, LX/4ot;->b:LX/0dN;

    invoke-interface {v3, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/String;LX/0dN;)V

    goto/16 :goto_4

    :cond_c
    invoke-static {}, LX/2ve;->values()[LX/2ve;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    goto/16 :goto_6
.end method

.method private static a(Lcom/facebook/katana/InternSettingsActivity;LX/1Bf;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0l2;LX/6Uq;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/0dC;LX/2Gc;LX/2Gq;LX/2Bz;Ljava/util/concurrent/ExecutorService;LX/3Q7;LX/1Mj;LX/6ZG;LX/0Or;LX/94q;Lcom/facebook/contactlogs/ContactLogsUploadRunner;LX/EjM;Lcom/facebook/contacts/upload/ContinuousContactUploadClient;LX/03V;Landroid/content/Context;LX/4mE;LX/0YI;LX/7Tx;LX/Gaw;LX/Hhn;Lcom/facebook/performancelogger/PerformanceLogger;LX/44G;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/0Y9;LX/D39;LX/HYU;LX/1C2;LX/FAK;LX/0ad;LX/0Zb;LX/17X;LX/0Or;LX/0Ot;LX/GiU;LX/0Xl;Ljava/lang/Boolean;LX/CIm;LX/3y7;LX/3y7;LX/3y7;LX/31K;LX/3y7;LX/3y7;LX/3y7;LX/2l5;LX/AP0;LX/7Wg;LX/Bhy;LX/Gvj;LX/1ER;LX/0aG;LX/D3X;LX/D3Z;LX/0Uh;LX/FmM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/InternSettingsActivity;",
            "LX/1Bf;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0l2;",
            "LX/6Uq;",
            "LX/17W;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/2Gc;",
            "LX/2Gq;",
            "LX/2Bz;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/3Q7;",
            "LX/1Mj;",
            "LX/6ZG;",
            "LX/0Or",
            "<",
            "LX/GaL;",
            ">;",
            "LX/94q;",
            "Lcom/facebook/contactlogs/ContactLogsUploadRunner;",
            "LX/EjM;",
            "Lcom/facebook/contacts/upload/ContinuousContactUploadClient;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/Context;",
            "LX/4mE;",
            "LX/0YI;",
            "LX/7Tx;",
            "LX/Gaw;",
            "LX/Hhn;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/44G;",
            "Lcom/facebook/contacts/background/AddressBookPeriodicRunner;",
            "LX/0Y9;",
            "LX/D39;",
            "LX/HYU;",
            "LX/1C2;",
            "LX/FAK;",
            "LX/0ad;",
            "LX/0Zb;",
            "LX/17X;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;",
            "LX/GiU;",
            "LX/0Xl;",
            "Ljava/lang/Boolean;",
            "LX/CIm;",
            "LX/3y7;",
            "LX/3y7;",
            "LX/3y7;",
            "LX/31K;",
            "LX/3y7;",
            "LX/3y7;",
            "LX/3y7;",
            "LX/2l5;",
            "LX/AP0;",
            "LX/7Wg;",
            "LX/Bhy;",
            "LX/Gvj;",
            "LX/1ER;",
            "LX/0aG;",
            "LX/D3X;",
            "LX/D3Z;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/FmM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2401704
    iput-object p1, p0, Lcom/facebook/katana/InternSettingsActivity;->a:LX/1Bf;

    iput-object p2, p0, Lcom/facebook/katana/InternSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/katana/InternSettingsActivity;->c:LX/0l2;

    iput-object p4, p0, Lcom/facebook/katana/InternSettingsActivity;->d:LX/6Uq;

    iput-object p5, p0, Lcom/facebook/katana/InternSettingsActivity;->e:LX/17W;

    iput-object p6, p0, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object p7, p0, Lcom/facebook/katana/InternSettingsActivity;->g:LX/0dC;

    iput-object p8, p0, Lcom/facebook/katana/InternSettingsActivity;->h:LX/2Gc;

    iput-object p9, p0, Lcom/facebook/katana/InternSettingsActivity;->i:LX/2Gq;

    iput-object p10, p0, Lcom/facebook/katana/InternSettingsActivity;->j:LX/2Bz;

    iput-object p11, p0, Lcom/facebook/katana/InternSettingsActivity;->k:Ljava/util/concurrent/ExecutorService;

    iput-object p12, p0, Lcom/facebook/katana/InternSettingsActivity;->l:LX/3Q7;

    iput-object p13, p0, Lcom/facebook/katana/InternSettingsActivity;->m:LX/1Mj;

    iput-object p14, p0, Lcom/facebook/katana/InternSettingsActivity;->n:LX/6ZG;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->o:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->p:LX/94q;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->q:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->r:LX/EjM;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->s:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->t:LX/03V;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->v:LX/4mE;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->w:LX/0YI;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->x:LX/7Tx;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->y:LX/Gaw;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->z:LX/Hhn;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->A:Lcom/facebook/performancelogger/PerformanceLogger;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->B:LX/44G;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->C:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->D:LX/0Y9;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->E:LX/D39;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->F:LX/HYU;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->G:LX/1C2;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->H:LX/FAK;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->I:LX/0ad;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->J:LX/0Zb;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->K:LX/17X;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->L:LX/0Or;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->M:LX/0Ot;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->N:LX/GiU;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->O:LX/0Xl;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->P:Ljava/lang/Boolean;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->Q:LX/CIm;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->R:LX/3y7;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->S:LX/3y7;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->T:LX/3y7;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->U:LX/31K;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->V:LX/3y7;

    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->W:LX/3y7;

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->X:LX/3y7;

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->Y:LX/2l5;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->Z:LX/AP0;

    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->aa:LX/7Wg;

    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ab:LX/Bhy;

    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ac:LX/Gvj;

    move-object/from16 v0, p56

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ad:LX/1ER;

    move-object/from16 v0, p57

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ae:LX/0aG;

    move-object/from16 v0, p58

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->af:LX/D3X;

    move-object/from16 v0, p59

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ag:LX/D3Z;

    move-object/from16 v0, p60

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ah:LX/0Uh;

    move-object/from16 v0, p61

    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ai:LX/FmM;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 64

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v63

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/katana/InternSettingsActivity;

    invoke-static/range {v63 .. v63}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v3

    check-cast v3, LX/1Bf;

    invoke-static/range {v63 .. v63}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v63 .. v63}, LX/0l2;->a(LX/0QB;)LX/0l2;

    move-result-object v5

    check-cast v5, LX/0l2;

    invoke-static/range {v63 .. v63}, LX/6Uq;->a(LX/0QB;)LX/6Uq;

    move-result-object v6

    check-cast v6, LX/6Uq;

    invoke-static/range {v63 .. v63}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v7

    check-cast v7, LX/17W;

    invoke-static/range {v63 .. v63}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v63 .. v63}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v9

    check-cast v9, LX/0dC;

    invoke-static/range {v63 .. v63}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v10

    check-cast v10, LX/2Gc;

    invoke-static/range {v63 .. v63}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v11

    check-cast v11, LX/2Gq;

    invoke-static/range {v63 .. v63}, LX/2Bz;->a(LX/0QB;)LX/2Bz;

    move-result-object v12

    check-cast v12, LX/2Bz;

    invoke-static/range {v63 .. v63}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v63 .. v63}, LX/3Q7;->a(LX/0QB;)LX/3Q7;

    move-result-object v14

    check-cast v14, LX/3Q7;

    invoke-static/range {v63 .. v63}, LX/1Mj;->a(LX/0QB;)LX/1Mj;

    move-result-object v15

    check-cast v15, LX/1Mj;

    invoke-static/range {v63 .. v63}, LX/6ZG;->a(LX/0QB;)LX/6ZG;

    move-result-object v16

    check-cast v16, LX/6ZG;

    const/16 v17, 0x1912

    move-object/from16 v0, v63

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {v63 .. v63}, LX/94q;->a(LX/0QB;)LX/94q;

    move-result-object v18

    check-cast v18, LX/94q;

    invoke-static/range {v63 .. v63}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->a(LX/0QB;)Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    move-result-object v19

    check-cast v19, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-static/range {v63 .. v63}, LX/EjM;->a(LX/0QB;)LX/EjM;

    move-result-object v20

    check-cast v20, LX/EjM;

    invoke-static/range {v63 .. v63}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(LX/0QB;)Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    move-result-object v21

    check-cast v21, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-static/range {v63 .. v63}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v22

    check-cast v22, LX/03V;

    const-class v23, Landroid/content/Context;

    move-object/from16 v0, v63

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/content/Context;

    invoke-static/range {v63 .. v63}, LX/4mE;->a(LX/0QB;)LX/4mE;

    move-result-object v24

    check-cast v24, LX/4mE;

    invoke-static/range {v63 .. v63}, LX/0YI;->a(LX/0QB;)LX/0YI;

    move-result-object v25

    check-cast v25, LX/0YI;

    invoke-static/range {v63 .. v63}, LX/7Tx;->a(LX/0QB;)LX/7Tx;

    move-result-object v26

    check-cast v26, LX/7Tx;

    invoke-static/range {v63 .. v63}, LX/Gaw;->a(LX/0QB;)LX/Gaw;

    move-result-object v27

    check-cast v27, LX/Gaw;

    invoke-static/range {v63 .. v63}, LX/Hhn;->a(LX/0QB;)LX/Hhn;

    move-result-object v28

    check-cast v28, LX/Hhn;

    invoke-static/range {v63 .. v63}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v29

    check-cast v29, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {v63 .. v63}, LX/44G;->a(LX/0QB;)LX/44G;

    move-result-object v30

    check-cast v30, LX/44G;

    invoke-static/range {v63 .. v63}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v31

    check-cast v31, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static/range {v63 .. v63}, LX/0Y9;->a(LX/0QB;)LX/0Y9;

    move-result-object v32

    check-cast v32, LX/0Y9;

    invoke-static/range {v63 .. v63}, LX/D39;->a(LX/0QB;)LX/D39;

    move-result-object v33

    check-cast v33, LX/D39;

    invoke-static/range {v63 .. v63}, LX/HYU;->a(LX/0QB;)LX/HYU;

    move-result-object v34

    check-cast v34, LX/HYU;

    invoke-static/range {v63 .. v63}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v35

    check-cast v35, LX/1C2;

    invoke-static/range {v63 .. v63}, LX/FAK;->a(LX/0QB;)LX/FAK;

    move-result-object v36

    check-cast v36, LX/FAK;

    invoke-static/range {v63 .. v63}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v37

    check-cast v37, LX/0ad;

    invoke-static/range {v63 .. v63}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v38

    check-cast v38, LX/0Zb;

    invoke-static/range {v63 .. v63}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v39

    check-cast v39, LX/17X;

    const/16 v40, 0x1439

    move-object/from16 v0, v63

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v40

    const/16 v41, 0x8a

    move-object/from16 v0, v63

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v41

    invoke-static/range {v63 .. v63}, LX/GiU;->a(LX/0QB;)LX/GiU;

    move-result-object v42

    check-cast v42, LX/GiU;

    invoke-static/range {v63 .. v63}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v43

    check-cast v43, LX/0Xl;

    invoke-static/range {v63 .. v63}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v44

    check-cast v44, Ljava/lang/Boolean;

    invoke-static/range {v63 .. v63}, LX/CIm;->a(LX/0QB;)LX/CIm;

    move-result-object v45

    check-cast v45, LX/CIm;

    invoke-static/range {v63 .. v63}, LX/3y6;->a(LX/0QB;)LX/3y7;

    move-result-object v46

    check-cast v46, LX/3y7;

    invoke-static/range {v63 .. v63}, LX/76f;->a(LX/0QB;)LX/76f;

    move-result-object v47

    check-cast v47, LX/3y7;

    invoke-static/range {v63 .. v63}, LX/Fju;->a(LX/0QB;)LX/Fju;

    move-result-object v48

    check-cast v48, LX/3y7;

    invoke-static/range {v63 .. v63}, LX/31K;->a(LX/0QB;)LX/31K;

    move-result-object v49

    check-cast v49, LX/31K;

    invoke-static/range {v63 .. v63}, LX/FZv;->a(LX/0QB;)LX/3y7;

    move-result-object v50

    check-cast v50, LX/3y7;

    invoke-static/range {v63 .. v63}, LX/AlA;->a(LX/0QB;)LX/3y7;

    move-result-object v51

    check-cast v51, LX/3y7;

    invoke-static/range {v63 .. v63}, LX/GjE;->a(LX/0QB;)LX/GjE;

    move-result-object v52

    check-cast v52, LX/3y7;

    invoke-static/range {v63 .. v63}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v53

    check-cast v53, LX/2l5;

    invoke-static/range {v63 .. v63}, LX/AP0;->a(LX/0QB;)LX/AP0;

    move-result-object v54

    check-cast v54, LX/AP0;

    invoke-static/range {v63 .. v63}, LX/7Wg;->a(LX/0QB;)LX/7Wg;

    move-result-object v55

    check-cast v55, LX/7Wg;

    invoke-static/range {v63 .. v63}, LX/Bhy;->a(LX/0QB;)LX/Bhy;

    move-result-object v56

    check-cast v56, LX/Bhy;

    invoke-static/range {v63 .. v63}, LX/Gvj;->a(LX/0QB;)LX/Gvj;

    move-result-object v57

    check-cast v57, LX/Gvj;

    invoke-static/range {v63 .. v63}, LX/1ER;->a(LX/0QB;)LX/1ER;

    move-result-object v58

    check-cast v58, LX/1ER;

    invoke-static/range {v63 .. v63}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v59

    check-cast v59, LX/0aG;

    invoke-static/range {v63 .. v63}, LX/D3X;->a(LX/0QB;)LX/D3X;

    move-result-object v60

    check-cast v60, LX/D3X;

    invoke-static/range {v63 .. v63}, LX/D3Z;->a(LX/0QB;)LX/D3Z;

    move-result-object v61

    check-cast v61, LX/D3Z;

    invoke-static/range {v63 .. v63}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v62

    check-cast v62, LX/0Uh;

    invoke-static/range {v63 .. v63}, LX/FmM;->a(LX/0QB;)LX/FmM;

    move-result-object v63

    check-cast v63, LX/FmM;

    invoke-static/range {v2 .. v63}, Lcom/facebook/katana/InternSettingsActivity;->a(Lcom/facebook/katana/InternSettingsActivity;LX/1Bf;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0l2;LX/6Uq;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/0dC;LX/2Gc;LX/2Gq;LX/2Bz;Ljava/util/concurrent/ExecutorService;LX/3Q7;LX/1Mj;LX/6ZG;LX/0Or;LX/94q;Lcom/facebook/contactlogs/ContactLogsUploadRunner;LX/EjM;Lcom/facebook/contacts/upload/ContinuousContactUploadClient;LX/03V;Landroid/content/Context;LX/4mE;LX/0YI;LX/7Tx;LX/Gaw;LX/Hhn;Lcom/facebook/performancelogger/PerformanceLogger;LX/44G;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/0Y9;LX/D39;LX/HYU;LX/1C2;LX/FAK;LX/0ad;LX/0Zb;LX/17X;LX/0Or;LX/0Ot;LX/GiU;LX/0Xl;Ljava/lang/Boolean;LX/CIm;LX/3y7;LX/3y7;LX/3y7;LX/31K;LX/3y7;LX/3y7;LX/3y7;LX/2l5;LX/AP0;LX/7Wg;LX/Bhy;LX/Gvj;LX/1ER;LX/0aG;LX/D3X;LX/D3Z;LX/0Uh;LX/FmM;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 2401767
    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2401768
    invoke-virtual {p4, p3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401769
    :cond_0
    return-void

    .line 2401770
    :cond_1
    instance-of v0, p2, Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    .line 2401771
    check-cast p2, Landroid/preference/PreferenceCategory;

    .line 2401772
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2401773
    invoke-virtual {p2, v0}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-direct {p0, p1, v1, p3, p4}, Lcom/facebook/katana/InternSettingsActivity;->a(Ljava/lang/String;Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/PreferenceScreen;)V

    .line 2401774
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/katana/InternSettingsActivity;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2401751
    sget-object v0, LX/D3Q;->a:[Ljava/lang/String;

    array-length v0, v0

    move v0, v0

    .line 2401752
    if-lt p1, v0, :cond_0

    .line 2401753
    invoke-virtual {p0}, Lcom/facebook/katana/InternSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Test Completed"

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2401754
    :goto_0
    return-void

    .line 2401755
    :cond_0
    sget-object v0, LX/D3Q;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    move-object v0, v0

    .line 2401756
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2401757
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->E:LX/D39;

    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2401758
    if-nez v0, :cond_1

    .line 2401759
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2401760
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2401761
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2401762
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2401763
    new-instance v1, Lcom/facebook/katana/InternSettingsActivity$33;

    invoke-direct {v1, p0, p1}, Lcom/facebook/katana/InternSettingsActivity$33;-><init>(Lcom/facebook/katana/InternSettingsActivity;I)V

    .line 2401764
    const/16 v2, 0x2710

    move v2, v2

    .line 2401765
    int-to-long v2, v2

    const v4, -0xf1d5702

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 2401766
    :catch_0
    invoke-virtual {p0}, Lcom/facebook/katana/InternSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Couldn\'t start activity"

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/katana/InternSettingsActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2401748
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0, p1}, LX/4oq;->setText(Ljava/lang/String;)V

    .line 2401749
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2401750
    return-void
.end method

.method private b()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2401744
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401745
    const-string v1, "Download Contacts Coefficients Now"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401746
    new-instance v1, LX/GtN;

    invoke-direct {v1, p0}, LX/GtN;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401747
    return-object v0
.end method

.method private b(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2401775
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401776
    const-string v1, "Refresh All Gks"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401777
    new-instance v1, LX/Gt5;

    invoke-direct {v1, p0}, LX/Gt5;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401778
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401779
    return-void
.end method

.method private b(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 2401737
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401738
    const-string v1, "Fresh Feed Overlay"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401739
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401740
    new-instance v1, LX/7mq;

    invoke-direct {v1, p0}, LX/7mq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401741
    new-instance v1, LX/7ms;

    invoke-direct {v1, p0}, LX/7ms;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401742
    new-instance v1, LX/7mr;

    invoke-direct {v1, p0}, LX/7mr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401743
    return-void
.end method

.method public static b$redex0(Lcom/facebook/katana/InternSettingsActivity;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2401728
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2401729
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 2401730
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2401731
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceScreen;)V

    .line 2401732
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/InternSettingsActivity;->onContentChanged()V

    .line 2401733
    return-void

    .line 2401734
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->an:LX/4oq;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401735
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 2401736
    iget-object v3, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v1, v0, v0, v3}, Lcom/facebook/katana/InternSettingsActivity;->a(Ljava/lang/String;Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/PreferenceScreen;)V

    goto :goto_0
.end method

.method private c(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2401722
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401723
    const-string v1, "Refresh trace upload rate limit"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401724
    const-string v1, "Sets the last upload timestamp to 0 so we can upload trace files for debugging"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401725
    new-instance v1, LX/Gt6;

    invoke-direct {v1, p0}, LX/Gt6;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2401726
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401727
    return-void
.end method

.method private c(Landroid/preference/PreferenceScreen;)V
    .locals 4

    .prologue
    .line 2401705
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2401706
    const-string v1, "Instant Articles"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401707
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2401708
    new-instance v1, LX/4om;

    invoke-direct {v1, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2401709
    const-string v2, "Launch article by ID"

    invoke-virtual {v1, v2}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401710
    const-string v2, "Article ID"

    invoke-virtual {v1, v2}, LX/4om;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2401711
    invoke-virtual {v1}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2401712
    invoke-virtual {v1}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 2401713
    new-instance v2, LX/GtI;

    invoke-direct {v2, p0}, LX/GtI;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v1, v2}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2401714
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401715
    new-instance v1, LX/4ok;

    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2401716
    sget-object v2, LX/Chk;->a:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2401717
    const-string v2, "View core logs"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2401718
    const-string v2, "Show core logs at the end of the instant article session"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2401719
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2401720
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2401721
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2400919
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2400920
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2400921
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->ap:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2400922
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2400923
    :cond_0
    return-void
.end method

.method private d(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400924
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400925
    const-string v1, "Resets PerfDebugTracer state"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400926
    const-string v1, "If the current request is stuck, resets the tracing state so that another trace can be requested"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400927
    new-instance v1, LX/Gt7;

    invoke-direct {v1, p0}, LX/Gt7;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400928
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400929
    return-void
.end method

.method private d(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 2400642
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400643
    const-string v1, "Aldrin"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400644
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400645
    new-instance v1, LX/GQM;

    invoke-direct {v1, p0}, LX/GQM;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400646
    return-void
.end method

.method private e(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400647
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400648
    const-string v1, "Force Configuration Fetch"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400649
    new-instance v1, LX/Gt8;

    invoke-direct {v1, p0}, LX/Gt8;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400650
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400651
    return-void
.end method

.method private e(Landroid/preference/PreferenceScreen;)V
    .locals 5

    .prologue
    .line 2400652
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400653
    const-string v1, "Gametime"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400654
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400655
    new-instance v1, LX/4oq;

    invoke-direct {v1, p0}, LX/4oq;-><init>(Landroid/content/Context;)V

    .line 2400656
    new-instance v2, LX/0Tn;

    sget-object v3, LX/0Tm;->c:LX/0Tn;

    const-string v4, "gametime_internal_preferences"

    invoke-direct {v2, v3, v4}, LX/0Tn;-><init>(LX/0To;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/4oq;->a(LX/0Tn;)V

    .line 2400657
    const-string v2, "Launch match by page ID"

    invoke-virtual {v1, v2}, LX/4oq;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400658
    const-string v2, "Page ID"

    invoke-virtual {v1, v2}, LX/4oq;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2400659
    invoke-virtual {v1}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 2400660
    invoke-virtual {v1}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 2400661
    invoke-virtual {v1}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    const-string v3, "This hint is purposefully long to make this line long enough to fit the whole screen"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2400662
    new-instance v2, LX/GtK;

    invoke-direct {v2, p0}, LX/GtK;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v1, v2}, LX/4oq;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2400663
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400664
    return-void
.end method

.method private f(Landroid/preference/PreferenceGroup;)V
    .locals 3

    .prologue
    .line 2400665
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400666
    const-string v1, "MobileConfig"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400667
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2400668
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400669
    return-void
.end method

.method private f(Landroid/preference/PreferenceScreen;)V
    .locals 5

    .prologue
    .line 2400670
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400671
    const-string v1, "Native Templates"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400672
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400673
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400674
    const-string v2, "Native Templates Shell"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400675
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    sget-object v3, LX/0ax;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "native_template_shell"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "id"

    const-string v4, "intern/directory/"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "search"

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "title"

    const-string v4, "NT Directory"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2400676
    new-instance v3, LX/GtL;

    invoke-direct {v3, p0, v2}, LX/GtL;-><init>(Lcom/facebook/katana/InternSettingsActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400677
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400678
    return-void
.end method

.method private g(Landroid/preference/PreferenceGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2400679
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400680
    const-string v1, "Crash the app"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400681
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400682
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400683
    const-string v1, "Soft Error"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400684
    const-string v1, "Report a soft error"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400685
    new-instance v1, LX/GtA;

    invoke-direct {v1, p0}, LX/GtA;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400686
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400687
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400688
    const-string v1, "Native Soft Error"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400689
    const-string v1, "Report a native soft error"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400690
    new-instance v1, LX/GtB;

    invoke-direct {v1, p0}, LX/GtB;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400691
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400692
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400693
    const-string v1, "Native crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400694
    const-string v1, "Cause a native crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400695
    new-instance v1, LX/GtX;

    invoke-direct {v1}, LX/GtX;-><init>()V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400696
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400697
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400698
    const-string v1, "ANR and Native crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400699
    const-string v1, "ANR followed by a native crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400700
    new-instance v1, LX/GtY;

    invoke-direct {v1}, LX/GtY;-><init>()V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400701
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400702
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400703
    const-string v1, "Java Crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400704
    const-string v1, "Cause a Java crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400705
    new-instance v1, LX/Gap;

    invoke-direct {v1}, LX/Gap;-><init>()V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400706
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400707
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400708
    const-string v1, "Out Of Memory Crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400709
    const-string v1, "Cause an out of memory crash"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400710
    new-instance v1, LX/Gaq;

    invoke-direct {v1}, LX/Gaq;-><init>()V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400711
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400712
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400713
    const-string v1, "App Not Responding Error"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400714
    const-string v1, "Simulate a stalled main thread"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400715
    new-instance v1, LX/Gar;

    invoke-direct {v1}, LX/Gar;-><init>()V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400716
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400717
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400718
    const-string v1, "App Not Responding (recover)"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400719
    const-string v1, "Simulate a stalled main thread and come back after 10 seconds"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400720
    new-instance v1, LX/Gas;

    invoke-direct {v1}, LX/Gas;-><init>()V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400721
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400722
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400723
    sget-object v1, LX/0eJ;->n:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2400724
    const-string v1, "Enable core dumping"

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400725
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400726
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/4ok;->setEnabled(Z)V

    .line 2400727
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400728
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400729
    const-string v1, "Upload libs to GLC"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400730
    const-string v1, "Upload all system libraries to the GLC"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400731
    new-instance v1, LX/GtC;

    invoke-direct {v1, p0}, LX/GtC;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400732
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400733
    return-void
.end method

.method private g(Landroid/preference/PreferenceScreen;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2400734
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400735
    const-string v1, "NOTIFICATIONS SETTINGS"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400736
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400737
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400738
    const-string v1, "Push Notification"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400739
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400740
    invoke-direct {p0}, Lcom/facebook/katana/InternSettingsActivity;->a()Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400741
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->l:LX/3Q7;

    invoke-virtual {v0, v4}, LX/3Q7;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2400742
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400743
    const-string v1, "Popup Notifications"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400744
    new-instance v1, LX/4or;

    invoke-direct {v1, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2400745
    new-array v2, v5, [Ljava/lang/CharSequence;

    const-string v3, "Reset Nux"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2400746
    new-array v2, v5, [Ljava/lang/CharSequence;

    const-string v3, "reset_nux"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2400747
    const-string v2, "Popup Notifications"

    invoke-virtual {v1, v2}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2400748
    const-string v2, "Reset Popup Notifications Nux"

    invoke-virtual {v1, v2}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400749
    const-string v2, "Reset"

    invoke-virtual {v1, v2}, LX/4or;->setPositiveButtonText(Ljava/lang/CharSequence;)V

    .line 2400750
    const-string v2, "Cancel"

    invoke-virtual {v1, v2}, LX/4or;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2400751
    const-string v2, "popup_nux"

    invoke-virtual {v1, v2}, LX/4or;->setKey(Ljava/lang/String;)V

    .line 2400752
    new-instance v2, LX/GtM;

    invoke-direct {v2, p0}, LX/GtM;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v1, v2}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2400753
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400754
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400755
    :cond_0
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400756
    const-string v1, "Inline Notification NUX Preference"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400757
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400758
    new-instance v1, LX/DqY;

    invoke-direct {v1, p0}, LX/DqY;-><init>(Landroid/content/Context;)V

    .line 2400759
    const-string v2, "Reset Inline Notification NUX status"

    invoke-virtual {v1, v2}, LX/DqY;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400760
    const-string v2, "Clears client preferences and fetches status from server"

    invoke-virtual {v1, v2}, LX/DqY;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400761
    const-class v2, LX/DqY;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/DqY;->setKey(Ljava/lang/String;)V

    .line 2400762
    new-instance v2, Lcom/facebook/notifications/preferences/InlineNotificationNuxResetServerPreference$1;

    invoke-direct {v2, v1}, Lcom/facebook/notifications/preferences/InlineNotificationNuxResetServerPreference$1;-><init>(LX/DqY;)V

    invoke-virtual {v1, v2}, LX/DqY;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400763
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400764
    new-instance v1, LX/Dqa;

    invoke-direct {v1, p0}, LX/Dqa;-><init>(Landroid/content/Context;)V

    .line 2400765
    const-string v2, "Open Inline Notification NUX server config"

    invoke-virtual {v1, v2}, LX/Dqa;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400766
    const-string v2, "Click \'Reset View State For User\' to enable NUX eligibility"

    invoke-virtual {v1, v2}, LX/Dqa;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400767
    const-class v2, LX/Dqa;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Dqa;->setKey(Ljava/lang/String;)V

    .line 2400768
    new-instance v2, LX/DqZ;

    invoke-direct {v2, v1}, LX/DqZ;-><init>(LX/Dqa;)V

    invoke-virtual {v1, v2}, LX/Dqa;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400769
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400770
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400771
    const-string v1, "Native Notification Settings Preference"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400772
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400773
    new-instance v1, LX/Dqc;

    invoke-direct {v1, p0}, LX/Dqc;-><init>(Landroid/content/Context;)V

    .line 2400774
    const-string v2, "Reset Local Push Setting synced status"

    invoke-virtual {v1, v2}, LX/Dqc;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400775
    const-string v2, "Clears local push settings synced client preferences"

    invoke-virtual {v1, v2}, LX/Dqc;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400776
    const-class v2, LX/Dqc;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Dqc;->setKey(Ljava/lang/String;)V

    .line 2400777
    new-instance v2, LX/Dqb;

    invoke-direct {v2, v1}, LX/Dqb;-><init>(LX/Dqc;)V

    invoke-virtual {v1, v2}, LX/Dqc;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400778
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400779
    new-instance v1, LX/Dqi;

    invoke-direct {v1, p0}, LX/Dqi;-><init>(Landroid/content/Context;)V

    .line 2400780
    const-string v2, "Open Notification Native Settings"

    invoke-virtual {v1, v2}, LX/Dqi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400781
    const-string v2, "Opens Notification Settings Fragment"

    invoke-virtual {v1, v2}, LX/Dqi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400782
    const-class v2, LX/Dqi;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Dqi;->setKey(Ljava/lang/String;)V

    .line 2400783
    new-instance v2, LX/Dqh;

    invoke-direct {v2, v1}, LX/Dqh;-><init>(LX/Dqi;)V

    invoke-virtual {v1, v2}, LX/Dqi;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400784
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400785
    new-instance v1, LX/Dqg;

    invoke-direct {v1, p0}, LX/Dqg;-><init>(Landroid/content/Context;)V

    .line 2400786
    const-string v2, "Open Local Push Settings"

    invoke-virtual {v1, v2}, LX/Dqg;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400787
    const-string v2, "Opens Notification Local Push Settings"

    invoke-virtual {v1, v2}, LX/Dqg;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400788
    const-class v2, LX/Dqg;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Dqg;->setKey(Ljava/lang/String;)V

    .line 2400789
    new-instance v2, LX/Dqf;

    invoke-direct {v2, v1}, LX/Dqf;-><init>(LX/Dqg;)V

    invoke-virtual {v1, v2}, LX/Dqg;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400790
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400791
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400792
    const-string v1, "Notifications DB Preferences"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400793
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400794
    new-instance v1, LX/Dqe;

    invoke-direct {v1, p0}, LX/Dqe;-><init>(Landroid/content/Context;)V

    .line 2400795
    const-string v2, "Clear the Local Notifications Database"

    invoke-virtual {v1, v2}, LX/Dqe;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400796
    const-class v2, LX/Dqe;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Dqe;->setKey(Ljava/lang/String;)V

    .line 2400797
    new-instance v2, LX/Dqd;

    invoke-direct {v2, v1}, LX/Dqd;-><init>(LX/Dqe;)V

    invoke-virtual {v1, v2}, LX/Dqe;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400798
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400799
    return-void
.end method

.method private h(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400800
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400801
    const-string v1, "Run Browser Automation Test"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400802
    new-instance v1, LX/GtD;

    invoke-direct {v1, p0}, LX/GtD;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400803
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400804
    return-void
.end method

.method private h(Landroid/preference/PreferenceScreen;)V
    .locals 4

    .prologue
    .line 2400805
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400806
    const-string v1, "Inline Composer Prompts"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400807
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400808
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400809
    const-string v2, "Discarded Prompt"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400810
    const-string v2, "Show reasons why prompts are not seen"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400811
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2400812
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400813
    return-void
.end method

.method private i(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400814
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400815
    const-string v1, "Fundraiser Creation Flow"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400816
    new-instance v1, LX/GtE;

    invoke-direct {v1, p0}, LX/GtE;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400817
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400818
    return-void
.end method

.method private i(Landroid/preference/PreferenceScreen;)V
    .locals 4

    .prologue
    .line 2400819
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400820
    const-string v1, "React OTA"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400821
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400822
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400823
    const-string v2, "React OTA Update Info"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400824
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/fbreact/autoupdater/fbprefs/ReactOTATestInternalSettingsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2400825
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400826
    return-void
.end method

.method private j(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400827
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400828
    const-string v1, "Fundraiser Page"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400829
    new-instance v1, LX/GtF;

    invoke-direct {v1, p0}, LX/GtF;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400830
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400831
    return-void
.end method

.method private j(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 2400832
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400833
    const-string v1, "Facecast"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400834
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400835
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400836
    sget-object v2, LX/1CA;->z:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2400837
    const-string v2, "Display Live Video Overlay Stats"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400838
    const-string v2, "Display metadata of the live videos"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400839
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400840
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400841
    return-void
.end method

.method private k(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400842
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400843
    const-string v1, "Fundraiser Native (P4P) Donation Flow"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400844
    new-instance v1, LX/GtG;

    invoke-direct {v1, p0}, LX/GtG;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400845
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400846
    return-void
.end method

.method private k(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 2400847
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400848
    const-string v1, "360 Photo"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400849
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400850
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400851
    sget-object v2, LX/1CA;->h:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2400852
    const-string v2, "Display Inline Photo Specs"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400853
    const-string v2, "Display metadata of the 360 photo"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400854
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400855
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400856
    return-void
.end method

.method private l(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400857
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400858
    const-string v1, "Open website with fb://webview"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400859
    new-instance v1, LX/GtH;

    invoke-direct {v1, p0}, LX/GtH;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400860
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400861
    return-void
.end method

.method private l(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 2400862
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400863
    const-string v1, "Fresco"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400864
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400865
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400866
    sget-object v2, LX/0eJ;->p:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2400867
    const-string v2, "Enable Debug Overlay"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400868
    const-string v2, "Overlay image information on top of images"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400869
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400870
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400871
    return-void
.end method

.method private m(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2400872
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400873
    const v1, 0x7f0309f6

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2400874
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400875
    return-void
.end method

.method private m(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 2400876
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400877
    const-string v1, "CameraCore"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400878
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2400879
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400880
    sget-object v2, LX/0eJ;->q:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2400881
    const-string v2, "Enable Fps Overlay"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400882
    const-string v2, "Overlay an FPS counter in the bottom left corner"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400883
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400884
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400885
    return-void
.end method

.method private n(Landroid/preference/PreferenceGroup;)V
    .locals 3

    .prologue
    .line 2400886
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2400887
    :goto_0
    return-void

    .line 2400888
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/katana/InternSettingsActivity;->m(Landroid/preference/PreferenceGroup;)V

    .line 2400889
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400890
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2400891
    const-string v1, "Facebook at Work"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400892
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400893
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400894
    const-string v2, "Open blocking SSO dialog now"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400895
    new-instance v2, LX/GtO;

    invoke-direct {v2, p0}, LX/GtO;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400896
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400897
    const-string v2, "Launch Sign up Flow"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400898
    const-string v2, "Launch the sign up NUX tht is shown after registration"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400899
    new-instance v2, LX/GtP;

    invoke-direct {v2, p0}, LX/GtP;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400900
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 2400901
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400902
    const-string v2, "Launch Sign up Flow with all steps"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400903
    const-string v2, "Launch the sign up flow with all possible steps"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400904
    new-instance v2, LX/GtQ;

    invoke-direct {v2, p0}, LX/GtQ;-><init>(Lcom/facebook/katana/InternSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400905
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2400906
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2400907
    invoke-static {p0, p0}, Lcom/facebook/katana/InternSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2400908
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->Q:LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2400909
    invoke-virtual {p0}, Lcom/facebook/katana/InternSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 2400910
    invoke-virtual {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2400911
    iput-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    .line 2400912
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->ao:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v0}, Lcom/facebook/katana/InternSettingsActivity;->a(Landroid/preference/PreferenceScreen;)V

    .line 2400913
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity;->J:LX/0Zb;

    const-string v1, "internal_settings_opened"

    invoke-interface {v0, v1}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 2400914
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5d1c403

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2400915
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStart()V

    .line 2400916
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->Q:LX/CIm;

    invoke-virtual {v1, p0}, LX/CIm;->b(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2400917
    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity;->Q:LX/CIm;

    const v2, 0x7f083441

    invoke-virtual {v1, v2}, LX/CIm;->a(I)V

    .line 2400918
    const/16 v1, 0x23

    const v2, -0x43f52abe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
