.class public Lcom/facebook/katana/NotificationSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/10X;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:Z

.field private c:LX/3Q7;

.field private d:LX/3Py;

.field private e:LX/0s5;

.field public f:LX/CIm;

.field private g:LX/3QE;

.field private h:LX/Dqy;

.field private i:LX/33a;

.field private j:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2401901
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 2401897
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2401898
    const v1, 0x7f0309f6

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 2401899
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401900
    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/NotificationSettingsActivity;Z)V
    .locals 3

    .prologue
    .line 2401889
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 2401890
    sget-object v1, LX/0hM;->k:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 2401891
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2401892
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->k:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CIm;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2401893
    iget-object v1, p0, Lcom/facebook/katana/NotificationSettingsActivity;->d:LX/3Py;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/3Py;->a(Z)V

    .line 2401894
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->j:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 2401895
    return-void

    .line 2401896
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/preference/PreferenceGroup;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2401869
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->c:LX/3Q7;

    invoke-virtual {v0, v6}, LX/3Q7;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2401870
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->l:LX/0Tn;

    const v2, 0x7f08115c

    invoke-virtual {v0, p0, v1, v2, v5}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401871
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v2, LX/0hM;->m:LX/0Tn;

    const v3, 0x7f08343b

    const v4, 0x7f08342f

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IIZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401872
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v2, LX/0hM;->n:LX/0Tn;

    const v3, 0x7f08343a

    const v4, 0x7f083439

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IIZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401873
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v2, LX/0hM;->o:LX/0Tn;

    const v3, 0x7f083437

    const v4, 0x7f083438

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IIZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401874
    new-instance v0, LX/4ou;

    invoke-direct {v0, p0}, LX/4ou;-><init>(Landroid/content/Context;)V

    .line 2401875
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/4ou;->setRingtoneType(I)V

    .line 2401876
    sget-object v1, LX/0hM;->p:LX/0Tn;

    .line 2401877
    iget-object v2, v0, LX/4ou;->a:LX/2qy;

    invoke-virtual {v2, v1}, LX/2qy;->a(LX/0Tn;)V

    .line 2401878
    invoke-virtual {v0, v6}, LX/4ou;->setShowSilent(Z)V

    .line 2401879
    invoke-virtual {v0, v5}, LX/4ou;->setShowDefault(Z)V

    .line 2401880
    const v1, 0x7f083436

    invoke-virtual {v0, v1}, LX/4ou;->setTitle(I)V

    .line 2401881
    const v1, 0x7f083435

    invoke-virtual {v0, v1}, LX/4ou;->setSummary(I)V

    .line 2401882
    iget-object v1, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    invoke-virtual {v1, v0}, LX/CIm;->a(Landroid/preference/Preference;)V

    .line 2401883
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401884
    invoke-direct {p0, p1}, Lcom/facebook/katana/NotificationSettingsActivity;->a(Landroid/preference/PreferenceGroup;)V

    .line 2401885
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->g:LX/3QE;

    invoke-virtual {v0}, LX/3QE;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2401886
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->h:LX/Dqy;

    new-instance v1, Lcom/facebook/katana/NotificationSettingsActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/katana/NotificationSettingsActivity$2;-><init>(Lcom/facebook/katana/NotificationSettingsActivity;Landroid/preference/PreferenceGroup;)V

    invoke-virtual {v0, p1, v1}, LX/Dqy;->a(Landroid/preference/PreferenceGroup;Ljava/lang/Runnable;)V

    .line 2401887
    :goto_0
    return-void

    .line 2401888
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/katana/NotificationSettingsActivity;->c(Landroid/preference/PreferenceGroup;)V

    goto :goto_0
.end method

.method private c(Landroid/preference/PreferenceGroup;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2401792
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->q:LX/0Tn;

    const v2, 0x7f083434

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401793
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->r:LX/0Tn;

    const v2, 0x7f083440

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401794
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->s:LX/0Tn;

    const v2, 0x7f083433

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401795
    iget-boolean v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->b:Z

    if-nez v0, :cond_0

    .line 2401796
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->t:LX/0Tn;

    const v2, 0x7f08343f

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401797
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->u:LX/0Tn;

    const v2, 0x7f08343e

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401798
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->v:LX/0Tn;

    const v2, 0x7f08343d

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401799
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->w:LX/0Tn;

    const v2, 0x7f083432

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401800
    iget-boolean v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->b:Z

    if-nez v0, :cond_1

    .line 2401801
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->x:LX/0Tn;

    const v2, 0x7f083431    # 1.81046E38f

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401802
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->y:LX/0Tn;

    const v2, 0x7f083430

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401803
    iget-boolean v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->b:Z

    if-nez v0, :cond_2

    .line 2401804
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    sget-object v1, LX/0hM;->z:LX/0Tn;

    const v2, 0x7f08343c

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2401805
    :cond_2
    return-void
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 2401868
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->k:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2401866
    const-string v0, "app_settings"

    move-object v0, v0

    .line 2401867
    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2401863
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->a(Landroid/os/Bundle;)V

    .line 2401864
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/NotificationSettingsActivity;->requestWindowFeature(I)Z

    .line 2401865
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2401862
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2401840
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2401841
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2401842
    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2401843
    invoke-static {v1}, LX/3Q7;->a(LX/0QB;)LX/3Q7;

    move-result-object v0

    check-cast v0, LX/3Q7;

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->c:LX/3Q7;

    .line 2401844
    invoke-static {v1}, LX/3Py;->b(LX/0QB;)LX/3Py;

    move-result-object v0

    check-cast v0, LX/3Py;

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->d:LX/3Py;

    .line 2401845
    invoke-static {v1}, LX/0s5;->a(LX/0QB;)LX/0s5;

    move-result-object v0

    check-cast v0, LX/0s5;

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->e:LX/0s5;

    .line 2401846
    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->b:Z

    .line 2401847
    invoke-static {v1}, LX/CIm;->b(LX/0QB;)LX/CIm;

    move-result-object v0

    check-cast v0, LX/CIm;

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    .line 2401848
    invoke-static {v1}, LX/3QE;->a(LX/0QB;)LX/3QE;

    move-result-object v0

    check-cast v0, LX/3QE;

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->g:LX/3QE;

    .line 2401849
    const-class v0, LX/Dqz;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Dqz;

    .line 2401850
    new-instance p1, LX/Dqy;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/33a;->b(LX/0QB;)LX/33a;

    move-result-object v4

    check-cast v4, LX/33a;

    invoke-direct {p1, p0, v2, v3, v4}, LX/Dqy;-><init>(Lcom/facebook/base/activity/FbPreferenceActivity;LX/1Ck;LX/0tX;LX/33a;)V

    .line 2401851
    move-object v0, p1

    .line 2401852
    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->h:LX/Dqy;

    .line 2401853
    invoke-static {v1}, LX/33a;->b(LX/0QB;)LX/33a;

    move-result-object v0

    check-cast v0, LX/33a;

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->i:LX/33a;

    .line 2401854
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2401855
    invoke-virtual {p0}, Lcom/facebook/katana/NotificationSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->j:Landroid/preference/PreferenceScreen;

    .line 2401856
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->j:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v0}, Lcom/facebook/katana/NotificationSettingsActivity;->b(Landroid/preference/PreferenceGroup;)V

    .line 2401857
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    iget-object v1, p0, Lcom/facebook/katana/NotificationSettingsActivity;->j:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, LX/CIm;->a(Landroid/preference/PreferenceGroup;)V

    .line 2401858
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->j:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/katana/NotificationSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2401859
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->i:LX/33a;

    .line 2401860
    iget-object v1, v0, LX/33a;->a:LX/0Zb;

    const-string v2, "push_settings_opened"

    invoke-static {v2}, LX/33a;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2401861
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 2401836
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->finish()V

    .line 2401837
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    if-eqz v0, :cond_0

    .line 2401838
    invoke-static {p0}, LX/CIm;->b(Landroid/app/Activity;)V

    .line 2401839
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x604919b0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2401831
    invoke-virtual {p0}, Lcom/facebook/katana/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 2401832
    if-eqz v1, :cond_0

    .line 2401833
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 2401834
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onDestroy()V

    .line 2401835
    const/16 v1, 0x23

    const v2, -0x4aa5f062

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x22

    const v4, 0x2a3ab0e5

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2401810
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStart()V

    .line 2401811
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->b(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 2401812
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    const v4, 0x7f083443

    invoke-virtual {v0, v4}, LX/CIm;->a(I)V

    .line 2401813
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    .line 2401814
    iget-object v4, v0, LX/CIm;->e:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v4, v2}, Lcom/facebook/widget/SwitchCompat;->setVisibility(I)V

    .line 2401815
    iget-object v5, v0, LX/CIm;->e:Lcom/facebook/widget/SwitchCompat;

    if-nez v2, :cond_4

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v5, v4}, Lcom/facebook/widget/SwitchCompat;->setClickable(Z)V

    .line 2401816
    invoke-direct {p0}, Lcom/facebook/katana/NotificationSettingsActivity;->d()Z

    move-result v4

    .line 2401817
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->e:LX/0s5;

    invoke-virtual {v0}, LX/0s5;->c()Z

    move-result v5

    .line 2401818
    iget-object v6, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    if-nez v5, :cond_1

    move v0, v1

    :goto_1
    new-instance v7, LX/GtZ;

    invoke-direct {v7, p0}, LX/GtZ;-><init>(Lcom/facebook/katana/NotificationSettingsActivity;)V

    .line 2401819
    iget-object v8, v6, LX/CIm;->e:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v8, v4}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2401820
    iget-object v8, v6, LX/CIm;->e:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v8, v0}, Lcom/facebook/widget/SwitchCompat;->setEnabled(Z)V

    .line 2401821
    if-eqz v7, :cond_0

    .line 2401822
    iget-object v8, v6, LX/CIm;->e:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v8, v7}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2401823
    :cond_0
    if-nez v5, :cond_2

    if-eqz v4, :cond_2

    .line 2401824
    :goto_2
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->j:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 2401825
    iget-object v2, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    if-eqz v1, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_3
    invoke-virtual {v2, v0}, LX/CIm;->a(F)V

    .line 2401826
    const v0, 0x75648c0f

    invoke-static {v0, v3}, LX/02F;->c(II)V

    return-void

    :cond_1
    move v0, v2

    .line 2401827
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2401828
    goto :goto_2

    .line 2401829
    :cond_3
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_3

    .line 2401830
    :cond_4
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2401806
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    .line 2401807
    iget-object v0, p0, Lcom/facebook/katana/NotificationSettingsActivity;->f:LX/CIm;

    if-eqz v0, :cond_0

    .line 2401808
    invoke-static {p0}, LX/CIm;->a(Landroid/app/Activity;)V

    .line 2401809
    :cond_0
    return-void
.end method
