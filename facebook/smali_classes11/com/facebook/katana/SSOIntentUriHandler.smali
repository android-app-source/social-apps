.class public Lcom/facebook/katana/SSOIntentUriHandler;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# annotations
.annotation runtime Lcom/facebook/base/activity/DeliverOnNewIntentWhenFinishing;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2401982
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2401983
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2401984
    :cond_0
    :goto_0
    return-void

    .line 2401985
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2401986
    invoke-virtual {p0, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2401987
    invoke-static {p0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 2401988
    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/katana/service/AppSession;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2401989
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v0, v1

    .line 2401990
    sget-object v1, LX/2A1;->STATUS_LOGGED_IN:LX/2A1;

    if-ne v0, v1, :cond_0

    .line 2401991
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2401992
    invoke-virtual {p0}, Lcom/facebook/katana/SSOIntentUriHandler;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2401993
    sget-object v0, LX/0ax;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/katana/SSOIntentUriHandler;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2401994
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(LX/0QB;)Lcom/facebook/katana/urimap/IntentHandlerUtil;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-virtual {v0, p0, v1}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2401995
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/SSOIntentUriHandler;->finish()V

    .line 2401996
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xc7c2f45

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2401997
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2401998
    invoke-virtual {p0}, Lcom/facebook/katana/SSOIntentUriHandler;->finish()V

    .line 2401999
    const/16 v1, 0x23

    const v2, 0x227ec317

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
