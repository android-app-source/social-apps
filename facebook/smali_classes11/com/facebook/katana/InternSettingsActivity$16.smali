.class public final Lcom/facebook/katana/InternSettingsActivity$16;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4oi;

.field public final synthetic b:LX/4oi;

.field public final synthetic c:LX/4oq;

.field public final synthetic d:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;LX/4oi;LX/4oi;LX/4oq;)V
    .locals 0

    .prologue
    .line 2400345
    iput-object p1, p0, Lcom/facebook/katana/InternSettingsActivity$16;->d:Lcom/facebook/katana/InternSettingsActivity;

    iput-object p2, p0, Lcom/facebook/katana/InternSettingsActivity$16;->a:LX/4oi;

    iput-object p3, p0, Lcom/facebook/katana/InternSettingsActivity$16;->b:LX/4oi;

    iput-object p4, p0, Lcom/facebook/katana/InternSettingsActivity$16;->c:LX/4oq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2400346
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->d:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->c:LX/0l2;

    const/4 v1, 0x1

    .line 2400347
    iput-boolean v1, v0, LX/0l2;->b:Z

    .line 2400348
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->a:LX/4oi;

    if-eqz v0, :cond_0

    .line 2400349
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->a:LX/4oi;

    invoke-virtual {v0, v2}, LX/4oi;->setEnabled(Z)V

    .line 2400350
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->a:LX/4oi;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400351
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->a:LX/4oi;

    const-string v1, "Monkey mode irrevocably on. Reinstall the app or clear application data to turn it off."

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400352
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->b:LX/4oi;

    if-eqz v0, :cond_1

    .line 2400353
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->b:LX/4oi;

    invoke-virtual {v0, v2}, LX/4oi;->setEnabled(Z)V

    .line 2400354
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->c:LX/4oq;

    if-eqz v0, :cond_2

    .line 2400355
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$16;->c:LX/4oq;

    invoke-virtual {v0, v2}, LX/4oq;->setEnabled(Z)V

    .line 2400356
    :cond_2
    return-void
.end method
