.class public Lcom/facebook/katana/GoogleNowIntentHandler;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/Eln;


# annotations
.annotation runtime Lcom/facebook/base/activity/DeliverOnNewIntentWhenFinishing;
.end annotation


# instance fields
.field public p:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2400237
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/GoogleNowIntentHandler;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/GoogleNowIntentHandler;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2400236
    iput-object p1, p0, Lcom/facebook/katana/GoogleNowIntentHandler;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/katana/GoogleNowIntentHandler;->q:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/katana/GoogleNowIntentHandler;

    const/16 v1, 0xc

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v1, v0}, Lcom/facebook/katana/GoogleNowIntentHandler;->a(Lcom/facebook/katana/GoogleNowIntentHandler;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2400232
    if-nez p1, :cond_1

    .line 2400233
    :cond_0
    :goto_0
    return-void

    .line 2400234
    :cond_1
    invoke-static {p1}, Lcom/facebook/katana/GoogleNowIntentHandler;->d(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2400235
    invoke-direct {p0, p1}, Lcom/facebook/katana/GoogleNowIntentHandler;->e(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static d(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 2400217
    const-string v0, "com.google.android.gms.actions.SEARCH_ACTION"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private e(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2400227
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2400228
    if-nez v1, :cond_0

    .line 2400229
    :goto_0
    return-void

    .line 2400230
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/facebook/katana/GoogleNowIntentHandler;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "target_fragment"

    sget-object v3, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "query_function"

    invoke-static {v1}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "query_vertical"

    const-string v3, "content"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "source"

    sget-object v3, LX/8ci;->J:LX/8ci;

    invoke-virtual {v3}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "query_title"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2400231
    iget-object v1, p0, Lcom/facebook/katana/GoogleNowIntentHandler;->q:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2400223
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2400224
    invoke-virtual {p0}, Lcom/facebook/katana/GoogleNowIntentHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/katana/GoogleNowIntentHandler;->c(Landroid/content/Intent;)V

    .line 2400225
    invoke-virtual {p0}, Lcom/facebook/katana/GoogleNowIntentHandler;->finish()V

    .line 2400226
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2400218
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2400219
    invoke-static {p0, p0}, Lcom/facebook/katana/GoogleNowIntentHandler;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2400220
    invoke-virtual {p0}, Lcom/facebook/katana/GoogleNowIntentHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/katana/GoogleNowIntentHandler;->c(Landroid/content/Intent;)V

    .line 2400221
    invoke-virtual {p0}, Lcom/facebook/katana/GoogleNowIntentHandler;->finish()V

    .line 2400222
    return-void
.end method
