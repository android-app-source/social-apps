.class public Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fv;


# instance fields
.field public a:LX/17T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Kl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Fk3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/FkC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Fk4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Fk5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DbA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/3Lb;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/B9u;

.field private q:Landroid/widget/ScrollView;

.field public r:Landroid/widget/TextView;

.field public s:Landroid/widget/TextView;

.field public t:Landroid/widget/ImageView;

.field public u:Lcom/facebook/fbui/facepile/FacepileView;

.field public v:Landroid/widget/TextView;

.field public w:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2209327
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2209328
    sget-object v0, LX/B9u;->THREAD_LIST:LX/B9u;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->p:LX/B9u;

    .line 2209329
    return-void
.end method

.method public static a(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 2209321
    const-string v0, "diode_qp_module"

    .line 2209322
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2209323
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->p:LX/B9u;

    if-eqz v0, :cond_0

    .line 2209324
    const-string v0, "dest"

    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->p:LX/B9u;

    invoke-virtual {v1}, LX/B9u;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2209325
    :cond_0
    const-string v0, "user_stage"

    sget-object v1, LX/23m;->INSTALL_NOW:LX/23m;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2209326
    return-void
.end method

.method public static d(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;)V
    .locals 2

    .prologue
    .line 2209315
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "view"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2209316
    sget-object v1, LX/CIp;->y:Ljava/lang/String;

    .line 2209317
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 2209318
    invoke-static {p0, v0}, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->a(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2209319
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2209320
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209303
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2209304
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;

    invoke-static {v0}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v3

    check-cast v3, LX/17T;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/3Kl;->a(LX/0QB;)LX/3Kl;

    move-result-object v5

    check-cast v5, LX/3Kl;

    const/16 v6, 0x27c5

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x12cb

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/Fk3;->b(LX/0QB;)LX/Fk3;

    move-result-object v8

    check-cast v8, LX/Fk3;

    invoke-static {v0}, LX/FkC;->a(LX/0QB;)LX/FkC;

    move-result-object v9

    check-cast v9, LX/FkC;

    invoke-static {v0}, LX/Fk4;->b(LX/0QB;)LX/Fk4;

    move-result-object v10

    check-cast v10, LX/Fk4;

    invoke-static {v0}, LX/Fk5;->b(LX/0QB;)LX/Fk5;

    move-result-object v11

    check-cast v11, LX/Fk5;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static {v0}, LX/DbA;->a(LX/0QB;)LX/DbA;

    move-result-object p1

    check-cast p1, LX/DbA;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->a:LX/17T;

    iput-object v4, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->b:LX/0Zb;

    iput-object v5, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->c:LX/3Kl;

    iput-object v6, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->e:LX/0Or;

    iput-object v8, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->f:LX/Fk3;

    iput-object v9, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->g:LX/FkC;

    iput-object v10, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->h:LX/Fk4;

    iput-object v11, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->i:LX/Fk5;

    iput-object v12, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v13, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->k:Ljava/lang/Boolean;

    iput-object p1, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->l:LX/DbA;

    iput-object v0, v2, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->m:LX/0ad;

    .line 2209305
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->c:LX/3Kl;

    .line 2209306
    sget-object v1, LX/3LZ;->TOP_FRIENDS_ON_MESSENGER:LX/3LZ;

    sget-object v2, LX/3LZ;->FRIENDS_ON_MESSENGER:LX/3LZ;

    invoke-static {v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    .line 2209307
    iget-object v1, v0, LX/3Kl;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Lb;

    .line 2209308
    new-instance v3, LX/3La;

    invoke-direct {v3, v2}, LX/3La;-><init>(Ljava/util/EnumSet;)V

    .line 2209309
    iput-object v3, v1, LX/3Lb;->z:LX/3La;

    .line 2209310
    move-object v0, v1

    .line 2209311
    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->n:LX/3Lb;

    .line 2209312
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->n:LX/3Lb;

    new-instance v1, LX/FBM;

    invoke-direct {v1, p0}, LX/FBM;-><init>(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;)V

    .line 2209313
    iput-object v1, v0, LX/3Lb;->B:LX/3Mb;

    .line 2209314
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2209301
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->q:Landroid/widget/ScrollView;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->fullScroll(I)Z

    .line 2209302
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2209260
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->q:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2209300
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x500468c6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2209282
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2209283
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2209284
    if-eqz v0, :cond_1

    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2209285
    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/B9u;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->p:LX/B9u;

    .line 2209286
    :cond_0
    :goto_0
    const/4 p1, 0x1

    const/4 v5, 0x0

    .line 2209287
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2209288
    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->r:Landroid/widget/TextView;

    const v3, 0x7f08321a

    new-array v4, p1, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2209289
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->s:Landroid/widget/TextView;

    const v2, 0x7f08321d

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2209290
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->t:Landroid/widget/ImageView;

    const v2, 0x7f0203d1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2209291
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->w:Landroid/widget/Button;

    const v2, 0x7f083214

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 2209292
    goto :goto_1

    .line 2209293
    :goto_1
    goto :goto_2

    .line 2209294
    :goto_2
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->w:Landroid/widget/Button;

    new-instance v2, LX/FBN;

    invoke-direct {v2, p0}, LX/FBN;-><init>(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2209295
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->n:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->a()V

    .line 2209296
    const v0, 0x7145f988

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2209297
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2209298
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/B9u;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->p:LX/B9u;

    goto :goto_0
    .line 2209299
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x591b81d9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209280
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2209281
    const v1, 0x7f03041d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x578914a6

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x31a5748f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209277
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2209278
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->g:LX/FkC;

    invoke-virtual {v1}, LX/FkC;->a()V

    .line 2209279
    const/16 v1, 0x2b

    const v2, -0x226766c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2bd32c8d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209270
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2209271
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2209272
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2209273
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    if-lez v2, :cond_1

    .line 2209274
    invoke-static {p0}, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->d(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;)V

    .line 2209275
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x385d4ff2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2209276
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v4, LX/FBO;

    invoke-direct {v4, p0, v1}, LX/FBO;-><init>(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209261
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2209262
    const v0, 0x7f0d0c9b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->q:Landroid/widget/ScrollView;

    .line 2209263
    const v0, 0x7f0d0c9c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->r:Landroid/widget/TextView;

    .line 2209264
    const v0, 0x7f0d0c9d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->s:Landroid/widget/TextView;

    .line 2209265
    const v0, 0x7f0d0c9e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->t:Landroid/widget/ImageView;

    .line 2209266
    const v0, 0x7f0d0c9f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->u:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2209267
    const v0, 0x7f0d0ca0

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->v:Landroid/widget/TextView;

    .line 2209268
    const v0, 0x7f0d0ca1

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->w:Landroid/widget/Button;

    .line 2209269
    return-void
.end method
