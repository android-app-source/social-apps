.class public Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/resources/ui/FbButton;

.field private d:Lcom/facebook/user/tiles/UserTileView;

.field public e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2209003
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209000
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2209001
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;

    const/16 v0, 0x12cd

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->a:LX/0Or;

    iput-object p1, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->b:LX/0Zb;

    .line 2209002
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5c563276

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2209004
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2209005
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->c:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/FBG;

    invoke-direct {v2, p0}, LX/FBG;-><init>(Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2209006
    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->d:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/8ue;->MESSENGER:LX/8ue;

    invoke-static {v0, v3}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2209007
    const/16 v0, 0x2b

    const v2, -0x44976294

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x52988c31

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2208992
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2208993
    const v0, 0x7f030421

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2208994
    const v0, 0x7f0d0ca6

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->d:Lcom/facebook/user/tiles/UserTileView;

    .line 2208995
    const v0, 0x7f0d0cad

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->e:Landroid/widget/TextView;

    .line 2208996
    const v0, 0x7f0d0cae

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->c:Lcom/facebook/resources/ui/FbButton;

    .line 2208997
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->c:Lcom/facebook/resources/ui/FbButton;

    const v3, 0x7f083230

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2208998
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->e:Landroid/widget/TextView;

    const v3, 0x7f08322f

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2208999
    const/16 v0, 0x2b

    const v3, -0x2420a7b2

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
