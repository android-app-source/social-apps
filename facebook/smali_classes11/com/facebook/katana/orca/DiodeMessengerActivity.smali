.class public Lcom/facebook/katana/orca/DiodeMessengerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public p:LX/14x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/G6t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/17T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2PR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/23i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/35f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:Lcom/facebook/katana/orca/DiodeHostFragment;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2209128
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(LX/B9u;)V
    .locals 3

    .prologue
    .line 2209163
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->y:Lcom/facebook/katana/orca/DiodeHostFragment;

    if-nez v0, :cond_0

    .line 2209164
    new-instance v0, Lcom/facebook/katana/orca/DiodeHostFragment;

    invoke-direct {v0}, Lcom/facebook/katana/orca/DiodeHostFragment;-><init>()V

    .line 2209165
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2209166
    const-string v2, "click_through"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2209167
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2209168
    move-object v0, v0

    .line 2209169
    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->y:Lcom/facebook/katana/orca/DiodeHostFragment;

    .line 2209170
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->y:Lcom/facebook/katana/orca/DiodeHostFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2209171
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/katana/orca/DiodeMessengerActivity;LX/14x;LX/G6t;LX/17T;Ljava/lang/Boolean;LX/2PR;LX/0ad;LX/23i;LX/0Ot;LX/35f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/orca/DiodeMessengerActivity;",
            "LX/14x;",
            "LX/G6t;",
            "LX/17T;",
            "Ljava/lang/Boolean;",
            "LX/2PR;",
            "LX/0ad;",
            "LX/23i;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/35f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2209162
    iput-object p1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->p:LX/14x;

    iput-object p2, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->q:LX/G6t;

    iput-object p3, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->r:LX/17T;

    iput-object p4, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->s:Ljava/lang/Boolean;

    iput-object p5, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->t:LX/2PR;

    iput-object p6, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->u:LX/0ad;

    iput-object p7, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->v:LX/23i;

    iput-object p8, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->w:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->x:LX/35f;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    invoke-static {v9}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v1

    check-cast v1, LX/14x;

    new-instance v5, LX/G6t;

    invoke-static {v9}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v2

    check-cast v2, LX/14x;

    invoke-static {v9}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {v9}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v2, v3, v4}, LX/G6t;-><init>(LX/14x;Ljava/lang/Boolean;LX/0ad;)V

    move-object v2, v5

    check-cast v2, LX/G6t;

    invoke-static {v9}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v3

    check-cast v3, LX/17T;

    invoke-static {v9}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-static {v9}, LX/2PR;->a(LX/0QB;)LX/2PR;

    move-result-object v5

    check-cast v5, LX/2PR;

    invoke-static {v9}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v9}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v7

    check-cast v7, LX/23i;

    const/16 v8, 0x455

    invoke-static {v9, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v9}, LX/35f;->a(LX/0QB;)LX/35f;

    move-result-object v9

    check-cast v9, LX/35f;

    invoke-static/range {v0 .. v9}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->a(Lcom/facebook/katana/orca/DiodeMessengerActivity;LX/14x;LX/G6t;LX/17T;Ljava/lang/Boolean;LX/2PR;LX/0ad;LX/23i;LX/0Ot;LX/35f;)V

    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2209155
    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->s:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2209156
    :cond_0
    :goto_0
    return v0

    .line 2209157
    :cond_1
    iget-boolean v2, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->z:Z

    if-nez v2, :cond_2

    move v0, v1

    .line 2209158
    goto :goto_0

    .line 2209159
    :cond_2
    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->t:LX/2PR;

    .line 2209160
    iget-boolean v3, v2, LX/2PR;->r:Z

    move v2, v3

    .line 2209161
    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->l()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private l()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2209172
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->t:LX/2PR;

    invoke-virtual {v1}, LX/2PR;->e()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->u:LX/0ad;

    sget-short v2, LX/FBY;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2209154
    const-string v0, "diode_messenger_activity"

    return-object v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2209151
    instance-of v0, p1, Lcom/facebook/katana/orca/DiodeHostFragment;

    if-eqz v0, :cond_0

    .line 2209152
    check-cast p1, Lcom/facebook/katana/orca/DiodeHostFragment;

    iput-object p1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->y:Lcom/facebook/katana/orca/DiodeHostFragment;

    .line 2209153
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2209133
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2209134
    invoke-static {p0, p0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2209135
    invoke-virtual {p0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_from_messenger_button"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->z:Z

    .line 2209136
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->p:LX/14x;

    invoke-virtual {v0}, LX/14x;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->q:LX/G6t;

    const/4 v1, 0x0

    .line 2209137
    iget-boolean v2, v0, LX/G6t;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/G6t;->a:LX/14x;

    invoke-virtual {v2}, LX/14x;->a()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/G6t;->c:LX/0ad;

    sget-short p1, LX/G6s;->a:S

    invoke-interface {v2, p1, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 2209138
    if-eqz v0, :cond_2

    .line 2209139
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->r:LX/17T;

    invoke-static {}, LX/2g8;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2209140
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->v:LX/23i;

    invoke-virtual {v0}, LX/23i;->c()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->v:LX/23i;

    invoke-virtual {v0}, LX/23i;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2209141
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->x:LX/35f;

    invoke-virtual {v1}, LX/35f;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2209142
    :cond_4
    const v0, 0x7f03041e

    invoke-virtual {p0, v0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->setContentView(I)V

    .line 2209143
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2209144
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2209145
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2209146
    new-instance v2, LX/FBI;

    invoke-direct {v2, p0, v1}, LX/FBI;-><init>(Lcom/facebook/katana/orca/DiodeMessengerActivity;LX/0gc;)V

    invoke-interface {v0, v2}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2209147
    invoke-virtual {p0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "click_through"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "click_through"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/B9u;

    .line 2209148
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->a(LX/B9u;)V

    .line 2209149
    return-void

    .line 2209150
    :cond_5
    sget-object v0, LX/B9u;->THREAD_LIST:LX/B9u;

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0xeecce71

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209129
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2209130
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->p:LX/14x;

    invoke-virtual {v1}, LX/14x;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->p:LX/14x;

    invoke-virtual {v1}, LX/14x;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeMessengerActivity;->v:LX/23i;

    invoke-virtual {v1}, LX/23i;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2209131
    invoke-virtual {p0}, Lcom/facebook/katana/orca/DiodeMessengerActivity;->finish()V

    .line 2209132
    :cond_0
    const/16 v1, 0x23

    const v2, 0x218bfab5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
