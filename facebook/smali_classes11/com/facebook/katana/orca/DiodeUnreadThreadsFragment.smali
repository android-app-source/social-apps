.class public Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/17T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FBU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Lcom/facebook/resources/ui/FbButton;

.field private h:Landroid/widget/LinearLayout;

.field public i:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

.field public j:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

.field public k:Landroid/view/View;

.field public l:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2209511
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static c(Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2209506
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2209507
    :goto_0
    return-void

    .line 2209508
    :cond_0
    const v0, 0x7f0d0cba

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2209509
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2209510
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209503
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2209504
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-static {p1}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v2

    check-cast v2, LX/17T;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    new-instance v1, LX/FBU;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p1}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-direct {v1, v4, v0}, LX/FBU;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    move-object v4, v1

    check-cast v4, LX/FBU;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->a:LX/17T;

    iput-object v3, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->b:LX/0Zb;

    iput-object v4, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->c:LX/FBU;

    iput-object p1, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->d:Ljava/util/concurrent/Executor;

    .line 2209505
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x52fa97f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209500
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2209501
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->g:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/FBV;

    invoke-direct {v2, p0}, LX/FBV;-><init>(Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2209502
    const/16 v1, 0x2b

    const v2, 0x7a06cd0a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3c1b5f3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209470
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2209471
    const v1, 0x7f030423

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x25e0c70b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2209472
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2209473
    const v0, 0x7f0d0cb3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->e:Landroid/widget/TextView;

    .line 2209474
    const v0, 0x7f0d0cb4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->f:Landroid/widget/TextView;

    .line 2209475
    const v0, 0x7f0d0cbb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->g:Lcom/facebook/resources/ui/FbButton;

    .line 2209476
    const v0, 0x7f0d0cb5

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->h:Landroid/widget/LinearLayout;

    .line 2209477
    const v0, 0x7f0d0cb7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->i:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    .line 2209478
    const v0, 0x7f0d0cb8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->j:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    .line 2209479
    const v0, 0x7f0d0cb6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->k:Landroid/view/View;

    .line 2209480
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2209481
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->l:I

    .line 2209482
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2209483
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->c:LX/FBU;

    const/4 v1, 0x2

    .line 2209484
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 2209485
    new-instance v3, LX/FFK;

    invoke-direct {v3}, LX/FFK;-><init>()V

    move-object v3, v3

    .line 2209486
    const-string v4, "thread_count"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string p1, "participant_count"

    const/4 p2, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v4, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2209487
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2209488
    iget-object v4, v0, LX/FBU;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2209489
    new-instance v4, LX/FBT;

    invoke-direct {v4, v0, v2}, LX/FBT;-><init>(LX/FBU;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object p1, v0, LX/FBU;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2209490
    move-object v0, v2

    .line 2209491
    new-instance v1, LX/FBW;

    invoke-direct {v1, p0}, LX/FBW;-><init>(Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;)V

    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2209492
    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 2209493
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 2209494
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0164

    iget v4, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->l:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->l:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p1

    aput-object v0, v5, p2

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2209495
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->f:Landroid/widget/TextView;

    const v2, 0x7f083237

    new-array v3, p2, [Ljava/lang/Object;

    aput-object v0, v3, p1

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2209496
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->g:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f083238

    new-array v3, p2, [Ljava/lang/Object;

    aput-object v0, v3, p1

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2209497
    return-void

    .line 2209498
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2209499
    const-string v2, "diode_unread_count_key"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto/16 :goto_0
.end method
