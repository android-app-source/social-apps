.class public Lcom/facebook/katana/orca/DiodeHostFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fv;


# instance fields
.field public a:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/14x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2hf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/forcemessenger/annotations/IsQpInDiodeDisabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/23i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2PR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/B9u;

.field public i:Landroid/support/v4/app/Fragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2209008
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2209009
    sget-object v0, LX/B9u;->THREAD_LIST:LX/B9u;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->h:LX/B9u;

    return-void
.end method

.method public static b(Lcom/facebook/katana/orca/DiodeHostFragment;)V
    .locals 3

    .prologue
    .line 2209010
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2209011
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 2209012
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0c9a

    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2209013
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2209014
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2209015
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/katana/orca/DiodeHostFragment;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v4

    check-cast v4, LX/14x;

    invoke-static {v0}, LX/2hf;->a(LX/0QB;)LX/2hf;

    move-result-object v5

    check-cast v5, LX/2hf;

    const/16 v6, 0x151b

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v7

    check-cast v7, LX/23i;

    invoke-static {v0}, LX/2PR;->a(LX/0QB;)LX/2PR;

    move-result-object p1

    check-cast p1, LX/2PR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/katana/orca/DiodeHostFragment;->a:LX/0iA;

    iput-object v4, v2, Lcom/facebook/katana/orca/DiodeHostFragment;->b:LX/14x;

    iput-object v5, v2, Lcom/facebook/katana/orca/DiodeHostFragment;->c:LX/2hf;

    iput-object v6, v2, Lcom/facebook/katana/orca/DiodeHostFragment;->d:LX/0Or;

    iput-object v7, v2, Lcom/facebook/katana/orca/DiodeHostFragment;->e:LX/23i;

    iput-object p1, v2, Lcom/facebook/katana/orca/DiodeHostFragment;->f:LX/2PR;

    iput-object v0, v2, Lcom/facebook/katana/orca/DiodeHostFragment;->g:LX/0ad;

    .line 2209016
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2209017
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, LX/0fv;

    if-eqz v0, :cond_0

    .line 2209018
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    check-cast v0, LX/0fv;

    invoke-interface {v0}, LX/0fv;->f()V

    .line 2209019
    :cond_0
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2209020
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, LX/0fv;

    if-eqz v0, :cond_0

    .line 2209021
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    check-cast v0, LX/0fv;

    invoke-interface {v0}, LX/0fv;->j()Z

    move-result v0

    .line 2209022
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2209023
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, LX/0fv;

    if-eqz v0, :cond_0

    .line 2209024
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    check-cast v0, LX/0fv;

    invoke-interface {v0}, LX/0fw;->k()LX/0g8;

    move-result-object v0

    .line 2209025
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0xfd9183c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2209026
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2209027
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2209028
    if-eqz v0, :cond_1

    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2209029
    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/B9u;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->h:LX/B9u;

    .line 2209030
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "is_from_messenger_button"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->j:Z

    .line 2209031
    const v0, 0x1e1f5ec6

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2209032
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2209033
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "click_through"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/B9u;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->h:LX/B9u;

    goto :goto_0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 2209034
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 2209035
    iput-object p1, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    .line 2209036
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x76bb8b2c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209037
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2209038
    const v1, 0x7f03041c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x7086e0fa

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x38180e36

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2209039
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2209040
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->b:LX/14x;

    invoke-virtual {v0}, LX/14x;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2209041
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->b:LX/14x;

    invoke-virtual {v0}, LX/14x;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2209042
    new-instance v0, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;

    invoke-direct {v0}, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;-><init>()V

    move-object v0, v0

    .line 2209043
    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    .line 2209044
    invoke-static {p0}, Lcom/facebook/katana/orca/DiodeHostFragment;->b(Lcom/facebook/katana/orca/DiodeHostFragment;)V

    .line 2209045
    :cond_0
    :goto_0
    const v0, -0x15da99bf

    invoke-static {v0, v1}, LX/02F;->f(II)V

    .line 2209046
    :goto_1
    return-void

    .line 2209047
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->e:LX/23i;

    invoke-virtual {v0}, LX/23i;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2209048
    new-instance v0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;

    invoke-direct {v0}, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;-><init>()V

    move-object v0, v0

    .line 2209049
    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    .line 2209050
    invoke-static {p0}, Lcom/facebook/katana/orca/DiodeHostFragment;->b(Lcom/facebook/katana/orca/DiodeHostFragment;)V

    .line 2209051
    goto :goto_0

    .line 2209052
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->e:LX/23i;

    invoke-virtual {v0}, LX/23i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2209053
    new-instance v0, Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;

    invoke-direct {v0}, Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;-><init>()V

    move-object v0, v0

    .line 2209054
    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    .line 2209055
    invoke-static {p0}, Lcom/facebook/katana/orca/DiodeHostFragment;->b(Lcom/facebook/katana/orca/DiodeHostFragment;)V

    .line 2209056
    goto :goto_0

    .line 2209057
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/facebook/katana/orca/DiodeQpFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/facebook/katana/orca/DiodeQpFragment;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/facebook/katana/orca/DiodeQpFragment;

    .line 2209058
    iget-boolean v2, v0, Lcom/facebook/katana/orca/DiodeQpFragment;->k:Z

    move v0, v2

    .line 2209059
    if-eqz v0, :cond_6

    .line 2209060
    :cond_4
    iget-object v3, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2209061
    new-instance v3, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;

    invoke-direct {v3}, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;-><init>()V

    .line 2209062
    :cond_5
    :goto_2
    move-object v0, v3

    .line 2209063
    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    .line 2209064
    invoke-static {p0}, Lcom/facebook/katana/orca/DiodeHostFragment;->b(Lcom/facebook/katana/orca/DiodeHostFragment;)V

    .line 2209065
    :cond_6
    const v0, -0x6f48e1cf

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_1

    .line 2209066
    :cond_7
    iget-object v3, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->f:LX/2PR;

    invoke-virtual {v3}, LX/2PR;->e()I

    move-result v4

    .line 2209067
    iget-boolean v3, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->j:Z

    if-eqz v3, :cond_8

    if-lez v4, :cond_8

    iget-object v3, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->g:LX/0ad;

    sget-short v5, LX/FBY;->a:S

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2209068
    new-instance v3, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-direct {v3}, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;-><init>()V

    .line 2209069
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2209070
    const-string v6, "diode_unread_count_key"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2209071
    invoke-virtual {v3, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_2

    .line 2209072
    :cond_8
    const/4 v3, 0x0

    .line 2209073
    iget-boolean v4, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->j:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->f:LX/2PR;

    .line 2209074
    iget-boolean v5, v4, LX/2PR;->r:Z

    move v4, v5

    .line 2209075
    if-eqz v4, :cond_9

    .line 2209076
    iget-object v3, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->a:LX/0iA;

    new-instance v4, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v5, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB_BADGEABLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v4, v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v5, LX/3kR;

    invoke-virtual {v3, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/13D;

    .line 2209077
    iget-object v4, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->f:LX/2PR;

    .line 2209078
    const/4 v7, 0x0

    iput-boolean v7, v4, LX/2PR;->r:Z

    .line 2209079
    invoke-static {v4}, LX/2PR;->i(LX/2PR;)V

    .line 2209080
    iget-object v7, v4, LX/2PR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v8, LX/2PR;->b:LX/0Tn;

    iget-object v9, v4, LX/2PR;->m:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    invoke-interface {v7, v8, v9, v10}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    .line 2209081
    :cond_9
    if-nez v3, :cond_a

    .line 2209082
    iget-object v3, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->a:LX/0iA;

    iget-object v4, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->h:LX/B9u;

    invoke-virtual {v4}, LX/B9u;->getTriggerForDestination()Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-result-object v4

    const-class v5, LX/3kR;

    invoke-virtual {v3, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/13D;

    .line 2209083
    :cond_a
    if-nez v3, :cond_b

    .line 2209084
    new-instance v3, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;

    invoke-direct {v3}, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;-><init>()V

    goto/16 :goto_2

    .line 2209085
    :cond_b
    iget-object v4, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->c:LX/2hf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v3

    .line 2209086
    if-nez v3, :cond_5

    .line 2209087
    new-instance v3, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;

    invoke-direct {v3}, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;-><init>()V

    goto/16 :goto_2
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2209088
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2209089
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 2209090
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeHostFragment;->i:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 2209091
    :cond_0
    return-void
.end method
