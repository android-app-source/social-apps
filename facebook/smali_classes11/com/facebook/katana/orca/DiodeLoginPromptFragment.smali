.class public Lcom/facebook/katana/orca/DiodeLoginPromptFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/resources/ui/FbButton;

.field private e:Lcom/facebook/user/tiles/UserTileView;

.field public f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2209103
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209104
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2209105
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    const/16 v0, 0x12cb

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    iput-object v2, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->b:LX/0Or;

    iput-object p1, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->c:LX/0Zb;

    .line 2209106
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6d3f228f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2209107
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2209108
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->d:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/FBH;

    invoke-direct {v2, p0}, LX/FBH;-><init>(Lcom/facebook/katana/orca/DiodeLoginPromptFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2209109
    iget-object v2, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->e:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    sget-object v3, LX/8ue;->MESSENGER:LX/8ue;

    invoke-static {v0, v3}, LX/8t9;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2209110
    const/16 v0, 0x2b

    const v2, -0x82d8072

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5e32620c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209111
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2209112
    const v1, 0x7f030421

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x291374be

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209113
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2209114
    const v0, 0x7f0d0cae

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->d:Lcom/facebook/resources/ui/FbButton;

    .line 2209115
    const v0, 0x7f0d0ca6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->e:Lcom/facebook/user/tiles/UserTileView;

    .line 2209116
    const v0, 0x7f0d0cad

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->f:Landroid/widget/TextView;

    .line 2209117
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->d:Lcom/facebook/resources/ui/FbButton;

    const p1, 0x7f08322e

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2209118
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->f:Landroid/widget/TextView;

    const p1, 0x7f08322d

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2209119
    return-void
.end method
