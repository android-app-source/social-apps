.class public Lcom/facebook/katana/orca/DiodeUnreadThreadView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/tiles/ThreadTileView;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2209383
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2209384
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209385
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2209386
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209387
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2209388
    const v0, 0x7f031547

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2209389
    const v0, 0x7f0d1ee0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->a:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2209390
    const v0, 0x7f0d091d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->b:Landroid/widget/TextView;

    .line 2209391
    return-void
.end method

.method private a(I)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/16 v4, 0x63

    .line 2209392
    if-gt p1, v4, :cond_0

    .line 2209393
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2209394
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083236

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/FBS;)V
    .locals 2

    .prologue
    .line 2209395
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->a:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2209396
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->b:Landroid/widget/TextView;

    .line 2209397
    iget v1, p1, LX/FBS;->d:I

    move v1, v1

    .line 2209398
    invoke-direct {p0, v1}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2209399
    return-void
.end method
