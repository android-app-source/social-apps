.class public Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;
.super Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;
.source ""


# instance fields
.field public q:LX/2lG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/bookmark/model/BookmarksGroup;

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2209703
    const v0, 0x7f0301c8

    const v1, 0x7f0d0746

    invoke-direct {p0, v0, v1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;-><init>(II)V

    .line 2209704
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;

    invoke-static {p0}, LX/2lG;->b(LX/0QB;)LX/2lG;

    move-result-object v1

    check-cast v1, LX/2lG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    iput-object v1, p1, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->q:LX/2lG;

    iput-object p0, p1, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->r:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2209693
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a(Landroid/os/Bundle;)V

    .line 2209694
    const-class v0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;

    invoke-static {v0, p0}, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2209695
    const v0, 0x7f0301c8

    iput v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->n:I

    .line 2209696
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    if-nez v0, :cond_0

    .line 2209697
    if-eqz p1, :cond_1

    .line 2209698
    const-string v0, "bookmarks_group"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 2209699
    :cond_0
    :goto_0
    return-void

    .line 2209700
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2209701
    if-eqz v0, :cond_0

    .line 2209702
    const-string v1, "bookmarks_group"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V
    .locals 6

    .prologue
    .line 2209685
    if-nez p2, :cond_1

    .line 2209686
    :cond_0
    :goto_0
    return-void

    .line 2209687
    :cond_1
    iget-object v0, p1, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    move-object v2, v0

    .line 2209688
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 2209689
    iget-object v4, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v5, v5, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v5, v5, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2209690
    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 2209691
    invoke-virtual {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->c()V

    goto :goto_0

    .line 2209692
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2209684
    invoke-static {}, LX/FBi;->values()[LX/FBi;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 2209673
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2209674
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->d()Ljava/util/List;

    move-result-object v3

    .line 2209675
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2209676
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 2209677
    invoke-virtual {p0, v0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->b(Lcom/facebook/bookmark/model/Bookmark;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2209678
    const-string v4, "two_rows"

    iget-object v5, v0, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2209679
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->q:LX/2lG;

    sget-object v5, LX/FBi;->FamilyBridgesProfile:LX/FBi;

    invoke-virtual {v4, v5, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b(LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)LX/2lI;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2209680
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2209681
    :cond_1
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->q:LX/2lG;

    sget-object v5, LX/FBi;->Bookmark:LX/FBi;

    invoke-virtual {v4, v5, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->c(LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)LX/2lI;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2209682
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a(Ljava/util/List;)V

    .line 2209683
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x61f469c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209667
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2209668
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-virtual {v1}, Lcom/facebook/bookmark/model/BookmarksGroup;->h()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2209669
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x57a4cda2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2209670
    :cond_1
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v2, v2, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/2l5;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2209671
    if-eqz v1, :cond_0

    .line 2209672
    new-instance v2, LX/FBh;

    invoke-direct {v2, p0}, LX/FBh;-><init>(Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;)V

    iget-object p1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->r:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2209664
    invoke-super/range {p0 .. p5}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 2209665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->t:Z

    .line 2209666
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2209654
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2209655
    const-string v0, "bookmarks_group"

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2209656
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x716fcb8d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2209657
    invoke-super {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onStart()V

    .line 2209658
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2209659
    if-eqz v0, :cond_1

    .line 2209660
    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v2, v2, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2209661
    :cond_0
    const v2, 0x7f0831f7

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2209662
    :cond_1
    :goto_0
    const v0, 0x35b73c28

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2209663
    :cond_2
    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v2, v2, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_0
.end method
