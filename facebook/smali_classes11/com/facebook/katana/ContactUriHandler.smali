.class public Lcom/facebook/katana/ContactUriHandler;
.super Lcom/facebook/katana/activity/BaseFacebookActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2400156
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 2400129
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b(Landroid/os/Bundle;)V

    .line 2400130
    invoke-virtual {p0}, Lcom/facebook/katana/ContactUriHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 2400131
    invoke-static {p0, v8}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 2400132
    if-eqz v0, :cond_0

    .line 2400133
    invoke-virtual {v0}, Lcom/facebook/katana/service/AppSession;->b()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v0

    .line 2400134
    iget-wide v4, v0, Lcom/facebook/katana/model/FacebookSessionInfo;->userId:J

    .line 2400135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profile/"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2400136
    if-eqz v1, :cond_5

    .line 2400137
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 2400138
    const-string v2, "content"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2400139
    const-string v0, "com.android.contacts"

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2400140
    invoke-virtual {p0}, Lcom/facebook/katana/ContactUriHandler;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2400141
    new-array v2, v8, [Ljava/lang/String;

    const-string v4, "data1"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2400142
    if-eqz v1, :cond_4

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2400143
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2400144
    invoke-virtual {p0}, Lcom/facebook/katana/ContactUriHandler;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 2400145
    const-string v4, "vnd.android.cursor.item/vnd.facebook.profile"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2400146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "profile/"

    invoke-static {v4}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    move-object v0, v6

    .line 2400147
    :goto_0
    if-eqz v1, :cond_3

    .line 2400148
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v1, v0

    .line 2400149
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(LX/0QB;)Lcom/facebook/katana/urimap/IntentHandlerUtil;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-virtual {v0, p0, v1}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2400150
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/ContactUriHandler;->finish()V

    .line 2400151
    return-void

    .line 2400152
    :cond_1
    :try_start_1
    const-string v4, "vnd.android.cursor.item/vnd.facebook.presence"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2400153
    sget-object v0, LX/0ax;->am:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    move-object v0, v6

    goto :goto_0

    .line 2400154
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 2400155
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :cond_3
    move-object v1, v0

    goto :goto_1

    :cond_4
    move-object v0, v6

    goto :goto_0

    :cond_5
    move-object v1, v6

    goto :goto_1
.end method
