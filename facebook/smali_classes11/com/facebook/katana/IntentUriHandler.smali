.class public Lcom/facebook/katana/IntentUriHandler;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f1;
.implements LX/Eln;


# annotations
.annotation runtime Lcom/facebook/base/activity/DeliverOnNewIntentWhenFinishing;
.end annotation


# instance fields
.field public p:LX/GR7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/katana/urimap/IntentHandlerUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2400299
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/IntentUriHandler;LX/GR7;Lcom/facebook/katana/urimap/IntentHandlerUtil;)V
    .locals 0

    .prologue
    .line 2400300
    iput-object p1, p0, Lcom/facebook/katana/IntentUriHandler;->p:LX/GR7;

    iput-object p2, p0, Lcom/facebook/katana/IntentUriHandler;->q:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/katana/IntentUriHandler;

    new-instance v2, LX/GR7;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const/16 p1, 0x259

    invoke-static {v1, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {v2, v0, p1}, LX/GR7;-><init>(LX/0Zb;LX/0Ot;)V

    move-object v0, v2

    check-cast v0, LX/GR7;

    invoke-static {v1}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(LX/0QB;)Lcom/facebook/katana/urimap/IntentHandlerUtil;

    move-result-object v1

    check-cast v1, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-static {p0, v0, v1}, Lcom/facebook/katana/IntentUriHandler;->a(Lcom/facebook/katana/IntentUriHandler;LX/GR7;Lcom/facebook/katana/urimap/IntentHandlerUtil;)V

    return-void
.end method

.method private l()Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2400301
    invoke-virtual {p0}, Lcom/facebook/katana/IntentUriHandler;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2400302
    const-string v0, "android.intent.extra.REFERRER"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2400303
    if-eqz v0, :cond_0

    .line 2400304
    :goto_0
    return-object v0

    .line 2400305
    :cond_0
    const-string v0, "android.intent.extra.REFERRER_NAME"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2400306
    if-eqz v0, :cond_1

    .line 2400307
    :try_start_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2400308
    :catch_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2400309
    goto :goto_0
.end method

.method private m()V
    .locals 11

    .prologue
    .line 2400258
    invoke-direct {p0}, Lcom/facebook/katana/IntentUriHandler;->n()Ljava/lang/String;

    move-result-object v0

    .line 2400259
    invoke-virtual {p0}, Lcom/facebook/katana/IntentUriHandler;->getReferrer()Landroid/net/Uri;

    move-result-object v1

    .line 2400260
    iget-object v2, p0, Lcom/facebook/katana/IntentUriHandler;->p:LX/GR7;

    const/4 v4, 0x0

    .line 2400261
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 2400262
    :cond_0
    :goto_0
    return-void

    .line 2400263
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v5, "http"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v5, "https"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2400264
    :cond_2
    const-string v4, "browser"

    .line 2400265
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 2400266
    :cond_3
    :goto_1
    if-eqz v4, :cond_0

    .line 2400267
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2400268
    iget-object v6, v2, LX/GR7;->b:LX/0Zb;

    sget-object v7, LX/GR6;->APP_INDEXING_EVENT_TYPE:LX/GR6;

    invoke-virtual {v7}, LX/GR6;->getEventName()Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x0

    invoke-interface {v6, v7, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2400269
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2400270
    const-string v7, "app_indexing"

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "launcher_type"

    invoke-virtual {v6, v7, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "launch_host"

    invoke-virtual {v6, v7, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "link_tag"

    invoke-virtual {v6, v7, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "referrer_uri"

    invoke-virtual {v6, v7, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2400271
    :cond_4
    goto :goto_0

    .line 2400272
    :cond_5
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v5, "android-app"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2400273
    :try_start_0
    invoke-static {v1}, LX/GR5;->a(Landroid/net/Uri;)LX/GR5;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2400274
    iget-object v5, v3, LX/GR5;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2400275
    const/4 p0, 0x1

    .line 2400276
    iget-object v6, v3, LX/GR5;->a:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v8

    .line 2400277
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    .line 2400278
    if-gtz v9, :cond_8

    .line 2400279
    const/4 v6, 0x0

    .line 2400280
    :goto_2
    move-object v3, v6

    .line 2400281
    if-eqz v3, :cond_0

    .line 2400282
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 2400283
    const-string v6, "com.google.android.googlequicksearchbox"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2400284
    const-string v4, "google_launcher"

    goto :goto_1

    .line 2400285
    :catch_0
    move-exception v4

    .line 2400286
    iget-object v3, v2, LX/GR7;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    sget-object v5, LX/GR7;->a:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2400287
    :cond_6
    const-string v6, "com.gau.go.launcherex"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2400288
    const-string v4, "go_launcher"

    goto/16 :goto_1

    :cond_7
    move-object v3, v4

    goto/16 :goto_1

    .line 2400289
    :cond_8
    new-instance v7, Landroid/net/Uri$Builder;

    invoke-direct {v7}, Landroid/net/Uri$Builder;-><init>()V

    const/4 v6, 0x0

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    .line 2400290
    if-le v9, p0, :cond_9

    .line 2400291
    invoke-interface {v8, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v10, v6}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2400292
    const/4 v6, 0x2

    move v7, v6

    :goto_3
    if-ge v7, v9, :cond_9

    .line 2400293
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v10, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2400294
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_3

    .line 2400295
    :cond_9
    iget-object v6, v3, LX/GR5;->a:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2400296
    iget-object v6, v3, LX/GR5;->a:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2400297
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    goto :goto_2
.end method

.method private n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2400257
    invoke-virtual {p0}, Lcom/facebook/katana/IntentUriHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2400298
    const-string v0, "infrastructure"

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2400254
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2400255
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/katana/IntentUriHandler;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x18000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/IntentUriHandler;->startActivity(Landroid/content/Intent;)V

    .line 2400256
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2400249
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2400250
    invoke-virtual {p0}, Lcom/facebook/katana/IntentUriHandler;->getReferrer()Landroid/net/Uri;

    move-result-object v1

    .line 2400251
    if-eqz v1, :cond_0

    .line 2400252
    const-string v2, "referrer"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2400253
    :cond_0
    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2400244
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2400245
    invoke-static {p0, p0}, Lcom/facebook/katana/IntentUriHandler;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2400246
    invoke-direct {p0}, Lcom/facebook/katana/IntentUriHandler;->m()V

    .line 2400247
    iget-object v0, p0, Lcom/facebook/katana/IntentUriHandler;->q:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-virtual {p0}, Lcom/facebook/katana/IntentUriHandler;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2400248
    return-void
.end method

.method public final getReferrer()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 2400241
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    .line 2400242
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->getReferrer()Landroid/net/Uri;

    move-result-object v0

    .line 2400243
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/IntentUriHandler;->l()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x6542a3b5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2400238
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2400239
    invoke-virtual {p0}, Lcom/facebook/katana/IntentUriHandler;->finish()V

    .line 2400240
    const/16 v1, 0x23

    const v2, -0x551b1707

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
