.class public Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2405002
    const-class v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    new-instance v1, Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreSerializer;

    invoke-direct {v1}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2405003
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2405001
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2404995
    if-nez p0, :cond_0

    .line 2404996
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2404997
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2404998
    invoke-static {p0, p1, p2}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreSerializer;->b(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;LX/0nX;LX/0my;)V

    .line 2404999
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2405000
    return-void
.end method

.method private static b(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2404991
    const-string v0, "version"

    iget-object v1, p0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->mVersion:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2404992
    const-string v0, "components"

    iget-object v1, p0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->mSkeletonString:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2404993
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2404994
    check-cast p1, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    invoke-static {p1, p2, p3}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreSerializer;->a(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;LX/0nX;LX/0my;)V

    return-void
.end method
