.class public Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreSerializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field private static final serialVersionUID:J = 0x54b2befb0b8db963L


# instance fields
.field private mSkeleton:Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;

.field public mSkeletonString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "components"
    .end annotation
.end field

.field public mVersion:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2404963
    const-class v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2404964
    const-class v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStoreSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2404967
    const-class v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    .line 2404968
    sput-object v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2404965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2404966
    return-void
.end method

.method private static a(LX/15w;)Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
    .locals 1

    .prologue
    .line 2404951
    const-class v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    invoke-virtual {p0, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/0lC;)Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
    .locals 4

    .prologue
    .line 2404952
    if-nez p0, :cond_0

    .line 2404953
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cannot deserialize FacewebComponentStore from a null String"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2404954
    :cond_0
    :try_start_0
    const-class v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    invoke-virtual {p1, p0, v0}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2404955
    :goto_0
    return-object v0

    .line 2404956
    :catch_0
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 2404957
    new-instance v1, LX/GvC;

    new-instance v2, Ljava/util/zip/GZIPInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, LX/GvC;-><init>(Ljava/io/InputStream;)V

    .line 2404958
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 2404959
    :catch_1
    move-exception v0

    .line 2404960
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not deserialize FacewebComponentStore, class not found"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2404961
    :catch_2
    move-exception v0

    .line 2404962
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not deserialize FacewebComponentStore, bad base64 input"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;LX/0lp;)Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
    .locals 1

    .prologue
    .line 2404933
    invoke-virtual {p1, p0}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 2404934
    invoke-static {v0}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->a(LX/15w;)Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;
    .locals 3

    .prologue
    .line 2404935
    iget-object v0, p0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->mSkeleton:Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;

    if-nez v0, :cond_0

    .line 2404936
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->mSkeletonString:Ljava/lang/String;

    const-class v2, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;

    iput-object v0, p0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->mSkeleton:Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;

    .line 2404937
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->mSkeleton:Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2404938
    iget-object v0, p0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0lC;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2404939
    invoke-virtual {p1, p0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$FacewebSkeletonPalCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2404940
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2404941
    invoke-direct {p0}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->c()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;

    move-result-object v3

    .line 2404942
    if-nez v3, :cond_0

    move-object v0, v2

    .line 2404943
    :goto_0
    return-object v0

    .line 2404944
    :cond_0
    iget-object v0, v3, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;->mRules:Ljava/util/List;

    invoke-static {v0}, LX/0sL;->a(Ljava/lang/Object;)V

    .line 2404945
    iget-object v0, v3, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;->mRules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$FacewebComponentsRule;

    .line 2404946
    invoke-virtual {v0, p1}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$FacewebComponentsRule;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2404947
    iget-object v1, v3, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$Skeleton;->mComponents:Ljava/util/Map;

    iget-object v5, v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$FacewebComponentsRule;->mComponentsId:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2404948
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2404949
    iget-boolean v0, v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore$FacewebComponentsRule;->mContinueSearch:Z

    if-nez v0, :cond_1

    :cond_2
    move-object v0, v2

    .line 2404950
    goto :goto_0
.end method
