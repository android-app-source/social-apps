.class public Lcom/facebook/katana/platform/PendingMediaUpload;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/platform/PendingMediaUpload;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2407026
    new-instance v0, LX/GwN;

    invoke-direct {v0}, LX/GwN;-><init>()V

    sput-object v0, Lcom/facebook/katana/platform/PendingMediaUpload;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2407022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407023
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/platform/PendingMediaUpload;->a:Ljava/lang/String;

    .line 2407024
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/platform/PendingMediaUpload;->b:Ljava/lang/String;

    .line 2407025
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2407018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407019
    iput-object p1, p0, Lcom/facebook/katana/platform/PendingMediaUpload;->a:Ljava/lang/String;

    .line 2407020
    iput-object p2, p0, Lcom/facebook/katana/platform/PendingMediaUpload;->b:Ljava/lang/String;

    .line 2407021
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2407017
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2407014
    iget-object v0, p0, Lcom/facebook/katana/platform/PendingMediaUpload;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2407015
    iget-object v0, p0, Lcom/facebook/katana/platform/PendingMediaUpload;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2407016
    return-void
.end method
