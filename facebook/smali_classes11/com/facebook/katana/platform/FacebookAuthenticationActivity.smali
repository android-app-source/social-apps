.class public Lcom/facebook/katana/platform/FacebookAuthenticationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Landroid/content/ComponentName;
    .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2406382
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/platform/FacebookAuthenticationActivity;Lcom/facebook/content/SecureContextHelper;Landroid/content/ComponentName;LX/03V;)V
    .locals 0

    .prologue
    .line 2406383
    iput-object p1, p0, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->q:Landroid/content/ComponentName;

    iput-object p3, p0, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->r:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->a(Lcom/facebook/katana/platform/FacebookAuthenticationActivity;Lcom/facebook/content/SecureContextHelper;Landroid/content/ComponentName;LX/03V;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2406384
    invoke-static {p0, p0}, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2406385
    invoke-virtual {p0}, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2406386
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2406387
    if-eqz v1, :cond_2

    const-string v2, "accountAuthenticatorResponse"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2406388
    if-nez v1, :cond_0

    .line 2406389
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2406390
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2406391
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->r:LX/03V;

    const-string v2, "add_account_api"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "incoming intent did not have expected extras "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406392
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2406393
    const-string v1, "accountAuthenticatorResponse"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/AccountAuthenticatorResponse;

    .line 2406394
    new-instance v2, LX/Gvp;

    invoke-direct {v2}, LX/Gvp;-><init>()V

    .line 2406395
    iput-object v1, v2, LX/Gvp;->a:Landroid/accounts/AccountAuthenticatorResponse;

    .line 2406396
    move-object v1, v2

    .line 2406397
    move-object v0, v1

    .line 2406398
    iget-object v1, p0, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->q:Landroid/content/ComponentName;

    .line 2406399
    iput-object v1, v0, LX/Gvp;->b:Landroid/content/ComponentName;

    .line 2406400
    move-object v0, v0

    .line 2406401
    invoke-virtual {v0}, LX/Gvp;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2406402
    iget-object v1, p0, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2406403
    invoke-virtual {p0}, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;->finish()V

    .line 2406404
    return-void

    .line 2406405
    :cond_1
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
