.class public final Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2407201
    new-instance v0, LX/Gwd;

    invoke-direct {v0}, LX/Gwd;-><init>()V

    sput-object v0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2407202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->a:Ljava/lang/String;

    .line 2407204
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->b:Z

    .line 2407205
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2407206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407207
    iput-object p1, p0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->a:Ljava/lang/String;

    .line 2407208
    iput-boolean p2, p0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->b:Z

    .line 2407209
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2407210
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2407211
    iget-object v0, p0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2407212
    iget-boolean v0, p0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2407213
    return-void
.end method
