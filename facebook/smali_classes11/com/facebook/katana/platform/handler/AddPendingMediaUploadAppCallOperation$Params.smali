.class public final Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/platform/common/action/PlatformAppCall;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2407165
    new-instance v0, LX/Gwb;

    invoke-direct {v0}, LX/Gwb;-><init>()V

    sput-object v0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2407174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407175
    const-class v0, Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/common/action/PlatformAppCall;

    iput-object v0, p0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->a:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2407176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->b:Ljava/lang/String;

    .line 2407177
    return-void
.end method

.method public constructor <init>(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2407170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407171
    iput-object p1, p0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->a:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2407172
    iput-object p2, p0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->b:Ljava/lang/String;

    .line 2407173
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2407169
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2407166
    iget-object v0, p0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->a:Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2407167
    iget-object v0, p0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2407168
    return-void
.end method
