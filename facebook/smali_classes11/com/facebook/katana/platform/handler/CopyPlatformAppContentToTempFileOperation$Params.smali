.class public final Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2407191
    new-instance v0, LX/Gwc;

    invoke-direct {v0}, LX/Gwc;-><init>()V

    sput-object v0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2407192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->a:Ljava/lang/String;

    .line 2407194
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->b:Ljava/util/ArrayList;

    .line 2407195
    iget-object v0, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2407196
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->c:Ljava/lang/String;

    .line 2407197
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2407186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407187
    iput-object p1, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->a:Ljava/lang/String;

    .line 2407188
    iput-object p2, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->b:Ljava/util/ArrayList;

    .line 2407189
    iput-object p3, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->c:Ljava/lang/String;

    .line 2407190
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2407185
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2407181
    iget-object v0, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2407182
    iget-object v0, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2407183
    iget-object v0, p0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2407184
    return-void
.end method
