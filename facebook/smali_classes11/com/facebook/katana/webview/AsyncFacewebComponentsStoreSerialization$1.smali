.class public final Lcom/facebook/katana/webview/AsyncFacewebComponentsStoreSerialization$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

.field public final synthetic b:LX/Gwq;


# direct methods
.method public constructor <init>(LX/Gwq;Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V
    .locals 0

    .prologue
    .line 2407443
    iput-object p1, p0, Lcom/facebook/katana/webview/AsyncFacewebComponentsStoreSerialization$1;->b:LX/Gwq;

    iput-object p2, p0, Lcom/facebook/katana/webview/AsyncFacewebComponentsStoreSerialization$1;->a:Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 2407444
    iget-object v0, p0, Lcom/facebook/katana/webview/AsyncFacewebComponentsStoreSerialization$1;->b:LX/Gwq;

    iget-object v0, v0, LX/Gwq;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/katana/webview/AsyncFacewebComponentsStoreSerialization$1;->a:Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    .line 2407445
    invoke-static {v0}, LX/GxG;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 2407446
    if-nez v2, :cond_0

    .line 2407447
    const-string v2, ""

    .line 2407448
    :cond_0
    new-instance v3, LX/Gwt;

    invoke-static {v0}, LX/38I;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, LX/Gwt;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 2407449
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2407450
    invoke-static {v0}, LX/Gwx;->d(Landroid/content/Context;)LX/Gwz;

    move-result-object v3

    .line 2407451
    if-nez v1, :cond_1

    .line 2407452
    new-instance v8, LX/Gww;

    sget-object v4, LX/Gwv;->UNKNOWN_ERROR:LX/Gwv;

    const-string v6, "Fail fetching FacewebComponentsStore"

    invoke-direct {v8, v4, v6}, LX/Gww;-><init>(LX/Gwv;Ljava/lang/String;)V

    move-object v4, v0

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, LX/Gwz;->a(Landroid/content/Context;ZLX/Gwt;Ljava/lang/String;LX/Gww;)V

    .line 2407453
    :goto_0
    return-void

    .line 2407454
    :cond_1
    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    invoke-static {v4}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    .line 2407455
    :try_start_0
    invoke-virtual {v1, v4}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->a(LX/0lC;)Ljava/lang/String;

    move-result-object v12

    .line 2407456
    const/4 v10, 0x1

    new-instance v13, LX/Gww;

    invoke-direct {v13, v1}, LX/Gww;-><init>(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V

    move-object v8, v3

    move-object v9, v0

    move-object v11, v2

    invoke-virtual/range {v8 .. v13}, LX/Gwz;->a(Landroid/content/Context;ZLX/Gwt;Ljava/lang/String;LX/Gww;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2407457
    :catch_0
    move-exception v4

    .line 2407458
    new-instance v8, LX/Gww;

    sget-object v6, LX/Gwv;->SERIALIZATION_ERROR:LX/Gwv;

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v6, v4}, LX/Gww;-><init>(LX/Gwv;Ljava/lang/String;)V

    move-object v4, v0

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, LX/Gwz;->a(Landroid/content/Context;ZLX/Gwt;Ljava/lang/String;LX/Gww;)V

    goto :goto_0
.end method
