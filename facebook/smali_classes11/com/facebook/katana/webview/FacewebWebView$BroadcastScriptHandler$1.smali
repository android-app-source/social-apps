.class public final Lcom/facebook/katana/webview/FacewebWebView$BroadcastScriptHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Gx9;


# direct methods
.method public constructor <init>(LX/Gx9;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2407721
    iput-object p1, p0, Lcom/facebook/katana/webview/FacewebWebView$BroadcastScriptHandler$1;->b:LX/Gx9;

    iput-object p2, p0, Lcom/facebook/katana/webview/FacewebWebView$BroadcastScriptHandler$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2407722
    invoke-static {}, LX/GxF;->getRegisteredFacewebWebViews()Ljava/util/List;

    move-result-object v0

    .line 2407723
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GxF;

    .line 2407724
    invoke-virtual {v0}, LX/GxF;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 2407725
    if-eqz v2, :cond_0

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2407726
    iget-object v2, p0, Lcom/facebook/katana/webview/FacewebWebView$BroadcastScriptHandler$1;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    goto :goto_0

    .line 2407727
    :cond_0
    goto :goto_0

    .line 2407728
    :cond_1
    return-void
.end method
