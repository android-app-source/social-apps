.class public Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/Gwk;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/Gwk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/Gwk;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407273
    iput-object p1, p0, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;->b:LX/0Or;

    .line 2407274
    iput-object p2, p0, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;->a:LX/Gwk;

    .line 2407275
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;
    .locals 5

    .prologue
    .line 2407276
    const-class v1, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;

    monitor-enter v1

    .line 2407277
    :try_start_0
    sget-object v0, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2407278
    sput-object v2, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2407279
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2407280
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2407281
    new-instance v4, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2407282
    new-instance v3, LX/Gwk;

    invoke-direct {v3}, LX/Gwk;-><init>()V

    .line 2407283
    move-object v3, v3

    .line 2407284
    move-object v3, v3

    .line 2407285
    check-cast v3, LX/Gwk;

    invoke-direct {v4, p0, v3}, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;-><init>(LX/0Or;LX/Gwk;)V

    .line 2407286
    move-object v0, v4

    .line 2407287
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2407288
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2407289
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2407290
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2407262
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2407263
    const-string v1, "login_approval_resend_code"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2407264
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2407265
    if-eqz v1, :cond_0

    .line 2407266
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2407267
    const-string v1, "loginApprovalsResendCodeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;

    .line 2407268
    iget-object v1, p0, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;->a:LX/Gwk;

    const-class v3, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2407269
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2407270
    return-object v0

    .line 2407271
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
