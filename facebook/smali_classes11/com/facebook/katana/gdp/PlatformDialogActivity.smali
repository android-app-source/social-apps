.class public abstract Lcom/facebook/katana/gdp/PlatformDialogActivity;
.super Lcom/facebook/katana/activity/BaseFacebookActivity;
.source ""


# static fields
.field public static p:Ljava/lang/String;

.field public static final s:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/275;

.field public B:Z

.field public C:LX/0Uh;

.field public final D:LX/278;

.field public q:Ljava/lang/String;

.field public r:Lcom/facebook/performancelogger/PerformanceLogger;

.field public t:Ljava/lang/String;

.field public u:Landroid/app/ProgressDialog;

.field public v:Ljava/lang/String;

.field public w:Lcom/facebook/webview/BasicWebView;

.field public x:LX/44G;

.field public y:LX/0W9;

.field private z:Lcom/facebook/fbservice/ops/BlueServiceFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2405842
    const-class v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;

    sput-object v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->s:Ljava/lang/Class;

    .line 2405843
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2405836
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;-><init>()V

    .line 2405837
    iput-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->t:Ljava/lang/String;

    .line 2405838
    iput-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->q:Ljava/lang/String;

    .line 2405839
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->B:Z

    .line 2405840
    new-instance v0, LX/GvZ;

    invoke-direct {v0, p0}, LX/GvZ;-><init>(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->D:LX/278;

    .line 2405841
    return-void
.end method

.method public static a(Lcom/facebook/katana/gdp/PlatformDialogActivity;ZLandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2405827
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    .line 2405828
    :goto_0
    if-nez p2, :cond_1

    .line 2405829
    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->setResult(I)V

    .line 2405830
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->finish()V

    .line 2405831
    return-void

    .line 2405832
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2405833
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2405834
    invoke-virtual {v1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2405835
    invoke-virtual {p0, v0, v1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1
.end method

.method private p()V
    .locals 3

    .prologue
    .line 2405746
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->z:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    invoke-virtual {v0}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2405747
    new-instance v0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;

    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "client_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;-><init>(Ljava/lang/String;)V

    .line 2405748
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2405749
    const-string v2, "app_info"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2405750
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2405751
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->z:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    const-string v2, "platform_get_app_permissions"

    invoke-virtual {v0, v2, v1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->start(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2405752
    :cond_0
    return-void
.end method

.method public static r(Lcom/facebook/katana/gdp/PlatformDialogActivity;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2405824
    const-string v0, "%s:%s:"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/katana/gdp/PlatformDialogActivity;->s:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 2405825
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2405826
    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static t(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V
    .locals 5

    .prologue
    .line 2405821
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->v:Ljava/lang/String;

    invoke-static {v0}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2405822
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->r:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x380007

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->r(Lcom/facebook/katana/gdp/PlatformDialogActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2405823
    return-void
.end method

.method public static u(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V
    .locals 1

    .prologue
    .line 2405817
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2405818
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2405819
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0

    .line 2405820
    :catch_1
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Uh;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2405807
    const/16 v0, 0x4e6

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2405808
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/4uH;->a(Landroid/content/Context;Z)LX/4uH;

    move-result-object v0

    .line 2405809
    invoke-interface {v0, p0}, LX/4u1;->a(Landroid/app/Activity;)Landroid/content/ComponentName;

    move-result-object v0

    .line 2405810
    :goto_0
    move-object v0, v0

    .line 2405811
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2405812
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "com.facebook.katana"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.facebook.wakizashi"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2405813
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "calling_package_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2405814
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    .line 2405815
    goto :goto_1

    .line 2405816
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2405780
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b(Landroid/os/Bundle;)V

    .line 2405781
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v0}, LX/44G;->b(LX/0QB;)LX/44G;

    move-result-object v2

    check-cast v2, LX/44G;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v4

    check-cast v4, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/275;->b(LX/0QB;)LX/275;

    move-result-object v5

    check-cast v5, LX/275;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v2, v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->x:LX/44G;

    iput-object v3, v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->y:LX/0W9;

    iput-object v4, v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->r:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object v5, v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->A:LX/275;

    iput-object v0, v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->C:LX/0Uh;

    .line 2405782
    if-eqz p1, :cond_0

    .line 2405783
    const-string v0, "calling_package"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->t:Ljava/lang/String;

    .line 2405784
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2405785
    :goto_1
    return-void

    .line 2405786
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->C:LX/0Uh;

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->a(LX/0Uh;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->t:Ljava/lang/String;

    goto :goto_0

    .line 2405787
    :cond_1
    const v0, 0x7f030fd6

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->setContentView(I)V

    .line 2405788
    new-instance v0, LX/Gvb;

    invoke-direct {v0, p0}, LX/Gvb;-><init>(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u:Landroid/app/ProgressDialog;

    .line 2405789
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->n()Landroid/webkit/WebViewClient;

    move-result-object v0

    const/4 v2, 0x0

    .line 2405790
    const v1, 0x7f0d2636

    invoke-virtual {p0, v1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/webview/BasicWebView;

    iput-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->w:Lcom/facebook/webview/BasicWebView;

    .line 2405791
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->w:Lcom/facebook/webview/BasicWebView;

    invoke-virtual {v1, v2}, Lcom/facebook/webview/BasicWebView;->setVerticalScrollBarEnabled(Z)V

    .line 2405792
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->w:Lcom/facebook/webview/BasicWebView;

    invoke-virtual {v1, v2}, Lcom/facebook/webview/BasicWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 2405793
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->w:Lcom/facebook/webview/BasicWebView;

    invoke-virtual {v1, v0}, Lcom/facebook/webview/BasicWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2405794
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->w:Lcom/facebook/webview/BasicWebView;

    invoke-virtual {v1}, Lcom/facebook/webview/BasicWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 2405795
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 2405796
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->y:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v1

    .line 2405797
    sget-object v2, Lcom/facebook/katana/gdp/PlatformDialogActivity;->p:Ljava/lang/String;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/facebook/katana/gdp/PlatformDialogActivity;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2405798
    :cond_2
    sput-object v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->p:Ljava/lang/String;

    .line 2405799
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    const-string v2, "facebook.com"

    const-string v0, "locale="

    invoke-virtual {v1, v2, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405800
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->m()V

    .line 2405801
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->q:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 2405802
    const-string v0, "PlatformDialogActivity"

    const-string v1, "mUrl was not set in setupDialogURL(). Any class inheriting from PlatformDialogActivity MUST set mUrl in this method."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405803
    :cond_4
    const-string v0, "getAppPermission"

    invoke-static {p0, v0}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->z:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 2405804
    iget-object v0, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->z:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v1, LX/Gva;

    invoke-direct {v1, p0}, LX/Gva;-><init>(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    .line 2405805
    iput-object v1, v0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    .line 2405806
    goto/16 :goto_1
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2405778
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->a(Lcom/facebook/katana/gdp/PlatformDialogActivity;ZLandroid/os/Bundle;)V

    .line 2405779
    return-void
.end method

.method public abstract m()V
.end method

.method public n()Landroid/webkit/WebViewClient;
    .locals 1

    .prologue
    .line 2405777
    new-instance v0, LX/Gvc;

    invoke-direct {v0, p0}, LX/Gvc;-><init>(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2405772
    if-nez p2, :cond_0

    .line 2405773
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->finish()V

    .line 2405774
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 2405775
    :goto_0
    return-void

    .line 2405776
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->p()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x8a2
        :pswitch_0
    .end packed-switch
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7dfbb4fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2405768
    invoke-super {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onPause()V

    .line 2405769
    invoke-static {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    .line 2405770
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->B:Z

    .line 2405771
    const/16 v1, 0x23

    const v2, -0x2f60af0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x59954368

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2405756
    invoke-super {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onResume()V

    .line 2405757
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2405758
    const/16 v1, 0x23

    const v2, 0x15e0b447

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2405759
    :goto_0
    return-void

    .line 2405760
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->B:Z

    .line 2405761
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v1

    .line 2405762
    if-eqz v1, :cond_1

    .line 2405763
    iget-object v2, v1, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v1, v2

    .line 2405764
    sget-object v2, LX/2A1;->STATUS_LOGGED_IN:LX/2A1;

    if-eq v1, v2, :cond_2

    .line 2405765
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->A:LX/275;

    invoke-virtual {v1, p0}, LX/275;->b(Landroid/app/Activity;)V

    .line 2405766
    :goto_1
    const v1, -0x5a5ca67c

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0

    .line 2405767
    :cond_2
    invoke-direct {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->p()V

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2405753
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2405754
    const-string v0, "calling_package"

    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405755
    return-void
.end method
