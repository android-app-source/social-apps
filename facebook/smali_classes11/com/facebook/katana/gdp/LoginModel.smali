.class public Lcom/facebook/katana/gdp/LoginModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/gdp/LoginModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/gdp/PermissionItem;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2405447
    new-instance v0, LX/GvS;

    invoke-direct {v0}, LX/GvS;-><init>()V

    sput-object v0, Lcom/facebook/katana/gdp/LoginModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2405441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2405442
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->a:Ljava/util/List;

    .line 2405443
    sget-object v0, Lcom/facebook/katana/gdp/PermissionItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->a:Ljava/util/List;

    .line 2405444
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->b:Ljava/lang/String;

    .line 2405445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->c:Ljava/lang/String;

    .line 2405446
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/gdp/PermissionItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2405429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2405430
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->a:Ljava/util/List;

    .line 2405431
    iput-object p1, p0, Lcom/facebook/katana/gdp/LoginModel;->b:Ljava/lang/String;

    .line 2405432
    iput-object p2, p0, Lcom/facebook/katana/gdp/LoginModel;->c:Ljava/lang/String;

    .line 2405433
    iget-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2405434
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/gdp/PermissionItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2405440
    iget-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2405439
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2405435
    iget-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2405436
    iget-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2405437
    iget-object v0, p0, Lcom/facebook/katana/gdp/LoginModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2405438
    return-void
.end method
