.class public Lcom/facebook/katana/gdp/ProxyAuthDialog;
.super Lcom/facebook/katana/gdp/PlatformDialogActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field private s:LX/03V;

.field private t:Ljava/lang/String;

.field private u:LX/0Uh;

.field public v:LX/0s6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2405980
    invoke-direct {p0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;-><init>()V

    .line 2405981
    return-void
.end method

.method private a(LX/03V;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2405977
    iput-object p1, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    .line 2405978
    iput-object p2, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->u:LX/0Uh;

    .line 2405979
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->a(LX/03V;LX/0Uh;)V

    invoke-static {v2}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v0

    check-cast v0, LX/0s6;

    iput-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->v:LX/0s6;

    return-void
.end method

.method private c(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 4

    .prologue
    const/16 v3, 0x40

    .line 2405972
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->u:LX/0Uh;

    const/16 v1, 0x4e6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2405973
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/4uH;->a(Landroid/content/Context;Z)LX/4uH;

    move-result-object v0

    .line 2405974
    invoke-interface {v0, p1, v3}, LX/4u1;->a(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 2405975
    :goto_0
    return-object v0

    .line 2405976
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->v:LX/0s6;

    invoke-virtual {v0, p1, v3}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    goto :goto_0
.end method

.method private p()[B
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2405982
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->u:LX/0Uh;

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->a(LX/0Uh;)Ljava/lang/String;

    move-result-object v1

    .line 2405983
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2405984
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "platform_login_get_calling_package"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2405985
    const-string v4, "calling_package_name"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2405986
    const-string v4, "client_id"

    iget-object v5, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->t:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2405987
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2405988
    iget-object v1, p0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->t:Ljava/lang/String;

    move-object v1, v1

    .line 2405989
    const-string v4, "saved_calling_package_name"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2405990
    :cond_0
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2405991
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    const-string v1, "ProxyAuthDialog"

    const-string v3, "Calling package and saved calling package are both null"

    invoke-virtual {v0, v1, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2405992
    :goto_0
    return-object v0

    .line 2405993
    :cond_1
    sget-object v4, LX/007;->g:Ljava/lang/String;

    move-object v4, v4

    .line 2405994
    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2405995
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "user_code"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2405996
    const-string v5, "device_request_user_code"

    invoke-virtual {v3, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2405997
    :cond_2
    invoke-interface {v0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2405998
    :try_start_0
    invoke-direct {p0, v1}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->c(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2405999
    :try_start_1
    const-string v1, "SHA-1"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2406000
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 2406001
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    goto :goto_0

    .line 2406002
    :catch_0
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    const-string v3, "ProxyAuthDialog-sig"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to read calling package\'s signature:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2406003
    goto :goto_0

    .line 2406004
    :catch_1
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    const-string v1, "ProxyAuthDialog-alg"

    const-string v3, "Failed to instantiate SHA-1 algorithm. It is evidently missing from this system."

    invoke-virtual {v0, v1, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2406005
    goto :goto_0
.end method

.method private q()LX/Gvc;
    .locals 1

    .prologue
    .line 2405971
    new-instance v0, LX/Gvh;

    invoke-direct {v0, p0}, LX/Gvh;-><init>(Lcom/facebook/katana/gdp/ProxyAuthDialog;)V

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2405954
    invoke-static {p0, p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2405955
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 2405956
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2405957
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2405958
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "client_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->t:Ljava/lang/String;

    .line 2405959
    iget-object v1, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    const-string v2, "client_id"

    iget-object v3, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->t:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405960
    :cond_0
    if-nez v0, :cond_3

    .line 2405961
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    const-string v1, "sso"

    const-string v2, "getCallingPackage==null; finish() called. see t1118578"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405962
    const v0, 0x7f080105

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 2405963
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->finish()V

    .line 2405964
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->b(Landroid/os/Bundle;)V

    .line 2405965
    return-void

    .line 2405966
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2405967
    :cond_3
    invoke-direct {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->p()[B

    move-result-object v0

    if-nez v0, :cond_1

    .line 2405968
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    const-string v1, "sso"

    const-string v2, "getCallingPackageSigHash == null; finish() called. see t7100098"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405969
    const v0, 0x7f080106

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 2405970
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->finish()V

    goto :goto_1
.end method

.method public final m()V
    .locals 6

    .prologue
    .line 2405929
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2405930
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 2405931
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2405932
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 2405933
    instance-of v5, v1, Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 2405934
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2405935
    :cond_1
    invoke-direct {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->p()[B

    move-result-object v0

    .line 2405936
    if-nez v0, :cond_2

    .line 2405937
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->s:LX/03V;

    const-string v1, "sso"

    const-string v2, "getCallingPackageSigHash == null; finish() called. see t7100098"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405938
    const v0, 0x7f080106

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 2405939
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->finish()V

    .line 2405940
    :goto_1
    return-void

    .line 2405941
    :cond_2
    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 2405942
    const-string v1, "type"

    const-string v3, "user_agent"

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405943
    const-string v1, "redirect_uri"

    const-string v3, "fbconnect://success"

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405944
    const-string v1, "display"

    const-string v3, "touch"

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405945
    const-string v1, "android_key"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405946
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2405947
    const-string v1, "https://m.%s/dialog/oauth"

    invoke-static {p0, v1}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2405948
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2405949
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .line 2405950
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2405951
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2405952
    :cond_3
    invoke-static {v3}, LX/FC5;->a(Ljava/util/Map;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object v1, v1

    .line 2405953
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthDialog;->q:Ljava/lang/String;

    goto :goto_1
.end method

.method public final synthetic n()Landroid/webkit/WebViewClient;
    .locals 1

    .prologue
    .line 2405928
    invoke-direct {p0}, Lcom/facebook/katana/gdp/ProxyAuthDialog;->q()LX/Gvc;

    move-result-object v0

    return-object v0
.end method
