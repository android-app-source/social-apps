.class public Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/GvI;


# instance fields
.field public a:LX/GvU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2405319
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2405320
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2405321
    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->c:LX/0Ot;

    .line 2405322
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;

    invoke-static {v3}, LX/GvU;->a(LX/0QB;)LX/GvU;

    move-result-object v1

    check-cast v1, LX/GvU;

    invoke-static {v3}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    const/16 p0, 0xdf4

    invoke-static {v3, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    iput-object v1, p1, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    iput-object v2, p1, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->b:LX/0if;

    iput-object v3, p1, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->c:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 0

    .prologue
    .line 2405318
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2405310
    iget-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->e:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    .line 2405311
    iget-boolean v3, v0, LX/GvU;->g:Z

    move v0, v3

    .line 2405312
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2405313
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->d:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    .line 2405314
    iget-boolean v3, v2, LX/GvU;->g:Z

    move v2, v3

    .line 2405315
    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2405316
    return-void

    .line 2405317
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final lU_()V
    .locals 0

    .prologue
    .line 2405309
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v0, 0x2a

    const v1, -0x547be55f

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2405276
    const-class v0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;

    invoke-static {v0, p0}, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2405277
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    invoke-virtual {v0, p0}, LX/GvU;->a(LX/GvI;)V

    .line 2405278
    const v0, 0x7f030793

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2405279
    const v0, 0x7f0d143c

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 2405280
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2405281
    invoke-static {v1}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2405282
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2405283
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v5, 0x7f0a08d2

    invoke-static {v2, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v1, v2}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2405284
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 2405285
    new-instance v1, LX/GvJ;

    invoke-direct {v1, p0}, LX/GvJ;-><init>(Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2405286
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a08d2

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 2405287
    const v0, 0x7f0d1257

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->e:Landroid/widget/ProgressBar;

    .line 2405288
    const v0, 0x7f0d143e

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->d:Landroid/widget/Button;

    .line 2405289
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f083601

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    .line 2405290
    iget-object v6, v0, LX/GvU;->a:LX/0Or;

    move-object v0, v6

    .line 2405291
    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v1, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2405292
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->d:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2405293
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->d:Landroid/widget/Button;

    new-instance v1, LX/GvK;

    invoke-direct {v1, p0}, LX/GvK;-><init>(Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2405294
    const v0, 0x7f0d143f

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2405295
    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2405296
    const/4 v2, 0x0

    .line 2405297
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    sget-wide v6, LX/0X5;->dM:J

    invoke-interface {v1, v6, v7, v8}, LX/0W4;->a(JZ)Z

    move-result v1

    .line 2405298
    if-eqz v1, :cond_1

    .line 2405299
    const v1, 0x7f030792

    invoke-virtual {p1, v1, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2405300
    const v1, 0x7f0d090e

    invoke-static {v2, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2405301
    const v5, 0x7f083609

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    invoke-virtual {v7}, LX/GvU;->e()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, v2

    .line 2405302
    :goto_0
    new-instance v2, LX/GvP;

    iget-object v5, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->b:LX/0if;

    iget-object v6, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    invoke-direct {v2, v5, v6, v1}, LX/GvP;-><init>(LX/0if;LX/GvU;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2405303
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a08d1

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    .line 2405304
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b22ed

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2405305
    new-instance v5, LX/62X;

    invoke-direct {v5, v1, v2}, LX/62X;-><init>(II)V

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2405306
    if-nez p3, :cond_0

    .line 2405307
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->b:LX/0if;

    sget-object v1, LX/0ig;->aQ:LX/0ih;

    const-string v2, "show_permissions"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2405308
    :cond_0
    const/16 v0, 0x2b

    const v1, 0x776223d2

    invoke-static {v10, v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v4

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3f3f956f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2405271
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->a:LX/GvU;

    invoke-virtual {v1, p0}, LX/GvU;->b(LX/GvI;)V

    .line 2405272
    iput-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->d:Landroid/widget/Button;

    .line 2405273
    iput-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;->e:Landroid/widget/ProgressBar;

    .line 2405274
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2405275
    const/16 v1, 0x2b

    const v2, 0x5e6b15a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
