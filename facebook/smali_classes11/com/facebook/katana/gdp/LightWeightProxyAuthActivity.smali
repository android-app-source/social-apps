.class public Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/GvR;
.implements LX/GvI;


# instance fields
.field private p:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/GvU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/0s6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/GvX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private u:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private v:LX/67X;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2405376
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;LX/0Or;LX/GvU;LX/0s6;LX/GvX;LX/0if;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;",
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;",
            "LX/GvU;",
            "LX/0s6;",
            "LX/GvX;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2405394
    iput-object p1, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    iput-object p3, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->r:LX/0s6;

    iput-object p4, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->s:LX/GvX;

    iput-object p5, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->t:LX/0if;

    iput-object p6, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->u:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;

    const/16 v1, 0x1677

    invoke-static {v6, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v6}, LX/GvU;->a(LX/0QB;)LX/GvU;

    move-result-object v2

    check-cast v2, LX/GvU;

    invoke-static {v6}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v3

    check-cast v3, LX/0s6;

    invoke-static {v6}, LX/GvX;->a(LX/0QB;)LX/GvX;

    move-result-object v4

    check-cast v4, LX/GvX;

    invoke-static {v6}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v0 .. v6}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->a(Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;LX/0Or;LX/GvU;LX/0s6;LX/GvX;LX/0if;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/katana/gdp/PermissionItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2405385
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2405386
    if-nez p1, :cond_1

    .line 2405387
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->s:LX/GvX;

    sget-object v2, LX/GvY;->PUBLIC_PROFILE:LX/GvY;

    invoke-virtual {v0, v2}, LX/GvX;->a(LX/GvY;)Lcom/facebook/katana/gdp/PermissionItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2405388
    :cond_0
    return-object v1

    .line 2405389
    :cond_1
    const/16 v0, 0x2c

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    .line 2405390
    sget-object v2, LX/GvY;->PUBLIC_PROFILE:LX/GvY;

    invoke-virtual {v2}, LX/GvY;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2405391
    const/4 v2, 0x0

    sget-object v3, LX/GvY;->PUBLIC_PROFILE:LX/GvY;

    invoke-virtual {v3}, LX/GvY;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2405392
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2405393
    iget-object v3, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->s:LX/GvX;

    invoke-static {v0}, LX/GvY;->fromName(Ljava/lang/String;)LX/GvY;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/GvX;->a(LX/GvY;)Lcom/facebook/katana/gdp/PermissionItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2405377
    new-instance v0, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;

    invoke-direct {v0}, Lcom/facebook/katana/gdp/LightWeightGdpPermissionsListFragment;-><init>()V

    move-object v0, v0

    .line 2405378
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2405379
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2405380
    const v2, 0x7f040050

    const v3, 0x7f040051

    const v4, 0x7f040050

    const v5, 0x7f040051

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0hH;->a(IIII)LX/0hH;

    .line 2405381
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 2405382
    const v2, 0x7f0d0553

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2405383
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2405384
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2405370
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2405371
    invoke-static {p0, p0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2405372
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->v:LX/67X;

    if-nez v0, :cond_0

    .line 2405373
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67X;

    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->v:LX/67X;

    .line 2405374
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->v:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2405375
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2405359
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    .line 2405360
    iget-object v1, v0, LX/GvU;->f:LX/3rL;

    move-object v1, v1

    .line 2405361
    iget-object v0, v1, LX/3rL;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 2405362
    iget-object v0, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->setResult(I)V

    .line 2405363
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 2405364
    invoke-virtual {p0}, Landroid/app/Activity;->finishAfterTransition()V

    .line 2405365
    :goto_1
    return-void

    .line 2405366
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2405367
    iget-object v0, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2405368
    iget-object v0, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 2405369
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2405395
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2405396
    const v0, 0x7f030790

    invoke-virtual {p0, v0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->setContentView(I)V

    .line 2405397
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2405398
    const-string v2, "client_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2405399
    const-string v3, "scope"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2405400
    const-string v4, "calling_package_key"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2405401
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2405402
    iget-object v5, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->r:LX/0s6;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, LX/01H;->f(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 2405403
    if-eqz v5, :cond_0

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;

    .line 2405404
    if-nez v0, :cond_1

    .line 2405405
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2405406
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get application info for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v1, v2}, LX/Gvi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2405407
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->setResult(ILandroid/content/Intent;)V

    .line 2405408
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    .line 2405409
    invoke-virtual {p0}, Landroid/app/Activity;->finishAfterTransition()V

    .line 2405410
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 2405411
    goto :goto_0

    .line 2405412
    :cond_1
    invoke-direct {p0, v3}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2405413
    iget-object v3, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    new-instance v4, Lcom/facebook/katana/gdp/LoginModel;

    invoke-direct {v4, v2, v0, v1}, Lcom/facebook/katana/gdp/LoginModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 2405414
    iput-object v4, v3, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    .line 2405415
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    .line 2405416
    iput-object p0, v0, LX/GvU;->i:LX/GvR;

    .line 2405417
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    invoke-virtual {v0, p0}, LX/GvU;->a(LX/GvI;)V

    .line 2405418
    new-instance v0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;

    invoke-direct {v0}, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;-><init>()V

    .line 2405419
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2405420
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2405421
    const v2, 0x7f0d1435

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2405422
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2405423
    const v0, 0x7f0d1434

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2405424
    new-instance v1, LX/GvQ;

    invoke-direct {v1, p0}, LX/GvQ;-><init>(Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2405425
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2405331
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2405332
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->onBackPressed()V

    .line 2405333
    return-void
.end method

.method public final lU_()V
    .locals 0

    .prologue
    .line 2405358
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 2405334
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2405335
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/katana/gdp/WebViewProxyAuth;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2405336
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2405337
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2b

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2405338
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2405339
    const/16 v0, 0x2b

    if-ne p1, v0, :cond_0

    .line 2405340
    invoke-virtual {p0, p2, p3}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->setResult(ILandroid/content/Intent;)V

    .line 2405341
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->finish()V

    .line 2405342
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 2405343
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->t:LX/0if;

    sget-object v2, LX/0ig;->aQ:LX/0ih;

    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    .line 2405344
    iget-boolean v3, v0, LX/GvU;->h:Z

    move v0, v3

    .line 2405345
    if-eqz v0, :cond_1

    const-string v0, "back_out_of_permission_list"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2405346
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    .line 2405347
    iget-boolean v1, v0, LX/GvU;->h:Z

    move v0, v1

    .line 2405348
    if-nez v0, :cond_0

    .line 2405349
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->t:LX/0if;

    sget-object v1, LX/0ig;->aQ:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2405350
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2405351
    return-void

    .line 2405352
    :cond_1
    const-string v0, "cancel_dialog"

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x539c046d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2405353
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2405354
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    invoke-virtual {v1, p0}, LX/GvU;->b(LX/GvI;)V

    .line 2405355
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->q:LX/GvU;

    const/4 v2, 0x0

    .line 2405356
    iput-object v2, v1, LX/GvU;->i:LX/GvR;

    .line 2405357
    const/16 v1, 0x23

    const v2, -0x23042e3e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
