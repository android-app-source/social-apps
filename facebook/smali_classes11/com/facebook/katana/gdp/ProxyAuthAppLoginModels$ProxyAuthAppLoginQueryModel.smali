.class public final Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x13e42065
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2405890
    const-class v0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2405889
    const-class v0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2405921
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2405922
    return-void
.end method

.method private l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2405919
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->f:Ljava/util/List;

    .line 2405920
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2405908
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2405909
    invoke-direct {p0}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v6

    .line 2405910
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2405911
    invoke-virtual {p0}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2405912
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2405913
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2405914
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2405915
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2405916
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2405917
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2405918
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2405906
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2405907
    iget-wide v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->e:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2405903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2405904
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2405905
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2405900
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2405901
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->e:J

    .line 2405902
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2405897
    new-instance v0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;

    invoke-direct {v0}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;-><init>()V

    .line 2405898
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2405899
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2405896
    const v0, -0x5fa5b318

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2405895
    const v0, 0x540d2983

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2405893
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->g:Ljava/lang/String;

    .line 2405894
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2405891
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->h:Ljava/lang/String;

    .line 2405892
    iget-object v0, p0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method
