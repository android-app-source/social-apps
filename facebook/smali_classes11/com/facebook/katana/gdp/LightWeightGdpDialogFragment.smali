.class public Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/GvI;


# instance fields
.field public a:LX/8tu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/GvU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/widget/ProgressBar;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2405105
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2405106
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2405107
    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->d:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;

    invoke-static {v4}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v1

    check-cast v1, LX/8tu;

    invoke-static {v4}, LX/GvU;->a(LX/0QB;)LX/GvU;

    move-result-object v2

    check-cast v2, LX/GvU;

    invoke-static {v4}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    const/16 p0, 0xdf4

    invoke-static {v4, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    iput-object v1, p1, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->a:LX/8tu;

    iput-object v2, p1, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    iput-object v3, p1, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->c:LX/0if;

    iput-object v4, p1, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->d:LX/0Ot;

    return-void
.end method

.method private e()V
    .locals 12

    .prologue
    .line 2405108
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    invoke-virtual {v0}, LX/GvU;->d()Ljava/util/List;

    move-result-object v0

    .line 2405109
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    invoke-virtual {v1}, LX/GvU;->e()Ljava/lang/String;

    move-result-object v1

    .line 2405110
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/GvG;

    invoke-direct {v3, p0}, LX/GvG;-><init>(Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;)V

    .line 2405111
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2405112
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/katana/gdp/PermissionItem;

    .line 2405113
    iget-boolean v7, v4, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    move v7, v7

    .line 2405114
    if-eqz v7, :cond_0

    .line 2405115
    iget-object v7, v4, Lcom/facebook/katana/gdp/PermissionItem;->e:Ljava/lang/String;

    move-object v4, v7

    .line 2405116
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2405117
    :cond_1
    const v4, 0x7f08360e

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2405118
    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v11, 0x0

    .line 2405119
    const-string v4, ""

    .line 2405120
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    .line 2405121
    if-ne v8, v0, :cond_3

    .line 2405122
    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2405123
    :cond_2
    :goto_1
    move-object v4, v4

    .line 2405124
    const v5, 0x7f083602

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2405125
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2405126
    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2405127
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    .line 2405128
    const v6, 0x7f083603

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2405129
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2405130
    new-instance v6, LX/GvH;

    invoke-direct {v6, v3}, LX/GvH;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2405131
    move-object v0, v5

    .line 2405132
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2405133
    return-void

    .line 2405134
    :cond_3
    if-ne v8, v6, :cond_4

    .line 2405135
    const v4, 0x7f081d8b

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v2, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2405136
    :cond_4
    if-le v8, v6, :cond_2

    .line 2405137
    const v4, 0x7f081dd0

    new-array v7, v6, [Ljava/lang/Object;

    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v11

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v0

    invoke-virtual {v2, v4, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v7, v4

    move v4, v6

    .line 2405138
    :goto_2
    add-int/lit8 v9, v8, -0x1

    if-ge v4, v9, :cond_5

    .line 2405139
    const v9, 0x7f081dd0

    new-array v10, v6, [Ljava/lang/Object;

    aput-object v7, v10, v11

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v10, v0

    invoke-virtual {v2, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2405140
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2405141
    :cond_5
    const v4, 0x7f081d8b

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v7, v6, v11

    add-int/lit8 v7, v8, -0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v2, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1
.end method


# virtual methods
.method public final b()V
    .locals 0

    .prologue
    .line 2405142
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2405143
    iget-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->e:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    .line 2405144
    iget-boolean v3, v0, LX/GvU;->g:Z

    move v0, v3

    .line 2405145
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2405146
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->f:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    .line 2405147
    iget-boolean v3, v2, LX/GvU;->g:Z

    move v2, v3

    .line 2405148
    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2405149
    return-void

    .line 2405150
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final lU_()V
    .locals 0

    .prologue
    .line 2405151
    invoke-direct {p0}, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->e()V

    .line 2405152
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x3f7d0a5e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2405153
    const-class v0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2405154
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    invoke-virtual {v0, p0}, LX/GvU;->a(LX/GvI;)V

    .line 2405155
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030791

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 2405156
    const v0, 0x7f0d1257

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->e:Landroid/widget/ProgressBar;

    .line 2405157
    const v0, 0x7f0d1437

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2405158
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    sget-wide v4, LX/0X5;->dN:J

    invoke-interface {v1, v4, v5, v6}, LX/0W4;->a(JZ)Z

    move-result v1

    .line 2405159
    if-eqz v1, :cond_1

    .line 2405160
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    .line 2405161
    iget-object v4, v1, LX/GvU;->a:LX/0Or;

    move-object v1, v4

    .line 2405162
    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b22ec

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    .line 2405163
    iget-object v1, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-class v4, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2405164
    invoke-virtual {v0, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2405165
    :goto_0
    const v0, 0x7f0d1438

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2405166
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    .line 2405167
    iget-object v4, v1, LX/GvU;->a:LX/0Or;

    move-object v1, v4

    .line 2405168
    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2405169
    const v0, 0x7f0d143a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->f:Landroid/widget/Button;

    .line 2405170
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f083601

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    .line 2405171
    iget-object p1, v0, LX/GvU;->a:LX/0Or;

    move-object v0, p1

    .line 2405172
    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2405173
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->f:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2405174
    const v0, 0x7f0d1439

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->g:Landroid/widget/TextView;

    .line 2405175
    invoke-direct {p0}, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->e()V

    .line 2405176
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->a:LX/8tu;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2405177
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->f:Landroid/widget/Button;

    new-instance v1, LX/GvF;

    invoke-direct {v1, p0}, LX/GvF;-><init>(Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2405178
    if-nez p3, :cond_0

    .line 2405179
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->c:LX/0if;

    sget-object v1, LX/0ig;->aQ:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2405180
    iget-object v0, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->c:LX/0if;

    sget-object v1, LX/0ig;->aQ:LX/0ih;

    const-string v4, "show_lightweight_login_dialog"

    invoke-virtual {v0, v1, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2405181
    :cond_0
    const v0, -0x58f1fa4f

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v3

    .line 2405182
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0xc6b8fcc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2405183
    iget-object v1, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->b:LX/GvU;

    invoke-virtual {v1, p0}, LX/GvU;->b(LX/GvI;)V

    .line 2405184
    iput-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->f:Landroid/widget/Button;

    .line 2405185
    iput-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->e:Landroid/widget/ProgressBar;

    .line 2405186
    iput-object v2, p0, Lcom/facebook/katana/gdp/LightWeightGdpDialogFragment;->g:Landroid/widget/TextView;

    .line 2405187
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2405188
    const/16 v1, 0x2b

    const v2, 0x41c2c89a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
