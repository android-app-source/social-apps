.class public Lcom/facebook/katana/gdp/PermissionItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/gdp/PermissionItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:LX/GvY;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2405599
    new-instance v0, LX/GvV;

    invoke-direct {v0}, LX/GvV;-><init>()V

    sput-object v0, Lcom/facebook/katana/gdp/PermissionItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2405585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2405586
    iput-boolean v2, p0, Lcom/facebook/katana/gdp/PermissionItem;->a:Z

    .line 2405587
    iput-boolean v1, p0, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    .line 2405588
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->a:Z

    .line 2405589
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2405590
    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->b:LX/GvY;

    .line 2405591
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->c:Ljava/lang/String;

    .line 2405592
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->d:Ljava/lang/String;

    .line 2405593
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->e:Ljava/lang/String;

    .line 2405594
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    .line 2405595
    return-void

    :cond_0
    move v0, v2

    .line 2405596
    goto :goto_0

    .line 2405597
    :cond_1
    invoke-static {}, LX/GvY;->values()[LX/GvY;

    move-result-object v3

    aget-object v0, v3, v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2405598
    goto :goto_2
.end method

.method public constructor <init>(ZLX/GvY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2405564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2405565
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->a:Z

    .line 2405566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    .line 2405567
    iput-boolean p1, p0, Lcom/facebook/katana/gdp/PermissionItem;->a:Z

    .line 2405568
    iput-object p2, p0, Lcom/facebook/katana/gdp/PermissionItem;->b:LX/GvY;

    .line 2405569
    iput-object p3, p0, Lcom/facebook/katana/gdp/PermissionItem;->c:Ljava/lang/String;

    .line 2405570
    iput-object p4, p0, Lcom/facebook/katana/gdp/PermissionItem;->d:Ljava/lang/String;

    .line 2405571
    iput-object p5, p0, Lcom/facebook/katana/gdp/PermissionItem;->e:Ljava/lang/String;

    .line 2405572
    iput-boolean p6, p0, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    .line 2405573
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2405584
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2405574
    iget-boolean v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2405575
    iget-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->b:LX/GvY;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2405576
    iget-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2405577
    iget-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2405578
    iget-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2405579
    iget-boolean v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 2405580
    return-void

    :cond_0
    move v0, v2

    .line 2405581
    goto :goto_0

    .line 2405582
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/gdp/PermissionItem;->b:LX/GvY;

    invoke-virtual {v0}, LX/GvY;->ordinal()I

    move-result v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2405583
    goto :goto_2
.end method
