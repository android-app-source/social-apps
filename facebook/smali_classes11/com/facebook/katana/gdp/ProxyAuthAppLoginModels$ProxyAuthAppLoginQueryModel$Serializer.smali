.class public final Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2405886
    const-class v0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;

    new-instance v1, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2405887
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2405888
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 2405865
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2405866
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    .line 2405867
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2405868
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2405869
    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    .line 2405870
    const-string v4, "expiration_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2405871
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2405872
    :cond_0
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2405873
    if-eqz v2, :cond_1

    .line 2405874
    const-string v2, "granted_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2405875
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2405876
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2405877
    if-eqz v2, :cond_2

    .line 2405878
    const-string v3, "signed_request"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2405879
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2405880
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2405881
    if-eqz v2, :cond_3

    .line 2405882
    const-string v3, "token"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2405883
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2405884
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2405885
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2405864
    check-cast p1, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel$Serializer;->a(Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
