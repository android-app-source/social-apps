.class public abstract Lcom/facebook/katana/fragment/BaseFacebookFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fg;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2404047
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static g(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2404069
    const-string v0, "dialogFragment:tag:%s"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/support/v4/app/DialogFragment;
    .locals 1

    .prologue
    .line 2404068
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/2A2;
    .locals 2

    .prologue
    .line 2404064
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2404065
    instance-of v1, v0, LX/2A2;

    if-eqz v1, :cond_0

    .line 2404066
    check-cast v0, LX/2A2;

    .line 2404067
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2404059
    iget-object v0, p0, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2404060
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->d()LX/2A2;

    move-result-object v0

    .line 2404061
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/2A2;->b()LX/0kX;

    move-result-object v0

    invoke-virtual {v0}, LX/0kX;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a:Ljava/lang/String;

    .line 2404062
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a:Ljava/lang/String;

    return-object v0

    .line 2404063
    :cond_1
    const-string v0, "NONE_FACEBOOK_ACTIVITY"

    goto :goto_0
.end method

.method public final e(I)V
    .locals 4

    .prologue
    .line 2404048
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2404049
    if-nez v0, :cond_0

    .line 2404050
    :goto_0
    return-void

    .line 2404051
    :cond_0
    invoke-static {p1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->g(I)Ljava/lang/String;

    move-result-object v1

    .line 2404052
    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2404053
    instance-of v2, v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_1

    .line 2404054
    check-cast v0, Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    goto :goto_0

    .line 2404055
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expect a DialogFragment for tag: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2404056
    invoke-static {}, LX/0sL;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2404057
    invoke-static {v1, v0}, Ljunit/framework/Assert;->assertNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2404058
    :cond_2
    goto :goto_0
.end method
