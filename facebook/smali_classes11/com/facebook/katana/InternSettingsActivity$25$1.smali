.class public final Lcom/facebook/katana/InternSettingsActivity$25$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2Gb;

.field public final synthetic c:LX/Gt4;


# direct methods
.method public constructor <init>(LX/Gt4;Ljava/lang/String;LX/2Gb;)V
    .locals 0

    .prologue
    .line 2400402
    iput-object p1, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->c:LX/Gt4;

    iput-object p2, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->b:LX/2Gb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 2400403
    const-string v0, "clear"

    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2400404
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->c:LX/Gt4;

    iget-object v0, v0, LX/Gt4;->c:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->h()V

    .line 2400405
    :cond_0
    :goto_0
    return-void

    .line 2400406
    :cond_1
    const-string v0, "register"

    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2400407
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->b:LX/2Gb;

    invoke-interface {v0}, LX/2Gb;->c()V

    goto :goto_0

    .line 2400408
    :cond_2
    const-string v0, "ensure"

    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2400409
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->b:LX/2Gb;

    invoke-interface {v0}, LX/2Gb;->d()V

    goto :goto_0

    .line 2400410
    :cond_3
    const-string v0, "unregister"

    iget-object v1, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2400411
    iget-object v0, p0, Lcom/facebook/katana/InternSettingsActivity$25$1;->b:LX/2Gb;

    invoke-interface {v0}, LX/2Gb;->e()V

    goto :goto_0
.end method
