.class public Lcom/facebook/katana/UriAuthHandler;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field public p:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2402007
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/katana/UriAuthHandler;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v0, p0, Lcom/facebook/katana/UriAuthHandler;->p:LX/0Zb;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2402008
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2402009
    invoke-static {p0, p0}, Lcom/facebook/katana/UriAuthHandler;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2402010
    invoke-virtual {p0}, Lcom/facebook/katana/UriAuthHandler;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2402011
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2402012
    if-nez v0, :cond_0

    move-object v0, v2

    .line 2402013
    :goto_0
    iget-object v3, p0, Lcom/facebook/katana/UriAuthHandler;->p:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "uri_auth_handler_activity_created"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "extras"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v3, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2402014
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 2402015
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v5

    .line 2402016
    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 2402017
    invoke-static {v5}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    .line 2402018
    const-string v3, "landing_page"

    invoke-virtual {v4, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2402019
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 2402020
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, v6, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v3, v2

    .line 2402021
    :goto_1
    const-string v2, "contactpoint"

    invoke-virtual {v4, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2402022
    invoke-static {v5}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v2

    check-cast v2, LX/0WJ;

    .line 2402023
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2402024
    invoke-virtual {v2}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v2

    .line 2402025
    if-eqz v2, :cond_2

    .line 2402026
    iget-object v7, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v7, v7

    .line 2402027
    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2402028
    invoke-static {v5}, LX/2gK;->b(LX/0QB;)LX/2gK;

    move-result-object v1

    check-cast v1, LX/2gK;

    .line 2402029
    if-eqz v3, :cond_1

    .line 2402030
    :goto_2
    invoke-interface {v0, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2402031
    invoke-virtual {p0}, Lcom/facebook/katana/UriAuthHandler;->finish()V

    .line 2402032
    :goto_3
    return-void

    .line 2402033
    :cond_0
    invoke-virtual {v0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2402034
    :cond_1
    invoke-virtual {v1}, LX/2gK;->a()Landroid/content/Intent;

    move-result-object v3

    goto :goto_2

    .line 2402035
    :cond_2
    if-eqz v2, :cond_3

    .line 2402036
    const/16 v0, 0x5db0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/UriAuthHandler;->showDialog(I)V

    goto :goto_3

    .line 2402037
    :cond_3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "username"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "reg_login_nonce"

    const-string v5, "nonce"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2402038
    if-eqz v3, :cond_4

    .line 2402039
    const-string v2, "calling_intent"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2402040
    :cond_4
    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2402041
    invoke-virtual {p0}, Lcom/facebook/katana/UriAuthHandler;->finish()V

    goto :goto_3

    :cond_5
    move-object v3, v2

    goto :goto_1
.end method

.method public final onCreateDialog(I)Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 2402042
    packed-switch p1, :pswitch_data_0

    .line 2402043
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2402044
    :pswitch_0
    const v0, 0x7f0800cd

    invoke-virtual {p0, v0}, Lcom/facebook/katana/UriAuthHandler;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x1080027

    const v0, 0x7f0800ce

    invoke-virtual {p0, v0}, Lcom/facebook/katana/UriAuthHandler;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f080036

    invoke-virtual {p0, v0}, Lcom/facebook/katana/UriAuthHandler;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Gtb;

    invoke-direct {v5, p0}, LX/Gtb;-><init>(Lcom/facebook/katana/UriAuthHandler;)V

    const/4 v9, 0x1

    move-object v0, p0

    move-object v7, v6

    move-object v8, v6

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5db0
        :pswitch_0
    .end packed-switch
.end method
