.class public Lcom/facebook/katana/RemoveRawContactsService;
.super LX/1ZN;
.source ""


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2401971
    const-string v0, "RemoveRawContactsService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2401972
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x35260d34    # -7141734.0f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2401973
    iget-object v1, p0, Lcom/facebook/katana/RemoveRawContactsService;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2401974
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    iget-object v3, p0, Lcom/facebook/katana/RemoveRawContactsService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.facebook.auth.login"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2401975
    invoke-virtual {p0}, Lcom/facebook/katana/RemoveRawContactsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2401976
    :cond_0
    const/16 v1, 0x25

    const v2, -0x4d7742e0

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 2401977
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 2401978
    if-eqz v0, :cond_0

    .line 2401979
    invoke-virtual {v0}, Lcom/facebook/katana/service/AppSession;->b()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/katana/model/FacebookSessionInfo;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/katana/RemoveRawContactsService;->a:Ljava/lang/String;

    .line 2401980
    :cond_0
    invoke-super {p0, p1, p2}, LX/1ZN;->onStart(Landroid/content/Intent;I)V

    .line 2401981
    return-void
.end method
