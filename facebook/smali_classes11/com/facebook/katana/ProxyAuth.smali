.class public Lcom/facebook/katana/ProxyAuth;
.super Landroid/app/Activity;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0So;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private g:J

.field private h:LX/4hi;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private i:LX/0Uq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private k:Z

.field private l:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2401969
    const-class v0, Lcom/facebook/katana/ProxyAuth;

    sput-object v0, Lcom/facebook/katana/ProxyAuth;->a:Ljava/lang/Class;

    .line 2401970
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/facebook/katana/ProxyAuth;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".child_act_launched"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/ProxyAuth;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2401961
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 2401962
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2401963
    iput-object v0, p0, Lcom/facebook/katana/ProxyAuth;->c:LX/0Ot;

    .line 2401964
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2401965
    iput-object v0, p0, Lcom/facebook/katana/ProxyAuth;->d:LX/0Ot;

    .line 2401966
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2401967
    iput-object v0, p0, Lcom/facebook/katana/ProxyAuth;->e:LX/0Ot;

    .line 2401968
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/katana/ProxyAuth;->g:J

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2401902
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 2401903
    invoke-virtual {p0}, Lcom/facebook/katana/ProxyAuth;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2401904
    invoke-virtual {p0}, Lcom/facebook/katana/ProxyAuth;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2401905
    :cond_0
    const-string v1, "scope"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2401906
    invoke-direct {p0, v1}, Lcom/facebook/katana/ProxyAuth;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2401907
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2401908
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2401909
    const-string v0, "platform_launch_time_ms"

    iget-wide v2, p0, Lcom/facebook/katana/ProxyAuth;->g:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2401910
    const-string v0, "calling_package_key"

    iget-object v2, p0, Lcom/facebook/katana/ProxyAuth;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2401911
    iget-object v0, p0, Lcom/facebook/katana/ProxyAuth;->j:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2a

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2401912
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/ProxyAuth;->k:Z

    .line 2401913
    return-void

    .line 2401914
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/katana/gdp/WebViewProxyAuth;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2401955
    if-nez p1, :cond_1

    .line 2401956
    invoke-virtual {p0}, Lcom/facebook/katana/ProxyAuth;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 2401957
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/katana/ProxyAuth;->l:Ljava/lang/String;

    .line 2401958
    :goto_1
    return-void

    .line 2401959
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2401960
    :cond_1
    const-string v0, "calling_package_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/ProxyAuth;->l:Ljava/lang/String;

    goto :goto_1
.end method

.method private static a(Lcom/facebook/katana/ProxyAuth;LX/0Ot;LX/0Ot;LX/0Ot;LX/0So;LX/4hi;LX/0Uq;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/ProxyAuth;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0So;",
            "LX/4hi;",
            "LX/0Uq;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2401954
    iput-object p1, p0, Lcom/facebook/katana/ProxyAuth;->c:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/katana/ProxyAuth;->d:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/katana/ProxyAuth;->e:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/katana/ProxyAuth;->f:LX/0So;

    iput-object p5, p0, Lcom/facebook/katana/ProxyAuth;->h:LX/4hi;

    iput-object p6, p0, Lcom/facebook/katana/ProxyAuth;->i:LX/0Uq;

    iput-object p7, p0, Lcom/facebook/katana/ProxyAuth;->j:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/ProxyAuth;

    const/16 v1, 0xac0

    invoke-static {v7, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xdf4

    invoke-static {v7, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x17d

    invoke-static {v7, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v7}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v7}, LX/4hi;->a(LX/0QB;)LX/4hi;

    move-result-object v5

    check-cast v5, LX/4hi;

    invoke-static {v7}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v6

    check-cast v6, LX/0Uq;

    invoke-static {v7}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v0 .. v7}, Lcom/facebook/katana/ProxyAuth;->a(Lcom/facebook/katana/ProxyAuth;LX/0Ot;LX/0Ot;LX/0Ot;LX/0So;LX/4hi;LX/0Uq;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2401943
    iget-object v0, p0, Lcom/facebook/katana/ProxyAuth;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x4c6

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2401944
    :goto_0
    return v0

    .line 2401945
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/ProxyAuth;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->dL:J

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 2401946
    if-nez v0, :cond_1

    move v0, v1

    .line 2401947
    goto :goto_0

    .line 2401948
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/ProxyAuth;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2401949
    goto :goto_0

    .line 2401950
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2401951
    if-eqz p1, :cond_3

    .line 2401952
    const/16 v0, 0x2c

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    .line 2401953
    :cond_3
    invoke-static {v0}, Lcom/facebook/katana/ProxyAuth;->a(Ljava/util/List;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2401939
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2401940
    invoke-static {v0}, Lcom/facebook/katana/ProxyAuth;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2401941
    const/4 v0, 0x0

    .line 2401942
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2401937
    if-nez p0, :cond_1

    .line 2401938
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "email"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "public_profile"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2401932
    const/16 v0, 0x2a

    if-ne p1, v0, :cond_0

    .line 2401933
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/ProxyAuth;->k:Z

    .line 2401934
    invoke-virtual {p0, p2, p3}, Lcom/facebook/katana/ProxyAuth;->setResult(ILandroid/content/Intent;)V

    .line 2401935
    invoke-virtual {p0}, Lcom/facebook/katana/ProxyAuth;->finish()V

    .line 2401936
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x5f4e49a7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2401915
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2401916
    invoke-static {p0, p0}, Lcom/facebook/katana/ProxyAuth;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2401917
    iget-object v1, p0, Lcom/facebook/katana/ProxyAuth;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/katana/ProxyAuth;->g:J

    .line 2401918
    iget-object v1, p0, Lcom/facebook/katana/ProxyAuth;->i:LX/0Uq;

    invoke-virtual {v1}, LX/0Uq;->b()V

    .line 2401919
    invoke-direct {p0, p1}, Lcom/facebook/katana/ProxyAuth;->a(Landroid/os/Bundle;)V

    .line 2401920
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/katana/ProxyAuth;->k:Z

    .line 2401921
    if-eqz p1, :cond_0

    .line 2401922
    sget-object v1, Lcom/facebook/katana/ProxyAuth;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/katana/ProxyAuth;->k:Z

    .line 2401923
    const-string v1, "platform_launch_time_ms"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/katana/ProxyAuth;->g:J

    .line 2401924
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/katana/ProxyAuth;->k:Z

    if-nez v1, :cond_2

    .line 2401925
    iget-object v1, p0, Lcom/facebook/katana/ProxyAuth;->h:LX/4hi;

    invoke-virtual {v1}, LX/4hi;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2401926
    sget-object v1, Lcom/facebook/katana/ProxyAuth;->a:Ljava/lang/Class;

    const-string v2, "Api requests exceed the rate limit"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2401927
    invoke-virtual {p0}, Lcom/facebook/katana/ProxyAuth;->finish()V

    .line 2401928
    const/16 v1, 0x23

    const v2, 0x3af34f7

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2401929
    :goto_0
    return-void

    .line 2401930
    :cond_1
    invoke-direct {p0}, Lcom/facebook/katana/ProxyAuth;->a()V

    .line 2401931
    :cond_2
    const v1, 0x14ce9611

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0
.end method
