.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2190193
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;

    new-instance v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2190194
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2190195
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2190196
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2190197
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2190198
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2190199
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2190200
    if-eqz v2, :cond_4

    .line 2190201
    const-string p0, "viewer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190202
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2190203
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2190204
    if-eqz p0, :cond_3

    .line 2190205
    const-string v0, "throwback"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190206
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2190207
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2190208
    if-eqz v0, :cond_2

    .line 2190209
    const-string v2, "throwback_settings"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190210
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2190211
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2190212
    if-eqz v2, :cond_0

    .line 2190213
    const-string p0, "subscription_confirmation_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190214
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2190215
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2190216
    if-eqz v2, :cond_1

    .line 2190217
    const-string p0, "subscription_status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190218
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2190219
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2190220
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2190221
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2190222
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2190223
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2190224
    check-cast p1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel$Serializer;->a(Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
