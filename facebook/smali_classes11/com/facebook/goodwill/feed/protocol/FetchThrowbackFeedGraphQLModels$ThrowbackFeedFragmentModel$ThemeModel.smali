.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x93dfb78
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189005
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189004
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2189002
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2189003
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2188988
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2188989
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2188990
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2188991
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2188992
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x2757227d

    invoke-static {v4, v3, v5}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2188993
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->m()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x399ec0fd

    invoke-static {v5, v4, v6}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2188994
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2188995
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2188996
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2188997
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2188998
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2188999
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2189000
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2189001
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2188972
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2188973
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2188974
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2757227d

    invoke-static {v2, v0, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2188975
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2188976
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    .line 2188977
    iput v3, v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->h:I

    move-object v1, v0

    .line 2188978
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2188979
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x399ec0fd

    invoke-static {v2, v0, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2188980
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2188981
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    .line 2188982
    iput v3, v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->i:I

    move-object v1, v0

    .line 2188983
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2188984
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2188985
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2188986
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2188987
    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2188969
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2188970
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2188971
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2188965
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2188966
    const/4 v0, 0x3

    const v1, -0x2757227d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->h:I

    .line 2188967
    const/4 v0, 0x4

    const v1, 0x399ec0fd

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->i:I

    .line 2188968
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2189006
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;-><init>()V

    .line 2189007
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2189008
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2188964
    const v0, -0x3e5ef64

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2188963
    const v0, 0x7cdd349c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2188961
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->f:Ljava/lang/String;

    .line 2188962
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2188959
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->g:Ljava/lang/String;

    .line 2188960
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubtitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2188957
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2188958
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2188955
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2188956
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
