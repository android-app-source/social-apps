.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2188800
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2188801
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2188802
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2188803
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2188804
    if-nez p1, :cond_0

    .line 2188805
    :goto_0
    return v0

    .line 2188806
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2188807
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2188808
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2188809
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2188810
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2188811
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2188812
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2188813
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2188814
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2188815
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2188816
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2188817
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2188818
    :sswitch_2
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2188819
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2188820
    const/4 v1, 0x2

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2188821
    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 2188822
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2188823
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2188824
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2188825
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2188826
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2188827
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2188828
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2188829
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2188830
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2188831
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2188832
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2188833
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2188834
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2188835
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2188836
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2188837
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2188838
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2188839
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2188840
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2188841
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2188842
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x536b3362 -> :sswitch_3
        -0x4e9d8163 -> :sswitch_2
        -0x2757227d -> :sswitch_0
        0x3a4965d -> :sswitch_6
        0x164942b3 -> :sswitch_5
        0x399ec0fd -> :sswitch_1
        0x4f2cfe79 -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2188843
    if-nez p0, :cond_0

    move v0, v1

    .line 2188844
    :goto_0
    return v0

    .line 2188845
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2188846
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2188847
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2188848
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2188849
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2188850
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2188851
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2188863
    const/4 v7, 0x0

    .line 2188864
    const/4 v1, 0x0

    .line 2188865
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2188866
    invoke-static {v2, v3, v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2188867
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2188868
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2188869
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2188852
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2188853
    if-eqz p0, :cond_0

    .line 2188854
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2188855
    if-eq v0, p0, :cond_0

    .line 2188856
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2188857
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2188858
    sparse-switch p2, :sswitch_data_0

    .line 2188859
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2188860
    :sswitch_0
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2188861
    invoke-static {v0, p3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2188862
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x536b3362 -> :sswitch_1
        -0x4e9d8163 -> :sswitch_0
        -0x2757227d -> :sswitch_1
        0x3a4965d -> :sswitch_1
        0x164942b3 -> :sswitch_1
        0x399ec0fd -> :sswitch_1
        0x4f2cfe79 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2188797
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2188798
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2188799
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2188792
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2188793
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2188794
    :cond_0
    iput-object p1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2188795
    iput p2, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->b:I

    .line 2188796
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2188791
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2188790
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2188787
    iget v0, p0, LX/1vt;->c:I

    .line 2188788
    move v0, v0

    .line 2188789
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2188784
    iget v0, p0, LX/1vt;->c:I

    .line 2188785
    move v0, v0

    .line 2188786
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2188781
    iget v0, p0, LX/1vt;->b:I

    .line 2188782
    move v0, v0

    .line 2188783
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2188778
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2188779
    move-object v0, v0

    .line 2188780
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2188769
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2188770
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2188771
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2188772
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2188773
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2188774
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2188775
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2188776
    invoke-static {v3, v9, v2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2188777
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2188766
    iget v0, p0, LX/1vt;->c:I

    .line 2188767
    move v0, v0

    .line 2188768
    return v0
.end method
