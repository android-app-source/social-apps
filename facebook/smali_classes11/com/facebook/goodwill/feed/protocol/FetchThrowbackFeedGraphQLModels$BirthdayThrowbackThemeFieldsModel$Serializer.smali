.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2188722
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel;

    new-instance v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2188723
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2188724
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2188709
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2188710
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2188711
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2188712
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2188713
    if-eqz v2, :cond_0

    .line 2188714
    const-string p0, "subtitle"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2188715
    invoke-static {v1, v2, p1}, LX/F0P;->a(LX/15i;ILX/0nX;)V

    .line 2188716
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2188717
    if-eqz v2, :cond_1

    .line 2188718
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2188719
    invoke-static {v1, v2, p1}, LX/F0Q;->a(LX/15i;ILX/0nX;)V

    .line 2188720
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2188721
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2188708
    check-cast p1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel$Serializer;->a(Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$BirthdayThrowbackThemeFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
