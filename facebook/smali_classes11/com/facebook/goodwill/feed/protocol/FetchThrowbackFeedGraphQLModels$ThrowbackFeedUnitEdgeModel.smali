.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x60b4addc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189272
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189273
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2189270
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2189271
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2189260
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2189261
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2189262
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2189263
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->k()Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2189264
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2189265
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2189266
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2189267
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2189268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2189269
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2189247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2189248
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2189249
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 2189250
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2189251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;

    .line 2189252
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->e:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 2189253
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->k()Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2189254
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->k()Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    .line 2189255
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->k()Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2189256
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;

    .line 2189257
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->g:Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    .line 2189258
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2189259
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189274
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->e:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->e:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 2189275
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->e:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2189244
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;-><init>()V

    .line 2189245
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2189246
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2189243
    const v0, 0x49d9daf9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2189242
    const v0, -0x28fc50f0

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189238
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->f:Ljava/lang/String;

    .line 2189239
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189240
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->g:Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->g:Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    .line 2189241
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->g:Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    return-object v0
.end method
