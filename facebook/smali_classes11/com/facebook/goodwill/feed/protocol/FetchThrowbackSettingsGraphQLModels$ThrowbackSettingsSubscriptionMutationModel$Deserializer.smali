.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2190191
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;

    new-instance v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2190192
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2190190
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2190122
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2190123
    const/4 v2, 0x0

    .line 2190124
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2190125
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190126
    :goto_0
    move v1, v2

    .line 2190127
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2190128
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2190129
    new-instance v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;

    invoke-direct {v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;-><init>()V

    .line 2190130
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2190131
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2190132
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2190133
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2190134
    :cond_0
    return-object v1

    .line 2190135
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190136
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2190137
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2190138
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2190139
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2190140
    const-string v4, "viewer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2190141
    const/4 v3, 0x0

    .line 2190142
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 2190143
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190144
    :goto_2
    move v1, v3

    .line 2190145
    goto :goto_1

    .line 2190146
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2190147
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2190148
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2190149
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190150
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2190151
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2190152
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2190153
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2190154
    const-string v5, "throwback"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2190155
    const/4 v4, 0x0

    .line 2190156
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 2190157
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190158
    :goto_4
    move v1, v4

    .line 2190159
    goto :goto_3

    .line 2190160
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2190161
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2190162
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 2190163
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190164
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_b

    .line 2190165
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2190166
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2190167
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_a

    if-eqz v5, :cond_a

    .line 2190168
    const-string v6, "throwback_settings"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2190169
    const/4 v5, 0x0

    .line 2190170
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_11

    .line 2190171
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190172
    :goto_6
    move v1, v5

    .line 2190173
    goto :goto_5

    .line 2190174
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2190175
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2190176
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_c
    move v1, v4

    goto :goto_5

    .line 2190177
    :cond_d
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2190178
    :cond_e
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_10

    .line 2190179
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2190180
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2190181
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_e

    if-eqz v7, :cond_e

    .line 2190182
    const-string p0, "subscription_confirmation_text"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    .line 2190183
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_7

    .line 2190184
    :cond_f
    const-string p0, "subscription_status"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2190185
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_7

    .line 2190186
    :cond_10
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2190187
    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 2190188
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2190189
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_6

    :cond_11
    move v1, v5

    move v6, v5

    goto :goto_7
.end method
