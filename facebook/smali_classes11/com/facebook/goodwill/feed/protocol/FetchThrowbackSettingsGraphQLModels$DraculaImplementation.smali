.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2190090
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2190091
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2190115
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2190116
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2190093
    if-nez p1, :cond_0

    .line 2190094
    :goto_0
    return v0

    .line 2190095
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2190096
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2190097
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2190098
    const v2, -0x62ccccec

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2190099
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2190100
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2190101
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2190102
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2190103
    const v2, -0x2db11404

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2190104
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2190105
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2190106
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2190107
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2190108
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2190109
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2190110
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2190111
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2190112
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2190113
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2190114
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x62ccccec -> :sswitch_1
        -0x2db11404 -> :sswitch_2
        0x3294a69d -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2190092
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2190048
    sparse-switch p2, :sswitch_data_0

    .line 2190049
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2190050
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2190051
    const v1, -0x62ccccec

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2190052
    :goto_0
    :sswitch_1
    return-void

    .line 2190053
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2190054
    const v1, -0x2db11404

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x62ccccec -> :sswitch_2
        -0x2db11404 -> :sswitch_1
        0x3294a69d -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2190084
    if-eqz p1, :cond_0

    .line 2190085
    invoke-static {p0, p1, p2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2190086
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;

    .line 2190087
    if-eq v0, v1, :cond_0

    .line 2190088
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2190089
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2190083
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2190081
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2190082
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2190117
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2190118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2190119
    :cond_0
    iput-object p1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2190120
    iput p2, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->b:I

    .line 2190121
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2190080
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2190079
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2190076
    iget v0, p0, LX/1vt;->c:I

    .line 2190077
    move v0, v0

    .line 2190078
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2190073
    iget v0, p0, LX/1vt;->c:I

    .line 2190074
    move v0, v0

    .line 2190075
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2190070
    iget v0, p0, LX/1vt;->b:I

    .line 2190071
    move v0, v0

    .line 2190072
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2190067
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2190068
    move-object v0, v0

    .line 2190069
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2190058
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2190059
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2190060
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2190061
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2190062
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2190063
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2190064
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2190065
    invoke-static {v3, v9, v2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2190066
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2190055
    iget v0, p0, LX/1vt;->c:I

    .line 2190056
    move v0, v0

    .line 2190057
    return v0
.end method
