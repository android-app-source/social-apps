.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd4c6cb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189512
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189511
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2189509
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2189510
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2189499
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2189500
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2189501
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x164942b3

    invoke-static {v2, v1, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2189502
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x3a4965d

    invoke-static {v3, v2, v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2189503
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2189504
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2189505
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2189506
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2189507
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2189508
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2189483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2189484
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2189485
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x164942b3

    invoke-static {v2, v0, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2189486
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2189487
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;

    .line 2189488
    iput v3, v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->f:I

    move-object v1, v0

    .line 2189489
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2189490
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3a4965d

    invoke-static {v2, v0, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2189491
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2189492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;

    .line 2189493
    iput v3, v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->g:I

    move-object v1, v0

    .line 2189494
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2189495
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2189496
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2189497
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2189498
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189468
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->e:Ljava/lang/String;

    .line 2189469
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2189479
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2189480
    const/4 v0, 0x1

    const v1, 0x164942b3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->f:I

    .line 2189481
    const/4 v0, 0x2

    const v1, 0x3a4965d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->g:I

    .line 2189482
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2189476
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;-><init>()V

    .line 2189477
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2189478
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2189475
    const v0, 0x748196f9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2189474
    const v0, -0x723dce59

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubtitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189472
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2189473
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189470
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2189471
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
