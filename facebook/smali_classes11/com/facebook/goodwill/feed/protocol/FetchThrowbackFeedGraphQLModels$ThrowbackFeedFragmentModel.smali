.class public final Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x58aaad11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189037
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2189038
    const-class v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2189039
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2189040
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 2189041
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2189042
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2189043
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2189044
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->k()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2189045
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2189046
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2189047
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    const v11, -0x536b3362

    invoke-static {v10, v9, v11}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2189048
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2189049
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->p()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2189050
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->q()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2189051
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2189052
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->s()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2189053
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->t()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2189054
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->u()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 2189055
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->v()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 2189056
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->w()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 2189057
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->x()LX/1vs;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, LX/1vs;->b:I

    move/from16 v19, v0

    const v21, 0x4f2cfe79

    move-object/from16 v0, v20

    move/from16 v1, v19

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 2189058
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->y()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 2189059
    const/16 v21, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2189060
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2189061
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 2189062
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 2189063
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 2189064
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 2189065
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 2189066
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 2189067
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 2189068
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 2189069
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 2189070
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 2189071
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 2189072
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2189073
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2189074
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2189075
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2189076
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2189077
    const/16 v5, 0x11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->v:J

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 2189078
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2189079
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2189080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2189081
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2189082
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    .line 2189083
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2189084
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189085
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->e:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    .line 2189086
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2189087
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189088
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2189089
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189090
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189091
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2189092
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189093
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2189094
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189095
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189096
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2189097
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189098
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2189099
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189100
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189101
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2189102
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x536b3362

    invoke-static {v2, v0, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2189103
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2189104
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189105
    iput v3, v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j:I

    move-object v1, v0

    .line 2189106
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2189107
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189108
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2189109
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189110
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189111
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2189112
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189113
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2189114
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189115
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189116
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->w()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2189117
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->w()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    .line 2189118
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->w()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 2189119
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189120
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->s:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    .line 2189121
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->x()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 2189122
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->x()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4f2cfe79

    invoke-static {v2, v0, v3}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2189123
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->x()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2189124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189125
    iput v3, v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->t:I

    move-object v1, v0

    .line 2189126
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->y()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2189127
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->y()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    .line 2189128
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->y()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 2189129
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    .line 2189130
    iput-object v0, v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->u:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    .line 2189131
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2189132
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    .line 2189133
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2189134
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_a
    move-object p0, v1

    .line 2189135
    goto :goto_0
.end method

.method public final a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAccentImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189136
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->e:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->e:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    .line 2189137
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->e:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2189149
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2189150
    const/4 v0, 0x5

    const v1, -0x536b3362

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j:I

    .line 2189151
    const/16 v0, 0xf

    const v1, 0x4f2cfe79

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->t:I

    .line 2189152
    const/16 v0, 0x11

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->v:J

    .line 2189153
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2189138
    new-instance v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;-><init>()V

    .line 2189139
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2189140
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2189141
    const v0, -0x114f594c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2189142
    const v0, -0x120c6bf6

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189143
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189144
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189145
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->g:Ljava/lang/String;

    .line 2189146
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189035
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189036
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189147
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189148
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHeaderText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189009
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2189010
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189013
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189014
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189011
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l:Ljava/lang/String;

    .line 2189012
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189015
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m:Ljava/lang/String;

    .line 2189016
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189017
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2189018
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189019
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o:Ljava/lang/String;

    .line 2189020
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189021
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->p:Ljava/lang/String;

    .line 2189022
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189023
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->q:Ljava/lang/String;

    .line 2189024
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189025
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r:Ljava/lang/String;

    .line 2189026
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTheme"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189027
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->s:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->s:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    .line 2189028
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->s:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    return-object v0
.end method

.method public final x()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThrowbackSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189029
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2189030
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->t:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final y()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThrowbackUnits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2189031
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->u:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->u:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    .line 2189032
    iget-object v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->u:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    return-object v0
.end method

.method public final z()J
    .locals 2

    .prologue
    .line 2189033
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2189034
    iget-wide v0, p0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->v:J

    return-wide v0
.end method
