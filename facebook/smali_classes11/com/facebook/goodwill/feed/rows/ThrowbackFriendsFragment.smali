.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/F0x;

.field private b:Lcom/facebook/widget/listview/BetterListView;

.field public c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

.field public d:LX/17W;

.field public e:LX/0wM;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2191052
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v1, p1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->d:LX/17W;

    iput-object p0, p1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->e:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2191049
    const-class v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;

    invoke-static {v0, p0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2191050
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2191051
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6f45535e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191048
    const v1, 0x7f0314aa

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5d15fd90

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2191034
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2191035
    const-string v0, "friend_list"

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2191036
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2191037
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2191038
    if-eqz p2, :cond_0

    const-string v0, "friend_list"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191039
    const-string v0, "friend_list"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    .line 2191040
    :cond_0
    const v0, 0x7f0d2eed    # 1.876648E38f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 2191041
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/F0y;

    invoke-direct {v1, p0}, LX/F0y;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2191042
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 2191043
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->a:LX/F0x;

    if-nez v1, :cond_1

    .line 2191044
    new-instance v1, LX/F0x;

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    iget-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->d:LX/17W;

    iget-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->e:LX/0wM;

    invoke-direct {v1, v2, p1, p2}, LX/F0x;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;LX/17W;LX/0wM;)V

    iput-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->a:LX/F0x;

    .line 2191045
    :cond_1
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->a:LX/F0x;

    move-object v1, v1

    .line 2191046
    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2191047
    return-void
.end method
