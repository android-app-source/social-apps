.class public Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

.field private final b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

.field private final c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191714
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2191715
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    .line 2191716
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    .line 2191717
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    .line 2191718
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;

    .line 2191719
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;
    .locals 7

    .prologue
    .line 2191703
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;

    monitor-enter v1

    .line 2191704
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191705
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191706
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191707
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191708
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;)V

    .line 2191709
    move-object v0, p0

    .line 2191710
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191711
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191712
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191713
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2191692
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191693
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191694
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2191695
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v1

    .line 2191696
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2191697
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    invoke-virtual {p1, v2, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191698
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    invoke-virtual {p1, v2, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191699
    invoke-static {v1}, LX/F0s;->a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2191700
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191701
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2191702
    :cond_0
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2191685
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191686
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191687
    if-eqz v0, :cond_0

    .line 2191688
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191689
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;

    .line 2191690
    iget-object p0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->c:Lcom/facebook/graphql/model/FeedUnit;

    move-object v0, p0

    .line 2191691
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
