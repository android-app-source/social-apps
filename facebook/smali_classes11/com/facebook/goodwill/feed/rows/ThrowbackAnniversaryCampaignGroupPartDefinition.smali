.class public Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190368
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2190369
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

    .line 2190370
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 2190371
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;

    .line 2190372
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;
    .locals 6

    .prologue
    .line 2190373
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;

    monitor-enter v1

    .line 2190374
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190375
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190376
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190377
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190378
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;)V

    .line 2190379
    move-object v0, p0

    .line 2190380
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190381
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190382
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190383
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2190384
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;

    .line 2190385
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190386
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 2190387
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v2

    .line 2190388
    iput-object v2, v1, LX/39x;->q:LX/0Px;

    .line 2190389
    move-object v1, v1

    .line 2190390
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2190391
    iput-object v2, v1, LX/39x;->p:LX/0Px;

    .line 2190392
    move-object v1, v1

    .line 2190393
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2190394
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 2190395
    if-eqz v1, :cond_0

    .line 2190396
    new-instance v3, LX/3dL;

    invoke-direct {v3}, LX/3dL;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p3

    .line 2190397
    iput-object p3, v3, LX/3dL;->E:Ljava/lang/String;

    .line 2190398
    move-object v3, v3

    .line 2190399
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object p3

    .line 2190400
    iput-object p3, v3, LX/3dL;->ag:Ljava/lang/String;

    .line 2190401
    move-object v3, v3

    .line 2190402
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object p3

    .line 2190403
    iput-object p3, v3, LX/3dL;->aK:Ljava/lang/String;

    .line 2190404
    move-object v3, v3

    .line 2190405
    new-instance p3, LX/2dc;

    invoke-direct {p3}, LX/2dc;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 2190406
    iput-object v1, p3, LX/2dc;->h:Ljava/lang/String;

    .line 2190407
    move-object v1, p3

    .line 2190408
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 2190409
    iput-object v1, v3, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2190410
    move-object v1, v3

    .line 2190411
    invoke-virtual {v1}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 2190412
    :goto_0
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->g()Ljava/lang/String;

    move-result-object p3

    .line 2190413
    iput-object p3, v3, LX/23u;->m:Ljava/lang/String;

    .line 2190414
    move-object v3, v3

    .line 2190415
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p3

    .line 2190416
    iput-object p3, v3, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2190417
    move-object v3, v3

    .line 2190418
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p3

    .line 2190419
    iput-object p3, v3, LX/23u;->k:LX/0Px;

    .line 2190420
    move-object v3, v3

    .line 2190421
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2190422
    iput-object v1, v3, LX/23u;->d:LX/0Px;

    .line 2190423
    move-object v1, v3

    .line 2190424
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2190425
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    move-object v1, v1

    .line 2190426
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190427
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190428
    const/4 v0, 0x0

    return-object v0

    .line 2190429
    :cond_0
    new-instance v1, LX/3dL;

    invoke-direct {v1}, LX/3dL;-><init>()V

    invoke-virtual {v1}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2190430
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;

    .line 2190431
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
