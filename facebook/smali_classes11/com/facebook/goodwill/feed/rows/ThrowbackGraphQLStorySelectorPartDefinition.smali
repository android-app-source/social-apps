.class public Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;

.field private final b:Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

.field private final d:Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191526
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2191527
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;

    .line 2191528
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;

    .line 2191529
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->c:Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    .line 2191530
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;

    .line 2191531
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;
    .locals 7

    .prologue
    .line 2191533
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;

    monitor-enter v1

    .line 2191534
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191535
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191536
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191537
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191538
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;-><init>(Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;)V

    .line 2191539
    move-object v0, p0

    .line 2191540
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191541
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191542
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191543
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2191544
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191545
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->c:Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackSharedStoryPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackGraphQLStorySelectorPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2191546
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2191532
    const/4 v0, 0x1

    return v0
.end method
