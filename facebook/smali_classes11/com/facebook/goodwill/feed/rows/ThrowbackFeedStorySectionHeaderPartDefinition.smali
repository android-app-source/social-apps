.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/F0p;",
        "LX/F0q;",
        "LX/1Ps;",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:LX/1Ua;

.field private static final c:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x41400000    # 12.0f

    .line 2190830
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    .line 2190831
    iput v1, v0, LX/1UY;->b:F

    .line 2190832
    move-object v0, v0

    .line 2190833
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->b:LX/1Ua;

    .line 2190834
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    .line 2190835
    iput v1, v0, LX/1UY;->b:F

    .line 2190836
    move-object v0, v0

    .line 2190837
    const/high16 v1, 0x41000000    # 8.0f

    .line 2190838
    iput v1, v0, LX/1UY;->c:F

    .line 2190839
    move-object v0, v0

    .line 2190840
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190827
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2190828
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2190829
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;
    .locals 4

    .prologue
    .line 2190865
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    monitor-enter v1

    .line 2190866
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190867
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190868
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190869
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190870
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2190871
    move-object v0, p0

    .line 2190872
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190873
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190874
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190875
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2190864
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2190876
    check-cast p2, LX/F0p;

    .line 2190877
    iget-object v0, p2, LX/F0p;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-object v1, v0

    .line 2190878
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->b:LX/1Ua;

    .line 2190879
    :goto_0
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    .line 2190880
    iget-object v4, p2, LX/F0p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v4

    .line 2190881
    invoke-direct {v3, v4, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2190882
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 2190883
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2190884
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2190885
    iget-boolean v3, p2, LX/F0p;->c:Z

    move v3, v3

    .line 2190886
    new-instance v4, LX/F0q;

    invoke-direct {v4, v2, v0, v1, v3}, LX/F0q;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V

    return-object v4

    .line 2190887
    :cond_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->c:LX/1Ua;

    goto :goto_0

    .line 2190888
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x22065b74

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2190844
    check-cast p2, LX/F0q;

    check-cast p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;

    .line 2190845
    iget-object v1, p2, LX/F0q;->a:Ljava/lang/String;

    .line 2190846
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2190847
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2190848
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2190849
    :goto_0
    iget-object v1, p2, LX/F0q;->b:Ljava/lang/String;

    .line 2190850
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2190851
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2190852
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->e:Lcom/facebook/resources/ui/FbTextView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2190853
    :goto_1
    iget-object v1, p2, LX/F0q;->c:Landroid/net/Uri;

    .line 2190854
    if-eqz v1, :cond_0

    .line 2190855
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2190856
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2190857
    :cond_0
    iget-boolean v1, p2, LX/F0q;->d:Z

    .line 2190858
    if-eqz v1, :cond_3

    .line 2190859
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->f:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 p0, 0x4

    invoke-virtual {v2, p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2190860
    :goto_2
    const/16 v1, 0x1f

    const v2, -0x374a8370    # -371684.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2190861
    :cond_1
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 2190862
    :cond_2
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->e:Lcom/facebook/resources/ui/FbTextView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 2190863
    :cond_3
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->f:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2190841
    check-cast p1, LX/F0p;

    .line 2190842
    iget-object v0, p1, LX/F0p;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-object v0, v0

    .line 2190843
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/F0p;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
