.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;

.field private final b:Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190786
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2190787
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;

    .line 2190788
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;

    .line 2190789
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;
    .locals 5

    .prologue
    .line 2190775
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;

    monitor-enter v1

    .line 2190776
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190777
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190778
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190779
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190780
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;)V

    .line 2190781
    move-object v0, p0

    .line 2190782
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190783
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190784
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2190772
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190773
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFriendversaryGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionSelectorPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2190774
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2190771
    const/4 v0, 0x1

    return v0
.end method
