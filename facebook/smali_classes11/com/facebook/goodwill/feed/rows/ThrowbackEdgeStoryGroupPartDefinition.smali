.class public Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

.field private final c:Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190744
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2190745
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    .line 2190746
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    .line 2190747
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;

    .line 2190748
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    .line 2190749
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;
    .locals 7

    .prologue
    .line 2190750
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;

    monitor-enter v1

    .line 2190751
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190752
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190753
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190754
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190755
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;)V

    .line 2190756
    move-object v0, p0

    .line 2190757
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190758
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190759
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2190761
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190762
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190763
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190764
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    .line 2190765
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2190766
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190767
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackEdgeStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190768
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2190769
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190770
    invoke-static {p1}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
