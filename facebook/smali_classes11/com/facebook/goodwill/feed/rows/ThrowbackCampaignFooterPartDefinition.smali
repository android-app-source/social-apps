.class public Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3ZK;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Landroid/content/Context;

.field public final e:LX/1Kf;

.field private final f:LX/03V;

.field public final g:LX/1Nq;

.field public final h:LX/Es5;

.field public final i:LX/1Cn;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/Context;LX/1Kf;LX/03V;LX/1Nq;LX/Es5;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190591
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2190592
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 2190593
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2190594
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2190595
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->d:Landroid/content/Context;

    .line 2190596
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->e:LX/1Kf;

    .line 2190597
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->f:LX/03V;

    .line 2190598
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->g:LX/1Nq;

    .line 2190599
    iput-object p8, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->h:LX/Es5;

    .line 2190600
    iput-object p9, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->i:LX/1Cn;

    .line 2190601
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;
    .locals 13

    .prologue
    .line 2190602
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    monitor-enter v1

    .line 2190603
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190604
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190605
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190606
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190607
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v8

    check-cast v8, LX/1Kf;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v10

    check-cast v10, LX/1Nq;

    invoke-static {v0}, LX/Es5;->a(LX/0QB;)LX/Es5;

    move-result-object v11

    check-cast v11, LX/Es5;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v12

    check-cast v12, LX/1Cn;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/Context;LX/1Kf;LX/03V;LX/1Nq;LX/Es5;LX/1Cn;)V

    .line 2190608
    move-object v0, v3

    .line 2190609
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190610
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190611
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190612
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)Ljava/lang/Void;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;TE;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 2190613
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2190614
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/F0s;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v10

    .line 2190615
    invoke-static {v10}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2190616
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v1, 0x7f080fd9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2190617
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/F0s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2190618
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/F0s;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v2

    .line 2190619
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/F0s;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v3

    .line 2190620
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iget-object v4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v5, 0x7f082a1f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v6, 0x7f082a22

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v7, 0x7f082a23

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f()I

    move-result v7

    const-string v8, "throwback_permalink"

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    invoke-static {v9}, LX/CFE;->a(Lcom/facebook/graphql/model/GraphQLUser;)LX/0Px;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 2190621
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2190622
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2190623
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2190624
    new-instance v5, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;-><init>(Lcom/facebook/graphql/model/GraphQLMedia;)V

    invoke-virtual {v0, v5}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 2190625
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2190626
    :cond_1
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2190627
    const/4 v0, 0x0

    .line 2190628
    :goto_1
    return-object v0

    .line 2190629
    :cond_2
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2190630
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2190631
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2190632
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {p0, v1, v0, p3}, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->a$redex0(Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2190633
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a$redex0(Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2190634
    invoke-static {p1}, LX/F0s;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v3

    .line 2190635
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2190636
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    .line 2190637
    :cond_0
    new-instance v0, LX/F0m;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LX/F0m;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v0, v6

    .line 2190638
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2190639
    sget-object v0, LX/3ZK;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2190640
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3c7a9fa9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2190641
    check-cast p4, LX/3ZK;

    .line 2190642
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/3ZK;->setHasBottomDivider(Z)V

    .line 2190643
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, LX/3ZK;->setHasButton(Z)V

    .line 2190644
    const/16 v1, 0x1f

    const v2, -0x77340724

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2190645
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2190646
    if-eqz p1, :cond_0

    .line 2190647
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2190648
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2190649
    :goto_0
    return v0

    .line 2190650
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2190651
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/F0s;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    .line 2190652
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 2190653
    goto :goto_0

    .line 2190654
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
