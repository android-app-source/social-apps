.class public Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "LX/F1J;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/F1J;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/PhotoLayoutPartDefinition;

.field private final e:LX/1qa;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:LX/1Ad;

.field public final h:LX/23R;

.field public final i:LX/9hF;

.field private final j:LX/C51;

.field private final k:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2191614
    new-instance v0, LX/F1K;

    invoke-direct {v0}, LX/F1K;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->a:LX/1Cz;

    .line 2191615
    const-class v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;Lcom/facebook/feedplugins/attachments/PhotoLayoutPartDefinition;LX/1qa;LX/C51;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1Ad;Landroid/content/res/Resources;LX/23R;LX/9hF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191603
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2191604
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->k:Landroid/content/res/Resources;

    .line 2191605
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->c:Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    .line 2191606
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->d:Lcom/facebook/feedplugins/attachments/PhotoLayoutPartDefinition;

    .line 2191607
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->e:LX/1qa;

    .line 2191608
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2191609
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->g:LX/1Ad;

    .line 2191610
    iput-object p8, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->h:LX/23R;

    .line 2191611
    iput-object p9, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->i:LX/9hF;

    .line 2191612
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->j:LX/C51;

    .line 2191613
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/F1J;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2191577
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2191584
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 2191585
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191586
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2191587
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2191588
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2191589
    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->k:Landroid/content/res/Resources;

    const v4, 0x7f0b20da

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2191590
    iget-object v4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->e:LX/1qa;

    invoke-virtual {v4, v0, v3, v1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2191591
    invoke-static {v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v0

    .line 2191592
    if-nez v0, :cond_0

    .line 2191593
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->g:LX/1Ad;

    sget-object v4, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2191594
    const v1, 0x7f0d2efc

    iget-object v4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->c:Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    new-instance v5, LX/Bye;

    invoke-direct {v5, v0}, LX/Bye;-><init>(LX/1aZ;)V

    invoke-interface {p1, v1, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2191595
    const v0, 0x7f0d2efc

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->d:Lcom/facebook/feedplugins/attachments/PhotoLayoutPartDefinition;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2191596
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/F1L;

    invoke-direct {v1, p0, v2}, LX/F1L;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191597
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->j:LX/C51;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    const-string v2, "throwbackPhotoAttachmentPage"

    const-string v3, "goodwill_throwback_permalink_ufi"

    invoke-virtual {v0, p2, v1, v2, v3}, LX/C51;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Ljava/lang/String;Ljava/lang/String;)LX/C50;

    move-result-object v0

    .line 2191598
    return-object v0

    .line 2191599
    :cond_0
    invoke-static {v0}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    .line 2191600
    iput-boolean v5, v0, LX/1bX;->g:Z

    .line 2191601
    move-object v0, v0

    .line 2191602
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x21fa6f07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191581
    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/F1J;

    .line 2191582
    invoke-virtual {p4, p2}, LX/F1J;->setShareOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2191583
    const/16 v1, 0x1f

    const v2, 0x2ab1238

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2191578
    check-cast p4, LX/F1J;

    .line 2191579
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/F1J;->setShareOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2191580
    return-void
.end method
