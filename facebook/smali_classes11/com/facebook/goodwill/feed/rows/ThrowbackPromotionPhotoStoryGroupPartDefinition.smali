.class public Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191720
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2191721
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    .line 2191722
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    .line 2191723
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    .line 2191724
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;
    .locals 6

    .prologue
    .line 2191725
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;

    monitor-enter v1

    .line 2191726
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191727
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191728
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191729
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191730
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;)V

    .line 2191731
    move-object v0, p0

    .line 2191732
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191733
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191734
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2191736
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191737
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191738
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;

    .line 2191739
    iget-object v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->c:Lcom/facebook/graphql/model/FeedUnit;

    move-object v0, v1

    .line 2191740
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2191741
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2191742
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    invoke-virtual {p1, v2, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191743
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191744
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionPhotoStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191745
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2191746
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2191747
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191748
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;

    .line 2191749
    iget-object p0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->c:Lcom/facebook/graphql/model/FeedUnit;

    move-object v0, p0

    .line 2191750
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    move v0, v1

    .line 2191751
    :goto_0
    return v0

    .line 2191752
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191753
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;

    .line 2191754
    iget-object p0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->c:Lcom/facebook/graphql/model/FeedUnit;

    move-object v0, p0

    .line 2191755
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2191756
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
