.class public Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190442
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2190443
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2190444
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->b:Landroid/content/Context;

    .line 2190445
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 2190446
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2190447
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2190448
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;
    .locals 9

    .prologue
    .line 2190468
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;

    monitor-enter v1

    .line 2190469
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190470
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190471
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190472
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190473
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 2190474
    move-object v0, v3

    .line 2190475
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190476
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190477
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190478
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2190460
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;)Lcom/facebook/goodwill/composer/GoodwillComposerEvent;
    .locals 10

    .prologue
    .line 2190461
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f082a1f

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f082a22

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f082a23

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->f()I

    move-result v7

    const-string v8, "throwback_permalink"

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 2190462
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2190463
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2190464
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2190465
    new-instance v5, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;-><init>(Lcom/facebook/graphql/model/GraphQLMedia;)V

    invoke-virtual {v0, v5}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 2190466
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2190467
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2190459
    sget-object v0, LX/3VK;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2190450
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;

    const/4 v4, 0x0

    .line 2190451
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2190452
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    .line 2190453
    new-instance v2, LX/F0k;

    invoke-direct {v2, p0, v1}, LX/F0k;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;)V

    move-object v1, v2

    .line 2190454
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-interface {p1, v2, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2190455
    const v2, 0x7f0d175f

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2190456
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2190457
    return-object v4

    .line 2190458
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignSharePartDefinition;->b:Landroid/content/Context;

    const v1, 0x7f080fd9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2190449
    const/4 v0, 0x1

    return v0
.end method
