.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

.field public final b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;

.field public final c:LX/F1E;

.field public final d:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

.field public final e:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;

.field public final f:LX/03V;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;LX/F1E;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191471
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2191472
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    .line 2191473
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    .line 2191474
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;

    .line 2191475
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->c:LX/F1E;

    .line 2191476
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->e:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;

    .line 2191477
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->f:LX/03V;

    .line 2191478
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;
    .locals 10

    .prologue
    .line 2191460
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;

    monitor-enter v1

    .line 2191461
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191462
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191463
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191464
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191465
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;

    const-class v7, LX/F1E;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/F1E;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;LX/F1E;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;LX/03V;)V

    .line 2191466
    move-object v0, v3

    .line 2191467
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191468
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191469
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2191431
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191432
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191433
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 2191434
    const-string v1, "redesign_v1"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2191435
    new-instance v1, LX/F0p;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, p2, v2, v3}, LX/F0p;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;Z)V

    .line 2191436
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    invoke-virtual {p1, v2, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191437
    :goto_0
    const/4 v2, 0x0

    .line 2191438
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    .line 2191439
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v7, :cond_1

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;

    .line 2191440
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;->j()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 2191441
    add-int/lit8 v3, v2, 0x1

    .line 2191442
    const-string v2, "redesign_v1"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2191443
    const/4 v2, 0x3

    if-eq v3, v2, :cond_0

    if-ne v3, v5, :cond_3

    :cond_0
    const/4 v2, 0x1

    .line 2191444
    :goto_2
    iget-object v8, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    new-instance v9, LX/F14;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;->j()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v10

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p3

    if-nez v2, :cond_4

    const/4 v1, 0x1

    :goto_3
    invoke-direct {v9, p2, v10, p3, v1}, LX/F14;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLUser;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Z)V

    invoke-virtual {p1, v8, v9}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191445
    :goto_4
    const/4 v1, 0x3

    if-lt v3, v1, :cond_8

    .line 2191446
    :cond_1
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->e:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191447
    const/4 v0, 0x0

    move-object v0, v0

    .line 2191448
    return-object v0

    .line 2191449
    :cond_2
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2191450
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 2191451
    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 2191452
    :cond_5
    if-ne v3, v5, :cond_6

    const/4 v2, 0x1

    .line 2191453
    :goto_5
    iget-object v8, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->c:LX/F1E;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2191454
    new-instance p3, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;

    invoke-static {v8}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v8}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;

    invoke-direct {p3, v9, v10, v2}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;Ljava/lang/Boolean;)V

    .line 2191455
    move-object v2, p3

    .line 2191456
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;->j()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_4

    .line 2191457
    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    .line 2191458
    :cond_7
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;->f:LX/03V;

    const-string v3, "ThrowbackFriendversaryGroupPartDefinition_null_friend_edge"

    const-string v8, "Received null friend edge"

    invoke-virtual {v1, v3, v8}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2191459
    :goto_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_1

    :cond_8
    move v1, v3

    goto :goto_6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2191427
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191428
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191429
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 2191430
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
