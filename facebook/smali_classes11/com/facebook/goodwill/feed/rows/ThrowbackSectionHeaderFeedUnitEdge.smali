.class public Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge;
.super Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2191763
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;-><init>()V

    .line 2191764
    new-instance v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;

    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-super {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2191765
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b(Ljava/lang/String;)V

    .line 2191766
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2191767
    const-string v0, "GoodwillThrowback.SECTION_MARKER"

    return-object v0
.end method
