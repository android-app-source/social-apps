.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191094
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2191095
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->a:LX/0Ot;

    .line 2191096
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->b:LX/0Ot;

    .line 2191097
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->c:LX/0Ot;

    .line 2191098
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 2191099
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    monitor-enter v1

    .line 2191100
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191101
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191102
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191103
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191104
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    const/16 v4, 0x1f0f

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1f2a

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xa51

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2191105
    move-object v0, v3

    .line 2191106
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191107
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191108
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191109
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2191110
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    const/4 v3, 0x0

    .line 2191111
    invoke-static {p2}, LX/F0s;->a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2191112
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p2, v1, v2}, LX/F0s;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191113
    :cond_0
    :goto_0
    return-object v3

    .line 2191114
    :cond_1
    invoke-static {p2}, LX/F0s;->b(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191115
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2191116
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2191117
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v2

    .line 2191118
    iput-object v2, v1, LX/39x;->q:LX/0Px;

    .line 2191119
    move-object v1, v1

    .line 2191120
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2191121
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->g()Ljava/lang/String;

    move-result-object p0

    .line 2191122
    iput-object p0, v2, LX/23u;->m:Ljava/lang/String;

    .line 2191123
    move-object v2, v2

    .line 2191124
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    .line 2191125
    iput-object p0, v2, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2191126
    move-object v2, v2

    .line 2191127
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2191128
    iput-object v1, v2, LX/23u;->k:LX/0Px;

    .line 2191129
    move-object v1, v2

    .line 2191130
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2191131
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2191132
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    move-object v1, v1

    .line 2191133
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2191134
    :cond_2
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2191135
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v2

    .line 2191136
    iput-object v2, v1, LX/39x;->q:LX/0Px;

    .line 2191137
    move-object v1, v1

    .line 2191138
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2191139
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->g()Ljava/lang/String;

    move-result-object p0

    .line 2191140
    iput-object p0, v2, LX/23u;->m:Ljava/lang/String;

    .line 2191141
    move-object v2, v2

    .line 2191142
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    .line 2191143
    iput-object p0, v2, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2191144
    move-object v2, v2

    .line 2191145
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2191146
    iput-object v1, v2, LX/23u;->k:LX/0Px;

    .line 2191147
    move-object v1, v2

    .line 2191148
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2191149
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    move-object v1, v1

    .line 2191150
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2191151
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 2191152
    invoke-static {p1}, LX/F0s;->b(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/F0s;->a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
