.class public Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:LX/2dq;

.field private final g:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2191663
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f800000    # -4.0f

    .line 2191664
    iput v1, v0, LX/1UY;->d:F

    .line 2191665
    move-object v0, v0

    .line 2191666
    const/high16 v1, 0x41400000    # 12.0f

    .line 2191667
    iput v1, v0, LX/1UY;->c:F

    .line 2191668
    move-object v0, v0

    .line 2191669
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;LX/2dq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191655
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2191656
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->g:Landroid/content/res/Resources;

    .line 2191657
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->b:Landroid/content/Context;

    .line 2191658
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2191659
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;

    .line 2191660
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2191661
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->f:LX/2dq;

    .line 2191662
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2191670
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2191644
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v7, 0x0

    .line 2191645
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->g:Landroid/content/res/Resources;

    const v1, 0x7f0b20da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2191646
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->b:Landroid/content/Context;

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    int-to-float v1, v0

    .line 2191647
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->a:LX/1Ua;

    invoke-direct {v2, v7, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191648
    iget-object v6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->f:LX/2dq;

    const/high16 v3, 0x41000000    # 8.0f

    add-float/2addr v1, v3

    sget-object v3, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->a:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    const/4 v2, 0x0

    .line 2191649
    new-instance v3, LX/F1M;

    invoke-direct {v3, p0, p2}, LX/F1M;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v3, v3

    .line 2191650
    iget-object v4, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2191651
    check-cast v4, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g()Ljava/lang/String;

    move-result-object v4

    .line 2191652
    iget-object v5, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2191653
    check-cast v5, LX/0jW;

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191654
    return-object v7
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2191628
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2191629
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191630
    if-eqz v0, :cond_0

    .line 2191631
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191632
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2191633
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191634
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 2191635
    :goto_0
    return v0

    .line 2191636
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191637
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2191638
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2191639
    if-eqz v0, :cond_2

    .line 2191640
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2191641
    const/4 v0, 0x1

    goto :goto_0

    .line 2191642
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2191643
    goto :goto_0
.end method
