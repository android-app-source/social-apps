.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2190961
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2190962
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2190963
    const-string v1, "friend_list"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2190964
    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    .line 2190965
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2190966
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2190967
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->p:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    .line 2190968
    iget-object v2, v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;->a:LX/0Px;

    move-object v1, v2

    .line 2190969
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2190970
    const/16 v2, 0x32

    if-lt v1, v2, :cond_0

    .line 2190971
    const v1, 0x7f083166

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2190972
    :goto_0
    new-instance v1, LX/F0v;

    invoke-direct {v1, p0}, LX/F0v;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2190973
    return-void

    .line 2190974
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0163

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2190975
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2190976
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "friend_list"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->p:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    .line 2190977
    const v0, 0x7f0314a9

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->setContentView(I)V

    .line 2190978
    new-instance v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;

    invoke-direct {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;-><init>()V

    .line 2190979
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->p:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    .line 2190980
    iput-object v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    .line 2190981
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 2190982
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2190983
    invoke-direct {p0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsActivity;->a()V

    .line 2190984
    return-void
.end method
