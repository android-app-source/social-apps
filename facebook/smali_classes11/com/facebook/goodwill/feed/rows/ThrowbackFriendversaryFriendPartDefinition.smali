.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLUser;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;

.field private final c:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2191386
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 2191387
    iput v1, v0, LX/1UY;->c:F

    .line 2191388
    move-object v0, v0

    .line 2191389
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;Ljava/lang/Boolean;)V
    .locals 0
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191400
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2191401
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2191402
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;

    .line 2191403
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->c:Ljava/lang/Boolean;

    .line 2191404
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2191405
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2191391
    check-cast p2, Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x0

    .line 2191392
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v5, LX/1X9;->BOTTOM:LX/1X9;

    .line 2191393
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->d:LX/1Ua;

    .line 2191394
    :goto_1
    iget-object v6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v0, LX/1X6;

    const/4 v3, -0x1

    const v4, 0x7f0207fa

    invoke-direct/range {v0 .. v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;IILX/1X9;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191395
    if-eqz p2, :cond_0

    .line 2191396
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191397
    :cond_0
    return-object v1

    .line 2191398
    :cond_1
    sget-object v5, LX/1X9;->MIDDLE:LX/1X9;

    goto :goto_0

    .line 2191399
    :cond_2
    sget-object v2, LX/1Ua;->a:LX/1Ua;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2191390
    const/4 v0, 0x1

    return v0
.end method
