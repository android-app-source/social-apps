.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;",
        ">;",
        "LX/F1I;",
        "LX/1Ps;",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:LX/1Ua;

.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2191483
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 2191484
    iput v1, v0, LX/1UY;->b:F

    .line 2191485
    move-object v0, v0

    .line 2191486
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191487
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2191488
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2191489
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;
    .locals 4

    .prologue
    .line 2191490
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;

    monitor-enter v1

    .line 2191491
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191492
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191493
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191494
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191495
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2191496
    move-object v0, p0

    .line 2191497
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191498
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191499
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191500
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2191501
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2191502
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2191503
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191504
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 2191505
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    sget-object v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderPartDefinition;->b:LX/1Ua;

    sget-object v5, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v3, v1, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191506
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2191507
    new-instance v2, Landroid/text/SpannableString;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2191508
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2191509
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v2

    .line 2191510
    :goto_0
    new-instance v2, LX/F1I;

    invoke-direct {v2, v1, v0}, LX/F1I;-><init>(Landroid/text/Spannable;Landroid/net/Uri;)V

    return-object v2

    :cond_0
    move-object v0, v1

    move-object v1, v2

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3f899ff9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191511
    check-cast p2, LX/F1I;

    check-cast p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;

    .line 2191512
    iget-object v1, p2, LX/F1I;->a:Landroid/text/Spannable;

    .line 2191513
    if-eqz v1, :cond_0

    .line 2191514
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2191515
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2191516
    :goto_0
    goto :goto_2

    .line 2191517
    :goto_1
    iget-object v1, p2, LX/F1I;->b:Landroid/net/Uri;

    .line 2191518
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2191519
    const/16 v1, 0x1f

    const v2, 0x7af7520d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2191520
    :cond_0
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 2191521
    :goto_2
    iget-object v2, p4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->e:Lcom/facebook/resources/ui/FbTextView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2191522
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191523
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191524
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 2191525
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
