.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2190926
    new-instance v0, LX/F0t;

    invoke-direct {v0}, LX/F0t;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2190927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2190928
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->a:Ljava/lang/String;

    .line 2190929
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->b:Ljava/lang/String;

    .line 2190930
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->c:Ljava/lang/String;

    .line 2190931
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->d:Ljava/lang/String;

    .line 2190932
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2190933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2190934
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->a:Ljava/lang/String;

    .line 2190935
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->b:Ljava/lang/String;

    .line 2190936
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->c:Ljava/lang/String;

    .line 2190937
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->d:Ljava/lang/String;

    .line 2190938
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2190939
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2190940
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2190941
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2190942
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2190943
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2190944
    return-void
.end method
