.class public Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190655
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2190656
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->e:LX/0Ot;

    .line 2190657
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->a:LX/0Ot;

    .line 2190658
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->b:LX/0Ot;

    .line 2190659
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->c:LX/0Ot;

    .line 2190660
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->d:LX/0Ot;

    .line 2190661
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->f:LX/0Ot;

    .line 2190662
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->g:LX/0Ot;

    .line 2190663
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;
    .locals 11

    .prologue
    .line 2190682
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;

    monitor-enter v1

    .line 2190683
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190684
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190685
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190686
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190687
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;

    const/16 v4, 0x1f0f

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1f2a

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa51

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1f35

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x233e

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1f0e

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xae0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2190688
    move-object v0, v3

    .line 2190689
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190690
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190691
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190692
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2190693
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190694
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2190695
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 2190696
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2190697
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190698
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    .line 2190699
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2190700
    if-eqz v1, :cond_1

    .line 2190701
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/F0s;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190702
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190703
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190704
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 2190705
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2190706
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 2190707
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 2190708
    new-instance v3, LX/39x;

    invoke-direct {v3}, LX/39x;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v4

    .line 2190709
    iput-object v4, v3, LX/39x;->q:LX/0Px;

    .line 2190710
    move-object v3, v3

    .line 2190711
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2190712
    new-instance v4, LX/23u;

    invoke-direct {v4}, LX/23u;-><init>()V

    .line 2190713
    iput-object v0, v4, LX/23u;->m:Ljava/lang/String;

    .line 2190714
    move-object v4, v4

    .line 2190715
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p3

    .line 2190716
    iput-object p3, v4, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2190717
    move-object v4, v4

    .line 2190718
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2190719
    iput-object v3, v4, LX/23u;->k:LX/0Px;

    .line 2190720
    move-object v3, v4

    .line 2190721
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2190722
    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 2190723
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p3

    invoke-virtual {p3, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p3, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    move-object v0, v3

    .line 2190724
    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190725
    :goto_2
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190726
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2190727
    :cond_2
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 2190728
    new-instance v3, LX/39x;

    invoke-direct {v3}, LX/39x;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v4

    .line 2190729
    iput-object v4, v3, LX/39x;->q:LX/0Px;

    .line 2190730
    move-object v3, v3

    .line 2190731
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2190732
    new-instance v4, LX/23u;

    invoke-direct {v4}, LX/23u;-><init>()V

    .line 2190733
    iput-object v0, v4, LX/23u;->m:Ljava/lang/String;

    .line 2190734
    move-object v4, v4

    .line 2190735
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p3

    .line 2190736
    iput-object p3, v4, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2190737
    move-object v4, v4

    .line 2190738
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2190739
    iput-object v3, v4, LX/23u;->k:LX/0Px;

    .line 2190740
    move-object v3, v4

    .line 2190741
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2190742
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    move-object v0, v3

    .line 2190743
    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2190664
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2190665
    if-eqz p1, :cond_0

    .line 2190666
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2190667
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2190668
    :goto_0
    return v0

    .line 2190669
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2190670
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 2190671
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v3

    .line 2190672
    if-nez v3, :cond_2

    move v0, v1

    .line 2190673
    goto :goto_0

    .line 2190674
    :cond_2
    iget-object v4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v1

    .line 2190675
    goto :goto_0

    .line 2190676
    :cond_3
    const-string v4, "friendversary_video"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2190677
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    .line 2190678
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 2190679
    :cond_5
    const-string v4, "friendversary_collage"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "friendversary_profile_pictures"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2190680
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    move v0, v1

    .line 2190681
    goto :goto_0
.end method
