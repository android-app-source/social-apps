.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

.field private final b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

.field private final c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;

.field private final d:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191308
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2191309
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    .line 2191310
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    .line 2191311
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;

    .line 2191312
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    .line 2191313
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;
    .locals 7

    .prologue
    .line 2191324
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;

    monitor-enter v1

    .line 2191325
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191326
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191327
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191328
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191329
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;)V

    .line 2191330
    move-object v0, p0

    .line 2191331
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191332
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191333
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191334
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2191315
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2191316
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    .line 2191317
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2191318
    new-instance v2, LX/F0p;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, v1, v3, v4}, LX/F0p;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;Z)V

    .line 2191319
    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderPartDefinition;

    invoke-virtual {p1, v3, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191320
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;

    invoke-virtual {p1, v2, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191321
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAttachmentPartDefinition;

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191322
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignGroupPartDefinition;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2191323
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2191314
    const/4 v0, 0x1

    return v0
.end method
