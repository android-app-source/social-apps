.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3ZK;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Landroid/content/Context;

.field public final e:LX/1Kf;

.field private final f:LX/03V;

.field public final g:LX/1Nq;

.field public final h:LX/Es5;

.field public final i:LX/0Uh;

.field public final j:LX/1Cn;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/Context;LX/1Kf;LX/03V;LX/1Nq;LX/Es5;LX/0Uh;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191249
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2191250
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 2191251
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2191252
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2191253
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->d:Landroid/content/Context;

    .line 2191254
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->e:LX/1Kf;

    .line 2191255
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->f:LX/03V;

    .line 2191256
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->g:LX/1Nq;

    .line 2191257
    iput-object p8, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->h:LX/Es5;

    .line 2191258
    iput-object p9, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->i:LX/0Uh;

    .line 2191259
    iput-object p10, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->j:LX/1Cn;

    .line 2191260
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;
    .locals 14

    .prologue
    .line 2191238
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    monitor-enter v1

    .line 2191239
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191240
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191241
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191242
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191243
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v8

    check-cast v8, LX/1Kf;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v10

    check-cast v10, LX/1Nq;

    invoke-static {v0}, LX/Es5;->a(LX/0QB;)LX/Es5;

    move-result-object v11

    check-cast v11, LX/Es5;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v13

    check-cast v13, LX/1Cn;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/Context;LX/1Kf;LX/03V;LX/1Nq;LX/Es5;LX/0Uh;LX/1Cn;)V

    .line 2191244
    move-object v0, v3

    .line 2191245
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191246
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191247
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2191237
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)Ljava/lang/Void;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;",
            ">;TE;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 2191215
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191216
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v10

    .line 2191217
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v1, 0x7f080fd9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2191218
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2191219
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v2

    .line 2191220
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v3

    .line 2191221
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iget-object v4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v5, 0x7f082a1f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v6, 0x7f082a22

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->d:Landroid/content/Context;

    const v7, 0x7f082a23

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->f()I

    move-result v7

    const-string v8, "throwback_permalink"

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    invoke-static {v9}, LX/CFE;->a(Lcom/facebook/graphql/model/GraphQLUser;)LX/0Px;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 2191222
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2191223
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2191224
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2191225
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2191226
    new-instance v5, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;-><init>(Lcom/facebook/graphql/model/GraphQLMedia;)V

    invoke-virtual {v0, v5}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 2191227
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2191228
    :cond_1
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2191229
    const/4 v0, 0x0

    .line 2191230
    :goto_1
    return-object v0

    .line 2191231
    :cond_2
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191232
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->i:LX/0Uh;

    const/16 v2, 0x4e1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v1, v1

    .line 2191233
    if-eqz v1, :cond_3

    .line 2191234
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {p0, p2, v0, p3}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->a$redex0(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191235
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2191236
    :cond_3
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {p0, p2, v0, p3}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->b(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private static a$redex0(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;",
            ">;",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2191209
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191210
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    invoke-static {v0}, LX/6X1;->a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v3

    .line 2191211
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2191212
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    .line 2191213
    :cond_0
    new-instance v0, LX/F18;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LX/F18;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v0, v6

    .line 2191214
    goto :goto_0
.end method

.method public static b(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;)Landroid/view/View$OnClickListener;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;",
            ">;",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2191261
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191262
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2191263
    sget-object v1, LX/F0r;->COLLAGE_V1:LX/F0r;

    invoke-virtual {v1}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2191264
    new-instance v0, LX/F15;

    invoke-direct {v0, p0, p2}, LX/F15;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;)V

    move-object v0, v0

    .line 2191265
    :goto_0
    return-object v0

    .line 2191266
    :cond_0
    sget-object v1, LX/F0r;->POLAROID_V1:LX/F0r;

    invoke-virtual {v1}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2191267
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v2

    .line 2191268
    const/4 p1, 0x0

    .line 2191269
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2191270
    if-eqz v1, :cond_2

    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    .line 2191271
    iput-object p1, v1, LX/39x;->t:Ljava/lang/String;

    .line 2191272
    move-object p1, v1

    .line 2191273
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    const/4 p2, 0x0

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 2191274
    iput-object v1, p1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2191275
    move-object v1, p1

    .line 2191276
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2191277
    :goto_2
    new-instance p1, LX/F16;

    invoke-direct {p1, p0, p3, v2, v1}, LX/F16;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;LX/1Po;Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    move-object v0, p1

    .line 2191278
    goto/16 :goto_0

    .line 2191279
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2191280
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    move v1, p1

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 2191208
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2191207
    sget-object v0, LX/3ZK;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2191206
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6d39168a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191202
    check-cast p4, LX/3ZK;

    .line 2191203
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/3ZK;->setHasBottomDivider(Z)V

    .line 2191204
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, LX/3ZK;->setHasButton(Z)V

    .line 2191205
    const/16 v1, 0x1f

    const v2, 0x11950094

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2191195
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191196
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191197
    if-eqz v0, :cond_0

    .line 2191198
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191199
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2191200
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2191201
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
