.class public Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/1x9;

.field private final b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final d:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

.field private final e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

.field private final h:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;LX/14w;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190479
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2190480
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 2190481
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 2190482
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    .line 2190483
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2190484
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2190485
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->g:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    .line 2190486
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->h:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2190487
    new-instance v0, LX/1x9;

    invoke-direct {v0, p8}, LX/1x9;-><init>(LX/14w;)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->a:LX/1x9;

    .line 2190488
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;
    .locals 12

    .prologue
    .line 2190489
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;

    monitor-enter v1

    .line 2190490
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190491
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190492
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190493
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190494
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v11

    check-cast v11, LX/14w;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;LX/14w;)V

    .line 2190495
    move-object v0, v3

    .line 2190496
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190497
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190498
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190499
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2190500
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190501
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2190502
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2190503
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2190504
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2190505
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190506
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190507
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190508
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190509
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190510
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->g:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190511
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->h:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190512
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2190513
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190514
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAttachedStoryPartDefinition;->a:LX/1x9;

    invoke-virtual {v0, p1}, LX/1x9;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
