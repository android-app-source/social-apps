.class public Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final d:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

.field private final f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2190338
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2190339
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    .line 2190340
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2190341
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2190342
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2190343
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    .line 2190344
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 2190345
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 2190346
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;
    .locals 11

    .prologue
    .line 2190357
    const-class v1, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;

    monitor-enter v1

    .line 2190358
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2190359
    sput-object v2, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2190360
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2190361
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2190362
    new-instance v3, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;)V

    .line 2190363
    move-object v0, v3

    .line 2190364
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2190365
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2190366
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2190367
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2190348
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190349
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190350
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190351
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190352
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190353
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190354
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190355
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/BasicThrowbackFeedStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2190356
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2190347
    const/4 v0, 0x1

    return v0
.end method
