.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2191338
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2191339
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 2191340
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    .line 2191341
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2191342
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2191343
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2191344
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;
    .locals 9

    .prologue
    .line 2191345
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;

    monitor-enter v1

    .line 2191346
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2191347
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2191348
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191349
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2191350
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2191351
    move-object v0, v3

    .line 2191352
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2191353
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2191354
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2191355
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2191356
    sget-object v0, LX/3VK;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2191357
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    const/4 v10, 0x0

    const/high16 v9, -0x3ee00000    # -10.0f

    .line 2191358
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;->a()LX/0Px;

    move-result-object v2

    .line 2191359
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2191360
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;

    .line 2191361
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;->j()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2191362
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;->j()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2191363
    new-instance v5, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    .line 2191364
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    if-nez v7, :cond_3

    const/4 v7, 0x0

    :goto_1
    move-object v7, v7

    .line 2191365
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v8

    .line 2191366
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p3

    if-nez p3, :cond_4

    const/4 p3, 0x0

    :goto_2
    move-object v0, p3

    .line 2191367
    invoke-direct {v5, v6, v7, v8, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2191368
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2191369
    :cond_1
    new-instance v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;-><init>(LX/0Px;)V

    .line 2191370
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-interface {p1, v1, v10}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191371
    const v1, 0x7f0d175f

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    const v3, 0x7f083165

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2191372
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/F1D;

    invoke-direct {v2, p0, v0}, LX/F1D;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191373
    const-string v0, "redesign_v1"

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2191374
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ed00000    # -11.0f

    .line 2191375
    iput v1, v0, LX/1UY;->d:F

    .line 2191376
    move-object v0, v0

    .line 2191377
    iput v9, v0, LX/1UY;->b:F

    .line 2191378
    move-object v0, v0

    .line 2191379
    iput v9, v0, LX/1UY;->c:F

    .line 2191380
    move-object v0, v0

    .line 2191381
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    .line 2191382
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2191383
    :cond_2
    return-object v10

    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p3

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2191384
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 2191385
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
