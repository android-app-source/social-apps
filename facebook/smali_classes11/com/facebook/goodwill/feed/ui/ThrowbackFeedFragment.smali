.class public Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/5vN;


# instance fields
.field public A:LX/0g8;

.field public B:LX/0g4;

.field public C:Landroid/view/View;

.field public D:Landroid/view/View;

.field public E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

.field public F:Z

.field public G:Z

.field public final H:LX/F0H;

.field public a:LX/F08;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/F0B;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1CY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DCC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DCO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/F0J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1Cn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/CF2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/F1S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:LX/1Qq;

.field public w:LX/F1r;

.field public x:Landroid/view/View;

.field private y:Landroid/view/View;

.field public z:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2192530
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    .line 2192531
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->G:Z

    .line 2192532
    new-instance v0, LX/F1c;

    invoke-direct {v0, p0}, LX/F1c;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->H:LX/F0H;

    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2192472
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 2192473
    const v0, 0x7f0307d4

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2192474
    const v0, 0x7f0d14b5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    .line 2192475
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    iget-boolean v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->F:Z

    .line 2192476
    iput-boolean v2, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->e:Z

    .line 2192477
    const v0, 0x7f0314a5

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->D:Landroid/view/View;

    .line 2192478
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, LX/F1r;->setVisibility(I)V

    .line 2192479
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    new-instance v2, LX/F1f;

    invoke-direct {v2, p0}, LX/F1f;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    new-instance v6, LX/F1g;

    invoke-direct {v6, p0}, LX/F1g;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    .line 2192480
    iget-object p1, v0, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2192481
    iget-object p1, v0, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2192482
    iget-object p1, v0, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnSecondaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2192483
    iget-object p1, v0, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2192484
    iput-object v6, p1, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 2192485
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    new-instance v2, LX/F1h;

    invoke-direct {v2, p0}, LX/F1h;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    new-instance v6, LX/F1j;

    invoke-direct {v6, p0}, LX/F1j;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    .line 2192486
    iget-object p1, v0, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    invoke-virtual {p1, v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2192487
    iget-object p1, v0, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    .line 2192488
    iput-object v6, p1, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->g:LX/F1i;

    .line 2192489
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    new-instance v2, LX/F1l;

    invoke-direct {v2, p0}, LX/F1l;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->setNotificationButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2192490
    const v0, 0x7f0d14b6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->C:Landroid/view/View;

    .line 2192491
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2192492
    const v0, 0x7f0d14b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2192493
    const v2, 0x7f0d14b4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/BetterListView;

    .line 2192494
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->r:LX/0Uh;

    const/16 v6, 0x417

    const/4 p1, 0x0

    invoke-virtual {v2, v6, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v2, v2

    .line 2192495
    if-nez v2, :cond_1

    .line 2192496
    invoke-virtual {v0, v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2192497
    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2192498
    new-instance v0, LX/2iI;

    invoke-direct {v0, v1}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    .line 2192499
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    invoke-interface {v0}, LX/0g8;->k()V

    .line 2192500
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->b(Z)V

    .line 2192501
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    invoke-interface {v0, v5}, LX/0g8;->d(Z)V

    .line 2192502
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->x:Landroid/view/View;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 2192503
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->x:Landroid/view/View;

    const v1, 0x7f0d2ef1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->z:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2192504
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    const-string v1, "goodwill_throwback_feed"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->z:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    .line 2192505
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->b:LX/F0B;

    const/16 v2, 0xa

    .line 2192506
    iput-object v0, v1, LX/F0B;->j:Lcom/facebook/api/feedtype/FeedType;

    .line 2192507
    iput v2, v1, LX/F0B;->i:I

    .line 2192508
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->b:LX/F0B;

    new-instance v2, LX/F1Y;

    invoke-direct {v2, p0}, LX/F1Y;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    .line 2192509
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/F0B;

    iput-object v3, v0, LX/F08;->i:LX/F0B;

    .line 2192510
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/F1Y;

    iput-object v3, v0, LX/F08;->j:LX/F1Y;

    .line 2192511
    new-instance v0, LX/F1a;

    invoke-direct {v0, p0}, LX/F1a;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->B:LX/0g4;

    .line 2192512
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->d:LX/1CY;

    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->u(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/0fz;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->B:LX/0g4;

    invoke-virtual {v0, v1, v2}, LX/1CY;->a(LX/0fz;LX/0g4;)V

    .line 2192513
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->e:LX/DCC;

    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->u(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/0fz;

    move-result-object v1

    .line 2192514
    iget-object v2, v1, LX/0fz;->a:LX/0qq;

    move-object v1, v2

    .line 2192515
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->B:LX/0g4;

    invoke-virtual {v0, v1, v2}, LX/DCC;->a(LX/0qq;LX/0g4;)V

    .line 2192516
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->f:LX/DCO;

    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->u(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/0fz;

    move-result-object v1

    .line 2192517
    iget-object v2, v1, LX/0fz;->a:LX/0qq;

    move-object v1, v2

    .line 2192518
    new-instance v2, LX/F1b;

    invoke-direct {v2, p0}, LX/F1b;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-virtual {v0, v1, v2}, LX/DCO;->a(LX/0qq;LX/0g4;)V

    .line 2192519
    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->z()V

    .line 2192520
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->z:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-static {v5, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a(ZLcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 2192521
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->j:LX/CF2;

    .line 2192522
    iget-object v1, v0, LX/CF2;->b:LX/11i;

    sget-object v2, LX/CF2;->a:LX/CF1;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2192523
    if-eqz v1, :cond_0

    .line 2192524
    const-string v2, "ThrowbackFeedViewCreation"

    const v3, -0x5881c8d9

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v1

    const-string v2, "ThrowbackFeedDataFetch"

    const v3, -0x7b3dc7db

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2192525
    :cond_0
    return-void

    .line 2192526
    :cond_1
    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2192527
    invoke-virtual {v1, v4}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2192528
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2192529
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;LX/F08;LX/F0B;LX/1Db;LX/1CY;LX/DCC;LX/DCO;LX/F0J;LX/0Ot;LX/1Cn;LX/CF2;LX/0Or;LX/1CX;LX/1DS;LX/0Ot;LX/0Ot;LX/F1S;LX/0Or;LX/0Uh;Lcom/facebook/content/SecureContextHelper;LX/0wM;LX/0ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;",
            "LX/F08;",
            "LX/F0B;",
            "LX/1Db;",
            "LX/1CY;",
            "LX/DCC;",
            "LX/DCO;",
            "LX/F0J;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/1Cn;",
            "LX/CF2;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/1CX;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackFeedRootGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;",
            "LX/F1S;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0wM;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2192471
    iput-object p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    iput-object p2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->b:LX/F0B;

    iput-object p3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->c:LX/1Db;

    iput-object p4, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->d:LX/1CY;

    iput-object p5, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->e:LX/DCC;

    iput-object p6, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->f:LX/DCO;

    iput-object p7, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    iput-object p8, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->h:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->i:LX/1Cn;

    iput-object p10, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->j:LX/CF2;

    iput-object p11, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->k:LX/0Or;

    iput-object p12, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->l:LX/1CX;

    iput-object p13, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->m:LX/1DS;

    iput-object p14, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->p:LX/F1S;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->q:LX/0Or;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->r:LX/0Uh;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->s:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t:LX/0wM;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->u:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 24

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v23

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-static/range {v23 .. v23}, LX/F08;->a(LX/0QB;)LX/F08;

    move-result-object v3

    check-cast v3, LX/F08;

    invoke-static/range {v23 .. v23}, LX/F0B;->a(LX/0QB;)LX/F0B;

    move-result-object v4

    check-cast v4, LX/F0B;

    invoke-static/range {v23 .. v23}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v5

    check-cast v5, LX/1Db;

    invoke-static/range {v23 .. v23}, LX/1CY;->a(LX/0QB;)LX/1CY;

    move-result-object v6

    check-cast v6, LX/1CY;

    invoke-static/range {v23 .. v23}, LX/DCC;->a(LX/0QB;)LX/DCC;

    move-result-object v7

    check-cast v7, LX/DCC;

    invoke-static/range {v23 .. v23}, LX/DCO;->a(LX/0QB;)LX/DCO;

    move-result-object v8

    check-cast v8, LX/DCO;

    invoke-static/range {v23 .. v23}, LX/F0J;->a(LX/0QB;)LX/F0J;

    move-result-object v9

    check-cast v9, LX/F0J;

    const/16 v10, 0x19c6

    move-object/from16 v0, v23

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {v23 .. v23}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v11

    check-cast v11, LX/1Cn;

    invoke-static/range {v23 .. v23}, LX/CF2;->a(LX/0QB;)LX/CF2;

    move-result-object v12

    check-cast v12, LX/CF2;

    const/16 v13, 0x1430

    move-object/from16 v0, v23

    invoke-static {v0, v13}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {v23 .. v23}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v14

    check-cast v14, LX/1CX;

    invoke-static/range {v23 .. v23}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v15

    check-cast v15, LX/1DS;

    const/16 v16, 0x2342

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x1a75

    move-object/from16 v0, v23

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const-class v18, LX/F1S;

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/F1S;

    const/16 v19, 0x1399

    move-object/from16 v0, v23

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    invoke-static/range {v23 .. v23}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v20

    check-cast v20, LX/0Uh;

    invoke-static/range {v23 .. v23}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v21

    check-cast v21, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v23 .. v23}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v22

    check-cast v22, LX/0wM;

    invoke-static/range {v23 .. v23}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v23

    check-cast v23, LX/0ad;

    invoke-static/range {v2 .. v23}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;LX/F08;LX/F0B;LX/1Db;LX/1CY;LX/DCC;LX/DCO;LX/F0J;LX/0Ot;LX/1Cn;LX/CF2;LX/0Or;LX/1CX;LX/1DS;LX/0Ot;LX/0Ot;LX/F1S;LX/0Or;LX/0Uh;Lcom/facebook/content/SecureContextHelper;LX/0wM;LX/0ad;)V

    return-void
.end method

.method public static a(ZLcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V
    .locals 0

    .prologue
    .line 2192467
    if-eqz p0, :cond_0

    .line 2192468
    invoke-virtual {p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2192469
    :goto_0
    return-void

    .line 2192470
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;Z)V
    .locals 2

    .prologue
    .line 2192465
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    new-instance v1, LX/F1V;

    invoke-direct {v1, p0, p1}, LX/F1V;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;Z)V

    invoke-virtual {v0, p1, v1}, LX/F0J;->a(ZLX/F0H;)V

    .line 2192466
    return-void
.end method

.method public static d$redex0(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V
    .locals 13

    .prologue
    .line 2192299
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 2192300
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192301
    iget-boolean v3, v2, LX/F08;->h:Z

    move v2, v3

    .line 2192302
    iput-boolean v2, v1, LX/F1r;->n:Z

    .line 2192303
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192304
    iget-object v3, v2, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v2, v3

    .line 2192305
    const/16 v10, 0x8

    const/4 v9, 0x3

    const/4 v5, 0x1

    const/16 v8, 0xe

    const/4 v6, 0x0

    .line 2192306
    const-string v3, "ipb_v1"

    .line 2192307
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->n:Ljava/lang/String;

    move-object v4, v4

    .line 2192308
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, v1, LX/F1r;->l:Z

    .line 2192309
    iget-object v3, v1, LX/F1r;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget-boolean v4, v1, LX/F1r;->l:Z

    .line 2192310
    iput-boolean v4, v3, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->z:Z

    .line 2192311
    iget-boolean v3, v1, LX/F1r;->l:Z

    if-eqz v3, :cond_0

    .line 2192312
    iget-object v3, v1, LX/F1r;->h:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0b208f

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2192313
    iget-object v3, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->o:LX/1Fb;

    move-object v3, v3

    .line 2192314
    if-eqz v3, :cond_0

    invoke-static {v3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2192315
    iget-object v4, v1, LX/F1r;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v4, v3, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2192316
    iget-object v3, v1, LX/F1r;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2192317
    iget-object v3, v1, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2192318
    iget-object v3, v1, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2192319
    iget-object v3, v1, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2192320
    iget-object v3, v1, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    const v4, 0x7f0d2f00

    invoke-virtual {v3, v9, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2192321
    iget-object v3, v1, LX/F1r;->i:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2192322
    iget-object v3, v1, LX/F1r;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2192323
    iget-object v3, v1, LX/F1r;->d:Landroid/widget/TextView;

    iget-object v4, v1, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b20bc

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    neg-int v7, v7

    iget-object v8, v1, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v8

    iget-object v9, v1, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v9

    invoke-virtual {v3, v4, v7, v8, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2192324
    iget-object v3, v1, LX/F1r;->c:Landroid/widget/TextView;

    iget-object v4, v1, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b20bb

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v8, v1, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v8

    iget-object v9, v1, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v9

    invoke-virtual {v3, v4, v7, v8, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2192325
    iget-object v3, v1, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0a00d3

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2192326
    iget-object v3, v1, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0a00d6

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2192327
    iget-object v3, v1, LX/F1r;->c:Landroid/widget/TextView;

    sget-object v4, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v7, LX/0xr;->LIGHT:LX/0xr;

    iget-object v8, v1, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v8

    invoke-static {v3, v4, v7, v8}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2192328
    :cond_0
    iget-object v3, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->p:Ljava/lang/String;

    move-object v3, v3

    .line 2192329
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2192330
    iget-object v3, v1, LX/F1r;->j:Landroid/widget/TextView;

    .line 2192331
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->p:Ljava/lang/String;

    move-object v4, v4

    .line 2192332
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192333
    iget-object v3, v1, LX/F1r;->j:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2192334
    iget-object v3, v1, LX/F1r;->h:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2192335
    const/4 v4, -0x2

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2192336
    iget-object v4, v1, LX/F1r;->h:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v4, v3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2192337
    iget-object v3, v1, LX/F1r;->j:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    .line 2192338
    iget-object v4, v1, LX/F1r;->j:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    .line 2192339
    iget-boolean v7, v1, LX/F1r;->l:Z

    if-eqz v7, :cond_6

    .line 2192340
    iget-object v7, v1, LX/F1r;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-virtual {v7}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b2094

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2192341
    :goto_0
    iget-boolean v7, v1, LX/F1r;->n:Z

    move v7, v7

    .line 2192342
    if-eqz v7, :cond_1

    .line 2192343
    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0b2099

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2192344
    :cond_1
    iget-object v7, v1, LX/F1r;->j:Landroid/widget/TextView;

    iget-object v8, v1, LX/F1r;->j:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v8

    iget-object v9, v1, LX/F1r;->j:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v9

    invoke-virtual {v7, v8, v3, v9, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2192345
    iget-object v3, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->q:LX/1Fb;

    move-object v3, v3

    .line 2192346
    if-eqz v3, :cond_2

    invoke-static {v3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2192347
    iget-object v4, v1, LX/F1r;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v4, v3, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2192348
    iget-object v3, v1, LX/F1r;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2192349
    :cond_2
    :goto_1
    iget-object v3, v1, LX/F1r;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    .line 2192350
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    move-object v4, v4

    .line 2192351
    invoke-virtual {v3, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->setImageURLs(Ljava/util/List;)V

    .line 2192352
    iget-wide v11, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->a:J

    move-wide v3, v11

    .line 2192353
    new-instance v7, Ljava/util/Date;

    const-wide/16 v9, 0x3e8

    mul-long/2addr v3, v9

    invoke-direct {v7, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 2192354
    iget-object v3, v1, LX/F1r;->c:Landroid/widget/TextView;

    .line 2192355
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v8, "MMM"

    invoke-direct {v4, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 2192356
    const-string v8, "\\d*"

    invoke-virtual {v4, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2192357
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v8, "MMMM"

    invoke-direct {v4, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 2192358
    :cond_3
    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f083177

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 2192359
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192360
    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083178

    new-array v8, v5, [Ljava/lang/Object;

    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string v10, "d"

    invoke-direct {v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v6

    invoke-virtual {v3, v4, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2192361
    iget-object v4, v1, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192362
    iget-object v3, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->c:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    move-object v4, v3

    .line 2192363
    if-eqz v4, :cond_4

    .line 2192364
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v7, 0x3f81651f

    if-ne v3, v7, :cond_9

    .line 2192365
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->m()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2192366
    if-eqz v3, :cond_8

    move v3, v5

    :goto_2
    if-eqz v3, :cond_b

    .line 2192367
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->l()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2192368
    if-eqz v3, :cond_a

    :goto_3
    if-eqz v5, :cond_c

    .line 2192369
    iget-object v3, v1, LX/F1r;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2192370
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->m()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v7, v3, LX/1vs;->b:I

    const v3, 0x7f0d14c3

    invoke-virtual {v1, v3}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v5, v7, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192371
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    const v3, 0x7f0d14c4

    invoke-virtual {v1, v3}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v4, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192372
    :cond_4
    :goto_4
    iget-object v3, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2192373
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->k:Ljava/lang/String;

    move-object v4, v4

    .line 2192374
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    const/4 v5, 0x1

    :goto_5
    move v3, v5

    .line 2192375
    iput-boolean v3, v1, LX/F1r;->m:Z

    .line 2192376
    iget-boolean v3, v1, LX/F1r;->m:Z

    if-eqz v3, :cond_d

    .line 2192377
    iget-object v3, v1, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2192378
    iget-object v5, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->h:Ljava/lang/String;

    move-object v5, v5

    .line 2192379
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2192380
    iget-object v6, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->k:Ljava/lang/String;

    move-object v6, v6

    .line 2192381
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->a(II)V

    .line 2192382
    iget-object v3, v1, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    .line 2192383
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->f:LX/1Fb;

    move-object v4, v4

    .line 2192384
    invoke-virtual {v3, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->setImage(LX/1Fb;)V

    .line 2192385
    iget-object v3, v1, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    .line 2192386
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->g:Ljava/lang/String;

    move-object v4, v4

    .line 2192387
    invoke-virtual {v3, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2192388
    iget-object v3, v1, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    .line 2192389
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->i:Ljava/lang/String;

    move-object v4, v4

    .line 2192390
    invoke-virtual {v3, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2192391
    iget-object v3, v1, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    .line 2192392
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->j:Ljava/lang/String;

    move-object v4, v4

    .line 2192393
    invoke-virtual {v3, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 2192394
    :goto_6
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->D:Landroid/view/View;

    const/4 v7, 0x0

    .line 2192395
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192396
    iget-object v3, v2, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v5, v3

    .line 2192397
    const-string v2, "standard"

    .line 2192398
    iget-object v3, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->n:Ljava/lang/String;

    move-object v3, v3

    .line 2192399
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2192400
    const v2, 0x7f0d2ee4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192401
    invoke-virtual {v2, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2192402
    iget-object v3, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->d:LX/1Fb;

    move-object v3, v3

    .line 2192403
    invoke-static {v3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2192404
    :cond_5
    :goto_7
    const v2, 0x7f0d2ee5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2192405
    iget-object v3, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2192406
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192407
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192408
    iget-object v2, v1, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v1, v2

    .line 2192409
    iget-object v2, v1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->l:Ljava/lang/String;

    move-object v1, v2

    .line 2192410
    if-nez v1, :cond_11

    .line 2192411
    invoke-virtual {v0}, LX/F0J;->f()Z

    move-result v2

    if-eqz v2, :cond_10

    sget-object v2, LX/F0I;->STATUS_SUBSCRIBED_HIGHLIGHTS:LX/F0I;

    .line 2192412
    :goto_8
    iput-object v2, v0, LX/F0J;->g:LX/F0I;

    .line 2192413
    return-void

    .line 2192414
    :cond_6
    iget-object v3, v1, LX/F1r;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-virtual {v3}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b2093

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2192415
    invoke-virtual {v1}, LX/F1r;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0b2095

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto/16 :goto_0

    .line 2192416
    :cond_7
    iget-object v3, v1, LX/F1r;->j:Landroid/widget/TextView;

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2192417
    iget-object v3, v1, LX/F1r;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v10}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_8
    move v3, v6

    .line 2192418
    goto/16 :goto_2

    :cond_9
    move v3, v6

    goto/16 :goto_2

    :cond_a
    move v5, v6

    goto/16 :goto_3

    :cond_b
    move v5, v6

    goto/16 :goto_3

    .line 2192419
    :cond_c
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v5, -0x7c77e09a    # -8.00057E-37f

    if-ne v3, v5, :cond_4

    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2192420
    iget-object v3, v1, LX/F1r;->c:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2192421
    iget-object v3, v1, LX/F1r;->d:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 2192422
    :cond_d
    iget-object v3, v1, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2192423
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->g:Ljava/lang/String;

    move-object v4, v4

    .line 2192424
    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2192425
    iget-object v3, v1, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2192426
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->i:Ljava/lang/String;

    move-object v4, v4

    .line 2192427
    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2192428
    iget-object v3, v1, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2192429
    iget-object v4, v2, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->j:Ljava/lang/String;

    move-object v4, v4

    .line 2192430
    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 2192431
    :cond_f
    const-string v2, "ipb_v1"

    .line 2192432
    iget-object v3, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->n:Ljava/lang/String;

    move-object v3, v3

    .line 2192433
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2192434
    iget-object v2, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    move-object v2, v2

    .line 2192435
    if-eqz v2, :cond_5

    .line 2192436
    iget-object v2, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    move-object v2, v2

    .line 2192437
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_5

    .line 2192438
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b20a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2192439
    const v2, 0x7f0d2ee6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192440
    invoke-virtual {v2, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2192441
    iget-object v3, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    move-object v3, v3

    .line 2192442
    iget-object v4, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    move-object v4, v4

    .line 2192443
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2192444
    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2192445
    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2192446
    const/high16 v3, -0x3ee00000    # -10.0f

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setRotation(F)V

    .line 2192447
    const v3, 0x7f0d2ee7

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192448
    invoke-virtual {v3, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2192449
    iget-object v4, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    move-object v4, v4

    .line 2192450
    iget-object v7, v5, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    move-object v7, v7

    .line 2192451
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2192452
    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v6, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2192453
    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v6, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2192454
    const/high16 v4, 0x420c0000    # 35.0f

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setRotation(F)V

    .line 2192455
    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->invalidate()V

    .line 2192456
    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->invalidate()V

    goto/16 :goto_7

    .line 2192457
    :cond_10
    sget-object v2, LX/F0I;->STATUS_UNSUBSCRIBED:LX/F0I;

    goto/16 :goto_8

    .line 2192458
    :cond_11
    const-string v2, "UNSUBSCRIBED"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2192459
    sget-object v2, LX/F0I;->STATUS_UNSUBSCRIBED:LX/F0I;

    goto/16 :goto_8

    .line 2192460
    :cond_12
    const-string v2, "SUBSCRIBED_ALL"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2192461
    sget-object v2, LX/F0I;->STATUS_SUBSCRIBED_ALL:LX/F0I;

    goto/16 :goto_8

    .line 2192462
    :cond_13
    const-string v2, "SUBSCRIBED_HIGHLIGHTS"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 2192463
    sget-object v2, LX/F0I;->STATUS_SUBSCRIBED_HIGHLIGHTS:LX/F0I;

    goto/16 :goto_8

    .line 2192464
    :cond_14
    sget-object v2, LX/F0I;->STATUS_UNKNOW:LX/F0I;

    goto/16 :goto_8
.end method

.method public static k(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V
    .locals 3

    .prologue
    .line 2192296
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2192297
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->l:LX/1CX;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v1

    const v2, 0x7f083175

    invoke-virtual {v1, v2}, LX/4mn;->a(I)LX/4mn;

    move-result-object v1

    const v2, 0x7f083176

    invoke-virtual {v1, v2}, LX/4mn;->b(I)LX/4mn;

    move-result-object v1

    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 2192298
    :cond_0
    return-void
.end method

.method public static o$redex0(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V
    .locals 3

    .prologue
    .line 2192284
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->y:Landroid/view/View;

    .line 2192285
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2192286
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2192287
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->r:LX/0Uh;

    const/16 v1, 0x458

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2192288
    if-eqz v0, :cond_1

    .line 2192289
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, LX/0ax;->fG:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2192290
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2192291
    :goto_0
    return-void

    .line 2192292
    :cond_1
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08316c

    new-instance v2, LX/F1W;

    invoke-direct {v2, p0}, LX/F1W;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2192293
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/2EJ;->a(Landroid/view/View;)V

    .line 2192294
    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->s(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v2}, LX/F0J;->e()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2192295
    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public static s(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)Landroid/widget/CheckBox;
    .locals 2

    .prologue
    .line 2192283
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->y:Landroid/view/View;

    const v1, 0x7f0d2f03

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;
    .locals 11

    .prologue
    .line 2192269
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->v:LX/1Qq;

    if-nez v0, :cond_0

    .line 2192270
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->p:LX/F1S;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2192271
    sget-object v2, LX/F1s;->a:LX/F1s;

    move-object v2, v2

    .line 2192272
    new-instance v3, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment$15;

    invoke-direct {v3, p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment$15;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    iget-object v4, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    invoke-static {v4}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v4

    .line 2192273
    new-instance v5, LX/F1R;

    .line 2192274
    new-instance v7, LX/F1t;

    const-class v6, LX/F26;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/F26;

    invoke-direct {v7, v6}, LX/F1t;-><init>(LX/F26;)V

    .line 2192275
    move-object v10, v7

    .line 2192276
    check-cast v10, LX/F1t;

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    invoke-direct/range {v5 .. v10}, LX/F1R;-><init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;LX/F1t;)V

    .line 2192277
    move-object v0, v5

    .line 2192278
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->m:LX/1DS;

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->n:LX/0Ot;

    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->u(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/0fz;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2192279
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2192280
    move-object v0, v1

    .line 2192281
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->v:LX/1Qq;

    .line 2192282
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->v:LX/1Qq;

    return-object v0
.end method

.method public static u(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/0fz;
    .locals 1

    .prologue
    .line 2192266
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192267
    iget-object p0, v0, LX/F08;->b:LX/0fz;

    move-object v0, p0

    .line 2192268
    return-object v0
.end method

.method private z()V
    .locals 6

    .prologue
    .line 2192533
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    new-instance v1, LX/F1d;

    invoke-direct {v1, p0}, LX/F1d;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2192534
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    invoke-interface {v0, v1}, LX/0g8;->d(Landroid/view/View;)V

    .line 2192535
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2192536
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->c:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 2192537
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    invoke-virtual {v0}, LX/F08;->clearUserData()V

    .line 2192538
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192539
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2192540
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v2

    .line 2192541
    :goto_0
    move-object v1, v1

    .line 2192542
    const/4 v4, 0x1

    const/4 p0, 0x0

    .line 2192543
    iput-boolean p0, v0, LX/F08;->k:Z

    .line 2192544
    iput-boolean v4, v0, LX/F08;->l:Z

    .line 2192545
    iget-object v2, v0, LX/F08;->c:LX/1Ck;

    sget-object v3, LX/F07;->LOAD_INITIAL_FEED:LX/F07;

    invoke-static {v0, v4, v1}, LX/F08;->a$redex0(LX/F08;ZLjava/util/Map;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/F06;

    invoke-direct {v5, v0}, LX/F06;-><init>(LX/F08;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2192546
    iput-boolean p0, v0, LX/F08;->l:Z

    .line 2192547
    return-void

    .line 2192548
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 2192549
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "story_id"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2192550
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2192551
    const-string v4, "story_id"

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2192552
    :cond_2
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "campaign_id"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2192553
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2192554
    const-string v4, "campaign_id"

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2192555
    :cond_3
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "source"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2192556
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2192557
    const-string v3, "source"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    move-object v1, v2

    .line 2192558
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2192187
    const-string v0, "goodwill_throwback"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2192188
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2192189
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2192190
    return-void
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2192191
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2192192
    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v6

    new-instance v0, LX/5Mj;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5Mk;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/5Mj;-><init>(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;LX/5Mk;)V

    invoke-interface {v6, v0}, LX/0fn;->a(LX/5Mj;)V

    .line 2192193
    return-void
.end method

.method public final kR_()LX/0cQ;
    .locals 1

    .prologue
    .line 2192194
    sget-object v0, LX/0cQ;->THROWBACK_FEED_FRAGMENT:LX/0cQ;

    return-object v0
.end method

.method public final lr_()V
    .locals 1

    .prologue
    .line 2192195
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->lr_()V

    .line 2192196
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->d:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->d()V

    .line 2192197
    return-void
.end method

.method public final mJ_()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2192198
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    invoke-virtual {v0}, LX/F08;->clearUserData()V

    .line 2192199
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 2192200
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->D:Landroid/view/View;

    invoke-interface {v0, v1}, LX/0g8;->b(Landroid/view/View;)V

    .line 2192201
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->x:Landroid/view/View;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 2192202
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    invoke-virtual {v0, v2}, LX/F1r;->setVisibility(I)V

    .line 2192203
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    invoke-virtual {v0, v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->setVisibility(I)V

    .line 2192204
    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2192205
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    invoke-virtual {v0}, LX/F08;->e()V

    .line 2192206
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2192207
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2192208
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->mJ_()V

    .line 2192209
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 2192210
    :cond_1
    :goto_0
    return-void

    .line 2192211
    :cond_2
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 2192212
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2192213
    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2192214
    const-string v1, "is_uploading_media"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2192215
    new-instance v2, LX/F1X;

    invoke-direct {v2, p0}, LX/F1X;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2192216
    goto :goto_0

    .line 2192217
    :sswitch_1
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->mJ_()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x6dc -> :sswitch_0
        0x6de -> :sswitch_0
        0x740 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2192218
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2192219
    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2192220
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2192221
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2192222
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2192223
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/16 v3, 0x2a

    const v4, 0x59b06734

    invoke-static {v2, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2192224
    iget-object v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->r:LX/0Uh;

    const/16 v4, 0x4e0

    invoke-virtual {v3, v4, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 2192225
    iget-object v4, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->u:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget-short v7, LX/1Cs;->f:S

    invoke-interface {v4, v5, v6, v7, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v4

    .line 2192226
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->F:Z

    .line 2192227
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2192228
    iget-object v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->j:LX/CF2;

    .line 2192229
    iget-object v4, v3, LX/CF2;->b:LX/11i;

    sget-object v5, LX/CF2;->a:LX/CF1;

    invoke-interface {v4, v5}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v4

    const-string v5, "ThrowbackFeedViewCreation"

    const v6, -0x2647f209

    invoke-static {v4, v5, v6}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2192230
    new-instance v3, LX/F1r;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/F1r;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    .line 2192231
    const v3, 0x7f0314af

    invoke-virtual {v0, v3, v8, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->x:Landroid/view/View;

    .line 2192232
    const v3, 0x7f0314b6

    invoke-virtual {v0, v3, v8, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->y:Landroid/view/View;

    .line 2192233
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2192234
    invoke-direct {p0, v0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2192235
    const v0, -0x66e31a8d

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v1

    :cond_0
    move v0, v1

    .line 2192236
    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6bf02216

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2192237
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2192238
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->G:Z

    .line 2192239
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->v:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2192240
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192241
    invoke-static {v1}, LX/F08;->k(LX/F08;)V

    .line 2192242
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->d:LX/1CY;

    invoke-virtual {v1}, LX/1CY;->e()V

    .line 2192243
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->e:LX/DCC;

    invoke-virtual {v1}, LX/DCC;->a()V

    .line 2192244
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->f:LX/DCO;

    invoke-virtual {v1}, LX/DCO;->a()V

    .line 2192245
    const/16 v1, 0x2b

    const v2, 0x7829c595

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetach()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3acf839

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2192246
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDetach()V

    .line 2192247
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2192248
    iget-object v2, v1, LX/F08;->c:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2192249
    const/16 v1, 0x2b

    const v2, 0x782f15f0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6bffc153

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2192250
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 2192251
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2192252
    if-eqz v1, :cond_0

    .line 2192253
    const v2, 0x7f083163

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2192254
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2192255
    iget-boolean v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->F:Z

    if-eqz v2, :cond_0

    .line 2192256
    const v2, 0x7f083164

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2192257
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    .line 2192258
    iput-object v2, v4, LX/108;->j:Ljava/lang/String;

    .line 2192259
    move-object v2, v4

    .line 2192260
    iget-object v4, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t:LX/0wM;

    const v5, 0x7f0208ac

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2192261
    iput-object v4, v2, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2192262
    move-object v2, v2

    .line 2192263
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2192264
    new-instance v2, LX/F1e;

    invoke-direct {v2, p0}, LX/F1e;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2192265
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x73de87db

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
