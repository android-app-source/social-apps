.class public final Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/F1P;

.field public b:LX/F27;

.field public c:J

.field public d:I

.field public e:I

.field public f:Landroid/graphics/Point;

.field public g:Landroid/graphics/Point;

.field public h:I

.field public i:I

.field public j:I

.field public final synthetic k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

.field private final l:Lcom/facebook/common/callercontext/CallerContext;

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2191860
    iput-object p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2191861
    const-class v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->l:Lcom/facebook/common/callercontext/CallerContext;

    .line 2191862
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20c3    # 1.849328E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->m:I

    .line 2191863
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->n:I

    .line 2191864
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->o:I

    .line 2191865
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->p:I

    .line 2191866
    if-eqz p3, :cond_2

    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2191867
    :goto_0
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->n:I

    add-int/2addr v1, v4

    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->m:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, -0x2

    invoke-direct {v0, v5, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    .line 2191868
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->n:I

    add-int/2addr v1, v4

    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->m:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, -0x2

    invoke-direct {v0, v5, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    .line 2191869
    if-eqz p3, :cond_3

    .line 2191870
    const/16 v0, 0xa

    const/16 v1, 0x2d

    invoke-static {p1, v0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->c(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;II)I

    move-result v0

    move v6, v0

    .line 2191871
    :goto_1
    sget-object v0, LX/F1P;->BEFORE_INITIAL_ANIMATION:LX/F1P;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a:LX/F1P;

    .line 2191872
    iget v0, p1, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->j:I

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->h:I

    .line 2191873
    iget v0, p1, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->k:I

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->i:I

    .line 2191874
    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2191875
    new-instance v0, LX/F27;

    if-eqz p3, :cond_4

    move v2, v5

    :goto_2
    if-eqz p3, :cond_5

    move v3, v5

    :goto_3
    if-nez p3, :cond_0

    const/4 v5, 0x1

    :cond_0
    invoke-direct/range {v0 .. v5}, LX/F27;-><init>(Landroid/content/res/Resources;IIIZ)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b:LX/F27;

    .line 2191876
    iput v6, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->j:I

    .line 2191877
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b:LX/F27;

    int-to-float v1, v6

    invoke-virtual {v0, v1}, LX/F27;->a(F)V

    .line 2191878
    invoke-static {p2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 2191879
    if-eqz v0, :cond_1

    .line 2191880
    iget-object v1, p1, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b:LX/1HI;

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 2191881
    new-instance v1, LX/F1O;

    invoke-direct {v1, p0, p1}, LX/F1O;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;)V

    iget-object v2, p1, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2191882
    :cond_1
    return-void

    .line 2191883
    :cond_2
    iget v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->p:I

    iget v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->o:I

    invoke-static {p1, v0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;II)I

    move-result v4

    goto :goto_0

    .line 2191884
    :cond_3
    const/16 v0, -0x1e

    const/16 v1, 0x1e

    invoke-static {p1, v0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;II)I

    move-result v0

    move v6, v0

    goto :goto_1

    .line 2191885
    :cond_4
    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->n:I

    goto :goto_2

    :cond_5
    iget v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->m:I

    goto :goto_3
.end method

.method private e()I
    .locals 2

    .prologue
    .line 2191859
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private f()I
    .locals 2

    .prologue
    .line 2191858
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private g()I
    .locals 2

    .prologue
    .line 2191857
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private g(II)V
    .locals 5

    .prologue
    .line 2191851
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    add-int/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    sub-int/2addr v0, v1

    .line 2191852
    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->e()I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->e()I

    move-result v2

    add-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    .line 2191853
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v3, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g()I

    move-result v3

    div-int/lit8 v3, v3, 0x3

    sub-int/2addr v2, v3

    .line 2191854
    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f()I

    move-result v3

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f()I

    move-result v4

    add-int/2addr v4, p2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g()I

    move-result v4

    div-int/lit8 v4, v4, 0x3

    add-int/2addr v3, v4

    .line 2191855
    iget-object v4, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-virtual {v4, v0, v2, v1, v3}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->invalidate(IIII)V

    .line 2191856
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2191850
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b:LX/F27;

    invoke-virtual {v0}, LX/F27;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 2191848
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 2191849
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2191845
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b:LX/F27;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, LX/F27;->a(II)V

    .line 2191846
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b:LX/F27;

    invoke-virtual {v0, p1}, LX/F27;->draw(Landroid/graphics/Canvas;)V

    .line 2191847
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2191844
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b:LX/F27;

    invoke-virtual {v0}, LX/F27;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 2191814
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->offset(II)V

    .line 2191815
    return-void
.end method

.method public final b(J)V
    .locals 7

    .prologue
    .line 2191827
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a:LX/F1P;

    sget-object v1, LX/F1P;->DURING_INITIAL_ANIMATION:LX/F1P;

    if-ne v0, v1, :cond_0

    .line 2191828
    iget-wide v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->c:J

    sub-long v0, p1, v0

    .line 2191829
    long-to-double v0, v0

    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v0, v2

    .line 2191830
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget-object v2, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->B:Landroid/view/animation/Interpolator;

    const-wide v4, 0x3fe999999999999aL    # 0.8

    div-double/2addr v0, v4

    double-to-float v0, v0

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 2191831
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->d:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->d:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 2191832
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->e:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->e:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 2191833
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget v2, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->h:I

    iput v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->h:I

    .line 2191834
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->k:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget v2, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->i:I

    iput v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->i:I

    .line 2191835
    :goto_0
    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->h:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2191836
    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->h:I

    neg-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2191837
    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->i:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2191838
    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->i:I

    neg-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2191839
    invoke-direct {p0, v1, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g(II)V

    .line 2191840
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Point;->offset(II)V

    .line 2191841
    return-void

    .line 2191842
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int v1, v0, v1

    .line 2191843
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 2191824
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 2191825
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 2191826
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2191823
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->g:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(II)V
    .locals 0

    .prologue
    .line 2191820
    iput p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->h:I

    .line 2191821
    iput p2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->i:I

    .line 2191822
    return-void
.end method

.method public final e(II)V
    .locals 0

    .prologue
    .line 2191817
    iput p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->d:I

    .line 2191818
    iput p2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->e:I

    .line 2191819
    return-void
.end method

.method public final f(II)Z
    .locals 1

    .prologue
    .line 2191816
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-ge v0, p1, :cond_0

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->e()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-ge v0, p2, :cond_0

    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f()I

    move-result v0

    if-le v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
