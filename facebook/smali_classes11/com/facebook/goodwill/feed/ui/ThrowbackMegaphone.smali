.class public Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Landroid/widget/ImageButton;

.field private e:Lcom/facebook/resources/ui/FbButton;

.field private f:LX/1Fb;

.field public g:LX/F1i;

.field private final h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2192612
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2192613
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2192610
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2192611
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2192633
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2192634
    new-instance v0, LX/F1u;

    invoke-direct {v0, p0}, LX/F1u;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->h:Landroid/view/View$OnClickListener;

    .line 2192635
    const v0, 0x7f0314b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2192636
    const v0, 0x7f0d2ef6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192637
    const v0, 0x7f0d2ef3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2192638
    const v0, 0x7f0d2ef4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2192639
    const v0, 0x7f0d2ef7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->d:Landroid/widget/ImageButton;

    .line 2192640
    const v0, 0x7f0d2ef5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2192641
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->d:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2192642
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 2192628
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2192629
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2192630
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2192631
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2192632
    return-void
.end method

.method public setImage(LX/1Fb;)V
    .locals 4

    .prologue
    .line 2192624
    iput-object p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->f:LX/1Fb;

    .line 2192625
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->f:LX/1Fb;

    invoke-static {v1}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v1

    const-class v2, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    const-string v3, "goodwill_throwback"

    invoke-static {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2192626
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2192627
    return-void
.end method

.method public setOnDismissListener(LX/F1i;)V
    .locals 0

    .prologue
    .line 2192622
    iput-object p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->g:LX/F1i;

    .line 2192623
    return-void
.end method

.method public setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2192620
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2192621
    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2192618
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2192619
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2192616
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192617
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2192614
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192615
    return-void
.end method
