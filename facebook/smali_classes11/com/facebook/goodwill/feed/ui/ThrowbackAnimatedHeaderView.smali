.class public Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private A:Landroid/os/Handler;

.field public B:Landroid/view/animation/Interpolator;

.field public a:LX/0wY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field private final l:I

.field private final m:I

.field private final n:D

.field private final o:I

.field private final p:I

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field public final s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

.field private final u:Landroid/graphics/PointF;

.field private final v:Ljava/util/Random;

.field private w:I

.field public final x:LX/0wa;

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 2191982
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2191983
    iput v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->d:I

    .line 2191984
    const/16 v0, 0x1e

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->e:I

    .line 2191985
    const/16 v0, 0xa

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->f:I

    .line 2191986
    const/16 v0, 0x2d

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->g:I

    .line 2191987
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->h:I

    .line 2191988
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->i:I

    .line 2191989
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->j:I

    .line 2191990
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b20cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->k:I

    .line 2191991
    const/16 v0, 0x64

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->l:I

    .line 2191992
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->m:I

    .line 2191993
    const-wide v0, 0x3fe999999999999aL    # 0.8

    iput-wide v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->n:D

    .line 2191994
    const/4 v0, 0x4

    iput v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->o:I

    .line 2191995
    iput v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->p:I

    .line 2191996
    const-string v0, "LEFT"

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->q:Ljava/lang/String;

    .line 2191997
    const-string v0, "RIGHT"

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->r:Ljava/lang/String;

    .line 2191998
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    .line 2191999
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2192000
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->u:Landroid/graphics/PointF;

    .line 2192001
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->v:Ljava/util/Random;

    .line 2192002
    new-instance v0, LX/F1Q;

    invoke-direct {v0, p0}, LX/F1Q;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;)V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->x:LX/0wa;

    .line 2192003
    const-class v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2192004
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->setWillNotDraw(Z)V

    .line 2192005
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->A:Landroid/os/Handler;

    .line 2192006
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v2, v2, v2, v0}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->B:Landroid/view/animation/Interpolator;

    .line 2192007
    return-void
.end method

.method public static synthetic a(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;II)I
    .locals 1

    .prologue
    .line 2192008
    invoke-direct {p0, p1, p2}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(II)I

    move-result v0

    return v0
.end method

.method private a(II)Landroid/graphics/Point;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2192009
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2192010
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->w:I

    sub-int/2addr v0, v1

    sub-int/2addr v0, p1

    invoke-direct {p0, v2, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(II)I

    move-result v0

    .line 2192011
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getHeight()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-direct {p0, v2, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(II)I

    move-result v1

    .line 2192012
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2

    .line 2192013
    :cond_0
    iget v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->w:I

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getWidth()I

    move-result v1

    sub-int/2addr v1, p1

    invoke-direct {p0, v0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(IIILjava/lang/String;)Landroid/graphics/Point;
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 2192036
    const-string v0, "LEFT"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2192037
    div-int/lit8 v0, p1, 0x2

    rsub-int/lit8 v0, v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->w:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    sub-int/2addr v1, p1

    invoke-direct {p0, v0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(II)I

    move-result v0

    .line 2192038
    :goto_0
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2192039
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b20c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 2192040
    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    int-to-double v2, v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 2192041
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getHeight()I

    move-result v2

    div-int/lit8 v3, p2, 0x2

    sub-int/2addr v2, v3

    sub-int v1, v2, v1

    .line 2192042
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {p0, v2, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(II)I

    move-result v1

    .line 2192043
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2

    .line 2192044
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->w:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getWidth()I

    move-result v1

    div-int/lit8 v2, p1, 0x2

    sub-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(II)I

    move-result v0

    goto :goto_0
.end method

.method private a()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2192014
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 2192015
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2192016
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2192017
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-ge v2, v1, :cond_0

    .line 2192018
    const-string v1, "LEFT"

    move-object v4, v1

    .line 2192019
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b()I

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a()I

    move-result v5

    iget v6, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->j:I

    invoke-direct {p0, v1, v5, v6, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(IIILjava/lang/String;)Landroid/graphics/Point;

    move-result-object v8

    .line 2192020
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    move v10, v3

    move v6, v3

    .line 2192021
    :goto_2
    const/16 v1, 0x14

    if-ge v10, v1, :cond_2

    .line 2192022
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b()I

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a()I

    move-result v5

    iget v7, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->j:I

    invoke-direct {p0, v1, v5, v7, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(IIILjava/lang/String;)Landroid/graphics/Point;

    move-result-object v7

    .line 2192023
    const v5, 0x7fffffff

    .line 2192024
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v9, v3

    :goto_3
    if-ge v9, v12, :cond_1

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    .line 2192025
    iget v13, v1, Landroid/graphics/Point;->x:I

    iget v14, v7, Landroid/graphics/Point;->x:I

    sub-int/2addr v13, v14

    .line 2192026
    iget v1, v1, Landroid/graphics/Point;->y:I

    iget v14, v7, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v14

    .line 2192027
    mul-int/2addr v13, v13

    mul-int/2addr v1, v1

    add-int/2addr v1, v13

    .line 2192028
    if-ge v1, v5, :cond_5

    .line 2192029
    :goto_4
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move v5, v1

    goto :goto_3

    .line 2192030
    :cond_0
    const-string v1, "RIGHT"

    move-object v4, v1

    goto :goto_1

    .line 2192031
    :cond_1
    if-ge v6, v5, :cond_4

    move-object v1, v7

    .line 2192032
    :goto_5
    add-int/lit8 v6, v10, 0x1

    move v10, v6

    move-object v8, v1

    move v6, v5

    goto :goto_2

    .line 2192033
    :cond_2
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2192034
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2192035
    :cond_3
    return-object v11

    :cond_4
    move v5, v6

    move-object v1, v8

    goto :goto_5

    :cond_5
    move v1, v5

    goto :goto_4
.end method

.method private static a(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;LX/0wY;LX/1HI;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 2192045
    iput-object p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a:LX/0wY;

    iput-object p2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b:LX/1HI;

    iput-object p3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->c:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-static {v2}, LX/0wX;->a(LX/0QB;)LX/0wX;

    move-result-object v0

    check-cast v0, LX/0wY;

    invoke-static {v2}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v1

    check-cast v1, LX/1HI;

    invoke-static {v2}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;LX/0wY;LX/1HI;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private b(II)I
    .locals 2

    .prologue
    .line 2191981
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->v:Ljava/util/Random;

    sub-int v1, p2, p1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method private b(IIILjava/lang/String;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 2191946
    iget-boolean v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->z:Z

    if-eqz v0, :cond_0

    .line 2191947
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(IIILjava/lang/String;)Landroid/graphics/Point;

    move-result-object v0

    .line 2191948
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(II)Landroid/graphics/Point;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;)V
    .locals 2

    .prologue
    .line 2191942
    iget-boolean v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->y:Z

    if-nez v0, :cond_0

    .line 2191943
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a:LX/0wY;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->x:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 2191944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->y:Z

    .line 2191945
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;II)I
    .locals 3

    .prologue
    .line 2191938
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 2191939
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2191940
    iget-object v2, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->v:Ljava/util/Random;

    sub-int/2addr v1, v0

    invoke-virtual {v2, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 2191941
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->v:Ljava/util/Random;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-nez v1, :cond_0

    neg-int v0, v0

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 2191923
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getHeight()I

    move-result v1

    if-gtz v1, :cond_1

    .line 2191924
    :cond_0
    return-void

    .line 2191925
    :cond_1
    iput p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->w:I

    .line 2191926
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->getWidth()I

    move-result v5

    .line 2191927
    invoke-direct {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2191928
    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v0

    move v4, v0

    :goto_0
    if-ge v3, v7, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2191929
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    .line 2191930
    iget v2, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v2, v8

    .line 2191931
    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v9, v1, Landroid/graphics/Point;->x:I

    div-int/lit8 v10, v5, 0x2

    if-le v9, v10, :cond_2

    neg-int v2, v2

    :cond_2
    add-int/2addr v2, v8

    .line 2191932
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a()I

    move-result v8

    mul-int/lit8 v8, v8, -0x2

    invoke-virtual {v0, v2, v8}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->c(II)V

    .line 2191933
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a()I

    move-result v8

    mul-int/lit8 v8, v8, -0x2

    invoke-virtual {v0, v2, v8}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->e(II)V

    .line 2191934
    new-instance v2, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$DelayedAnimationRunnable;

    iget v8, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-direct {v2, p0, v0, v8, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$DelayedAnimationRunnable;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;II)V

    .line 2191935
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->A:Landroid/os/Handler;

    mul-int/lit8 v1, v4, 0x64

    int-to-long v8, v1

    const v1, -0x2fb2e0e5

    invoke-static {v0, v2, v8, v9, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2191936
    add-int/lit8 v1, v4, 0x1

    .line 2191937
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v1

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2191949
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2191950
    invoke-virtual {v0, p1}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a(Landroid/graphics/Canvas;)V

    .line 2191951
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2191952
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x2

    const v1, 0x651d170c

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2191953
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 2191954
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 2191955
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2191956
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    if-eqz v0, :cond_0

    .line 2191957
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->u:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    sub-int v1, v3, v1

    iget-object v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->u:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    sub-int v3, v4, v3

    invoke-virtual {v0, v1, v3}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b(II)V

    .line 2191958
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2191959
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->u:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 2191960
    invoke-static {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->b(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;)V

    .line 2191961
    const v0, 0x63e3bc6b

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return v6

    .line 2191962
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    .line 2191963
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2191964
    invoke-virtual {v0, v3, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->f(II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2191965
    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2191966
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    iget v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->h:I

    iget v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->i:I

    invoke-virtual {v0, v1, v3}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->d(II)V

    .line 2191967
    :cond_1
    invoke-virtual {p0, v6}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 2191968
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 2191969
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    if-eqz v0, :cond_0

    .line 2191970
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->t:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    iget-object v1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->u:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    sub-int v1, v3, v1

    iget-object v3, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->u:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    sub-int v3, v4, v3

    invoke-virtual {v0, v1, v3}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setImageURLs(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x7

    .line 2191971
    iget-boolean v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->z:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    move v1, v0

    .line 2191972
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2191973
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v2, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 2191974
    iget-object v4, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    new-instance v5, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-boolean v6, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->z:Z

    invoke-direct {v5, p0, v0, v6}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;Ljava/lang/String;Z)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2191975
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v1, v2

    .line 2191976
    goto :goto_0

    .line 2191977
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    new-instance v1, LX/F1N;

    invoke-direct {v1, p0}, LX/F1N;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2191978
    return-void
.end method

.method public setUseIPBStyle(Z)V
    .locals 0

    .prologue
    .line 2191979
    iput-boolean p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->z:Z

    .line 2191980
    return-void
.end method
