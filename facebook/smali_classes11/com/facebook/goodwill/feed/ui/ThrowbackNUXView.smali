.class public Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/View;

.field public c:Landroid/widget/Button;

.field public d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2192703
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2192704
    invoke-direct {p0, p1}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a(Landroid/content/Context;)V

    .line 2192705
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2192700
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2192701
    invoke-direct {p0, p1}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a(Landroid/content/Context;)V

    .line 2192702
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2192697
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2192698
    invoke-direct {p0, p1}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a(Landroid/content/Context;)V

    .line 2192699
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2192685
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->removeAllViews()V

    .line 2192686
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2192687
    const v1, 0x7f0314b2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2192688
    const v0, 0x7f0d2efb

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a:Landroid/view/View;

    .line 2192689
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2192690
    const v0, 0x7f0d2efa

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    .line 2192691
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2192692
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    const v1, 0x7f0d2ef9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->c:Landroid/widget/Button;

    .line 2192693
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    const v1, 0x7f0d2ef8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192694
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2192695
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    const v1, 0x7f0d2b99

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2192696
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2192706
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2192707
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2192708
    return-void
.end method

.method public final a(Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2192666
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a:Landroid/view/View;

    move-object v1, v0

    .line 2192667
    :goto_0
    const v0, 0x7f0d2ef8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192668
    iget-object v2, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->b:LX/1Fb;

    move-object v2, v2

    .line 2192669
    invoke-static {v2}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "goodwill_throwback"

    invoke-static {v3, v4}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2192670
    const v0, 0x7f0d02c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2192671
    iget-object v2, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->g:Ljava/lang/String;

    move-object v2, v2

    .line 2192672
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192673
    const v0, 0x7f0d0626

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2192674
    iget-object v2, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->i:Ljava/lang/String;

    move-object v2, v2

    .line 2192675
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2192676
    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->e:Z

    if-eqz v0, :cond_0

    .line 2192677
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->c:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2192678
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->c:Landroid/widget/Button;

    .line 2192679
    iget-object v2, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->j:Ljava/lang/String;

    move-object v2, v2

    .line 2192680
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2192681
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->c:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 2192682
    :cond_0
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2192683
    return-void

    .line 2192684
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    move-object v1, v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;Z)V
    .locals 5

    .prologue
    .line 2192654
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a:Landroid/view/View;

    move-object v1, v0

    .line 2192655
    :goto_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    .line 2192656
    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a(Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;Z)V

    .line 2192657
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0400f9

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 2192658
    invoke-virtual {p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040044

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 2192659
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2192660
    new-instance v4, LX/F1v;

    invoke-direct {v4, p0, v0, p2}, LX/F1v;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;Landroid/view/View;Z)V

    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2192661
    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2192662
    invoke-virtual {v1, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2192663
    return-void

    .line 2192664
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b:Landroid/view/View;

    move-object v1, v0

    goto :goto_0

    .line 2192665
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a:Landroid/view/View;

    goto :goto_1
.end method

.method public setNotificationButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2192652
    iget-object v0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2192653
    return-void
.end method

.method public setUserCanViewPermalink(Z)V
    .locals 0

    .prologue
    .line 2192650
    iput-boolean p1, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->e:Z

    .line 2192651
    return-void
.end method
