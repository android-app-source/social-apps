.class public Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:LX/1Fb;

.field public final c:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

.field public final d:LX/1Fb;

.field public final e:Ljava/lang/String;

.field public final f:LX/1Fb;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/lang/String;

.field public final o:LX/1Fb;

.field public final p:Ljava/lang/String;

.field public final q:LX/1Fb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2188286
    new-instance v0, LX/F0D;

    invoke-direct {v0}, LX/F0D;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLX/1Fb;Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;LX/1Fb;Ljava/lang/String;LX/1Fb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/1Fb;Ljava/lang/String;LX/1Fb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/1Fb;",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;",
            "LX/1Fb;",
            "Ljava/lang/String;",
            "LX/1Fb;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/1Fb;",
            "Ljava/lang/String;",
            "LX/1Fb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2188267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2188268
    iput-wide p1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->a:J

    .line 2188269
    iput-object p3, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->b:LX/1Fb;

    .line 2188270
    iput-object p4, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->c:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    .line 2188271
    iput-object p5, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->d:LX/1Fb;

    .line 2188272
    iput-object p6, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->e:Ljava/lang/String;

    .line 2188273
    iput-object p7, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->f:LX/1Fb;

    .line 2188274
    iput-object p8, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->g:Ljava/lang/String;

    .line 2188275
    iput-object p9, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->h:Ljava/lang/String;

    .line 2188276
    iput-object p10, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->i:Ljava/lang/String;

    .line 2188277
    iput-object p11, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->j:Ljava/lang/String;

    .line 2188278
    iput-object p12, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->k:Ljava/lang/String;

    .line 2188279
    iput-object p13, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->l:Ljava/lang/String;

    .line 2188280
    iput-object p14, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    .line 2188281
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->n:Ljava/lang/String;

    .line 2188282
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->o:LX/1Fb;

    .line 2188283
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->p:Ljava/lang/String;

    .line 2188284
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->q:LX/1Fb;

    .line 2188285
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2188287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2188288
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->a:J

    .line 2188289
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/1Fb;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->b:LX/1Fb;

    .line 2188290
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->c:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    .line 2188291
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/1Fb;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->d:LX/1Fb;

    .line 2188292
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->e:Ljava/lang/String;

    .line 2188293
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/1Fb;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->f:LX/1Fb;

    .line 2188294
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->g:Ljava/lang/String;

    .line 2188295
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->h:Ljava/lang/String;

    .line 2188296
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->i:Ljava/lang/String;

    .line 2188297
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->j:Ljava/lang/String;

    .line 2188298
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->k:Ljava/lang/String;

    .line 2188299
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->l:Ljava/lang/String;

    .line 2188300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2188301
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2188302
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    .line 2188303
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->n:Ljava/lang/String;

    .line 2188304
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/1Fb;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->o:LX/1Fb;

    .line 2188305
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->p:Ljava/lang/String;

    .line 2188306
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/1Fb;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->q:LX/1Fb;

    .line 2188307
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2188266
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2188248
    iget-wide v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2188249
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->b:LX/1Fb;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2188250
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->c:Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2188251
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->d:LX/1Fb;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2188252
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188253
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->f:LX/1Fb;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2188254
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188255
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188256
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188257
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188258
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188259
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188260
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->m:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2188261
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188262
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->o:LX/1Fb;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2188263
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2188264
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->q:LX/1Fb;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2188265
    return-void
.end method
