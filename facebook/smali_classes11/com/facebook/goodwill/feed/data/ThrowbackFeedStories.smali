.class public Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

.field public final e:LX/0us;

.field public final f:LX/0ta;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2188371
    new-instance v0, LX/F0E;

    invoke-direct {v0}, LX/F0E;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/0Px;LX/0Px;LX/0P1;Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;LX/0us;LX/0ta;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;",
            ">;",
            "Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;",
            "LX/0us;",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2188334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2188335
    iput-object p1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->a:LX/0Px;

    .line 2188336
    iput-object p2, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->b:LX/0Px;

    .line 2188337
    iput-object p3, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->c:LX/0P1;

    .line 2188338
    iput-object p4, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    .line 2188339
    iput-object p5, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->e:LX/0us;

    .line 2188340
    iput-object p6, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->f:LX/0ta;

    .line 2188341
    return-void
.end method

.method public synthetic constructor <init>(LX/0Px;LX/0Px;LX/0P1;Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;LX/0us;LX/0ta;B)V
    .locals 0

    .prologue
    .line 2188370
    invoke-direct/range {p0 .. p6}, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;-><init>(LX/0Px;LX/0Px;LX/0P1;Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;LX/0us;LX/0ta;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2188350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2188351
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2188352
    if-nez v0, :cond_0

    .line 2188353
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2188354
    :goto_0
    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->a:LX/0Px;

    .line 2188355
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2188356
    if-nez v0, :cond_1

    .line 2188357
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2188358
    :goto_1
    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->b:LX/0Px;

    .line 2188359
    invoke-static {p1}, LX/4By;->c(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 2188360
    if-nez v0, :cond_2

    .line 2188361
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2188362
    :goto_2
    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->c:LX/0P1;

    .line 2188363
    const-class v0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    .line 2188364
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/0us;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->e:LX/0us;

    .line 2188365
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0ta;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->f:LX/0ta;

    .line 2188366
    return-void

    .line 2188367
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2188368
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 2188369
    :cond_2
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2188349
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2188342
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->a:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2188343
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2188344
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->c:LX/0P1;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 2188345
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2188346
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->e:LX/0us;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2188347
    iget-object v0, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->f:LX/0ta;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2188348
    return-void
.end method
