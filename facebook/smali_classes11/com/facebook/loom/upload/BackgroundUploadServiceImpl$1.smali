.class public final Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/02p;

.field public final synthetic c:Z

.field public final synthetic d:LX/2Lq;


# direct methods
.method public constructor <init>(LX/2Lq;Ljava/io/File;LX/02p;Z)V
    .locals 0

    .prologue
    .line 2411836
    iput-object p1, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->d:LX/2Lq;

    iput-object p2, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->b:LX/02p;

    iput-boolean p4, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 2411837
    iget-object v0, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2411838
    :goto_0
    return-void

    .line 2411839
    :cond_0
    iget-object v0, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->d:LX/2Lq;

    iget-object v1, v0, LX/2Lq;->f:Ljava/util/Set;

    monitor-enter v1

    .line 2411840
    :try_start_0
    iget-object v0, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->d:LX/2Lq;

    iget-object v0, v0, LX/2Lq;->f:Ljava/util/Set;

    iget-object v2, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->a:Ljava/io/File;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2411841
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2411842
    iget-object v0, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->d:LX/2Lq;

    iget-object v1, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->a:Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->b:LX/02p;

    iget-boolean v3, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->c:Z

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2411843
    if-nez v3, :cond_1

    iget-object v4, v0, LX/2Lq;->c:LX/2Lr;

    invoke-virtual {v4}, LX/2Lr;->a()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2411844
    :goto_1
    iget-object v0, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->d:LX/2Lq;

    iget-object v1, v0, LX/2Lq;->f:Ljava/util/Set;

    monitor-enter v1

    .line 2411845
    :try_start_1
    iget-object v0, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->d:LX/2Lq;

    iget-object v0, v0, LX/2Lq;->f:Ljava/util/Set;

    iget-object v2, p0, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;->a:Ljava/io/File;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2411846
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2411847
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 2411848
    :cond_1
    new-instance v4, LX/14U;

    invoke-direct {v4}, LX/14U;-><init>()V

    .line 2411849
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2411850
    iput-object v5, v4, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2411851
    const-class v5, LX/2Lq;

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    .line 2411852
    :try_start_3
    iget-object v6, v0, LX/2Lq;->d:LX/11H;

    iget-object v7, v0, LX/2Lq;->e:LX/2Ls;

    invoke-virtual {v6, v7, v1, v4, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    .line 2411853
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2411854
    if-nez v3, :cond_2

    .line 2411855
    iget-object v4, v0, LX/2Lq;->c:LX/2Lr;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 2411856
    neg-long v11, v6

    invoke-static {v4, v11, v12}, LX/2Lr;->b(LX/2Lr;J)V

    .line 2411857
    :cond_2
    if-eqz v2, :cond_3

    .line 2411858
    iget-object v4, v0, LX/2Lq;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$2;

    invoke-direct {v5, v0, v2, v1}, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$2;-><init>(LX/2Lq;LX/02p;Ljava/io/File;)V

    const v6, -0x27137d29

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 2411859
    :cond_3
    goto :goto_1

    .line 2411860
    :catch_0
    move-exception v4

    .line 2411861
    const-string v5, "BackgroundUploadServiceImpl"

    const-string v6, "Upload failed for trace %s"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v4, v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2411862
    invoke-static {v0, v1, v2}, LX/2Lq;->b(LX/2Lq;Ljava/io/File;LX/02p;)V

    goto :goto_1

    .line 2411863
    :cond_4
    :try_start_4
    const-string v4, "BackgroundUploadServiceImpl"

    const-string v5, "Upload failed for trace %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2411864
    invoke-static {v0, v1, v2}, LX/2Lq;->b(LX/2Lq;Ljava/io/File;LX/02p;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method
