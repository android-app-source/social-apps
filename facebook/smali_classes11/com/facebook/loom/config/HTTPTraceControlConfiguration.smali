.class public Lcom/facebook/loom/config/HTTPTraceControlConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pm;
.implements LX/0Pk;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/loom/config/HTTPTraceControlConfigurationDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/loom/config/HTTPTraceControlConfiguration;",
        ">;",
        "LX/0Pk;"
    }
.end annotation


# instance fields
.field private a:I

.field private final mEnabledEventProviders:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "enabled_event_providers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2411667
    const-class v0, Lcom/facebook/loom/config/HTTPTraceControlConfigurationDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2411664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411665
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/config/HTTPTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    .line 2411666
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2411662
    iget-object v0, p0, Lcom/facebook/loom/config/HTTPTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    invoke-static {v0}, Lcom/facebook/loom/config/LoomConfiguration;->a(Ljava/util/List;)I

    move-result v0

    iput v0, p0, Lcom/facebook/loom/config/HTTPTraceControlConfiguration;->a:I

    .line 2411663
    return-object p0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2411661
    iget v0, p0, Lcom/facebook/loom/config/HTTPTraceControlConfiguration;->a:I

    return v0
.end method
