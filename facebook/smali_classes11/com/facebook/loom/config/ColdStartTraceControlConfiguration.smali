.class public Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pm;
.implements LX/0Pk;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/loom/config/ColdStartTraceControlConfigurationDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;",
        ">;",
        "LX/0Pk;"
    }
.end annotation


# instance fields
.field private a:I

.field private final mCoinflipSampleRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "coinflip_sample_rate"
    .end annotation
.end field

.field private mCpuSamplingRateMs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cpu_sampling_rate_ms"
    .end annotation
.end field

.field private final mEnabledEventProviders:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "enabled_event_providers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mStopQPLMarker:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stop_qpl_marker"
    .end annotation
.end field

.field private mTimedOutUploadSampleRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timed_out_upload_sample_rate"
    .end annotation
.end field

.field private final mTraceTimeoutMs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_trace_timeout_ms"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2411633
    const-class v0, Lcom/facebook/loom/config/ColdStartTraceControlConfigurationDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2411626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411627
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    .line 2411628
    iput v1, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mCoinflipSampleRate:I

    .line 2411629
    iput v1, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mStopQPLMarker:I

    .line 2411630
    iput v1, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mTraceTimeoutMs:I

    .line 2411631
    iput v1, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mTimedOutUploadSampleRate:I

    .line 2411632
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2411624
    iget-object v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    invoke-static {v0}, Lcom/facebook/loom/config/LoomConfiguration;->a(Ljava/util/List;)I

    move-result v0

    iput v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->a:I

    .line 2411625
    return-object p0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2411623
    iget v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->a:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2411634
    iget v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mStopQPLMarker:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2411622
    iget v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mCoinflipSampleRate:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2411621
    iget v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mTraceTimeoutMs:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2411620
    iget v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mTimedOutUploadSampleRate:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2411619
    iget v0, p0, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->mCpuSamplingRateMs:I

    return v0
.end method
