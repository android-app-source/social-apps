.class public Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field public c:I

.field private d:Landroid/graphics/Paint;

.field public e:Landroid/graphics/Paint;

.field public f:Landroid/graphics/Paint;

.field private g:I

.field public h:F

.field public i:Landroid/animation/ValueAnimator;

.field public j:Landroid/animation/ValueAnimator;

.field public k:F

.field public l:F

.field private m:F

.field public n:I

.field private o:I

.field private p:I

.field private q:Z

.field private r:Z

.field private final s:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2319242
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2319243
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a:I

    .line 2319244
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b:I

    .line 2319245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->r:Z

    .line 2319246
    new-instance v0, LX/G5g;

    invoke-direct {v0, p0}, LX/G5g;-><init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->s:Landroid/view/animation/Animation$AnimationListener;

    .line 2319247
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a(Landroid/util/AttributeSet;)V

    .line 2319248
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a()V

    .line 2319249
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2319234
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2319235
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a:I

    .line 2319236
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b:I

    .line 2319237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->r:Z

    .line 2319238
    new-instance v0, LX/G5g;

    invoke-direct {v0, p0}, LX/G5g;-><init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->s:Landroid/view/animation/Animation$AnimationListener;

    .line 2319239
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a(Landroid/util/AttributeSet;)V

    .line 2319240
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a()V

    .line 2319241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2319226
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2319227
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a:I

    .line 2319228
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b:I

    .line 2319229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->r:Z

    .line 2319230
    new-instance v0, LX/G5g;

    invoke-direct {v0, p0}, LX/G5g;-><init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->s:Landroid/view/animation/Animation$AnimationListener;

    .line 2319231
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a(Landroid/util/AttributeSet;)V

    .line 2319232
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a()V

    .line 2319233
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 2319218
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 2319219
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a:I

    .line 2319220
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b:I

    .line 2319221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->r:Z

    .line 2319222
    new-instance v0, LX/G5g;

    invoke-direct {v0, p0}, LX/G5g;-><init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->s:Landroid/view/animation/Animation$AnimationListener;

    .line 2319223
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a(Landroid/util/AttributeSet;)V

    .line 2319224
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a()V

    .line 2319225
    return-void
.end method

.method public static synthetic a(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)I
    .locals 2

    .prologue
    .line 2319217
    iget v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    return v0
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2319201
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    .line 2319202
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->p:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2319203
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 2319204
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 2319205
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2319206
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->e:Landroid/graphics/Paint;

    .line 2319207
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->f:Landroid/graphics/Paint;

    .line 2319208
    new-array v0, v6, [I

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->o:I

    aput v1, v0, v4

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->p:I

    aput v1, v0, v5

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->i:Landroid/animation/ValueAnimator;

    .line 2319209
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->i:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2319210
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->i:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 2319211
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->i:Landroid/animation/ValueAnimator;

    new-instance v1, LX/G5h;

    invoke-direct {v1, p0}, LX/G5h;-><init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2319212
    new-array v0, v6, [I

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->p:I

    aput v1, v0, v4

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->o:I

    aput v1, v0, v5

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->j:Landroid/animation/ValueAnimator;

    .line 2319213
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->j:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->g:I

    mul-int/lit8 v1, v1, 0x2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2319214
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->j:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 2319215
    iget-object v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->j:Landroid/animation/ValueAnimator;

    new-instance v1, LX/G5i;

    invoke-direct {v1, p0}, LX/G5i;-><init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2319216
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/16 v1, 0x1f4

    const/4 v0, 0x3

    const/4 v2, 0x0

    .line 2319186
    if-eqz p1, :cond_0

    .line 2319187
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->DotProgressIndicator:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2319188
    :try_start_0
    const/16 v0, 0x3

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    .line 2319189
    const/16 v0, 0x2

    const/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->g:I

    .line 2319190
    const/16 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->o:I

    .line 2319191
    const/16 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->p:I

    .line 2319192
    const/16 v0, 0x4

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2319193
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2319194
    :goto_0
    return-void

    .line 2319195
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 2319196
    :cond_0
    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    .line 2319197
    iput v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->g:I

    .line 2319198
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->o:I

    .line 2319199
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->p:I

    .line 2319200
    iput-boolean v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->q:Z

    goto :goto_0
.end method

.method private b()V
    .locals 0

    .prologue
    .line 2319182
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a()V

    .line 2319183
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->requestLayout()V

    .line 2319184
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d()V

    .line 2319185
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2319178
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->clearAnimation()V

    .line 2319179
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->postInvalidate()V

    .line 2319180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->r:Z

    .line 2319181
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2319250
    new-instance v0, LX/G5j;

    invoke-direct {v0, p0}, LX/G5j;-><init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V

    .line 2319251
    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/G5j;->setDuration(J)V

    .line 2319252
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/G5j;->setRepeatCount(I)V

    .line 2319253
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/G5j;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2319254
    iget-object v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->s:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, LX/G5j;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2319255
    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2319256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->r:Z

    .line 2319257
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4d00f63b    # 1.35226288E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2319118
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 2319119
    const/16 v1, 0x2d

    const v2, 0x4416de90

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x167dc7a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2319138
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c()V

    .line 2319139
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 2319140
    const/16 v1, 0x2d

    const v2, 0x5e08755b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2319120
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2319121
    iget-boolean v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->r:Z

    if-nez v0, :cond_0

    .line 2319122
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d()V

    .line 2319123
    :cond_0
    const/4 v1, 0x0

    .line 2319124
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    if-ge v0, v2, :cond_7

    .line 2319125
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    add-float/2addr v2, v3

    .line 2319126
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getMeasuredHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    .line 2319127
    iget v4, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    iget v5, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    if-ne v4, v5, :cond_2

    .line 2319128
    add-float/2addr v2, v1

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    iget-object v5, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2319129
    :cond_1
    :goto_1
    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->m:F

    add-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 2319130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2319131
    :cond_2
    iget v4, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    if-ne v0, v4, :cond_4

    .line 2319132
    add-float v4, v2, v1

    int-to-float v3, v3

    iget-boolean v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->q:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    iget v5, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->h:F

    add-float/2addr v2, v5

    :goto_2
    iget-object v5, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v3, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_3
    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    goto :goto_2

    .line 2319133
    :cond_4
    iget v4, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_6

    .line 2319134
    add-float v4, v2, v1

    int-to-float v3, v3

    iget-boolean v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->q:Z

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->l:F

    iget v5, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->h:F

    sub-float/2addr v2, v5

    :goto_3
    iget-object v5, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v3, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    goto :goto_3

    .line 2319135
    :cond_6
    iget v4, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    if-ge v0, v4, :cond_1

    .line 2319136
    add-float/2addr v2, v1

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    iget-object v5, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 2319137
    :cond_7
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2319141
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2319142
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getMeasuredHeight()I

    move-result v0

    .line 2319143
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getMeasuredWidth()I

    move-result v1

    .line 2319144
    invoke-virtual {p0, v1, v0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->setMeasuredDimension(II)V

    .line 2319145
    if-le v0, v1, :cond_0

    .line 2319146
    iget v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    div-int v0, v1, v0

    div-int/lit8 v0, v0, 0x4

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    .line 2319147
    :goto_0
    iget v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->l:F

    .line 2319148
    iget v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    int-to-float v0, v0

    iget v2, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    mul-float/2addr v0, v2

    .line 2319149
    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    .line 2319150
    int-to-float v1, v1

    sub-float v0, v1, v0

    int-to-float v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->m:F

    .line 2319151
    return-void

    .line 2319152
    :cond_0
    div-int/lit8 v0, v0, 0x4

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->k:F

    goto :goto_0
.end method

.method public final onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2319153
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 2319154
    const/16 v0, 0x8

    if-eq p2, v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 2319155
    :cond_0
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c()V

    .line 2319156
    :cond_1
    return-void
.end method

.method public setAnimationTime(I)V
    .locals 0

    .prologue
    .line 2319157
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c()V

    .line 2319158
    iput p1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->g:I

    .line 2319159
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b()V

    .line 2319160
    return-void
.end method

.method public setBounceEnabled(Z)V
    .locals 0

    .prologue
    .line 2319161
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c()V

    .line 2319162
    iput-boolean p1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->q:Z

    .line 2319163
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b()V

    .line 2319164
    return-void
.end method

.method public setEndColor(I)V
    .locals 0

    .prologue
    .line 2319165
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c()V

    .line 2319166
    iput p1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->p:I

    .line 2319167
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b()V

    .line 2319168
    return-void
.end method

.method public setNumberOfDots(I)V
    .locals 1

    .prologue
    .line 2319169
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c()V

    .line 2319170
    iput p1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    .line 2319171
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    .line 2319172
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b()V

    .line 2319173
    return-void
.end method

.method public setStartColor(I)V
    .locals 0

    .prologue
    .line 2319174
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c()V

    .line 2319175
    iput p1, p0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->o:I

    .line 2319176
    invoke-direct {p0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->b()V

    .line 2319177
    return-void
.end method
