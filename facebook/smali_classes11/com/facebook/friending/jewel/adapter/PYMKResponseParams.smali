.class public Lcom/facebook/friending/jewel/adapter/PYMKResponseParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATER:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friending/jewel/adapter/PYMKResponseParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:LX/EzN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2187038
    new-instance v0, LX/EzM;

    invoke-direct {v0}, LX/EzM;-><init>()V

    sput-object v0, Lcom/facebook/friending/jewel/adapter/PYMKResponseParams;->CREATER:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2187039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187040
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friending/jewel/adapter/PYMKResponseParams;->a:J

    .line 2187041
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/EzN;->valueOf(Ljava/lang/String;)LX/EzN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/adapter/PYMKResponseParams;->b:LX/EzN;

    .line 2187042
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2187043
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2187044
    iget-wide v0, p0, Lcom/facebook/friending/jewel/adapter/PYMKResponseParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2187045
    iget-object v0, p0, Lcom/facebook/friending/jewel/adapter/PYMKResponseParams;->b:LX/EzN;

    invoke-virtual {v0}, LX/EzN;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2187046
    return-void
.end method
