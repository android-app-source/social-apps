.class public Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Ezh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private c:LX/1P0;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2187467
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2187464
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2187465
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;

    new-instance v1, LX/Ezh;

    invoke-static {v2}, LX/2iT;->b(LX/0QB;)LX/2iT;

    move-result-object v3

    check-cast v3, LX/2iT;

    new-instance v0, LX/Ezb;

    invoke-static {v2}, LX/Ezl;->a(LX/0QB;)LX/Ezl;

    move-result-object p1

    check-cast p1, LX/Ezl;

    invoke-direct {v0, p1}, LX/Ezb;-><init>(LX/Ezl;)V

    move-object p1, v0

    check-cast p1, LX/Ezb;

    invoke-static {v2}, LX/Ezl;->a(LX/0QB;)LX/Ezl;

    move-result-object v0

    check-cast v0, LX/Ezl;

    invoke-direct {v1, v3, p1, v0}, LX/Ezh;-><init>(LX/2iT;LX/Ezb;LX/Ezl;)V

    move-object v2, v1

    check-cast v2, LX/Ezh;

    iput-object v2, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->a:LX/Ezh;

    .line 2187466
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6234638e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2187463
    const v1, 0x7f030bfa

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x72ca89d1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x761483ba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2187451
    iget-object v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->a:LX/Ezh;

    iget-object v2, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2187452
    iget-object v5, v1, LX/Ezh;->d:LX/1OX;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 2187453
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2187454
    iget-object v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->a:LX/Ezh;

    .line 2187455
    iget-object v2, v1, LX/Ezh;->c:LX/Ezl;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, LX/Ezl;->a(Z)V

    .line 2187456
    iget-object v2, v1, LX/Ezh;->b:LX/Ezb;

    const/4 v5, 0x0

    .line 2187457
    iput-object v5, v2, LX/Ezb;->b:LX/Ezh;

    .line 2187458
    iput-object v5, v2, LX/Ezb;->c:LX/1DI;

    .line 2187459
    iput-object v4, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->c:LX/1P0;

    .line 2187460
    iput-object v4, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2187461
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2187462
    const/16 v1, 0x2b

    const v2, -0x434df573

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2187437
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2187438
    const v0, 0x7f0d1db7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2187439
    new-instance v0, LX/1P0;

    iget-object v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->c:LX/1P0;

    .line 2187440
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->c:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2187441
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 2187442
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->a:LX/Ezh;

    .line 2187443
    iget-object v1, v0, LX/Ezh;->c:LX/Ezl;

    invoke-virtual {v1, v0}, LX/Ezl;->a(LX/Ezg;)V

    .line 2187444
    iget-object v1, v0, LX/Ezh;->b:LX/Ezb;

    .line 2187445
    iput-object v0, v1, LX/Ezb;->b:LX/Ezh;

    .line 2187446
    iput-object v0, v1, LX/Ezb;->c:LX/1DI;

    .line 2187447
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->a:LX/Ezh;

    iget-object v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2187448
    iget-object p0, v0, LX/Ezh;->b:LX/Ezb;

    invoke-virtual {v1, p0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2187449
    iget-object p0, v0, LX/Ezh;->d:LX/1OX;

    invoke-virtual {v1, p0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2187450
    return-void
.end method
