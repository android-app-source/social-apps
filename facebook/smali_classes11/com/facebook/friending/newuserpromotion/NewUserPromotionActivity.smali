.class public Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private p:I

.field private q:Landroid/graphics/drawable/TransitionDrawable;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2187522
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2187523
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->t:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2187516
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->q:Landroid/graphics/drawable/TransitionDrawable;

    iget v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->p:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    .line 2187517
    const v0, 0x7f04007c

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2187518
    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2187519
    iget-object v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2187520
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->s:Landroid/view/View;

    const v1, 0x7f04007a

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2187521
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 2187511
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 2187512
    invoke-virtual {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 2187513
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v0, v1}, LX/470;->a(Landroid/view/Window;I)V

    .line 2187514
    return-void

    .line 2187515
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x500

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2187495
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2187496
    invoke-direct {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->b()V

    .line 2187497
    invoke-virtual {p0, v2, v2}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->overridePendingTransition(II)V

    .line 2187498
    const v0, 0x7f030bfb

    invoke-virtual {p0, v0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->setContentView(I)V

    .line 2187499
    invoke-virtual {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->p:I

    .line 2187500
    const v0, 0x7f0d1db8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->q:Landroid/graphics/drawable/TransitionDrawable;

    .line 2187501
    const v0, 0x7f0d1db9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->r:Landroid/view/View;

    .line 2187502
    const v0, 0x7f0d1dba

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->s:Landroid/view/View;

    .line 2187503
    const v0, 0x7f0d1dbb

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2187504
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2187505
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2187506
    const v1, 0x7f0d1db9

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2187507
    if-nez v1, :cond_0

    .line 2187508
    new-instance v1, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;

    invoke-direct {v1}, Lcom/facebook/friending/newuserpromotion/NewUserFriendingFragment;-><init>()V

    move-object v1, v1

    .line 2187509
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, LX/0hH;->a(II)LX/0hH;

    move-result-object v0

    const v2, 0x7f0d1db9

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2187510
    :cond_0
    return-void
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2187490
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2187491
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1db9

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2187492
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2187493
    invoke-virtual {p0, v3, v3}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->overridePendingTransition(II)V

    .line 2187494
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2187489
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2187488
    return-void
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 2187473
    invoke-direct {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->a()V

    .line 2187474
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x2b01f6c0

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2187485
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2187486
    invoke-direct {p0}, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->a()V

    .line 2187487
    const v1, 0x388b3867

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x36f9e915

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2187481
    iput-object v2, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->s:Landroid/view/View;

    .line 2187482
    iput-object v2, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->r:Landroid/view/View;

    .line 2187483
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2187484
    const/16 v1, 0x23

    const v2, 0x4b7b6c2d    # 1.6477229E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 2187475
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->t:Z

    if-nez v0, :cond_1

    .line 2187476
    :cond_0
    :goto_0
    return-void

    .line 2187477
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->q:Landroid/graphics/drawable/TransitionDrawable;

    iget v1, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->p:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2187478
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->r:Landroid/view/View;

    const v1, 0x7f04007b

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2187479
    iget-object v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->s:Landroid/view/View;

    const v1, 0x7f040079

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2187480
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/newuserpromotion/NewUserPromotionActivity;->t:Z

    goto :goto_0
.end method
