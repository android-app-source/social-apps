.class public final Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""

# interfaces
.implements Lcom/google/android/gms/common/internal/ReflectedParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Z

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/G7t;

    invoke-direct {v0}, LX/G7t;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IZZZ)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->a:I

    iput-boolean p2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->b:Z

    iput-boolean p3, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->c:Z

    iput-boolean p4, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->d:Z

    return-void
.end method

.method public constructor <init>(LX/G7S;)V
    .locals 4

    const/4 v0, 0x1

    iget-boolean v1, p1, LX/G7S;->a:Z

    iget-boolean v2, p1, LX/G7S;->b:Z

    iget-boolean v3, p1, LX/G7S;->c:Z

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;-><init>(IZZZ)V

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-boolean p2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->b:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x2

    iget-boolean p2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->c:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    iget-boolean p2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->d:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x3e8

    iget p2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->a:I

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void
.end method
