.class public LX/GGW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/widget/LinearLayout$LayoutParams;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/0pf;

.field private final c:LX/1CK;

.field public final d:LX/1DS;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2334321
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, LX/GGW;->a:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(LX/0pf;LX/1CK;LX/1DS;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pf;",
            "LX/1CK;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334323
    iput-object p1, p0, LX/GGW;->b:LX/0pf;

    .line 2334324
    iput-object p2, p0, LX/GGW;->c:LX/1CK;

    .line 2334325
    iput-object p3, p0, LX/GGW;->d:LX/1DS;

    .line 2334326
    iput-object p4, p0, LX/GGW;->e:LX/0Ot;

    .line 2334327
    return-void
.end method

.method public static a(LX/0QB;)LX/GGW;
    .locals 7

    .prologue
    .line 2334328
    const-class v1, LX/GGW;

    monitor-enter v1

    .line 2334329
    :try_start_0
    sget-object v0, LX/GGW;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2334330
    sput-object v2, LX/GGW;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2334331
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2334332
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2334333
    new-instance v6, LX/GGW;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v3

    check-cast v3, LX/0pf;

    invoke-static {v0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v4

    check-cast v4, LX/1CK;

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v5

    check-cast v5, LX/1DS;

    const/16 p0, 0x6bd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/GGW;-><init>(LX/0pf;LX/1CK;LX/1DS;LX/0Ot;)V

    .line 2334334
    move-object v0, v6

    .line 2334335
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2334336
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GGW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2334337
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2334338
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 2334339
    iget-object v1, p0, LX/GGW;->c:LX/1CK;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, p2, v6, v2}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V

    .line 2334340
    iget-object v1, p0, LX/GGW;->b:LX/0pf;

    invoke-virtual {v1, p2}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    .line 2334341
    iput-boolean v7, v1, LX/1g0;->o:Z

    .line 2334342
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2334343
    new-instance v2, Lcom/facebook/adinterfaces/nativepreview/AdInterfacesNativePreviewRenderer$1;

    invoke-direct {v2, p0}, Lcom/facebook/adinterfaces/nativepreview/AdInterfacesNativePreviewRenderer$1;-><init>(LX/GGW;)V

    .line 2334344
    new-instance v3, LX/GGU;

    new-instance v4, LX/GGT;

    invoke-direct {v4, p0}, LX/GGT;-><init>(LX/GGW;)V

    invoke-direct {v3, p0, v1, v2, v4}, LX/GGU;-><init>(LX/GGW;Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2334345
    new-instance v2, LX/1Cq;

    invoke-direct {v2}, LX/1Cq;-><init>()V

    .line 2334346
    iput-object p2, v2, LX/1Cq;->a:Ljava/lang/Object;

    .line 2334347
    iget-object v4, p0, LX/GGW;->d:LX/1DS;

    iget-object v5, p0, LX/GGW;->e:LX/0Ot;

    invoke-virtual {v4, v5, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v2

    .line 2334348
    iput-object v3, v2, LX/1Ql;->f:LX/1PW;

    .line 2334349
    move-object v2, v2

    .line 2334350
    invoke-virtual {v2}, LX/1Ql;->e()LX/1Qq;

    move-result-object v2

    move-object v2, v2

    .line 2334351
    new-instance v3, LX/GGV;

    invoke-direct {v3, v1, v0}, LX/GGV;-><init>(Landroid/content/Context;Z)V

    .line 2334352
    invoke-interface {v2}, LX/1Qq;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 2334353
    :goto_0
    if-ge v0, v4, :cond_0

    .line 2334354
    invoke-interface {v2, v0, v6, v3}, LX/1Qq;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 2334355
    invoke-virtual {v3, v5}, LX/GGV;->addView(Landroid/view/View;)V

    .line 2334356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2334357
    :cond_0
    invoke-interface {v2, v4, v6, v3}, LX/1Qq;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2334358
    if-eqz v0, :cond_1

    .line 2334359
    new-instance v2, LX/GGV;

    invoke-direct {v2, v1, v7}, LX/GGV;-><init>(Landroid/content/Context;Z)V

    .line 2334360
    invoke-virtual {v2, v0}, LX/GGV;->addView(Landroid/view/View;)V

    .line 2334361
    sget-object v0, LX/GGW;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2, v0}, LX/GGV;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2334362
    invoke-virtual {v3, v2}, LX/GGV;->addView(Landroid/view/View;)V

    .line 2334363
    :cond_1
    sget-object v0, LX/GGW;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v3, v0}, LX/GGV;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2334364
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2334365
    return-void
.end method
