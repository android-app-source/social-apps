.class public LX/GZP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Ljava/lang/String;

.field private final b:LX/7iP;

.field private final c:Landroid/content/Context;

.field private d:LX/7j6;

.field private final e:LX/7iO;

.field private final f:LX/0Zb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7j6;LX/0Zb;LX/7iO;LX/7iP;)V
    .locals 7

    .prologue
    .line 2366500
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/GZP;-><init>(Landroid/content/Context;LX/7j6;Ljava/lang/String;LX/0Zb;LX/7iO;LX/7iP;)V

    .line 2366501
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/7j6;Ljava/lang/String;LX/0Zb;LX/7iO;LX/7iP;)V
    .locals 0

    .prologue
    .line 2366502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2366503
    iput-object p1, p0, LX/GZP;->c:Landroid/content/Context;

    .line 2366504
    iput-object p2, p0, LX/GZP;->d:LX/7j6;

    .line 2366505
    iput-object p3, p0, LX/GZP;->a:Ljava/lang/String;

    .line 2366506
    iput-object p4, p0, LX/GZP;->f:LX/0Zb;

    .line 2366507
    iput-object p5, p0, LX/GZP;->e:LX/7iO;

    .line 2366508
    iput-object p6, p0, LX/GZP;->b:LX/7iP;

    .line 2366509
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x3590145

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366510
    iget-object v1, p0, LX/GZP;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2366511
    const v1, -0x799e4ad9

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2366512
    :goto_0
    return-void

    .line 2366513
    :cond_0
    iget-object v1, p0, LX/GZP;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, LX/GZP;->e:LX/7iO;

    invoke-static {v1, v2, v3}, LX/7iS;->b(Ljava/lang/String;Ljava/lang/Boolean;LX/7iO;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2366514
    iget-object v2, p0, LX/GZP;->f:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2366515
    iget-object v1, p0, LX/GZP;->d:LX/7j6;

    iget-object v2, p0, LX/GZP;->a:Ljava/lang/String;

    iget-object v3, p0, LX/GZP;->b:LX/7iP;

    invoke-virtual {v1, v2, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2366516
    const v1, 0x778b0b9c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
