.class public final LX/G1u;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2313144
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2313145
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2313146
    :goto_0
    return v1

    .line 2313147
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2313148
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2313149
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2313150
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2313151
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2313152
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2313153
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2313154
    :cond_2
    const-string v5, "profile_tile_items"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2313155
    invoke-static {p0, p1}, LX/G1k;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2313156
    :cond_3
    const-string v5, "view_mediaset"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2313157
    invoke-static {p0, p1}, LX/G1t;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2313158
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2313159
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2313160
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2313161
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2313162
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2313163
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2313164
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2313165
    if-eqz v0, :cond_0

    .line 2313166
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313167
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2313168
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2313169
    if-eqz v0, :cond_1

    .line 2313170
    const-string v1, "profile_tile_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313171
    invoke-static {p0, v0, p2, p3}, LX/G1k;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2313172
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2313173
    if-eqz v0, :cond_2

    .line 2313174
    const-string v1, "view_mediaset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313175
    invoke-static {p0, v0, p2}, LX/G1t;->a(LX/15i;ILX/0nX;)V

    .line 2313176
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2313177
    return-void
.end method
