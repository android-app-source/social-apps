.class public LX/Gb8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13F;
.implements LX/13G;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/10N;

.field private c:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/Gb5;

.field private final f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/10N;LX/0Or;LX/Gb5;Ljava/lang/Boolean;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/10N;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/Gb5;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369121
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gb8;->c:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    .line 2369122
    iput-object p1, p0, LX/Gb8;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2369123
    iput-object p2, p0, LX/Gb8;->b:LX/10N;

    .line 2369124
    iput-object p3, p0, LX/Gb8;->d:LX/0Or;

    .line 2369125
    iput-object p4, p0, LX/Gb8;->e:LX/Gb5;

    .line 2369126
    iput-object p5, p0, LX/Gb8;->f:Ljava/lang/Boolean;

    .line 2369127
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2369170
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369169
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 12

    .prologue
    .line 2369136
    iget-object v0, p0, LX/Gb8;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Gb8;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gb8;->c:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gb8;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gb8;->e:LX/Gb5;

    const-wide/16 v10, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2369137
    iget-object v2, v0, LX/Gb5;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2369138
    if-nez v2, :cond_6

    move v2, v4

    .line 2369139
    :goto_0
    move v0, v2

    .line 2369140
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gb8;->e:LX/Gb5;

    .line 2369141
    iget-object v1, v0, LX/Gb5;->d:LX/10N;

    invoke-virtual {v1}, LX/10N;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, v0, LX/Gb5;->e:LX/10M;

    iget-object v1, v0, LX/Gb5;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, LX/10M;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/Gb5;->d:LX/10N;

    invoke-virtual {v1}, LX/10N;->j()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2369142
    if-nez v0, :cond_2

    .line 2369143
    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 2369144
    :goto_2
    return-object v0

    .line 2369145
    :cond_2
    iget-object v0, p0, LX/Gb8;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2369146
    if-eqz v0, :cond_5

    .line 2369147
    sget-object v1, LX/26p;->k:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2369148
    iget-object v1, p0, LX/Gb8;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2369149
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_2

    .line 2369150
    :cond_3
    iget-object v0, p0, LX/Gb8;->e:LX/Gb5;

    iget-object v1, p0, LX/Gb8;->c:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;->targetedNux:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2369151
    iget-object v2, v0, LX/Gb5;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2369152
    if-eqz v2, :cond_4

    if-nez v1, :cond_a

    :cond_4
    move v2, v5

    .line 2369153
    :goto_3
    move v0, v2

    .line 2369154
    if-eqz v0, :cond_5

    .line 2369155
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_2

    .line 2369156
    :cond_5
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_2

    .line 2369157
    :cond_6
    iget-object v6, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/26p;->m:LX/0Tn;

    iget-object v3, v0, LX/Gb5;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v7, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    invoke-interface {v6, v3, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 2369158
    iget-object v3, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/26p;->o:LX/0Tn;

    invoke-virtual {v8, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-interface {v3, v2, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 2369159
    int-to-long v2, v2

    const-wide/32 v8, 0x48190800

    mul-long/2addr v2, v8

    .line 2369160
    cmp-long v8, v6, v10

    if-eqz v8, :cond_7

    iget-object v8, v0, LX/Gb5;->c:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    sub-long v6, v8, v6

    cmp-long v2, v6, v2

    if-lez v2, :cond_8

    :cond_7
    move v2, v5

    goto/16 :goto_0

    :cond_8
    move v2, v4

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 2369161
    :cond_a
    iget-object v6, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/26p;->n:LX/0Tn;

    iget-object v3, v0, LX/Gb5;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v7, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    const-string v7, ""

    invoke-interface {v6, v3, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2369162
    const-string v6, ""

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2369163
    iget-object v6, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/26p;->l:LX/0Tn;

    invoke-virtual {v3, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    invoke-interface {v6, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 2369164
    if-nez v3, :cond_b

    .line 2369165
    const-string v2, "default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    goto/16 :goto_3

    .line 2369166
    :cond_b
    iget-object v3, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v5, LX/26p;->n:LX/0Tn;

    invoke-virtual {v5, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-interface {v3, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    move v2, v4

    .line 2369167
    goto/16 :goto_3

    .line 2369168
    :cond_c
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v2, v4

    goto/16 :goto_3

    :cond_d
    move v2, v5

    goto/16 :goto_3
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2369135
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "targeted_nux"

    iget-object v2, p0, LX/Gb8;->c:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;->targetedNux:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2369134
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2369131
    if-eqz p1, :cond_0

    .line 2369132
    check-cast p1, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    iput-object p1, p0, LX/Gb8;->c:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    .line 2369133
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2369130
    const-string v0, "4196"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369129
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369128
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    return-object v0
.end method
