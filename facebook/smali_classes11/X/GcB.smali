.class public final LX/GcB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3x2;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field public final synthetic b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 0

    .prologue
    .line 2371285
    iput-object p1, p0, LX/GcB;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    iput-object p2, p0, LX/GcB;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2371286
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2371287
    const v3, 0x7f0d0bfc

    if-ne v0, v3, :cond_1

    .line 2371288
    iget-object v0, p0, LX/GcB;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    if-eqz v0, :cond_0

    .line 2371289
    iget-object v0, p0, LX/GcB;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    .line 2371290
    iput-boolean v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->G:Z

    .line 2371291
    iget-object v0, p0, LX/GcB;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    iget-object v2, p0, LX/GcB;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-interface {v0, v2}, LX/2Ad;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2371292
    :cond_0
    :goto_0
    return v1

    .line 2371293
    :cond_1
    const v3, 0x7f0d3203

    if-ne v0, v3, :cond_4

    .line 2371294
    iget-object v3, p0, LX/GcB;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2371295
    invoke-static {v3}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 2371296
    invoke-static {v0}, LX/0hM;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    .line 2371297
    iget-object v4, p0, LX/GcB;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    invoke-static {v4, v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->c(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;Ljava/lang/String;)Z

    move-result v4

    .line 2371298
    iget-object v0, p0, LX/GcB;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    if-nez v4, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v5, v3, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2371299
    iget-object v0, p0, LX/GcB;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->p:LX/8D3;

    iget-object v3, p0, LX/GcB;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v3, v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    if-nez v4, :cond_2

    move v2, v1

    .line 2371300
    :cond_2
    iget-object v4, v0, LX/8D3;->a:LX/0Zb;

    const-string v5, "logged_out_push_notification_setting_changed"

    invoke-static {v5}, LX/8D3;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "uid"

    invoke-virtual {v5, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "notifications_enabled"

    invoke-virtual {v5, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2371301
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2371302
    goto :goto_1

    :cond_4
    move v1, v2

    .line 2371303
    goto :goto_0
.end method
