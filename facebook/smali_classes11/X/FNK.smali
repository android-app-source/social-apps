.class public LX/FNK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:J

.field public static b:Lcom/facebook/messaging/model/threadkey/ThreadKey;


# instance fields
.field private final c:LX/2Ud;

.field public final d:LX/FNh;

.field private final e:LX/2Uq;

.field public final f:Landroid/content/Context;

.field private final g:LX/3Rc;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2232420
    const-wide/16 v0, -0x64

    .line 2232421
    sput-wide v0, LX/FNK;->a:J

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    sput-object v0, LX/FNK;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    return-void
.end method

.method public constructor <init>(LX/2Ud;LX/FNh;LX/2Uq;LX/3Rc;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232423
    iput-object p1, p0, LX/FNK;->c:LX/2Ud;

    .line 2232424
    iput-object p2, p0, LX/FNK;->d:LX/FNh;

    .line 2232425
    iput-object p3, p0, LX/FNK;->e:LX/2Uq;

    .line 2232426
    iput-object p4, p0, LX/FNK;->g:LX/3Rc;

    .line 2232427
    iput-object p5, p0, LX/FNK;->f:Landroid/content/Context;

    .line 2232428
    return-void
.end method

.method private a(JZLX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 8
    .param p4    # LX/6ek;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2232429
    if-eqz p3, :cond_3

    iget-object v0, p0, LX/FNK;->c:LX/2Ud;

    invoke-virtual {v0, p1, p2}, LX/2Ud;->a(J)J

    move-result-wide v0

    .line 2232430
    :goto_0
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v2

    iget-object v3, p0, LX/FNK;->g:LX/3Rc;

    invoke-virtual {v3}, LX/3Rc;->a()I

    move-result v3

    .line 2232431
    iput v3, v2, LX/6fr;->b:I

    .line 2232432
    move-object v2, v2

    .line 2232433
    invoke-virtual {v2}, LX/6fr;->g()Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-result-object v2

    .line 2232434
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v3

    if-nez p4, :cond_0

    sget-object p4, LX/6ek;->INBOX:LX/6ek;

    .line 2232435
    :cond_0
    iput-object p4, v3, LX/6g6;->A:LX/6ek;

    .line 2232436
    move-object v3, v3

    .line 2232437
    sget-wide v4, LX/FNK;->a:J

    invoke-static {v4, v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    .line 2232438
    iput-object v4, v3, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2232439
    move-object v3, v3

    .line 2232440
    const/4 v4, 0x0

    .line 2232441
    iput-boolean v4, v3, LX/6g6;->u:Z

    .line 2232442
    move-object v3, v3

    .line 2232443
    iput-wide v0, v3, LX/6g6;->j:J

    .line 2232444
    move-object v3, v3

    .line 2232445
    iput-wide p1, v3, LX/6g6;->M:J

    .line 2232446
    move-object v3, v3

    .line 2232447
    iput-wide v0, v3, LX/6g6;->k:J

    .line 2232448
    move-object v0, v3

    .line 2232449
    iget-object v1, p0, LX/FNK;->h:LX/0Px;

    if-nez v1, :cond_1

    .line 2232450
    new-instance v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->EMAIL:LX/0XG;

    const-string v5, ""

    invoke-direct {v3, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    iget-object v4, p0, LX/FNK;->f:Landroid/content/Context;

    const v5, 0x7f082eb5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2232451
    new-instance v3, LX/6fz;

    invoke-direct {v3}, LX/6fz;-><init>()V

    .line 2232452
    iput-object v1, v3, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2232453
    move-object v1, v3

    .line 2232454
    invoke-virtual {v1}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v1

    new-instance v3, LX/6fz;

    invoke-direct {v3}, LX/6fz;-><init>()V

    iget-object v4, p0, LX/FNK;->d:LX/FNh;

    invoke-virtual {v4}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v4

    .line 2232455
    iput-object v4, v3, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2232456
    move-object v3, v3

    .line 2232457
    invoke-virtual {v3}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v3

    invoke-static {v1, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/FNK;->h:LX/0Px;

    .line 2232458
    :cond_1
    iget-object v1, p0, LX/FNK;->h:LX/0Px;

    move-object v1, v1

    .line 2232459
    iput-object v1, v0, LX/6g6;->h:Ljava/util/List;

    .line 2232460
    move-object v0, v0

    .line 2232461
    iput-object v2, v0, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2232462
    move-object v0, v0

    .line 2232463
    iget-object v1, p0, LX/FNK;->f:Landroid/content/Context;

    const v2, 0x7f082eb6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2232464
    iput-object v1, v0, LX/6g6;->o:Ljava/lang/String;

    .line 2232465
    move-object v0, v0

    .line 2232466
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-nez v1, :cond_2

    .line 2232467
    const/4 v1, 0x4

    .line 2232468
    iput v1, v0, LX/6g6;->T:I

    .line 2232469
    :cond_2
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    return-object v0

    :cond_3
    move-wide v0, p1

    .line 2232470
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(JLX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1
    .param p3    # LX/6ek;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2232471
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, LX/FNK;->a(JZLX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    return-object v0
.end method
