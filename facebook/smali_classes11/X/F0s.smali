.class public final LX/F0s;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2190921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2190922
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2190899
    invoke-static {p0, p1, p2}, LX/Es3;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2190900
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2190901
    invoke-static {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2190914
    if-nez p0, :cond_1

    .line 2190915
    :cond_0
    :goto_0
    return-object v0

    .line 2190916
    :cond_1
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v1, :cond_2

    .line 2190917
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    goto :goto_0

    .line 2190918
    :cond_2
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz v1, :cond_0

    .line 2190919
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    .line 2190920
    invoke-static {v0}, LX/6X1;->a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2190913
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/F0s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 2190912
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, ""

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Z
    .locals 1

    .prologue
    .line 2190911
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2190903
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v1, :cond_1

    .line 2190904
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 2190905
    const-string v1, "friendversary_collage"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2190906
    :cond_0
    :goto_0
    return v0

    .line 2190907
    :cond_1
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz v1, :cond_2

    .line 2190908
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2190909
    sget-object v1, LX/F0r;->COLLAGE_V1:LX/F0r;

    invoke-virtual {v1}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2190910
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Z
    .locals 1

    .prologue
    .line 2190902
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
