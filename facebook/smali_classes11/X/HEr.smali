.class public LX/HEr;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;

.field public c:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442856
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2442857
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2442858
    iput-object v0, p0, LX/HEr;->a:LX/0Px;

    .line 2442859
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442860
    new-instance v1, LX/HEq;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030e93

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {v1, p0, v0}, LX/HEq;-><init>(LX/HEr;Lcom/facebook/resources/ui/FbTextView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2442861
    invoke-virtual {p0, p2}, LX/HEr;->e(I)Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;

    move-result-object v0

    .line 2442862
    if-eqz v0, :cond_0

    .line 2442863
    check-cast p1, LX/HEq;

    .line 2442864
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2442865
    iget-object v1, p1, LX/HEq;->l:LX/HEr;

    iget-object v1, v1, LX/HEr;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082212

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, p0

    const/4 p0, 0x1

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->k()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, p0

    const/4 p0, 0x2

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, p0

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2442866
    iget-object v2, p1, LX/HEq;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2442867
    iget-object v1, p1, LX/HEq;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p1, LX/HEq;->l:LX/HEr;

    iget-object v2, v2, LX/HEr;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2442868
    :cond_0
    return-void
.end method

.method public final e(I)Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442869
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/HEr;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/HEr;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2442870
    iget-object v0, p0, LX/HEr;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
