.class public LX/FSH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uo;

.field public final b:LX/0WJ;

.field public final c:LX/0qj;

.field public final d:LX/AjI;

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Uo;LX/0WJ;LX/0qj;LX/AjI;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241725
    iput-object p1, p0, LX/FSH;->a:LX/0Uo;

    .line 2241726
    iput-object p2, p0, LX/FSH;->b:LX/0WJ;

    .line 2241727
    iput-object p3, p0, LX/FSH;->c:LX/0qj;

    .line 2241728
    iput-object p4, p0, LX/FSH;->d:LX/AjI;

    .line 2241729
    iput-object p5, p0, LX/FSH;->e:LX/0ad;

    .line 2241730
    return-void
.end method

.method public static a(LX/0QB;)LX/FSH;
    .locals 7

    .prologue
    .line 2241731
    new-instance v1, LX/FSH;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v2

    check-cast v2, LX/0Uo;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {p0}, LX/0qj;->b(LX/0QB;)LX/0qj;

    move-result-object v4

    check-cast v4, LX/0qj;

    invoke-static {p0}, LX/AjI;->b(LX/0QB;)LX/AjI;

    move-result-object v5

    check-cast v5, LX/AjI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct/range {v1 .. v6}, LX/FSH;-><init>(LX/0Uo;LX/0WJ;LX/0qj;LX/AjI;LX/0ad;)V

    .line 2241732
    move-object v0, v1

    .line 2241733
    return-object v0
.end method

.method public static c(LX/FSH;)Z
    .locals 3

    .prologue
    .line 2241734
    iget-object v0, p0, LX/FSH;->e:LX/0ad;

    sget-short v1, LX/0fe;->aq:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static d(LX/FSH;)Z
    .locals 3

    .prologue
    .line 2241735
    iget-object v0, p0, LX/FSH;->e:LX/0ad;

    sget-short v1, LX/0fe;->ar:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
