.class public LX/Gpm;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "LX/Gqd;",
        "Lcom/facebook/instantshopping/model/data/InstantShoppingTitleAndDateBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Go7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/Go4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0W9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final g:Ljava/util/Calendar;

.field private h:LX/GoE;


# direct methods
.method public constructor <init>(LX/Gqd;)V
    .locals 1

    .prologue
    .line 2395741
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395742
    const-class v0, LX/Gpm;

    invoke-static {v0, p0}, LX/Gpm;->a(Ljava/lang/Class;LX/02k;)V

    .line 2395743
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/Gpm;->g:Ljava/util/Calendar;

    .line 2395744
    return-void
.end method

.method private a(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2395740
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const/16 v1, 0x18

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gpm;

    invoke-static {p0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v1

    check-cast v1, LX/Go7;

    invoke-static {p0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v2

    check-cast v2, LX/Go4;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object p0

    check-cast p0, LX/0W9;

    iput-object v1, p1, LX/Gpm;->d:LX/Go7;

    iput-object v2, p1, LX/Gpm;->e:LX/Go4;

    iput-object p0, p1, LX/Gpm;->f:LX/0W9;

    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 6

    .prologue
    .line 2395714
    check-cast p1, LX/GpK;

    const/4 v3, 0x2

    const/4 v5, 0x1

    .line 2395715
    iget-object v0, p0, LX/Gpm;->g:Ljava/util/Calendar;

    .line 2395716
    iget-object v1, p1, LX/GpK;->g:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/GpK;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p1, LX/GpK;->g:Ljava/util/Calendar;

    invoke-static {v0, v1}, LX/GpK;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, LX/GpK;->h:Ljava/util/Calendar;

    invoke-static {v0, v1}, LX/GpK;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2395717
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395718
    check-cast v0, LX/Gqd;

    .line 2395719
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v2

    iput-object v2, p0, LX/Gpm;->h:LX/GoE;

    .line 2395720
    if-eqz v1, :cond_3

    .line 2395721
    iget-object v1, p0, LX/Gpm;->g:Ljava/util/Calendar;

    .line 2395722
    :goto_1
    iget-object v2, p1, LX/GpK;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2395723
    iget-object v4, v0, LX/Gqd;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395724
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 2395725
    iget-object v4, v0, LX/Gqd;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395726
    iget-object v2, p0, LX/Gpm;->f:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v3, v5, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2395727
    iget-object v2, v0, LX/Gqd;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395728
    iget-boolean v1, p1, LX/GpK;->i:Z

    move v1, v1

    .line 2395729
    if-eqz v1, :cond_2

    .line 2395730
    iget-object v1, v0, LX/Gqd;->b:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2395731
    const-string v1, "%s - %s"

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2395732
    iget-object v4, p1, LX/GpK;->g:Ljava/util/Calendar;

    move-object v4, v4

    .line 2395733
    invoke-direct {p0, v4}, LX/Gpm;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 2395734
    iget-object v3, p1, LX/GpK;->h:Ljava/util/Calendar;

    move-object v3, v3

    .line 2395735
    invoke-direct {p0, v3}, LX/Gpm;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2395736
    iget-object v2, v0, LX/Gqd;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395737
    :cond_2
    return-void

    .line 2395738
    :cond_3
    iget-object v1, p1, LX/GpK;->g:Ljava/util/Calendar;

    move-object v1, v1

    .line 2395739
    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395710
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 2395711
    iget-object v0, p0, LX/Gpm;->d:LX/Go7;

    const-string v1, "title_date_start"

    iget-object v2, p0, LX/Gpm;->h:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395712
    iget-object v0, p0, LX/Gpm;->e:LX/Go4;

    iget-object v1, p0, LX/Gpm;->h:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395713
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395706
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 2395707
    iget-object v0, p0, LX/Gpm;->d:LX/Go7;

    const-string v1, "title_date_end"

    iget-object v2, p0, LX/Gpm;->h:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395708
    iget-object v0, p0, LX/Gpm;->e:LX/Go4;

    iget-object v1, p0, LX/Gpm;->h:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395709
    return-void
.end method
