.class public LX/GsJ;
.super LX/GsI;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2399372
    const-class v0, LX/GsJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GsJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2399369
    invoke-direct {p0, p1, p2}, LX/GsI;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2399370
    iput-object p3, p0, LX/GsI;->b:Ljava/lang/String;

    .line 2399371
    return-void
.end method

.method public static synthetic a(LX/GsJ;)V
    .locals 0

    .prologue
    .line 2399373
    invoke-super {p0}, LX/GsI;->cancel()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 2399349
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2399350
    invoke-virtual {v0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Gsc;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2399351
    const-string v0, "bridge_args"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2399352
    const-string v2, "bridge_args"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2399353
    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2399354
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2399355
    invoke-static {v2}, LX/Gs8;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v0

    .line 2399356
    const-string v2, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2399357
    :cond_0
    :goto_0
    const-string v0, "method_results"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2399358
    const-string v2, "method_results"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2399359
    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2399360
    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "{}"

    .line 2399361
    :cond_1
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2399362
    invoke-static {v2}, LX/Gs8;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v0

    .line 2399363
    const-string v2, "com.facebook.platform.protocol.RESULT_ARGS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2399364
    :cond_2
    :goto_1
    const-string v0, "version"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2399365
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    invoke-static {}, LX/GsS;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2399366
    return-object v1

    .line 2399367
    :catch_0
    goto :goto_0

    .line 2399368
    :catch_1
    goto :goto_1
.end method

.method public final cancel()V
    .locals 5

    .prologue
    .line 2399336
    iget-object v0, p0, LX/GsI;->d:Landroid/webkit/WebView;

    move-object v0, v0

    .line 2399337
    iget-boolean v1, p0, LX/GsI;->j:Z

    move v1, v1

    .line 2399338
    if-eqz v1, :cond_0

    .line 2399339
    iget-boolean v1, p0, LX/GsI;->h:Z

    move v1, v1

    .line 2399340
    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/webkit/WebView;->isShown()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2399341
    :cond_0
    invoke-super {p0}, LX/GsI;->cancel()V

    .line 2399342
    :cond_1
    :goto_0
    return-void

    .line 2399343
    :cond_2
    iget-boolean v1, p0, LX/GsJ;->b:Z

    if-nez v1, :cond_1

    .line 2399344
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/GsJ;->b:Z

    .line 2399345
    const-string v1, "(function() {  var event = document.createEvent(\'Event\');  event.initEvent(\'fbPlatformDialogMustClose\',true,true);  document.dispatchEvent(event);})();"

    .line 2399346
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 2399347
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2399348
    new-instance v1, Lcom/facebook/internal/FacebookWebFallbackDialog$1;

    invoke-direct {v1, p0}, Lcom/facebook/internal/FacebookWebFallbackDialog$1;-><init>(LX/GsJ;)V

    const-wide/16 v2, 0x5dc

    const v4, 0x77292c44

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
