.class public final LX/Ggy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2382833
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2382834
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2382835
    :goto_0
    return v1

    .line 2382836
    :cond_0
    const-string v11, "unread_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2382837
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 2382838
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 2382839
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2382840
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2382841
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2382842
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2382843
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 2382844
    :cond_3
    const-string v11, "edges"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2382845
    invoke-static {p0, p1}, LX/9r8;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2382846
    :cond_4
    const-string v11, "next_update_condition"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2382847
    invoke-static {p0, p1}, LX/9rJ;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2382848
    :cond_5
    const-string v11, "page_info"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2382849
    invoke-static {p0, p1}, LX/3Bn;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2382850
    :cond_6
    const-string v11, "session_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 2382851
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2382852
    :cond_7
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2382853
    const/4 v10, 0x0

    .line 2382854
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v11, :cond_f

    .line 2382855
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2382856
    :goto_2
    move v4, v10

    .line 2382857
    goto :goto_1

    .line 2382858
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2382859
    :cond_9
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2382860
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2382861
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2382862
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 2382863
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2382864
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2382865
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2382866
    if-eqz v0, :cond_a

    .line 2382867
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 2382868
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1

    .line 2382869
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2382870
    :cond_d
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_e

    .line 2382871
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2382872
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2382873
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_d

    if-eqz v11, :cond_d

    .line 2382874
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 2382875
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 2382876
    :cond_e
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2382877
    invoke-virtual {p1, v10, v4}, LX/186;->b(II)V

    .line 2382878
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_2

    :cond_f
    move v4, v10

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2382879
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2382880
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2382881
    if-eqz v0, :cond_0

    .line 2382882
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382883
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2382884
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2382885
    if-eqz v0, :cond_1

    .line 2382886
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382887
    invoke-static {p0, v0, p2, p3}, LX/9r8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2382888
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2382889
    if-eqz v0, :cond_2

    .line 2382890
    const-string v1, "next_update_condition"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382891
    invoke-static {p0, v0, p2, p3}, LX/9rJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2382892
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2382893
    if-eqz v0, :cond_3

    .line 2382894
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382895
    invoke-static {p0, v0, p2}, LX/3Bn;->a(LX/15i;ILX/0nX;)V

    .line 2382896
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2382897
    if-eqz v0, :cond_4

    .line 2382898
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382899
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2382900
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2382901
    if-eqz v0, :cond_6

    .line 2382902
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382903
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2382904
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2382905
    if-eqz v1, :cond_5

    .line 2382906
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382907
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2382908
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2382909
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2382910
    if-eqz v0, :cond_7

    .line 2382911
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2382912
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2382913
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2382914
    return-void
.end method
