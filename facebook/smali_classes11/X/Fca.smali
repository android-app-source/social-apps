.class public final LX/Fca;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ACu;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;

.field public final synthetic b:LX/FdQ;

.field public final synthetic c:LX/CyH;

.field public final synthetic d:LX/Fcb;


# direct methods
.method public constructor <init>(LX/Fcb;Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;LX/FdQ;LX/CyH;)V
    .locals 0

    .prologue
    .line 2262492
    iput-object p1, p0, LX/Fca;->d:LX/Fcb;

    iput-object p2, p0, LX/Fca;->a:Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;

    iput-object p3, p0, LX/Fca;->b:LX/FdQ;

    iput-object p4, p0, LX/Fca;->c:LX/CyH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(FF)V
    .locals 3

    .prologue
    .line 2262470
    iget-object v0, p0, LX/Fca;->a:Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;

    invoke-static {p1}, LX/Fcb;->b(F)I

    move-result v1

    invoke-static {p2}, LX/Fcb;->b(F)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->a(II)V

    .line 2262471
    return-void
.end method

.method public final b(FF)V
    .locals 8

    .prologue
    .line 2262472
    iget-object v0, p0, LX/Fca;->b:LX/FdQ;

    new-instance v1, LX/CyH;

    iget-object v2, p0, LX/Fca;->c:LX/CyH;

    .line 2262473
    iget-object v3, v2, LX/CyH;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2262474
    const/4 v3, 0x0

    invoke-static {p1}, LX/Fcb;->b(F)I

    move-result v4

    int-to-float v4, v4

    invoke-static {p2}, LX/Fcb;->b(F)I

    move-result v5

    int-to-float v5, v5

    const p2, 0x461c4000    # 10000.0f

    const/4 p0, 0x0

    .line 2262475
    cmpl-float v6, v4, p0

    if-nez v6, :cond_0

    cmpl-float v6, v5, p2

    if-nez v6, :cond_0

    .line 2262476
    const-string v6, "default"

    .line 2262477
    :goto_0
    move-object v4, v6

    .line 2262478
    invoke-direct {v1, v2, v3, v4}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/FdQ;->a(LX/CyH;)V

    .line 2262479
    return-void

    .line 2262480
    :cond_0
    new-instance v6, LX/0lp;

    invoke-direct {v6}, LX/0lp;-><init>()V

    .line 2262481
    new-instance v7, Ljava/io/StringWriter;

    invoke-direct {v7}, Ljava/io/StringWriter;-><init>()V

    .line 2262482
    :try_start_0
    invoke-virtual {v6, v7}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v6

    .line 2262483
    invoke-virtual {v6}, LX/0nX;->f()V

    .line 2262484
    cmpl-float p0, v4, p0

    if-lez p0, :cond_1

    .line 2262485
    const-string p0, "price_lower_bound"

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result p1

    mul-int/lit8 p1, p1, 0x64

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262486
    :cond_1
    cmpg-float p0, v5, p2

    if-gez p0, :cond_2

    .line 2262487
    const-string p0, "price_upper_bound"

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result p1

    mul-int/lit8 p1, p1, 0x64

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262488
    :cond_2
    invoke-virtual {v6}, LX/0nX;->g()V

    .line 2262489
    invoke-virtual {v6}, LX/0nX;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2262490
    invoke-virtual {v7}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 2262491
    :catch_0
    const-string v6, "default"

    goto :goto_0
.end method
