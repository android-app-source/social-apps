.class public LX/Gsi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field public d:I

.field public e:LX/GsB;

.field public f:Landroid/os/Bundle;

.field private g:Lcom/facebook/AccessToken;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2400037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2400038
    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v0

    iput-object v0, p0, LX/Gsi;->g:Lcom/facebook/AccessToken;

    .line 2400039
    iget-object v0, p0, LX/Gsi;->g:Lcom/facebook/AccessToken;

    if-nez v0, :cond_0

    .line 2400040
    invoke-static {p1}, LX/Gsc;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2400041
    if-eqz v0, :cond_1

    .line 2400042
    iput-object v0, p0, LX/Gsi;->b:Ljava/lang/String;

    .line 2400043
    :cond_0
    invoke-direct {p0, p1, p2, p3}, LX/Gsi;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2400044
    return-void

    .line 2400045
    :cond_1
    new-instance v0, LX/GAA;

    const-string v1, "Attempted to create a builder without a valid access token or a valid default Application ID."

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2400055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2400056
    if-nez p2, :cond_0

    .line 2400057
    invoke-static {p1}, LX/Gsc;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 2400058
    :cond_0
    const-string v0, "applicationId"

    invoke-static {p2, v0}, LX/Gsd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2400059
    iput-object p2, p0, LX/Gsi;->b:Ljava/lang/String;

    .line 2400060
    invoke-direct {p0, p1, p3, p4}, LX/Gsi;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2400061
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2400062
    iput-object p1, p0, LX/Gsi;->a:Landroid/content/Context;

    .line 2400063
    iput-object p2, p0, LX/Gsi;->c:Ljava/lang/String;

    .line 2400064
    if-eqz p3, :cond_0

    .line 2400065
    iput-object p3, p0, LX/Gsi;->f:Landroid/os/Bundle;

    .line 2400066
    :goto_0
    return-void

    .line 2400067
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/Gsi;->f:Landroid/os/Bundle;

    goto :goto_0
.end method


# virtual methods
.method public a()LX/GsI;
    .locals 6

    .prologue
    .line 2400046
    iget-object v0, p0, LX/Gsi;->g:Lcom/facebook/AccessToken;

    if-eqz v0, :cond_0

    .line 2400047
    iget-object v0, p0, LX/Gsi;->f:Landroid/os/Bundle;

    const-string v1, "app_id"

    iget-object v2, p0, LX/Gsi;->g:Lcom/facebook/AccessToken;

    .line 2400048
    iget-object v3, v2, Lcom/facebook/AccessToken;->k:Ljava/lang/String;

    move-object v2, v3

    .line 2400049
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2400050
    iget-object v0, p0, LX/Gsi;->f:Landroid/os/Bundle;

    const-string v1, "access_token"

    iget-object v2, p0, LX/Gsi;->g:Lcom/facebook/AccessToken;

    .line 2400051
    iget-object v3, v2, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v2, v3

    .line 2400052
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2400053
    :goto_0
    new-instance v0, LX/GsI;

    iget-object v1, p0, LX/Gsi;->a:Landroid/content/Context;

    iget-object v2, p0, LX/Gsi;->c:Ljava/lang/String;

    iget-object v3, p0, LX/Gsi;->f:Landroid/os/Bundle;

    iget v4, p0, LX/Gsi;->d:I

    iget-object v5, p0, LX/Gsi;->e:LX/GsB;

    invoke-direct/range {v0 .. v5}, LX/GsI;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILX/GsB;)V

    return-object v0

    .line 2400054
    :cond_0
    iget-object v0, p0, LX/Gsi;->f:Landroid/os/Bundle;

    const-string v1, "app_id"

    iget-object v2, p0, LX/Gsi;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
