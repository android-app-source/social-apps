.class public final LX/G0G;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;)V
    .locals 0

    .prologue
    .line 2308668
    iput-object p1, p0, LX/G0G;->a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2308672
    iget-object v0, p0, LX/G0G;->a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;->w:LX/Fze;

    new-instance v1, LX/G0F;

    invoke-direct {v1, p0, p1}, LX/G0F;-><init>(LX/G0G;Ljava/lang/String;)V

    .line 2308673
    iget-object v2, v0, LX/Fze;->d:LX/1Ck;

    const-string v3, "CLEAR_MEMORIAL_PROFILE_PICTURE_CACHE"

    new-instance p0, LX/Fza;

    invoke-direct {p0, v0}, LX/Fza;-><init>(LX/Fze;)V

    new-instance p1, LX/Fzb;

    invoke-direct {p1, v0, v1}, LX/Fzb;-><init>(LX/Fze;LX/0TF;)V

    invoke-virtual {v2, v3, p0, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2308674
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2308670
    iget-object v0, p0, LX/G0G;->a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;->x:LX/G0B;

    const v1, 0x7f08336a

    invoke-virtual {v0, v1}, LX/G0B;->a(I)V

    .line 2308671
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2308669
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, LX/G0G;->a(Ljava/lang/String;)V

    return-void
.end method
