.class public LX/FHb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/FHb;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/03V;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/43E;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0SW;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2Mo;

.field private final g:LX/0V8;

.field private final h:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/0SW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/03V;LX/0Or;LX/0SW;LX/0Ot;LX/2Mo;LX/0V8;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/43E;",
            ">;",
            "LX/0SW;",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "LX/2Mo;",
            "LX/0V8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2220757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220758
    iput-object p1, p0, LX/FHb;->a:LX/0Zb;

    .line 2220759
    iput-object p2, p0, LX/FHb;->b:LX/03V;

    .line 2220760
    iput-object p3, p0, LX/FHb;->c:LX/0Or;

    .line 2220761
    iput-object p4, p0, LX/FHb;->d:LX/0SW;

    .line 2220762
    iput-object p5, p0, LX/FHb;->e:LX/0Ot;

    .line 2220763
    iput-object p6, p0, LX/FHb;->f:LX/2Mo;

    .line 2220764
    iput-object p7, p0, LX/FHb;->g:LX/0V8;

    .line 2220765
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x7b98a000

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/FHb;->h:LX/0QI;

    .line 2220766
    return-void
.end method

.method private static a(LX/FHb;Lcom/facebook/ui/media/attachments/MediaResource;)J
    .locals 2

    .prologue
    .line 2220750
    iget-object v0, p0, LX/FHb;->h:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SW;

    .line 2220751
    if-nez v0, :cond_0

    .line 2220752
    const-wide/16 v0, 0x0

    .line 2220753
    :goto_0
    return-wide v0

    .line 2220754
    :cond_0
    invoke-virtual {v0}, LX/0SW;->stop()LX/0SW;

    .line 2220755
    iget-object v1, p0, LX/FHb;->h:LX/0QI;

    invoke-interface {v1, p1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 2220756
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FHb;
    .locals 11

    .prologue
    .line 2220735
    sget-object v0, LX/FHb;->i:LX/FHb;

    if-nez v0, :cond_1

    .line 2220736
    const-class v1, LX/FHb;

    monitor-enter v1

    .line 2220737
    :try_start_0
    sget-object v0, LX/FHb;->i:LX/FHb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2220738
    if-eqz v2, :cond_0

    .line 2220739
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2220740
    new-instance v3, LX/FHb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0x1803

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    .line 2220741
    invoke-static {}, LX/0SW;->createUnstarted()LX/0SW;

    move-result-object v7

    move-object v7, v7

    .line 2220742
    check-cast v7, LX/0SW;

    const/16 v8, 0x24e

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/2Mo;->a(LX/0QB;)LX/2Mo;

    move-result-object v9

    check-cast v9, LX/2Mo;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v10

    check-cast v10, LX/0V8;

    invoke-direct/range {v3 .. v10}, LX/FHb;-><init>(LX/0Zb;LX/03V;LX/0Or;LX/0SW;LX/0Ot;LX/2Mo;LX/0V8;)V

    .line 2220743
    move-object v0, v3

    .line 2220744
    sput-object v0, LX/FHb;->i:LX/FHb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2220745
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2220746
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2220747
    :cond_1
    sget-object v0, LX/FHb;->i:LX/FHb;

    return-object v0

    .line 2220748
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2220749
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 2220726
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2220727
    const-string v1, "compose"

    .line 2220728
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2220729
    const-string v1, "uuid"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220730
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220731
    const-string v1, "attachment_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220732
    const-string v1, "attachment_type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220733
    const-string v1, "media_source"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v2}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220734
    return-object v0
.end method

.method private static a(LX/FHb;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/Map;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2220714
    const-string v1, "current_rtt"

    iget-object v0, p0, LX/FHb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->k()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220715
    const-string v1, "current_bandwidth"

    iget-object v0, p0, LX/FHb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->f()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220716
    const-string v0, "upload_size"

    iget-wide v2, p2, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220717
    iget-object v0, p0, LX/FHb;->f:LX/2Mo;

    invoke-virtual {v0}, LX/2Mo;->b()LX/CMt;

    move-result-object v0

    .line 2220718
    const-string v1, "upload_conn_quality"

    iget-object v2, v0, LX/CMt;->a:LX/CMv;

    invoke-virtual {v2}, LX/CMv;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220719
    const-string v1, "upload_conn_quality_confidence"

    iget-object v0, v0, LX/CMt;->b:LX/CMs;

    invoke-virtual {v0}, LX/CMs;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220720
    const-string v0, "upload_full_quality_photo"

    invoke-virtual {p1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220721
    const-string v0, "parallel_upload"

    invoke-virtual {p1, v0, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220722
    if-eqz p3, :cond_0

    .line 2220723
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2220724
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2220725
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 2220712
    const-string v1, "native_resizer"

    iget-object v0, p0, LX/FHb;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/43E;

    invoke-virtual {v0}, LX/43E;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220713
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;I)V
    .locals 4

    .prologue
    .line 2220703
    iget-object v0, p0, LX/FHb;->d:LX/0SW;

    .line 2220704
    const-wide/16 v2, 0x0

    iput-wide v2, v0, LX/0SW;->elapsedNanos:J

    .line 2220705
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0SW;->isRunning:Z

    .line 2220706
    move-object v0, v0

    .line 2220707
    invoke-virtual {v0}, LX/0SW;->start()LX/0SW;

    .line 2220708
    const-string v0, "media_upload_resize_start"

    invoke-static {v0, p1}, LX/FHb;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2220709
    const-string v1, "phase"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220710
    iget-object v1, p0, LX/FHb;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2220711
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;IILjava/io/File;IZ)V
    .locals 6

    .prologue
    .line 2220681
    iget-object v0, p0, LX/FHb;->d:LX/0SW;

    invoke-virtual {v0}, LX/0SW;->stop()LX/0SW;

    .line 2220682
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2220683
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2220684
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2220685
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2220686
    const-string v2, "media_upload_resize_end"

    invoke-static {v2, p1}, LX/FHb;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2220687
    const-string v3, "phase"

    invoke-virtual {v2, v3, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220688
    const-string v3, "resize_skipped_from_cache"

    invoke-virtual {v2, v3, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220689
    const-string v3, "original_size"

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220690
    const-string v0, "resized_size"

    invoke-virtual {p4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220691
    const-string v0, "requested_width"

    invoke-virtual {v2, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220692
    const-string v0, "requested_height"

    invoke-virtual {v2, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220693
    const-string v0, "original_width"

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220694
    const-string v0, "original_height"

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220695
    invoke-virtual {p4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2220696
    const-string v0, "resized_width"

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220697
    const-string v0, "resized_height"

    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220698
    const-string v0, "resized_quality"

    invoke-virtual {v2, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220699
    const-string v0, "elapsed_time"

    iget-object v1, p0, LX/FHb;->d:LX/0SW;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v3}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220700
    invoke-direct {p0, v2}, LX/FHb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2220701
    iget-object v0, p0, LX/FHb;->a:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2220702
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2220670
    iget-object v0, p0, LX/FHb;->d:LX/0SW;

    invoke-virtual {v0}, LX/0SW;->stop()LX/0SW;

    .line 2220671
    const-string v0, "media_upload_resize_end"

    invoke-static {v0, p1}, LX/FHb;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2220672
    const-string v0, "phase"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220673
    const-string v0, "elapsed_time"

    iget-object v2, p0, LX/FHb;->d:LX/0SW;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220674
    instance-of v0, p3, Ljava/lang/Error;

    if-eqz v0, :cond_0

    .line 2220675
    const-string v2, "exception_info"

    move-object v0, p3

    check-cast v0, Ljava/lang/Error;

    invoke-virtual {v0}, Ljava/lang/Error;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220676
    :goto_0
    invoke-direct {p0, v1}, LX/FHb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2220677
    iget-object v0, p0, LX/FHb;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2220678
    iget-object v0, p0, LX/FHb;->b:LX/03V;

    const-string v1, "orca_upload_resize_failure"

    invoke-virtual {v0, v1, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2220679
    return-void

    .line 2220680
    :cond_0
    const-string v2, "exception_info"

    move-object v0, p3

    check-cast v0, Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/Exception;Ljava/util/Map;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/Exception;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2220637
    const-string v0, "media_upload_unpublished_failure"

    invoke-static {v0, p1}, LX/FHb;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 2220638
    invoke-static/range {v0 .. v5}, LX/FHb;->a(LX/FHb;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/Map;ZZ)V

    .line 2220639
    const-string v0, "elapsed_time"

    invoke-static {p0, p1}, LX/FHb;->a(LX/FHb;Lcom/facebook/ui/media/attachments/MediaResource;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220640
    const-string v0, "exception_info"

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220641
    const-string v0, "available_internal_storage"

    iget-object v2, p0, LX/FHb;->g:LX/0V8;

    sget-object v3, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v2, v3}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220642
    const-string v0, "available_external_storage"

    iget-object v2, p0, LX/FHb;->g:LX/0V8;

    sget-object v3, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v2, v3}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220643
    iget-object v0, p0, LX/FHb;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2220644
    const-string v0, "is_transient_api_error"

    .line 2220645
    invoke-static {p2}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    .line 2220646
    instance-of v4, v2, LX/2Oo;

    if-eqz v4, :cond_0

    .line 2220647
    check-cast v2, LX/2Oo;

    .line 2220648
    invoke-virtual {v2}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v2

    .line 2220649
    if-eqz v2, :cond_0

    .line 2220650
    iget-boolean v3, v2, Lcom/facebook/http/protocol/ApiErrorResult;->mIsTransient:Z

    move v2, v3

    .line 2220651
    :goto_0
    move v2, v2

    .line 2220652
    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220653
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/util/Map;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2220664
    const-string v0, "media_upload_unpublished_end"

    invoke-static {v0, p1}, LX/FHb;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 2220665
    invoke-static/range {v0 .. v5}, LX/FHb;->a(LX/FHb;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/Map;ZZ)V

    .line 2220666
    const-string v0, "elapsed_time"

    invoke-static {p0, p1}, LX/FHb;->a(LX/FHb;Lcom/facebook/ui/media/attachments/MediaResource;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220667
    const-string v0, "unpublished_fbid"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220668
    iget-object v0, p0, LX/FHb;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2220669
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/Map;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2220659
    const-string v0, "media_upload_unpublished_canceled"

    invoke-static {v0, p1}, LX/FHb;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 2220660
    invoke-static/range {v0 .. v5}, LX/FHb;->a(LX/FHb;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/Map;ZZ)V

    .line 2220661
    const-string v0, "elapsed_time"

    invoke-static {p0, p1}, LX/FHb;->a(LX/FHb;Lcom/facebook/ui/media/attachments/MediaResource;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220662
    iget-object v0, p0, LX/FHb;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2220663
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;ZZ)V
    .locals 6

    .prologue
    .line 2220654
    iget-object v0, p0, LX/FHb;->h:LX/0QI;

    invoke-static {}, LX/0SW;->createStarted()LX/0SW;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2220655
    const-string v0, "media_upload_unpublished_start"

    invoke-static {v0, p1}, LX/FHb;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2220656
    const/4 v3, 0x0

    move-object v0, p0

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, LX/FHb;->a(LX/FHb;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/Map;ZZ)V

    .line 2220657
    iget-object v0, p0, LX/FHb;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2220658
    return-void
.end method
