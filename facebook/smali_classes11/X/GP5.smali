.class public LX/GP5;
.super LX/GP4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GP4",
        "<",
        "LX/GQ8;",
        ">;"
    }
.end annotation


# instance fields
.field private c:Lcom/facebook/common/locale/Country;

.field private d:Lcom/facebook/resources/ui/FbEditText;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/GQ8;Landroid/content/res/Resources;LX/GOy;LX/GNs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2348380
    invoke-direct {p0, p1, p2, p3, p4}, LX/GP4;-><init>(LX/GQ7;Landroid/content/res/Resources;LX/GOy;LX/GNs;)V

    .line 2348381
    return-void
.end method

.method public static b(LX/GP5;Z)V
    .locals 1

    .prologue
    .line 2348378
    iget-object v0, p0, LX/GP5;->f:Landroid/view/View;

    invoke-static {v0, p1}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2348379
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2348364
    iget-object v0, p0, LX/GP5;->c:Lcom/facebook/common/locale/Country;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2348365
    const v0, 0x7f0d2483

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2348366
    const v0, 0x7f0d018a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/GP5;->d:Lcom/facebook/resources/ui/FbEditText;

    .line 2348367
    const v0, 0x7f0d018b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/GP5;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2348368
    const v0, 0x7f0d2484

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GP5;->f:Landroid/view/View;

    .line 2348369
    iget-object v0, p0, LX/GP4;->f:LX/GQ7;

    move-object v0, v0

    .line 2348370
    check-cast v0, LX/GQ8;

    new-instance v2, LX/GP1;

    invoke-direct {v2, p0}, LX/GP1;-><init>(LX/GP5;)V

    .line 2348371
    iput-object v2, v0, LX/GQ7;->a:LX/GP0;

    .line 2348372
    iget-object v0, p0, LX/GP5;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->hasFocus()Z

    move-result v0

    invoke-static {p0, v0}, LX/GP5;->b(LX/GP5;Z)V

    .line 2348373
    iget-object v0, p0, LX/GP5;->d:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/GP2;

    invoke-direct {v2, p0}, LX/GP2;-><init>(LX/GP5;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2348374
    iget-object v0, p0, LX/GP5;->d:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/GP3;

    invoke-direct {v2, p0}, LX/GP3;-><init>(LX/GP5;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2348375
    iget-object v0, p0, LX/GP5;->c:Lcom/facebook/common/locale/Country;

    invoke-static {v0}, LX/46H;->b(Lcom/facebook/common/locale/Country;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2348376
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2348377
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)V
    .locals 0
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348361
    iput-object p4, p0, LX/GP5;->c:Lcom/facebook/common/locale/Country;

    .line 2348362
    invoke-super {p0, p1, p2, p3}, LX/GP4;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2348363
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 2348382
    iget-object v0, p0, LX/GP5;->c:Lcom/facebook/common/locale/Country;

    invoke-static {v0}, LX/46H;->b(Lcom/facebook/common/locale/Country;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GP5;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/GP5;->c:Lcom/facebook/common/locale/Country;

    .line 2348383
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 p0, 0x3

    if-lt v2, p0, :cond_2

    invoke-static {v0, v1}, LX/46H;->a(Ljava/lang/CharSequence;Lcom/facebook/common/locale/Country;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 2348384
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348360
    const-string v0, "zip_code"

    return-object v0
.end method

.method public final c()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2348359
    iget-object v0, p0, LX/GP5;->d:Lcom/facebook/resources/ui/FbEditText;

    return-object v0
.end method

.method public final d()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 2348358
    iget-object v0, p0, LX/GP5;->e:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method
