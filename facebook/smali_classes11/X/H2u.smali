.class public LX/H2u;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/nearby/protocol/NearbyTilesParams;",
        "Lcom/facebook/nearby/protocol/NearbyTilesResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0SG;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/content/res/Resources;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2419269
    invoke-direct {p0, p3}, LX/0ro;-><init>(LX/0sO;)V

    .line 2419270
    iput-object p1, p0, LX/H2u;->b:LX/0SG;

    .line 2419271
    iput-object p2, p0, LX/H2u;->c:Landroid/content/res/Resources;

    .line 2419272
    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2419273
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 2419274
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 2419275
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 2419276
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 2419277
    :try_start_0
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2419278
    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2419279
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2419280
    :catch_0
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2419281
    :cond_0
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2419282
    check-cast p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419283
    const-class v0, Lcom/facebook/nearby/model/NearbyTilesMethodResult;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/NearbyTilesMethodResult;

    .line 2419284
    if-nez v0, :cond_0

    .line 2419285
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Invalid JSON result"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2419286
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2419287
    iget-object v8, v0, Lcom/facebook/nearby/model/NearbyTilesMethodResult;->nearbyTiles:Lcom/facebook/nearby/model/NearbyTiles;

    .line 2419288
    iget-object v0, v8, Lcom/facebook/nearby/model/NearbyTiles;->tiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMapTileNode;

    .line 2419289
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMapTileNode;->a()Lcom/facebook/graphql/model/GraphQLMapTile;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2419290
    :cond_1
    iget-object v0, v8, Lcom/facebook/nearby/model/NearbyTiles;->canonicalization:Ljava/lang/String;

    invoke-static {v0}, LX/H2u;->a(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    .line 2419291
    new-instance v0, Lcom/facebook/nearby/protocol/NearbyTilesResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/H2u;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v5, v8, Lcom/facebook/nearby/model/NearbyTiles;->version:Ljava/lang/String;

    iget-object v6, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/util/Set;

    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/util/Set;

    iget-object v8, v8, Lcom/facebook/nearby/model/NearbyTiles;->displayRegionHint:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/facebook/nearby/protocol/NearbyTilesResult;-><init>(LX/0ta;JLjava/util/List;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/protocol/NearbyTilesParams;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2419292
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 8

    .prologue
    .line 2419293
    check-cast p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;

    .line 2419294
    iget-object v0, p0, LX/H2u;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b14f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2419295
    new-instance v0, LX/H2S;

    invoke-direct {v0}, LX/H2S;-><init>()V

    move-object v0, v0

    .line 2419296
    const-string v2, "places_render_priority_1"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "places_render_priority_2"

    const-string v4, "2"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2419297
    iget-object v2, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->f:Landroid/location/Location;

    move-object v2, v2

    .line 2419298
    if-eqz v2, :cond_0

    .line 2419299
    const-string v3, "location_latitude"

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "location_longitude"

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "location_accuracy"

    .line 2419300
    iget v4, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->g:F

    move v4, v4

    .line 2419301
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "location_stale_time"

    .line 2419302
    iget v4, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->h:I

    move v4, v4

    .line 2419303
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "facepile_pic_size"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2419304
    :cond_0
    iget-object v1, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-object v1, v1

    .line 2419305
    if-nez v1, :cond_2

    .line 2419306
    const-string v1, "altitude"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "accuracy"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "speed"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2419307
    :cond_1
    :goto_0
    return-object v0

    .line 2419308
    :cond_2
    const-string v2, "altitude"

    .line 2419309
    iget v3, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->i:F

    move v3, v3

    .line 2419310
    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "accuracy"

    .line 2419311
    iget v4, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->j:F

    move v4, v4

    .line 2419312
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "user_heading"

    .line 2419313
    iget v4, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->k:F

    move v4, v4

    .line 2419314
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "user_heading_accuracy"

    .line 2419315
    iget v4, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->l:F

    move v4, v4

    .line 2419316
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "speed"

    .line 2419317
    iget v4, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->c:F

    move v4, v4

    .line 2419318
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "region_north_latitude"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "region_west_longitude"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "region_south_latitude"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "region_east_longitude"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "region_zoom"

    .line 2419319
    iget v3, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->e:F

    move v3, v3

    .line 2419320
    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2419321
    iget-object v1, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->a:Ljava/util/List;

    move-object v1, v1

    .line 2419322
    if-eqz v1, :cond_3

    .line 2419323
    const-string v2, "topics_list"

    .line 2419324
    sget-object v3, LX/2Rb;->INSTANCE:LX/2Rb;

    move-object v3, v3

    .line 2419325
    invoke-static {v1, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2419326
    :cond_3
    iget-object v1, p1, Lcom/facebook/nearby/protocol/NearbyTilesParams;->b:LX/0Px;

    move-object v1, v1

    .line 2419327
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2419328
    const-string v2, "query_function"

    invoke-virtual {v0, v2, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    goto/16 :goto_0
.end method
