.class public final enum LX/GmC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GmC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GmC;

.field public static final enum INVITED:LX/GmC;

.field public static final enum PENDING:LX/GmC;

.field public static final enum UNINVITED:LX/GmC;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2391648
    new-instance v0, LX/GmC;

    const-string v1, "UNINVITED"

    invoke-direct {v0, v1, v2}, LX/GmC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GmC;->UNINVITED:LX/GmC;

    new-instance v0, LX/GmC;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v3}, LX/GmC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GmC;->PENDING:LX/GmC;

    new-instance v0, LX/GmC;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v4}, LX/GmC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GmC;->INVITED:LX/GmC;

    const/4 v0, 0x3

    new-array v0, v0, [LX/GmC;

    sget-object v1, LX/GmC;->UNINVITED:LX/GmC;

    aput-object v1, v0, v2

    sget-object v1, LX/GmC;->PENDING:LX/GmC;

    aput-object v1, v0, v3

    sget-object v1, LX/GmC;->INVITED:LX/GmC;

    aput-object v1, v0, v4

    sput-object v0, LX/GmC;->$VALUES:[LX/GmC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2391647
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GmC;
    .locals 1

    .prologue
    .line 2391645
    const-class v0, LX/GmC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GmC;

    return-object v0
.end method

.method public static values()[LX/GmC;
    .locals 1

    .prologue
    .line 2391646
    sget-object v0, LX/GmC;->$VALUES:[LX/GmC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GmC;

    return-object v0
.end method
