.class public LX/GDg;
.super LX/GDY;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GDY",
        "<",
        "LX/AAf;",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;",
        ">;"
    }
.end annotation


# static fields
.field private static q:LX/0Xm;


# instance fields
.field public final b:LX/GG6;

.field public final c:LX/GNI;

.field public final d:LX/GG3;

.field private final e:LX/GER;

.field public f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public g:LX/GNL;

.field private h:LX/0ad;

.field private i:LX/0W3;

.field private j:LX/16I;

.field private k:LX/0lB;

.field private l:LX/0oz;

.field private m:LX/2U3;

.field private n:LX/2Pc;

.field private o:LX/8wR;

.field private p:LX/2PZ;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/GG6;LX/GNL;LX/GNI;LX/0ad;LX/0W3;LX/16I;LX/0lB;LX/0oz;LX/2U3;LX/2Pc;LX/8wR;LX/2PZ;LX/GER;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330021
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p13

    invoke-direct/range {v1 .. v6}, LX/GDY;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/2U3;)V

    .line 2330022
    iput-object p5, p0, LX/GDg;->b:LX/GG6;

    .line 2330023
    iput-object p7, p0, LX/GDg;->c:LX/GNI;

    .line 2330024
    iput-object p4, p0, LX/GDg;->d:LX/GG3;

    .line 2330025
    iput-object p6, p0, LX/GDg;->g:LX/GNL;

    .line 2330026
    iput-object p8, p0, LX/GDg;->h:LX/0ad;

    .line 2330027
    move-object/from16 v0, p9

    iput-object v0, p0, LX/GDg;->i:LX/0W3;

    .line 2330028
    move-object/from16 v0, p10

    iput-object v0, p0, LX/GDg;->j:LX/16I;

    .line 2330029
    move-object/from16 v0, p11

    iput-object v0, p0, LX/GDg;->k:LX/0lB;

    .line 2330030
    move-object/from16 v0, p12

    iput-object v0, p0, LX/GDg;->l:LX/0oz;

    .line 2330031
    move-object/from16 v0, p13

    iput-object v0, p0, LX/GDg;->m:LX/2U3;

    .line 2330032
    move-object/from16 v0, p14

    iput-object v0, p0, LX/GDg;->n:LX/2Pc;

    .line 2330033
    move-object/from16 v0, p15

    iput-object v0, p0, LX/GDg;->o:LX/8wR;

    .line 2330034
    move-object/from16 v0, p16

    iput-object v0, p0, LX/GDg;->p:LX/2PZ;

    .line 2330035
    move-object/from16 v0, p17

    iput-object v0, p0, LX/GDg;->e:LX/GER;

    .line 2330036
    return-void
.end method

.method public static a(LX/0QB;)LX/GDg;
    .locals 3

    .prologue
    .line 2330240
    const-class v1, LX/GDg;

    monitor-enter v1

    .line 2330241
    :try_start_0
    sget-object v0, LX/GDg;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2330242
    sput-object v2, LX/GDg;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2330243
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330244
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/GDg;->b(LX/0QB;)LX/GDg;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2330245
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GDg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2330246
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2330247
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V
    .locals 11

    .prologue
    .line 2330236
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v6, v0

    .line 2330237
    :goto_0
    iget-object v7, p0, LX/GDg;->e:LX/GER;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v10

    new-instance v0, LX/GDd;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/GDd;-><init>(LX/GDg;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Landroid/content/Context;)V

    move-object v1, v7

    move-object v2, v8

    move-object v3, v9

    move v4, v10

    move v5, v6

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, LX/GER;->a(Ljava/lang/String;Ljava/lang/String;IILX/GDc;)V

    .line 2330238
    return-void

    .line 2330239
    :cond_0
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0
.end method

.method public static a$redex0(LX/GDg;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 2330231
    if-eqz p5, :cond_1

    .line 2330232
    iget-object v0, p0, LX/GDg;->c:LX/GNI;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/GNI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;LX/GCE;Landroid/content/Context;)V

    .line 2330233
    :cond_0
    :goto_0
    return-void

    .line 2330234
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v0, v1, :cond_0

    invoke-virtual {p3}, LX/GCE;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330235
    invoke-virtual {p0, p2, p4}, LX/GDg;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/GDg;
    .locals 18

    .prologue
    .line 2330229
    new-instance v0, LX/GDg;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v3

    check-cast v3, LX/GF4;

    invoke-static/range {p0 .. p0}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v4

    check-cast v4, LX/GG3;

    invoke-static/range {p0 .. p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v5

    check-cast v5, LX/GG6;

    invoke-static/range {p0 .. p0}, LX/GNL;->a(LX/0QB;)LX/GNL;

    move-result-object v6

    check-cast v6, LX/GNL;

    invoke-static/range {p0 .. p0}, LX/GNI;->a(LX/0QB;)LX/GNI;

    move-result-object v7

    check-cast v7, LX/GNI;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v10

    check-cast v10, LX/16I;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v11

    check-cast v11, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v12

    check-cast v12, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v13

    check-cast v13, LX/2U3;

    invoke-static/range {p0 .. p0}, LX/2Pc;->a(LX/0QB;)LX/2Pc;

    move-result-object v14

    check-cast v14, LX/2Pc;

    invoke-static/range {p0 .. p0}, LX/8wR;->a(LX/0QB;)LX/8wR;

    move-result-object v15

    check-cast v15, LX/8wR;

    invoke-static/range {p0 .. p0}, LX/2PZ;->a(LX/0QB;)LX/2PZ;

    move-result-object v16

    check-cast v16, LX/2PZ;

    invoke-static/range {p0 .. p0}, LX/GER;->a(LX/0QB;)LX/GER;

    move-result-object v17

    check-cast v17, LX/GER;

    invoke-direct/range {v0 .. v17}, LX/GDg;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/GG6;LX/GNL;LX/GNI;LX/0ad;LX/0W3;LX/16I;LX/0lB;LX/0oz;LX/2U3;LX/2Pc;LX/8wR;LX/2PZ;LX/GER;)V

    .line 2330230
    return-object v0
.end method

.method private f()[B
    .locals 7

    .prologue
    .line 2330201
    :try_start_0
    iget-object v0, p0, LX/GDg;->k:LX/0lB;

    iget-object v1, p0, LX/GDg;->d:LX/GG3;

    iget-object v2, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330202
    new-instance v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;

    invoke-direct {v3}, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;-><init>()V

    .line 2330203
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->adAccountId:Ljava/lang/String;

    .line 2330204
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->adStatus:Ljava/lang/Object;

    .line 2330205
    invoke-static {v2}, LX/GG3;->K(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->audienceOption:Ljava/lang/String;

    .line 2330206
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-static {v4}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v5

    iput-wide v5, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->budget:J

    .line 2330207
    invoke-static {v2}, LX/GG3;->N(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->budgetType:Ljava/lang/String;

    .line 2330208
    invoke-static {v1, v2}, LX/GG3;->L(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->currency:Ljava/lang/String;

    .line 2330209
    iget-object v4, v1, LX/GG3;->i:LX/0oz;

    invoke-virtual {v4}, LX/0oz;->c()LX/0p3;

    move-result-object v4

    invoke-virtual {v4}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->connectionQualityClass:Ljava/lang/String;

    .line 2330210
    invoke-static {v2}, LX/GG3;->M(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v4

    iput v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->duration:I

    .line 2330211
    iget-object v4, v1, LX/GG3;->d:LX/GG6;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v5

    invoke-virtual {v4, v5}, LX/GG6;->c(I)J

    move-result-wide v5

    iput-wide v5, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->endTime:J

    .line 2330212
    invoke-static {v2}, LX/GG3;->Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->flow:Ljava/lang/String;

    .line 2330213
    iget-object v4, v1, LX/GG3;->g:Ljava/lang/String;

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->flowId:Ljava/lang/String;

    .line 2330214
    invoke-static {v2}, LX/GG3;->P(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v4

    iput v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->lowerEstimate:I

    .line 2330215
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->objective:LX/8wL;

    .line 2330216
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->pageId:Ljava/lang/String;

    .line 2330217
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->n()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->placement:Ljava/lang/String;

    .line 2330218
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v4

    .line 2330219
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v4, v5

    .line 2330220
    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->savedAudienceId:Ljava/lang/String;

    .line 2330221
    iget-object v4, v1, LX/GG3;->d:LX/GG6;

    invoke-virtual {v4}, LX/GG6;->a()J

    move-result-wide v5

    iput-wide v5, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->startTime:J

    .line 2330222
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->storyId:Ljava/lang/String;

    .line 2330223
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->targetingSpec:Ljava/lang/String;

    .line 2330224
    invoke-static {v2}, LX/GG3;->O(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v4

    iput v4, v3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->upperEstimate:I

    .line 2330225
    move-object v1, v3

    .line 2330226
    invoke-virtual {v0, v1}, LX/0lC;->c(Ljava/lang/Object;)[B
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2330227
    :goto_0
    return-object v0

    .line 2330228
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;
    .locals 8

    .prologue
    .line 2330105
    invoke-static {}, LX/AAg;->a()LX/AAf;

    move-result-object v0

    const-string v1, "input"

    .line 2330106
    new-instance v3, LX/4DC;

    invoke-direct {v3}, LX/4DC;-><init>()V

    .line 2330107
    new-instance v4, LX/4DB;

    invoke-direct {v4}, LX/4DB;-><init>()V

    .line 2330108
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2330109
    if-eqz v5, :cond_7

    .line 2330110
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2330111
    invoke-virtual {v4, v5}, LX/4DB;->b(Ljava/lang/String;)LX/4DB;

    .line 2330112
    :goto_0
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v5

    .line 2330113
    if-eqz v5, :cond_9

    .line 2330114
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v5

    .line 2330115
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    .line 2330116
    :goto_1
    invoke-virtual {v3, v4}, LX/4DC;->a(LX/4DB;)LX/4DC;

    .line 2330117
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->c(Ljava/lang/String;)LX/4DC;

    .line 2330118
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v4

    .line 2330119
    invoke-virtual {v4}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 2330120
    invoke-virtual {v3, v4}, LX/4DC;->d(Ljava/lang/String;)LX/4DC;

    .line 2330121
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->a(Ljava/lang/String;)LX/4DC;

    .line 2330122
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->b(Ljava/lang/Integer;)LX/4DC;

    .line 2330123
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v4, v4

    .line 2330124
    if-eqz v4, :cond_0

    .line 2330125
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v4, v4

    .line 2330126
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/CreativeAdModel;->n()LX/4Cf;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->a(LX/4Cf;)LX/4DC;

    .line 2330127
    :cond_0
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v4, v4

    .line 2330128
    if-eqz v4, :cond_1

    .line 2330129
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v4, v4

    .line 2330130
    new-instance v5, LX/4DA;

    invoke-direct {v5}, LX/4DA;-><init>()V

    iget-object v6, v4, Lcom/facebook/adinterfaces/model/EventSpecModel;->b:Ljava/lang/String;

    .line 2330131
    const-string v7, "event_boost_type"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330132
    move-object v5, v5

    .line 2330133
    iget-object v6, v4, Lcom/facebook/adinterfaces/model/EventSpecModel;->a:Ljava/lang/String;

    .line 2330134
    const-string v7, "event_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330135
    move-object v5, v5

    .line 2330136
    move-object v4, v5

    .line 2330137
    const-string v5, "event_spec"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2330138
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2330139
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->b(Ljava/lang/String;)LX/4DC;

    .line 2330140
    :cond_2
    iget-object v4, p0, LX/GDg;->b:LX/GG6;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v5

    invoke-virtual {v4, v5}, LX/GG6;->c(I)J

    move-result-wide v5

    long-to-int v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->a(Ljava/lang/Integer;)LX/4DC;

    .line 2330141
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n()Ljava/lang/String;

    move-result-object v4

    .line 2330142
    const-string v5, "placement"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330143
    iget-object v4, p0, LX/GDg;->d:LX/GG3;

    .line 2330144
    iget-object v5, v4, LX/GG3;->g:Ljava/lang/String;

    move-object v4, v5

    .line 2330145
    const-string v5, "flow_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330146
    iget v4, p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    move v4, v4

    .line 2330147
    if-lez v4, :cond_3

    .line 2330148
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-object v4, v4

    .line 2330149
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->NO_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    if-ne v4, v5, :cond_3

    .line 2330150
    iget v4, p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    move v4, v4

    .line 2330151
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2330152
    const-string v5, "bid_amount"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2330153
    const-string v4, "NO_PACING"

    .line 2330154
    const-string v5, "pacing_type"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330155
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eq v4, v5, :cond_4

    .line 2330156
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v4

    .line 2330157
    const-string v5, "call_to_action_type"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330158
    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330159
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v4, v5

    .line 2330160
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    move-object v4, v5

    .line 2330161
    if-eqz v4, :cond_4

    .line 2330162
    new-instance v4, LX/4Cd;

    invoke-direct {v4}, LX/4Cd;-><init>()V

    .line 2330163
    iget-object v5, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330164
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v5, v6

    .line 2330165
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    move-object v5, v6

    .line 2330166
    invoke-virtual {v4, v5}, LX/4Cd;->a(Ljava/lang/String;)LX/4Cd;

    .line 2330167
    const-string v5, "call_to_action_data"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2330168
    :cond_4
    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    if-eq v4, v5, :cond_5

    .line 2330169
    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2330170
    const-string v5, "objective"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330171
    :cond_5
    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330172
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->p:Ljava/lang/String;

    move-object v4, v5

    .line 2330173
    if-eqz v4, :cond_6

    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330174
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->q:[Ljava/lang/String;

    move-object v4, v5

    .line 2330175
    if-eqz v4, :cond_6

    .line 2330176
    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330177
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->p:Ljava/lang/String;

    move-object v4, v5

    .line 2330178
    const-string v5, "checkout_reserved_campaign_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330179
    iget-object v4, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330180
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->q:[Ljava/lang/String;

    move-object v4, v5

    .line 2330181
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 2330182
    const-string v5, "checkout_payment_ids"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2330183
    :cond_6
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2330184
    const-string v5, "offline_mutation"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2330185
    move-object v2, v3

    .line 2330186
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v1, "validateOnly"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/AAf;

    return-object v0

    .line 2330187
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2330188
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v6

    .line 2330189
    if-eqz v5, :cond_8

    .line 2330190
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2330191
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v6

    .line 2330192
    invoke-virtual {v4, v5}, LX/4DB;->b(Ljava/lang/String;)LX/4DB;

    goto/16 :goto_0

    .line 2330193
    :cond_8
    iget-object v5, p0, LX/GDg;->g:LX/GNL;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/GNL;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)LX/4Ch;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(LX/4Ch;)LX/4DB;

    goto/16 :goto_0

    .line 2330194
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2330195
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v6

    .line 2330196
    if-eqz v5, :cond_a

    .line 2330197
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2330198
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v6

    .line 2330199
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    goto/16 :goto_1

    .line 2330200
    :cond_a
    const-string v5, "NCPP"

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    goto/16 :goto_1
.end method

.method public final a(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2330093
    iget-object v0, p0, LX/GDg;->d:LX/GG3;

    const/4 v6, 0x0

    .line 2330094
    const-string v4, "submit_flow_click"

    const-string v5, "create"

    move-object v2, v0

    move-object v3, p2

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2330095
    iget-object v0, p0, LX/GDg;->j:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2330096
    invoke-virtual {p0, p2, p3}, LX/GDg;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    .line 2330097
    :goto_0
    return-void

    .line 2330098
    :cond_0
    iput-object p2, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330099
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2330100
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2330101
    iget-object v1, p0, LX/GDg;->c:LX/GNI;

    invoke-virtual {v1, v0, p2, p1, p3}, LX/GNI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;LX/GCE;Landroid/content/Context;)V

    goto :goto_0

    .line 2330102
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->s()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2330103
    invoke-virtual {p0, p2, p3}, LX/GDg;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    goto :goto_0

    .line 2330104
    :cond_2
    invoke-direct {p0, p1, p2, p3, v0}, LX/GDg;->a(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2330085
    iget-object v0, p0, LX/GDg;->d:LX/GG3;

    iget-object v1, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2330086
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2330087
    if-eqz v0, :cond_0

    .line 2330088
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2330089
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 2330090
    iget-object v0, p0, LX/GDg;->n:LX/2Pc;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0}, LX/GDg;->f()[B

    move-result-object v3

    invoke-virtual {v0, v1, p3, v2, v3}, LX/2Pc;->a(Ljava/lang/String;Ljava/lang/String;Z[B)V

    .line 2330091
    iget-object v0, p0, LX/GDg;->o:LX/8wR;

    new-instance v1, LX/8wS;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v3

    invoke-static {v3}, LX/GG6;->a(LX/GGB;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/8wS;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2330092
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2330082
    iput-object p1, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330083
    const v0, 0x7f080aec

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V

    .line 2330084
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2330075
    iget-object v0, p0, LX/GDg;->d:LX/GG3;

    iget-object v1, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v8, 0x0

    .line 2330076
    const-string v6, "submit_flow"

    const-string v7, "create"

    move-object v4, v0

    move-object v5, v1

    move-object v9, v8

    invoke-static/range {v4 .. v9}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2330077
    iget-object v0, p0, LX/GDg;->d:LX/GG3;

    iget-object v1, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2330078
    const/4 v0, 0x0

    iput-object v0, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330079
    iget-object v0, p0, LX/GDY;->f:LX/GF4;

    move-object v0, v0

    .line 2330080
    new-instance v1, LX/GFV;

    iget-object v2, p0, LX/GDY;->a:LX/4At;

    sget-object v3, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    invoke-direct {v1, v2, v3}, LX/GFV;-><init>(LX/4At;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2330081
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2330073
    iget-object v0, p0, LX/GDg;->p:LX/2PZ;

    invoke-virtual {v0, p1}, LX/2PZ;->a(Ljava/lang/String;)V

    .line 2330074
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 9

    .prologue
    .line 2330063
    iget-object v0, p0, LX/GDg;->d:LX/GG3;

    iget-object v1, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330064
    const-string v5, "submit_flow_error"

    const-string v6, "create"

    const/4 v7, 0x0

    move-object v3, v0

    move-object v4, v1

    move-object v8, p1

    invoke-static/range {v3 .. v8}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2330065
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_0

    .line 2330066
    iget-object v0, p0, LX/GDY;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2330067
    check-cast p1, LX/4Ua;

    .line 2330068
    iget-object v0, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget-object v0, v0, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    const-string v1, "&#039;"

    const-string v2, "\'"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2330069
    iget-object v1, p0, LX/GDY;->f:LX/GF4;

    move-object v1, v1

    .line 2330070
    new-instance v2, LX/GFP;

    invoke-direct {v2, v0}, LX/GFP;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2330071
    :goto_0
    return-void

    .line 2330072
    :cond_0
    invoke-super {p0, p1}, LX/GDY;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V
    .locals 12

    .prologue
    .line 2330053
    iput-object p2, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330054
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    .line 2330055
    iget-object v0, p1, LX/GCE;->c:Ljava/lang/Boolean;

    move-object v0, v0

    .line 2330056
    if-eqz v0, :cond_0

    .line 2330057
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 2330058
    iget-object v2, p1, LX/GCE;->c:Ljava/lang/Boolean;

    move-object v2, v2

    .line 2330059
    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    move-object v0, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, LX/GDg;->a$redex0(LX/GDg;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;Landroid/content/Context;Z)V

    .line 2330060
    :goto_0
    return-void

    .line 2330061
    :cond_0
    iget-object v0, p0, LX/GDg;->m:LX/2U3;

    const-class v2, LX/GDg;

    const-string v3, "Fetch to ShowCheckoutExperience required even though it should have been cached"

    invoke-virtual {v0, v2, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2330062
    iget-object v0, p0, LX/GDg;->e:LX/GER;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v10

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    new-instance v2, LX/GDe;

    move-object v3, p0

    move-object v4, v1

    move-object v5, p2

    move-object v6, p1

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, LX/GDe;-><init>(LX/GDg;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;Landroid/content/Context;)V

    move-object v3, v0

    move-object v4, v8

    move-object v5, v9

    move v6, v10

    move v7, v11

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, LX/GER;->a(Ljava/lang/String;Ljava/lang/String;IILX/GDc;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2330047
    iget-object v0, p0, LX/GDg;->n:LX/2Pc;

    invoke-virtual {v0, p1}, LX/2Pc;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2330048
    if-nez v0, :cond_0

    .line 2330049
    iget-object v0, p0, LX/GDg;->m:LX/2U3;

    const-class v1, LX/GDg;

    const-string v2, "Promotion Id was null in offline mutation."

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2330050
    :goto_0
    return-void

    .line 2330051
    :cond_0
    iget-object v1, p0, LX/GDg;->n:LX/2Pc;

    const/4 v2, 0x1

    invoke-direct {p0}, LX/GDg;->f()[B

    move-result-object v3

    invoke-virtual {v1, v0, p1, v2, v3}, LX/2Pc;->a(Ljava/lang/String;Ljava/lang/String;Z[B)V

    .line 2330052
    iget-object v1, p0, LX/GDg;->o:LX/8wR;

    new-instance v2, LX/8wS;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-direct {v2, v0, v3}, LX/8wS;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2330046
    iget-object v1, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/GDg;->i:LX/0W3;

    sget-wide v2, LX/0X5;->l:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2330038
    iget-object v2, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/GDg;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v2, v3, :cond_1

    .line 2330039
    :cond_0
    :goto_0
    return v0

    .line 2330040
    :cond_1
    iget-object v2, p0, LX/GDg;->i:LX/0W3;

    sget-wide v4, LX/0X5;->l:J

    invoke-interface {v2, v4, v5, v0}, LX/0W4;->a(JZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2330041
    iget-object v2, p0, LX/GDg;->j:LX/16I;

    invoke-virtual {v2}, LX/16I;->a()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 2330042
    goto :goto_0

    .line 2330043
    :cond_2
    iget-object v2, p0, LX/GDg;->l:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    .line 2330044
    sget-object v3, LX/GDf;->a:[I

    invoke-virtual {v2}, LX/0p3;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 2330045
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2330037
    iget-object v1, p0, LX/GDg;->j:LX/16I;

    invoke-virtual {v1}, LX/16I;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/GDg;->h:LX/0ad;

    sget-short v2, LX/GDK;->u:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
