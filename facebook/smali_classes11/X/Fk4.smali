.class public LX/Fk4;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0en;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:Landroid/app/DownloadManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2278094
    const-class v0, LX/Fk4;

    sput-object v0, LX/Fk4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0en;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/app/DownloadManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278096
    iput-object p1, p0, LX/Fk4;->b:LX/0en;

    .line 2278097
    iput-object p2, p0, LX/Fk4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2278098
    iput-object p3, p0, LX/Fk4;->d:Landroid/app/DownloadManager;

    .line 2278099
    return-void
.end method

.method public static b(LX/0QB;)LX/Fk4;
    .locals 4

    .prologue
    .line 2278100
    new-instance v3, LX/Fk4;

    invoke-static {p0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v0

    check-cast v0, LX/0en;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2S1;->b(LX/0QB;)Landroid/app/DownloadManager;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    invoke-direct {v3, v0, v1, v2}, LX/Fk4;-><init>(LX/0en;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/app/DownloadManager;)V

    .line 2278101
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const-wide/16 v2, -0x1

    .line 2278102
    iget-object v0, p0, LX/Fk4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/FkE;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 2278103
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 2278104
    :try_start_0
    iget-object v2, p0, LX/Fk4;->d:Landroid/app/DownloadManager;

    const/4 v3, 0x1

    new-array v3, v3, [J

    const/4 v4, 0x0

    aput-wide v0, v3, v4

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2278105
    :cond_0
    iget-object v0, p0, LX/Fk4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/FkE;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2278106
    if-eqz v0, :cond_1

    .line 2278107
    :try_start_1
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2278108
    invoke-virtual {v2}, Ljava/net/URI;->isAbsolute()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "file"

    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2278109
    :try_start_2
    invoke-static {v2}, LX/0en;->a(Ljava/net/URI;)Ljava/io/File;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    .line 2278110
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2278111
    :cond_1
    :goto_1
    iget-object v0, p0, LX/Fk4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {p1}, LX/FkE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2278112
    :goto_2
    return-void

    .line 2278113
    :catch_0
    goto :goto_2

    .line 2278114
    :catch_1
    const/4 v2, 0x0

    .line 2278115
    goto :goto_0

    .line 2278116
    :cond_2
    invoke-static {v0}, LX/0en;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    goto :goto_0

    .line 2278117
    :catch_2
    goto :goto_1
.end method
