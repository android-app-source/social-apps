.class public LX/FSO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pf;


# instance fields
.field private final a:LX/1QP;

.field private final b:LX/1QM;

.field private final c:LX/1Q7;

.field private final d:LX/1QQ;

.field public final e:LX/FSM;

.field private final f:LX/1QF;

.field private final g:LX/FSQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/1PT;LX/1Q6;LX/1Q5;LX/1Q7;LX/1QA;LX/FSM;LX/1QF;LX/FSQ;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241852
    invoke-static {p1}, LX/1Q6;->a(Landroid/content/Context;)LX/1QP;

    move-result-object v0

    iput-object v0, p0, LX/FSO;->a:LX/1QP;

    .line 2241853
    invoke-virtual {p5, p2}, LX/1Q5;->a(Ljava/lang/String;)LX/1QM;

    move-result-object v0

    iput-object v0, p0, LX/FSO;->b:LX/1QM;

    .line 2241854
    iput-object p6, p0, LX/FSO;->c:LX/1Q7;

    .line 2241855
    invoke-static {p3}, LX/1QA;->a(LX/1PT;)LX/1QQ;

    move-result-object v0

    iput-object v0, p0, LX/FSO;->d:LX/1QQ;

    .line 2241856
    iput-object p8, p0, LX/FSO;->e:LX/FSM;

    .line 2241857
    iput-object p9, p0, LX/FSO;->f:LX/1QF;

    .line 2241858
    iput-object p10, p0, LX/FSO;->g:LX/FSQ;

    .line 2241859
    return-void
.end method


# virtual methods
.method public final a()LX/1Q9;
    .locals 1

    .prologue
    .line 2241860
    iget-object v0, p0, LX/FSO;->c:LX/1Q7;

    invoke-virtual {v0}, LX/1Q7;->a()LX/1Q9;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 6

    .prologue
    .line 2241861
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/FSQ;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 2241862
    iget-object v0, p0, LX/FSO;->f:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2241863
    iget-object v0, p0, LX/FSO;->f:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1

    .prologue
    .line 2241864
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/1R6;)V

    .line 2241865
    return-void
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 2241866
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0, p1}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(LX/1Rb;)V

    .line 2241867
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2241868
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/FSQ;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2241869
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2241870
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2241871
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2241872
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2241873
    return-void
.end method

.method public final a(LX/22C;)V
    .locals 1

    .prologue
    .line 2241874
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/22C;)V

    .line 2241875
    return-void
.end method

.method public final a(LX/34p;)V
    .locals 1

    .prologue
    .line 2241876
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/34p;)V

    .line 2241877
    return-void
.end method

.method public final a(LX/5Oj;)V
    .locals 1

    .prologue
    .line 2241880
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/5Oj;)V

    .line 2241881
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2241878
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/FSQ;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2241879
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2241899
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/FSQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2241900
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2241897
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2241898
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2241895
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2}, LX/FSQ;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 2241896
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2241893
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(Ljava/lang/String;)V

    .line 2241894
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2241891
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2}, LX/FSQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241892
    return-void
.end method

.method public final a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 2241889
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2241890
    return-void
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2241887
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a([Ljava/lang/Object;)V

    .line 2241888
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2241886
    iget-object v0, p0, LX/FSO;->f:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 2241884
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(LX/1R6;)V

    .line 2241885
    return-void
.end method

.method public final b(LX/22C;)V
    .locals 1

    .prologue
    .line 2241882
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(LX/22C;)V

    .line 2241883
    return-void
.end method

.method public final b(LX/5Oj;)V
    .locals 1

    .prologue
    .line 2241847
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(LX/5Oj;)V

    .line 2241848
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2241849
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2241850
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2241814
    iget-object v0, p0, LX/FSO;->f:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 2241815
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2241816
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2}, LX/FSQ;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241817
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2241818
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(Z)V

    .line 2241819
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2241820
    iget-object v0, p0, LX/FSO;->d:LX/1QQ;

    invoke-virtual {v0}, LX/1QQ;->c()LX/1PT;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 2241821
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2241822
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2241823
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241824
    iget-object v0, p0, LX/FSO;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getAnalyticsModule()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2241825
    iget-object v0, p0, LX/FSO;->a:LX/1QP;

    invoke-virtual {v0}, LX/1QP;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241826
    iget-object v0, p0, LX/FSO;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getFontFoundry()LX/1QO;

    move-result-object v0

    return-object v0
.end method

.method public getNavigator()LX/5KM;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241827
    iget-object v0, p0, LX/FSO;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getNavigator()LX/5KM;

    move-result-object v0

    return-object v0
.end method

.method public getTraitCollection()LX/1QN;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241828
    iget-object v0, p0, LX/FSO;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getTraitCollection()LX/1QN;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2241829
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2241830
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->i()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2241831
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 2241832
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->iN_()V

    .line 2241833
    return-void
.end method

.method public final iO_()Z
    .locals 1

    .prologue
    .line 2241834
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->iO_()Z

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2241835
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->j()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2241836
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->k()V

    .line 2241837
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2241838
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->l()Z

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 2241839
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 2241840
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->m_(Z)V

    .line 2241841
    return-void
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 2241842
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2241843
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->p()V

    .line 2241844
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2241845
    iget-object v0, p0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->q()Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2241846
    iget-object v0, p0, LX/FSO;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->r()Z

    move-result v0

    return v0
.end method
