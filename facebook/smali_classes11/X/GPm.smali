.class public LX/GPm;
.super LX/6u5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6u5",
        "<",
        "Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349440
    invoke-direct {p0, p1}, LX/6u5;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349441
    return-void
.end method

.method public static b(LX/0QB;)LX/GPm;
    .locals 2

    .prologue
    .line 2349442
    new-instance v1, LX/GPm;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/GPm;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349443
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2349444
    check-cast p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;

    .line 2349445
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "tax_info"

    .line 2349446
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349447
    move-object v0, v0

    .line 2349448
    const-string v1, "POST"

    .line 2349449
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349450
    move-object v0, v0

    .line 2349451
    const-string v1, "act_%s/tax_info"

    .line 2349452
    iget-object v2, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2349453
    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2349454
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2349455
    move-object v0, v0

    .line 2349456
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2349457
    const-string v2, "business_name"

    .line 2349458
    iget-object v3, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2349459
    invoke-static {v1, v2, v3}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 2349460
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "business_address"

    .line 2349461
    iget-object p0, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->c:Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-object p0, p0

    .line 2349462
    invoke-virtual {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->f()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349463
    const-string v2, "tax_id"

    .line 2349464
    iget-object v3, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2349465
    invoke-static {v1, v2, v3}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 2349466
    iget-object v2, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->c:Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-object v2, v2

    .line 2349467
    invoke-virtual {v2}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->e()Lcom/facebook/common/locale/Country;

    move-result-object v2

    const-string v3, "BR"

    invoke-static {v3}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2349468
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "is_agency_representing_client"

    .line 2349469
    iget-boolean p0, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->h:Z

    move p0, p0

    .line 2349470
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349471
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "is_client_based_in_france"

    .line 2349472
    iget-boolean p0, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->i:Z

    move p0, p0

    .line 2349473
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349474
    iget-boolean v2, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->h:Z

    move v2, v2

    .line 2349475
    if-eqz v2, :cond_0

    .line 2349476
    iget-boolean v2, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->i:Z

    move v2, v2

    .line 2349477
    if-eqz v2, :cond_0

    .line 2349478
    const-string v2, "client_business_name"

    .line 2349479
    iget-object v3, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2349480
    invoke-static {v1, v2, v3}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 2349481
    const-string v2, "client_email"

    .line 2349482
    iget-object v3, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2349483
    invoke-static {v1, v2, v3}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 2349484
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "client_address"

    .line 2349485
    iget-object p0, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->f:Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-object p0, p0

    .line 2349486
    invoke-virtual {p0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->f()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349487
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "is_client_paying_invoices"

    .line 2349488
    iget-boolean p0, p1, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;->j:Z

    move p0, p0

    .line 2349489
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349490
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "business_mandate_received"

    const-string p0, "true"

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2349491
    :cond_0
    move-object v1, v1

    .line 2349492
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2349493
    move-object v0, v0

    .line 2349494
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2349495
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349496
    move-object v0, v0

    .line 2349497
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349498
    const-string v0, "post_business_address"

    return-object v0
.end method
