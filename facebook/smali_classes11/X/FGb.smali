.class public final enum LX/FGb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FGb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FGb;

.field public static final enum FAILED:LX/FGb;

.field public static final enum IN_PROGRESS:LX/FGb;

.field public static final enum NOT_ACTIVE:LX/FGb;

.field public static final enum SUCCEEDED:LX/FGb;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2219188
    new-instance v0, LX/FGb;

    const-string v1, "NOT_ACTIVE"

    invoke-direct {v0, v1, v2}, LX/FGb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGb;->NOT_ACTIVE:LX/FGb;

    .line 2219189
    new-instance v0, LX/FGb;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/FGb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGb;->IN_PROGRESS:LX/FGb;

    .line 2219190
    new-instance v0, LX/FGb;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v4}, LX/FGb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGb;->SUCCEEDED:LX/FGb;

    .line 2219191
    new-instance v0, LX/FGb;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/FGb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGb;->FAILED:LX/FGb;

    .line 2219192
    const/4 v0, 0x4

    new-array v0, v0, [LX/FGb;

    sget-object v1, LX/FGb;->NOT_ACTIVE:LX/FGb;

    aput-object v1, v0, v2

    sget-object v1, LX/FGb;->IN_PROGRESS:LX/FGb;

    aput-object v1, v0, v3

    sget-object v1, LX/FGb;->SUCCEEDED:LX/FGb;

    aput-object v1, v0, v4

    sget-object v1, LX/FGb;->FAILED:LX/FGb;

    aput-object v1, v0, v5

    sput-object v0, LX/FGb;->$VALUES:[LX/FGb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2219187
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FGb;
    .locals 1

    .prologue
    .line 2219186
    const-class v0, LX/FGb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FGb;

    return-object v0
.end method

.method public static values()[LX/FGb;
    .locals 1

    .prologue
    .line 2219185
    sget-object v0, LX/FGb;->$VALUES:[LX/FGb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FGb;

    return-object v0
.end method
