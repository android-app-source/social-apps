.class public final enum LX/GPV;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GPV;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GPV;

.field public static final enum AUTH_COMPLETED:LX/GPV;

.field public static final enum AUTH_PENDING:LX/GPV;

.field public static final enum CAPTURE_PENDING:LX/GPV;

.field public static final enum COMPLETED:LX/GPV;

.field public static final enum FAILED:LX/GPV;

.field public static final enum INITED:LX/GPV;

.field public static final enum PENDING:LX/GPV;

.field public static final enum PROCESSING:LX/GPV;

.field public static final enum REVERSE_AUTH_PENDING:LX/GPV;

.field public static final enum REVIEW_CANCEL_PENDING:LX/GPV;

.field public static final enum REVIEW_SUCCESS_PENDING:LX/GPV;

.field public static final enum WAIT_FOR_REVIEW:LX/GPV;

.field public static final enum WAIT_FOR_REVIEW_PENDING:LX/GPV;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2348943
    new-instance v0, LX/GPV;

    const-string v1, "INITED"

    invoke-direct {v0, v1, v3}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->INITED:LX/GPV;

    .line 2348944
    new-instance v0, LX/GPV;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v4}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->PENDING:LX/GPV;

    .line 2348945
    new-instance v0, LX/GPV;

    const-string v1, "PROCESSING"

    invoke-direct {v0, v1, v5}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->PROCESSING:LX/GPV;

    .line 2348946
    new-instance v0, LX/GPV;

    const-string v1, "WAIT_FOR_REVIEW_PENDING"

    invoke-direct {v0, v1, v6}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->WAIT_FOR_REVIEW_PENDING:LX/GPV;

    .line 2348947
    new-instance v0, LX/GPV;

    const-string v1, "WAIT_FOR_REVIEW"

    invoke-direct {v0, v1, v7}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->WAIT_FOR_REVIEW:LX/GPV;

    .line 2348948
    new-instance v0, LX/GPV;

    const-string v1, "REVIEW_SUCCESS_PENDING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->REVIEW_SUCCESS_PENDING:LX/GPV;

    .line 2348949
    new-instance v0, LX/GPV;

    const-string v1, "REVIEW_CANCEL_PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->REVIEW_CANCEL_PENDING:LX/GPV;

    .line 2348950
    new-instance v0, LX/GPV;

    const-string v1, "COMPLETED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->COMPLETED:LX/GPV;

    .line 2348951
    new-instance v0, LX/GPV;

    const-string v1, "FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->FAILED:LX/GPV;

    .line 2348952
    new-instance v0, LX/GPV;

    const-string v1, "AUTH_PENDING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->AUTH_PENDING:LX/GPV;

    .line 2348953
    new-instance v0, LX/GPV;

    const-string v1, "AUTH_COMPLETED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->AUTH_COMPLETED:LX/GPV;

    .line 2348954
    new-instance v0, LX/GPV;

    const-string v1, "REVERSE_AUTH_PENDING"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->REVERSE_AUTH_PENDING:LX/GPV;

    .line 2348955
    new-instance v0, LX/GPV;

    const-string v1, "CAPTURE_PENDING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/GPV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPV;->CAPTURE_PENDING:LX/GPV;

    .line 2348956
    const/16 v0, 0xd

    new-array v0, v0, [LX/GPV;

    sget-object v1, LX/GPV;->INITED:LX/GPV;

    aput-object v1, v0, v3

    sget-object v1, LX/GPV;->PENDING:LX/GPV;

    aput-object v1, v0, v4

    sget-object v1, LX/GPV;->PROCESSING:LX/GPV;

    aput-object v1, v0, v5

    sget-object v1, LX/GPV;->WAIT_FOR_REVIEW_PENDING:LX/GPV;

    aput-object v1, v0, v6

    sget-object v1, LX/GPV;->WAIT_FOR_REVIEW:LX/GPV;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GPV;->REVIEW_SUCCESS_PENDING:LX/GPV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GPV;->REVIEW_CANCEL_PENDING:LX/GPV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GPV;->COMPLETED:LX/GPV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/GPV;->FAILED:LX/GPV;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/GPV;->AUTH_PENDING:LX/GPV;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/GPV;->AUTH_COMPLETED:LX/GPV;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/GPV;->REVERSE_AUTH_PENDING:LX/GPV;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/GPV;->CAPTURE_PENDING:LX/GPV;

    aput-object v2, v0, v1

    sput-object v0, LX/GPV;->$VALUES:[LX/GPV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2348968
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)LX/GPV;
    .locals 5

    .prologue
    .line 2348960
    invoke-static {}, LX/GPV;->values()[LX/GPV;

    move-result-object v0

    .line 2348961
    array-length v4, v0

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 2348962
    invoke-interface {v2}, LX/6LU;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 2348963
    :goto_1
    move-object v0, v1

    .line 2348964
    const-string v1, "Invalid payment status: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GPV;

    return-object v0

    .line 2348965
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2348966
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/GPV;
    .locals 1

    .prologue
    .line 2348967
    const-class v0, LX/GPV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GPV;

    return-object v0
.end method

.method public static values()[LX/GPV;
    .locals 1

    .prologue
    .line 2348959
    sget-object v0, LX/GPV;->$VALUES:[LX/GPV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GPV;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2348958
    invoke-virtual {p0}, LX/GPV;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348957
    invoke-virtual {p0}, LX/GPV;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
