.class public LX/HCC;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HBs;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/HDM;

.field private final f:LX/HDR;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HC0;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/01T;

.field public final k:J

.field public final l:Landroid/content/Context;

.field public final m:LX/HBt;

.field private final n:I

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HCB;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

.field private q:LX/15i;

.field private r:I

.field public s:Z

.field public t:Z

.field public u:LX/6WJ;

.field public v:LX/6WJ;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/HDM;LX/HDR;LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;JLandroid/content/Context;LX/HBt;I)V
    .locals 3
    .param p11    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # LX/HBt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HBs;",
            ">;",
            "LX/HDM;",
            "LX/HDR;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HC0;",
            ">;",
            "LX/01T;",
            "J",
            "Landroid/content/Context;",
            "Lcom/facebook/pages/common/editpage/EditPageFragment$UpdateEditPageDataListener;",
            "I)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2438292
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2438293
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/HCC;->o:Ljava/util/List;

    .line 2438294
    const/4 v1, 0x0

    iput-object v1, p0, LX/HCC;->q:LX/15i;

    const/4 v1, 0x0

    iput v1, p0, LX/HCC;->r:I

    .line 2438295
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/HCC;->s:Z

    .line 2438296
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/HCC;->t:Z

    .line 2438297
    const/4 v1, 0x0

    iput-object v1, p0, LX/HCC;->u:LX/6WJ;

    .line 2438298
    const/4 v1, 0x0

    iput-object v1, p0, LX/HCC;->v:LX/6WJ;

    .line 2438299
    iput-object p1, p0, LX/HCC;->a:LX/0Ot;

    .line 2438300
    iput-object p2, p0, LX/HCC;->b:LX/0Ot;

    .line 2438301
    iput-object p3, p0, LX/HCC;->c:LX/0Ot;

    .line 2438302
    iput-object p4, p0, LX/HCC;->d:LX/0Ot;

    .line 2438303
    iput-object p5, p0, LX/HCC;->e:LX/HDM;

    .line 2438304
    iput-object p6, p0, LX/HCC;->f:LX/HDR;

    .line 2438305
    iput-object p7, p0, LX/HCC;->g:LX/0Ot;

    .line 2438306
    iput-object p8, p0, LX/HCC;->h:LX/0Ot;

    .line 2438307
    iput-object p9, p0, LX/HCC;->i:LX/0Ot;

    .line 2438308
    iput-object p10, p0, LX/HCC;->j:LX/01T;

    .line 2438309
    iput-wide p11, p0, LX/HCC;->k:J

    .line 2438310
    move-object/from16 v0, p13

    iput-object v0, p0, LX/HCC;->l:Landroid/content/Context;

    .line 2438311
    move-object/from16 v0, p14

    iput-object v0, p0, LX/HCC;->m:LX/HBt;

    .line 2438312
    move/from16 v0, p15

    iput v0, p0, LX/HCC;->n:I

    .line 2438313
    return-void
.end method

.method public static a$redex0(LX/HCC;LX/9X7;)V
    .locals 6

    .prologue
    .line 2438314
    iget-object v0, p0, LX/HCC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-wide v2, p0, LX/HCC;->k:J

    invoke-direct {p0}, LX/HCC;->e()Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v4

    const-string v5, "edit_page"

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/9XE;->a(LX/9X7;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Ljava/lang/String;)V

    .line 2438315
    return-void
.end method

.method public static a$redex0(LX/HCC;Ljava/lang/String;)V
    .locals 10
    .param p0    # LX/HCC;
        .annotation build Lcom/facebook/graphql/calls/PageActionChannelType;
        .end annotation
    .end param

    .prologue
    .line 2438279
    sget-object v0, LX/9X7;->EDIT_SWITCH_DEFAULT_ACTIONS:LX/9X7;

    invoke-static {p0, v0}, LX/HCC;->a$redex0(LX/HCC;LX/9X7;)V

    .line 2438280
    iget-object v0, p0, LX/HCC;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "delete_order_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, LX/HCC;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HBs;

    iget-wide v4, p0, LX/HCC;->k:J

    .line 2438281
    new-instance v3, LX/9Xe;

    invoke-direct {v3}, LX/9Xe;-><init>()V

    move-object v3, v3

    .line 2438282
    const-string v6, "input"

    new-instance v7, LX/4HY;

    invoke-direct {v7}, LX/4HY;-><init>()V

    .line 2438283
    const-string v8, "channel_type"

    invoke-virtual {v7, v8, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438284
    move-object v7, v7

    .line 2438285
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 2438286
    const-string v9, "page_id"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438287
    move-object v7, v7

    .line 2438288
    invoke-virtual {v3, v6, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/9Xe;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2438289
    iget-object v6, v1, LX/HBs;->b:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 2438290
    new-instance v3, LX/HC1;

    invoke-direct {v3, p0}, LX/HC1;-><init>(LX/HCC;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2438291
    return-void
.end method

.method private e()Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;
    .locals 5

    .prologue
    .line 2438274
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, LX/HCC;->r:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2438275
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, LX/HCC;->q:LX/15i;

    iget v2, p0, LX/HCC;->r:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2438276
    const/4 v1, 0x3

    const-class v3, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2438277
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 2438278
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0
.end method

.method public static f(LX/HCC;)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2438316
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget v0, p0, LX/HCC;->r:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 2438317
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, LX/HCC;->q:LX/15i;

    iget v4, p0, LX/HCC;->r:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2438318
    const-class v3, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    invoke-virtual {v0, v4, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2438319
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iget-object v0, p0, LX/HCC;->q:LX/15i;

    iget v4, p0, LX/HCC;->r:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2438320
    invoke-virtual {v0, v4, v5}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2438321
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    iget-object v0, p0, LX/HCC;->q:LX/15i;

    iget v4, p0, LX/HCC;->r:I

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2438322
    const-class v3, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    invoke-virtual {v0, v4, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2438323
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_4
    iget-object v0, p0, LX/HCC;->q:LX/15i;

    iget v4, p0, LX/HCC;->r:I

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    invoke-virtual {v0, v4, v5}, LX/15i;->g(II)I

    move-result v3

    .line 2438324
    invoke-virtual {v0, v3, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    return v1

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 2438325
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    :cond_0
    move v0, v2

    .line 2438326
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2438327
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    :cond_2
    move v0, v2

    .line 2438328
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 2438329
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0

    :cond_4
    move v0, v2

    .line 2438330
    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    .line 2438331
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :cond_6
    move v1, v2

    .line 2438332
    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 2438260
    sget-object v0, LX/HCB;->TEMPLATE:LX/HCB;

    invoke-virtual {v0}, LX/HCB;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2438261
    new-instance v0, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;

    iget-object v1, p0, LX/HCC;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0303c3

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;-><init>(Landroid/view/View;)V

    .line 2438262
    :goto_0
    return-object v0

    .line 2438263
    :cond_0
    sget-object v0, LX/HCB;->BUTTONS:LX/HCB;

    invoke-virtual {v0}, LX/HCB;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2438264
    iget-object v0, p0, LX/HCC;->e:LX/HDM;

    iget-object v1, p0, LX/HCC;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030455

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2438265
    new-instance p0, LX/HDL;

    const-class v2, LX/H8V;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/H8V;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p0, v2, v3, v1}, LX/HDL;-><init>(LX/H8V;LX/23P;Landroid/view/View;)V

    .line 2438266
    move-object v0, p0

    .line 2438267
    goto :goto_0

    .line 2438268
    :cond_1
    sget-object v0, LX/HCB;->TABS:LX/HCB;

    invoke-virtual {v0}, LX/HCB;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2438269
    iget-object v0, p0, LX/HCC;->f:LX/HDR;

    iget-object v1, p0, LX/HCC;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030459

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2438270
    new-instance v4, LX/HDQ;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v5

    check-cast v5, LX/23P;

    const-class v6, LX/HDK;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/HDK;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    invoke-static {v0}, LX/HDI;->a(LX/0QB;)LX/HDI;

    move-result-object v8

    check-cast v8, LX/HDI;

    const/16 v9, 0x12c4

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2b68

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v11, v1

    invoke-direct/range {v4 .. v11}, LX/HDQ;-><init>(LX/23P;LX/HDK;LX/0wM;LX/HDI;LX/0Ot;LX/0Ot;Landroid/view/View;)V

    .line 2438271
    move-object v0, v4

    .line 2438272
    goto :goto_0

    .line 2438273
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2438247
    instance-of v0, p1, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;

    if-eqz v0, :cond_1

    .line 2438248
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/HCC;->q:LX/15i;

    iget v2, p0, LX/HCC;->r:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2438249
    check-cast p1, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;

    new-instance v1, LX/HC2;

    invoke-direct {v1, p0}, LX/HC2;-><init>(LX/HCC;)V

    const/4 v6, 0x0

    .line 2438250
    iget-object v4, p1, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v3, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    invoke-virtual {v0, v2, v6, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$CurrentTemplateDataModel$TemplatesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v5, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2438251
    iget-object v3, p1, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    const v4, 0x7f0836d2

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2438252
    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    iget-object v4, p1, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2438253
    iget-object v3, p1, Lcom/facebook/pages/common/editpage/viewholders/EditPageTemplateViewHolder;->q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v3, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2438254
    :cond_0
    :goto_0
    return-void

    .line 2438255
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2438256
    :cond_1
    instance-of v0, p1, LX/HDL;

    if-eqz v0, :cond_2

    move-object v1, p1

    .line 2438257
    check-cast v1, LX/HDL;

    iget-wide v2, p0, LX/HCC;->k:J

    iget-object v4, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-direct {p0}, LX/HCC;->e()Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v5

    new-instance v6, LX/HC3;

    invoke-direct {v6, p0}, LX/HC3;-><init>(LX/HCC;)V

    invoke-virtual/range {v1 .. v6}, LX/HDL;->a(JLcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2438258
    :cond_2
    instance-of v0, p1, LX/HDQ;

    if-eqz v0, :cond_0

    move-object v1, p1

    .line 2438259
    check-cast v1, LX/HDQ;

    iget-wide v2, p0, LX/HCC;->k:J

    iget-object v4, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-direct {p0}, LX/HCC;->e()Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v5

    new-instance v6, LX/HC4;

    invoke-direct {v6, p0}, LX/HC4;-><init>(LX/HCC;)V

    new-instance v7, LX/HC5;

    invoke-direct {v7, p0}, LX/HC5;-><init>(LX/HCC;)V

    new-instance v8, LX/HC6;

    invoke-direct {v8, p0}, LX/HC6;-><init>(LX/HCC;)V

    invoke-virtual/range {v1 .. v8}, LX/HDQ;->a(JLcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;)V
    .locals 6
    .param p1    # Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setEditPageData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2438224
    iput-object p1, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2438225
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/HCC;->q:LX/15i;

    const/4 v0, 0x0

    iput v0, p0, LX/HCC;->r:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2438226
    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    if-eqz v0, :cond_1

    .line 2438227
    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->mc_()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2438228
    invoke-virtual {v4, v3, v1}, LX/15i;->h(II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2438229
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    iput-object v4, p0, LX/HCC;->q:LX/15i;

    iput v3, p0, LX/HCC;->r:I

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2438230
    :cond_1
    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/HCC;->s:Z

    .line 2438231
    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->b()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->b()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_1
    iput-boolean v1, p0, LX/HCC;->t:Z

    .line 2438232
    iget-object v0, p0, LX/HCC;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2438233
    invoke-static {p0}, LX/HCC;->f(LX/HCC;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2438234
    iget-object v0, p0, LX/HCC;->o:Ljava/util/List;

    sget-object v1, LX/HCB;->TEMPLATE:LX/HCB;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2438235
    :cond_2
    iget-object v0, p0, LX/HCC;->j:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->b()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->b()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2438236
    if-eqz v0, :cond_3

    .line 2438237
    iget-object v0, p0, LX/HCC;->o:Ljava/util/List;

    sget-object v1, LX/HCB;->BUTTONS:LX/HCB;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2438238
    :cond_3
    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-interface {v0}, LX/HCg;->a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-interface {v0}, LX/HCg;->a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2438239
    if-eqz v0, :cond_4

    .line 2438240
    iget-object v0, p0, LX/HCC;->o:Ljava/util/List;

    sget-object v1, LX/HCB;->TABS:LX/HCB;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2438241
    :cond_4
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2438242
    return-void

    .line 2438243
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2438244
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_5
    move v0, v2

    .line 2438245
    goto/16 :goto_0

    :cond_6
    move v1, v2

    .line 2438246
    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2438222
    iget-object v0, p0, LX/HCC;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HCB;

    invoke-virtual {v0}, LX/HCB;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2438223
    iget-object v0, p0, LX/HCC;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
