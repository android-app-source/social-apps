.class public final LX/FVm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FUw;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/FVn;


# direct methods
.method public constructor <init>(LX/FVn;Z)V
    .locals 0

    .prologue
    .line 2251074
    iput-object p1, p0, LX/FVm;->b:LX/FVn;

    iput-boolean p2, p0, LX/FVm;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2251075
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-boolean v1, p0, LX/FVm;->a:Z

    invoke-static {v0, v1}, LX/FVn;->c(LX/FVn;Z)V

    .line 2251076
    return-void
.end method

.method public final a(LX/FVy;)V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2251077
    iget-object v0, p1, LX/FVy;->a:LX/0am;

    move-object v0, v0

    .line 2251078
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2251079
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-boolean v1, p0, LX/FVm;->a:Z

    invoke-static {v0, v1}, LX/FVn;->c(LX/FVn;Z)V

    .line 2251080
    :goto_0
    return-void

    .line 2251081
    :cond_0
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-object v0, v0, LX/FVn;->o:LX/FVq;

    .line 2251082
    iget-object v3, v0, LX/FVq;->b:LX/0Yb;

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/FVq;->b:LX/0Yb;

    invoke-virtual {v3}, LX/0Yb;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2251083
    iget-object v3, v0, LX/FVq;->b:LX/0Yb;

    invoke-virtual {v3}, LX/0Yb;->c()V

    .line 2251084
    :cond_1
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-object v0, v0, LX/FVn;->b:LX/FWe;

    .line 2251085
    iget-object v3, v0, LX/FWe;->a:LX/1Ck;

    const-string v4, "task_key_fetch_cached_only_saved_items"

    invoke-virtual {v3, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2251086
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-object v0, v0, LX/FVn;->j:LX/FVS;

    invoke-virtual {v0}, LX/FVS;->d()V

    .line 2251087
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-object v0, v0, LX/FVn;->r:LX/FWC;

    invoke-virtual {v0}, LX/FWC;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2251088
    :goto_1
    iget-object v3, p0, LX/FVm;->b:LX/FVn;

    iget-object v3, v3, LX/FVn;->r:LX/FWC;

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2251089
    iget-object v4, v3, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v6

    .line 2251090
    :goto_2
    move v3, v4

    .line 2251091
    if-eqz v3, :cond_6

    .line 2251092
    iget-object v3, p0, LX/FVm;->b:LX/FVn;

    iget-object v3, v3, LX/FVn;->q:LX/0g8;

    invoke-interface {v3}, LX/0g8;->n()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2251093
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    sget-object v3, LX/FWA;->FROM_SERVER:LX/FWA;

    .line 2251094
    invoke-static {v0, p1, v3}, LX/FVn;->a$redex0(LX/FVn;LX/FVy;LX/FWA;)V

    .line 2251095
    move v3, v2

    .line 2251096
    :goto_3
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-object v0, v0, LX/FVn;->d:LX/FVO;

    invoke-virtual {v0}, LX/FVO;->d()V

    .line 2251097
    iget-object v0, p0, LX/FVm;->b:LX/FVn;

    iget-object v4, v0, LX/FVn;->j:LX/FVS;

    .line 2251098
    iget-object v0, p1, LX/FVy;->a:LX/0am;

    move-object v0, v0

    .line 2251099
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2251100
    :goto_4
    iget-object v5, v4, LX/FVS;->g:LX/FWh;

    .line 2251101
    const-string v9, "SAVED_FRESH_ITEM_LOAD"

    const-string v10, "SAVED_DASH_START_EARLY_FETCH_ADVANTAGE"

    iget-object v8, v5, LX/FWh;->e:Ljava/lang/Long;

    if-nez v8, :cond_d

    const-string v8, ""

    :goto_5
    invoke-static {v10, v8}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v8

    invoke-static {v5, v9, v8}, LX/FWh;->a(LX/FWh;Ljava/lang/String;LX/0P1;)V

    .line 2251102
    invoke-static {v4}, LX/FVS;->k(LX/FVS;)V

    .line 2251103
    const/4 v5, 0x2

    iput v5, v4, LX/FVS;->d:I

    .line 2251104
    if-eqz v3, :cond_c

    .line 2251105
    iget-object v5, v4, LX/FVS;->g:LX/FWh;

    invoke-virtual {v5}, LX/FWh;->k()V

    .line 2251106
    :cond_2
    :goto_6
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 2251107
    goto :goto_1

    .line 2251108
    :cond_4
    iget-object v3, p0, LX/FVm;->b:LX/FVn;

    .line 2251109
    iput-object p1, v3, LX/FVn;->t:LX/FVy;

    .line 2251110
    iget-object v3, p0, LX/FVm;->b:LX/FVn;

    iget-object v3, v3, LX/FVn;->l:LX/FVX;

    .line 2251111
    iget-object v4, v3, LX/FVX;->a:Landroid/view/View;

    if-nez v4, :cond_5

    .line 2251112
    iget-object v4, v3, LX/FVX;->b:Landroid/view/ViewStub;

    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    iput-object v4, v3, LX/FVX;->a:Landroid/view/View;

    .line 2251113
    :cond_5
    iget-object v4, v3, LX/FVX;->a:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2251114
    iget-object v4, v3, LX/FVX;->a:Landroid/view/View;

    new-instance v5, LX/FVW;

    invoke-direct {v5, v3}, LX/FVW;-><init>(LX/FVX;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2251115
    :cond_6
    move v3, v0

    goto :goto_3

    :cond_7
    move v1, v2

    .line 2251116
    goto :goto_4

    .line 2251117
    :cond_8
    iget-object v4, p1, LX/FVy;->a:LX/0am;

    move-object v4, v4

    .line 2251118
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 2251119
    invoke-virtual {v4, v5}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Px;

    .line 2251120
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v9

    .line 2251121
    iget-object v5, v3, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eq v5, v9, :cond_9

    move v4, v6

    .line 2251122
    goto/16 :goto_2

    :cond_9
    move v8, v7

    .line 2251123
    :goto_7
    if-ge v8, v9, :cond_b

    .line 2251124
    iget-object v5, v3, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FVs;

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    move v4, v6

    .line 2251125
    goto/16 :goto_2

    .line 2251126
    :cond_a
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    goto :goto_7

    :cond_b
    move v4, v7

    .line 2251127
    goto/16 :goto_2

    .line 2251128
    :cond_c
    if-nez v1, :cond_2

    .line 2251129
    iget-object v5, v4, LX/FVS;->i:LX/FVp;

    new-instance v6, LX/FVR;

    invoke-direct {v6, v4}, LX/FVR;-><init>(LX/FVS;)V

    iget-object v7, v4, LX/FVS;->f:LX/0am;

    invoke-virtual {v5, v6, v7}, LX/FVp;->a(LX/FVP;LX/0am;)Z

    move-result v5

    .line 2251130
    if-eqz v5, :cond_2

    .line 2251131
    iget-object v5, v4, LX/FVS;->g:LX/FWh;

    invoke-virtual {v5}, LX/FWh;->k()V

    goto :goto_6

    .line 2251132
    :cond_d
    iget-object v8, v5, LX/FWh;->e:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_5
.end method
