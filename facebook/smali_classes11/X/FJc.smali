.class public final LX/FJc;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "Ljava/util/List",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FJf;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/FJf;I)V
    .locals 0

    .prologue
    .line 2223701
    iput-object p1, p0, LX/FJc;->c:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iput-object p2, p0, LX/FJc;->a:LX/FJf;

    iput p3, p0, LX/FJc;->b:I

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 2223702
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2223703
    :cond_0
    iget-object v0, p0, LX/FJc;->a:LX/FJf;

    invoke-interface {v0}, LX/FJf;->a()V

    .line 2223704
    :cond_1
    return-void

    .line 2223705
    :cond_2
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2223706
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2223707
    iget-object v1, p0, LX/FJc;->c:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget v2, p0, LX/FJc;->b:I

    invoke-static {v1, v0, v2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a$redex0(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Ljava/util/List;I)LX/1FJ;

    move-result-object v1

    .line 2223708
    if-eqz v1, :cond_3

    .line 2223709
    iget-object v2, p0, LX/FJc;->a:LX/FJf;

    invoke-interface {v2, v1}, LX/FJf;->a(LX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2223710
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 2223711
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_1

    .line 2223712
    :cond_3
    :try_start_1
    iget-object v1, p0, LX/FJc;->a:LX/FJf;

    invoke-interface {v1}, LX/FJf;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2223713
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 2223714
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_2

    .line 2223715
    :cond_4
    :try_start_2
    iget-object v1, p0, LX/FJc;->a:LX/FJf;

    invoke-interface {v1}, LX/FJf;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2223716
    :cond_5
    throw v1
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 2223717
    iget-object v0, p0, LX/FJc;->a:LX/FJf;

    invoke-interface {v0}, LX/FJf;->a()V

    .line 2223718
    return-void
.end method
