.class public LX/GwK;
.super LX/Gvy;
.source ""


# static fields
.field private static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final l:LX/GwZ;

.field private final m:LX/4hp;

.field private final n:LX/2Qw;

.field private final o:LX/1nC;

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/74n;

.field private final r:LX/8PL;

.field public s:LX/8PF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2406999
    const-class v0, LX/GwK;

    sput-object v0, LX/GwK;->k:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;LX/8PL;LX/0aG;LX/0Zb;LX/GwZ;Ljava/util/concurrent/Executor;LX/1nC;LX/2Qw;LX/0Or;LX/4hp;LX/74n;LX/BM1;LX/0TD;LX/BKe;Lcom/facebook/content/SecureContextHelper;)V
    .locals 13
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/8PL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/platform/annotations/AreNativeSharePhotoThumbnailsEnabled;
        .end annotation
    .end param
    .param p13    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/8PL;",
            "LX/0aG;",
            "LX/0Zb;",
            "LX/GwZ;",
            "Ljava/util/concurrent/Executor;",
            "LX/1nC;",
            "LX/2Qw;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/4hp;",
            "LX/74n;",
            "LX/BM1;",
            "LX/0TD;",
            "LX/BKe;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406989
    const/16 v6, 0x83

    invoke-virtual {p2}, LX/4hh;->a()Lcom/facebook/platform/common/action/PlatformAppCall;

    move-result-object v7

    invoke-virtual {p2}, LX/8PL;->g()Z

    move-result v8

    move-object v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p6

    move-object v5, p1

    move-object/from16 v9, p13

    move-object/from16 v10, p12

    move-object/from16 v11, p14

    move-object/from16 v12, p15

    invoke-direct/range {v1 .. v12}, LX/Gvy;-><init>(LX/0aG;LX/0Zb;Ljava/util/concurrent/Executor;Landroid/app/Activity;ILcom/facebook/platform/common/action/PlatformAppCall;ZLX/0TD;LX/BM1;LX/BKe;Lcom/facebook/content/SecureContextHelper;)V

    .line 2406990
    move-object/from16 v0, p5

    iput-object v0, p0, LX/GwK;->l:LX/GwZ;

    .line 2406991
    move-object/from16 v0, p7

    iput-object v0, p0, LX/GwK;->o:LX/1nC;

    .line 2406992
    move-object/from16 v0, p8

    iput-object v0, p0, LX/GwK;->n:LX/2Qw;

    .line 2406993
    move-object/from16 v0, p9

    iput-object v0, p0, LX/GwK;->p:LX/0Or;

    .line 2406994
    move-object/from16 v0, p10

    iput-object v0, p0, LX/GwK;->m:LX/4hp;

    .line 2406995
    move-object/from16 v0, p11

    iput-object v0, p0, LX/GwK;->q:LX/74n;

    .line 2406996
    iput-object p2, p0, LX/GwK;->r:LX/8PL;

    .line 2406997
    iget-object v1, p0, LX/GwK;->n:LX/2Qw;

    invoke-virtual {v1, p2}, LX/2Qw;->a(LX/8PL;)LX/8PF;

    move-result-object v1

    iput-object v1, p0, LX/GwK;->s:LX/8PF;

    .line 2406998
    return-void
.end method

.method private a(LX/0m9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2406929
    iget-object v0, p0, LX/GwK;->s:LX/8PF;

    .line 2406930
    iget-object v1, v0, LX/8PF;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2406931
    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2406932
    if-nez v0, :cond_0

    .line 2406933
    new-instance v0, LX/GwJ;

    invoke-direct {v0}, LX/GwJ;-><init>()V

    throw v0

    .line 2406934
    :cond_0
    new-instance v1, LX/2ro;

    invoke-direct {v1}, LX/2ro;-><init>()V

    iget-boolean v2, p0, LX/Gvy;->d:Z

    .line 2406935
    iput-boolean v2, v1, LX/2ro;->c:Z

    .line 2406936
    move-object v1, v1

    .line 2406937
    iget-object v2, p0, LX/GwK;->s:LX/8PF;

    .line 2406938
    iget-object v3, v2, LX/8PF;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2406939
    invoke-virtual {p1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, LX/2ro;->a:Ljava/lang/String;

    .line 2406940
    iput-object v2, v1, LX/2ro;->b:Ljava/lang/String;

    .line 2406941
    move-object v1, v1

    .line 2406942
    iget-object v2, p0, LX/GwK;->r:LX/8PL;

    .line 2406943
    iget-object v3, v2, LX/8PL;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2406944
    iput-object v2, v1, LX/2ro;->e:Ljava/lang/String;

    .line 2406945
    move-object v1, v1

    .line 2406946
    const/4 v2, 0x0

    .line 2406947
    invoke-static {p0}, LX/GwK;->d(LX/GwK;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2406948
    :cond_1
    :goto_0
    move-object v2, v2

    .line 2406949
    iput-object v2, v1, LX/2ro;->j:Lcom/facebook/ipc/composer/intent/SharePreview;

    .line 2406950
    move-object v1, v1

    .line 2406951
    invoke-virtual {v1}, LX/2ro;->a()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v1

    .line 2406952
    sget-object v2, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    const-string v3, "openGraphActionDialogActionExecutor"

    invoke-static {v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    sget-object v3, LX/2rt;->SHARE:LX/2rt;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 v3, 0x0

    .line 2406953
    invoke-static {p0}, LX/GwK;->d(LX/GwK;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2406954
    :cond_2
    :goto_1
    move-object v0, v3

    .line 2406955
    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableAttachToAlbum(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v2, "ANDROID_PLATFORM_COMPOSER"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v2, "platform_composer"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPlatformConfiguration(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0

    .line 2406956
    :cond_3
    iget-object v3, p0, LX/GwK;->s:LX/8PF;

    .line 2406957
    invoke-static {v3}, LX/8PF;->i(LX/8PF;)V

    .line 2406958
    iget-object v5, v3, LX/8PF;->i:Landroid/net/Uri;

    move-object v3, v5

    .line 2406959
    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2406960
    iget-object v5, p0, LX/GwK;->s:LX/8PF;

    iget-object v6, p0, LX/GwK;->s:LX/8PF;

    .line 2406961
    iget-object p1, v6, LX/8PF;->e:Ljava/lang/String;

    move-object v6, p1

    .line 2406962
    invoke-static {v5}, LX/8PF;->i(LX/8PF;)V

    .line 2406963
    iget-object p1, v5, LX/8PF;->h:Ljava/util/Set;

    invoke-interface {p1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    move v5, p1

    .line 2406964
    if-eqz v5, :cond_1

    .line 2406965
    const-string v5, "title"

    invoke-static {v0, v5}, LX/GwK;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2406966
    const-string v6, "description"

    invoke-static {v0, v6}, LX/GwK;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2406967
    new-instance p1, LX/89O;

    invoke-direct {p1}, LX/89O;-><init>()V

    .line 2406968
    iput-object v5, p1, LX/89O;->a:Ljava/lang/String;

    .line 2406969
    move-object v5, p1

    .line 2406970
    iput-object v6, v5, LX/89O;->b:Ljava/lang/String;

    .line 2406971
    move-object v5, v5

    .line 2406972
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2406973
    :cond_4
    iput-object v2, v5, LX/89O;->d:Ljava/lang/String;

    .line 2406974
    move-object v2, v5

    .line 2406975
    invoke-virtual {v2}, LX/89O;->a()Lcom/facebook/ipc/composer/intent/SharePreview;

    move-result-object v2

    goto :goto_0

    .line 2406976
    :cond_5
    if-eqz v3, :cond_1

    .line 2406977
    new-instance v2, LX/89O;

    invoke-direct {v2}, LX/89O;-><init>()V

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2406978
    iput-object v3, v2, LX/89O;->d:Ljava/lang/String;

    .line 2406979
    move-object v2, v2

    .line 2406980
    const/4 v3, 0x1

    .line 2406981
    iput-boolean v3, v2, LX/89O;->e:Z

    .line 2406982
    move-object v2, v2

    .line 2406983
    invoke-virtual {v2}, LX/89O;->a()Lcom/facebook/ipc/composer/intent/SharePreview;

    move-result-object v2

    goto/16 :goto_0

    .line 2406984
    :cond_6
    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2406985
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    .line 2406986
    if-eqz v3, :cond_7

    const-string v5, "http"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2406987
    invoke-static {v3}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v3

    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    goto/16 :goto_1

    .line 2406988
    :cond_7
    const v5, -0x4dba1a9d

    invoke-static {v3, v5}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v3

    invoke-static {v3}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v3

    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    goto/16 :goto_1
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2406831
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2406832
    if-nez v0, :cond_0

    .line 2406833
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "og:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2406834
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0lF;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2406835
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2406836
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/GwK;Landroid/content/Intent;Landroid/os/Bundle;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406923
    iget-object v0, p0, LX/GwK;->s:LX/8PF;

    invoke-virtual {v0, p2}, LX/8PF;->a(Landroid/os/Bundle;)LX/0m9;

    move-result-object v0

    .line 2406924
    const-string v1, "tags"

    invoke-virtual {v0, v1}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 2406925
    const-string v1, "place"

    invoke-virtual {v0, v1}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 2406926
    iget-object v1, p0, LX/GwK;->l:LX/GwZ;

    iget-object v2, p0, LX/GwK;->s:LX/8PF;

    .line 2406927
    iget-object p0, v2, LX/8PF;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2406928
    invoke-virtual {v1, p1, v2, v0}, LX/GwZ;->a(Landroid/content/Intent;Ljava/lang/String;LX/0m9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/GwK;)Z
    .locals 2

    .prologue
    .line 2406922
    iget-object v0, p0, LX/GwK;->s:LX/8PF;

    invoke-virtual {v0}, LX/8PF;->f()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/GwK;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406909
    iget-object v0, p0, LX/GwK;->s:LX/8PF;

    invoke-virtual {v0}, LX/8PF;->e()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2406910
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/GwK;->a$redex0(LX/GwK;Landroid/content/Intent;Landroid/os/Bundle;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406911
    :goto_0
    return-object v0

    .line 2406912
    :cond_0
    iget-object v0, p0, LX/GwK;->l:LX/GwZ;

    iget-object v1, p0, LX/GwK;->s:LX/8PF;

    invoke-virtual {v1}, LX/8PF;->e()LX/0P1;

    move-result-object v1

    .line 2406913
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2406914
    new-instance v3, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;

    invoke-direct {v3, v1}, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;-><init>(LX/0P1;)V

    .line 2406915
    const-string v4, "platform_upload_staging_resource_photos_params"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2406916
    const-string v3, "platform_upload_staging_resource_photos"

    .line 2406917
    iget-object v4, v0, LX/GwZ;->b:LX/0aG;

    const v1, 0x1c3dda55

    invoke-static {v4, v3, v2, v1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2406918
    move-object v2, v4

    .line 2406919
    move-object v0, v2

    .line 2406920
    new-instance v1, LX/GwI;

    invoke-direct {v1, p0, p1}, LX/GwI;-><init>(LX/GwK;Landroid/content/Intent;)V

    .line 2406921
    invoke-static {v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 2406904
    iget-object v0, p0, LX/GwK;->m:LX/4hp;

    iget-object v1, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406905
    iget-object v2, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2406906
    invoke-virtual {v0, v1}, LX/4hp;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2406907
    invoke-super {p0}, LX/Gvy;->a()V

    .line 2406908
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2406899
    if-eqz p1, :cond_0

    .line 2406900
    const-string v0, "action_processor"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;

    .line 2406901
    iget-object v1, p0, LX/GwK;->n:LX/2Qw;

    invoke-virtual {v1, v0}, LX/2Qw;->a(Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;)LX/8PF;

    move-result-object v0

    iput-object v0, p0, LX/GwK;->s:LX/8PF;

    .line 2406902
    :cond_0
    invoke-super {p0, p1}, LX/Gvy;->a(Landroid/os/Bundle;)V

    .line 2406903
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/8Or;
    .locals 2

    .prologue
    .line 2406892
    invoke-super {p0, p1}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v0

    const-string v1, "android_og_dialog"

    .line 2406893
    iput-object v1, v0, LX/8Or;->h:Ljava/lang/String;

    .line 2406894
    move-object v0, v0

    .line 2406895
    const-string v1, "ogshare"

    .line 2406896
    iput-object v1, v0, LX/8Or;->g:Ljava/lang/String;

    .line 2406897
    move-object v0, v0

    .line 2406898
    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406842
    :try_start_0
    iget-object v0, p0, LX/GwK;->s:LX/8PF;

    invoke-virtual {v0}, LX/8PF;->h()V
    :try_end_0
    .catch LX/8PD; {:try_start_0 .. :try_end_0} :catch_0

    .line 2406843
    iget-object v0, p0, LX/GwK;->s:LX/8PF;

    invoke-virtual {v0}, LX/8PF;->a()LX/0m9;

    move-result-object v0

    .line 2406844
    :try_start_1
    invoke-direct {p0, v0}, LX/GwK;->a(LX/0m9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    :try_end_1
    .catch LX/GwJ; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2406845
    const-string v2, "place"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2406846
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0lF;->o()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2406847
    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Gvy;->c(Ljava/lang/String;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    .line 2406848
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v2

    invoke-virtual {v2}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406849
    :cond_0
    const-string v2, "place"

    invoke-virtual {v0, v2}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 2406850
    invoke-static {p0}, LX/GwK;->d(LX/GwK;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2406851
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2406852
    :goto_0
    move-object v1, v2

    .line 2406853
    const-string v2, "tags"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 2406854
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2406855
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0lF;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2406856
    invoke-virtual {v3}, LX/0lF;->e()I

    move-result v5

    .line 2406857
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_1

    .line 2406858
    invoke-virtual {v3, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v6

    .line 2406859
    invoke-virtual {v6}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2406860
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2406861
    :cond_1
    const-string v2, "tags"

    invoke-virtual {v0, v2}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 2406862
    new-instance v2, LX/GwG;

    invoke-direct {v2, p0, v4}, LX/GwG;-><init>(LX/GwK;Ljava/util/ArrayList;)V

    .line 2406863
    iget-object v3, p0, LX/Gvy;->h:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2406864
    :goto_2
    return-object v0

    .line 2406865
    :catch_0
    move-exception v0

    .line 2406866
    iget-object v1, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406867
    invoke-virtual {v0}, LX/8PD;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    .line 2406868
    if-eqz v2, :cond_6

    .line 2406869
    invoke-virtual {v0}, LX/8PD;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 2406870
    :goto_3
    move-object v1, v2

    .line 2406871
    if-eqz v1, :cond_2

    .line 2406872
    invoke-virtual {p0, v1}, LX/4hY;->c(Landroid/os/Bundle;)V

    .line 2406873
    :goto_4
    const/4 v0, 0x0

    goto :goto_2

    .line 2406874
    :cond_2
    invoke-virtual {v0}, LX/8PD;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Gvy;->a(Ljava/lang/String;)V

    goto :goto_4

    .line 2406875
    :catch_1
    move-exception v0

    .line 2406876
    invoke-virtual {v0}, LX/GwJ;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/Gvy;->a(Ljava/lang/String;)V

    .line 2406877
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 2406878
    :cond_3
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2406879
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2406880
    iget-object v2, p0, LX/GwK;->s:LX/8PF;

    invoke-virtual {v2}, LX/8PF;->f()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2406881
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    const-string v7, "content"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2406882
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2406883
    :cond_4
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2406884
    :cond_5
    new-instance v2, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;

    iget-object v5, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406885
    iget-object v6, v5, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2406886
    const-string v6, ".jpeg"

    invoke-direct {v2, v5, v4, v6}, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 2406887
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2406888
    const-string v6, "platform_copy_platform_app_content_params"

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2406889
    iget-object v2, p0, LX/Gvy;->b:LX/0aG;

    const-string v6, "platform_copy_platform_app_content"

    const v7, -0x56ed28f3

    invoke-static {v2, v6, v5, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    move-object v2, v2

    .line 2406890
    new-instance v4, LX/GwH;

    invoke-direct {v4, p0, v3, v1}, LX/GwH;-><init>(LX/GwK;LX/0Pz;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 2406891
    iget-object v3, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v2, v4, v3}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_3
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2406837
    invoke-super {p0, p1}, LX/Gvy;->b(Landroid/os/Bundle;)V

    .line 2406838
    const-string v0, "action_processor"

    iget-object v1, p0, LX/GwK;->s:LX/8PF;

    .line 2406839
    new-instance p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;

    invoke-direct {p0, v1}, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;-><init>(LX/8PF;)V

    move-object v1, p0

    .line 2406840
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2406841
    return-void
.end method
