.class public LX/HBs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0tX;

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0tX;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2437984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2437985
    iput-object p1, p0, LX/HBs;->a:Landroid/content/Context;

    .line 2437986
    iput-object p2, p0, LX/HBs;->b:LX/0tX;

    .line 2437987
    iput-object p3, p0, LX/HBs;->c:LX/0Uh;

    .line 2437988
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2437989
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2437990
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2437991
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2437992
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2437993
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/HBs;
    .locals 4

    .prologue
    .line 2437982
    new-instance v3, LX/HBs;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, LX/HBs;-><init>(Landroid/content/Context;LX/0tX;LX/0Uh;)V

    .line 2437983
    return-object v3
.end method
