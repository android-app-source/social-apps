.class public LX/Gxb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0dz;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2408300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2408301
    iput-object p1, p0, LX/Gxb;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2408302
    iput-object p2, p0, LX/Gxb;->b:LX/0dz;

    .line 2408303
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 3

    .prologue
    .line 2408304
    iget-object v0, p0, LX/Gxb;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/288;->c:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2408305
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2408306
    iget-object v1, p0, LX/Gxb;->b:LX/0dz;

    invoke-virtual {v1, v0}, LX/0dz;->a(Ljava/lang/String;)V

    .line 2408307
    iget-object v0, p0, LX/Gxb;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/288;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2408308
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
