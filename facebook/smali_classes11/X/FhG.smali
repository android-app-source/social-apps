.class public final LX/FhG;
.super LX/4d9;
.source ""


# instance fields
.field public final synthetic a:LX/7B6;

.field public final synthetic b:LX/FhH;


# direct methods
.method public constructor <init>(LX/FhH;LX/7B6;)V
    .locals 0

    .prologue
    .line 2272487
    iput-object p1, p0, LX/FhG;->b:LX/FhH;

    iput-object p2, p0, LX/FhG;->a:LX/7B6;

    invoke-direct {p0}, LX/4d9;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 2272488
    iget-object v0, p0, LX/FhG;->b:LX/FhH;

    iget-object v1, p0, LX/FhG;->a:LX/7B6;

    invoke-virtual {v0, v1, p1}, LX/7HT;->a(LX/7B6;Ljava/lang/Throwable;)V

    .line 2272489
    iget-object v0, p0, LX/FhG;->b:LX/FhH;

    sget-object v1, LX/7HZ;->ERROR:LX/7HZ;

    .line 2272490
    invoke-virtual {v0, v1}, LX/7HT;->a(LX/7HZ;)V

    .line 2272491
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2272492
    iget-object v0, p0, LX/FhG;->b:LX/FhH;

    iget-object v0, v0, LX/FhH;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhX;

    check-cast p1, LX/7Hc;

    iget-object v1, p0, LX/FhG;->b:LX/FhH;

    iget-object v2, v1, LX/FhH;->b:LX/7BZ;

    iget-object v1, p0, LX/FhG;->a:LX/7B6;

    check-cast v1, Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, p1, v2, v1}, LX/FhX;->a(LX/7Hc;LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;)LX/7Hc;

    move-result-object v0

    .line 2272493
    new-instance v1, LX/7Hi;

    iget-object v2, p0, LX/FhG;->a:LX/7B6;

    sget-object v3, LX/7HY;->REMOTE:LX/7HY;

    sget-object v4, LX/7Ha;->EXACT:LX/7Ha;

    invoke-direct {v1, v2, v0, v3, v4}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    .line 2272494
    iget v0, p0, LX/4d9;->b:I

    move v2, v0

    .line 2272495
    invoke-virtual {p0}, LX/4d9;->a()V

    .line 2272496
    iget-object v0, p0, LX/FhG;->b:LX/FhH;

    iget-object v0, v0, LX/FhH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v3, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;

    invoke-direct {v3, p0, v1, v2}, Lcom/facebook/search/suggestions/fetchers/BaseRemoteTypeaheadFetcher$1$1;-><init>(LX/FhG;LX/7Hi;I)V

    invoke-virtual {v0, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2272497
    return-void
.end method
