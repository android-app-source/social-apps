.class public final LX/GxT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWC;


# instance fields
.field public final synthetic a:LX/GxZ;


# direct methods
.method public constructor <init>(LX/GxZ;)V
    .locals 0

    .prologue
    .line 2408132
    iput-object p1, p0, LX/GxT;->a:LX/GxZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 2

    .prologue
    .line 2408122
    iget-object v0, p0, LX/GxT;->a:LX/GxZ;

    const/4 p0, 0x1

    .line 2408123
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/GxZ;->k:Z

    .line 2408124
    iput-boolean p0, v0, LX/GxZ;->w:Z

    .line 2408125
    iget-boolean v1, v0, LX/GxZ;->v:Z

    if-nez v1, :cond_0

    .line 2408126
    iput-boolean p0, v0, LX/GxZ;->v:Z

    .line 2408127
    :cond_0
    iget-object v1, v0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v1}, LX/GxF;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2408128
    if-eqz v1, :cond_1

    .line 2408129
    iget-object p0, v0, LX/GxZ;->i:Lcom/facebook/performancelogger/PerformanceLogger;

    const p1, 0x240004

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "FacewebPageRPCLoadCompleted:"

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, p1, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2408130
    :cond_1
    sget-object v1, LX/GxY;->CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

    invoke-virtual {v0, v1}, LX/GxZ;->a(LX/GxY;)V

    .line 2408131
    return-void
.end method
