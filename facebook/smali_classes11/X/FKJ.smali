.class public abstract LX/FKJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/MarkThreadFields;",
        "Lcom/facebook/messaging/service/model/MarkThreadResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/6iW;


# direct methods
.method public constructor <init>(LX/0Or;LX/6iW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/6iW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2224950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224951
    iput-object p1, p0, LX/FKJ;->a:LX/0Or;

    .line 2224952
    iput-object p2, p0, LX/FKJ;->b:LX/6iW;

    .line 2224953
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2224954
    check-cast p1, Lcom/facebook/messaging/service/model/MarkThreadFields;

    const-wide/16 v6, -0x1

    .line 2224955
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2224956
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2224957
    const-string v0, "name"

    iget-object v2, p0, LX/FKJ;->b:LX/6iW;

    invoke-virtual {v2}, LX/6iW;->getApiName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2224958
    const-string v0, "state"

    iget-boolean v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2224959
    iget-wide v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    .line 2224960
    const-string v0, "watermark_timestamp"

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2224961
    :goto_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tag"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2224962
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2224963
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_2

    .line 2224964
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 2224965
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ct_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2224966
    :goto_1
    new-instance v0, LX/14N;

    const-string v1, "markThread"

    const-string v2, "POST"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 2224967
    :cond_0
    iget-object v0, p0, LX/FKJ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_1

    .line 2224968
    const-string v0, "sync_seq_id"

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 2224969
    :cond_1
    const-string v0, "action_id"

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 2224970
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v0, v1}, LX/DoA;->b(J)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2224971
    check-cast p1, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2224972
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2224973
    new-instance v0, LX/6ic;

    invoke-direct {v0}, LX/6ic;-><init>()V

    iget-object v1, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2224974
    iput-object v1, v0, LX/6ic;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2224975
    move-object v0, v0

    .line 2224976
    new-instance v1, Lcom/facebook/messaging/service/model/MarkThreadResult;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/MarkThreadResult;-><init>(LX/6ic;)V

    move-object v0, v1

    .line 2224977
    return-object v0
.end method
