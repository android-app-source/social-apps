.class public LX/Gvn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Gvn;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2c4;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Or;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/2c4;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2406095
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/Gvn;->a:Landroid/content/Context;

    .line 2406096
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    iput-object v0, p0, LX/Gvn;->b:LX/0Ot;

    .line 2406097
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Or;

    iput-object v0, p0, LX/Gvn;->c:LX/0Or;

    .line 2406098
    return-void
.end method

.method public static a(LX/0QB;)LX/Gvn;
    .locals 6

    .prologue
    .line 2406099
    sget-object v0, LX/Gvn;->d:LX/Gvn;

    if-nez v0, :cond_1

    .line 2406100
    const-class v1, LX/Gvn;

    monitor-enter v1

    .line 2406101
    :try_start_0
    sget-object v0, LX/Gvn;->d:LX/Gvn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2406102
    if-eqz v2, :cond_0

    .line 2406103
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2406104
    new-instance v4, LX/Gvn;

    const-class v3, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v5, 0xe7c

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x2acf

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/Gvn;-><init>(Landroid/content/Context;LX/0Ot;LX/0Or;)V

    .line 2406105
    move-object v0, v4

    .line 2406106
    sput-object v0, LX/Gvn;->d:LX/Gvn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2406107
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2406108
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2406109
    :cond_1
    sget-object v0, LX/Gvn;->d:LX/Gvn;

    return-object v0

    .line 2406110
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2406111
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
