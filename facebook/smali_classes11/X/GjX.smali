.class public final LX/GjX;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private final b:Landroid/graphics/Rect;

.field private final c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2387814
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2387815
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/GjX;->b:Landroid/graphics/Rect;

    .line 2387816
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/GjX;->c:Landroid/graphics/Paint;

    .line 2387817
    iget-object v0, p0, LX/GjX;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 2387818
    iget-object v0, p0, LX/GjX;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2387819
    invoke-virtual {p0}, LX/GjX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    mul-int/lit8 v0, v0, 0x30

    int-to-float v0, v0

    invoke-virtual {p0, v0}, LX/GjX;->setCameraDistance(F)V

    .line 2387820
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;I)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 2387825
    iput-object p1, p0, LX/GjX;->a:Landroid/graphics/Bitmap;

    .line 2387826
    iget-object v0, p0, LX/GjX;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 2387827
    iget-object v0, p0, LX/GjX;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2387828
    :cond_0
    :goto_0
    return-void

    .line 2387829
    :cond_1
    iget-object v0, p0, LX/GjX;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 2387830
    iget-object v1, p0, LX/GjX;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 2387831
    const/16 v2, 0x30

    if-ne p2, v2, :cond_2

    .line 2387832
    iget-object v2, p0, LX/GjX;->b:Landroid/graphics/Rect;

    int-to-float v0, v0

    mul-float/2addr v0, v5

    add-float/2addr v0, v5

    float-to-int v0, v0

    invoke-virtual {v2, v4, v4, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 2387833
    :cond_2
    const/16 v2, 0x50

    if-ne p2, v2, :cond_0

    .line 2387834
    iget-object v2, p0, LX/GjX;->b:Landroid/graphics/Rect;

    int-to-float v3, v0

    mul-float/2addr v3, v5

    sub-float/2addr v3, v5

    float-to-int v3, v3

    invoke-virtual {v2, v4, v3, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2387821
    iget-object v0, p0, LX/GjX;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GjX;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2387822
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/GjX;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/GjX;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2387823
    iget-object v1, p0, LX/GjX;->a:Landroid/graphics/Bitmap;

    iget-object v2, p0, LX/GjX;->b:Landroid/graphics/Rect;

    iget-object v3, p0, LX/GjX;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2387824
    :cond_0
    return-void
.end method
