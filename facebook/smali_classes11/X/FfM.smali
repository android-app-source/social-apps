.class public final LX/FfM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FfI;

.field public final synthetic b:LX/FfN;


# direct methods
.method public constructor <init>(LX/FfN;LX/FfI;)V
    .locals 0

    .prologue
    .line 2268040
    iput-object p1, p0, LX/FfM;->b:LX/FfN;

    iput-object p2, p0, LX/FfM;->a:LX/FfI;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 2268041
    iget-object v0, p0, LX/FfM;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->b()V

    .line 2268042
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2268043
    iget-object v0, p0, LX/FfM;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->a()V

    .line 2268044
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2268045
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2268046
    if-eqz p1, :cond_0

    .line 2268047
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268048
    if-eqz v0, :cond_0

    .line 2268049
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268050
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2268051
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268052
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2268053
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268054
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2268055
    :cond_0
    iget-object v0, p0, LX/FfM;->b:LX/FfN;

    iget-object v0, v0, LX/FfN;->f:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v2, "See more remote fetch returned null data"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2268056
    iget-object v0, p0, LX/FfM;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->a()V

    .line 2268057
    :goto_0
    return-void

    .line 2268058
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268059
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    .line 2268060
    iget-object v0, p0, LX/FfM;->b:LX/FfN;

    iget-object v0, v0, LX/FfN;->f:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v2, "Failed to grab page info"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2268061
    iget-object v0, p0, LX/FfM;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->a()V

    goto :goto_0

    .line 2268062
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268063
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v2

    .line 2268064
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2268065
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    move v1, v3

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;

    .line 2268066
    :try_start_0
    iget-object v6, p0, LX/FfM;->b:LX/FfN;

    iget-object v6, v6, LX/FfN;->c:LX/Cwo;

    invoke-virtual {v6, v0}, LX/Cwo;->a(Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;)Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object v0

    .line 2268067
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2268068
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2268069
    :catch_0
    move-exception v0

    .line 2268070
    iget-object v6, p0, LX/FfM;->b:LX/FfN;

    iget-object v6, v6, LX/FfN;->f:LX/2Sc;

    invoke-virtual {v6, v0}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_2

    .line 2268071
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2268072
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268073
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->j()LX/0Px;

    move-result-object v4

    .line 2268074
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2268075
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_3
    if-ge v2, v6, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel;

    .line 2268076
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 2268077
    :try_start_1
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;

    move-result-object v0

    invoke-static {v0}, LX/Cwp;->a(Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;)LX/CwL;

    move-result-object v0

    .line 2268078
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch LX/7C4; {:try_start_1 .. :try_end_1} :catch_1

    .line 2268079
    :cond_4
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2268080
    :catch_1
    move-exception v0

    .line 2268081
    iget-object v7, p0, LX/FfM;->b:LX/FfN;

    iget-object v7, v7, LX/FfN;->f:LX/2Sc;

    invoke-virtual {v7, v0}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_4

    .line 2268082
    :cond_5
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2268083
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2268084
    iget-object v0, p0, LX/FfM;->b:LX/FfN;

    iget-object v0, v0, LX/FfN;->f:LX/2Sc;

    sget-object v4, LX/3Ql;->FETCH_NEEDLE_FILTERS_FAIL:LX/3Ql;

    const-string v5, "No needle filters received, filter bar will disappear!"

    invoke-virtual {v0, v4, v5}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2268085
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268086
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 2268087
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2268088
    iget-object v0, p0, LX/FfM;->a:LX/FfI;

    invoke-virtual {v5, v6, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v5, v6, v4}, LX/15i;->h(II)Z

    move-result v4

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, LX/15i;->h(II)Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, LX/FfI;->a(LX/0Px;LX/0Px;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 2268089
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method
