.class public LX/H5u;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H5s;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2425474
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/H5u;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425475
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2425476
    iput-object p1, p0, LX/H5u;->b:LX/0Ot;

    .line 2425477
    return-void
.end method

.method public static a(LX/0QB;)LX/H5u;
    .locals 4

    .prologue
    .line 2425478
    const-class v1, LX/H5u;

    monitor-enter v1

    .line 2425479
    :try_start_0
    sget-object v0, LX/H5u;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2425480
    sput-object v2, LX/H5u;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2425481
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425482
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2425483
    new-instance v3, LX/H5u;

    const/16 p0, 0x2af7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H5u;-><init>(LX/0Ot;)V

    .line 2425484
    move-object v0, v3

    .line 2425485
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2425486
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2425487
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2425488
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2425489
    check-cast p2, LX/H5t;

    .line 2425490
    iget-object v0, p0, LX/H5u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialComponentSpec;

    iget-object v1, p2, LX/H5t;->a:Ljava/lang/String;

    iget-object v2, p2, LX/H5t;->b:Ljava/lang/String;

    const/4 p2, 0x3

    const/4 p0, -0x1

    const/4 v8, 0x1

    .line 2425491
    iget-object v3, v0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialComponentSpec;->b:LX/10M;

    invoke-virtual {v3, v1}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v4

    .line 2425492
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00d1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-interface {v3, v5}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v3, v6}, LX/1up;->c(F)LX/1up;

    move-result-object v6

    iget-object v3, v0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialComponentSpec;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v4}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v3

    sget-object v7, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v6, 0x7f0b0a8b

    invoke-interface {v3, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v6, 0x7f0b0a8b

    invoke-interface {v3, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const v6, 0x7f0b0a9c

    invoke-interface {v3, v8, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v6, 0x7f0b0060

    invoke-interface {v3, p2, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0052

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/notifications/push/loggedoutpush/interstitial/NotificationsLoggedOutPushInterstitialComponentSpec;->a:LX/2g9;

    invoke-virtual {v4, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v4

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f081185

    invoke-virtual {p1, v5}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-virtual {v4, v2}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v4

    const/16 v5, 0x108

    invoke-virtual {v4, v5}, LX/2gA;->h(I)LX/2gA;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f020918

    invoke-interface {v4, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0a9d

    invoke-interface {v4, v8, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0a9e

    invoke-interface {v4, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2425493
    const v4, 0x58bcc846

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2425494
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f081186

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0207fa

    invoke-interface {v4, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    .line 2425495
    const v5, 0x58bcd5d6

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2425496
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2425497
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2425498
    invoke-static {}, LX/1dS;->b()V

    .line 2425499
    iget v0, p1, LX/1dQ;->b:I

    .line 2425500
    sparse-switch v0, :sswitch_data_0

    .line 2425501
    :goto_0
    return-object v2

    .line 2425502
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2425503
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2425504
    check-cast v1, LX/H5t;

    .line 2425505
    iget-object p1, p0, LX/H5u;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/H5t;->c:Landroid/view/View$OnClickListener;

    .line 2425506
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2425507
    goto :goto_0

    .line 2425508
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2425509
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2425510
    check-cast v1, LX/H5t;

    .line 2425511
    iget-object p1, p0, LX/H5u;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/H5t;->d:Landroid/view/View$OnClickListener;

    .line 2425512
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2425513
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x58bcc846 -> :sswitch_0
        0x58bcd5d6 -> :sswitch_1
    .end sparse-switch
.end method
