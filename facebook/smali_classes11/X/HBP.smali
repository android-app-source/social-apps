.class public LX/HBP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/11S;


# direct methods
.method public constructor <init>(LX/0SG;LX/11S;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2437166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2437167
    iput-object p1, p0, LX/HBP;->a:LX/0SG;

    .line 2437168
    iput-object p2, p0, LX/HBP;->b:LX/11S;

    .line 2437169
    return-void
.end method

.method public static b(LX/0QB;)LX/HBP;
    .locals 3

    .prologue
    .line 2437184
    new-instance v2, LX/HBP;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11S;

    invoke-direct {v2, v0, v1}, LX/HBP;-><init>(LX/0SG;LX/11S;)V

    .line 2437185
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Context;J)I
    .locals 4

    .prologue
    .line 2437177
    invoke-virtual {p0, p2, p3}, LX/HBP;->a(J)J

    move-result-wide v0

    .line 2437178
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const-wide/16 v2, 0xa

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 2437179
    const/4 v0, -0x1

    .line 2437180
    :goto_0
    return v0

    .line 2437181
    :cond_0
    const-wide/16 v2, 0x5

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 2437182
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0566

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 2437183
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0567

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(J)J
    .locals 5

    .prologue
    .line 2437176
    iget-object v0, p0, LX/HBP;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final b(Landroid/content/Context;J)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 2437172
    invoke-virtual {p0, p2, p3}, LX/HBP;->a(J)J

    move-result-wide v0

    .line 2437173
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    const-wide/16 v2, 0xa

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 2437174
    const/4 v0, 0x0

    .line 2437175
    :goto_0
    return-object v0

    :cond_0
    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083694

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HBP;->b:LX/11S;

    sget-object v5, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    invoke-interface {v4, v5, p2, p3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083695

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(J)Z
    .locals 5

    .prologue
    .line 2437170
    invoke-virtual {p0, p1, p2}, LX/HBP;->a(J)J

    move-result-wide v0

    .line 2437171
    const-wide/16 v2, 0xa

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
