.class public LX/Fkf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2278851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278852
    return-void
.end method

.method public static a(LX/Fkf;Ljava/lang/String;)LX/0Ve;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2278853
    new-instance v0, LX/Fkc;

    invoke-direct {v0, p0, p1}, LX/Fkc;-><init>(LX/Fkf;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(LX/Fkf;Ljava/lang/String;)LX/0Ve;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2278854
    new-instance v0, LX/Fke;

    invoke-direct {v0, p0, p1}, LX/Fke;-><init>(LX/Fkf;Ljava/lang/String;)V

    return-object v0
.end method
