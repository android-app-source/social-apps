.class public LX/GVl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2359847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2359848
    return-void
.end method

.method public static a(LX/0mj;LX/5pG;)V
    .locals 4

    .prologue
    .line 2359849
    invoke-interface {p1}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v0

    .line 2359850
    :goto_0
    invoke-interface {v0}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2359851
    invoke-interface {v0}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v1

    .line 2359852
    sget-object v2, LX/GVk;->a:[I

    invoke-interface {p1, v1}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2359853
    new-instance v0, LX/5p9;

    const-string v1, "Unknown data type"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2359854
    :pswitch_0
    const-string v2, "null"

    invoke-virtual {p0, v1, v2}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    goto :goto_0

    .line 2359855
    :pswitch_1
    invoke-interface {p1, v1}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0mj;

    goto :goto_0

    .line 2359856
    :pswitch_2
    invoke-interface {p1, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    goto :goto_0

    .line 2359857
    :pswitch_3
    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    goto :goto_0

    .line 2359858
    :pswitch_4
    invoke-virtual {p0}, LX/0mj;->c()LX/0n9;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v2

    .line 2359859
    invoke-interface {p1, v1}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v1

    invoke-static {v2, v1}, LX/GVl;->a(LX/0n9;LX/5pG;)V

    goto :goto_0

    .line 2359860
    :pswitch_5
    invoke-virtual {p0}, LX/0mj;->c()LX/0n9;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v2

    .line 2359861
    invoke-interface {p1, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    invoke-static {v2, v1}, LX/GVl;->a(LX/0zL;LX/5pC;)V

    goto :goto_0

    .line 2359862
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(LX/0n9;LX/5pG;)V
    .locals 4

    .prologue
    .line 2359863
    invoke-interface {p1}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v0

    .line 2359864
    :goto_0
    invoke-interface {v0}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2359865
    invoke-interface {v0}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v1

    .line 2359866
    sget-object v2, LX/GVk;->a:[I

    invoke-interface {p1, v1}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2359867
    new-instance v0, LX/5p9;

    const-string v1, "Unknown data type"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2359868
    :pswitch_0
    const-string v2, "null"

    .line 2359869
    invoke-static {p0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2359870
    goto :goto_0

    .line 2359871
    :pswitch_1
    invoke-interface {p1, v1}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2359872
    invoke-static {p0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2359873
    goto :goto_0

    .line 2359874
    :pswitch_2
    invoke-interface {p1, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 2359875
    invoke-static {p0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2359876
    goto :goto_0

    .line 2359877
    :pswitch_3
    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2359878
    invoke-static {p0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2359879
    goto :goto_0

    .line 2359880
    :pswitch_4
    invoke-virtual {p0, v1}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v2

    .line 2359881
    invoke-interface {p1, v1}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v1

    invoke-static {v2, v1}, LX/GVl;->a(LX/0n9;LX/5pG;)V

    goto :goto_0

    .line 2359882
    :pswitch_5
    invoke-virtual {p0, v1}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v2

    .line 2359883
    invoke-interface {p1, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    invoke-static {v2, v1}, LX/GVl;->a(LX/0zL;LX/5pC;)V

    goto :goto_0

    .line 2359884
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(LX/0zL;LX/5pC;)V
    .locals 4

    .prologue
    .line 2359885
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2359886
    sget-object v1, LX/GVk;->a:[I

    invoke-interface {p1, v0}, LX/5pC;->getType(I)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2359887
    new-instance v0, LX/5p9;

    const-string v1, "Unknown data type"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2359888
    :pswitch_0
    const-string v1, "null"

    .line 2359889
    invoke-static {p0, v1}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 2359890
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2359891
    :pswitch_1
    invoke-interface {p1, v0}, LX/5pC;->getBoolean(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2359892
    invoke-static {p0, v1}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 2359893
    goto :goto_1

    .line 2359894
    :pswitch_2
    invoke-interface {p1, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 2359895
    invoke-static {p0, v1}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 2359896
    goto :goto_1

    .line 2359897
    :pswitch_3
    invoke-interface {p1, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2359898
    invoke-static {p0, v1}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 2359899
    goto :goto_1

    .line 2359900
    :pswitch_4
    invoke-virtual {p0}, LX/0zL;->k()LX/0n9;

    move-result-object v1

    .line 2359901
    invoke-interface {p1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    invoke-static {v1, v2}, LX/GVl;->a(LX/0n9;LX/5pG;)V

    goto :goto_1

    .line 2359902
    :pswitch_5
    invoke-virtual {p0}, LX/0zL;->l()LX/0zL;

    move-result-object v1

    .line 2359903
    invoke-interface {p1, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v2

    invoke-static {v1, v2}, LX/GVl;->a(LX/0zL;LX/5pC;)V

    goto :goto_1

    .line 2359904
    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
