.class public final LX/Gfo;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Gfq;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Gfp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Gfq",
            "<TE;>.PaginatedPymlCardBottomComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Gfq;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Gfq;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2377950
    iput-object p1, p0, LX/Gfo;->b:LX/Gfq;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2377951
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Gfo;->c:[Ljava/lang/String;

    .line 2377952
    iput v3, p0, LX/Gfo;->d:I

    .line 2377953
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Gfo;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Gfo;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Gfo;LX/1De;IILX/Gfp;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Gfq",
            "<TE;>.PaginatedPymlCardBottomComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2377954
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2377955
    iput-object p4, p0, LX/Gfo;->a:LX/Gfp;

    .line 2377956
    iget-object v0, p0, LX/Gfo;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2377957
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2377958
    invoke-super {p0}, LX/1X5;->a()V

    .line 2377959
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gfo;->a:LX/Gfp;

    .line 2377960
    iget-object v0, p0, LX/Gfo;->b:LX/Gfq;

    iget-object v0, v0, LX/Gfq;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2377961
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Gfq;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2377962
    iget-object v1, p0, LX/Gfo;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Gfo;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Gfo;->d:I

    if-ge v1, v2, :cond_2

    .line 2377963
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2377964
    :goto_0
    iget v2, p0, LX/Gfo;->d:I

    if-ge v0, v2, :cond_1

    .line 2377965
    iget-object v2, p0, LX/Gfo;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2377966
    iget-object v2, p0, LX/Gfo;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2377967
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2377968
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2377969
    :cond_2
    iget-object v0, p0, LX/Gfo;->a:LX/Gfp;

    .line 2377970
    invoke-virtual {p0}, LX/Gfo;->a()V

    .line 2377971
    return-object v0
.end method
