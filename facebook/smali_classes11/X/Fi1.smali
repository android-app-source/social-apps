.class public LX/Fi1;
.super LX/2SP;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2SP;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2SR;

.field public c:LX/2SR;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2SV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2274060
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 2274061
    new-instance v0, LX/Fi0;

    invoke-direct {v0, p0}, LX/Fi0;-><init>(LX/Fi1;)V

    iput-object v0, p0, LX/Fi1;->b:LX/2SR;

    .line 2274062
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2274063
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SV;

    .line 2274064
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2274065
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274066
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Fi1;->a:LX/0Px;

    .line 2274067
    return-void
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 2274068
    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-static {v0}, LX/2SP;->a(LX/0Px;)LX/7BE;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 4

    .prologue
    .line 2274069
    iput-object p1, p0, LX/Fi1;->c:LX/2SR;

    .line 2274070
    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 2274071
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/Fi1;->b:LX/2SR;

    :goto_1
    invoke-virtual {v0, v1, p2}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    .line 2274072
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2274073
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 2274074
    :cond_1
    return-void
.end method

.method public final a(LX/Cwb;)V
    .locals 3

    .prologue
    .line 2274075
    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 2274076
    invoke-virtual {v0, p1}, LX/2SP;->a(LX/Cwb;)V

    .line 2274077
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2274078
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 5
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2274079
    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 2274080
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, LX/EPu;->MEMORY:LX/EPu;

    if-ne p2, v3, :cond_0

    sget-object v3, LX/7BE;->READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2274081
    :cond_0
    invoke-virtual {v0, p1, p2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2274082
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2274083
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 3

    .prologue
    .line 2274084
    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 2274085
    invoke-virtual {v0, p1}, LX/2SP;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2274086
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2274087
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2274088
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2274089
    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 2274090
    invoke-virtual {v0}, LX/2SP;->c()V

    .line 2274091
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2274092
    :cond_0
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2274093
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2274094
    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, LX/Fi1;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 2274095
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LX/7BE;->NOT_READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2274096
    :cond_0
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2274097
    invoke-virtual {v0}, LX/2SP;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2274098
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2274099
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2274100
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2274101
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
