.class public LX/GlX;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/Gkm;


# instance fields
.field public final c:LX/Glb;

.field public final d:LX/Gld;

.field public final e:LX/DZB;

.field private final f:LX/GkM;

.field private final g:Landroid/content/res/Resources;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field private i:LX/Glk;

.field private j:I

.field public k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/5OM;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2391088
    const-class v0, LX/GlX;

    sput-object v0, LX/GlX;->a:Ljava/lang/Class;

    .line 2391089
    sget-object v0, LX/Gkm;->RECENTLY_VISITED:LX/Gkm;

    sput-object v0, LX/GlX;->b:LX/Gkm;

    return-void
.end method

.method public constructor <init>(LX/Glk;Lcom/facebook/content/SecureContextHelper;LX/DZB;LX/Gld;LX/GkM;Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # LX/Glk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Gld;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2391079
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2391080
    iput-object p2, p0, LX/GlX;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2391081
    iput-object p3, p0, LX/GlX;->e:LX/DZB;

    .line 2391082
    iput-object p6, p0, LX/GlX;->g:Landroid/content/res/Resources;

    .line 2391083
    iput-object p1, p0, LX/GlX;->i:LX/Glk;

    .line 2391084
    iput-object p4, p0, LX/GlX;->d:LX/Gld;

    .line 2391085
    iput-object p5, p0, LX/GlX;->f:LX/GkM;

    .line 2391086
    new-instance v0, LX/Glb;

    invoke-direct {v0, p0}, LX/Glb;-><init>(LX/GlX;)V

    iput-object v0, p0, LX/GlX;->c:LX/Glb;

    .line 2391087
    return-void
.end method

.method private a(LX/Glo;I)V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v9, -0x1

    const/4 v1, 0x0

    .line 2391062
    sget-object v0, LX/GlO;->FAVORITES_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2391063
    iget-object v0, p0, LX/GlX;->f:LX/GkM;

    invoke-virtual {v0}, LX/GkM;->a()Landroid/content/Intent;

    move-result-object v7

    .line 2391064
    iget-object v5, p1, LX/Glo;->l:LX/Gll;

    invoke-static {p2}, LX/GlX;->i(I)I

    move-result v4

    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v2, 0x7f083721

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v7, :cond_1

    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v2, 0x7f083725

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, LX/GlQ;

    invoke-direct {v2, p0, v7}, LX/GlQ;-><init>(LX/GlX;Landroid/content/Intent;)V

    move-object v10, v2

    move-object v2, v0

    move-object v0, v10

    :goto_1
    move-object v10, v0

    move-object v0, v5

    move-object v5, v10

    move-object v11, v2

    move-object v2, v3

    move-object v3, v11

    move v12, v4

    move-object v4, v1

    move v1, v12

    .line 2391065
    :goto_2
    invoke-virtual/range {v0 .. v6}, LX/Gll;->a(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;Z)V

    .line 2391066
    :cond_0
    :goto_3
    return-void

    :cond_1
    move-object v0, v1

    .line 2391067
    goto :goto_0

    .line 2391068
    :cond_2
    sget-object v0, LX/GlO;->RECENTLY_JOINED_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 2391069
    iget-object v5, p1, LX/Glo;->l:LX/Gll;

    invoke-static {p2}, LX/GlX;->i(I)I

    move-result v4

    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v2, 0x7f083722

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-static {v0}, LX/Glb;->a(Ljava/util/HashMap;)Z

    move-result v6

    move-object v2, v3

    move-object v0, v5

    move-object v3, v1

    move-object v5, v1

    move v10, v4

    move-object v4, v1

    move v1, v10

    .line 2391070
    goto :goto_2

    :cond_3
    sget-object v0, LX/GlO;->FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_6

    .line 2391071
    iget-object v0, p1, LX/Glo;->l:LX/Gll;

    invoke-static {p2}, LX/GlX;->i(I)I

    move-result v1

    iget-object v2, p0, LX/GlX;->g:Landroid/content/res/Resources;

    invoke-virtual {p0}, LX/GlX;->d()LX/Gkm;

    move-result-object v3

    invoke-static {v3, v6}, LX/Gla;->a(LX/Gkm;Z)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v4, 0x7f083724

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v5, 0x7f0203f9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    new-instance v5, LX/GlR;

    invoke-direct {v5, p0}, LX/GlR;-><init>(LX/GlX;)V

    iget-object v8, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-static {v8}, LX/Glb;->a(Ljava/util/HashMap;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, LX/GlX;->c:LX/Glb;

    iget v8, v8, LX/Glb;->e:I

    if-eq v8, v9, :cond_5

    :cond_4
    move v6, v7

    :cond_5
    invoke-virtual/range {v0 .. v6}, LX/Gll;->a(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;Z)V

    goto :goto_3

    .line 2391072
    :cond_6
    sget-object v0, LX/GlO;->HIDDEN_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2391073
    iget-object v0, p0, LX/GlX;->k:Ljava/util/HashMap;

    .line 2391074
    sget-object v1, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkp;

    .line 2391075
    if-eqz v1, :cond_b

    .line 2391076
    iget-boolean v0, v1, LX/Gkp;->d:Z

    move v1, v0

    .line 2391077
    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :goto_4
    move v8, v1

    .line 2391078
    iget-object v5, p1, LX/Glo;->l:LX/Gll;

    invoke-static {p2}, LX/GlX;->i(I)I

    move-result v4

    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v1, 0x7f083723

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v8, :cond_8

    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v1, 0x7f083726

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_5
    if-eqz v8, :cond_9

    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v1, 0x7f0203f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_6
    new-instance v1, LX/GlS;

    invoke-direct {v1, p0, v8, p1}, LX/GlS;-><init>(LX/GlX;ZLX/Glo;)V

    iget-object v8, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-static {v8}, LX/Glb;->a(Ljava/util/HashMap;)Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, LX/GlX;->c:LX/Glb;

    iget v8, v8, LX/Glb;->e:I

    if-ne v8, v9, :cond_7

    iget-object v8, p0, LX/GlX;->c:LX/Glb;

    iget v8, v8, LX/Glb;->c:I

    if-eq v8, v9, :cond_a

    :cond_7
    move v6, v7

    move-object v10, v1

    move v1, v4

    move-object v4, v0

    move-object v0, v5

    move-object v5, v10

    move-object v11, v3

    move-object v3, v2

    move-object v2, v11

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v1, 0x7f083727

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    :cond_9
    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v1, 0x7f0219e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_6

    :cond_a
    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_1

    :cond_b
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public static declared-synchronized a$redex0(LX/GlX;Landroid/view/View;)V
    .locals 10

    .prologue
    .line 2391039
    monitor-enter p0

    :try_start_0
    new-instance v1, LX/5OM;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2391040
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 2391041
    iget-object v0, p0, LX/GlX;->g:Landroid/content/res/Resources;

    const v3, 0x7f0a0a24

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/5OG;->a(Landroid/content/res/ColorStateList;)V

    .line 2391042
    invoke-virtual {p0}, LX/GlX;->d()LX/Gkm;

    move-result-object v3

    .line 2391043
    invoke-static {}, LX/Gkm;->values()[LX/Gkm;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    .line 2391044
    const/4 v7, 0x1

    invoke-static {v6, v7}, LX/Gla;->a(LX/Gkm;Z)I

    move-result v7

    .line 2391045
    sget-object v8, LX/GlZ;->a:[I

    invoke-virtual {v6}, LX/Gkm;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 2391046
    const/4 v8, 0x0

    :goto_1
    move v8, v8

    .line 2391047
    invoke-virtual {v2, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    .line 2391048
    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2391049
    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2391050
    invoke-virtual {v6, v3}, LX/Gkm;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 2391051
    new-instance v8, LX/GlT;

    invoke-direct {v8, p0, v6}, LX/GlT;-><init>(LX/GlX;LX/Gkm;)V

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2391052
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2391053
    :cond_0
    invoke-virtual {v1, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2391054
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/GlX;->l:Ljava/lang/ref/WeakReference;

    .line 2391055
    new-instance v0, LX/GlU;

    invoke-direct {v0, p0}, LX/GlU;-><init>(LX/GlX;)V

    .line 2391056
    iput-object v0, v1, LX/0ht;->H:LX/2dD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2391057
    monitor-exit p0

    return-void

    .line 2391058
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2391059
    :pswitch_0
    const v8, 0x7f020092

    goto :goto_1

    .line 2391060
    :pswitch_1
    const v8, 0x7f020de3

    goto :goto_1

    .line 2391061
    :pswitch_2
    const v8, 0x7f021603

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private f()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2391033
    iget-object v0, p0, LX/GlX;->k:Ljava/util/HashMap;

    if-nez v0, :cond_0

    move v0, v1

    .line 2391034
    :goto_0
    return v0

    .line 2391035
    :cond_0
    iget-object v0, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2391036
    invoke-virtual {v0}, LX/Gkp;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2391037
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2391038
    goto :goto_0
.end method

.method public static g(LX/GlX;I)LX/Gkn;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 2391019
    invoke-direct {p0}, LX/GlX;->f()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2391020
    :goto_0
    return-object v0

    .line 2391021
    :cond_0
    iget-object v0, p0, LX/GlX;->k:Ljava/util/HashMap;

    sget-object v2, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2391022
    if-eqz v0, :cond_1

    .line 2391023
    iget-boolean v2, v0, LX/Gkp;->d:Z

    move v0, v2

    .line 2391024
    if-nez v0, :cond_1

    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->f:I

    if-eq v0, v3, :cond_1

    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->f:I

    if-le p1, v0, :cond_1

    .line 2391025
    sget-object v0, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    goto :goto_0

    .line 2391026
    :cond_1
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->c:I

    if-le p1, v0, :cond_2

    .line 2391027
    sget-object v0, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    goto :goto_0

    .line 2391028
    :cond_2
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->e:I

    if-eq v0, v3, :cond_3

    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->e:I

    if-le p1, v0, :cond_3

    .line 2391029
    sget-object v0, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    goto :goto_0

    .line 2391030
    :cond_3
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->a:I

    if-le p1, v0, :cond_4

    .line 2391031
    sget-object v0, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 2391032
    goto :goto_0
.end method

.method private static i(I)I
    .locals 1

    .prologue
    .line 2391013
    sget-object v0, LX/GlO;->FAVORITES_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_0

    .line 2391014
    const v0, 0x7f0d020a

    .line 2391015
    :goto_0
    return v0

    .line 2391016
    :cond_0
    sget-object v0, LX/GlO;->FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 2391017
    const v0, 0x7f0d020b

    goto :goto_0

    .line 2391018
    :cond_1
    const v0, 0x7f0d020c

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2391000
    sget-object v0, LX/GlO;->POG:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2391001
    new-instance v1, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;-><init>(Landroid/content/Context;)V

    .line 2391002
    const/4 v0, 0x1

    .line 2391003
    iput-boolean v0, v1, LX/DaX;->t:Z

    .line 2391004
    new-instance v0, LX/Gln;

    invoke-direct {v0, v1}, LX/Gln;-><init>(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    .line 2391005
    :goto_0
    return-object v0

    .line 2391006
    :cond_0
    sget-object v0, LX/GlO;->FAVORITES_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-eq p2, v0, :cond_1

    sget-object v0, LX/GlO;->FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-eq p2, v0, :cond_1

    sget-object v0, LX/GlO;->RECENTLY_JOINED_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-eq p2, v0, :cond_1

    sget-object v0, LX/GlO;->HIDDEN_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2391007
    :cond_1
    new-instance v0, LX/Glo;

    new-instance v1, LX/Gll;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Gll;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/Glo;-><init>(LX/Gll;)V

    goto :goto_0

    .line 2391008
    :cond_2
    sget-object v0, LX/GlO;->FILTERED_GROUPS_NULL_STATE:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-eq p2, v0, :cond_3

    sget-object v0, LX/GlO;->FAVORITES_NULL_STATE:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-eq p2, v0, :cond_3

    sget-object v0, LX/GlO;->GENERAL_NULL_STATE:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_4

    .line 2391009
    :cond_3
    new-instance v0, LX/Glp;

    new-instance v1, LX/Glm;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Glm;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/Glp;-><init>(LX/Glm;)V

    goto :goto_0

    .line 2391010
    :cond_4
    sget-object v0, LX/GlO;->SECTION_LOADING:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_5

    .line 2391011
    new-instance v0, LX/Glq;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030865

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Glq;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2391012
    :cond_5
    new-instance v0, LX/GlW;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/GlW;-><init>(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2390968
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2390969
    sget-object v1, LX/GlO;->FAVORITES_NULL_STATE:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GlO;->FILTERED_GROUPS_NULL_STATE:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GlO;->GENERAL_NULL_STATE:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2390970
    :cond_0
    check-cast p1, LX/Glp;

    .line 2390971
    sget-object v1, LX/GlO;->FILTERED_GROUPS_NULL_STATE:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_8

    .line 2390972
    iget-object v1, p1, LX/Glp;->l:LX/Glm;

    const v2, 0x7f083729

    invoke-virtual {v1, v2}, LX/Glm;->a(I)V

    .line 2390973
    :cond_1
    :goto_0
    return-void

    .line 2390974
    :cond_2
    sget-object v1, LX/GlO;->FAVORITES_SECTION_HEADER:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_3

    sget-object v1, LX/GlO;->FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_3

    sget-object v1, LX/GlO;->RECENTLY_JOINED_SECTION_HEADER:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_3

    sget-object v1, LX/GlO;->HIDDEN_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 2390975
    :cond_3
    check-cast p1, LX/Glo;

    invoke-direct {p0, p1, v0}, LX/GlX;->a(LX/Glo;I)V

    goto :goto_0

    .line 2390976
    :cond_4
    sget-object v1, LX/GlO;->SECTION_LOADING:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 2390977
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    invoke-virtual {v0, p2}, LX/Glb;->a(I)LX/Gkn;

    move-result-object v0

    .line 2390978
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/GlX;->k:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2390979
    check-cast p1, LX/Glq;

    iget-object v1, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2390980
    iget-boolean v1, v0, LX/Gkp;->e:Z

    move v0, v1

    .line 2390981
    iget-object v2, p1, LX/Glq;->l:Landroid/view/View;

    if-eqz v0, :cond_a

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2390982
    goto :goto_0

    .line 2390983
    :cond_5
    sget-object v1, LX/GlO;->POG:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2390984
    invoke-virtual {p0, p2}, LX/GlX;->e(I)LX/Gkq;

    move-result-object v0

    .line 2390985
    if-eqz v0, :cond_7

    .line 2390986
    check-cast p1, LX/Gln;

    .line 2390987
    invoke-static {p0, p2}, LX/GlX;->g(LX/GlX;I)LX/Gkn;

    move-result-object v1

    .line 2390988
    sget-object v2, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    invoke-virtual {v1, v2}, LX/Gkn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2390989
    iget-object v1, p1, LX/Gln;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    const v2, 0x7f0d0209

    invoke-virtual {v1, v2}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->setId(I)V

    .line 2390990
    :goto_2
    iget-object v1, p1, LX/Gln;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    iget-object v2, p0, LX/GlX;->i:LX/Glk;

    .line 2390991
    iget-wide v4, v2, LX/Glk;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object v2, v4

    .line 2390992
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(LX/Gkq;D)V

    goto/16 :goto_0

    .line 2390993
    :cond_6
    iget-object v1, p1, LX/Gln;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    const v2, 0x7f0d0208

    invoke-virtual {v1, v2}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->setId(I)V

    goto :goto_2

    .line 2390994
    :cond_7
    sget-object v0, LX/GlX;->a:Ljava/lang/Class;

    const-string v1, "Tried to bind pog to null object, skipping."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2390995
    :cond_8
    sget-object v1, LX/GlO;->FAVORITES_NULL_STATE:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_9

    .line 2390996
    iget-object v1, p1, LX/Glp;->l:LX/Glm;

    const v2, 0x7f083728

    invoke-virtual {v1, v2}, LX/Glm;->a(I)V

    goto/16 :goto_0

    .line 2390997
    :cond_9
    sget-object v1, LX/GlO;->GENERAL_NULL_STATE:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2390998
    iget-object v1, p1, LX/Glp;->l:LX/Glm;

    const v2, 0x7f08372a

    invoke-virtual {v1, v2}, LX/Glm;->a(I)V

    goto/16 :goto_0

    .line 2390999
    :cond_a
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public final a(Ljava/util/HashMap;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2390823
    iput-object p1, p0, LX/GlX;->k:Ljava/util/HashMap;

    .line 2390824
    iput p2, p0, LX/GlX;->j:I

    .line 2390825
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    const/4 v5, 0x0

    .line 2390826
    invoke-static {v0}, LX/Glb;->a(LX/Glb;)V

    .line 2390827
    sget-object v1, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkp;

    .line 2390828
    sget-object v2, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gkp;

    .line 2390829
    sget-object v3, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    invoke-virtual {p1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Gkp;

    .line 2390830
    sget-object v4, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Gkp;

    .line 2390831
    invoke-static {v1}, LX/Glb;->a(LX/Gkp;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {v3}, LX/Glb;->a(LX/Gkp;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {v2}, LX/Glb;->a(LX/Gkp;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {v4}, LX/Glb;->a(LX/Gkp;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2390832
    iput v5, v0, LX/Glb;->g:I

    .line 2390833
    :goto_0
    return-void

    .line 2390834
    :cond_0
    invoke-static {p1}, LX/Glb;->a(Ljava/util/HashMap;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2390835
    iput v5, v0, LX/Glb;->a:I

    .line 2390836
    invoke-static {v3}, LX/Glb;->a(LX/Gkp;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2390837
    iget v3, v0, LX/Glb;->a:I

    .line 2390838
    const/4 v5, 0x1

    move v5, v5

    .line 2390839
    add-int/2addr v3, v5

    iput v3, v0, LX/Glb;->b:I

    .line 2390840
    iget v3, v0, LX/Glb;->b:I

    .line 2390841
    const/4 v5, 0x1

    move v5, v5

    .line 2390842
    add-int/2addr v3, v5

    .line 2390843
    :goto_1
    invoke-static {v2}, LX/Glb;->a(LX/Gkp;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2390844
    iput v3, v0, LX/Glb;->e:I

    .line 2390845
    iget v3, v0, LX/Glb;->e:I

    .line 2390846
    const/4 v5, 0x1

    move v5, v5

    .line 2390847
    add-int/2addr v3, v5

    invoke-virtual {v2}, LX/Gkp;->a()I

    move-result v2

    add-int/2addr v2, v3

    .line 2390848
    iget-object v5, v0, LX/Glb;->h:Ljava/util/HashMap;

    sget-object p0, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    add-int/lit8 v3, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2390849
    :cond_1
    iput v3, v0, LX/Glb;->c:I

    .line 2390850
    iget v2, v0, LX/Glb;->c:I

    .line 2390851
    const/4 v3, 0x1

    move v3, v3

    .line 2390852
    add-int/2addr v2, v3

    .line 2390853
    invoke-static {v1}, LX/Glb;->a(LX/Gkp;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2390854
    iput v2, v0, LX/Glb;->d:I

    .line 2390855
    iget v1, v0, LX/Glb;->d:I

    .line 2390856
    const/4 v2, 0x1

    move v2, v2

    .line 2390857
    add-int/2addr v1, v2

    .line 2390858
    :goto_2
    invoke-static {v4}, LX/Glb;->a(LX/Gkp;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2390859
    iput v1, v0, LX/Glb;->f:I

    .line 2390860
    iget v1, v0, LX/Glb;->f:I

    .line 2390861
    const/4 v2, 0x1

    move v2, v2

    .line 2390862
    add-int/2addr v1, v2

    invoke-virtual {v4}, LX/Gkp;->a()I

    move-result v2

    add-int/2addr v1, v2

    .line 2390863
    iget-object v2, v0, LX/Glb;->h:Ljava/util/HashMap;

    sget-object v3, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2390864
    :cond_2
    iget-object v1, v0, LX/Glb;->i:LX/GlX;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    .line 2390865
    :cond_3
    iget v5, v0, LX/Glb;->a:I

    .line 2390866
    const/4 p0, 0x1

    move p0, p0

    .line 2390867
    add-int/2addr v5, p0

    invoke-virtual {v3}, LX/Gkp;->a()I

    move-result v3

    add-int/2addr v5, v3

    .line 2390868
    iget-object p0, v0, LX/Glb;->h:Ljava/util/HashMap;

    sget-object p2, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    add-int/lit8 v3, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p0, p2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2390869
    :cond_4
    invoke-virtual {v1}, LX/Gkp;->a()I

    move-result v1

    add-int/2addr v2, v1

    .line 2390870
    iget-object v3, v0, LX/Glb;->h:Ljava/util/HashMap;

    sget-object v5, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    add-int/lit8 v1, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    move v3, v5

    goto/16 :goto_1
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    .line 2390960
    invoke-super {p0, p1}, LX/1OM;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 2390961
    iget-object v0, p0, LX/GlX;->i:LX/Glk;

    .line 2390962
    iget v1, v0, LX/Glk;->b:I

    move v0, v1

    .line 2390963
    iget-object v1, p0, LX/GlX;->i:LX/Glk;

    .line 2390964
    iget v2, v1, LX/Glk;->c:I

    move v1, v2

    .line 2390965
    add-int/lit8 v1, v1, 0x2

    mul-int/2addr v0, v1

    .line 2390966
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getRecycledViewPool()LX/1YF;

    move-result-object v1

    sget-object v2, LX/GlO;->POG:LX/GlO;

    invoke-virtual {v2}, LX/GlO;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v0}, LX/1YF;->a(II)V

    .line 2390967
    return-void
.end method

.method public final d()LX/Gkm;
    .locals 3

    .prologue
    .line 2390957
    iget-object v0, p0, LX/GlX;->e:LX/DZB;

    sget-object v1, LX/DZA;->h:LX/0Tn;

    sget-object v2, LX/GlX;->b:LX/Gkm;

    invoke-virtual {v2}, LX/Gkm;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/DZB;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2390958
    invoke-static {v0}, LX/Gkm;->valueOf(Ljava/lang/String;)LX/Gkm;

    move-result-object v0

    .line 2390959
    return-object v0
.end method

.method public final e(I)LX/Gkq;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2390925
    invoke-direct {p0}, LX/GlX;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/GlX;->f(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2390926
    :goto_0
    return-object v0

    .line 2390927
    :cond_1
    invoke-static {p0, p1}, LX/GlX;->g(LX/GlX;I)LX/Gkn;

    move-result-object v0

    .line 2390928
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 2390929
    invoke-static {p0, p1}, LX/GlX;->g(LX/GlX;I)LX/Gkn;

    move-result-object v6

    .line 2390930
    if-eqz v6, :cond_3

    .line 2390931
    iget-object v2, p0, LX/GlX;->c:LX/Glb;

    iget v2, v2, LX/Glb;->b:I

    if-eq v2, v5, :cond_6

    .line 2390932
    const/4 v2, 0x1

    move v2, v2

    .line 2390933
    :goto_1
    iget-object v4, p0, LX/GlX;->c:LX/Glb;

    iget v4, v4, LX/Glb;->d:I

    if-eq v4, v5, :cond_5

    .line 2390934
    const/4 v4, 0x1

    move v4, v4

    .line 2390935
    :goto_2
    iget-object v7, p0, LX/GlX;->c:LX/Glb;

    iget v7, v7, LX/Glb;->c:I

    if-eq v7, v5, :cond_2

    .line 2390936
    iget-object v3, p0, LX/GlX;->c:LX/Glb;

    iget v3, v3, LX/Glb;->c:I

    .line 2390937
    const/4 v7, 0x1

    move v7, v7

    .line 2390938
    add-int/2addr v3, v7

    .line 2390939
    :cond_2
    sget-object v7, LX/GlV;->a:[I

    invoke-virtual {v6}, LX/Gkn;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_0

    :cond_3
    move v2, v5

    .line 2390940
    :goto_3
    move v2, v2

    .line 2390941
    iget-object v3, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2390942
    if-eqz v0, :cond_4

    :try_start_0
    invoke-virtual {v0}, LX/Gkp;->f()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 2390943
    invoke-virtual {v0, v2}, LX/Gkp;->a(I)LX/Gkq;

    move-result-object v0

    goto :goto_0

    .line 2390944
    :cond_4
    sget-object v0, LX/GlX;->a:Ljava/lang/Class;

    const-string v2, "getGridItemData failed"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    move-object v0, v1

    .line 2390945
    goto :goto_0

    .line 2390946
    :catch_0
    move-exception v0

    .line 2390947
    sget-object v2, LX/GlX;->a:Ljava/lang/Class;

    const-string v3, "CursorIndexOutOfBoundsException "

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 2390948
    :pswitch_0
    const/4 v3, 0x1

    move v3, v3

    .line 2390949
    sub-int v3, p1, v3

    sub-int v2, v3, v2

    goto :goto_3

    .line 2390950
    :pswitch_1
    iget-object v2, p0, LX/GlX;->c:LX/Glb;

    iget v2, v2, LX/Glb;->e:I

    sub-int v2, p1, v2

    .line 2390951
    const/4 v3, 0x1

    move v3, v3

    .line 2390952
    sub-int/2addr v2, v3

    goto :goto_3

    .line 2390953
    :pswitch_2
    sub-int v2, p1, v3

    sub-int/2addr v2, v4

    goto :goto_3

    .line 2390954
    :pswitch_3
    iget-object v2, p0, LX/GlX;->c:LX/Glb;

    iget v2, v2, LX/Glb;->f:I

    sub-int v2, p1, v2

    .line 2390955
    const/4 v3, 0x1

    move v3, v3

    .line 2390956
    sub-int/2addr v2, v3

    goto :goto_3

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v2, v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 2390921
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GlX;->l:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GlX;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2390922
    iget-object v0, p0, LX/GlX;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5OM;

    invoke-virtual {v0}, LX/0ht;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2390923
    :cond_0
    monitor-exit p0

    return-void

    .line 2390924
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f(I)Z
    .locals 2

    .prologue
    .line 2390920
    invoke-virtual {p0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    sget-object v1, LX/GlO;->POG:LX/GlO;

    invoke-virtual {v1}, LX/GlO;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2390896
    invoke-direct {p0}, LX/GlX;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2390897
    sget-object v0, LX/GlO;->LOADING:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    .line 2390898
    :goto_0
    return v0

    .line 2390899
    :cond_0
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->a:I

    if-ne p1, v0, :cond_1

    .line 2390900
    sget-object v0, LX/GlO;->FAVORITES_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390901
    :cond_1
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->c:I

    if-ne p1, v0, :cond_2

    .line 2390902
    sget-object v0, LX/GlO;->FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390903
    :cond_2
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->e:I

    if-ne p1, v0, :cond_3

    .line 2390904
    sget-object v0, LX/GlO;->RECENTLY_JOINED_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390905
    :cond_3
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->f:I

    if-ne p1, v0, :cond_4

    .line 2390906
    sget-object v0, LX/GlO;->HIDDEN_GROUPS_SECTION_HEADER:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390907
    :cond_4
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->g:I

    if-ne p1, v0, :cond_5

    .line 2390908
    sget-object v0, LX/GlO;->GENERAL_NULL_STATE:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390909
    :cond_5
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->b:I

    if-ne p1, v0, :cond_6

    .line 2390910
    sget-object v0, LX/GlO;->FAVORITES_NULL_STATE:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390911
    :cond_6
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    iget v0, v0, LX/Glb;->d:I

    if-ne p1, v0, :cond_7

    .line 2390912
    sget-object v0, LX/GlO;->FILTERED_GROUPS_NULL_STATE:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390913
    :cond_7
    iget-object v0, p0, LX/GlX;->c:LX/Glb;

    .line 2390914
    invoke-virtual {v0, p1}, LX/Glb;->a(I)LX/Gkn;

    move-result-object p0

    if-eqz p0, :cond_a

    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 2390915
    if-eqz v0, :cond_8

    .line 2390916
    sget-object v0, LX/GlO;->SECTION_LOADING:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390917
    :cond_8
    if-ltz p1, :cond_9

    .line 2390918
    sget-object v0, LX/GlO;->POG:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2390919
    :cond_9
    sget-object v0, LX/GlO;->INVALID:LX/GlO;

    invoke-virtual {v0}, LX/GlO;->ordinal()I

    move-result v0

    goto :goto_0

    :cond_a
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final ij_()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2390871
    invoke-direct {p0}, LX/GlX;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2390872
    :goto_0
    return v2

    .line 2390873
    :cond_0
    iget-object v0, p0, LX/GlX;->k:Ljava/util/HashMap;

    sget-object v1, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2390874
    iget-object v1, p0, LX/GlX;->k:Ljava/util/HashMap;

    sget-object v3, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkp;

    .line 2390875
    iget-object v3, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gkp;

    .line 2390876
    invoke-static {v2}, LX/Gla;->a(LX/Gkp;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2390877
    add-int/lit8 v3, v3, 0x1

    .line 2390878
    :cond_1
    :goto_2
    iget-boolean v5, v2, LX/Gkp;->d:Z

    move v5, v5

    .line 2390879
    if-eqz v5, :cond_2

    .line 2390880
    invoke-virtual {v2}, LX/Gkp;->a()I

    move-result v5

    sub-int/2addr v3, v5

    .line 2390881
    :cond_2
    iget-object v5, p0, LX/GlX;->c:LX/Glb;

    invoke-virtual {v2}, LX/Gkp;->c()Ljava/lang/Enum;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/Glb;->a(Ljava/lang/Enum;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_6

    .line 2390882
    iget-boolean v5, v2, LX/Gkp;->d:Z

    move v2, v5

    .line 2390883
    if-nez v2, :cond_6

    .line 2390884
    add-int/lit8 v3, v3, 0x1

    move v2, v3

    :goto_3
    move v3, v2

    .line 2390885
    goto :goto_1

    .line 2390886
    :cond_3
    if-eqz v0, :cond_4

    if-ne v0, v2, :cond_4

    invoke-virtual {v0}, LX/Gkp;->f()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, LX/GlX;->k:Ljava/util/HashMap;

    invoke-static {v5}, LX/Glb;->a(Ljava/util/HashMap;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2390887
    const/4 v5, 0x1

    move v5, v5

    .line 2390888
    const/4 v6, 0x1

    move v6, v6

    .line 2390889
    add-int/2addr v5, v6

    add-int/2addr v3, v5

    .line 2390890
    :cond_4
    if-ne v1, v2, :cond_1

    invoke-virtual {v1}, LX/Gkp;->f()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2390891
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/Gkp;->d()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2390892
    const/4 v5, 0x1

    move v5, v5

    .line 2390893
    const/4 v6, 0x1

    move v6, v6

    .line 2390894
    add-int/2addr v5, v6

    add-int/2addr v3, v5

    goto :goto_2

    .line 2390895
    :cond_5
    iget v0, p0, LX/GlX;->j:I

    add-int v2, v0, v3

    goto/16 :goto_0

    :cond_6
    move v2, v3

    goto :goto_3
.end method
