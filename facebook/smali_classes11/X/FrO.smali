.class public final LX/FrO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;)V
    .locals 0

    .prologue
    .line 2295427
    iput-object p1, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2295428
    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2295429
    :cond_0
    :goto_0
    return-void

    .line 2295430
    :cond_1
    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080039

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2295431
    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2295432
    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2295433
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2295434
    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->q:LX/BQ1;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2295435
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2295436
    if-nez v0, :cond_1

    .line 2295437
    :cond_0
    :goto_0
    return-void

    .line 2295438
    :cond_1
    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->q:LX/BQ1;

    .line 2295439
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2295440
    invoke-virtual {v0, v1}, LX/BQ1;->a(Ljava/lang/Object;)V

    .line 2295441
    iget-object v0, p0, LX/FrO;->a:Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-static {v0}, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;->b$redex0(Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;)V

    goto :goto_0
.end method
