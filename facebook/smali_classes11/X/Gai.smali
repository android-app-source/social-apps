.class public final LX/Gai;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)V
    .locals 0

    .prologue
    .line 2368577
    iput-object p1, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2368578
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    .line 2368579
    iput-boolean v4, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->r:Z

    .line 2368580
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    invoke-virtual {v0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GraphQL call cancelled. CollectionId = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v3, v3, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->i:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2368581
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2368582
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    .line 2368583
    iput-boolean v4, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->r:Z

    .line 2368584
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    invoke-virtual {v0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GraphQL call failed. CollectionId = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v3, v3, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->i:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2368585
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2368586
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 2368587
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    .line 2368588
    iput-boolean v4, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->r:Z

    .line 2368589
    if-eqz p1, :cond_0

    .line 2368590
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2368591
    if-eqz v0, :cond_0

    .line 2368592
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2368593
    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2368594
    :cond_0
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    invoke-virtual {v0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GraphQL query successfully finished but result came back null. CollectionId = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v3, v3, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->i:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2368595
    :goto_0
    return-void

    .line 2368596
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2368597
    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2368598
    iget-object v1, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    .line 2368599
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2368600
    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2368601
    iput-object v0, v1, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->t:Ljava/lang/String;

    .line 2368602
    iget-object v1, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    .line 2368603
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2368604
    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;->j()Z

    move-result v0

    .line 2368605
    iput-boolean v0, v1, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->s:Z

    .line 2368606
    :cond_2
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v1, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->j:LX/Gam;

    .line 2368607
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2368608
    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->a()LX/0Px;

    move-result-object v0

    .line 2368609
    iget-object v2, v1, LX/Gam;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2368610
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    invoke-virtual {v0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->a()Ljava/lang/String;

    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v0, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->j:LX/Gam;

    invoke-virtual {v0}, LX/Gam;->size()I

    .line 2368611
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v0, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->q:LX/162;

    if-nez v0, :cond_3

    .line 2368612
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v1, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v1, v1, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->j:LX/Gam;

    invoke-virtual {v1, v4}, LX/Gam;->b(I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 2368613
    iput-object v1, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->q:LX/162;

    .line 2368614
    :cond_3
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v0, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2368615
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    iget-object v0, v0, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2368616
    iget-object v0, p0, LX/Gai;->a:Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;

    invoke-static {v0}, Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;->c(Lcom/facebook/curatedcollections/CuratedCollectionLandingPageFragment;)LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method
