.class public final LX/GYI;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$FetchCommerceStoreQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;I)V
    .locals 0

    .prologue
    .line 2364968
    iput-object p1, p0, LX/GYI;->b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    iput p2, p0, LX/GYI;->a:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2364969
    sget-object v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->j:Ljava/lang/String;

    const-string v1, "fail to fetch commerce store query"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2364970
    iget-object v0, p0, LX/GYI;->b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;Z)V

    .line 2364971
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2364972
    check-cast p1, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$FetchCommerceStoreQueryModel;

    const/4 v1, 0x0

    .line 2364973
    iget-object v0, p0, LX/GYI;->b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-static {v0, v1}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;Z)V

    .line 2364974
    iget-object v0, p0, LX/GYI;->b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-static {v0, v1}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->b$redex0(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;Z)V

    .line 2364975
    iget-object v0, p0, LX/GYI;->b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$FetchCommerceStoreQueryModel;->a()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    move-result-object v1

    .line 2364976
    iput-object v1, v0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    .line 2364977
    iget-object v2, v0, LX/GXY;->d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    if-eqz v2, :cond_0

    .line 2364978
    iget-object v2, v0, LX/GXY;->d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2364979
    :cond_0
    iget-object v0, p0, LX/GYI;->b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    .line 2364980
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2364981
    if-eqz v0, :cond_1

    iget v0, p0, LX/GYI;->a:I

    if-eqz v0, :cond_1

    .line 2364982
    iget-object v0, p0, LX/GYI;->b:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget v2, p0, LX/GYI;->a:I

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2364983
    :cond_1
    return-void
.end method
