.class public final LX/Fpy;
.super LX/1L3;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;)V
    .locals 0

    .prologue
    .line 2291994
    iput-object p1, p0, LX/Fpy;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-direct {p0}, LX/1L3;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2291995
    check-cast p1, LX/1ZY;

    .line 2291996
    iget-object v0, p0, LX/Fpy;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v0

    iget-object v1, p1, LX/1ZY;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1ZY;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;)LX/16n;

    move-result-object v2

    .line 2291997
    if-eqz v2, :cond_0

    instance-of v0, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fpy;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/99v;

    move-object v1, v2

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    iget-wide v4, p1, LX/1ZY;->c:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/99v;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2291998
    :cond_0
    :goto_0
    return-void

    .line 2291999
    :cond_1
    iget-object v0, p0, LX/Fpy;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v0

    iget-object v1, p1, LX/1ZY;->a:Ljava/lang/String;

    iget-object v3, p1, LX/1ZY;->b:Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0, v1, v3, v4, v5}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;)V

    .line 2292000
    iget-object v0, p0, LX/Fpy;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/99v;

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    iget-boolean v1, p1, LX/1ZY;->d:Z

    if-eqz v1, :cond_2

    sget-object v1, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    :goto_1
    invoke-virtual {v0, v2, v1}, LX/99v;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/55G;)V

    .line 2292001
    iget-object v0, p0, LX/Fpy;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    goto :goto_0

    .line 2292002
    :cond_2
    sget-object v1, LX/55G;->LOCAL_ONLY:LX/55G;

    goto :goto_1
.end method
