.class public final LX/Fmm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2283881
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 2283882
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2283883
    :goto_0
    return v1

    .line 2283884
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_3

    .line 2283885
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2283886
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2283887
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 2283888
    const-string v7, "is_verified"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2283889
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 2283890
    :cond_1
    const-string v7, "is_verified_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2283891
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2283892
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2283893
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2283894
    if-eqz v3, :cond_4

    .line 2283895
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 2283896
    :cond_4
    if-eqz v0, :cond_5

    .line 2283897
    invoke-virtual {p1, v2, v4}, LX/186;->a(IZ)V

    .line 2283898
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 2283899
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2283900
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2283901
    if-eqz v0, :cond_0

    .line 2283902
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2283903
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2283904
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2283905
    if-eqz v0, :cond_1

    .line 2283906
    const-string v1, "is_verified_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2283907
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2283908
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2283909
    return-void
.end method
