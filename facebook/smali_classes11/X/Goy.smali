.class public LX/Goy;
.super LX/Goq;
.source ""

# interfaces
.implements LX/GoX;
.implements LX/GoY;
.implements LX/Gob;


# instance fields
.field private final a:LX/CHk;


# direct methods
.method public constructor <init>(LX/CHk;)V
    .locals 3

    .prologue
    .line 2394671
    invoke-interface {p1}, LX/CHj;->c()LX/CHa;

    move-result-object v0

    const/16 v1, 0x6a

    invoke-interface {p1}, LX/CHj;->e()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/Goq;-><init>(LX/CHa;II)V

    .line 2394672
    iput-object p1, p0, LX/Goy;->a:LX/CHk;

    .line 2394673
    return-void
.end method


# virtual methods
.method public final D()LX/GoE;
    .locals 3

    .prologue
    .line 2394675
    new-instance v0, LX/GoE;

    iget-object v1, p0, LX/Goy;->a:LX/CHk;

    invoke-interface {v1}, LX/CHj;->jt_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Goy;->a:LX/CHk;

    invoke-interface {v2}, LX/CHj;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLInterfaces$InstantShoppingCompositeBlockElementFragment$$BlockElements$;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2394674
    iget-object v0, p0, LX/Goy;->a:LX/CHk;

    invoke-interface {v0}, LX/CHk;->jq_()LX/0Px;

    move-result-object v0

    return-object v0
.end method
