.class public LX/Gn2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Gn2;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0ad;

.field public c:Z

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2393320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393321
    iput-boolean v0, p0, LX/Gn2;->c:Z

    .line 2393322
    iput-boolean v0, p0, LX/Gn2;->d:Z

    .line 2393323
    iput-boolean v0, p0, LX/Gn2;->e:Z

    .line 2393324
    iput-object p1, p0, LX/Gn2;->b:LX/0ad;

    .line 2393325
    iput-object p2, p0, LX/Gn2;->a:LX/0Or;

    .line 2393326
    return-void
.end method

.method public static a(LX/0QB;)LX/Gn2;
    .locals 5

    .prologue
    .line 2393327
    sget-object v0, LX/Gn2;->f:LX/Gn2;

    if-nez v0, :cond_1

    .line 2393328
    const-class v1, LX/Gn2;

    monitor-enter v1

    .line 2393329
    :try_start_0
    sget-object v0, LX/Gn2;->f:LX/Gn2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2393330
    if-eqz v2, :cond_0

    .line 2393331
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2393332
    new-instance v4, LX/Gn2;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 p0, 0x122d

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/Gn2;-><init>(LX/0ad;LX/0Or;)V

    .line 2393333
    move-object v0, v4

    .line 2393334
    sput-object v0, LX/Gn2;->f:LX/Gn2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393335
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2393336
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2393337
    :cond_1
    sget-object v0, LX/Gn2;->f:LX/Gn2;

    return-object v0

    .line 2393338
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2393339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
