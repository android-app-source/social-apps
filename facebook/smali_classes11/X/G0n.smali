.class public LX/G0n;
.super LX/99d;
.source ""


# instance fields
.field private final a:LX/BQ1;

.field private final b:LX/G1I;

.field public final c:LX/FvG;

.field public final d:LX/1Qq;

.field public final e:LX/1Qq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/1Qq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/1Qq;

.field private final h:Z

.field private final i:Z


# direct methods
.method public constructor <init>(LX/BQ1;LX/G1I;LX/FvG;LX/1Qq;LX/1Qq;LX/1Qq;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2310306
    const/4 v0, 0x4

    new-array v0, v0, [LX/1Qq;

    new-instance v1, LX/99f;

    invoke-direct {v1, p3}, LX/99f;-><init>(LX/1Cw;)V

    aput-object v1, v0, v2

    aput-object p4, v0, v3

    const/4 v1, 0x2

    aput-object p5, v0, v1

    const/4 v1, 0x3

    aput-object p6, v0, v1

    invoke-direct {p0, v3, v0}, LX/99d;-><init>(Z[LX/1Qq;)V

    .line 2310307
    iput-object p1, p0, LX/G0n;->a:LX/BQ1;

    .line 2310308
    iput-object p2, p0, LX/G0n;->b:LX/G1I;

    .line 2310309
    iput-object p3, p0, LX/G0n;->c:LX/FvG;

    .line 2310310
    iput-object p4, p0, LX/G0n;->d:LX/1Qq;

    .line 2310311
    iput-object p5, p0, LX/G0n;->f:LX/1Qq;

    .line 2310312
    iput-boolean p7, p0, LX/G0n;->i:Z

    .line 2310313
    const/4 v0, 0x0

    iput-object v0, p0, LX/G0n;->e:LX/1Qq;

    .line 2310314
    iput-object p6, p0, LX/G0n;->g:LX/1Qq;

    .line 2310315
    iput-boolean v2, p0, LX/G0n;->h:Z

    .line 2310316
    return-void
.end method

.method public constructor <init>(LX/BQ1;LX/G1I;LX/FvG;LX/1Qq;LX/1Qq;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2310295
    const/4 v0, 0x3

    new-array v0, v0, [LX/1Qq;

    new-instance v1, LX/99f;

    invoke-direct {v1, p3}, LX/99f;-><init>(LX/1Cw;)V

    aput-object v1, v0, v2

    aput-object p4, v0, v3

    const/4 v1, 0x2

    aput-object p5, v0, v1

    invoke-direct {p0, v3, v0}, LX/99d;-><init>(Z[LX/1Qq;)V

    .line 2310296
    iput-object p1, p0, LX/G0n;->a:LX/BQ1;

    .line 2310297
    iput-object p2, p0, LX/G0n;->b:LX/G1I;

    .line 2310298
    iput-object p3, p0, LX/G0n;->c:LX/FvG;

    .line 2310299
    iput-object p4, p0, LX/G0n;->d:LX/1Qq;

    .line 2310300
    iput-boolean p6, p0, LX/G0n;->i:Z

    .line 2310301
    iput-object v4, p0, LX/G0n;->f:LX/1Qq;

    .line 2310302
    iput-object v4, p0, LX/G0n;->e:LX/1Qq;

    .line 2310303
    iput-object p5, p0, LX/G0n;->g:LX/1Qq;

    .line 2310304
    iput-boolean v2, p0, LX/G0n;->h:Z

    .line 2310305
    return-void
.end method

.method public constructor <init>(LX/FvG;LX/1Qq;LX/1Qq;LX/1Qq;LX/1Qq;LX/BQ1;LX/G1I;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2310284
    const/4 v0, 0x5

    new-array v0, v0, [LX/1Qq;

    const/4 v1, 0x0

    new-instance v2, LX/99f;

    invoke-direct {v2, p1}, LX/99f;-><init>(LX/1Cw;)V

    aput-object v2, v0, v1

    aput-object p2, v0, v3

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    const/4 v1, 0x4

    aput-object p5, v0, v1

    invoke-direct {p0, v3, v0}, LX/99d;-><init>(Z[LX/1Qq;)V

    .line 2310285
    iput-object p1, p0, LX/G0n;->c:LX/FvG;

    .line 2310286
    iput-object p3, p0, LX/G0n;->d:LX/1Qq;

    .line 2310287
    iput-object p2, p0, LX/G0n;->e:LX/1Qq;

    .line 2310288
    iput-object p4, p0, LX/G0n;->f:LX/1Qq;

    .line 2310289
    iput-object p5, p0, LX/G0n;->g:LX/1Qq;

    .line 2310290
    iput-object p6, p0, LX/G0n;->a:LX/BQ1;

    .line 2310291
    iput-object p7, p0, LX/G0n;->b:LX/G1I;

    .line 2310292
    iput-boolean p8, p0, LX/G0n;->i:Z

    .line 2310293
    iput-boolean v3, p0, LX/G0n;->h:Z

    .line 2310294
    return-void
.end method

.method public constructor <init>(LX/FvG;LX/1Qq;LX/1Qq;LX/1Qq;LX/BQ1;LX/G1I;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2310273
    const/4 v0, 0x4

    new-array v0, v0, [LX/1Qq;

    const/4 v1, 0x0

    new-instance v2, LX/99f;

    invoke-direct {v2, p1}, LX/99f;-><init>(LX/1Cw;)V

    aput-object v2, v0, v1

    aput-object p2, v0, v3

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    invoke-direct {p0, v3, v0}, LX/99d;-><init>(Z[LX/1Qq;)V

    .line 2310274
    iput-object p1, p0, LX/G0n;->c:LX/FvG;

    .line 2310275
    iput-object p3, p0, LX/G0n;->d:LX/1Qq;

    .line 2310276
    iput-object p2, p0, LX/G0n;->e:LX/1Qq;

    .line 2310277
    iput-boolean p7, p0, LX/G0n;->i:Z

    .line 2310278
    const/4 v0, 0x0

    iput-object v0, p0, LX/G0n;->f:LX/1Qq;

    .line 2310279
    iput-object p4, p0, LX/G0n;->g:LX/1Qq;

    .line 2310280
    iput-object p5, p0, LX/G0n;->a:LX/BQ1;

    .line 2310281
    iput-object p6, p0, LX/G0n;->b:LX/G1I;

    .line 2310282
    iput-boolean v3, p0, LX/G0n;->h:Z

    .line 2310283
    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 2310272
    iget-object v0, p0, LX/G0n;->g:LX/1Qq;

    invoke-virtual {p0, v0}, LX/62C;->a(LX/1Cw;)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2310271
    iget-object v0, p0, LX/G0n;->g:LX/1Qq;

    invoke-virtual {p0, v0}, LX/99d;->a(LX/1Qq;)I

    move-result v0

    return v0
.end method

.method public final getCount()I
    .locals 3

    .prologue
    .line 2310255
    iget-object v0, p0, LX/G0n;->a:LX/BQ1;

    .line 2310256
    iget-object v1, v0, LX/BPy;->e:LX/BPx;

    move-object v1, v1

    .line 2310257
    sget-object v2, LX/BPx;->UNINITIALIZED:LX/BPx;

    if-eq v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2310258
    if-nez v0, :cond_1

    .line 2310259
    const/4 v0, 0x0

    .line 2310260
    :cond_0
    :goto_1
    return v0

    .line 2310261
    :cond_1
    iget-object v0, p0, LX/G0n;->b:LX/G1I;

    invoke-virtual {v0}, LX/G1I;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/G0n;->i:Z

    if-eqz v0, :cond_2

    .line 2310262
    iget-object v0, p0, LX/G0n;->c:LX/FvG;

    invoke-virtual {v0}, LX/62C;->getCount()I

    move-result v0

    iget-object v1, p0, LX/G0n;->d:LX/1Qq;

    invoke-interface {v1}, LX/1Qq;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 2310263
    iget-boolean v1, p0, LX/G0n;->h:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/G0n;->e:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2310264
    iget-object v1, p0, LX/G0n;->e:LX/1Qq;

    invoke-interface {v1}, LX/1Qq;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    .line 2310265
    :cond_2
    invoke-super {p0}, LX/99d;->getCount()I

    move-result v0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2310270
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x0

    .line 2310267
    const-string v0, "BaseTimelineAdapter.getView-%d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x2e510697

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 2310268
    :try_start_0
    invoke-super {p0, p1, p2, p3}, LX/99d;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2310269
    const v1, -0x21f4f43b

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x1cb65e8f

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    throw v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 2310266
    invoke-virtual {p0}, LX/62C;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, LX/99d;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
