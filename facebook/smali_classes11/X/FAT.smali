.class public final LX/FAT;
.super LX/GnT;
.source ""


# instance fields
.field public final synthetic a:LX/FAa;


# direct methods
.method public constructor <init>(LX/FAa;)V
    .locals 0

    .prologue
    .line 2206976
    iput-object p1, p0, LX/FAT;->a:LX/FAa;

    invoke-direct {p0}, LX/GnT;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2206977
    check-cast p1, LX/Gna;

    const/4 v4, 0x0

    .line 2206978
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    invoke-static {v0}, LX/FAa;->aa(LX/FAa;)Ljava/lang/String;

    move-result-object v1

    .line 2206979
    iget-boolean v0, p1, LX/Gna;->a:Z

    move v0, v0

    .line 2206980
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->O:LX/CIe;

    invoke-virtual {v0, v1}, LX/CIe;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2206981
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->ab:LX/0ad;

    sget-short v2, LX/CHN;->s:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2206982
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    invoke-virtual {v0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, LX/FAT;->a:LX/FAa;

    invoke-virtual {v2}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0828c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2206983
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2206984
    :cond_0
    :goto_0
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->O:LX/CIe;

    .line 2206985
    iget-boolean v2, p1, LX/Gna;->a:Z

    move v2, v2

    .line 2206986
    iget-object v3, v0, LX/CIe;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206987
    return-void

    .line 2206988
    :cond_1
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->X:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206989
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->az:LX/0ht;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->az:LX/0ht;

    .line 2206990
    iget-boolean v2, v0, LX/0ht;->r:Z

    move v0, v2

    .line 2206991
    if-eqz v0, :cond_2

    .line 2206992
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->az:LX/0ht;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2206993
    :cond_2
    iget-object v0, p0, LX/FAT;->a:LX/FAa;

    .line 2206994
    invoke-virtual {v0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030975

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    .line 2206995
    iget-object v2, p0, LX/FAT;->a:LX/FAa;

    new-instance v3, LX/0ht;

    iget-object v4, p0, LX/FAT;->a:LX/FAa;

    invoke-virtual {v4}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2206996
    iput-object v3, v2, LX/FAa;->az:LX/0ht;

    .line 2206997
    iget-object v2, p0, LX/FAT;->a:LX/FAa;

    iget-object v2, v2, LX/FAa;->az:LX/0ht;

    invoke-virtual {v2, v0}, LX/0ht;->d(Landroid/view/View;)V

    .line 2206998
    iget-object v2, p0, LX/FAT;->a:LX/FAa;

    iget-object v2, v2, LX/FAa;->az:LX/0ht;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0ht;->d(Z)V

    .line 2206999
    iget-object v2, p0, LX/FAT;->a:LX/FAa;

    iget-object v2, v2, LX/FAa;->az:LX/0ht;

    iget-object v3, p0, LX/FAT;->a:LX/FAa;

    .line 2207000
    iget-object v4, v3, LX/Chc;->D:Landroid/view/View;

    move-object v4, v4

    .line 2207001
    move-object v3, v4

    .line 2207002
    const v4, 0x7f0d16ab

    invoke-static {v3, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ht;->f(Landroid/view/View;)V

    .line 2207003
    iget-object v2, p0, LX/FAT;->a:LX/FAa;

    iget-object v2, v2, LX/FAa;->az:LX/0ht;

    sget-object v3, LX/3AV;->CENTER:LX/3AV;

    invoke-virtual {v2, v3}, LX/0ht;->a(LX/3AV;)V

    .line 2207004
    iget-object v2, p0, LX/FAT;->a:LX/FAa;

    iget-object v2, v2, LX/FAa;->az:LX/0ht;

    new-instance v3, LX/FAR;

    invoke-direct {v3, p0}, LX/FAR;-><init>(LX/FAT;)V

    .line 2207005
    iput-object v3, v2, LX/0ht;->H:LX/2dD;

    .line 2207006
    const v2, 0x7f0d183f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2207007
    new-instance v2, LX/FAS;

    invoke-direct {v2, p0}, LX/FAS;-><init>(LX/FAT;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method
