.class public final LX/GON;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/EditPaymentCardActivity;)V
    .locals 0

    .prologue
    .line 2347516
    iput-object p1, p0, LX/GON;->a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2347517
    iget-object v0, p0, LX/GON;->a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->w()V

    .line 2347518
    iget-object v0, p0, LX/GON;->a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    const-string v1, "payments_add_card_success"

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->g(Ljava/lang/String;)V

    .line 2347519
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2347520
    iget-object v1, p0, LX/GON;->a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->setResult(ILandroid/content/Intent;)V

    .line 2347521
    iget-object v0, p0, LX/GON;->a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->finish()V

    .line 2347522
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2347514
    iget-object v0, p0, LX/GON;->a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    iget-object v1, p0, LX/GON;->a:Lcom/facebook/adspayments/activity/EditPaymentCardActivity;

    invoke-virtual {v1}, Lcom/facebook/adspayments/activity/EditPaymentCardActivity;->o()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    .line 2347515
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347513
    invoke-direct {p0}, LX/GON;->a()V

    return-void
.end method
