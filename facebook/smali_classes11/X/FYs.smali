.class public final LX/FYs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:J

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/FYx;


# direct methods
.method public constructor <init>(LX/FYx;IJLandroid/content/Context;)V
    .locals 1

    .prologue
    .line 2256607
    iput-object p1, p0, LX/FYs;->d:LX/FYx;

    iput p2, p0, LX/FYs;->a:I

    iput-wide p3, p0, LX/FYs;->b:J

    iput-object p5, p0, LX/FYs;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2256606
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2256589
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2256590
    if-eqz p1, :cond_0

    .line 2256591
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2256592
    if-nez v0, :cond_1

    .line 2256593
    :cond_0
    :goto_0
    return-void

    .line 2256594
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2256595
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2256596
    iget-object v1, p0, LX/FYs;->d:LX/FYx;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v1, v0}, LX/FYx;->a$redex0(LX/FYx;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/395;

    move-result-object v0

    .line 2256597
    iget v1, p0, LX/FYs;->a:I

    invoke-virtual {v0, v1}, LX/395;->a(I)LX/395;

    .line 2256598
    iget-object v1, p0, LX/FYs;->d:LX/FYx;

    iget-wide v2, p0, LX/FYs;->b:J

    iget-object v4, p0, LX/FYs;->c:Landroid/content/Context;

    .line 2256599
    const-class v5, LX/0f8;

    invoke-static {v4, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0f8;

    .line 2256600
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256601
    invoke-interface {v5}, LX/0f8;->i()LX/0hE;

    move-result-object v5

    .line 2256602
    new-instance p0, LX/FYw;

    invoke-direct {p0, v1, v2, v3}, LX/FYw;-><init>(LX/FYx;J)V

    invoke-interface {v5, p0}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 2256603
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256604
    invoke-interface {v5, v0}, LX/0hE;->a(LX/395;)V

    .line 2256605
    goto :goto_0
.end method
