.class public LX/Gd8;
.super LX/Gd2;
.source ""


# instance fields
.field private a:LX/0mh;

.field public b:Z


# direct methods
.method public constructor <init>(LX/0mh;Z)V
    .locals 0

    .prologue
    .line 2373212
    invoke-direct {p0}, LX/Gd2;-><init>()V

    .line 2373213
    iput-object p1, p0, LX/Gd8;->a:LX/0mh;

    .line 2373214
    iput-boolean p2, p0, LX/Gd8;->b:Z

    .line 2373215
    return-void
.end method

.method private a(ILjava/lang/String;)LX/0mj;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2373216
    iget-object v0, p0, LX/Gd8;->a:LX/0mh;

    .line 2373217
    const-string v1, "react_over_the_air_updates"

    const/4 v2, 0x0

    sget-object v3, LX/0mq;->CLIENT_EVENT:LX/0mq;

    iget-boolean v4, p0, LX/Gd8;->b:Z

    invoke-static {v1, p2, v2, v3, v4}, LX/1tk;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/1tk;

    move-result-object v1

    move-object v1, v1

    .line 2373218
    invoke-virtual {v0, v1}, LX/0mh;->a(LX/1tk;)LX/0mj;

    move-result-object v0

    .line 2373219
    invoke-virtual {v0}, LX/0mj;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2373220
    const/4 v0, 0x0

    .line 2373221
    :goto_0
    return-object v0

    .line 2373222
    :cond_0
    const-string v1, "update_bundle_version"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    goto :goto_0
.end method

.method private a(LX/2fU;Ljava/lang/String;)LX/0mj;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2373223
    invoke-virtual {p1}, LX/2fU;->c()I

    move-result v0

    invoke-direct {p0, v0, p2}, LX/Gd8;->a(ILjava/lang/String;)LX/0mj;

    move-result-object v0

    .line 2373224
    if-nez v0, :cond_0

    .line 2373225
    const/4 v0, 0x0

    .line 2373226
    :goto_0
    return-object v0

    .line 2373227
    :cond_0
    const-string v1, "download_size"

    invoke-virtual {p1}, LX/2fU;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    goto :goto_0
.end method


# virtual methods
.method public final a(IJ)V
    .locals 4

    .prologue
    .line 2373228
    const-string v0, "react_ota_bundle_activated"

    invoke-direct {p0, p1, v0}, LX/Gd8;->a(ILjava/lang/String;)LX/0mj;

    move-result-object v0

    .line 2373229
    if-nez v0, :cond_0

    .line 2373230
    :goto_0
    return-void

    .line 2373231
    :cond_0
    const-string v1, "duration"

    const-wide/16 v2, 0x3e8

    div-long v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 2373232
    invoke-virtual {v0}, LX/0mj;->e()V

    goto :goto_0
.end method

.method public final a(LX/2fU;)V
    .locals 1

    .prologue
    .line 2373233
    const-string v0, "react_ota_download_started"

    invoke-direct {p0, p1, v0}, LX/Gd8;->a(LX/2fU;Ljava/lang/String;)LX/0mj;

    move-result-object v0

    .line 2373234
    if-nez v0, :cond_0

    .line 2373235
    :goto_0
    return-void

    .line 2373236
    :cond_0
    invoke-virtual {v0}, LX/0mj;->e()V

    goto :goto_0
.end method

.method public final a(LX/2fU;J)V
    .locals 4

    .prologue
    .line 2373237
    const-string v0, "react_ota_download_succeeded"

    invoke-direct {p0, p1, v0}, LX/Gd8;->a(LX/2fU;Ljava/lang/String;)LX/0mj;

    move-result-object v0

    .line 2373238
    if-nez v0, :cond_0

    .line 2373239
    :goto_0
    return-void

    .line 2373240
    :cond_0
    const-string v1, "duration"

    const-wide/16 v2, 0x3e8

    div-long v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 2373241
    invoke-virtual {v0}, LX/0mj;->e()V

    goto :goto_0
.end method

.method public final a(LX/2fU;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2373242
    const-string v0, "react_ota_processing_failed"

    invoke-direct {p0, p1, v0}, LX/Gd8;->a(LX/2fU;Ljava/lang/String;)LX/0mj;

    move-result-object v0

    .line 2373243
    if-nez v0, :cond_0

    .line 2373244
    :goto_0
    return-void

    .line 2373245
    :cond_0
    const-string p0, "error_code"

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 2373246
    const-string p0, "error_message"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 2373247
    invoke-virtual {v0}, LX/0mj;->e()V

    goto :goto_0
.end method

.method public final b(LX/2fU;)V
    .locals 1

    .prologue
    .line 2373248
    const-string v0, "react_ota_verification_succeeded"

    invoke-direct {p0, p1, v0}, LX/Gd8;->a(LX/2fU;Ljava/lang/String;)LX/0mj;

    move-result-object v0

    .line 2373249
    if-nez v0, :cond_0

    .line 2373250
    :goto_0
    return-void

    .line 2373251
    :cond_0
    invoke-virtual {v0}, LX/0mj;->e()V

    goto :goto_0
.end method
