.class public LX/Glj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Landroid/content/Context;

.field public c:LX/0Zb;

.field public d:LX/0kv;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Zb;LX/0kv;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2391204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2391205
    iput-object p1, p0, LX/Glj;->b:Landroid/content/Context;

    .line 2391206
    iput-object p2, p0, LX/Glj;->c:LX/0Zb;

    .line 2391207
    iput-object p3, p0, LX/Glj;->d:LX/0kv;

    .line 2391208
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Glj;->a:Z

    .line 2391209
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILX/Gkq;LX/Gkm;)V
    .locals 3

    .prologue
    .line 2391210
    iget-object v0, p0, LX/Glj;->c:LX/0Zb;

    const-string v1, "groups_grid_group_menu_selected"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2391211
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2391212
    const-string v1, "groups_grid"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 2391213
    iget-object v1, p3, LX/Gkq;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2391214
    invoke-virtual {v0, v1}, LX/0oG;->d(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    iget-object v1, p0, LX/Glj;->d:LX/0kv;

    iget-object v2, p0, LX/Glj;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "menu_option"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    const-string v1, "is_favorite"

    .line 2391215
    iget-boolean v2, p3, LX/Gkq;->g:Z

    move v2, v2

    .line 2391216
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    const-string v1, "badge_count"

    .line 2391217
    iget v2, p3, LX/Gkq;->f:I

    move v2, v2

    .line 2391218
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    const-string v1, "ordering"

    invoke-virtual {p4}, LX/Gkm;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2391219
    :cond_0
    return-void
.end method
