.class public LX/Ff8;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:LX/FiL;

.field private final b:LX/FiR;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:LX/A0U;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SeeMoreResultPageUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FiL;LX/FiR;Landroid/view/LayoutInflater;LX/A0U;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2267594
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2267595
    iput-object p1, p0, LX/Ff8;->a:LX/FiL;

    .line 2267596
    iput-object p2, p0, LX/Ff8;->b:LX/FiR;

    .line 2267597
    iput-object p3, p0, LX/Ff8;->c:Landroid/view/LayoutInflater;

    .line 2267598
    iput-object p4, p0, LX/Ff8;->d:LX/A0U;

    .line 2267599
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2267600
    iget-object v0, p0, LX/Ff8;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0307ed

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2267579
    check-cast p2, Lcom/facebook/search/model/SeeMoreResultPageUnit;

    .line 2267580
    check-cast p3, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2267581
    iget-object v0, p0, LX/Ff8;->a:LX/FiL;

    .line 2267582
    iget-object v1, p2, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object v1, v1

    .line 2267583
    iget-object p0, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object p0, p0

    .line 2267584
    invoke-virtual {p3, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2267585
    iget-object p0, v0, LX/FiL;->a:LX/8i7;

    iget-object p1, v0, LX/FiL;->b:LX/0Uh;

    invoke-static {p0, v1, p1}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->a(LX/8i7;Lcom/facebook/search/model/EntityTypeaheadUnit;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p3, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2267586
    iget-object p0, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    move-object p0, p0

    .line 2267587
    invoke-virtual {p3, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2267588
    iget-object p0, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    move-object p0, p0

    .line 2267589
    invoke-virtual {p3, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2267590
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SeeMoreResultPageUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2267591
    iput-object p1, p0, LX/Ff8;->e:LX/0Px;

    .line 2267592
    const v0, -0x1bb03b2f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2267593
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2267578
    iget-object v0, p0, LX/Ff8;->e:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ff8;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2267577
    iget-object v0, p0, LX/Ff8;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2267576
    int-to-long v0, p1

    return-wide v0
.end method
