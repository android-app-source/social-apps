.class public final LX/Frq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "LX/FsL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G4x;

.field public final synthetic b:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

.field public final synthetic c:LX/FqO;

.field public final synthetic d:J

.field public final synthetic e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic f:LX/Frv;


# direct methods
.method public constructor <init>(LX/Frv;LX/G4x;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;LX/FqO;JLjava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 1

    .prologue
    .line 2296136
    iput-object p1, p0, LX/Frq;->f:LX/Frv;

    iput-object p2, p0, LX/Frq;->a:LX/G4x;

    iput-object p3, p0, LX/Frq;->b:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    iput-object p4, p0, LX/Frq;->c:LX/FqO;

    iput-wide p5, p0, LX/Frq;->d:J

    iput-object p7, p0, LX/Frq;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2296137
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2296138
    check-cast p1, LX/FsL;

    .line 2296139
    iget-object v0, p0, LX/Frq;->a:LX/G4x;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/G4x;->a(Z)V

    .line 2296140
    iget-object v0, p1, LX/FsL;->b:LX/0ta;

    invoke-static {v0}, Lcom/facebook/timeline/protocol/ResultSource;->fromGraphQLResultDataFreshness(LX/0ta;)Lcom/facebook/timeline/protocol/ResultSource;

    move-result-object v0

    .line 2296141
    iget-object v1, p0, LX/Frq;->b:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    iget-object v2, p1, LX/FsL;->a:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a(Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;Lcom/facebook/timeline/protocol/ResultSource;)V

    .line 2296142
    iget-object v1, p0, LX/Frq;->c:LX/FqO;

    iget-object v2, p1, LX/FsL;->b:LX/0ta;

    iget-wide v4, p0, LX/Frq;->d:J

    invoke-interface {v1, v2, v0, v4, v5}, LX/FqO;->a(LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;J)V

    .line 2296143
    iget-object v0, p0, LX/Frq;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2296144
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2296145
    iget-object v0, p0, LX/Frq;->f:LX/Frv;

    iget-object v0, v0, LX/Frv;->a:LX/BQB;

    invoke-virtual {v0}, LX/BQB;->j()V

    .line 2296146
    iget-object v0, p0, LX/Frq;->a:LX/G4x;

    sget-object v1, LX/G5A;->FAILED:LX/G5A;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/G4x;->a(LX/G5A;LX/Fsp;)V

    .line 2296147
    return-void
.end method
