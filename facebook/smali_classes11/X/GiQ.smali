.class public final LX/GiQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GiS;

.field private b:Ljava/lang/String;

.field private c:J

.field private final d:LX/GiJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GiS;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2385792
    iput-object p1, p0, LX/GiQ;->a:LX/GiS;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2385793
    iput-object p2, p0, LX/GiQ;->b:Ljava/lang/String;

    .line 2385794
    iget-object v0, p1, LX/GiS;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/GiQ;->c:J

    .line 2385795
    const/4 v0, 0x0

    iput-object v0, p0, LX/GiQ;->d:LX/GiJ;

    .line 2385796
    return-void
.end method

.method public constructor <init>(LX/GiS;Ljava/lang/String;LX/GiJ;)V
    .locals 2

    .prologue
    .line 2385787
    iput-object p1, p0, LX/GiQ;->a:LX/GiS;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2385788
    iput-object p2, p0, LX/GiQ;->b:Ljava/lang/String;

    .line 2385789
    iget-object v0, p1, LX/GiS;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/GiQ;->c:J

    .line 2385790
    iput-object p3, p0, LX/GiQ;->d:LX/GiJ;

    .line 2385791
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2385785
    iget-object v0, p0, LX/GiQ;->a:LX/GiS;

    iget-object v0, v0, LX/GiS;->c:LX/1vi;

    new-instance v1, LX/2jQ;

    iget-object v2, p0, LX/GiQ;->b:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, LX/2jQ;-><init>(Ljava/lang/Throwable;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2385786
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2385753
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2385754
    const/4 v2, 0x0

    .line 2385755
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385756
    instance-of v0, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionTailLoadFragmentModel;

    if-eqz v0, :cond_2

    .line 2385757
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385758
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionTailLoadFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionTailLoadFragmentModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionUnitsFragmentModel;

    move-result-object v2

    .line 2385759
    :cond_0
    :goto_0
    iget-object v0, p0, LX/GiQ;->d:LX/GiJ;

    if-eqz v0, :cond_1

    .line 2385760
    iget-object v0, p0, LX/GiQ;->d:LX/GiJ;

    invoke-interface {v0, v2}, LX/GiJ;->a(Ljava/lang/Object;)V

    .line 2385761
    :cond_1
    if-nez v2, :cond_6

    .line 2385762
    iget-object v0, p0, LX/GiQ;->a:LX/GiS;

    iget-object v0, v0, LX/GiS;->c:LX/1vi;

    new-instance v1, LX/2jS;

    const-string v2, "ERROR_INVALID_RESPONSE"

    iget-object v3, p0, LX/GiQ;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/2jS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2385763
    :goto_1
    return-void

    .line 2385764
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385765
    instance-of v0, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionHeadLoadFragmentModel;

    if-eqz v0, :cond_3

    .line 2385766
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385767
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionHeadLoadFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionHeadLoadFragmentModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchReactionUnitsFragmentModel;

    move-result-object v2

    goto :goto_0

    .line 2385768
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385769
    instance-of v0, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeDashboardReactionTailLoadFragmentModel;

    if-eqz v0, :cond_4

    .line 2385770
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385771
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeDashboardReactionTailLoadFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeDashboardReactionTailLoadFragmentModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionUnitsFragmentModel;

    move-result-object v2

    goto :goto_0

    .line 2385772
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385773
    instance-of v0, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionTailLoadFragmentModel;

    if-eqz v0, :cond_5

    .line 2385774
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385775
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionTailLoadFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionTailLoadFragmentModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;

    move-result-object v2

    goto :goto_0

    .line 2385776
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385777
    instance-of v0, v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionHeadLoadFragmentModel;

    if-eqz v0, :cond_0

    .line 2385778
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385779
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionHeadLoadFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionHeadLoadFragmentModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeLeagueReactionUnitsFragmentModel;

    move-result-object v2

    goto :goto_0

    .line 2385780
    :cond_6
    invoke-interface {v2}, LX/9qT;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2385781
    iget-object v0, p0, LX/GiQ;->a:LX/GiS;

    iget-object v0, v0, LX/GiS;->c:LX/1vi;

    new-instance v1, LX/2jS;

    const-string v2, "NO_UNITS_RETURNED"

    iget-object v3, p0, LX/GiQ;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/2jS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    .line 2385782
    :cond_7
    iget-wide v8, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v0, v8

    .line 2385783
    iget-wide v4, p0, LX/GiQ;->c:J

    sub-long v4, v0, v4

    .line 2385784
    iget-object v0, p0, LX/GiQ;->a:LX/GiS;

    iget-object v0, v0, LX/GiS;->c:LX/1vi;

    new-instance v1, LX/2jV;

    iget-object v3, p0, LX/GiQ;->b:Ljava/lang/String;

    iget-wide v6, p0, LX/GiQ;->c:J

    invoke-direct/range {v1 .. v7}, LX/2jV;-><init>(LX/9qT;Ljava/lang/String;JJ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method
