.class public final LX/FfI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/FfJ;

.field private b:LX/Cw8;


# direct methods
.method public constructor <init>(LX/FfJ;LX/Cw8;)V
    .locals 0

    .prologue
    .line 2267829
    iput-object p1, p0, LX/FfI;->a:LX/FfJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2267830
    iput-object p2, p0, LX/FfI;->b:LX/Cw8;

    .line 2267831
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2267827
    iget-object v0, p0, LX/FfI;->a:LX/FfJ;

    iget-object v1, p0, LX/FfI;->b:LX/Cw8;

    invoke-static {v0, v1}, LX/FfJ;->e(LX/FfJ;LX/Cw8;)V

    .line 2267828
    return-void
.end method

.method public final a(LX/0Px;LX/0Px;Ljava/lang/String;ZZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/EntityTypeaheadUnit;",
            ">;",
            "LX/0Px",
            "<",
            "LX/CwL;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 2267832
    iget-object v0, p0, LX/FfI;->a:LX/FfJ;

    iget-object v1, p0, LX/FfI;->b:LX/Cw8;

    .line 2267833
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2267834
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2267835
    new-instance v2, Lcom/facebook/search/model/SeeMoreResultPageUnit;

    invoke-direct {v2, v3}, Lcom/facebook/search/model/SeeMoreResultPageUnit;-><init>(Lcom/facebook/search/model/EntityTypeaheadUnit;)V

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2267836
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 2267837
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 2267838
    move-object v2, v3

    .line 2267839
    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    .line 2267840
    const/4 p0, 0x0

    .line 2267841
    if-nez v2, :cond_1

    .line 2267842
    invoke-static {v0, v1}, LX/FfJ;->e(LX/FfJ;LX/Cw8;)V

    .line 2267843
    :goto_1
    return-void

    .line 2267844
    :cond_1
    iget-object v7, v0, LX/FfJ;->h:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2267845
    iget-object v7, v0, LX/FfJ;->b:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Px;

    .line 2267846
    if-eqz v7, :cond_9

    .line 2267847
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p3

    .line 2267848
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1, v7}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 2267849
    invoke-interface {p1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2267850
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    .line 2267851
    :goto_2
    if-eqz v3, :cond_5

    iget-object v7, v0, LX/FfJ;->c:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2267852
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 2267853
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p4

    :goto_3
    if-ge p0, p4, :cond_2

    invoke-virtual {v3, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/CwL;

    .line 2267854
    iget-object p5, v7, LX/CwL;->b:Ljava/lang/String;

    move-object p5, p5

    .line 2267855
    invoke-interface {p2, p5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267856
    add-int/lit8 p0, p0, 0x1

    goto :goto_3

    .line 2267857
    :cond_2
    iget-object v7, v0, LX/FfJ;->c:Ljava/util/Map;

    invoke-interface {v7, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267858
    :cond_3
    iget-object v7, v0, LX/FfJ;->d:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267859
    iget-object v7, v0, LX/FfJ;->b:Ljava/util/Map;

    invoke-interface {v7, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267860
    iget-object v7, v0, LX/FfJ;->e:Ljava/util/Map;

    invoke-interface {v7, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267861
    if-eqz v5, :cond_7

    .line 2267862
    iget-object v7, v0, LX/FfJ;->g:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2267863
    :goto_4
    if-eqz v6, :cond_8

    .line 2267864
    iget-object v7, v0, LX/FfJ;->f:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2267865
    :goto_5
    iget-object v7, v0, LX/FfJ;->j:LX/FfG;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p4

    move-object p0, v1

    move-object p2, v3

    const/4 v2, 0x0

    .line 2267866
    iget-object p5, v7, LX/FfG;->e:Ljava/util/Map;

    invoke-interface {p5, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, LX/Ff8;

    .line 2267867
    if-nez p5, :cond_a

    .line 2267868
    :cond_4
    :goto_6
    goto :goto_1

    .line 2267869
    :cond_5
    if-eqz v3, :cond_3

    iget-object v7, v0, LX/FfJ;->d:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2267870
    iget-object v7, v0, LX/FfJ;->d:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/CwL;

    .line 2267871
    iget-object p2, v7, LX/CwL;->b:Ljava/lang/String;

    move-object p4, p2

    .line 2267872
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p5

    move p2, p0

    :goto_7
    if-ge p2, p5, :cond_3

    invoke-virtual {v3, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/CwL;

    .line 2267873
    iget-object p0, v7, LX/CwL;->b:Ljava/lang/String;

    move-object p0, p0

    .line 2267874
    invoke-virtual {p0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2267875
    iget-object p0, v0, LX/FfJ;->c:Ljava/util/Map;

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map;

    invoke-interface {p0, p4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267876
    :cond_6
    add-int/lit8 v7, p2, 0x1

    move p2, v7

    goto :goto_7

    .line 2267877
    :cond_7
    iget-object v7, v0, LX/FfJ;->g:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2267878
    :cond_8
    iget-object v7, v0, LX/FfJ;->f:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    move p3, p0

    move-object p1, v2

    goto/16 :goto_2

    .line 2267879
    :cond_a
    invoke-virtual {p5, p1}, LX/Ff8;->a(LX/0Px;)V

    .line 2267880
    iget-object p5, v7, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {p5, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, LX/Fje;

    .line 2267881
    if-eqz p5, :cond_4

    .line 2267882
    iget-object v0, v7, LX/FfG;->g:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    .line 2267883
    if-eqz v0, :cond_b

    if-lez p4, :cond_b

    if-nez p3, :cond_b

    .line 2267884
    invoke-virtual {v0, p2}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->setFilters(LX/0Px;)V

    .line 2267885
    :cond_b
    iget-object v0, v7, LX/FfG;->h:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    .line 2267886
    if-eqz v0, :cond_e

    if-nez p4, :cond_e

    .line 2267887
    invoke-virtual {p2}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v3

    move v1, v2

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    if-nez v1, :cond_d

    .line 2267888
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CwL;

    .line 2267889
    iget-object v4, v1, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-object v1, v4

    .line 2267890
    if-eqz v1, :cond_c

    const/4 v1, 0x1

    goto :goto_8

    :cond_c
    move v1, v2

    goto :goto_8

    .line 2267891
    :cond_d
    if-eqz v1, :cond_10

    :goto_9
    invoke-virtual {v0, p2}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->setFilters(LX/0Px;)V

    .line 2267892
    :cond_e
    if-nez p3, :cond_f

    .line 2267893
    invoke-static {p0}, LX/Cw8;->getPositionOfCoreFilterType(LX/Cw8;)I

    move-result v0

    .line 2267894
    iget-object v1, v7, LX/FfG;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-ne v1, v0, :cond_f

    .line 2267895
    iget-object v0, v7, LX/FfG;->j:LX/Cvm;

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v3, v7, LX/FfG;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-static {v3}, LX/Cw8;->getCoreFilterTypeAt(I)LX/Cw8;

    move-result-object v3

    invoke-static {v7}, LX/FfG;->i(LX/FfG;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, LX/Cvm;->a(Ljava/lang/Boolean;LX/Cw8;Ljava/lang/String;)V

    .line 2267896
    :cond_f
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2267897
    sget-object v0, LX/EQG;->EMPTY:LX/EQG;

    invoke-virtual {p5, v0}, LX/Fje;->setState(LX/EQG;)V

    goto/16 :goto_6

    .line 2267898
    :cond_10
    const/4 p2, 0x0

    goto :goto_9

    .line 2267899
    :cond_11
    iget-object v0, v7, LX/FfG;->d:LX/FfJ;

    invoke-virtual {v0, p0}, LX/FfJ;->b(LX/Cw8;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2267900
    sget-object v0, LX/EQG;->LOADING_MORE:LX/EQG;

    invoke-virtual {p5, v0}, LX/Fje;->setState(LX/EQG;)V

    .line 2267901
    iget-object v0, p5, LX/Fje;->q:LX/0g8;

    move-object v0, v0

    .line 2267902
    if-nez p3, :cond_4

    .line 2267903
    invoke-interface {v0, v2}, LX/0g8;->d(I)V

    .line 2267904
    new-instance v0, LX/FfC;

    invoke-direct {v0, v7, p0}, LX/FfC;-><init>(LX/FfG;LX/Cw8;)V

    move-object v0, v0

    .line 2267905
    invoke-virtual {p5, v0, v2}, LX/Fje;->a(LX/EQF;I)V

    goto/16 :goto_6

    .line 2267906
    :cond_12
    sget-object v0, LX/EQG;->LOADING_FINISHED:LX/EQG;

    invoke-virtual {p5, v0}, LX/Fje;->setState(LX/EQG;)V

    goto/16 :goto_6
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2267824
    iget-object v0, p0, LX/FfI;->a:LX/FfJ;

    iget-object v1, p0, LX/FfI;->b:LX/Cw8;

    .line 2267825
    iget-object p0, v0, LX/FfJ;->h:Ljava/util/Set;

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2267826
    return-void
.end method
