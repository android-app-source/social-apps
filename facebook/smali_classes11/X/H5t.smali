.class public final LX/H5t;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/H5u;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Landroid/view/View$OnClickListener;

.field public d:Landroid/view/View$OnClickListener;

.field public final synthetic e:LX/H5u;


# direct methods
.method public constructor <init>(LX/H5u;)V
    .locals 1

    .prologue
    .line 2425469
    iput-object p1, p0, LX/H5t;->e:LX/H5u;

    .line 2425470
    move-object v0, p1

    .line 2425471
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2425472
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2425473
    const-string v0, "NotificationsLoggedOutPushInterstitialComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2425449
    if-ne p0, p1, :cond_1

    .line 2425450
    :cond_0
    :goto_0
    return v0

    .line 2425451
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2425452
    goto :goto_0

    .line 2425453
    :cond_3
    check-cast p1, LX/H5t;

    .line 2425454
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2425455
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2425456
    if-eq v2, v3, :cond_0

    .line 2425457
    iget-object v2, p0, LX/H5t;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/H5t;->a:Ljava/lang/String;

    iget-object v3, p1, LX/H5t;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2425458
    goto :goto_0

    .line 2425459
    :cond_5
    iget-object v2, p1, LX/H5t;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2425460
    :cond_6
    iget-object v2, p0, LX/H5t;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/H5t;->b:Ljava/lang/String;

    iget-object v3, p1, LX/H5t;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2425461
    goto :goto_0

    .line 2425462
    :cond_8
    iget-object v2, p1, LX/H5t;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2425463
    :cond_9
    iget-object v2, p0, LX/H5t;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/H5t;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/H5t;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2425464
    goto :goto_0

    .line 2425465
    :cond_b
    iget-object v2, p1, LX/H5t;->c:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_a

    .line 2425466
    :cond_c
    iget-object v2, p0, LX/H5t;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/H5t;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/H5t;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2425467
    goto :goto_0

    .line 2425468
    :cond_d
    iget-object v2, p1, LX/H5t;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
