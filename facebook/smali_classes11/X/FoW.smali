.class public final LX/FoW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fig/listitem/FigListItem;

.field public final synthetic b:LX/Fob;


# direct methods
.method public constructor <init>(LX/Fob;Lcom/facebook/fig/listitem/FigListItem;)V
    .locals 0

    .prologue
    .line 2289701
    iput-object p1, p0, LX/FoW;->b:LX/Fob;

    iput-object p2, p0, LX/FoW;->a:Lcom/facebook/fig/listitem/FigListItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x7627de65

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2289702
    iget-object v0, p0, LX/FoW;->b:LX/Fob;

    iget-object v0, v0, LX/Fob;->e:LX/FoT;

    if-eqz v0, :cond_0

    .line 2289703
    iget-object v0, p0, LX/FoW;->b:LX/Fob;

    iget-object v2, v0, LX/Fob;->e:LX/FoT;

    iget-object v0, p0, LX/FoW;->a:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0}, Lcom/facebook/fig/listitem/FigListItem;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2289704
    iget-object v5, v2, LX/FoT;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const p1, 0x7fba55ef

    if-ne v4, p1, :cond_1

    const/4 v4, 0x1

    :goto_0
    sget-object p1, LX/BOS;->CHARITY_FROM_CURATED_PICKER:LX/BOS;

    invoke-static {v5, v6, p0, v4, p1}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a$redex0(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;Ljava/lang/String;Ljava/lang/String;ZLX/BOS;)V

    .line 2289705
    :cond_0
    const v0, 0x4bfdbdc0    # 3.3258368E7f

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2289706
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method
