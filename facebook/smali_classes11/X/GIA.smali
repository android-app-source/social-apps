.class public LX/GIA;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/2U3;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/GDs;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/GLN;

.field private final d:LX/1Ck;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/GNI;

.field private final g:LX/GOy;

.field public final h:LX/GDg;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

.field public m:Z


# direct methods
.method public constructor <init>(LX/2U3;LX/0Or;LX/GLN;LX/1Ck;LX/GNI;LX/0Or;LX/GOy;LX/GDg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2U3;",
            "LX/0Or",
            "<",
            "LX/GDs;",
            ">;",
            "LX/GLN;",
            "LX/1Ck;",
            "LX/GNI;",
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;",
            "LX/GOy;",
            "LX/GDg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2335839
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2335840
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GIA;->m:Z

    .line 2335841
    iput-object p1, p0, LX/GIA;->a:LX/2U3;

    .line 2335842
    iput-object p2, p0, LX/GIA;->b:LX/0Or;

    .line 2335843
    iput-object p3, p0, LX/GIA;->c:LX/GLN;

    .line 2335844
    iput-object p4, p0, LX/GIA;->d:LX/1Ck;

    .line 2335845
    iput-object p5, p0, LX/GIA;->f:LX/GNI;

    .line 2335846
    iput-object p6, p0, LX/GIA;->e:LX/0Or;

    .line 2335847
    iput-object p7, p0, LX/GIA;->g:LX/GOy;

    .line 2335848
    iput-object p8, p0, LX/GIA;->h:LX/GDg;

    .line 2335849
    return-void
.end method

.method public static a(LX/0QB;)LX/GIA;
    .locals 1

    .prologue
    .line 2335850
    invoke-static {p0}, LX/GIA;->b(LX/0QB;)LX/GIA;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/GGB;)V
    .locals 3

    .prologue
    .line 2335851
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2335852
    const-string v1, "Unsupported status for account component"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335853
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335854
    if-eqz p1, :cond_0

    .line 2335855
    const-string v1, "status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335856
    invoke-virtual {p1}, LX/GGB;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335857
    :cond_0
    const-string v1, "page id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335858
    iget-object v1, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335859
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335860
    iget-object v1, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2335861
    const-string v1, "objective:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335862
    iget-object v1, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335863
    :cond_1
    iget-object v1, p0, LX/GIA;->a:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2335864
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;)V
    .locals 3

    .prologue
    .line 2335865
    iget-object v0, p0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335866
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    move-object v0, v1

    .line 2335867
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335868
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 6
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 2335882
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2335883
    iput-object p1, p0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335884
    iput-object p2, p0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2335885
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2335886
    iget-object v2, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v2

    .line 2335887
    iget-object v3, p0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2335888
    new-instance v4, LX/GI1;

    invoke-direct {v4, p0}, LX/GI1;-><init>(LX/GIA;)V

    invoke-virtual {v3, v4}, LX/GCE;->a(LX/8wQ;)V

    .line 2335889
    new-instance v4, LX/GI2;

    invoke-direct {v4, p0}, LX/GI2;-><init>(LX/GIA;)V

    invoke-virtual {v3, v4}, LX/GCE;->a(LX/8wQ;)V

    .line 2335890
    const/4 v4, 0x3

    invoke-direct {p0, v3}, LX/GIA;->b(LX/GCE;)LX/GFR;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/GCE;->a(ILX/GFR;)V

    .line 2335891
    sget v4, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->y:I

    invoke-direct {p0, v3}, LX/GIA;->c(LX/GCE;)LX/GFR;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/GCE;->a(ILX/GFR;)V

    .line 2335892
    sget v4, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u:I

    invoke-direct {p0, v3}, LX/GIA;->b(LX/GCE;)LX/GFR;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/GCE;->a(ILX/GFR;)V

    .line 2335893
    const/16 v4, 0x12d

    invoke-direct {p0, v3}, LX/GIA;->d(LX/GCE;)LX/GFR;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/GCE;->a(ILX/GFR;)V

    .line 2335894
    invoke-direct {p0, v0}, LX/GIA;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2335895
    iget-object v4, p0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const v5, 0x7f0827ec

    invoke-virtual {v4, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(I)V

    .line 2335896
    iget-object v4, p0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    new-instance v5, LX/GI3;

    invoke-direct {v5, p0, v0, v3}, LX/GI3;-><init>(LX/GIA;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;LX/GCE;)V

    invoke-virtual {v4, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2335897
    :cond_0
    invoke-direct {p0, v2}, LX/GIA;->b(LX/GGB;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2335898
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v0

    invoke-direct {p0, v0}, LX/GIA;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;)V

    .line 2335899
    :goto_0
    return-void

    .line 2335900
    :cond_1
    sget-object v0, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v2, v0, :cond_2

    sget-object v0, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v2, v0, :cond_2

    sget-object v0, LX/GGB;->FINISHED:LX/GGB;

    if-eq v2, v0, :cond_2

    sget-object v0, LX/GGB;->REJECTED:LX/GGB;

    if-ne v2, v0, :cond_5

    .line 2335901
    :cond_2
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2335902
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2335903
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GIA;->i:LX/0Px;

    .line 2335904
    iget-object v0, p0, LX/GIA;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 2335905
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v1, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-direct {p0, v0}, LX/GIA;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V

    goto :goto_0

    .line 2335906
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2335907
    :cond_4
    invoke-static {p0}, LX/GIA;->b(LX/GIA;)V

    goto :goto_0

    .line 2335908
    :cond_5
    invoke-direct {p0, v2}, LX/GIA;->a(LX/GGB;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2335869
    invoke-static {p1}, LX/GNI;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)LX/ADX;

    move-result-object v0

    invoke-virtual {v0}, LX/ADX;->isLockedIntoPrepay()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GIA;->g:LX/GOy;

    invoke-static {p1}, LX/GNI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GOy;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/GIA;Ljava/lang/String;)V
    .locals 8

    .prologue
    const v2, 0x5a0007

    .line 2335870
    iget-object v0, p0, LX/GIA;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2335871
    iget-object v0, p0, LX/GIA;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2335872
    iget-object v0, p0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2335873
    iget-object v1, p0, LX/GIA;->d:LX/1Ck;

    sget-object v2, LX/GDr;->FETCH_AD_ACCOUNTS:LX/GDr;

    iget-object v0, p0, LX/GIA;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GDs;

    iget-object v3, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2335874
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2335875
    iget-object v5, v0, LX/GDs;->a:LX/0tX;

    .line 2335876
    new-instance v4, LX/ABG;

    invoke-direct {v4}, LX/ABG;-><init>()V

    move-object v4, v4

    .line 2335877
    const-string v6, "page_id"

    invoke-virtual {v4, v6, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "fetch_saved_audiences"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v4

    const-string v6, "num_of_saved_audiences_to_fetch"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/ABG;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2335878
    new-instance v5, LX/GDq;

    invoke-direct {v5, v0}, LX/GDq;-><init>(LX/GDs;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2335879
    new-instance v3, LX/GI7;

    invoke-direct {v3, p0, p1}, LX/GI7;-><init>(LX/GIA;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2335880
    return-void
.end method

.method private b(LX/GCE;)LX/GFR;
    .locals 1

    .prologue
    .line 2335881
    new-instance v0, LX/GI4;

    invoke-direct {v0, p0, p1}, LX/GI4;-><init>(LX/GIA;LX/GCE;)V

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GIA;
    .locals 9

    .prologue
    .line 2335825
    new-instance v0, LX/GIA;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v1

    check-cast v1, LX/2U3;

    const/16 v2, 0x168a

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const-class v3, LX/GLN;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/GLN;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/GNI;->a(LX/0QB;)LX/GNI;

    move-result-object v5

    check-cast v5, LX/GNI;

    const/16 v6, 0x103d

    invoke-static {p0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v7

    check-cast v7, LX/GOy;

    invoke-static {p0}, LX/GDg;->a(LX/0QB;)LX/GDg;

    move-result-object v8

    check-cast v8, LX/GDg;

    invoke-direct/range {v0 .. v8}, LX/GIA;-><init>(LX/2U3;LX/0Or;LX/GLN;LX/1Ck;LX/GNI;LX/0Or;LX/GOy;LX/GDg;)V

    .line 2335826
    return-object v0
.end method

.method public static b(LX/GIA;)V
    .locals 3

    .prologue
    .line 2335827
    iget-object v0, p0, LX/GIA;->c:LX/GLN;

    iget-object v1, p0, LX/GIA;->i:LX/0Px;

    new-instance v2, LX/GI8;

    invoke-direct {v2, p0}, LX/GI8;-><init>(LX/GIA;)V

    invoke-static {v1, v2}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GLN;->a(Ljava/util/List;)LX/GLM;

    move-result-object v0

    .line 2335828
    iget-object v1, p0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335829
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    move-object v1, v2

    .line 2335830
    new-instance v2, LX/GI9;

    invoke-direct {v2, p0}, LX/GI9;-><init>(LX/GIA;)V

    invoke-virtual {v1, v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2335831
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2335832
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GIA;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2335833
    iget-object v0, p0, LX/GIA;->i:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2335834
    iget-object v0, p0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335835
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    move-object v0, v2

    .line 2335836
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->setSelected(I)V

    .line 2335837
    :cond_0
    return-void

    .line 2335838
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V
    .locals 3

    .prologue
    .line 2335821
    iget-object v0, p0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335822
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    move-object v0, v1

    .line 2335823
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335824
    return-void
.end method

.method private b(LX/GGB;)Z
    .locals 1

    .prologue
    .line 2335820
    sget-object v0, LX/GGB;->ACTIVE:LX/GGB;

    if-eq p1, v0, :cond_1

    sget-object v0, LX/GGB;->PENDING:LX/GGB;

    if-eq p1, v0, :cond_1

    sget-object v0, LX/GGB;->CREATING:LX/GGB;

    if-eq p1, v0, :cond_1

    sget-object v0, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq p1, v0, :cond_1

    sget-object v0, LX/GGB;->FINISHED:LX/GGB;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, LX/GGB;->PAUSED:LX/GGB;

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/GCE;)LX/GFR;
    .locals 1

    .prologue
    .line 2335819
    new-instance v0, LX/GI5;

    invoke-direct {v0, p0, p1}, LX/GI5;-><init>(LX/GIA;LX/GCE;)V

    return-object v0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2335817
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2335818
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/GCE;)LX/GFR;
    .locals 1

    .prologue
    .line 2335816
    new-instance v0, LX/GI6;

    invoke-direct {v0, p0, p1}, LX/GI6;-><init>(LX/GIA;LX/GCE;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2335811
    invoke-super {p0}, LX/GHg;->a()V

    .line 2335812
    iget-object v0, p0, LX/GIA;->d:LX/1Ck;

    sget-object v1, LX/GDr;->FETCH_AD_ACCOUNTS:LX/GDr;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2335813
    iput-object v2, p0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335814
    iput-object v2, p0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2335815
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2335808
    const-string v0, "selected_ad_account_id"

    iget-object v1, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335809
    const-string v0, "selected_ad_account_has_payment_method"

    invoke-direct {p0}, LX/GIA;->c()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2335810
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335807
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    invoke-direct {p0, p1, p2}, LX/GIA;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2335805
    iput-object p1, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2335806
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335794
    if-nez p1, :cond_0

    .line 2335795
    :goto_0
    return-void

    .line 2335796
    :cond_0
    iget-object v0, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2335797
    const-string v1, "selected_ad_account_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2335798
    iget-object v2, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Ljava/lang/String;)V

    .line 2335799
    iget-object v2, p0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335800
    iget-object v3, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    move-object v2, v3

    .line 2335801
    iget-object v3, p0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v3, v1}, LX/GG6;->a(LX/0Px;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->setSelected(I)V

    .line 2335802
    const-string v2, "selected_ad_account_has_payment_method"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, LX/GIA;->m:Z

    .line 2335803
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2335804
    new-instance v3, LX/GFj;

    invoke-direct {v3, v0, v1}, LX/GFj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0
.end method
