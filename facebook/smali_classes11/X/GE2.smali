.class public LX/GE2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Integer;

.field public static final b:Ljava/lang/Integer;


# instance fields
.field private final c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2330801
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/GE2;->a:Ljava/lang/Integer;

    .line 2330802
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/GE2;->b:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2330804
    iput-object p1, p0, LX/GE2;->c:LX/0tX;

    .line 2330805
    return-void
.end method

.method public static b(LX/0QB;)LX/GE2;
    .locals 2

    .prologue
    .line 2330806
    new-instance v1, LX/GE2;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/GE2;-><init>(LX/0tX;)V

    .line 2330807
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;Ljava/lang/Integer;Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;",
            "Ljava/lang/Integer;",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2330808
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2330809
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2330810
    invoke-static {}, LX/ABW;->a()LX/ABV;

    move-result-object v2

    .line 2330811
    const-string v1, "page_id"

    invoke-virtual {v2, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "component_app"

    invoke-virtual {v1, v3, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "story_id"

    invoke-virtual {v1, v3, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "ad_account_id"

    invoke-virtual {v1, v3, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "duration"

    invoke-virtual {v1, v3, p5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "selected_budget"

    invoke-virtual {v1, v3, p6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "audience"

    if-nez p8, :cond_2

    :goto_0
    invoke-virtual {v1, v3, p7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "audience_id"

    invoke-virtual {v1, v3, p8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "targeting"

    if-eqz p9, :cond_0

    if-eqz p8, :cond_3

    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "max_budgets_count"

    invoke-virtual/range {p10 .. p10}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "use_default_settings"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2330812
    if-eqz p11, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-object/from16 v0, p11

    if-eq v0, v1, :cond_1

    .line 2330813
    const-string v1, "objective"

    move-object/from16 v0, p11

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2330814
    :cond_1
    iget-object v1, p0, LX/GE2;->c:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2330815
    new-instance v2, LX/GE0;

    invoke-direct {v2, p0}, LX/GE0;-><init>(LX/GE2;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1

    .line 2330816
    :cond_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->CUSTOM_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object p7

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    const/4 v5, 0x1

    invoke-virtual {p9, v1, v5}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(ZZ)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
