.class public final LX/GC7;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2328042
    iput-object p1, p0, LX/GC7;->b:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iput-object p2, p0, LX/GC7;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2328043
    iget-object v0, p0, LX/GC7;->b:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    .line 2328044
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 2328045
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->d:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2328046
    new-instance v1, LX/0ju;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f08003f

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f083514

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance p0, LX/GC8;

    invoke-direct {p0, v0}, LX/GC8;-><init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;)V

    invoke-virtual {v1, v2, p0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2328047
    :goto_0
    return-void

    .line 2328048
    :cond_0
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2328049
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v1, v2

    .line 2328050
    invoke-virtual {v1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    .line 2328051
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 2328052
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2328053
    const-string v1, "error"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 2328054
    const-string v2, "error_user_title"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2328055
    invoke-static {v0, v1}, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->b$redex0(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2328056
    :catch_0
    move-exception v1

    .line 2328057
    const-string v2, "ResetPasswordFragment"

    const-string p0, "inconceivable JSON exception"

    invoke-static {v2, p0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2328058
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2328059
    iget-object v0, p0, LX/GC7;->b:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->c:LX/2Ne;

    iget-object v1, p0, LX/GC7;->b:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->i:Ljava/lang/String;

    .line 2328060
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/GB4;->RECOVERY_SUCCESS:LX/GB4;

    invoke-virtual {v4}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "account_recovery"

    .line 2328061
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2328062
    move-object v3, v3

    .line 2328063
    const-string v4, "crypted_id"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2328064
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2328065
    :goto_0
    iget-object v1, p0, LX/GC7;->b:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->l:LX/2Nk;

    iget-object v2, p0, LX/GC7;->a:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/2Nk;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2328066
    return-void

    .line 2328067
    :cond_0
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 2328068
    goto :goto_0
.end method
