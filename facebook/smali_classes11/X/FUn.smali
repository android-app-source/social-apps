.class public LX/FUn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v0, -0x80000000

    .line 2249646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2249647
    iput v0, p0, LX/FUn;->a:I

    .line 2249648
    iput v0, p0, LX/FUn;->b:I

    .line 2249649
    const-wide/16 v0, -0xb

    iput-wide v0, p0, LX/FUn;->c:J

    return-void
.end method


# virtual methods
.method public final a(II)Z
    .locals 6

    .prologue
    .line 2249650
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2249651
    iget-wide v0, p0, LX/FUn;->c:J

    sub-long v0, v2, v0

    const-wide/16 v4, 0xa

    cmp-long v0, v0, v4

    if-gtz v0, :cond_0

    iget v0, p0, LX/FUn;->a:I

    if-ne v0, p1, :cond_0

    iget v0, p0, LX/FUn;->b:I

    if-eq v0, p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2249652
    :goto_0
    iput-wide v2, p0, LX/FUn;->c:J

    .line 2249653
    iput p1, p0, LX/FUn;->a:I

    .line 2249654
    iput p2, p0, LX/FUn;->b:I

    .line 2249655
    return v0

    .line 2249656
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
