.class public LX/GBM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;",
        "Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2326867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326868
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2326869
    check-cast p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;

    .line 2326870
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "cuid"

    .line 2326871
    iget-object v4, p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2326872
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "flow"

    .line 2326873
    iget-object v4, p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->d:LX/4gx;

    move-object v4, v4

    .line 2326874
    iget-object v4, v4, LX/4gx;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "provider"

    .line 2326875
    iget-object v4, p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->e:LX/4gy;

    move-object v4, v4

    .line 2326876
    iget-object v4, v4, LX/4gy;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2326877
    iget-object v1, p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->b:Ljava/util/List;

    if-nez v1, :cond_2

    .line 2326878
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2326879
    :goto_0
    move-object v1, v1

    .line 2326880
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2326881
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "emails"

    invoke-static {v1}, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2326882
    :cond_0
    iget-object v1, p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->c:Ljava/util/List;

    if-nez v1, :cond_3

    .line 2326883
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2326884
    :goto_1
    move-object v1, v1

    .line 2326885
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2326886
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "id_tokens"

    invoke-static {v1}, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2326887
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "openidConnectAccountRecovery"

    .line 2326888
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2326889
    move-object v1, v1

    .line 2326890
    const-string v2, "POST"

    .line 2326891
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2326892
    move-object v1, v1

    .line 2326893
    const-string v2, "auth/openidconnect_account_recovery"

    .line 2326894
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2326895
    move-object v1, v1

    .line 2326896
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 2326897
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2326898
    move-object v0, v1

    .line 2326899
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2326900
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2326901
    move-object v0, v0

    .line 2326902
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_2
    iget-object v1, p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->b:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_0

    :cond_3
    iget-object v1, p1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;->c:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2326903
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2326904
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodResult;

    return-object v0
.end method
