.class public LX/GE8;
.super Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod",
        "<",
        "Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0W9;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0rq;LX/0sa;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/0W9;LX/0ad;LX/0Ot;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0rq;",
            "LX/0sa;",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/GF4;",
            "LX/GG6;",
            "LX/0W9;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/2U3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331075
    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;-><init>(LX/0tX;LX/0rq;LX/0sa;LX/GF4;LX/1Ck;LX/GG6;LX/0ad;)V

    .line 2331076
    move-object/from16 v0, p7

    iput-object v0, p0, LX/GE8;->a:LX/0W9;

    .line 2331077
    move-object/from16 v0, p9

    iput-object v0, p0, LX/GE8;->b:LX/0Ot;

    .line 2331078
    return-void
.end method

.method public static a(LX/0QB;)LX/GE8;
    .locals 11

    .prologue
    .line 2330972
    new-instance v1, LX/GE8;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v7

    check-cast v7, LX/GG6;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v8

    check-cast v8, LX/0W9;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v10, 0x71

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v1 .. v10}, LX/GE8;-><init>(LX/0rq;LX/0sa;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/0W9;LX/0ad;LX/0Ot;)V

    .line 2330973
    move-object v0, v1

    .line 2330974
    return-object v0
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;LX/GGK;)Lcom/facebook/adinterfaces/model/CreativeAdModel;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2331064
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 2331065
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2331066
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v5}, LX/15i;->g(II)I

    move-result v0

    .line 2331067
    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v3, v0, v5, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2331068
    :goto_0
    invoke-virtual {v1, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2331069
    iput-object v1, p1, LX/GGK;->e:Ljava/lang/String;

    .line 2331070
    move-object v1, p1

    .line 2331071
    iput-object v0, v1, LX/GGK;->h:Ljava/lang/String;

    .line 2331072
    move-object v0, v1

    .line 2331073
    invoke-virtual {v0}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    return-object v0

    .line 2331074
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .locals 5

    .prologue
    .line 2331018
    invoke-virtual {p0, p1}, LX/GE8;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v1

    .line 2331019
    new-instance v0, LX/GGQ;

    invoke-direct {v0}, LX/GGQ;-><init>()V

    invoke-virtual {p0, p1}, LX/GE8;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v2

    .line 2331020
    iput-object v2, v0, LX/GGN;->a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2331021
    move-object v0, v0

    .line 2331022
    iput-object v1, v0, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2331023
    move-object v0, v0

    .line 2331024
    const-string v2, "boosted_local_awareness_mobile"

    .line 2331025
    iput-object v2, v0, LX/GGI;->m:Ljava/lang/String;

    .line 2331026
    move-object v0, v0

    .line 2331027
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v2

    .line 2331028
    iput-object v2, v0, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2331029
    move-object v0, v0

    .line 2331030
    iput-object p2, v0, LX/GGI;->c:Ljava/lang/String;

    .line 2331031
    move-object v0, v0

    .line 2331032
    sget-object v2, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    .line 2331033
    iput-object v2, v0, LX/GGI;->b:LX/8wL;

    .line 2331034
    move-object v0, v0

    .line 2331035
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/GGB;

    move-result-object v2

    .line 2331036
    iput-object v2, v0, LX/GGI;->e:LX/GGB;

    .line 2331037
    move-object v0, v0

    .line 2331038
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2331039
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->v()Ljava/lang/String;

    move-result-object v2

    .line 2331040
    iput-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->o:Ljava/lang/String;

    .line 2331041
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2331042
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2331043
    iput-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    .line 2331044
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2331045
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->w()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2331046
    :goto_0
    move-object v2, v2

    .line 2331047
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h(Ljava/lang/String;)V

    .line 2331048
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v4, p0, LX/GE8;->a:LX/0W9;

    invoke-static {p1, v2, v3, v4}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;DLX/0W9;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v2

    .line 2331049
    iput-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2331050
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2331051
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->m()LX/2uF;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->m()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    move v2, v3

    :goto_1
    if-eqz v2, :cond_6

    .line 2331052
    const/4 v2, 0x0

    .line 2331053
    :goto_2
    move-object v2, v2

    .line 2331054
    iput-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    .line 2331055
    invoke-static {v1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;)LX/0Px;

    move-result-object v1

    .line 2331056
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    .line 2331057
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->w()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 2331058
    :goto_3
    iput-boolean v1, v0, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->b:Z

    .line 2331059
    return-object v0

    .line 2331060
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    const-string v2, ""

    goto :goto_0

    .line 2331061
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->m()LX/2uF;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object p0, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2331062
    invoke-virtual {p0, v2, v4}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_5

    move v2, v3

    goto :goto_1

    :cond_5
    move v2, v4

    goto :goto_1

    .line 2331063
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->m()LX/2uF;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object p0, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {p0, v2, v4}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {p0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2330980
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v0, p0, LX/GE8;->a:LX/0W9;

    invoke-static {p1, v4, v5, v0}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;DLX/0W9;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    .line 2330981
    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2330982
    :goto_0
    new-instance v3, LX/GGK;

    invoke-direct {v3}, LX/GGK;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->p()Ljava/lang/String;

    move-result-object v4

    .line 2330983
    iput-object v4, v3, LX/GGK;->c:Ljava/lang/String;

    .line 2330984
    move-object v3, v3

    .line 2330985
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->r()Ljava/lang/String;

    move-result-object v4

    .line 2330986
    iput-object v4, v3, LX/GGK;->d:Ljava/lang/String;

    .line 2330987
    move-object v3, v3

    .line 2330988
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->v()Ljava/lang/String;

    move-result-object v4

    .line 2330989
    iput-object v4, v3, LX/GGK;->f:Ljava/lang/String;

    .line 2330990
    move-object v3, v3

    .line 2330991
    iput-object v0, v3, LX/GGK;->a:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2330992
    move-object v0, v3

    .line 2330993
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->p()Ljava/lang/String;

    move-result-object v3

    .line 2330994
    iput-object v3, v0, LX/GGK;->b:Ljava/lang/String;

    .line 2330995
    move-object v3, v0

    .line 2330996
    invoke-virtual {p0, p1}, LX/GE8;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    .line 2330997
    if-nez v0, :cond_1

    .line 2330998
    iget-object v0, p0, LX/GE8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Default Spec Model should not be null."

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2330999
    invoke-static {p1, v3}, LX/GE8;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;LX/GGK;)Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    .line 2331000
    :goto_1
    return-object v0

    .line 2331001
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 2331002
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2331003
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2331004
    if-nez v5, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 2331005
    iget-object v0, p0, LX/GE8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Default Creative Spec Model or it\'s contents should not be null."

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2331006
    invoke-static {p1, v3}, LX/GE8;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;LX/GGK;)Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    goto :goto_1

    .line 2331007
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2331008
    :cond_2
    invoke-virtual {v4, v5, v7}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    invoke-virtual {v4, v5, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_3

    .line 2331009
    :cond_6
    invoke-virtual {v4, v5, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2331010
    invoke-virtual {v4, v5, v7}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v4, v5, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2331011
    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2331012
    iput-object v0, v3, LX/GGK;->e:Ljava/lang/String;

    .line 2331013
    move-object v0, v3

    .line 2331014
    const/4 v1, 0x2

    invoke-virtual {v4, v2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2331015
    iput-object v1, v0, LX/GGK;->h:Ljava/lang/String;

    .line 2331016
    move-object v0, v0

    .line 2331017
    invoke-virtual {v0}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2330979
    const-string v0, "task_key_fetch_local_awareness_data"

    return-object v0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2330976
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2330977
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2330978
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2330975
    const-string v0, "boosted_local_awareness_mobile"

    return-object v0
.end method
