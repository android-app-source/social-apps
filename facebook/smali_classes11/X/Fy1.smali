.class public final LX/Fy1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/Fy4;


# direct methods
.method public constructor <init>(LX/Fy4;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2306209
    iput-object p1, p0, LX/Fy1;->b:LX/Fy4;

    iput-object p2, p0, LX/Fy1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2306210
    iget-object v0, p0, LX/Fy1;->b:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->e:LX/BQ1;

    iget-object v1, p0, LX/Fy1;->b:LX/Fy4;

    iget-object v1, v1, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v0, v1}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2306211
    iget-object v0, p0, LX/Fy1;->b:LX/Fy4;

    invoke-static {v0}, LX/Fy4;->c(LX/Fy4;)V

    .line 2306212
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 2306213
    iget-object v0, p0, LX/Fy1;->b:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->lk_()V

    .line 2306214
    :cond_0
    invoke-static {p1}, LX/Fy4;->b(Ljava/lang/Throwable;)Z

    move-result v1

    move v0, v1

    .line 2306215
    if-eqz v0, :cond_1

    .line 2306216
    iget-object v0, p0, LX/Fy1;->b:LX/Fy4;

    invoke-virtual {v0, p1}, LX/Fy4;->a(Ljava/lang/Throwable;)V

    .line 2306217
    :goto_0
    iget-object v0, p0, LX/Fy1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2306218
    return-void

    .line 2306219
    :cond_1
    iget-object v0, p0, LX/Fy1;->b:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081588

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2306220
    check-cast p1, Ljava/lang/Void;

    .line 2306221
    iget-object v0, p0, LX/Fy1;->b:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->lk_()V

    .line 2306222
    iget-object v0, p0, LX/Fy1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x2ae8ed6

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2306223
    return-void
.end method
