.class public final LX/H90;
.super LX/H8F;
.source ""


# instance fields
.field public final synthetic a:LX/H94;


# direct methods
.method public constructor <init>(LX/H94;)V
    .locals 0

    .prologue
    .line 2433857
    iput-object p1, p0, LX/H90;->a:LX/H94;

    invoke-direct {p0}, LX/H8F;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2433858
    check-cast p1, LX/HDZ;

    .line 2433859
    iget-object v0, p0, LX/H90;->a:LX/H94;

    .line 2433860
    iget-boolean v1, p1, LX/HDZ;->a:Z

    move v1, v1

    .line 2433861
    if-eqz v1, :cond_0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object p1, p0

    .line 2433862
    :goto_0
    if-eqz v1, :cond_1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2433863
    :goto_1
    invoke-static {v0, p1, p0, v1}, LX/H94;->a$redex0(LX/H94;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    .line 2433864
    return-void

    .line 2433865
    :cond_0
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object p1, p0

    goto :goto_0

    .line 2433866
    :cond_1
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNFOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    goto :goto_1
.end method
