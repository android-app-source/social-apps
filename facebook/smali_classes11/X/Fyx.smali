.class public LX/Fyx;
.super LX/4oC;
.source ""


# instance fields
.field public a:LX/03V;

.field public b:LX/FzF;

.field public c:LX/Fys;

.field public d:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

.field public e:LX/Fyr;

.field public f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

.field public g:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

.field public h:Ljava/lang/String;

.field public i:Landroid/view/View$OnClickListener;

.field public final j:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

.field public final k:Lcom/facebook/resources/ui/FbButton;

.field public final l:Lcom/facebook/resources/ui/FbButton;

.field public final m:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

.field public final n:Landroid/view/View;

.field public final o:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2307268
    invoke-direct {p0, p1}, LX/4oC;-><init>(Landroid/content/Context;)V

    .line 2307269
    const-class v0, LX/Fyx;

    invoke-static {v0, p0}, LX/Fyx;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2307270
    const v0, 0x7f030fe1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2307271
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Fyx;->setOrientation(I)V

    .line 2307272
    const v0, 0x7f0d2649

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iput-object v0, p0, LX/Fyx;->j:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    .line 2307273
    const v0, 0x7f0d264f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Fyx;->k:Lcom/facebook/resources/ui/FbButton;

    .line 2307274
    const v0, 0x7f0d2650

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Fyx;->l:Lcom/facebook/resources/ui/FbButton;

    .line 2307275
    const v0, 0x7f0d264b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    iput-object v0, p0, LX/Fyx;->m:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    .line 2307276
    const v0, 0x7f0d264e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Fyx;->n:Landroid/view/View;

    .line 2307277
    const v0, 0x7f0d264d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    iput-object v0, p0, LX/Fyx;->o:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    .line 2307278
    iget-object v0, p0, LX/Fyx;->j:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 2307279
    iget-object v0, p0, LX/Fyx;->j:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307280
    iget-object v0, p0, LX/Fyx;->l:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Fyt;

    invoke-direct {v1, p0}, LX/Fyt;-><init>(LX/Fyx;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307281
    new-instance v0, LX/Fyu;

    invoke-direct {v0, p0}, LX/Fyu;-><init>(LX/Fyx;)V

    iput-object v0, p0, LX/Fyx;->i:Landroid/view/View$OnClickListener;

    .line 2307282
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/Fyx;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v3}, LX/FzF;->a(LX/0QB;)LX/FzF;

    move-result-object v2

    check-cast v2, LX/FzF;

    const-class p0, LX/Fys;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Fys;

    iput-object v1, p1, LX/Fyx;->a:LX/03V;

    iput-object v2, p1, LX/Fyx;->b:LX/FzF;

    iput-object v3, p1, LX/Fyx;->c:LX/Fys;

    return-void
.end method

.method public static a$redex0(LX/Fyx;Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 2307283
    iget-object v0, p0, LX/Fyx;->e:LX/Fyr;

    iget-object v1, p0, LX/Fyx;->g:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    iget-object v2, p0, LX/Fyx;->h:Ljava/lang/String;

    iget-object v3, p0, LX/Fyx;->o:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    invoke-virtual {v3}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->getSelectedPrivacyRow()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    const-string v5, "android_plutonium_expando"

    const-string v6, "native_plutonium_header"

    move-object v4, p1

    .line 2307284
    iget-object v7, v0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    const/4 v8, 0x1

    .line 2307285
    iput-boolean v8, v7, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->g:Z

    .line 2307286
    invoke-static {v0}, LX/Fyr;->d(LX/Fyr;)V

    .line 2307287
    iget-object v7, v0, LX/Fyr;->c:LX/Fyq;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->d()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307288
    iget-object v10, v9, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a:Ljava/lang/String;

    move-object v11, v10

    .line 2307289
    move-object v9, v2

    move-object v10, v3

    move-object v12, v4

    move-object v13, v5

    move-object p0, v6

    .line 2307290
    const-string p1, "SAVE"

    invoke-virtual {v12, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move-object p1, v7

    move-object v1, v8

    move-object v2, v9

    move-object v3, v10

    move-object v4, v11

    move-object v5, v13

    move-object v6, p0

    .line 2307291
    new-instance v7, LX/4IX;

    invoke-direct {v7}, LX/4IX;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4IX;->a(Ljava/util/List;)LX/4IX;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->c()LX/0Px;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4IX;->b(Ljava/util/List;)LX/4IX;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v8

    .line 2307292
    sget-object v9, LX/Fyp;->a:[I

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 2307293
    const/4 v9, 0x0

    :goto_0
    move-object v8, v9

    .line 2307294
    invoke-virtual {v7, v8}, LX/4IX;->a(Ljava/lang/String;)LX/4IX;

    move-result-object v7

    .line 2307295
    new-instance v8, LX/4Ij;

    invoke-direct {v8}, LX/4Ij;-><init>()V

    .line 2307296
    const-string v9, "session"

    invoke-virtual {v8, v9, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307297
    move-object v8, v8

    .line 2307298
    const-string v9, "privacy"

    invoke-virtual {v8, v9, v7}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2307299
    move-object v7, v8

    .line 2307300
    const-string v8, "profile_question_id"

    invoke-virtual {v7, v8, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307301
    move-object v7, v7

    .line 2307302
    const-string v8, "profile_question_option_id"

    invoke-virtual {v7, v8, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307303
    move-object v8, v7

    .line 2307304
    new-instance v7, LX/5zE;

    invoke-direct {v7}, LX/5zE;-><init>()V

    move-object v7, v7

    .line 2307305
    const-string v9, "input"

    invoke-virtual {v7, v9, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    const-string v9, "ref"

    invoke-virtual {v7, v9, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v9, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v7

    const-string v9, "session"

    invoke-virtual {v7, v9, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v9, "surface"

    invoke-virtual {v7, v9, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/5zE;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2307306
    iget-object v9, p1, LX/Fyq;->a:LX/0tX;

    invoke-virtual {v9, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2307307
    iget-object v9, p1, LX/Fyq;->b:LX/0Sh;

    new-instance v10, LX/Fyn;

    invoke-direct {v10, p1, v8}, LX/Fyn;-><init>(LX/Fyq;LX/4Ij;)V

    invoke-virtual {v9, v7, v10}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2307308
    :goto_1
    iget-object v7, v0, LX/Fyr;->c:LX/Fyq;

    .line 2307309
    iput-object v0, v7, LX/Fyq;->d:LX/Fyr;

    .line 2307310
    return-void

    .line 2307311
    :cond_0
    new-instance p1, LX/4Ik;

    invoke-direct {p1}, LX/4Ik;-><init>()V

    .line 2307312
    const-string v1, "session"

    invoke-virtual {p1, v1, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307313
    move-object p1, p1

    .line 2307314
    const-string v1, "profile_question_id"

    invoke-virtual {p1, v1, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307315
    move-object v1, p1

    .line 2307316
    new-instance p1, LX/5zF;

    invoke-direct {p1}, LX/5zF;-><init>()V

    move-object p1, p1

    .line 2307317
    const-string v2, "input"

    invoke-virtual {p1, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object p1

    const-string v2, "ref"

    invoke-virtual {p1, v2, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string v2, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object p1

    const-string v2, "session"

    invoke-virtual {p1, v2, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string v2, "surface"

    invoke-virtual {p1, v2, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    check-cast p1, LX/5zF;

    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p1

    .line 2307318
    iget-object v2, v7, LX/Fyq;->a:LX/0tX;

    invoke-virtual {v2, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    .line 2307319
    iget-object v2, v7, LX/Fyq;->b:LX/0Sh;

    new-instance v3, LX/Fyo;

    invoke-direct {v3, v7, v1}, LX/Fyo;-><init>(LX/Fyq;LX/4Ik;)V

    invoke-virtual {v2, p1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2307320
    goto :goto_1

    .line 2307321
    :pswitch_0
    const-string v9, "EVERYONE"

    goto/16 :goto_0

    .line 2307322
    :pswitch_1
    const-string v9, "FRIENDS"

    goto/16 :goto_0

    .line 2307323
    :pswitch_2
    const-string v9, "FRIENDS_OF_FRIENDS"

    goto/16 :goto_0

    .line 2307324
    :pswitch_3
    const-string v9, "SELF"

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
