.class public LX/FEi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static a:LX/0ih;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/0if;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2216600
    sget-object v0, LX/0ig;->ax:LX/0ih;

    sput-object v0, LX/FEi;->a:LX/0ih;

    return-void
.end method

.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216602
    iput-object p1, p0, LX/FEi;->b:LX/0if;

    .line 2216603
    return-void
.end method

.method public static a(LX/0QB;)LX/FEi;
    .locals 4

    .prologue
    .line 2216604
    const-class v1, LX/FEi;

    monitor-enter v1

    .line 2216605
    :try_start_0
    sget-object v0, LX/FEi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2216606
    sput-object v2, LX/FEi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2216607
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2216608
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2216609
    new-instance p0, LX/FEi;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/FEi;-><init>(LX/0if;)V

    .line 2216610
    move-object v0, p0

    .line 2216611
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2216612
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FEi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2216613
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2216614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2216615
    sget-object v0, LX/FEj;->GAME_LIST_DISMISS:LX/FEj;

    invoke-virtual {p0, v0}, LX/FEi;->a(LX/FEj;)V

    .line 2216616
    iget-object v0, p0, LX/FEi;->b:LX/0if;

    sget-object v1, LX/FEi;->a:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2216617
    return-void
.end method

.method public final a(LX/FEj;)V
    .locals 3

    .prologue
    .line 2216618
    sget-object v0, LX/FEj;->GAME_SELECT:LX/FEj;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2216619
    iget-object v0, p0, LX/FEi;->b:LX/0if;

    sget-object v1, LX/FEi;->a:LX/0ih;

    iget-object v2, p1, LX/FEj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2216620
    return-void

    .line 2216621
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
