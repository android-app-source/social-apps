.class public LX/FUm;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Sh;

.field private final c:LX/0Zr;

.field public final d:LX/FUf;

.field public final e:LX/FUW;

.field private final f:Ljava/lang/Object;

.field public final g:LX/G9N;

.field private h:Landroid/os/HandlerThread;

.field public i:Landroid/os/Handler;

.field private j:Z

.field public k:LX/FUL;

.field private final l:Landroid/os/Handler$Callback;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2249611
    const-class v0, LX/FUm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FUm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0Zr;LX/FUf;LX/FUW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2249612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2249613
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/FUm;->f:Ljava/lang/Object;

    .line 2249614
    new-instance v0, LX/G9N;

    invoke-direct {v0}, LX/G9N;-><init>()V

    iput-object v0, p0, LX/FUm;->g:LX/G9N;

    .line 2249615
    new-instance v0, LX/FUl;

    invoke-direct {v0, p0}, LX/FUl;-><init>(LX/FUm;)V

    iput-object v0, p0, LX/FUm;->l:Landroid/os/Handler$Callback;

    .line 2249616
    iput-object p1, p0, LX/FUm;->b:LX/0Sh;

    .line 2249617
    iput-object p2, p0, LX/FUm;->c:LX/0Zr;

    .line 2249618
    iput-object p3, p0, LX/FUm;->d:LX/FUf;

    .line 2249619
    iput-object p4, p0, LX/FUm;->e:LX/FUW;

    .line 2249620
    return-void
.end method

.method public static a(LX/0QB;)LX/FUm;
    .locals 5

    .prologue
    .line 2249621
    new-instance v4, LX/FUm;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v1

    check-cast v1, LX/0Zr;

    invoke-static {p0}, LX/FUf;->b(LX/0QB;)LX/FUf;

    move-result-object v2

    check-cast v2, LX/FUf;

    invoke-static {p0}, LX/FUW;->b(LX/0QB;)LX/FUW;

    move-result-object v3

    check-cast v3, LX/FUW;

    invoke-direct {v4, v0, v1, v2, v3}, LX/FUm;-><init>(LX/0Sh;LX/0Zr;LX/FUf;LX/FUW;)V

    .line 2249622
    move-object v0, v4

    .line 2249623
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2249624
    iget-object v0, p0, LX/FUm;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2249625
    iget-object v1, p0, LX/FUm;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 2249626
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/FUm;->j:Z

    .line 2249627
    iget-object v0, p0, LX/FUm;->i:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2249628
    iget-object v0, p0, LX/FUm;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 2249629
    const/4 v0, 0x0

    iput-object v0, p0, LX/FUm;->k:LX/FUL;

    .line 2249630
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/FUL;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2249631
    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2249632
    iget-object v0, p0, LX/FUm;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2249633
    iget-object v1, p0, LX/FUm;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 2249634
    :try_start_0
    iput-object p1, p0, LX/FUm;->k:LX/FUL;

    .line 2249635
    iget-object v0, p0, LX/FUm;->c:LX/0Zr;

    sget-object v2, LX/FUm;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, LX/FUm;->h:Landroid/os/HandlerThread;

    .line 2249636
    iget-object v0, p0, LX/FUm;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 2249637
    new-instance v0, Landroid/os/Handler;

    iget-object v2, p0, LX/FUm;->h:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, LX/FUm;->l:Landroid/os/Handler$Callback;

    invoke-direct {v0, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/FUm;->i:Landroid/os/Handler;

    .line 2249638
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FUm;->j:Z

    .line 2249639
    monitor-exit v1

    return-void

    .line 2249640
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2249641
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a([BII)V
    .locals 3

    .prologue
    .line 2249642
    iget-object v1, p0, LX/FUm;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 2249643
    :try_start_0
    iget-boolean v0, p0, LX/FUm;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FUm;->i:Landroid/os/Handler;

    const v2, 0x186a1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2249644
    iget-object v0, p0, LX/FUm;->i:Landroid/os/Handler;

    const v2, 0x186a1

    invoke-virtual {v0, v2, p2, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2249645
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
