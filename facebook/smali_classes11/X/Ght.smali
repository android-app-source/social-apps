.class public LX/Ght;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ghr;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Ght;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2384943
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ght;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384944
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2384945
    iput-object p1, p0, LX/Ght;->b:LX/0Ot;

    .line 2384946
    return-void
.end method

.method public static a(LX/0QB;)LX/Ght;
    .locals 4

    .prologue
    .line 2384947
    sget-object v0, LX/Ght;->c:LX/Ght;

    if-nez v0, :cond_1

    .line 2384948
    const-class v1, LX/Ght;

    monitor-enter v1

    .line 2384949
    :try_start_0
    sget-object v0, LX/Ght;->c:LX/Ght;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2384950
    if-eqz v2, :cond_0

    .line 2384951
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2384952
    new-instance v3, LX/Ght;

    const/16 p0, 0x230d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ght;-><init>(LX/0Ot;)V

    .line 2384953
    move-object v0, v3

    .line 2384954
    sput-object v0, LX/Ght;->c:LX/Ght;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384955
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2384956
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2384957
    :cond_1
    sget-object v0, LX/Ght;->c:LX/Ght;

    return-object v0

    .line 2384958
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2384959
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2384960
    check-cast p2, LX/Ghs;

    .line 2384961
    iget-object v0, p0, LX/Ght;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;

    iget-object v1, p2, LX/Ghs;->a:LX/Ghu;

    const/4 v3, 0x2

    .line 2384962
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v2, v0, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    iget-object p2, v1, LX/Ghu;->a:Ljava/lang/String;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {v2, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    sget-object p2, Lcom/facebook/gametime/ui/components/partdefinition/match/GametimeMatchTeamComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const p0, 0x7f0b22db

    invoke-interface {v2, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const p0, 0x7f0b22db

    invoke-interface {v2, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    iget-object p0, v1, LX/Ghu;->b:Ljava/lang/String;

    invoke-virtual {v3, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0050

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    iget p0, v1, LX/Ghu;->d:I

    invoke-virtual {v3, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 p0, 0x6

    const p2, 0x7f0b0060

    invoke-interface {v3, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v3, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    iget-object p0, v1, LX/Ghu;->c:Ljava/lang/String;

    invoke-virtual {v3, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0050

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    iget p0, v1, LX/Ghu;->d:I

    invoke-virtual {v3, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2384963
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2384964
    invoke-static {}, LX/1dS;->b()V

    .line 2384965
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/Ghr;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2384966
    new-instance v1, LX/Ghs;

    invoke-direct {v1, p0}, LX/Ghs;-><init>(LX/Ght;)V

    .line 2384967
    sget-object v2, LX/Ght;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ghr;

    .line 2384968
    if-nez v2, :cond_0

    .line 2384969
    new-instance v2, LX/Ghr;

    invoke-direct {v2}, LX/Ghr;-><init>()V

    .line 2384970
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Ghr;->a$redex0(LX/Ghr;LX/1De;IILX/Ghs;)V

    .line 2384971
    move-object v1, v2

    .line 2384972
    move-object v0, v1

    .line 2384973
    return-object v0
.end method
