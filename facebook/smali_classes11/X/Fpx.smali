.class public final LX/Fpx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;)V
    .locals 0

    .prologue
    .line 2291985
    iput-object p1, p0, LX/Fpx;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2291992
    iget-object v0, p0, LX/Fpx;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "timeline_see_more_fail"

    const-string v2, "Failed to fetch aggregated substories"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2291993
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2291986
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2291987
    iget-object v0, p0, LX/Fpx;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->o()LX/G4x;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2291988
    iget-object v0, p0, LX/Fpx;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    if-eqz v0, :cond_0

    .line 2291989
    iget-object v0, p0, LX/Fpx;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->e:LX/G4v;

    invoke-virtual {v0, p1}, LX/G4v;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2291990
    :cond_0
    iget-object v0, p0, LX/Fpx;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2291991
    return-void
.end method
