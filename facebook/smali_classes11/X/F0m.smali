.class public final LX/F0m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/FeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

.field public final synthetic c:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

.field public final synthetic d:LX/1Po;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2190552
    iput-object p1, p0, LX/F0m;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iput-object p2, p0, LX/F0m;->a:Lcom/facebook/graphql/model/FeedUnit;

    iput-object p3, p0, LX/F0m;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object p4, p0, LX/F0m;->c:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iput-object p5, p0, LX/F0m;->d:LX/1Po;

    iput-object p6, p0, LX/F0m;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x36d50398    # -700358.5f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2190553
    iget-object v0, p0, LX/F0m;->a:Lcom/facebook/graphql/model/FeedUnit;

    .line 2190554
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v2, :cond_1

    .line 2190555
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 2190556
    const-string v2, "friendversary_collage"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2190557
    const-string v2, "friendversary_card_collage_ipb"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2190558
    :goto_0
    move-object v2, v2

    .line 2190559
    iget-object v0, p0, LX/F0m;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->i:LX/1Cn;

    iget-object v3, p0, LX/F0m;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/CEz;->PERMALINK:LX/CEz;

    invoke-virtual {v0, v3, v2, v4}, LX/1Cn;->b(Ljava/lang/String;Ljava/lang/String;LX/CEz;)V

    move-object v0, p1

    .line 2190560
    check-cast v0, LX/3ZK;

    .line 2190561
    iget-object v3, v0, LX/3ZK;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-object v0, v3

    .line 2190562
    new-instance v3, LX/6WS;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2190563
    const v4, 0x7f0314b7

    invoke-virtual {v3, v4}, LX/5OM;->b(I)V

    .line 2190564
    new-instance v4, LX/F0l;

    invoke-direct {v4, p0, p1, v2}, LX/F0l;-><init>(LX/F0m;Landroid/view/View;Ljava/lang/String;)V

    .line 2190565
    iput-object v4, v3, LX/5OM;->p:LX/5OO;

    .line 2190566
    invoke-virtual {v3, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2190567
    const v0, 0x309f9e75

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2190568
    :cond_0
    const-string v2, "friendversary_profile_pictures"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2190569
    const-string v2, "friendversary_polaroids_ipb"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2190570
    :cond_1
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz v2, :cond_3

    .line 2190571
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2190572
    sget-object v2, LX/F0r;->COLLAGE_V1:LX/F0r;

    invoke-virtual {v2}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2190573
    const-string v2, "friendversary_card_collage_ipb"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2190574
    :cond_2
    sget-object v2, LX/F0r;->POLAROID_V1:LX/F0r;

    invoke-virtual {v2}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2190575
    const-string v2, "friendversary_polaroids_ipb"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2190576
    :cond_3
    const-string v2, ""

    goto :goto_0
.end method
