.class public final LX/HCJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/1a1;

.field public final synthetic b:LX/HCO;


# direct methods
.method public constructor <init>(LX/HCO;LX/1a1;)V
    .locals 0

    .prologue
    .line 2438539
    iput-object p1, p0, LX/HCJ;->b:LX/HCO;

    iput-object p2, p0, LX/HCJ;->a:LX/1a1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2438540
    invoke-static {p2}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2438541
    iget-object v0, p0, LX/HCJ;->b:LX/HCO;

    iget-object v0, v0, LX/HCO;->j:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    iget-object v1, p0, LX/HCJ;->a:LX/1a1;

    .line 2438542
    iget-object p0, v0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->k:LX/3xm;

    .line 2438543
    iget-object p1, p0, LX/3xm;->j:LX/3xj;

    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-static {p1, v0, v1}, LX/3xj;->c(LX/3xj;Landroid/support/v7/widget/RecyclerView;LX/1a1;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 2438544
    const-string p1, "ItemTouchHelper"

    const-string v0, "Start drag has been called but swiping is not enabled"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2438545
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2438546
    :cond_1
    iget-object p1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    if-eq p1, v0, :cond_2

    .line 2438547
    const-string p1, "ItemTouchHelper"

    const-string v0, "Start drag has been called with a view holder which is not a child of the RecyclerView which is controlled by this ItemTouchHelper."

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2438548
    :cond_2
    invoke-static {p0}, LX/3xm;->f(LX/3xm;)V

    .line 2438549
    const/4 p1, 0x0

    iput p1, p0, LX/3xm;->f:F

    iput p1, p0, LX/3xm;->e:F

    .line 2438550
    const/4 p1, 0x2

    invoke-static {p0, v1, p1}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    goto :goto_0
.end method
