.class public LX/GSz;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:Landroid/support/v4/view/ViewPager;

.field public b:Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;

.field public c:Landroid/widget/ImageView;

.field private d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2355091
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2355092
    const v0, 0x7f0300f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2355093
    const v0, 0x7f0d056d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    .line 2355094
    iget-object v0, p0, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    const-wide/high16 v2, -0x4008000000000000L    # -1.5

    invoke-virtual {p0}, LX/GSz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0060

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2355095
    iget-object v0, p0, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2355096
    const v0, 0x7f0d056e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;

    iput-object v0, p0, LX/GSz;->b:Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;

    .line 2355097
    const v0, 0x7f0d056c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GSz;->c:Landroid/widget/ImageView;

    .line 2355098
    const v0, 0x7f0d056b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/GSz;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2355099
    return-void
.end method


# virtual methods
.method public setSocialContext(Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;)V
    .locals 9

    .prologue
    .line 2355051
    iget-object v0, p0, LX/GSz;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p1}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2355052
    if-nez p1, :cond_0

    .line 2355053
    const/4 v2, 0x0

    .line 2355054
    :goto_0
    move-object v2, v2

    .line 2355055
    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2355056
    return-void

    .line 2355057
    :cond_0
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    .line 2355058
    invoke-virtual {p1}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2355059
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2355060
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {p1}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 2355061
    invoke-virtual {p1}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel;

    .line 2355062
    if-nez v2, :cond_3

    .line 2355063
    const/4 v6, 0x0

    .line 2355064
    :goto_2
    move-object v2, v6

    .line 2355065
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2355066
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2355067
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2355068
    iput-object v2, v4, LX/173;->e:LX/0Px;

    .line 2355069
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2355070
    iput-object v2, v4, LX/173;->f:Ljava/lang/String;

    .line 2355071
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto :goto_0

    .line 2355072
    :cond_3
    new-instance v6, LX/4W6;

    invoke-direct {v6}, LX/4W6;-><init>()V

    .line 2355073
    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel;->a()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v7

    .line 2355074
    if-nez v7, :cond_4

    .line 2355075
    const/4 v8, 0x0

    .line 2355076
    :goto_3
    move-object v7, v8

    .line 2355077
    iput-object v7, v6, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2355078
    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel;->b()I

    move-result v7

    .line 2355079
    iput v7, v6, LX/4W6;->c:I

    .line 2355080
    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel;->c()I

    move-result v7

    .line 2355081
    iput v7, v6, LX/4W6;->d:I

    .line 2355082
    invoke-virtual {v6}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v6

    goto :goto_2

    .line 2355083
    :cond_4
    new-instance v8, LX/170;

    invoke-direct {v8}, LX/170;-><init>()V

    .line 2355084
    invoke-virtual {v7}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    .line 2355085
    iput-object p0, v8, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2355086
    invoke-virtual {v7}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->c()Ljava/lang/String;

    move-result-object p0

    .line 2355087
    iput-object p0, v8, LX/170;->o:Ljava/lang/String;

    .line 2355088
    invoke-virtual {v7}, Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel$RangesModel$EntityModel;->d()Ljava/lang/String;

    move-result-object p0

    .line 2355089
    iput-object p0, v8, LX/170;->A:Ljava/lang/String;

    .line 2355090
    invoke-virtual {v8}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v8

    goto :goto_3
.end method
