.class public LX/F4p;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/F4p;


# instance fields
.field private final a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 4
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2197670
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2197671
    iput-object p1, p0, LX/F4p;->a:Ljava/lang/Boolean;

    .line 2197672
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2197673
    sget-object v1, LX/0ax;->N:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->GROUP_NATIVE_CREATE_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2197674
    return-void
.end method

.method public static a(LX/0QB;)LX/F4p;
    .locals 4

    .prologue
    .line 2197642
    sget-object v0, LX/F4p;->b:LX/F4p;

    if-nez v0, :cond_1

    .line 2197643
    const-class v1, LX/F4p;

    monitor-enter v1

    .line 2197644
    :try_start_0
    sget-object v0, LX/F4p;->b:LX/F4p;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2197645
    if-eqz v2, :cond_0

    .line 2197646
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2197647
    new-instance p0, LX/F4p;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {p0, v3}, LX/F4p;-><init>(Ljava/lang/Boolean;)V

    .line 2197648
    move-object v0, p0

    .line 2197649
    sput-object v0, LX/F4p;->b:LX/F4p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2197650
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2197651
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2197652
    :cond_1
    sget-object v0, LX/F4p;->b:LX/F4p;

    return-object v0

    .line 2197653
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2197654
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2197656
    invoke-super {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2197657
    if-nez v0, :cond_1

    .line 2197658
    const/4 v0, 0x0

    .line 2197659
    :cond_0
    :goto_0
    return-object v0

    .line 2197660
    :cond_1
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2197661
    invoke-virtual {v1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2197662
    const/4 v2, 0x0

    .line 2197663
    :goto_1
    move-object v1, v2

    .line 2197664
    if-eqz v1, :cond_0

    .line 2197665
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0

    .line 2197666
    :cond_2
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2197667
    invoke-virtual {v1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2197668
    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move-object v2, p0

    .line 2197669
    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2197655
    iget-object v0, p0, LX/F4p;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
