.class public LX/Fm2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2281935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2281936
    return-void
.end method

.method public static b(LX/0QB;)LX/Fm2;
    .locals 3

    .prologue
    .line 2281937
    new-instance v2, LX/Fm2;

    invoke-direct {v2}, LX/Fm2;-><init>()V

    .line 2281938
    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    .line 2281939
    iput-object v0, v2, LX/Fm2;->a:LX/0tX;

    iput-object v1, v2, LX/Fm2;->b:Ljava/util/concurrent/ExecutorService;

    .line 2281940
    return-object v2
.end method

.method private c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2281941
    iget-object v0, p0, LX/Fm2;->a:LX/0tX;

    .line 2281942
    new-instance v1, LX/FnW;

    invoke-direct {v1}, LX/FnW;-><init>()V

    move-object v1, v1

    .line 2281943
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2281944
    new-instance v2, LX/FnW;

    invoke-direct {v2}, LX/FnW;-><init>()V

    const-string v3, "campaign_id"

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "profile_image_size"

    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "count"

    const/16 p0, 0x12c

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    .line 2281945
    iget-object v3, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v3

    .line 2281946
    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2281947
    move-object v1, v1

    .line 2281948
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2281949
    invoke-direct {p0, p1}, LX/Fm2;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2281950
    new-instance v1, LX/Fm1;

    invoke-direct {v1, p0}, LX/Fm1;-><init>(LX/Fm2;)V

    iget-object v2, p0, LX/Fm2;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserSendInvitesMutationFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2281951
    new-instance v0, LX/4FA;

    invoke-direct {v0}, LX/4FA;-><init>()V

    .line 2281952
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281953
    move-object v0, v0

    .line 2281954
    const-string v1, "invitee_ids"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2281955
    move-object v0, v0

    .line 2281956
    const-string v1, "source"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281957
    move-object v0, v0

    .line 2281958
    new-instance v1, LX/FnZ;

    invoke-direct {v1}, LX/FnZ;-><init>()V

    move-object v1, v1

    .line 2281959
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FnZ;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2281960
    iget-object v1, p0, LX/Fm2;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
