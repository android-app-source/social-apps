.class public final enum LX/GOf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GOf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GOf;

.field public static final enum CHARGE:LX/GOf;

.field public static final enum GET_CVV_TOKEN:LX/GOf;

.field public static final enum GET_DEFAULT_PAYMENT_METHOD:LX/GOf;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2347713
    new-instance v0, LX/GOf;

    const-string v1, "GET_DEFAULT_PAYMENT_METHOD"

    invoke-direct {v0, v1, v2}, LX/GOf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOf;->GET_DEFAULT_PAYMENT_METHOD:LX/GOf;

    .line 2347714
    new-instance v0, LX/GOf;

    const-string v1, "GET_CVV_TOKEN"

    invoke-direct {v0, v1, v3}, LX/GOf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOf;->GET_CVV_TOKEN:LX/GOf;

    .line 2347715
    new-instance v0, LX/GOf;

    const-string v1, "CHARGE"

    invoke-direct {v0, v1, v4}, LX/GOf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOf;->CHARGE:LX/GOf;

    .line 2347716
    const/4 v0, 0x3

    new-array v0, v0, [LX/GOf;

    sget-object v1, LX/GOf;->GET_DEFAULT_PAYMENT_METHOD:LX/GOf;

    aput-object v1, v0, v2

    sget-object v1, LX/GOf;->GET_CVV_TOKEN:LX/GOf;

    aput-object v1, v0, v3

    sget-object v1, LX/GOf;->CHARGE:LX/GOf;

    aput-object v1, v0, v4

    sput-object v0, LX/GOf;->$VALUES:[LX/GOf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2347712
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GOf;
    .locals 1

    .prologue
    .line 2347710
    const-class v0, LX/GOf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GOf;

    return-object v0
.end method

.method public static values()[LX/GOf;
    .locals 1

    .prologue
    .line 2347711
    sget-object v0, LX/GOf;->$VALUES:[LX/GOf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GOf;

    return-object v0
.end method
