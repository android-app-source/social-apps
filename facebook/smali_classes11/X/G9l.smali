.class public LX/G9l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G96;

.field public b:LX/G93;


# direct methods
.method public constructor <init>(LX/G96;)V
    .locals 0

    .prologue
    .line 2323331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2323332
    iput-object p1, p0, LX/G9l;->a:LX/G96;

    .line 2323333
    return-void
.end method

.method private a(IIII)F
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 2323334
    invoke-direct {p0, p1, p2, p3, p4}, LX/G9l;->b(IIII)F

    move-result v4

    .line 2323335
    sub-int v1, p3, p1

    sub-int v1, p1, v1

    .line 2323336
    if-gez v1, :cond_0

    .line 2323337
    int-to-float v3, p1

    sub-int v1, p1, v1

    int-to-float v1, v1

    div-float v1, v3, v1

    move v3, v0

    .line 2323338
    :goto_0
    int-to-float v5, p2

    sub-int v6, p4, p2

    int-to-float v6, v6

    mul-float/2addr v1, v6

    sub-float v1, v5, v1

    float-to-int v1, v1

    .line 2323339
    if-gez v1, :cond_1

    .line 2323340
    int-to-float v5, p2

    sub-int v1, p2, v1

    int-to-float v1, v1

    div-float v1, v5, v1

    .line 2323341
    :goto_1
    int-to-float v5, p1

    sub-int/2addr v3, p1

    int-to-float v3, v3

    mul-float/2addr v1, v3

    add-float/2addr v1, v5

    float-to-int v1, v1

    .line 2323342
    invoke-direct {p0, p1, p2, v1, v0}, LX/G9l;->b(IIII)F

    move-result v0

    add-float/2addr v0, v4

    .line 2323343
    sub-float/2addr v0, v2

    return v0

    .line 2323344
    :cond_0
    iget-object v3, p0, LX/G9l;->a:LX/G96;

    .line 2323345
    iget v5, v3, LX/G96;->a:I

    move v3, v5

    .line 2323346
    if-lt v1, v3, :cond_3

    .line 2323347
    iget-object v3, p0, LX/G9l;->a:LX/G96;

    .line 2323348
    iget v5, v3, LX/G96;->a:I

    move v3, v5

    .line 2323349
    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, p1

    int-to-float v3, v3

    sub-int/2addr v1, p1

    int-to-float v1, v1

    div-float/2addr v3, v1

    .line 2323350
    iget-object v1, p0, LX/G9l;->a:LX/G96;

    .line 2323351
    iget v5, v1, LX/G96;->a:I

    move v1, v5

    .line 2323352
    add-int/lit8 v1, v1, -0x1

    move v7, v1

    move v1, v3

    move v3, v7

    goto :goto_0

    .line 2323353
    :cond_1
    iget-object v0, p0, LX/G9l;->a:LX/G96;

    .line 2323354
    iget v5, v0, LX/G96;->b:I

    move v0, v5

    .line 2323355
    if-lt v1, v0, :cond_2

    .line 2323356
    iget-object v0, p0, LX/G9l;->a:LX/G96;

    .line 2323357
    iget v5, v0, LX/G96;->b:I

    move v0, v5

    .line 2323358
    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p2

    int-to-float v0, v0

    sub-int/2addr v1, p2

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 2323359
    iget-object v0, p0, LX/G9l;->a:LX/G96;

    .line 2323360
    iget v5, v0, LX/G96;->b:I

    move v0, v5

    .line 2323361
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1

    :cond_3
    move v3, v1

    move v1, v2

    goto :goto_0
.end method

.method public static a(LX/G9l;LX/G92;LX/G92;)F
    .locals 6

    .prologue
    const/high16 v5, 0x40e00000    # 7.0f

    .line 2323362
    iget v0, p1, LX/G92;->a:F

    move v0, v0

    .line 2323363
    float-to-int v0, v0

    .line 2323364
    iget v1, p1, LX/G92;->b:F

    move v1, v1

    .line 2323365
    float-to-int v1, v1

    .line 2323366
    iget v2, p2, LX/G92;->a:F

    move v2, v2

    .line 2323367
    float-to-int v2, v2

    .line 2323368
    iget v3, p2, LX/G92;->b:F

    move v3, v3

    .line 2323369
    float-to-int v3, v3

    .line 2323370
    invoke-direct {p0, v0, v1, v2, v3}, LX/G9l;->a(IIII)F

    move-result v0

    .line 2323371
    iget v1, p2, LX/G92;->a:F

    move v1, v1

    .line 2323372
    float-to-int v1, v1

    .line 2323373
    iget v2, p2, LX/G92;->b:F

    move v2, v2

    .line 2323374
    float-to-int v2, v2

    .line 2323375
    iget v3, p1, LX/G92;->a:F

    move v3, v3

    .line 2323376
    float-to-int v3, v3

    .line 2323377
    iget v4, p1, LX/G92;->b:F

    move v4, v4

    .line 2323378
    float-to-int v4, v4

    .line 2323379
    invoke-direct {p0, v1, v2, v3, v4}, LX/G9l;->a(IIII)F

    move-result v1

    .line 2323380
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2323381
    div-float v0, v1, v5

    .line 2323382
    :goto_0
    return v0

    .line 2323383
    :cond_0
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2323384
    div-float/2addr v0, v5

    goto :goto_0

    .line 2323385
    :cond_1
    add-float/2addr v0, v1

    const/high16 v1, 0x41600000    # 14.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private static a(LX/G92;LX/G92;LX/G92;F)I
    .locals 2

    .prologue
    .line 2323386
    invoke-static {p0, p1}, LX/G92;->a(LX/G92;LX/G92;)F

    move-result v0

    div-float/2addr v0, p3

    invoke-static {v0}, LX/G9H;->a(F)I

    move-result v0

    .line 2323387
    invoke-static {p0, p2}, LX/G92;->a(LX/G92;LX/G92;)F

    move-result v1

    div-float/2addr v1, p3

    invoke-static {v1}, LX/G9H;->a(F)I

    move-result v1

    .line 2323388
    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x7

    .line 2323389
    and-int/lit8 v1, v0, 0x3

    packed-switch v1, :pswitch_data_0

    .line 2323390
    :goto_0
    :pswitch_0
    return v0

    .line 2323391
    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    .line 2323392
    goto :goto_0

    .line 2323393
    :pswitch_2
    add-int/lit8 v0, v0, -0x1

    .line 2323394
    goto :goto_0

    .line 2323395
    :pswitch_3
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2323396
    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/G9l;LX/G9q;)LX/G9C;
    .locals 11

    .prologue
    .line 2323397
    iget-object v0, p1, LX/G9q;->b:LX/G9m;

    move-object v2, v0

    .line 2323398
    iget-object v0, p1, LX/G9q;->c:LX/G9m;

    move-object v3, v0

    .line 2323399
    iget-object v0, p1, LX/G9q;->a:LX/G9m;

    move-object v4, v0

    .line 2323400
    invoke-static {p0, v2, v3}, LX/G9l;->a(LX/G9l;LX/G92;LX/G92;)F

    move-result v0

    .line 2323401
    invoke-static {p0, v2, v4}, LX/G9l;->a(LX/G9l;LX/G92;LX/G92;)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    move v5, v0

    .line 2323402
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v5, v0

    if-gez v0, :cond_0

    .line 2323403
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2323404
    throw v0

    .line 2323405
    :cond_0
    invoke-static {v2, v3, v4, v5}, LX/G9l;->a(LX/G92;LX/G92;LX/G92;F)I

    move-result v6

    .line 2323406
    invoke-static {v6}, LX/G9i;->a(I)LX/G9i;

    move-result-object v1

    .line 2323407
    invoke-virtual {v1}, LX/G9i;->d()I

    move-result v0

    add-int/lit8 v7, v0, -0x7

    .line 2323408
    const/4 v0, 0x0

    .line 2323409
    iget-object v8, v1, LX/G9i;->d:[I

    move-object v1, v8

    .line 2323410
    array-length v1, v1

    if-lez v1, :cond_1

    .line 2323411
    iget v1, v3, LX/G92;->a:F

    move v1, v1

    .line 2323412
    iget v8, v2, LX/G92;->a:F

    move v8, v8

    .line 2323413
    sub-float/2addr v1, v8

    .line 2323414
    iget v8, v4, LX/G92;->a:F

    move v8, v8

    .line 2323415
    add-float/2addr v1, v8

    .line 2323416
    iget v8, v3, LX/G92;->b:F

    move v8, v8

    .line 2323417
    iget v9, v2, LX/G92;->b:F

    move v9, v9

    .line 2323418
    sub-float/2addr v8, v9

    .line 2323419
    iget v9, v4, LX/G92;->b:F

    move v9, v9

    .line 2323420
    add-float/2addr v8, v9

    .line 2323421
    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x40400000    # 3.0f

    int-to-float v7, v7

    div-float v7, v10, v7

    sub-float v7, v9, v7

    .line 2323422
    iget v9, v2, LX/G92;->a:F

    move v9, v9

    .line 2323423
    iget v10, v2, LX/G92;->a:F

    move v10, v10

    .line 2323424
    sub-float/2addr v1, v10

    mul-float/2addr v1, v7

    add-float/2addr v1, v9

    float-to-int v9, v1

    .line 2323425
    iget v1, v2, LX/G92;->b:F

    move v1, v1

    .line 2323426
    iget v10, v2, LX/G92;->b:F

    move v10, v10

    .line 2323427
    sub-float/2addr v8, v10

    mul-float/2addr v7, v8

    add-float/2addr v1, v7

    float-to-int v7, v1

    .line 2323428
    const/4 v1, 0x4

    :goto_0
    const/16 v8, 0x10

    if-gt v1, v8, :cond_1

    .line 2323429
    int-to-float v8, v1

    :try_start_0
    invoke-direct {p0, v5, v9, v7, v8}, LX/G9l;->a(FIIF)LX/G9j;
    :try_end_0
    .catch LX/G8x; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2323430
    :cond_1
    invoke-static {v2, v3, v4, v0, v6}, LX/G9l;->a(LX/G92;LX/G92;LX/G92;LX/G92;I)LX/G9F;

    move-result-object v1

    .line 2323431
    iget-object v5, p0, LX/G9l;->a:LX/G96;

    .line 2323432
    sget-object v7, LX/G9A;->a:LX/G9A;

    move-object v7, v7

    .line 2323433
    invoke-virtual {v7, v5, v6, v6, v1}, LX/G9A;->a(LX/G96;IILX/G9F;)LX/G96;

    move-result-object v7

    move-object v5, v7

    .line 2323434
    if-nez v0, :cond_2

    .line 2323435
    const/4 v0, 0x3

    new-array v0, v0, [LX/G92;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    .line 2323436
    :goto_1
    new-instance v1, LX/G9C;

    invoke-direct {v1, v5, v0}, LX/G9C;-><init>(LX/G96;[LX/G92;)V

    return-object v1

    .line 2323437
    :catch_0
    shl-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2323438
    :cond_2
    const/4 v1, 0x4

    new-array v1, v1, [LX/G92;

    const/4 v6, 0x0

    aput-object v4, v1, v6

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/4 v2, 0x2

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v0, v1, v2

    move-object v0, v1

    goto :goto_1
.end method

.method private static a(LX/G92;LX/G92;LX/G92;LX/G92;I)LX/G9F;
    .locals 17

    .prologue
    .line 2323439
    move/from16 v0, p4

    int-to-float v1, v0

    const/high16 v2, 0x40600000    # 3.5f

    sub-float v3, v1, v2

    .line 2323440
    if-eqz p3, :cond_0

    .line 2323441
    invoke-virtual/range {p3 .. p3}, LX/G92;->a()F

    move-result v13

    .line 2323442
    invoke-virtual/range {p3 .. p3}, LX/G92;->b()F

    move-result v14

    .line 2323443
    const/high16 v1, 0x40400000    # 3.0f

    sub-float v6, v3, v1

    move v5, v6

    .line 2323444
    :goto_0
    const/high16 v1, 0x40600000    # 3.5f

    const/high16 v2, 0x40600000    # 3.5f

    const/high16 v4, 0x40600000    # 3.5f

    const/high16 v7, 0x40600000    # 3.5f

    .line 2323445
    invoke-virtual/range {p0 .. p0}, LX/G92;->a()F

    move-result v9

    .line 2323446
    invoke-virtual/range {p0 .. p0}, LX/G92;->b()F

    move-result v10

    .line 2323447
    invoke-virtual/range {p1 .. p1}, LX/G92;->a()F

    move-result v11

    .line 2323448
    invoke-virtual/range {p1 .. p1}, LX/G92;->b()F

    move-result v12

    .line 2323449
    invoke-virtual/range {p2 .. p2}, LX/G92;->a()F

    move-result v15

    .line 2323450
    invoke-virtual/range {p2 .. p2}, LX/G92;->b()F

    move-result v16

    move v8, v3

    .line 2323451
    invoke-static/range {v1 .. v16}, LX/G9F;->a(FFFFFFFFFFFFFFFF)LX/G9F;

    move-result-object v1

    return-object v1

    .line 2323452
    :cond_0
    invoke-virtual/range {p1 .. p1}, LX/G92;->a()F

    move-result v1

    invoke-virtual/range {p0 .. p0}, LX/G92;->a()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual/range {p2 .. p2}, LX/G92;->a()F

    move-result v2

    add-float v13, v1, v2

    .line 2323453
    invoke-virtual/range {p1 .. p1}, LX/G92;->b()F

    move-result v1

    invoke-virtual/range {p0 .. p0}, LX/G92;->b()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual/range {p2 .. p2}, LX/G92;->b()F

    move-result v2

    add-float v14, v1, v2

    move v6, v3

    move v5, v3

    .line 2323454
    goto :goto_0
.end method

.method private a(FIIF)LX/G9j;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/high16 v6, 0x40400000    # 3.0f

    .line 2323455
    mul-float v0, p4, p1

    float-to-int v0, v0

    .line 2323456
    sub-int v1, p2, v0

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2323457
    iget-object v1, p0, LX/G9l;->a:LX/G96;

    .line 2323458
    iget v3, v1, LX/G96;->a:I

    move v1, v3

    .line 2323459
    add-int/lit8 v1, v1, -0x1

    add-int v3, p2, v0

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2323460
    sub-int v1, v4, v2

    int-to-float v1, v1

    mul-float v3, p1, v6

    cmpg-float v1, v1, v3

    if-gez v1, :cond_0

    .line 2323461
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2323462
    throw v0

    .line 2323463
    :cond_0
    sub-int v1, p3, v0

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2323464
    iget-object v1, p0, LX/G9l;->a:LX/G96;

    .line 2323465
    iget v5, v1, LX/G96;->b:I

    move v1, v5

    .line 2323466
    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, p3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 2323467
    sub-int v0, v5, v3

    int-to-float v0, v0

    mul-float v1, p1, v6

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 2323468
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2323469
    throw v0

    .line 2323470
    :cond_1
    new-instance v0, LX/G9k;

    iget-object v1, p0, LX/G9l;->a:LX/G96;

    sub-int/2addr v4, v2

    sub-int/2addr v5, v3

    iget-object v7, p0, LX/G9l;->b:LX/G93;

    move v6, p1

    invoke-direct/range {v0 .. v7}, LX/G9k;-><init>(LX/G96;IIIIFLX/G93;)V

    .line 2323471
    invoke-virtual {v0}, LX/G9k;->a()LX/G9j;

    move-result-object v0

    return-object v0
.end method

.method private b(IIII)F
    .locals 19

    .prologue
    .line 2323472
    sub-int v3, p4, p2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    sub-int v4, p3, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v3, v4, :cond_0

    const/4 v3, 0x1

    move v12, v3

    .line 2323473
    :goto_0
    if-eqz v12, :cond_c

    .line 2323474
    :goto_1
    sub-int v3, p4, p2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v13

    .line 2323475
    sub-int v3, p3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v14

    .line 2323476
    neg-int v3, v13

    div-int/lit8 v5, v3, 0x2

    .line 2323477
    move/from16 v0, p2

    move/from16 v1, p4

    if-ge v0, v1, :cond_1

    const/4 v3, 0x1

    move v11, v3

    .line 2323478
    :goto_2
    move/from16 v0, p1

    move/from16 v1, p3

    if-ge v0, v1, :cond_2

    const/4 v3, 0x1

    .line 2323479
    :goto_3
    const/4 v6, 0x0

    .line 2323480
    add-int v15, p4, v11

    move/from16 v8, p2

    move v10, v5

    move/from16 v5, p1

    .line 2323481
    :goto_4
    if-eq v8, v15, :cond_b

    .line 2323482
    if-eqz v12, :cond_3

    move v9, v5

    .line 2323483
    :goto_5
    if-eqz v12, :cond_4

    move v7, v8

    .line 2323484
    :goto_6
    const/4 v4, 0x1

    if-ne v6, v4, :cond_5

    const/4 v4, 0x1

    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, LX/G9l;->a:LX/G96;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9, v7}, LX/G96;->a(II)Z

    move-result v7

    if-ne v4, v7, :cond_a

    .line 2323485
    const/4 v4, 0x2

    if-ne v6, v4, :cond_6

    .line 2323486
    move/from16 v0, p2

    move/from16 v1, p1

    invoke-static {v8, v5, v0, v1}, LX/G9H;->a(IIII)F

    move-result v3

    .line 2323487
    :goto_8
    return v3

    .line 2323488
    :cond_0
    const/4 v3, 0x0

    move v12, v3

    goto :goto_0

    .line 2323489
    :cond_1
    const/4 v3, -0x1

    move v11, v3

    goto :goto_2

    .line 2323490
    :cond_2
    const/4 v3, -0x1

    goto :goto_3

    :cond_3
    move v9, v8

    .line 2323491
    goto :goto_5

    :cond_4
    move v7, v5

    .line 2323492
    goto :goto_6

    .line 2323493
    :cond_5
    const/4 v4, 0x0

    goto :goto_7

    .line 2323494
    :cond_6
    add-int/lit8 v7, v6, 0x1

    .line 2323495
    :goto_9
    add-int v6, v10, v14

    .line 2323496
    if-lez v6, :cond_9

    .line 2323497
    move/from16 v0, p3

    if-eq v5, v0, :cond_7

    .line 2323498
    add-int v4, v5, v3

    .line 2323499
    sub-int v5, v6, v13

    .line 2323500
    :goto_a
    add-int/2addr v8, v11

    move v6, v7

    move v10, v5

    move v5, v4

    goto :goto_4

    :cond_7
    move v3, v7

    .line 2323501
    :goto_b
    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    .line 2323502
    add-int v3, p4, v11

    move/from16 v0, p3

    move/from16 v1, p2

    move/from16 v2, p1

    invoke-static {v3, v0, v1, v2}, LX/G9H;->a(IIII)F

    move-result v3

    goto :goto_8

    .line 2323503
    :cond_8
    const/high16 v3, 0x7fc00000    # NaNf

    goto :goto_8

    :cond_9
    move v4, v5

    move v5, v6

    goto :goto_a

    :cond_a
    move v7, v6

    goto :goto_9

    :cond_b
    move v3, v6

    goto :goto_b

    :cond_c
    move/from16 v17, p4

    move/from16 p4, p3

    move/from16 p3, v17

    move/from16 v18, p2

    move/from16 p2, p1

    move/from16 p1, v18

    goto/16 :goto_1
.end method
