.class public final enum LX/HBp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HBp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HBp;

.field public static final enum HEADER:LX/HBp;

.field public static final enum TEMPLATE:LX/HBp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2437906
    new-instance v0, LX/HBp;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/HBp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HBp;->HEADER:LX/HBp;

    .line 2437907
    new-instance v0, LX/HBp;

    const-string v1, "TEMPLATE"

    invoke-direct {v0, v1, v3}, LX/HBp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HBp;->TEMPLATE:LX/HBp;

    .line 2437908
    const/4 v0, 0x2

    new-array v0, v0, [LX/HBp;

    sget-object v1, LX/HBp;->HEADER:LX/HBp;

    aput-object v1, v0, v2

    sget-object v1, LX/HBp;->TEMPLATE:LX/HBp;

    aput-object v1, v0, v3

    sput-object v0, LX/HBp;->$VALUES:[LX/HBp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2437909
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HBp;
    .locals 1

    .prologue
    .line 2437910
    const-class v0, LX/HBp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HBp;

    return-object v0
.end method

.method public static values()[LX/HBp;
    .locals 1

    .prologue
    .line 2437911
    sget-object v0, LX/HBp;->$VALUES:[LX/HBp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HBp;

    return-object v0
.end method
