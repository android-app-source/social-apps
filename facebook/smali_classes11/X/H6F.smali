.class public final LX/H6F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Z

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2426040
    iput-object p1, p0, LX/H6F;->f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iput-object p2, p0, LX/H6F;->a:Ljava/lang/String;

    iput-object p3, p0, LX/H6F;->b:Ljava/lang/String;

    iput-object p4, p0, LX/H6F;->c:Ljava/lang/String;

    iput-boolean p5, p0, LX/H6F;->d:Z

    iput-object p6, p0, LX/H6F;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2426026
    const v1, 0x1e9bf2

    move-object v0, p1

    check-cast v0, LX/4Ua;

    .line 2426027
    iget-object v3, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v3

    .line 2426028
    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    if-ne v1, v0, :cond_0

    .line 2426029
    iget-object v0, p0, LX/H6F;->f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2426030
    iput-boolean v2, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    .line 2426031
    iget-object v0, p0, LX/H6F;->f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, p0, LX/H6F;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Z)V

    .line 2426032
    :goto_0
    return-void

    .line 2426033
    :cond_0
    const v1, 0x14b4d2

    move-object v0, p1

    check-cast v0, LX/4Ua;

    .line 2426034
    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v2

    .line 2426035
    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    if-ne v1, v0, :cond_1

    .line 2426036
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Error loading offers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2426037
    iget-object v0, p0, LX/H6F;->f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0

    .line 2426038
    :cond_1
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Error loading offers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2426039
    iget-object v0, p0, LX/H6F;->f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2426007
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2426008
    if-eqz p1, :cond_2

    .line 2426009
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2426010
    if-eqz v0, :cond_2

    .line 2426011
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2426012
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2426013
    iget-object v0, p0, LX/H6F;->f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2426014
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2426015
    check-cast v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v1

    iget-object v3, p0, LX/H6F;->b:Ljava/lang/String;

    iget-object v4, p0, LX/H6F;->c:Ljava/lang/String;

    iget-boolean v5, p0, LX/H6F;->d:Z

    .line 2426016
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Results: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2426017
    invoke-static {v1}, LX/H83;->a(LX/H70;)LX/H83;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2426018
    invoke-interface {v1}, LX/H6y;->me_()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    .line 2426019
    const/4 v6, 0x1

    iput-boolean v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2426020
    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    .line 2426021
    iget-object v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    new-instance v7, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;

    invoke-direct {v7, v0, v5}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$4;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Z)V

    invoke-virtual {v6, v7}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2426022
    iget-object v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ao:Ljava/lang/String;

    if-nez v6, :cond_0

    if-nez v3, :cond_0

    if-eqz v4, :cond_1

    .line 2426023
    :cond_0
    iget-object v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    iget-object v7, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    iget-object v8, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    iget-object p0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ao:Ljava/lang/String;

    iget-object p1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ap:Ljava/lang/String;

    iget-object v2, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v7, v8, p0, p1, v2}, LX/H82;->a(LX/H83;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2426024
    :cond_1
    :goto_0
    return-void

    .line 2426025
    :cond_2
    iget-object v0, p0, LX/H6F;->f:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0
.end method
