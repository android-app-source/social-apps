.class public LX/FQY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FQY;


# instance fields
.field private final a:Landroid/content/pm/PackageManager;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239452
    iput-object p1, p0, LX/FQY;->a:Landroid/content/pm/PackageManager;

    .line 2239453
    iput-object p2, p0, LX/FQY;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2239454
    return-void
.end method

.method public static a(LX/0QB;)LX/FQY;
    .locals 5

    .prologue
    .line 2239433
    sget-object v0, LX/FQY;->c:LX/FQY;

    if-nez v0, :cond_1

    .line 2239434
    const-class v1, LX/FQY;

    monitor-enter v1

    .line 2239435
    :try_start_0
    sget-object v0, LX/FQY;->c:LX/FQY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2239436
    if-eqz v2, :cond_0

    .line 2239437
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2239438
    new-instance p0, LX/FQY;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4}, LX/FQY;-><init>(Landroid/content/pm/PackageManager;Lcom/facebook/content/SecureContextHelper;)V

    .line 2239439
    move-object v0, p0

    .line 2239440
    sput-object v0, LX/FQY;->c:LX/FQY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2239441
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2239442
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2239443
    :cond_1
    sget-object v0, LX/FQY;->c:LX/FQY;

    return-object v0

    .line 2239444
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2239445
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2239450
    if-eqz p0, :cond_0

    new-instance v0, LX/8A4;

    invoke-direct {v0, p0}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v1, LX/8A3;->MODERATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2239446
    iget-object v0, p0, LX/FQY;->a:Landroid/content/pm/PackageManager;

    const-string v1, "com.facebook.pages.app"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2239447
    if-eqz v0, :cond_0

    .line 2239448
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->PAGES_MANAGER_LANDING_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "timeline_filter"

    const-string v3, "page_only"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "switch_page"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x18000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2239449
    :cond_0
    return-object v0
.end method
