.class public final LX/GtO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400546
    iput-object p1, p0, LX/GtO;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    .line 2400544
    iget-object v0, p0, LX/GtO;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->O:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.facebook.work.reauth.NEED_REAUTH_NOW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "sso_reauth_ref"

    const-string v3, "internal_pref"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2400545
    const/4 v0, 0x1

    return v0
.end method
