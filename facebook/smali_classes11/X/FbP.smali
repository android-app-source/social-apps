.class public LX/FbP;
.super LX/FbF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbF",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FbP;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260173
    invoke-direct {p0}, LX/FbF;-><init>()V

    .line 2260174
    return-void
.end method

.method public static a(LX/0QB;)LX/FbP;
    .locals 3

    .prologue
    .line 2260175
    sget-object v0, LX/FbP;->a:LX/FbP;

    if-nez v0, :cond_1

    .line 2260176
    const-class v1, LX/FbP;

    monitor-enter v1

    .line 2260177
    :try_start_0
    sget-object v0, LX/FbP;->a:LX/FbP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260178
    if-eqz v2, :cond_0

    .line 2260179
    :try_start_1
    new-instance v0, LX/FbP;

    invoke-direct {v0}, LX/FbP;-><init>()V

    .line 2260180
    move-object v0, v0

    .line 2260181
    sput-object v0, LX/FbP;->a:LX/FbP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260182
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260183
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260184
    :cond_1
    sget-object v0, LX/FbP;->a:LX/FbP;

    return-object v0

    .line 2260185
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260187
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2260188
    if-eqz v1, :cond_0

    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-direct {v0, v1, p3}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260189
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2260190
    if-eqz p1, :cond_0

    .line 2260191
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 2260192
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260193
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2260194
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 2260195
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
