.class public final LX/F6X;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/growth/model/Contactpoint;

.field public final synthetic b:LX/F6Y;


# direct methods
.method public constructor <init>(LX/F6Y;Lcom/facebook/growth/model/Contactpoint;)V
    .locals 0

    .prologue
    .line 2200459
    iput-object p1, p0, LX/F6X;->b:LX/F6Y;

    iput-object p2, p0, LX/F6X;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2200449
    iget-object v0, p0, LX/F6X;->b:LX/F6Y;

    iget-object v0, v0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-static {v0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->r(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V

    .line 2200450
    iget-object v0, p0, LX/F6X;->b:LX/F6Y;

    iget-object v0, v0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v0, v0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U8;

    iget-object v1, p0, LX/F6X;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v1}, LX/2U8;->a(Lcom/facebook/growth/model/Contactpoint;)Z

    .line 2200451
    iget-object v0, p0, LX/F6X;->b:LX/F6Y;

    iget-object v0, v0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-virtual {v0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->finish()V

    .line 2200452
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2200454
    iget-object v0, p0, LX/F6X;->b:LX/F6Y;

    iget-object v0, v0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    .line 2200455
    invoke-static {v0, p1}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->a$redex0(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2200456
    iget-object v0, p0, LX/F6X;->b:LX/F6Y;

    iget-object v0, v0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v0, v0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->r:LX/1CW;

    invoke-virtual {v0, p1, v1, v1}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 2200457
    iget-object v1, p0, LX/F6X;->b:LX/F6Y;

    iget-object v1, v1, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v1, v1, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->u:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2200458
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2200453
    invoke-direct {p0}, LX/F6X;->a()V

    return-void
.end method
