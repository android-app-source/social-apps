.class public LX/FKM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;",
        "Lcom/facebook/messaging/service/model/AddAdminsToGroupResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2225031
    check-cast p1, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;

    .line 2225032
    const-string v0, "t_%d/admins"

    iget-object v1, p1, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2225033
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2225034
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "admin_ids"

    .line 2225035
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2225036
    iget-object v4, p1, Lcom/facebook/messaging/service/model/AddAdminsToGroupParams;->b:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/UserKey;

    .line 2225037
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2225038
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 2225039
    invoke-virtual {v4}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225040
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    const-string v3, "addAdminsToGroup"

    .line 2225041
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2225042
    move-object v2, v2

    .line 2225043
    const-string v3, "POST"

    .line 2225044
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2225045
    move-object v2, v2

    .line 2225046
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2225047
    move-object v0, v2

    .line 2225048
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 2225049
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 2225050
    move-object v0, v0

    .line 2225051
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2225052
    move-object v0, v0

    .line 2225053
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2225027
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225028
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225029
    new-instance v1, Lcom/facebook/messaging/service/model/AddAdminsToGroupResult;

    const-string v2, "success"

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "success"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/AddAdminsToGroupResult;-><init>(Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
