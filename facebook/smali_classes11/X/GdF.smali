.class public final LX/GdF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/util/List",
        "<",
        "Lorg/apache/http/NameValuePair;",
        ">;",
        "LX/2fU;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2373332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 2373333
    check-cast p1, Ljava/util/List;

    .line 2373334
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "requestOtaMetaData"

    .line 2373335
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2373336
    move-object v0, v0

    .line 2373337
    const-string v1, "GET"

    .line 2373338
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2373339
    move-object v0, v0

    .line 2373340
    const-string v1, "v2.5/react_native_update"

    .line 2373341
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2373342
    move-object v0, v0

    .line 2373343
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2373344
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2373345
    move-object v0, v0

    .line 2373346
    iput-object p1, v0, LX/14O;->g:Ljava/util/List;

    .line 2373347
    move-object v0, v0

    .line 2373348
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    .line 2373349
    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2373350
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2373351
    if-nez v0, :cond_0

    .line 2373352
    sget-object v0, LX/2fU;->a:LX/2fU;

    move-object v0, v0

    .line 2373353
    :goto_0
    return-object v0

    .line 2373354
    :cond_0
    const-string v1, "update"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2373355
    if-nez v0, :cond_1

    .line 2373356
    sget-object v0, LX/2fU;->b:LX/2fU;

    move-object v0, v0

    .line 2373357
    goto :goto_0

    .line 2373358
    :cond_1
    const-string v1, "download_uri"

    invoke-static {v0, v1}, LX/GdG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2373359
    const-string v2, "version_code"

    invoke-static {v0, v2}, LX/GdG;->b(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 2373360
    const-string v3, "published_date"

    invoke-static {v0, v3}, LX/GdG;->d(LX/0lF;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 2373361
    const-string v4, "ota_bundle_type"

    invoke-static {v0, v4}, LX/GdG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2373362
    const-string v5, "file_size"

    .line 2373363
    invoke-static {v0, v5}, LX/GdG;->b(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    .line 2373364
    if-nez v6, :cond_7

    .line 2373365
    const/4 v6, 0x0

    .line 2373366
    :goto_1
    move v5, v6

    .line 2373367
    const-string v6, "resources_checksum"

    .line 2373368
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 2373369
    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 2373370
    if-eqz v7, :cond_2

    invoke-virtual {v7}, LX/0lF;->h()Z

    move-result p0

    if-nez p0, :cond_8

    :cond_2
    move-object v7, v8

    .line 2373371
    :goto_2
    move-object v6, v7

    .line 2373372
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2373373
    :cond_3
    sget-object v0, LX/2fU;->a:LX/2fU;

    move-object v0, v0

    .line 2373374
    goto :goto_0

    .line 2373375
    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_6

    .line 2373376
    :cond_5
    sget-object v0, LX/2fU;->a:LX/2fU;

    move-object v0, v0

    .line 2373377
    goto :goto_0

    .line 2373378
    :cond_6
    new-instance v0, LX/2fU;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct/range {v0 .. v6}, LX/2fU;-><init>(Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;ILjava/util/Map;)V

    goto :goto_0

    :cond_7
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_1

    .line 2373379
    :cond_8
    invoke-virtual {v7}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object p0

    :cond_9
    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2373380
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0lF;

    .line 2373381
    const-string p1, "key"

    invoke-static {v7, p1}, LX/GdG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2373382
    const-string p2, "value"

    invoke-static {v7, p2}, LX/GdG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2373383
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_9

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_9

    .line 2373384
    invoke-interface {v8, p1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_a
    move-object v7, v8

    .line 2373385
    goto :goto_2
.end method
