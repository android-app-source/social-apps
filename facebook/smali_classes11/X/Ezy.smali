.class public LX/Ezy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Ezy;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;

.field public final d:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0ad;)V
    .locals 4
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const v3, 0x7fffffff

    .line 2187757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187758
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/Ezy;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2187759
    iput-object p1, p0, LX/Ezy;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2187760
    iput-object p2, p0, LX/Ezy;->b:LX/0Or;

    .line 2187761
    iput-object p3, p0, LX/Ezy;->c:LX/0ad;

    .line 2187762
    iget-object v0, p0, LX/Ezy;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2187763
    iget-object v1, p0, LX/Ezy;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, LX/Ezy;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/Ezy;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v2, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2187764
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/Ezy;
    .locals 6

    .prologue
    .line 2187765
    sget-object v0, LX/Ezy;->e:LX/Ezy;

    if-nez v0, :cond_1

    .line 2187766
    const-class v1, LX/Ezy;

    monitor-enter v1

    .line 2187767
    :try_start_0
    sget-object v0, LX/Ezy;->e:LX/Ezy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2187768
    if-eqz v2, :cond_0

    .line 2187769
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2187770
    new-instance v5, LX/Ezy;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v3, p0, v4}, LX/Ezy;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0ad;)V

    .line 2187771
    move-object v0, v5

    .line 2187772
    sput-object v0, LX/Ezy;->e:LX/Ezy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2187773
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2187774
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2187775
    :cond_1
    sget-object v0, LX/Ezy;->e:LX/Ezy;

    return-object v0

    .line 2187776
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2187777
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/Ezy;)V
    .locals 3

    .prologue
    .line 2187754
    iget-object v0, p0, LX/Ezy;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2187755
    :goto_0
    return-void

    .line 2187756
    :cond_0
    iget-object v0, p0, LX/Ezy;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v0, p0, LX/Ezy;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iget-object v2, p0, LX/Ezy;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method
