.class public LX/FNr;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private c:Lcom/facebook/widget/listview/BetterListView;

.field private d:Lcom/facebook/widget/listview/BetterListView;

.field private e:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/view/View$OnClickListener;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2233399
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2233400
    invoke-direct {p0}, LX/FNr;->a()V

    .line 2233401
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2233396
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2233397
    invoke-direct {p0}, LX/FNr;->a()V

    .line 2233398
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2233375
    const v0, 0x7f031354

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2233376
    const v0, 0x7f0d2cb5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, LX/FNr;->a:Landroid/support/v4/view/ViewPager;

    .line 2233377
    const v0, 0x7f0d2cb6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, LX/FNr;->c:Lcom/facebook/widget/listview/BetterListView;

    .line 2233378
    const v0, 0x7f0d2cb8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, LX/FNr;->d:Lcom/facebook/widget/listview/BetterListView;

    .line 2233379
    const v0, 0x7f0d2cb9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/FNr;->e:LX/4ob;

    .line 2233380
    const v0, 0x7f0d2cb4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, LX/FNr;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2233381
    iget-object v0, p0, LX/FNr;->a:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/FNn;

    invoke-virtual {p0}, LX/FNr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/FNn;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2233382
    iget-object v0, p0, LX/FNr;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, LX/FNr;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2233383
    invoke-direct {p0}, LX/FNr;->b()V

    .line 2233384
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2233393
    iget-object v0, p0, LX/FNr;->e:LX/4ob;

    new-instance v1, LX/FNq;

    invoke-direct {v1, p0}, LX/FNq;-><init>(LX/FNr;)V

    .line 2233394
    iput-object v1, v0, LX/4ob;->c:LX/4oa;

    .line 2233395
    return-void
.end method


# virtual methods
.method public getAddressBookContactsListView()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 2233392
    iget-object v0, p0, LX/FNr;->d:Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public getCurrentlyVisibleListView()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 2233391
    iget-object v0, p0, LX/FNr;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FNr;->c:Lcom/facebook/widget/listview/BetterListView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FNr;->d:Lcom/facebook/widget/listview/BetterListView;

    goto :goto_0
.end method

.method public getMessengerContactsListView()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 2233390
    iget-object v0, p0, LX/FNr;->c:Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public setOnContactSyncButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2233388
    iput-object p1, p0, LX/FNr;->f:Landroid/view/View$OnClickListener;

    .line 2233389
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 1

    .prologue
    .line 2233385
    iget-object v0, p0, LX/FNr;->b:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2233386
    iput-object p1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2233387
    return-void
.end method
