.class public final LX/FVZ;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:LX/FVb;


# direct methods
.method public constructor <init>(LX/FVb;)V
    .locals 0

    .prologue
    .line 2250909
    iput-object p1, p0, LX/FVZ;->a:LX/FVb;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 12

    .prologue
    .line 2250876
    iget-object v0, p0, LX/FVZ;->a:LX/FVb;

    .line 2250877
    iget-object v1, v0, LX/FVb;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2250878
    iget-object v2, v0, LX/FVb;->g:LX/16H;

    iget-object v1, v0, LX/FVb;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/79Y;

    .line 2250879
    iget-object v3, v1, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v1, v3

    .line 2250880
    const/4 v11, 0x0

    .line 2250881
    iget-object v9, v2, LX/16H;->b:LX/0gh;

    const-string v10, "saved_dashboard"

    const-string v3, "action_name"

    const-string v4, "saved_dashboard_filter_button_clicked"

    const-string v5, "current_section_type"

    const-string v7, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v6, v1

    invoke-static/range {v3 .. v8}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    invoke-virtual {v9, v10, v11, v11, v3}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2250882
    :cond_0
    new-instance v1, LX/FWn;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/FWn;-><init>(Landroid/content/Context;)V

    .line 2250883
    iget-object v2, v1, LX/FWn;->a:LX/5OG;

    if-nez v2, :cond_1

    .line 2250884
    new-instance v2, LX/FWl;

    invoke-virtual {v1}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/FWl;-><init>(Landroid/content/Context;)V

    .line 2250885
    iput-object v2, v1, LX/FWn;->a:LX/5OG;

    .line 2250886
    iget-object v2, v1, LX/FWn;->a:LX/5OG;

    invoke-virtual {v2, v1}, LX/5OG;->a(LX/5OE;)V

    .line 2250887
    iget-object v2, v1, LX/FWn;->a:LX/5OG;

    invoke-virtual {v2, v1}, LX/5OG;->a(LX/5OF;)V

    .line 2250888
    :cond_1
    iget-object v2, v1, LX/FWn;->a:LX/5OG;

    move-object v2, v2

    .line 2250889
    const/4 v10, 0x1

    .line 2250890
    iget-object v3, v0, LX/FVb;->b:LX/0am;

    invoke-static {v3}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v3, v4}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2250891
    iget-object v4, v0, LX/FVb;->f:LX/79a;

    invoke-virtual {v4}, LX/79a;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_3

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/79Y;

    .line 2250892
    iget v8, v4, LX/79Y;->b:I

    move v8, v8

    .line 2250893
    move v8, v8

    .line 2250894
    invoke-virtual {v2, v8}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v8

    .line 2250895
    iget-object v9, v0, LX/FVb;->f:LX/79a;

    .line 2250896
    iget-object v11, v4, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v11, v11

    .line 2250897
    invoke-virtual {v9, v11}, LX/79a;->a(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)LX/0am;

    move-result-object v9

    sget-object v11, LX/79a;->a:LX/79Z;

    invoke-virtual {v9, v11}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/79Z;

    iget v9, v9, LX/79Z;->a:I

    move v9, v9

    .line 2250898
    invoke-virtual {v8, v9}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2250899
    iget-object v9, v4, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v9, v9

    .line 2250900
    if-ne v3, v9, :cond_2

    .line 2250901
    invoke-virtual {v8, v10}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2250902
    invoke-virtual {v8, v10}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2250903
    :cond_2
    new-instance v9, LX/FVa;

    invoke-direct {v9, v0, v4}, LX/FVa;-><init>(LX/FVb;LX/79Y;)V

    invoke-virtual {v8, v9}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2250904
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2250905
    :cond_3
    iget-object v2, v0, LX/FVb;->h:LX/2dD;

    .line 2250906
    iput-object v2, v1, LX/0ht;->H:LX/2dD;

    .line 2250907
    invoke-virtual {v1, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2250908
    return-void
.end method
