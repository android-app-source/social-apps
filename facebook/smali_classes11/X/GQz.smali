.class public final enum LX/GQz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GQz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GQz;

.field public static final enum EAGER_FLUSHING_EVENT:LX/GQz;

.field public static final enum EVENT_THRESHOLD:LX/GQz;

.field public static final enum EXPLICIT:LX/GQz;

.field public static final enum PERSISTED_EVENTS:LX/GQz;

.field public static final enum SESSION_CHANGE:LX/GQz;

.field public static final enum TIMER:LX/GQz;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2350982
    new-instance v0, LX/GQz;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v3}, LX/GQz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQz;->EXPLICIT:LX/GQz;

    .line 2350983
    new-instance v0, LX/GQz;

    const-string v1, "TIMER"

    invoke-direct {v0, v1, v4}, LX/GQz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQz;->TIMER:LX/GQz;

    .line 2350984
    new-instance v0, LX/GQz;

    const-string v1, "SESSION_CHANGE"

    invoke-direct {v0, v1, v5}, LX/GQz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQz;->SESSION_CHANGE:LX/GQz;

    .line 2350985
    new-instance v0, LX/GQz;

    const-string v1, "PERSISTED_EVENTS"

    invoke-direct {v0, v1, v6}, LX/GQz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQz;->PERSISTED_EVENTS:LX/GQz;

    .line 2350986
    new-instance v0, LX/GQz;

    const-string v1, "EVENT_THRESHOLD"

    invoke-direct {v0, v1, v7}, LX/GQz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQz;->EVENT_THRESHOLD:LX/GQz;

    .line 2350987
    new-instance v0, LX/GQz;

    const-string v1, "EAGER_FLUSHING_EVENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GQz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQz;->EAGER_FLUSHING_EVENT:LX/GQz;

    .line 2350988
    const/4 v0, 0x6

    new-array v0, v0, [LX/GQz;

    sget-object v1, LX/GQz;->EXPLICIT:LX/GQz;

    aput-object v1, v0, v3

    sget-object v1, LX/GQz;->TIMER:LX/GQz;

    aput-object v1, v0, v4

    sget-object v1, LX/GQz;->SESSION_CHANGE:LX/GQz;

    aput-object v1, v0, v5

    sget-object v1, LX/GQz;->PERSISTED_EVENTS:LX/GQz;

    aput-object v1, v0, v6

    sget-object v1, LX/GQz;->EVENT_THRESHOLD:LX/GQz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GQz;->EAGER_FLUSHING_EVENT:LX/GQz;

    aput-object v2, v0, v1

    sput-object v0, LX/GQz;->$VALUES:[LX/GQz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2350989
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GQz;
    .locals 1

    .prologue
    .line 2350990
    const-class v0, LX/GQz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GQz;

    return-object v0
.end method

.method public static values()[LX/GQz;
    .locals 1

    .prologue
    .line 2350991
    sget-object v0, LX/GQz;->$VALUES:[LX/GQz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GQz;

    return-object v0
.end method
