.class public LX/Gb5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0SG;

.field public final d:LX/10N;

.field public final e:LX/10M;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0SG;LX/10N;LX/10M;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            "LX/10N;",
            "LX/10M;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369025
    iput-object p1, p0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2369026
    iput-object p2, p0, LX/Gb5;->b:LX/0Or;

    .line 2369027
    iput-object p3, p0, LX/Gb5;->c:LX/0SG;

    .line 2369028
    iput-object p4, p0, LX/Gb5;->d:LX/10N;

    .line 2369029
    iput-object p5, p0, LX/Gb5;->e:LX/10M;

    .line 2369030
    return-void
.end method

.method public static b(LX/0QB;)LX/Gb5;
    .locals 6

    .prologue
    .line 2369031
    new-instance v0, LX/Gb5;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {p0}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v4

    check-cast v4, LX/10N;

    invoke-static {p0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v5

    check-cast v5, LX/10M;

    invoke-direct/range {v0 .. v5}, LX/Gb5;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0SG;LX/10N;LX/10M;)V

    .line 2369032
    return-object v0
.end method
