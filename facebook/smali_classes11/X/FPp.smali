.class public LX/FPp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements LX/2i9;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/widget/listview/BetterListView;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/FPP;

.field private c:I

.field private d:F

.field private e:F

.field private f:I


# direct methods
.method public constructor <init>(Lcom/facebook/widget/listview/BetterListView;)V
    .locals 1

    .prologue
    .line 2237960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237961
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/FPp;->a:Ljava/lang/ref/WeakReference;

    .line 2237962
    if-eqz p1, :cond_0

    .line 2237963
    invoke-virtual {p1}, Lcom/facebook/widget/listview/BetterListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/FPp;->c:I

    .line 2237964
    invoke-virtual {p1, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2237965
    iput-object p0, p1, Lcom/facebook/widget/listview/BetterListView;->z:LX/2i9;

    .line 2237966
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2237967
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 2237968
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/FPp;->e:F

    iput v0, p0, LX/FPp;->d:F

    .line 2237969
    iput v1, p0, LX/FPp;->f:I

    .line 2237970
    :cond_0
    return v1
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    .line 2237971
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2237972
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 2237973
    iget v0, p0, LX/FPp;->e:F

    cmpl-float v0, v2, v0

    if-eqz v0, :cond_2

    .line 2237974
    iget v0, p0, LX/FPp;->e:F

    cmpl-float v0, v2, v0

    if-lez v0, :cond_3

    .line 2237975
    const/4 v0, -0x1

    .line 2237976
    iget v1, p0, LX/FPp;->d:F

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_4

    .line 2237977
    iget v1, p0, LX/FPp;->e:F

    iput v1, p0, LX/FPp;->d:F

    move v1, v0

    .line 2237978
    :goto_0
    iget v0, p0, LX/FPp;->d:F

    sub-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v3, v0

    .line 2237979
    iget-object v0, p0, LX/FPp;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2237980
    if-nez v0, :cond_5

    .line 2237981
    :cond_0
    :goto_1
    move v0, v4

    .line 2237982
    if-eqz v0, :cond_1

    iget v0, p0, LX/FPp;->c:I

    if-le v3, v0, :cond_1

    iget v0, p0, LX/FPp;->f:I

    if-eq v0, v1, :cond_1

    .line 2237983
    iput v1, p0, LX/FPp;->f:I

    .line 2237984
    iget-object v0, p0, LX/FPp;->b:LX/FPP;

    if-eqz v0, :cond_1

    .line 2237985
    iget-object v0, p0, LX/FPp;->b:LX/FPP;

    iget v1, p0, LX/FPp;->f:I

    invoke-interface {v0, v1}, LX/FPP;->a(I)V

    .line 2237986
    :cond_1
    iput v2, p0, LX/FPp;->e:F

    .line 2237987
    :cond_2
    const/4 v0, 0x0

    return v0

    .line 2237988
    :cond_3
    const/4 v0, 0x1

    .line 2237989
    iget v1, p0, LX/FPp;->d:F

    cmpl-float v1, v2, v1

    if-ltz v1, :cond_4

    .line 2237990
    iget v1, p0, LX/FPp;->e:F

    iput v1, p0, LX/FPp;->d:F

    :cond_4
    move v1, v0

    goto :goto_0

    .line 2237991
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v6

    .line 2237992
    if-eqz v6, :cond_0

    .line 2237993
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result p1

    .line 2237994
    if-lez v1, :cond_7

    .line 2237995
    add-int/lit8 p2, v6, -0x1

    invoke-virtual {v0, p2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result p2

    .line 2237996
    add-int/2addr v6, p1

    .line 2237997
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getCount()I

    move-result p1

    if-lt v6, p1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingBottom()I

    move-result p1

    sub-int/2addr v6, p1

    if-le p2, v6, :cond_0

    :cond_6
    move v4, v5

    goto :goto_1

    .line 2237998
    :cond_7
    invoke-virtual {v0, v4}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    .line 2237999
    if-gtz p1, :cond_8

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingTop()I

    move-result p1

    if-ge v6, p1, :cond_0

    :cond_8
    move v4, v5

    goto :goto_1
.end method
