.class public LX/Gee;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GeG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1vg;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/GeG;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2375903
    iput-object p1, p0, LX/Gee;->a:LX/1vg;

    .line 2375904
    iput-object p2, p0, LX/Gee;->b:LX/0Ot;

    .line 2375905
    return-void
.end method

.method public static a(LX/0QB;)LX/Gee;
    .locals 5

    .prologue
    .line 2375906
    const-class v1, LX/Gee;

    monitor-enter v1

    .line 2375907
    :try_start_0
    sget-object v0, LX/Gee;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375908
    sput-object v2, LX/Gee;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375909
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375910
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375911
    new-instance v4, LX/Gee;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    const/16 p0, 0x20f7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/Gee;-><init>(LX/1vg;LX/0Ot;)V

    .line 2375912
    move-object v0, v4

    .line 2375913
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375914
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gee;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375915
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
