.class public final enum LX/G58;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G58;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G58;

.field public static final enum RECENT_SECTION:LX/G58;

.field public static final enum UNSEEN_SECTION:LX/G58;

.field public static final enum YEAR_SECTION:LX/G58;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2318430
    new-instance v0, LX/G58;

    const-string v1, "UNSEEN_SECTION"

    invoke-direct {v0, v1, v2}, LX/G58;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G58;->UNSEEN_SECTION:LX/G58;

    .line 2318431
    new-instance v0, LX/G58;

    const-string v1, "RECENT_SECTION"

    invoke-direct {v0, v1, v3}, LX/G58;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G58;->RECENT_SECTION:LX/G58;

    .line 2318432
    new-instance v0, LX/G58;

    const-string v1, "YEAR_SECTION"

    invoke-direct {v0, v1, v4}, LX/G58;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G58;->YEAR_SECTION:LX/G58;

    .line 2318433
    const/4 v0, 0x3

    new-array v0, v0, [LX/G58;

    sget-object v1, LX/G58;->UNSEEN_SECTION:LX/G58;

    aput-object v1, v0, v2

    sget-object v1, LX/G58;->RECENT_SECTION:LX/G58;

    aput-object v1, v0, v3

    sget-object v1, LX/G58;->YEAR_SECTION:LX/G58;

    aput-object v1, v0, v4

    sput-object v0, LX/G58;->$VALUES:[LX/G58;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2318429
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G58;
    .locals 1

    .prologue
    .line 2318428
    const-class v0, LX/G58;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G58;

    return-object v0
.end method

.method public static values()[LX/G58;
    .locals 1

    .prologue
    .line 2318427
    sget-object v0, LX/G58;->$VALUES:[LX/G58;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G58;

    return-object v0
.end method
