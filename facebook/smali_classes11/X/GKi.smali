.class public final LX/GKi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GKj;


# direct methods
.method public constructor <init>(LX/GKj;)V
    .locals 0

    .prologue
    .line 2341274
    iput-object p1, p0, LX/GKi;->a:LX/GKj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, -0x76b39aeb

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2341275
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/GKi;->a:LX/GKj;

    iget-object v2, v2, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341276
    sget-object v3, LX/8wL;->BOOSTED_COMPONENT_EDIT_PACING:LX/8wL;

    const v4, 0x7f080b41

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v3, v4, p1}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2341277
    invoke-static {v2}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v4

    .line 2341278
    sget-object p1, LX/8wL;->BOOSTED_COMPONENT_EDIT_PACING:LX/8wL;

    .line 2341279
    iput-object p1, v4, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2341280
    const-string p1, "data"

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2341281
    move-object v1, v3

    .line 2341282
    iget-object v2, p0, LX/GKi;->a:LX/GKj;

    .line 2341283
    iget-object v3, v2, LX/GHg;->b:LX/GCE;

    move-object v2, v3

    .line 2341284
    new-instance v3, LX/GFS;

    const/16 v4, 0x11

    invoke-direct {v3, v1, v4, v6}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v2, v3}, LX/GCE;->a(LX/8wN;)V

    .line 2341285
    const v1, -0x3c7987eb

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
