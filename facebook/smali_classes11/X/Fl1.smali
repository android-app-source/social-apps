.class public final LX/Fl1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2279294
    iput-object p1, p0, LX/Fl1;->b:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iput-object p2, p0, LX/Fl1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x67830cb1

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2279295
    iget-object v1, p0, LX/Fl1;->b:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->l:LX/0Zb;

    iget-object v2, p0, LX/Fl1;->b:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    const-string v3, "fundraiser"

    iget-object v4, p0, LX/Fl1;->b:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v4, v4, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->J:Ljava/lang/String;

    .line 2279296
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_page_tapped_donate"

    invoke-direct {v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "fundraiser_page"

    .line 2279297
    iput-object p1, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2279298
    move-object v6, v6

    .line 2279299
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {v6, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "source"

    invoke-virtual {v6, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "referral_source"

    invoke-virtual {v6, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v2, v6

    .line 2279300
    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2279301
    iget-object v1, p0, LX/Fl1;->b:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->s:LX/17W;

    iget-object v2, p0, LX/Fl1;->b:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Fl1;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2279302
    const v1, -0x5c47c7cf

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
