.class public final LX/G7R;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/G7l;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/G8a;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/G8C;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/G8N;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/G7Q;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:LX/G7y;

.field public static final i:LX/G7V;

.field public static final j:LX/G8Y;

.field public static final k:LX/G82;

.field private static final l:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<",
            "LX/G7l;",
            "LX/G7Q;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<",
            "LX/G8a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<",
            "LX/G8C;",
            "Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/G7R;->a:LX/2vn;

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/G7R;->b:LX/2vn;

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/G7R;->c:LX/2vn;

    new-instance v0, LX/G7N;

    invoke-direct {v0}, LX/G7N;-><init>()V

    sput-object v0, LX/G7R;->l:LX/2vq;

    new-instance v0, LX/G7O;

    invoke-direct {v0}, LX/G7O;-><init>()V

    sput-object v0, LX/G7R;->m:LX/2vq;

    new-instance v0, LX/G7P;

    invoke-direct {v0}, LX/G7P;-><init>()V

    sput-object v0, LX/G7R;->n:LX/2vq;

    sget-object v0, LX/G8M;->b:LX/2vs;

    sput-object v0, LX/G7R;->d:LX/2vs;

    new-instance v0, LX/2vs;

    const-string v1, "Auth.CREDENTIALS_API"

    sget-object v2, LX/G7R;->l:LX/2vq;

    sget-object v3, LX/G7R;->a:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/G7R;->e:LX/2vs;

    new-instance v0, LX/2vs;

    const-string v1, "Auth.GOOGLE_SIGN_IN_API"

    sget-object v2, LX/G7R;->n:LX/2vq;

    sget-object v3, LX/G7R;->c:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/G7R;->f:LX/2vs;

    new-instance v0, LX/2vs;

    const-string v1, "Auth.ACCOUNT_STATUS_API"

    sget-object v2, LX/G7R;->m:LX/2vq;

    sget-object v3, LX/G7R;->b:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/G7R;->g:LX/2vs;

    new-instance v0, LX/G8k;

    invoke-direct {v0}, LX/G8k;-><init>()V

    sput-object v0, LX/G7R;->h:LX/G7y;

    new-instance v0, LX/G7k;

    invoke-direct {v0}, LX/G7k;-><init>()V

    sput-object v0, LX/G7R;->i:LX/G7V;

    new-instance v0, LX/G8Z;

    invoke-direct {v0}, LX/G8Z;-><init>()V

    sput-object v0, LX/G7R;->j:LX/G8Y;

    new-instance v0, LX/G8B;

    invoke-direct {v0}, LX/G8B;-><init>()V

    sput-object v0, LX/G7R;->k:LX/G82;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
