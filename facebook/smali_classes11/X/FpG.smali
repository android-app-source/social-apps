.class public final LX/FpG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;)V
    .locals 0

    .prologue
    .line 2291304
    iput-object p1, p0, LX/FpG;->a:Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2291305
    iget-object v0, p0, LX/FpG;->a:Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;

    iget-object v1, p0, LX/FpG;->a:Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->c:Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;

    invoke-virtual {v1, p3}, Lcom/facebook/socialgood/ui/FundraiserCreationSuggestedCoverPhotoListViewAdapter;->a(I)Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    move-result-object v1

    .line 2291306
    iput-object v1, v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2291307
    iget-object v0, p0, LX/FpG;->a:Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;

    iget-object v1, p0, LX/FpG;->a:Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->b:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 p5, 0x0

    .line 2291308
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object p1, LX/8AB;->FUNDRAISER_CREATION:LX/8AB;

    invoke-virtual {p1}, LX/8AB;->name()Ljava/lang/String;

    move-result-object p1

    new-instance p2, LX/5Rw;

    invoke-direct {p2}, LX/5Rw;-><init>()V

    sget-object p3, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {p3}, LX/74j;->name()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, v1, p3}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object p2

    sget-object p3, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {p2, p3}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object p2

    sget-object p3, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {p2, p3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object p2

    sget-object p3, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {p2, p3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object p2

    sget-object p3, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {p2, p3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object p2

    sget-object p3, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {p2, p3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object p2

    sget-object p3, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {p2, p3}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object p2

    new-instance p3, LX/5Ry;

    invoke-direct {p3}, LX/5Ry;-><init>()V

    const p4, 0x3f0f5c29    # 0.56f

    .line 2291309
    iput p4, p3, LX/5Ry;->c:F

    .line 2291310
    move-object p3, p3

    .line 2291311
    const p4, 0x3fe3d70a    # 1.78f

    .line 2291312
    iput p4, p3, LX/5Ry;->d:F

    .line 2291313
    move-object p3, p3

    .line 2291314
    sget-object p4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    .line 2291315
    iput-object p4, p3, LX/5Ry;->e:LX/434;

    .line 2291316
    move-object p3, p3

    .line 2291317
    invoke-virtual {p3}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object p3

    .line 2291318
    iput-object p3, p2, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 2291319
    move-object p2, p2

    .line 2291320
    iput-boolean p5, p2, LX/5Rw;->f:Z

    .line 2291321
    move-object p2, p2

    .line 2291322
    iput-boolean p5, p2, LX/5Rw;->n:Z

    .line 2291323
    move-object p2, p2

    .line 2291324
    invoke-virtual {p2}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object p2

    invoke-static {p0, p1, p2}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object p0

    .line 2291325
    iget-object p1, v0, Lcom/facebook/socialgood/ui/create/FundraiserCreationSuggestedCoverPhotoFragment;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 p2, 0x3e8

    invoke-interface {p1, p0, p2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2291326
    return-void
.end method
