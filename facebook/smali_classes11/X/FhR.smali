.class public final LX/FhR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/7Hc",
        "<",
        "Lcom/facebook/search/api/SearchTypeaheadResult;",
        ">;",
        "LX/7Hc",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7BS;

.field public final synthetic b:LX/FhT;


# direct methods
.method public constructor <init>(LX/FhT;LX/7BS;)V
    .locals 0

    .prologue
    .line 2273020
    iput-object p1, p0, LX/FhR;->b:LX/FhT;

    iput-object p2, p0, LX/FhR;->a:LX/7BS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2273021
    check-cast p1, LX/7Hc;

    .line 2273022
    iget-object v0, p0, LX/FhR;->b:LX/FhT;

    iget-object v0, v0, LX/FhT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhX;

    iget-object v1, p0, LX/FhR;->a:LX/7BS;

    .line 2273023
    iget-object v2, v1, LX/7BS;->i:LX/7BZ;

    move-object v1, v2

    .line 2273024
    iget-object v2, p0, LX/FhR;->a:LX/7BS;

    .line 2273025
    iget-object p0, v2, LX/7BS;->a:Lcom/facebook/search/api/GraphSearchQuery;

    move-object v2, p0

    .line 2273026
    invoke-virtual {v0, p1, v1, v2}, LX/FhX;->a(LX/7Hc;LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;)LX/7Hc;

    move-result-object v0

    return-object v0
.end method
