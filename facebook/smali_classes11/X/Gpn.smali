.class public LX/Gpn;
.super LX/CoA;
.source ""


# instance fields
.field public g:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/GoE;


# direct methods
.method public constructor <init>(LX/CpT;)V
    .locals 3

    .prologue
    .line 2395745
    invoke-direct {p0, p1}, LX/CoA;-><init>(LX/CpT;)V

    .line 2395746
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gpn;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v2

    check-cast v2, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object p1

    check-cast p1, LX/Go4;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v2, p0, LX/Gpn;->g:LX/Go7;

    iput-object p1, p0, LX/Gpn;->h:LX/Go4;

    iput-object v0, p0, LX/Gpn;->i:LX/0ad;

    .line 2395747
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2395748
    check-cast p1, LX/Cm4;

    invoke-virtual {p0, p1}, LX/CoA;->a(LX/Cm4;)V

    return-void
.end method

.method public final a(LX/Cm4;)V
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2395749
    check-cast p1, LX/GpM;

    .line 2395750
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395751
    check-cast v0, LX/Gqe;

    .line 2395752
    invoke-interface {p1}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v1, v4, :cond_9

    move v1, v2

    .line 2395753
    :goto_0
    iput-boolean v1, v0, LX/Gqe;->O:Z

    .line 2395754
    iget-boolean v4, v0, LX/Gqe;->L:Z

    if-nez v4, :cond_b

    .line 2395755
    iget-object v4, v0, LX/Gqe;->J:LX/Grc;

    sget-object v5, LX/Grb;->HIDDEN:LX/Grb;

    invoke-virtual {v4, v5}, LX/Grc;->a(LX/Grb;)V

    .line 2395756
    :cond_0
    :goto_1
    invoke-interface {p1}, LX/Goc;->p()Z

    move-result v1

    .line 2395757
    iput-boolean v1, v0, LX/Gqe;->I:Z

    .line 2395758
    invoke-interface {p1}, LX/Gof;->lC_()Z

    move-result v1

    .line 2395759
    iput-boolean v1, v0, LX/Gqe;->N:Z

    .line 2395760
    iget-boolean v1, p1, LX/GpM;->b:Z

    move v1, v1

    .line 2395761
    invoke-virtual {v0}, LX/CpT;->w()LX/Cud;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2395762
    invoke-virtual {v0}, LX/CpT;->w()LX/Cud;

    move-result-object v5

    if-nez v1, :cond_d

    const/4 v4, 0x1

    .line 2395763
    :goto_2
    iput-boolean v4, v5, LX/Cud;->d:Z

    .line 2395764
    :cond_1
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 2395765
    check-cast v1, LX/CpT;

    .line 2395766
    iput-boolean v3, v1, LX/CpT;->D:Z

    .line 2395767
    invoke-super {p0, p1}, LX/CoA;->a(LX/Cm4;)V

    .line 2395768
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    iput-object v1, p0, LX/Gpn;->j:LX/GoE;

    .line 2395769
    invoke-interface {p1}, LX/God;->lB_()Z

    move-result v1

    .line 2395770
    iput-boolean v1, v0, LX/Gqe;->L:Z

    .line 2395771
    if-nez v1, :cond_2

    .line 2395772
    iget-object v4, v0, LX/Gqe;->J:LX/Grc;

    sget-object v5, LX/Grb;->HIDDEN:LX/Grb;

    invoke-virtual {v4, v5}, LX/Grc;->a(LX/Grb;)V

    .line 2395773
    :cond_2
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    iput-object v1, p0, LX/Gpn;->j:LX/GoE;

    .line 2395774
    invoke-interface {p1}, LX/Cm4;->t()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    if-ne v1, v4, :cond_a

    .line 2395775
    :goto_3
    iput-boolean v2, v0, LX/Gqe;->M:Z

    .line 2395776
    invoke-interface {p1}, LX/Gog;->ly_()Z

    move-result v1

    .line 2395777
    invoke-static {v0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v2

    .line 2395778
    iput-boolean v1, v2, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;->n:Z

    .line 2395779
    iput-boolean v1, v0, LX/Gqe;->P:Z

    .line 2395780
    invoke-interface {p1}, LX/Gok;->lF_()LX/2uF;

    move-result-object v1

    iget-object v2, p0, LX/Gpn;->j:LX/GoE;

    .line 2395781
    iget-object v3, v0, LX/Gqe;->D:LX/0ad;

    sget-short v4, LX/CHN;->v:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2395782
    iget-object v3, v0, LX/Cos;->a:LX/Ctg;

    move-object v3, v3

    .line 2395783
    const/4 p0, 0x0

    .line 2395784
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/39O;->a()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2395785
    :cond_3
    :goto_4
    instance-of v1, p1, LX/Goj;

    if-eqz v1, :cond_8

    .line 2395786
    invoke-interface {p1}, LX/Gon;->K()Ljava/util/List;

    move-result-object v1

    .line 2395787
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;

    .line 2395788
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2395789
    iget-object v4, v0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    if-nez v4, :cond_5

    .line 2395790
    invoke-static {v0}, LX/Gqe;->m(LX/Gqe;)V

    .line 2395791
    :cond_5
    iget-object v4, v0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {v4, v2}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;)V

    goto :goto_5

    .line 2395792
    :cond_6
    invoke-interface {p1}, LX/Goj;->lA_()Z

    move-result v1

    .line 2395793
    if-eqz v1, :cond_8

    .line 2395794
    iget-object v2, v0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    if-nez v2, :cond_7

    .line 2395795
    invoke-static {v0}, LX/Gqe;->m(LX/Gqe;)V

    .line 2395796
    :cond_7
    iget-object v2, v0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->a()V

    .line 2395797
    :cond_8
    return-void

    :cond_9
    move v1, v3

    .line 2395798
    goto/16 :goto_0

    :cond_a
    move v2, v3

    .line 2395799
    goto :goto_3

    .line 2395800
    :cond_b
    iget-object v5, v0, LX/Gqe;->J:LX/Grc;

    if-eqz v1, :cond_c

    sget-object v4, LX/Grb;->TILT_TO_PAN:LX/Grb;

    :goto_6
    invoke-virtual {v5, v4}, LX/Grc;->a(LX/Grb;)V

    .line 2395801
    invoke-virtual {v0}, LX/Gqe;->u()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2395802
    iget-object v4, v0, LX/Gqe;->J:LX/Grc;

    invoke-virtual {v4}, LX/Grc;->a()V

    goto/16 :goto_1

    .line 2395803
    :cond_c
    sget-object v4, LX/Grb;->EXPANDABLE:LX/Grb;

    goto :goto_6

    .line 2395804
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 2395805
    :cond_e
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 2395806
    iget-object v4, v0, LX/Gqe;->S:Ljava/util/List;

    if-eqz v4, :cond_f

    iget-object v4, v0, LX/Gqe;->S:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2395807
    :cond_f
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, LX/Gqe;->S:Ljava/util/List;

    .line 2395808
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :goto_7
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v8, v4, LX/1vs;->b:I

    .line 2395809
    const v4, 0x7f03097b

    .line 2395810
    iget-object v9, v0, LX/Cos;->a:LX/Ctg;

    move-object v9, v9

    .line 2395811
    invoke-interface {v9}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v9

    invoke-virtual {v6, v4, v9, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    .line 2395812
    invoke-interface {v3}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2395813
    iget-object v9, v0, LX/Gqe;->S:Ljava/util/List;

    new-instance v10, LX/GrY;

    const/4 v11, 0x1

    invoke-virtual {v5, v8, v11}, LX/15i;->g(II)I

    move-result v11

    invoke-static {v5, v11}, LX/Gqe;->a(LX/15i;I)Landroid/graphics/RectF;

    move-result-object v11

    const-class v12, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {v5, v8, p0, v12}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, LX/CHX;

    invoke-direct {v10, v4, v11, v5, v2}, LX/GrY;-><init>(Lcom/facebook/instantshopping/view/widget/TouchTargetView;Landroid/graphics/RectF;LX/CHX;LX/GoE;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2395814
    :cond_10
    new-instance v4, LX/GrZ;

    iget-object v5, v0, LX/Gqe;->S:Ljava/util/List;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, v0, LX/Gqe;->C:LX/GnF;

    invoke-direct {v4, v3, v5, v6, v7}, LX/GrZ;-><init>(LX/Ctg;Ljava/util/List;Landroid/content/Context;LX/GnF;)V

    iput-object v4, v0, LX/Gqe;->R:LX/GrZ;

    .line 2395815
    iget-object v4, v0, LX/Gqe;->R:LX/GrZ;

    invoke-virtual {v0, v4}, LX/Cos;->a(LX/Ctr;)V

    goto/16 :goto_4
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395816
    invoke-super {p0, p1}, LX/CoA;->a(Landroid/os/Bundle;)V

    .line 2395817
    iget-object v0, p0, LX/Gpn;->g:LX/Go7;

    const-string v1, "video_element_start"

    iget-object v2, p0, LX/Gpn;->j:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395818
    iget-object v0, p0, LX/Gpn;->h:LX/Go4;

    iget-object v1, p0, LX/Gpn;->j:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395819
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395820
    invoke-super {p0, p1}, LX/CoA;->b(Landroid/os/Bundle;)V

    .line 2395821
    iget-object v0, p0, LX/Gpn;->g:LX/Go7;

    const-string v1, "video_element_end"

    iget-object v2, p0, LX/Gpn;->j:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395822
    iget-object v0, p0, LX/Gpn;->h:LX/Go4;

    iget-object v1, p0, LX/Gpn;->j:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395823
    return-void
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2395824
    iget-object v1, p0, LX/Gpn;->i:LX/0ad;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Gpn;->i:LX/0ad;

    sget-short v2, LX/CHN;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
