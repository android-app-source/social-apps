.class public final LX/FIi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Uh;


# direct methods
.method public constructor <init>(LX/2Uh;)V
    .locals 0

    .prologue
    .line 2222547
    iput-object p1, p0, LX/FIi;->a:LX/2Uh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2222548
    iget-object v0, p0, LX/FIi;->a:LX/2Uh;

    iget-object v0, v0, LX/2Uh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2222549
    const-wide/16 v2, 0x3e8

    add-long v4, v0, v2

    .line 2222550
    new-instance v6, LX/FIH;

    invoke-direct {v6}, LX/FIH;-><init>()V

    move-wide v2, v0

    .line 2222551
    :goto_0
    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 2222552
    :try_start_0
    invoke-static {}, LX/FIN;->a()[B

    move-result-object v0

    .line 2222553
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v7, v0

    invoke-direct {v1, v0, v7}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 2222554
    iget-object v7, p0, LX/FIi;->a:LX/2Uh;

    iget-object v7, v7, LX/2Uh;->f:Ljava/net/DatagramSocket;

    invoke-virtual {v7, v1}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 2222555
    iget-object v7, p0, LX/FIi;->a:LX/2Uh;

    iget-object v7, v7, LX/2Uh;->a:LX/2Uj;

    .line 2222556
    iput-object v0, v6, LX/FIH;->a:[B

    .line 2222557
    move-object v0, v6

    .line 2222558
    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getLength()I

    move-result v1

    .line 2222559
    iput v1, v0, LX/FIH;->b:I

    .line 2222560
    move-object v0, v0

    .line 2222561
    new-instance v1, LX/FIO;

    iget-object v8, v0, LX/FIH;->a:[B

    iget v9, v0, LX/FIH;->b:I

    invoke-direct {v1, v8, v9}, LX/FIO;-><init>([BI)V

    move-object v0, v1

    .line 2222562
    invoke-virtual {v7, v0}, LX/2Uj;->a(LX/FIO;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222563
    iget-object v0, p0, LX/FIi;->a:LX/2Uh;

    iget-object v0, v0, LX/2Uh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, v2

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 2222564
    goto :goto_0

    .line 2222565
    :catch_0
    move-exception v0

    .line 2222566
    :try_start_1
    const-class v1, LX/FIh;

    const-string v7, "Error receiving on socket"

    invoke-static {v1, v7, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222567
    iget-object v0, p0, LX/FIi;->a:LX/2Uh;

    iget-object v0, v0, LX/2Uh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, v2

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 2222568
    goto :goto_0

    .line 2222569
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/FIi;->a:LX/2Uh;

    iget-object v1, v1, LX/2Uh;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    throw v0

    .line 2222570
    :cond_0
    iget-object v0, p0, LX/FIi;->a:LX/2Uh;

    iget-object v0, v0, LX/2Uh;->a:LX/2Uj;

    invoke-virtual {v0}, LX/2Uj;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2222571
    iget-object v0, p0, LX/FIi;->a:LX/2Uh;

    iget-object v0, v0, LX/2Uh;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIU;

    .line 2222572
    invoke-virtual {v0}, LX/FIU;->a()V

    goto :goto_1

    .line 2222573
    :cond_1
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    return-object v0
.end method
