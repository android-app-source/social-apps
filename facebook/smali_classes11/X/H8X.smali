.class public LX/H8X;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2433487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/HA6;)I
    .locals 1

    .prologue
    .line 2433488
    sget-object v0, LX/HA6;->EDITABLE:LX/HA6;

    if-ne p0, v0, :cond_0

    .line 2433489
    const v0, 0x7f020952

    .line 2433490
    :goto_0
    return v0

    .line 2433491
    :cond_0
    sget-object v0, LX/HA6;->INCOMPLETE:LX/HA6;

    if-ne p0, v0, :cond_1

    .line 2433492
    const v0, 0x7f0207c4

    goto :goto_0

    .line 2433493
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;Z)LX/HA6;
    .locals 2

    .prologue
    .line 2433494
    if-eqz p1, :cond_4

    .line 2433495
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x7ceb8589

    if-ne v0, v1, :cond_2

    .line 2433496
    invoke-static {p0}, LX/H8X;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2433497
    sget-object v0, LX/HA6;->INCOMPLETE:LX/HA6;

    .line 2433498
    :goto_0
    return-object v0

    .line 2433499
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2433500
    sget-object v0, LX/HA6;->EDITABLE:LX/HA6;

    goto :goto_0

    .line 2433501
    :cond_1
    sget-object v0, LX/HA6;->NONE:LX/HA6;

    goto :goto_0

    .line 2433502
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v0, v1, :cond_3

    .line 2433503
    sget-object v0, LX/HA6;->NONE:LX/HA6;

    goto :goto_0

    .line 2433504
    :cond_3
    sget-object v0, LX/HA6;->EDITABLE:LX/HA6;

    goto :goto_0

    .line 2433505
    :cond_4
    sget-object v0, LX/HA6;->NONE:LX/HA6;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Z
    .locals 2

    .prologue
    .line 2433506
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x7ceb8589

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
