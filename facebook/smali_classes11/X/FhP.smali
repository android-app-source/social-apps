.class public LX/FhP;
.super LX/FhH;
.source ""


# instance fields
.field private d:LX/0ad;

.field private e:LX/0Uh;


# direct methods
.method public constructor <init>(LX/1Ck;LX/2Sc;LX/Fhc;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/0ad;LX/7Hq;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/2Sc;",
            "LX/Fhc;",
            "LX/0Ot",
            "<",
            "LX/FhQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FhX;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/7Hq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2272871
    sget-object v2, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, LX/FhH;-><init>(LX/7BZ;LX/1Ck;LX/2Sc;LX/Fhc;LX/0Ot;LX/0Ot;LX/0Ot;LX/7Hq;)V

    .line 2272872
    move-object/from16 v0, p8

    iput-object v0, p0, LX/FhP;->d:LX/0ad;

    .line 2272873
    move-object/from16 v0, p7

    iput-object v0, p0, LX/FhP;->e:LX/0Uh;

    .line 2272874
    return-void
.end method

.method public static a(LX/0ad;LX/0Uh;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2272875
    sget-short v1, LX/100;->cn:S

    invoke-interface {p0, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-short v1, LX/100;->cm:S

    invoke-interface {p0, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, LX/2SU;->e:I

    invoke-virtual {p1, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static b(LX/0QB;)LX/FhP;
    .locals 10

    .prologue
    .line 2272876
    new-instance v0, LX/FhP;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v2

    check-cast v2, LX/2Sc;

    invoke-static {p0}, LX/Fhc;->a(LX/0QB;)LX/Fhc;

    move-result-object v3

    check-cast v3, LX/Fhc;

    const/16 v4, 0x34bf

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x34c2

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x271

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/FhZ;->a(LX/0QB;)LX/7Hq;

    move-result-object v9

    check-cast v9, LX/7Hq;

    invoke-direct/range {v0 .. v9}, LX/FhP;-><init>(LX/1Ck;LX/2Sc;LX/Fhc;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/0ad;LX/7Hq;)V

    .line 2272877
    return-object v0
.end method


# virtual methods
.method public final a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2272878
    invoke-super {p0}, LX/FhH;->h()V

    .line 2272879
    invoke-static {p1}, LX/FhH;->d(LX/7B6;)I

    move-result v0

    .line 2272880
    iget-object v1, p0, LX/FhH;->c:LX/Fhc;

    invoke-virtual {p0}, LX/FhP;->b()LX/7HY;

    move-result-object v2

    invoke-virtual {v1, v2, p1, v0}, LX/Fhc;->a(LX/7HY;LX/7B6;I)V

    .line 2272881
    iget-object v0, p0, LX/FhP;->d:LX/0ad;

    iget-object v1, p0, LX/FhP;->e:LX/0Uh;

    invoke-static {v0, v1}, LX/FhP;->a(LX/0ad;LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2272882
    invoke-virtual {p0, p1}, LX/FhH;->c(LX/7B6;)LX/4d9;

    move-result-object v6

    .line 2272883
    iget-object v0, p0, LX/FhH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhQ;

    iget-object v1, p0, LX/FhH;->b:LX/7BZ;

    .line 2272884
    iget v2, p0, LX/FhH;->h:I

    move v3, v2

    .line 2272885
    invoke-virtual {p0}, LX/FhH;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p1, LX/7B6;->d:LX/0P1;

    const-string v5, "FRIENDLY_NAME"

    invoke-virtual {v2, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p0}, LX/FhP;->b()LX/7HY;

    move-result-object v7

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, LX/FhQ;->a(LX/7BZ;LX/7B6;ILjava/lang/String;Ljava/lang/String;LX/4d9;LX/7HY;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2272886
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FhH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhQ;

    iget-object v1, p0, LX/FhH;->b:LX/7BZ;

    move-object v2, p1

    check-cast v2, Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272887
    iget v3, p0, LX/FhH;->h:I

    move v3, v3

    .line 2272888
    invoke-virtual {p0}, LX/FhH;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, LX/7B6;->d:LX/0P1;

    const-string v6, "FRIENDLY_NAME"

    invoke-virtual {v5, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/FhQ;->a(LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;ILjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2272889
    const-string v0, "fetch_remote_typeahead"

    return-object v0
.end method

.method public final b()LX/7HY;
    .locals 1

    .prologue
    .line 2272890
    sget-object v0, LX/7HY;->REMOTE:LX/7HY;

    return-object v0
.end method
