.class public LX/Fuu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;

.field public final b:LX/G3q;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G3B;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fuw;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fuv;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G3G;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Landroid/app/Activity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private final l:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final m:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/G3q;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G3q;",
            "LX/0Or",
            "<",
            "LX/G3B;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fuw;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fuv;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G3G;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2300617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300618
    iput-boolean v0, p0, LX/Fuu;->j:Z

    .line 2300619
    iput-boolean v0, p0, LX/Fuu;->k:Z

    .line 2300620
    new-instance v0, LX/Fus;

    invoke-direct {v0, p0}, LX/Fus;-><init>(LX/Fuu;)V

    iput-object v0, p0, LX/Fuu;->l:Landroid/view/View$OnClickListener;

    .line 2300621
    new-instance v0, LX/Fut;

    invoke-direct {v0, p0}, LX/Fut;-><init>(LX/Fuu;)V

    iput-object v0, p0, LX/Fuu;->m:Landroid/view/View$OnClickListener;

    .line 2300622
    iput-object p1, p0, LX/Fuu;->b:LX/G3q;

    .line 2300623
    iput-object p2, p0, LX/Fuu;->c:LX/0Or;

    .line 2300624
    iput-object p3, p0, LX/Fuu;->d:LX/0Or;

    .line 2300625
    iput-object p4, p0, LX/Fuu;->e:LX/0Or;

    .line 2300626
    iput-object p5, p0, LX/Fuu;->f:LX/0Or;

    .line 2300627
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;Ljava/lang/String;Lcom/facebook/resources/ui/FbButton;Lcom/facebook/resources/ui/FbButton;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/app/Activity;)V
    .locals 3
    .param p9    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 2300628
    iput-object p1, p0, LX/Fuu;->a:Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;

    .line 2300629
    iput-object p2, p0, LX/Fuu;->g:Ljava/lang/String;

    .line 2300630
    iput-object p9, p0, LX/Fuu;->h:Landroid/view/View;

    .line 2300631
    iput-object p10, p0, LX/Fuu;->i:Landroid/app/Activity;

    .line 2300632
    iget-object v0, p0, LX/Fuu;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2300633
    iget-object v0, p0, LX/Fuu;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2300634
    invoke-virtual {p5, p7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2300635
    invoke-virtual {p6, p8}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2300636
    iget-boolean v0, p0, LX/Fuu;->j:Z

    if-nez v0, :cond_0

    .line 2300637
    iget-object v0, p0, LX/Fuu;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G3G;

    const-string v1, "profile_nux"

    const/4 p2, 0x0

    .line 2300638
    const-string p1, "profile_nux_entry_point_view"

    invoke-static {v0, v1, p2, p2, p1}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2300639
    iput-boolean v2, p0, LX/Fuu;->j:Z

    .line 2300640
    :cond_0
    iget-boolean v0, p0, LX/Fuu;->k:Z

    if-nez v0, :cond_1

    .line 2300641
    iget-object v0, p0, LX/Fuu;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fuw;

    iget-object v1, p0, LX/Fuu;->g:Ljava/lang/String;

    .line 2300642
    new-instance p1, LX/4Ip;

    invoke-direct {p1}, LX/4Ip;-><init>()V

    .line 2300643
    const-string p2, "actor_id"

    invoke-virtual {p1, p2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2300644
    new-instance p2, LX/5yN;

    invoke-direct {p2}, LX/5yN;-><init>()V

    move-object p2, p2

    .line 2300645
    const-string p3, "input"

    invoke-virtual {p2, p3, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2300646
    invoke-static {p2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p1

    .line 2300647
    iget-object p2, v0, LX/Fuw;->a:LX/0tX;

    invoke-virtual {p2, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2300648
    iput-boolean v2, p0, LX/Fuu;->k:Z

    .line 2300649
    :cond_1
    return-void
.end method
