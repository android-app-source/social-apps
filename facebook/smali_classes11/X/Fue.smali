.class public LX/Fue;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:I

.field public final a:Landroid/content/Context;

.field public final b:LX/0ad;

.field private final c:LX/Fsr;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/FwD;

.field public final h:LX/FrB;

.field public final i:LX/BQB;

.field public final j:LX/Fx5;

.field public final k:LX/Fva;

.field public final l:LX/8p8;

.field public final m:LX/BQ1;

.field public final n:LX/G4m;

.field public final o:LX/5SB;

.field private final p:LX/Fv9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:LX/Fw7;

.field private final r:Z

.field public s:LX/FrA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/FrH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/Fsr;LX/0Or;LX/0Or;LX/0Or;LX/FwD;LX/FrB;LX/BQB;LX/8p8;LX/Fx5;LX/Fw7;Ljava/lang/Boolean;LX/Fva;LX/BQ1;LX/G4m;LX/BP0;)V
    .locals 2
    .param p13    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p14    # LX/Fva;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/Fsr;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/FwD;",
            "LX/FrB;",
            "LX/BQB;",
            "LX/8p8;",
            "LX/Fx5;",
            "LX/Fw7;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/timeline/header/IntroCardBinder$IntroCardExpandListener;",
            "LX/BQ1;",
            "LX/G4m;",
            "LX/BP0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300291
    iput-object p1, p0, LX/Fue;->a:Landroid/content/Context;

    .line 2300292
    iput-object p2, p0, LX/Fue;->b:LX/0ad;

    .line 2300293
    iput-object p3, p0, LX/Fue;->c:LX/Fsr;

    .line 2300294
    iput-object p4, p0, LX/Fue;->d:LX/0Or;

    .line 2300295
    iput-object p5, p0, LX/Fue;->e:LX/0Or;

    .line 2300296
    iput-object p6, p0, LX/Fue;->f:LX/0Or;

    .line 2300297
    iput-object p7, p0, LX/Fue;->g:LX/FwD;

    .line 2300298
    iput-object p8, p0, LX/Fue;->h:LX/FrB;

    .line 2300299
    iput-object p9, p0, LX/Fue;->i:LX/BQB;

    .line 2300300
    invoke-virtual {p3}, LX/Fsr;->e()LX/Fv9;

    move-result-object v1

    iput-object v1, p0, LX/Fue;->p:LX/Fv9;

    .line 2300301
    iput-object p10, p0, LX/Fue;->l:LX/8p8;

    .line 2300302
    iput-object p11, p0, LX/Fue;->j:LX/Fx5;

    .line 2300303
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Fue;->k:LX/Fva;

    .line 2300304
    iput-object p12, p0, LX/Fue;->q:LX/Fw7;

    .line 2300305
    invoke-virtual {p13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/Fue;->r:Z

    .line 2300306
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Fue;->m:LX/BQ1;

    .line 2300307
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Fue;->n:LX/G4m;

    .line 2300308
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Fue;->o:LX/5SB;

    .line 2300309
    return-void
.end method

.method public static b(LX/Fue;Lcom/facebook/timeline/header/TimelineIntroCardView;)V
    .locals 12

    .prologue
    .line 2300262
    invoke-static {p0}, LX/Fue;->e(LX/Fue;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2300263
    iget-object v0, p0, LX/Fue;->x:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2300264
    new-instance v0, LX/FuZ;

    invoke-direct {v0, p0}, LX/FuZ;-><init>(LX/Fue;)V

    iput-object v0, p0, LX/Fue;->x:Landroid/view/View$OnClickListener;

    .line 2300265
    :cond_0
    iget-object v0, p0, LX/Fue;->x:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 2300266
    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->b(Landroid/view/View$OnClickListener;)V

    .line 2300267
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardView;->h()V

    .line 2300268
    iget-object v0, p0, LX/Fue;->p:LX/Fv9;

    if-eqz v0, :cond_1

    .line 2300269
    iget-object v0, p0, LX/Fue;->p:LX/Fv9;

    .line 2300270
    iget-boolean v1, v0, LX/Fv9;->r:Z

    if-eqz v1, :cond_5

    .line 2300271
    :cond_1
    :goto_0
    return-void

    .line 2300272
    :cond_2
    iget-boolean v0, p0, LX/Fue;->r:Z

    if-nez v0, :cond_4

    .line 2300273
    iget-object v0, p0, LX/Fue;->w:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_3

    .line 2300274
    new-instance v0, LX/Fua;

    invoke-direct {v0, p0}, LX/Fua;-><init>(LX/Fue;)V

    iput-object v0, p0, LX/Fue;->w:Landroid/view/View$OnClickListener;

    .line 2300275
    :cond_3
    iget-object v0, p0, LX/Fue;->w:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 2300276
    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->c(Landroid/view/View$OnClickListener;)V

    .line 2300277
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardView;->g()V

    goto :goto_0

    .line 2300278
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Fv9;->r:Z

    .line 2300279
    iget-object v1, v0, LX/Fv9;->e:LX/BQ9;

    iget-wide v3, v0, LX/Fv9;->c:J

    iget-object v2, v0, LX/Fv9;->b:Ljava/lang/String;

    .line 2300280
    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "context_item_add_prompt_impression"

    move-object v6, v1

    move-wide v7, v3

    move-object v9, v2

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2300281
    if-eqz v5, :cond_6

    .line 2300282
    iget-object v6, v1, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300283
    :cond_6
    goto :goto_0
.end method

.method public static d(LX/Fue;)V
    .locals 3

    .prologue
    .line 2300287
    iget-object v0, p0, LX/Fue;->c:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->ll_()V

    .line 2300288
    iget-object v0, p0, LX/Fue;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/Fue;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->bJ:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2300289
    return-void
.end method

.method public static e(LX/Fue;)Z
    .locals 1

    .prologue
    .line 2300310
    iget-object v0, p0, LX/Fue;->t:LX/FrH;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fue;->t:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/Fue;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2300284
    iget-object v0, p0, LX/Fue;->u:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2300285
    new-instance v0, LX/Fub;

    invoke-direct {v0, p0}, LX/Fub;-><init>(LX/Fue;)V

    iput-object v0, p0, LX/Fue;->u:Landroid/view/View$OnClickListener;

    .line 2300286
    :cond_0
    iget-object v0, p0, LX/Fue;->u:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/header/TimelineIntroCardView;ZZ)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2300161
    iput-boolean p2, p0, LX/Fue;->y:Z

    .line 2300162
    iput-boolean p3, p0, LX/Fue;->z:Z

    .line 2300163
    const/4 v3, 0x0

    .line 2300164
    iget-object v2, p0, LX/Fue;->b:LX/0ad;

    sget-short v4, LX/0wf;->W:S

    invoke-interface {v2, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Fue;->m:LX/BQ1;

    .line 2300165
    iget-object v4, v2, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v2, v4

    .line 2300166
    if-eqz v2, :cond_e

    .line 2300167
    iget-object v4, p0, LX/Fue;->m:LX/BQ1;

    iget-object v5, p0, LX/Fue;->o:LX/5SB;

    const/4 v2, 0x0

    .line 2300168
    iget-object v6, v4, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v6, v6

    .line 2300169
    if-nez v6, :cond_f

    .line 2300170
    :cond_0
    :goto_0
    move v2, v2

    .line 2300171
    iget-object v4, p0, LX/Fue;->t:LX/FrH;

    if-nez v4, :cond_1

    .line 2300172
    new-instance v4, LX/FrH;

    invoke-direct {v4, v2}, LX/FrH;-><init>(I)V

    move-object v4, v4

    .line 2300173
    iput-object v4, p0, LX/Fue;->t:LX/FrH;

    .line 2300174
    :cond_1
    iget-object v4, p0, LX/Fue;->t:LX/FrH;

    iget-object v5, p0, LX/Fue;->m:LX/BQ1;

    .line 2300175
    iget-object v6, v5, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v5, v6

    .line 2300176
    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v5

    .line 2300177
    if-nez v5, :cond_13

    .line 2300178
    const/4 v6, 0x0

    .line 2300179
    :goto_1
    move-object v5, v6

    .line 2300180
    iget-object v6, p0, LX/Fue;->q:LX/Fw7;

    iget-object v7, p0, LX/Fue;->o:LX/5SB;

    iget-object v8, p0, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v6, v7, v8}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v6

    iget-object v7, p0, LX/Fue;->o:LX/5SB;

    iget-object v8, p0, LX/Fue;->m:LX/BQ1;

    const/4 v9, 0x3

    const/4 p2, 0x2

    .line 2300181
    sget-object p3, LX/Fve;->COLLAPSED:LX/Fve;

    if-ne v6, p3, :cond_14

    invoke-virtual {v8}, LX/BQ1;->aa()Z

    move-result p3

    if-eqz p3, :cond_14

    invoke-virtual {v8}, LX/BQ1;->ac()Z

    move-result p3

    if-nez p3, :cond_14

    invoke-static {v7, v8}, LX/FwD;->b(LX/5SB;LX/BQ1;)Z

    move-result p3

    if-eqz p3, :cond_14

    .line 2300182
    :cond_2
    :goto_2
    move v2, v9

    .line 2300183
    invoke-virtual {v4, v5, v2}, LX/FrH;->a(LX/Fr5;I)V

    .line 2300184
    iget-object v2, p0, LX/Fue;->s:LX/FrA;

    if-nez v2, :cond_3

    .line 2300185
    new-instance v2, LX/FuY;

    invoke-direct {v2, p0}, LX/FuY;-><init>(LX/Fue;)V

    .line 2300186
    iget-object v4, p0, LX/Fue;->h:LX/FrB;

    iget-object v5, p0, LX/Fue;->t:LX/FrH;

    sget-object v6, LX/Fr2;->INTRO_CARD_PUBLIC_ABOUT_ITEMS_STYLE:LX/Fr2;

    iget-object v7, p0, LX/Fue;->a:Landroid/content/Context;

    const v8, 0x7f081609

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v2, v7}, LX/FrB;->a(LX/FrH;LX/Fr2;Landroid/view/View$OnClickListener;Ljava/lang/String;)LX/FrA;

    move-result-object v2

    iput-object v2, p0, LX/Fue;->s:LX/FrA;

    .line 2300187
    :cond_3
    iget-object v2, p0, LX/Fue;->s:LX/FrA;

    invoke-virtual {p1, v2}, Lcom/facebook/timeline/header/TimelineIntroCardView;->a(LX/FrA;)V

    .line 2300188
    iget-object v2, p0, LX/Fue;->o:LX/5SB;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Fue;->q:LX/Fw7;

    iget-object v4, p0, LX/Fue;->o:LX/5SB;

    iget-object v5, p0, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v2, v4, v5}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v2

    sget-object v4, LX/Fve;->COLLAPSED:LX/Fve;

    if-eq v2, v4, :cond_4

    .line 2300189
    invoke-static {p0, p1}, LX/Fue;->b(LX/Fue;Lcom/facebook/timeline/header/TimelineIntroCardView;)V

    .line 2300190
    :cond_4
    invoke-static {p0}, LX/Fue;->e(LX/Fue;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2300191
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardView;->i()V

    .line 2300192
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardView;->j()Z

    move-result v2

    .line 2300193
    :goto_3
    iget-object v4, p0, LX/Fue;->s:LX/FrA;

    if-nez v4, :cond_d

    .line 2300194
    :goto_4
    iget-object v4, p0, LX/Fue;->i:LX/BQB;

    .line 2300195
    iput v3, v4, LX/BQB;->G:I

    .line 2300196
    move v3, v2

    .line 2300197
    iget-object v2, p0, LX/Fue;->l:LX/8p8;

    invoke-virtual {v2}, LX/8p8;->c()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2300198
    const/4 v2, 0x0

    .line 2300199
    :goto_5
    move v2, v2

    .line 2300200
    if-nez v3, :cond_6

    if-eqz v2, :cond_7

    :cond_6
    move v2, v0

    .line 2300201
    :goto_6
    if-nez v2, :cond_8

    .line 2300202
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->setVisibility(I)V

    .line 2300203
    :goto_7
    iget-boolean v0, p0, LX/Fue;->z:Z

    if-eqz v0, :cond_c

    .line 2300204
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e()V

    .line 2300205
    :goto_8
    return-void

    :cond_7
    move v2, v1

    .line 2300206
    goto :goto_6

    .line 2300207
    :cond_8
    invoke-virtual {p1, v1}, Lcom/facebook/timeline/header/TimelineIntroCardView;->setVisibility(I)V

    .line 2300208
    iget-object v2, p0, LX/Fue;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0df0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2300209
    iget-object v2, p0, LX/Fue;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0df1

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2300210
    iget-boolean v2, p0, LX/Fue;->y:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/Fue;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0b0df2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2300211
    :goto_9
    if-eqz v3, :cond_a

    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardView;->k()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2300212
    :goto_a
    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iput v0, p0, LX/Fue;->A:I

    .line 2300213
    invoke-virtual {p1, v4, v2, v5, v1}, Lcom/facebook/timeline/header/TimelineIntroCardView;->setPadding(IIII)V

    .line 2300214
    const v0, 0x7f0a00d5

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroCardView;->setBackgroundResource(I)V

    goto :goto_7

    :cond_9
    move v2, v1

    .line 2300215
    goto :goto_9

    :cond_a
    move v0, v1

    .line 2300216
    goto :goto_a

    .line 2300217
    :cond_b
    iget-object v0, p0, LX/Fue;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0df2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_b

    .line 2300218
    :cond_c
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->f()V

    goto :goto_8

    .line 2300219
    :cond_d
    iget-object v3, p0, LX/Fue;->s:LX/FrA;

    invoke-virtual {v3}, LX/FrA;->getCount()I

    move-result v3

    goto/16 :goto_4

    :cond_e
    move v2, v3

    goto/16 :goto_3

    .line 2300220
    :cond_f
    invoke-static {v4}, LX/FwD;->b(LX/BQ1;)Z

    move-result v6

    if-nez v6, :cond_10

    .line 2300221
    iget-object v2, v4, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v2, v2

    .line 2300222
    invoke-static {v2}, LX/FwD;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;)I

    move-result v2

    goto/16 :goto_0

    .line 2300223
    :cond_10
    invoke-virtual {v4}, LX/BQ1;->aa()Z

    move-result v6

    if-nez v6, :cond_11

    invoke-virtual {v5}, LX/5SB;->i()Z

    move-result v6

    if-eqz v6, :cond_12

    .line 2300224
    :cond_11
    :goto_c
    iget-object v6, v4, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v6, v6

    .line 2300225
    invoke-static {v6}, LX/FwD;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;)I

    move-result v6

    .line 2300226
    add-int/lit8 v7, v2, 0x1

    if-gt v6, v7, :cond_0

    invoke-static {v5, v4}, LX/FwD;->b(LX/5SB;LX/BQ1;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v5}, LX/5SB;->i()Z

    move-result v7

    if-nez v7, :cond_0

    move v2, v6

    .line 2300227
    goto/16 :goto_0

    .line 2300228
    :cond_12
    const/4 v2, 0x2

    goto :goto_c

    :cond_13
    new-instance v6, LX/Fr5;

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;

    move-result-object v8

    invoke-direct {v6, v7, v8}, LX/Fr5;-><init>(LX/0Px;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;)V

    goto/16 :goto_1

    .line 2300229
    :cond_14
    sget-object p3, LX/Fve;->COLLAPSED:LX/Fve;

    if-ne v6, p3, :cond_15

    invoke-virtual {v8}, LX/BQ1;->aa()Z

    move-result p3

    if-nez p3, :cond_15

    if-gt v2, p2, :cond_15

    invoke-static {v7, v8}, LX/FwD;->b(LX/5SB;LX/BQ1;)Z

    move-result p3

    if-nez p3, :cond_2

    .line 2300230
    :cond_15
    sget-object v9, LX/Fve;->COLLAPSED:LX/Fve;

    if-ne v6, v9, :cond_16

    const/4 v9, 0x1

    goto/16 :goto_2

    :cond_16
    move v9, p2

    goto/16 :goto_2

    .line 2300231
    :cond_17
    iget-object v2, p0, LX/Fue;->g:LX/FwD;

    iget-object v4, p0, LX/Fue;->m:LX/BQ1;

    iget-object v5, p0, LX/Fue;->o:LX/5SB;

    iget-object v6, p0, LX/Fue;->q:LX/Fw7;

    iget-object v7, p0, LX/Fue;->o:LX/5SB;

    iget-object v8, p0, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v6, v7, v8}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, LX/FwD;->a(LX/BQ1;LX/5SB;LX/Fve;)Ljava/lang/Integer;

    move-result-object v4

    .line 2300232
    const/4 v2, 0x1

    .line 2300233
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 2300234
    :goto_d
    iget-object v5, p0, LX/Fue;->i:LX/BQB;

    invoke-static {v4}, LX/FwC;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v4

    .line 2300235
    iput-object v4, v5, LX/BQB;->H:Ljava/lang/String;

    .line 2300236
    goto/16 :goto_5

    .line 2300237
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b()V

    .line 2300238
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d()V

    .line 2300239
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c()V

    .line 2300240
    const/4 v2, 0x0

    move v2, v2

    .line 2300241
    goto :goto_d

    .line 2300242
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b()V

    .line 2300243
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d()V

    .line 2300244
    invoke-static {p0}, LX/Fue;->f(LX/Fue;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Landroid/view/View$OnClickListener;)V

    .line 2300245
    const/4 v2, 0x1

    move v2, v2

    .line 2300246
    goto :goto_d

    .line 2300247
    :pswitch_2
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c()V

    .line 2300248
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d()V

    .line 2300249
    iget-object v2, p0, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, LX/Fue;->o:LX/5SB;

    invoke-virtual {v5}, LX/5SB;->i()Z

    move-result v5

    iget-object v6, p0, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->ad()LX/0Px;

    move-result-object v6

    invoke-static {p0}, LX/Fue;->f(LX/Fue;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {p1, v2, v5, v6, v7}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Ljava/lang/String;ZLX/0Px;Landroid/view/View$OnClickListener;)V

    .line 2300250
    const/4 v2, 0x1

    move v2, v2

    .line 2300251
    goto :goto_d

    .line 2300252
    :pswitch_3
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c()V

    .line 2300253
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b()V

    .line 2300254
    iget-object v2, p0, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->af()LX/0Px;

    move-result-object v5

    .line 2300255
    iget-object v6, p0, LX/Fue;->v:Landroid/view/View$OnClickListener;

    if-nez v6, :cond_18

    .line 2300256
    new-instance v6, LX/Fuc;

    invoke-direct {v6, p0}, LX/Fuc;-><init>(LX/Fue;)V

    iput-object v6, p0, LX/Fue;->v:Landroid/view/View$OnClickListener;

    .line 2300257
    :cond_18
    iget-object v6, p0, LX/Fue;->v:Landroid/view/View$OnClickListener;

    move-object v6, v6

    .line 2300258
    new-instance v7, LX/Fud;

    invoke-direct {v7, p0, p1}, LX/Fud;-><init>(LX/Fue;Lcom/facebook/timeline/header/TimelineIntroCardView;)V

    move-object v7, v7

    .line 2300259
    invoke-virtual {p1, v2, v5, v6, v7}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Ljava/lang/String;LX/0Px;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2300260
    const/4 v2, 0x1

    move v2, v2

    .line 2300261
    goto :goto_d

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
