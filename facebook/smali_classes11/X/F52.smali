.class public final enum LX/F52;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F52;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F52;

.field public static final enum Community:LX/F52;

.field public static final enum Invite:LX/F52;

.field public static final enum Privacy:LX/F52;

.field public static final enum Type:LX/F52;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2197901
    new-instance v0, LX/F52;

    const-string v1, "Invite"

    invoke-direct {v0, v1, v2}, LX/F52;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F52;->Invite:LX/F52;

    new-instance v0, LX/F52;

    const-string v1, "Community"

    invoke-direct {v0, v1, v3}, LX/F52;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F52;->Community:LX/F52;

    new-instance v0, LX/F52;

    const-string v1, "Privacy"

    invoke-direct {v0, v1, v4}, LX/F52;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F52;->Privacy:LX/F52;

    new-instance v0, LX/F52;

    const-string v1, "Type"

    invoke-direct {v0, v1, v5}, LX/F52;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F52;->Type:LX/F52;

    .line 2197902
    const/4 v0, 0x4

    new-array v0, v0, [LX/F52;

    sget-object v1, LX/F52;->Invite:LX/F52;

    aput-object v1, v0, v2

    sget-object v1, LX/F52;->Community:LX/F52;

    aput-object v1, v0, v3

    sget-object v1, LX/F52;->Privacy:LX/F52;

    aput-object v1, v0, v4

    sget-object v1, LX/F52;->Type:LX/F52;

    aput-object v1, v0, v5

    sput-object v0, LX/F52;->$VALUES:[LX/F52;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2197903
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F52;
    .locals 1

    .prologue
    .line 2197904
    const-class v0, LX/F52;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F52;

    return-object v0
.end method

.method public static values()[LX/F52;
    .locals 1

    .prologue
    .line 2197905
    sget-object v0, LX/F52;->$VALUES:[LX/F52;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F52;

    return-object v0
.end method
