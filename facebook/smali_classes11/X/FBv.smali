.class public final LX/FBv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/FBw;


# direct methods
.method public constructor <init>(LX/FBw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2209884
    iput-object p1, p0, LX/FBv;->b:LX/FBw;

    iput-object p2, p0, LX/FBv;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2209883
    iget-object v0, p0, LX/FBv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2209882
    iget-object v0, p0, LX/FBv;->b:LX/FBw;

    iget-object v0, v0, LX/FBw;->a:LX/FBx;

    iget-object v0, v0, LX/FBx;->a:Landroid/os/Bundle;

    iget-object v1, p0, LX/FBv;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2209881
    iget-object v0, p0, LX/FBv;->b:LX/FBw;

    iget-object v0, v0, LX/FBw;->a:LX/FBx;

    iget-object v1, p0, LX/FBv;->a:Ljava/lang/String;

    invoke-virtual {p0}, LX/FBv;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, LX/FBx;->a$redex0(LX/FBx;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
