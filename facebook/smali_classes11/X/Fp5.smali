.class public final LX/Fp5;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V
    .locals 0

    .prologue
    .line 2290538
    iput-object p1, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 10

    .prologue
    .line 2290539
    iget-object v0, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2290540
    iget-object v1, v0, LX/BOa;->a:LX/0Zb;

    const-string v2, "fundraiser_creation_tapped_create"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290541
    iget-object v0, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2290542
    iget-object v1, v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2290543
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 2290544
    :goto_0
    iget-object v1, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->c:LX/FkM;

    iget-object v2, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-static {v3}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->q(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)LX/2vO;

    move-result-object v3

    iget-object v4, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-static {v4}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)Z

    move-result v4

    .line 2290545
    invoke-virtual {v1, v2}, LX/FkM;->a(LX/0gc;)V

    .line 2290546
    if-eqz v4, :cond_2

    .line 2290547
    new-instance v5, LX/FnL;

    invoke-direct {v5}, LX/FnL;-><init>()V

    move-object v5, v5

    .line 2290548
    const-string v6, "input"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/FnL;

    .line 2290549
    new-instance v6, LX/FkK;

    iget-object v7, v1, LX/FkM;->c:LX/FkL;

    invoke-direct {v6, v7}, LX/FkK;-><init>(LX/FkL;)V

    move-object v7, v5

    .line 2290550
    :goto_1
    if-eqz v0, :cond_3

    .line 2290551
    new-instance v5, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2290552
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/46I;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2290553
    new-instance v9, LX/4cQ;

    const-string p0, "cover_photo_upload"

    new-instance p1, LX/4ct;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v5, v8, p2}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, p0, p1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2290554
    new-instance v5, LX/399;

    invoke-static {v9}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    .line 2290555
    sget-object v9, LX/0Re;->a:LX/0Re;

    move-object v9, v9

    .line 2290556
    invoke-direct {v5, v7, v8, v9}, LX/399;-><init>(LX/0zP;LX/0Px;LX/0Rf;)V

    .line 2290557
    :goto_2
    iget-object v7, v1, LX/FkM;->a:LX/0tX;

    invoke-virtual {v7, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    iget-object v7, v1, LX/FkM;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2290558
    return-void

    .line 2290559
    :cond_1
    iget-object v0, p0, LX/Fp5;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2290560
    :cond_2
    new-instance v5, LX/Fn7;

    invoke-direct {v5}, LX/Fn7;-><init>()V

    move-object v5, v5

    .line 2290561
    const-string v6, "input"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/Fn7;

    .line 2290562
    new-instance v6, LX/FkJ;

    iget-object v7, v1, LX/FkM;->c:LX/FkL;

    invoke-direct {v6, v7}, LX/FkJ;-><init>(LX/FkL;)V

    move-object v7, v5

    goto :goto_1

    .line 2290563
    :cond_3
    new-instance v5, LX/399;

    invoke-direct {v5, v7}, LX/399;-><init>(LX/0zP;)V

    goto :goto_2
.end method
