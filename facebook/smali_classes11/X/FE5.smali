.class public final LX/FE5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V
    .locals 0

    .prologue
    .line 2215995
    iput-object p1, p0, LX/FE5;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x12d205d5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2215996
    iget-object v1, p0, LX/FE5;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    iget-object v1, v1, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    if-eqz v1, :cond_0

    .line 2215997
    iget-object v1, p0, LX/FE5;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    iget-object v1, v1, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, LX/FEF;->a(I)V

    .line 2215998
    :cond_0
    iget-object v1, p0, LX/FE5;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    .line 2215999
    invoke-static {v1}, LX/6eE;->a(Landroid/support/v4/app/Fragment;)LX/6eE;

    move-result-object v2

    .line 2216000
    new-instance v4, LX/DgG;

    invoke-direct {v4, v2}, LX/DgG;-><init>(LX/6eE;)V

    move-object v2, v4

    .line 2216001
    sget-object v4, LX/DgH;->SELECT:LX/DgH;

    iput-object v4, v2, LX/DgG;->a:LX/DgH;

    .line 2216002
    move-object v2, v2

    .line 2216003
    invoke-virtual {v2}, LX/DgG;->b()LX/DgI;

    move-result-object v2

    .line 2216004
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2216005
    iget-object p0, v2, LX/DgI;->b:LX/6eE;

    invoke-virtual {p0, v4}, LX/6eE;->a(Landroid/os/Bundle;)V

    .line 2216006
    iget-object p0, v2, LX/DgI;->a:LX/DgH;

    if-eqz p0, :cond_1

    .line 2216007
    const-string p0, "button_style"

    iget-object p1, v2, LX/DgI;->a:LX/DgH;

    invoke-virtual {p1}, LX/DgH;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216008
    :cond_1
    iget-object p0, v2, LX/DgI;->c:Lcom/facebook/android/maps/model/LatLng;

    if-eqz p0, :cond_4

    .line 2216009
    const-string p0, "initial_pinned_location"

    iget-object p1, v2, LX/DgI;->c:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2216010
    :cond_2
    :goto_0
    const-string p0, "show_dismiss_button"

    iget-boolean p1, v2, LX/DgI;->e:Z

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2216011
    iget-object p0, v2, LX/DgI;->f:Ljava/lang/String;

    if-eqz p0, :cond_3

    .line 2216012
    const-string p0, "omni_m_action_id"

    iget-object p1, v2, LX/DgI;->f:Ljava/lang/String;

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216013
    :cond_3
    new-instance p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;-><init>()V

    .line 2216014
    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2216015
    move-object v2, p0

    .line 2216016
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    invoke-virtual {v4}, LX/0gc;->a()LX/0hH;

    move-result-object v4

    const-string p0, "LOCATION_SHARE_FRAGMENT_TAG"

    const/4 p1, 0x1

    invoke-virtual {v2, v4, p0, p1}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 2216017
    const v1, 0x6cb071e

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2216018
    :cond_4
    iget-object p0, v2, LX/DgI;->d:Lcom/facebook/messaging/location/sending/NearbyPlace;

    if-eqz p0, :cond_2

    .line 2216019
    const-string p0, "initial_nearby_place"

    iget-object p1, v2, LX/DgI;->d:Lcom/facebook/messaging/location/sending/NearbyPlace;

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method
