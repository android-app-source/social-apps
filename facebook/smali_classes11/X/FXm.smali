.class public final LX/FXm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AUN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AUN",
        "<",
        "Lcom/facebook/saved2/model/Saved2UnreadCountsTable_Queries$BaseQueryDAO;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/saved2/ui/Saved2Fragment;


# direct methods
.method public constructor <init>(Lcom/facebook/saved2/ui/Saved2Fragment;)V
    .locals 0

    .prologue
    .line 2255401
    iput-object p1, p0, LX/FXm;->a:Lcom/facebook/saved2/ui/Saved2Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2255402
    iget-object v0, p0, LX/FXm;->a:Lcom/facebook/saved2/ui/Saved2Fragment;

    iget-object v0, v0, Lcom/facebook/saved2/ui/Saved2Fragment;->s:LX/FXw;

    invoke-virtual {v0, v1, v1}, LX/FXP;->a(Landroid/database/Cursor;LX/AU0;)V

    .line 2255403
    return-void
.end method

.method public final a(LX/AU0;)V
    .locals 2

    .prologue
    .line 2255404
    check-cast p1, LX/BOG;

    .line 2255405
    iget-object v0, p0, LX/FXm;->a:Lcom/facebook/saved2/ui/Saved2Fragment;

    iget-object v0, v0, Lcom/facebook/saved2/ui/Saved2Fragment;->b:LX/FWt;

    invoke-virtual {v0}, LX/FWt;->f()V

    .line 2255406
    iget-object v0, p0, LX/FXm;->a:Lcom/facebook/saved2/ui/Saved2Fragment;

    iget-object v0, v0, Lcom/facebook/saved2/ui/Saved2Fragment;->s:LX/FXw;

    invoke-interface {p1}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/FXP;->a(Landroid/database/Cursor;LX/AU0;)V

    .line 2255407
    iget-object v0, p0, LX/FXm;->a:Lcom/facebook/saved2/ui/Saved2Fragment;

    .line 2255408
    iget-object v1, v0, Lcom/facebook/saved2/ui/Saved2Fragment;->b:LX/FWt;

    .line 2255409
    const-string p0, "DASH_TABS_LOAD_FROM_CACHE"

    invoke-static {v1, p0}, LX/FWt;->b(LX/FWt;Ljava/lang/String;)V

    .line 2255410
    iget-object v1, v0, Lcom/facebook/saved2/ui/Saved2Fragment;->a:LX/FXs;

    .line 2255411
    iget-boolean p0, v1, LX/FXs;->e:Z

    if-nez p0, :cond_0

    .line 2255412
    const/4 p0, 0x1

    iput-boolean p0, v1, LX/FXs;->e:Z

    .line 2255413
    iget-object p0, v1, LX/FXs;->a:LX/FXN;

    .line 2255414
    iget-object p1, p0, LX/FXN;->f:LX/FWt;

    .line 2255415
    const-string v1, "DASH_TABS_LOAD_FROM_NETWORK"

    invoke-static {p1, v1}, LX/FWt;->a(LX/FWt;Ljava/lang/String;)V

    .line 2255416
    new-instance p1, Lcom/facebook/saved2/network/Saved2DataFetcher$1;

    invoke-direct {p1, p0}, Lcom/facebook/saved2/network/Saved2DataFetcher$1;-><init>(LX/FXN;)V

    invoke-virtual {p1}, Lcom/facebook/saved2/network/Saved2DataFetcher$1;->start()V

    .line 2255417
    :cond_0
    iget-object v1, v0, Lcom/facebook/saved2/ui/Saved2Fragment;->t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    if-eqz v1, :cond_1

    .line 2255418
    iget-object v1, v0, Lcom/facebook/saved2/ui/Saved2Fragment;->t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    .line 2255419
    :cond_1
    invoke-static {v0}, Lcom/facebook/saved2/ui/Saved2Fragment;->e(Lcom/facebook/saved2/ui/Saved2Fragment;)V

    .line 2255420
    return-void
.end method
