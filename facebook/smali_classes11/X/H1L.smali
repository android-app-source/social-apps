.class public final LX/H1L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fig/textinput/FigEditText;

.field public final synthetic b:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:Landroid/view/ViewGroup;

.field public final synthetic e:LX/H0w;


# direct methods
.method public constructor <init>(LX/H0w;Lcom/facebook/fig/textinput/FigEditText;Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2415082
    iput-object p1, p0, LX/H1L;->e:LX/H0w;

    iput-object p2, p0, LX/H1L;->a:Lcom/facebook/fig/textinput/FigEditText;

    iput-object p3, p0, LX/H1L;->b:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iput-object p4, p0, LX/H1L;->c:Landroid/content/Context;

    iput-object p5, p0, LX/H1L;->d:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x1e815de

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2415083
    iget-object v1, p0, LX/H1L;->e:LX/H0w;

    iget-object v2, p0, LX/H1L;->a:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/H0w;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 2415084
    if-nez v1, :cond_0

    .line 2415085
    iget-object v1, p0, LX/H1L;->b:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    const-string v2, "Unable to parse value."

    invoke-virtual {v1, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v1

    invoke-virtual {v1}, LX/3oW;->b()V

    .line 2415086
    :goto_0
    const v1, -0x5bfb4dce

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2415087
    :cond_0
    iget-object v2, p0, LX/H1L;->b:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iget-object v3, p0, LX/H1L;->e:LX/H0w;

    invoke-virtual {v2, v3, v1}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;Ljava/lang/Object;)V

    .line 2415088
    iget-object v1, p0, LX/H1L;->b:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    const-string v2, "Override set! Restart the app for changes to take effect."

    invoke-virtual {v1, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v1

    invoke-virtual {v1}, LX/3oW;->b()V

    .line 2415089
    iget-object v1, p0, LX/H1L;->e:LX/H0w;

    iget-object v2, p0, LX/H1L;->c:Landroid/content/Context;

    iget-object v3, p0, LX/H1L;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3}, LX/H0w;->c(Landroid/content/Context;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method
