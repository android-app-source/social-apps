.class public final LX/GVx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GVw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GVw",
        "<",
        "LX/GWl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(Landroid/view/ViewGroup;LX/7iP;)LX/GWl;
    .locals 2

    .prologue
    .line 2360239
    new-instance v0, LX/GWl;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GWl;-><init>(Landroid/content/Context;)V

    .line 2360240
    iget-object v1, v0, LX/GWl;->b:LX/7iw;

    .line 2360241
    iput-object p1, v1, LX/7iw;->d:LX/7iP;

    .line 2360242
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360243
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    invoke-static {p1, v0}, LX/GVx;->b(Landroid/view/ViewGroup;LX/7iP;)LX/GWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;LX/7iP;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360244
    invoke-static {p1, p2}, LX/GVx;->b(Landroid/view/ViewGroup;LX/7iP;)LX/GWl;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 2360245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MERCHANT_PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-object v0
.end method

.method public final a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)Z
    .locals 1

    .prologue
    .line 2360246
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
