.class public LX/Fr0;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fr0;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294868
    const-string v0, "timeline"

    const/16 v1, 0x2e

    new-instance v2, LX/Fqz;

    invoke-direct {v2}, LX/Fqz;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 2294869
    return-void
.end method

.method public static a(LX/0QB;)LX/Fr0;
    .locals 3

    .prologue
    .line 2294870
    sget-object v0, LX/Fr0;->a:LX/Fr0;

    if-nez v0, :cond_1

    .line 2294871
    const-class v1, LX/Fr0;

    monitor-enter v1

    .line 2294872
    :try_start_0
    sget-object v0, LX/Fr0;->a:LX/Fr0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2294873
    if-eqz v2, :cond_0

    .line 2294874
    :try_start_1
    new-instance v0, LX/Fr0;

    invoke-direct {v0}, LX/Fr0;-><init>()V

    .line 2294875
    move-object v0, v0

    .line 2294876
    sput-object v0, LX/Fr0;->a:LX/Fr0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294877
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2294878
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2294879
    :cond_1
    sget-object v0, LX/Fr0;->a:LX/Fr0;

    return-object v0

    .line 2294880
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2294881
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
