.class public LX/GIZ;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336835
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336836
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2336831
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336832
    iput-object v0, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2336833
    iput-object v0, p0, LX/GIZ;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2336834
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336822
    check-cast p1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0, p1, p2}, LX/GIZ;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2336829
    iput-object p1, p0, LX/GIZ;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2336830
    return-void
.end method

.method public a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336823
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336824
    iput-object p1, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2336825
    iput-object p2, p0, LX/GIZ;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2336826
    iget-object p1, p0, LX/GHg;->b:LX/GCE;

    move-object p1, p1

    .line 2336827
    new-instance p2, LX/GIY;

    invoke-direct {p2, p0}, LX/GIY;-><init>(LX/GIZ;)V

    invoke-virtual {p1, p2}, LX/GCE;->a(LX/8wQ;)V

    .line 2336828
    return-void
.end method
