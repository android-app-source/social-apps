.class public final LX/FJS;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/momentsinvite/graphql/MomentsInviteActionPostbackMutationModels$MomentsInviteActionPostbackMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FJK;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/FJT;


# direct methods
.method public constructor <init>(LX/FJT;LX/FJK;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2223616
    iput-object p1, p0, LX/FJS;->c:LX/FJT;

    iput-object p2, p0, LX/FJS;->a:LX/FJK;

    iput-object p3, p0, LX/FJS;->b:Landroid/content/Context;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2223606
    const-class v0, LX/4bK;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2223607
    iget-object v0, p0, LX/FJS;->b:Landroid/content/Context;

    const v1, 0x7f0832c9

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2223608
    :goto_0
    iget-object v0, p0, LX/FJS;->a:LX/FJK;

    if-eqz v0, :cond_0

    .line 2223609
    iget-object v0, p0, LX/FJS;->a:LX/FJK;

    invoke-interface {v0}, LX/FJK;->b()V

    .line 2223610
    :cond_0
    return-void

    .line 2223611
    :cond_1
    iget-object v0, p0, LX/FJS;->c:LX/FJT;

    iget-object v0, v0, LX/FJT;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/FJT;->a:Ljava/lang/String;

    const-string v2, "Postback unknown failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2223612
    iget-object v0, p0, LX/FJS;->b:Landroid/content/Context;

    const v1, 0x7f0832c8

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2223613
    iget-object v0, p0, LX/FJS;->a:LX/FJK;

    if-eqz v0, :cond_0

    .line 2223614
    iget-object v0, p0, LX/FJS;->a:LX/FJK;

    invoke-interface {v0}, LX/FJK;->b()V

    .line 2223615
    :cond_0
    return-void
.end method
