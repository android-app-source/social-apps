.class public LX/Fsr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fq3;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FwJ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FqU;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FrD;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G0v;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FwI;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FvN;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G2c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Fq3;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/FwJ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FqU;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FwI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FrD;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G0v;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FvN;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G2c;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297670
    iput-object p1, p0, LX/Fsr;->a:LX/0Or;

    .line 2297671
    iput-object p2, p0, LX/Fsr;->b:LX/0Or;

    .line 2297672
    iput-object p4, p0, LX/Fsr;->c:LX/0Or;

    .line 2297673
    iput-object p5, p0, LX/Fsr;->d:LX/0Or;

    .line 2297674
    iput-object p3, p0, LX/Fsr;->e:LX/0Or;

    .line 2297675
    iput-object p6, p0, LX/Fsr;->f:LX/0Or;

    .line 2297676
    iput-object p7, p0, LX/Fsr;->g:LX/0Or;

    .line 2297677
    return-void
.end method

.method private a()LX/Fq3;
    .locals 1

    .prologue
    .line 2297666
    iget-object v0, p0, LX/Fsr;->h:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 2297667
    const/4 v0, 0x0

    .line 2297668
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Fsr;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fq3;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Fsr;
    .locals 11

    .prologue
    .line 2297655
    const-class v1, LX/Fsr;

    monitor-enter v1

    .line 2297656
    :try_start_0
    sget-object v0, LX/Fsr;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2297657
    sput-object v2, LX/Fsr;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2297658
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297659
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2297660
    new-instance v3, LX/Fsr;

    const/16 v4, 0x366f

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x361d

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x366e

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x3629

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x36a0

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x3667

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x36d1

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/Fsr;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2297661
    move-object v0, v3

    .line 2297662
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2297663
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fsr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297664
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2297665
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final A()LX/FvM;
    .locals 1

    .prologue
    .line 2297651
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297652
    if-nez v0, :cond_0

    .line 2297653
    iget-object v0, p0, LX/Fsr;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FvM;

    .line 2297654
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->A()LX/FvM;

    move-result-object v0

    goto :goto_0
.end method

.method public final B()LX/G2b;
    .locals 1

    .prologue
    .line 2297621
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297622
    if-nez v0, :cond_0

    .line 2297623
    iget-object v0, p0, LX/Fsr;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G2b;

    .line 2297624
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->B()LX/G2b;

    move-result-object v0

    goto :goto_0
.end method

.method public final E()V
    .locals 1

    .prologue
    .line 2297647
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297648
    if-nez v0, :cond_0

    .line 2297649
    :goto_0
    return-void

    .line 2297650
    :cond_0
    invoke-interface {v0}, LX/Fq3;->E()V

    goto :goto_0
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 2297643
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297644
    if-nez v0, :cond_0

    .line 2297645
    :goto_0
    return-void

    .line 2297646
    :cond_0
    invoke-interface {v0}, LX/Fq3;->F()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/ref/WeakReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Fq3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2297641
    iput-object p1, p0, LX/Fsr;->h:Ljava/lang/ref/WeakReference;

    .line 2297642
    return-void
.end method

.method public final e()LX/Fv9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2297678
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297679
    if-nez v0, :cond_0

    .line 2297680
    const/4 v0, 0x0

    .line 2297681
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->e()LX/Fv9;

    move-result-object v0

    goto :goto_0
.end method

.method public final lk_()V
    .locals 1

    .prologue
    .line 2297637
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297638
    if-nez v0, :cond_0

    .line 2297639
    :goto_0
    return-void

    .line 2297640
    :cond_0
    invoke-interface {v0}, LX/Fq3;->lk_()V

    goto :goto_0
.end method

.method public final ll_()V
    .locals 1

    .prologue
    .line 2297633
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297634
    if-nez v0, :cond_0

    .line 2297635
    :goto_0
    return-void

    .line 2297636
    :cond_0
    invoke-interface {v0}, LX/Fq3;->ll_()V

    goto :goto_0
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2297629
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297630
    if-nez v0, :cond_0

    .line 2297631
    :goto_0
    return-void

    .line 2297632
    :cond_0
    invoke-interface {v0}, LX/Fq3;->mJ_()V

    goto :goto_0
.end method

.method public final p()LX/FwE;
    .locals 1

    .prologue
    .line 2297625
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297626
    if-nez v0, :cond_0

    .line 2297627
    iget-object v0, p0, LX/Fsr;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwE;

    .line 2297628
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->p()LX/FwE;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()LX/FqT;
    .locals 1

    .prologue
    .line 2297617
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297618
    if-nez v0, :cond_0

    .line 2297619
    iget-object v0, p0, LX/Fsr;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FqT;

    .line 2297620
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->q()LX/FqT;

    move-result-object v0

    goto :goto_0
.end method

.method public final r()LX/FrC;
    .locals 1

    .prologue
    .line 2297613
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297614
    if-nez v0, :cond_0

    .line 2297615
    iget-object v0, p0, LX/Fsr;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FrC;

    .line 2297616
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->r()LX/FrC;

    move-result-object v0

    goto :goto_0
.end method

.method public final s()LX/G0u;
    .locals 1

    .prologue
    .line 2297609
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297610
    if-nez v0, :cond_0

    .line 2297611
    iget-object v0, p0, LX/Fsr;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G0u;

    .line 2297612
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->s()LX/G0u;

    move-result-object v0

    goto :goto_0
.end method

.method public final u()LX/Fw9;
    .locals 1

    .prologue
    .line 2297605
    invoke-direct {p0}, LX/Fsr;->a()LX/Fq3;

    move-result-object v0

    .line 2297606
    if-nez v0, :cond_0

    .line 2297607
    iget-object v0, p0, LX/Fsr;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fw9;

    .line 2297608
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/Fq3;->u()LX/Fw9;

    move-result-object v0

    goto :goto_0
.end method
