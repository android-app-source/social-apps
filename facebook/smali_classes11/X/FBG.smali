.class public final LX/FBG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;)V
    .locals 0

    .prologue
    .line 2208980
    iput-object p1, p0, LX/FBG;->a:Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x38b1b9e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2208981
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2208982
    const-string v2, "package:com.facebook.orca"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2208983
    iget-object v2, p0, LX/FBG;->a:Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;

    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 2208984
    iget-object v1, p0, LX/FBG;->a:Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;

    sget-object v2, LX/CIp;->A:Ljava/lang/String;

    .line 2208985
    iget-object v4, v1, Lcom/facebook/katana/orca/DiodeEnableMessengerFragment;->b:LX/0Zb;

    const-string p0, "click"

    const/4 p1, 0x1

    invoke-interface {v4, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2208986
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2208987
    const-string p0, "diode_qp_module"

    invoke-virtual {v4, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2208988
    const-string p0, "button"

    invoke-virtual {v4, p0}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2208989
    invoke-virtual {v4, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2208990
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2208991
    :cond_0
    const v1, 0x44f881dc

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
