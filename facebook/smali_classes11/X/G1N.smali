.class public LX/G1N;
.super LX/BPB;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BPB",
        "<",
        "LX/G1P;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2311079
    invoke-direct {p0}, LX/BPB;-><init>()V

    return-void
.end method


# virtual methods
.method public final d()Z
    .locals 1

    .prologue
    .line 2311080
    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    check-cast v0, LX/G1P;

    .line 2311081
    iget-object p0, v0, LX/G1P;->d:Ljava/lang/String;

    if-eqz p0, :cond_2

    iget-object p0, v0, LX/G1P;->b:LX/0am;

    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/G1P;->b:LX/0am;

    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;->a()I

    move-result p0

    if-lez p0, :cond_2

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2311082
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method
