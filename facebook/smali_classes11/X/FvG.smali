.class public LX/FvG;
.super LX/62C;
.source ""


# instance fields
.field public final a:LX/FvV;

.field private final b:LX/BQ1;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/BP9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/BQ1;LX/FvV;Ljava/util/List;)V
    .locals 1
    .param p1    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/FvV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BQ1;",
            "LX/FvV;",
            "Ljava/util/List",
            "<",
            "LX/1Cw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2301192
    const/4 v0, 0x0

    invoke-direct {p0, v0, p3}, LX/62C;-><init>(ZLjava/util/List;)V

    .line 2301193
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/FvG;->c:Ljava/util/Set;

    .line 2301194
    iput-object p1, p0, LX/FvG;->b:LX/BQ1;

    .line 2301195
    iput-object p2, p0, LX/FvG;->a:LX/FvV;

    .line 2301196
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2301197
    iget-object v0, p0, LX/FvG;->a:LX/FvV;

    .line 2301198
    iget-object p0, v0, LX/FvV;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    move-object v0, p0

    .line 2301199
    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301200
    invoke-super {p0, p1, p2, p3}, LX/62C;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2301201
    instance-of v0, v1, LX/BP9;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2301202
    check-cast v0, LX/BP9;

    .line 2301203
    iget-object v2, p0, LX/FvG;->c:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2301204
    iget-object v2, p0, LX/FvG;->c:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2301205
    :cond_0
    return-object v1
.end method
