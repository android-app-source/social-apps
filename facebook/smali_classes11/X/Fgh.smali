.class public LX/Fgh;
.super LX/FgU;
.source ""


# instance fields
.field private final b:LX/Fic;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FgX;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhD;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/Cwi;

.field private f:Ljava/lang/String;

.field private g:LX/Cvp;


# direct methods
.method public constructor <init>(LX/Fic;LX/0Ot;LX/0Ot;LX/Cwi;LX/Fgc;LX/7HW;LX/7Hk;LX/2Sd;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fic;",
            "LX/0Ot",
            "<",
            "LX/FgX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FhD;",
            ">;",
            "LX/Cwi;",
            "LX/Fgc;",
            "LX/7HW;",
            "LX/7Hk;",
            "LX/2Sd;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270729
    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, LX/FgU;-><init>(LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;)V

    .line 2270730
    iput-object p1, p0, LX/Fgh;->b:LX/Fic;

    .line 2270731
    iput-object p2, p0, LX/Fgh;->c:LX/0Ot;

    .line 2270732
    iput-object p3, p0, LX/Fgh;->d:LX/0Ot;

    .line 2270733
    iput-object p4, p0, LX/Fgh;->e:LX/Cwi;

    .line 2270734
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/7Hg;->g:Z

    .line 2270735
    return-void
.end method


# virtual methods
.method public final a(LX/7Hi;Ljava/lang/String;)LX/7HN;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/7HN",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2270736
    const/4 v0, 0x1

    .line 2270737
    iget-object v2, p0, LX/7HQ;->a:LX/7B6;

    invoke-static {v2}, LX/8ht;->a(LX/7B6;)Z

    move-result v2

    .line 2270738
    iget-object v3, p0, LX/7HQ;->a:LX/7B6;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2270739
    iget-object v6, p1, LX/7Hi;->b:LX/7Hc;

    move-object v6, v6

    .line 2270740
    iget-object v7, v6, LX/7Hc;->b:LX/0Px;

    move-object v6, v7

    .line 2270741
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2270742
    :cond_0
    :goto_0
    move v3, v4

    .line 2270743
    if-nez v3, :cond_1

    move v0, v1

    .line 2270744
    :cond_1
    iget-object v3, p0, LX/Fgh;->f:Ljava/lang/String;

    invoke-static {p2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2270745
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 2270746
    new-instance v0, LX/7HU;

    iget-object v1, p0, LX/Fgh;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/Fgh;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7HU;-><init>(LX/0Px;)V

    .line 2270747
    :goto_2
    return-object v0

    .line 2270748
    :cond_2
    if-eqz v1, :cond_3

    .line 2270749
    iget-object v0, p0, LX/Fgh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HN;

    goto :goto_2

    .line 2270750
    :cond_3
    if-eqz v2, :cond_4

    .line 2270751
    iget-object v0, p0, LX/Fgh;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HN;

    goto :goto_2

    .line 2270752
    :cond_4
    sget-object v0, LX/7Hb;->a:LX/7Hb;

    move-object v0, v0

    .line 2270753
    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_1

    .line 2270754
    :cond_6
    iget-object v6, v3, LX/7B6;->b:Ljava/lang/String;

    .line 2270755
    iget-object v7, p1, LX/7Hi;->a:LX/7B6;

    move-object v7, v7

    .line 2270756
    iget-object v7, v7, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    move v4, v5

    .line 2270757
    goto :goto_0

    .line 2270758
    :cond_7
    iget-object v6, p1, LX/7Hi;->c:LX/7HY;

    move-object v6, v6

    .line 2270759
    sget-object v7, LX/7HY;->REMOTE:LX/7HY;

    if-eq v6, v7, :cond_0

    .line 2270760
    iget-object v6, p1, LX/7Hi;->c:LX/7HY;

    move-object v6, v6

    .line 2270761
    sget-object v7, LX/7HY;->MEMORY_CACHE:LX/7HY;

    if-ne v6, v7, :cond_8

    .line 2270762
    iget-object v6, p1, LX/7Hi;->d:LX/7Ha;

    move-object v6, v6

    .line 2270763
    sget-object v7, LX/7Ha;->EXACT:LX/7Ha;

    if-eq v6, v7, :cond_0

    :cond_8
    move v4, v5

    .line 2270764
    goto :goto_0
.end method

.method public final a(LX/7B6;)V
    .locals 1

    .prologue
    .line 2270765
    iget-object v0, p1, LX/7B6;->b:Ljava/lang/String;

    iput-object v0, p0, LX/Fgh;->f:Ljava/lang/String;

    .line 2270766
    invoke-super {p0, p1}, LX/FgU;->a(LX/7B6;)V

    .line 2270767
    return-void
.end method

.method public final a(LX/Cvp;)V
    .locals 1

    .prologue
    .line 2270768
    iput-object p1, p0, LX/Fgh;->g:LX/Cvp;

    .line 2270769
    iget-object v0, p0, LX/Fgh;->b:LX/Fic;

    .line 2270770
    iget-object p0, v0, LX/Fic;->c:LX/Cwe;

    invoke-interface {p0, p1}, LX/Cwe;->a(LX/Cvp;)V

    .line 2270771
    return-void
.end method

.method public final b(LX/7Hi;Ljava/lang/String;)LX/7Hi;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v2, 0xc

    .line 2270772
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 2270773
    sget-object v1, LX/7HY;->LOCAL:LX/7HY;

    if-ne v0, v1, :cond_0

    .line 2270774
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 2270775
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v1, v1

    .line 2270776
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 2270777
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 2270778
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    .line 2270779
    new-instance v0, LX/7Hi;

    .line 2270780
    iget-object v2, p1, LX/7Hi;->a:LX/7B6;

    move-object v2, v2

    .line 2270781
    new-instance v3, LX/7Hc;

    .line 2270782
    iget-object v4, p1, LX/7Hi;->b:LX/7Hc;

    move-object v4, v4

    .line 2270783
    iget v5, v4, LX/7Hc;->c:I

    move v4, v5

    .line 2270784
    iget-object v5, p1, LX/7Hi;->b:LX/7Hc;

    move-object v5, v5

    .line 2270785
    iget v6, v5, LX/7Hc;->d:I

    move v5, v6

    .line 2270786
    invoke-direct {v3, v1, v4, v5}, LX/7Hc;-><init>(LX/0Px;II)V

    .line 2270787
    iget-object v1, p1, LX/7Hi;->c:LX/7HY;

    move-object v1, v1

    .line 2270788
    iget-object v4, p1, LX/7Hi;->d:LX/7Ha;

    move-object v4, v4

    .line 2270789
    invoke-direct {v0, v2, v3, v1, v4}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    move-object p1, v0

    .line 2270790
    :cond_0
    invoke-super {p0, p1, p2}, LX/FgU;->b(LX/7Hi;Ljava/lang/String;)LX/7Hi;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/7Hi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270791
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 2270792
    sget-object v1, LX/7HY;->MEMORY_CACHE:LX/7HY;

    if-ne v0, v1, :cond_0

    .line 2270793
    iget-object v0, p0, LX/Fgh;->e:LX/Cwi;

    .line 2270794
    iget-object v1, p1, LX/7Hi;->b:LX/7Hc;

    move-object v1, v1

    .line 2270795
    iget-object v2, v1, LX/7Hc;->b:LX/0Px;

    move-object v1, v2

    .line 2270796
    iget-object v2, v0, LX/Cwi;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2270797
    :cond_0
    invoke-super {p0, p1}, LX/FgU;->d(LX/7Hi;)V

    .line 2270798
    return-void
.end method

.method public final f()LX/0P2;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270799
    invoke-super {p0}, LX/FgU;->f()LX/0P2;

    move-result-object v0

    .line 2270800
    iget-object v1, p0, LX/Fgh;->g:LX/Cvp;

    if-eqz v1, :cond_0

    .line 2270801
    const-string v1, "typeahead_sid"

    iget-object v2, p0, LX/Fgh;->g:LX/Cvp;

    invoke-virtual {v2}, LX/Cvp;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2270802
    :cond_0
    return-object v0
.end method
