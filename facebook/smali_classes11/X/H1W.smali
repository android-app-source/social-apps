.class public final LX/H1W;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:LX/H1X;


# direct methods
.method public constructor <init>(LX/H1X;)V
    .locals 0

    .prologue
    .line 2415438
    iput-object p1, p0, LX/H1W;->a:LX/H1X;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6

    .prologue
    .line 2415439
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 2415440
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2415441
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2415442
    iget-object v0, p0, LX/H1W;->a:LX/H1X;

    iget-object v0, v0, LX/H1X;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0v;

    .line 2415443
    invoke-virtual {v0, v2}, LX/H0v;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2415444
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2415445
    :cond_1
    iput-object v3, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 2415446
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 2415447
    return-object v1
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 2415448
    iget-object v1, p0, LX/H1W;->a:LX/H1X;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 2415449
    iput-object v0, v1, LX/H1X;->b:Ljava/util/List;

    .line 2415450
    iget-object v0, p0, LX/H1W;->a:LX/H1X;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2415451
    return-void
.end method
