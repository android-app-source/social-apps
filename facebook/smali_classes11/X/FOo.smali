.class public final LX/FOo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Uu;


# direct methods
.method public constructor <init>(LX/2Uu;)V
    .locals 0

    .prologue
    .line 2235893
    iput-object p1, p0, LX/FOo;->a:LX/2Uu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2235894
    :try_start_0
    iget-object v0, p0, LX/FOo;->a:LX/2Uu;

    iget-object v0, v0, LX/2Uu;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOp;

    .line 2235895
    iget-object v2, v0, LX/FOp;->b:LX/6cy;

    sget-object v3, LX/6cx;->f:LX/2bA;

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, LX/48u;->a(LX/0To;J)J

    move-result-wide v2

    .line 2235896
    invoke-static {v0, v2, v3}, LX/FOp;->a(LX/FOp;J)Landroid/util/Pair;

    move-result-object v3

    .line 2235897
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2235898
    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, LX/0Px;

    .line 2235899
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2235900
    iget-object v3, v0, LX/FOp;->b:LX/6cy;

    sget-object v6, LX/6cx;->f:LX/2bA;

    invoke-virtual {v3, v6, v4, v5}, LX/48u;->b(LX/0To;J)V

    .line 2235901
    iget-object v3, v0, LX/FOp;->a:LX/3N0;

    invoke-virtual {v3, v2}, LX/3N0;->a(Ljava/util/List;)V

    .line 2235902
    iget-object v3, v0, LX/FOp;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2Oe;

    .line 2235903
    iget-object v4, v3, LX/2Oe;->l:LX/2Oi;

    invoke-virtual {v4, v2}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 2235904
    iget-object v4, v3, LX/2Oe;->b:LX/2Of;

    invoke-virtual {v4}, LX/2Of;->a()V

    .line 2235905
    iget-object v2, v0, LX/FOp;->c:LX/2Ow;

    invoke-virtual {v2}, LX/2Ow;->a()V

    .line 2235906
    :cond_0
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2235907
    :goto_0
    return-object v0

    :catch_0
    new-instance v0, LX/2YS;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    goto :goto_0
.end method
