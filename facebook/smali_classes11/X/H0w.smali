.class public abstract LX/H0w;
.super LX/H0v;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ParamType:",
        "Ljava/lang/Object;",
        ">",
        "LX/H0v;"
    }
.end annotation


# instance fields
.field public f:Ljava/lang/String;

.field public g:I

.field public h:J

.field public i:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TParamType;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H1U;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TParamType;",
            "Ljava/util/EnumSet",
            "<",
            "LX/H1O;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V
    .locals 1

    .prologue
    .line 2414513
    invoke-direct {p0, p3}, LX/H0v;-><init>(Ljava/lang/String;)V

    .line 2414514
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/H0w;->j:Ljava/util/List;

    .line 2414515
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/H0w;->l:Z

    .line 2414516
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    .line 2414517
    iput-wide p1, p0, LX/H0w;->h:J

    .line 2414518
    iput-object p4, p0, LX/H0w;->f:Ljava/lang/String;

    .line 2414519
    iput p5, p0, LX/H0w;->g:I

    .line 2414520
    const-string v0, "Param"

    iput-object v0, p0, LX/H0w;->c:Ljava/lang/CharSequence;

    .line 2414521
    const-string v0, "Config"

    iput-object v0, p0, LX/H0w;->d:Ljava/lang/CharSequence;

    .line 2414522
    invoke-virtual {p0, p6}, LX/H0w;->a(LX/4gq;)V

    .line 2414523
    return-void
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)LX/H0w;
    .locals 8

    .prologue
    .line 2414524
    sget-object v0, LX/H1N;->a:[I

    invoke-static {p0, p1}, LX/0X6;->c(J)LX/0oE;

    move-result-object v1

    invoke-virtual {v1}, LX/0oE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2414525
    new-instance v1, LX/H1Y;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/H1Y;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    :goto_0
    return-object v1

    .line 2414526
    :pswitch_0
    new-instance v1, LX/H0x;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/H0x;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    goto :goto_0

    .line 2414527
    :pswitch_1
    new-instance v1, LX/H0y;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/H0y;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    goto :goto_0

    .line 2414528
    :pswitch_2
    new-instance v1, LX/H10;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/H10;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    goto :goto_0

    .line 2414529
    :pswitch_3
    new-instance v1, LX/H1Y;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/H1Y;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/Object;LX/H1O;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TParamType;",
            "LX/H1O;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2414530
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2414531
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2414532
    :goto_0
    return-void

    .line 2414533
    :cond_0
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    invoke-virtual {v0, p2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2414534
    move-object v0, p1

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2414535
    invoke-virtual {v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030b25

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2414536
    const v1, 0x7f0d1c14

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2414537
    invoke-virtual {v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f030b24

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2414538
    invoke-virtual {p0, p1, v1}, LX/H0w;->c(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 2414539
    return-object v2
.end method

.method public abstract a(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TParamType;"
        }
    .end annotation
.end method

.method public a(LX/4gq;)V
    .locals 2

    .prologue
    .line 2414540
    invoke-virtual {p0, p1}, LX/H0w;->c(LX/4gq;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/H0w;->i:Ljava/lang/Object;

    .line 2414541
    invoke-virtual {p0, p1}, LX/H0w;->b(LX/4gq;)Z

    move-result v0

    iput-boolean v0, p0, LX/H0w;->k:Z

    .line 2414542
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2414543
    invoke-virtual {p0, p1}, LX/H0w;->g(LX/4gq;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/H1O;->DEFAULT:LX/H1O;

    invoke-direct {p0, v0, v1}, LX/H0w;->a(Ljava/lang/Object;LX/H1O;)V

    .line 2414544
    invoke-virtual {p0, p1}, LX/H0w;->f(LX/4gq;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/H1O;->CACHED:LX/H1O;

    invoke-direct {p0, v0, v1}, LX/H0w;->a(Ljava/lang/Object;LX/H1O;)V

    .line 2414545
    invoke-virtual {p0, p1}, LX/H0w;->e(LX/4gq;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/H1O;->LATEST:LX/H1O;

    invoke-direct {p0, v0, v1}, LX/H0w;->a(Ljava/lang/Object;LX/H1O;)V

    .line 2414546
    invoke-virtual {p0, p1}, LX/H0w;->b(LX/4gq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414547
    invoke-virtual {p0, p1}, LX/H0w;->d(LX/4gq;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/H1O;->OVERRIDE:LX/H1O;

    invoke-direct {p0, v0, v1}, LX/H0w;->a(Ljava/lang/Object;LX/H1O;)V

    .line 2414548
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 11

    .prologue
    const/16 v7, 0x8

    .line 2414549
    move-object v3, p1

    check-cast v3, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2414550
    const v0, 0x7f0d1c10

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2414551
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2414552
    iget-object v1, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2414553
    new-instance v4, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v5, 0x2

    invoke-direct {v4, p1, v5}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 2414554
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414555
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LX/H0w;->i:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2414556
    const-string v5, "\u2713"

    invoke-virtual {v4, v5}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2414557
    :cond_0
    const-string v5, ", "

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/EnumSet;

    .line 2414558
    invoke-virtual {v1}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2414559
    const-string v6, ""

    .line 2414560
    :goto_1
    move-object v1, v6

    .line 2414561
    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2414562
    new-instance v1, LX/H1K;

    invoke-direct {v1, p0, v3, p1, p2}, LX/H1K;-><init>(LX/H0w;Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;Landroid/content/Context;Landroid/view/ViewGroup;)V

    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414563
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2414564
    :cond_1
    const v0, 0x7f0d1c11

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2414565
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Value on next restart: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/H0w;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414566
    const v0, 0x7f0d1c12

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fig/textinput/FigEditText;

    .line 2414567
    const v0, 0x7f0d1c13

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/fig/button/FigButton;

    .line 2414568
    iget-boolean v0, p0, LX/H0w;->l:Z

    if-eqz v0, :cond_2

    .line 2414569
    invoke-virtual {v2, v7}, Lcom/facebook/fig/textinput/FigEditText;->setVisibility(I)V

    .line 2414570
    invoke-virtual {v6, v7}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2414571
    :goto_2
    const v0, 0x7f0d1c09

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2414572
    new-instance v1, LX/H1M;

    invoke-direct {v1, p0, v3, p1, p2}, LX/H1M;-><init>(LX/H0w;Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;Landroid/content/Context;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414573
    iget-boolean v1, p0, LX/H0w;->k:Z

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2414574
    return-void

    .line 2414575
    :cond_2
    new-instance v0, LX/H1L;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/H1L;-><init>(LX/H0w;Lcom/facebook/fig/textinput/FigEditText;Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;Landroid/content/Context;Landroid/view/ViewGroup;)V

    invoke-virtual {v6, v0}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2414576
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2414577
    invoke-virtual {v1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 2414578
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2414579
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2414580
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2414581
    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1
.end method

.method public b(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2414582
    move-object v0, p1

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2414583
    const v1, 0x7f0d1c15

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2414584
    invoke-virtual {p0}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414585
    const v1, 0x7f0d1c16

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2414586
    invoke-virtual {p0}, LX/H0v;->d()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414587
    const v1, 0x7f0d1c17

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2414588
    invoke-virtual {p0}, LX/H0v;->f()Ljava/lang/CharSequence;

    move-result-object v2

    .line 2414589
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2414590
    invoke-virtual {v1, v5}, Lcom/facebook/fig/listitem/FigListItem;->setVisibility(I)V

    .line 2414591
    :goto_0
    const v1, 0x7f0d1c18

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2414592
    iget-object v2, p0, LX/H0w;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2414593
    invoke-virtual {v1, v5}, Lcom/facebook/fig/listitem/FigListItem;->setVisibility(I)V

    .line 2414594
    :goto_1
    return-void

    .line 2414595
    :cond_0
    invoke-virtual {p0}, LX/H0v;->f()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2414596
    :cond_1
    iget-object v2, p0, LX/H0w;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 2414597
    iget-object v2, p0, LX/H0w;->j:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/H1U;

    .line 2414598
    invoke-virtual {v2}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414599
    iget-object v3, v2, LX/H1U;->a:LX/H1b;

    invoke-virtual {v3}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2414600
    new-instance v3, LX/H1I;

    invoke-direct {v3, p0, v0, v2, p1}, LX/H1I;-><init>(LX/H0w;Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;LX/H1U;Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2414601
    :cond_2
    iget-object v2, p0, LX/H0w;->j:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/H1U;

    iget-object v2, v2, LX/H1U;->a:LX/H1b;

    .line 2414602
    invoke-virtual {v2}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414603
    new-instance v3, LX/H1J;

    invoke-direct {v3, p0, v0, v2, p1}, LX/H1J;-><init>(LX/H0w;Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;LX/H1b;Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public abstract b(LX/4gq;)Z
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2414512
    invoke-super {p0, p1}, LX/H0v;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/H0w;->f:Ljava/lang/String;

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c(LX/4gq;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4gq;",
            ")TParamType;"
        }
    .end annotation
.end method

.method public final c(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2414509
    invoke-virtual {p0, p1, p2}, LX/H0w;->b(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 2414510
    invoke-virtual {p0, p1, p2}, LX/H0w;->a(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 2414511
    return-void
.end method

.method public abstract d(LX/4gq;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4gq;",
            ")TParamType;"
        }
    .end annotation
.end method

.method public abstract e(LX/4gq;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4gq;",
            ")TParamType;"
        }
    .end annotation
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2414508
    iget-object v0, p0, LX/H0w;->f:Ljava/lang/String;

    return-object v0
.end method

.method public abstract f(LX/4gq;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4gq;",
            ")TParamType;"
        }
    .end annotation
.end method

.method public abstract g(LX/4gq;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4gq;",
            ")TParamType;"
        }
    .end annotation
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2414507
    iget-object v0, p0, LX/H0w;->i:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
