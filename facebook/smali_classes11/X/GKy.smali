.class public LX/GKy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/01T;

.field public final b:LX/2U3;

.field public final c:LX/17Y;

.field public final d:LX/0ad;

.field private final e:Z


# direct methods
.method public constructor <init>(LX/0Uh;LX/2U3;LX/17Y;LX/01T;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2341875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2341876
    iput-object p2, p0, LX/GKy;->b:LX/2U3;

    .line 2341877
    iput-object p3, p0, LX/GKy;->c:LX/17Y;

    .line 2341878
    iput-object p4, p0, LX/GKy;->a:LX/01T;

    .line 2341879
    iput-object p5, p0, LX/GKy;->d:LX/0ad;

    .line 2341880
    const/16 v0, 0x5c6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/GKy;->e:Z

    .line 2341881
    return-void
.end method

.method public static b(LX/0QB;)LX/GKy;
    .locals 6

    .prologue
    .line 2341862
    new-instance v0, LX/GKy;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v2

    check-cast v2, LX/2U3;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct/range {v0 .. v5}, LX/GKy;-><init>(LX/0Uh;LX/2U3;LX/17Y;LX/01T;LX/0ad;)V

    .line 2341863
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2341867
    invoke-virtual {p0}, LX/GKy;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2341868
    :goto_0
    return-object v0

    .line 2341869
    :cond_0
    const-string v1, "/adsmanager/%s/detailed_targeting/%s/%s/%s/%s/%s/%s?isModal=%s"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v5

    aput-object p3, v2, v6

    aput-object p4, v2, v7

    const/4 v3, 0x3

    const-string v4, "AdInterfacesModule"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "onDetailedTargetingSelected"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object p5, v2, v3

    const/4 v3, 0x6

    aput-object p6, v2, v3

    const/4 v3, 0x7

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2341870
    sget-object v2, LX/0ax;->t:Ljava/lang/String;

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "LWI"

    aput-object v4, v3, v5

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2341871
    iget-object v1, p0, LX/GKy;->c:LX/17Y;

    invoke-interface {v1, p1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2341872
    if-nez v1, :cond_1

    .line 2341873
    iget-object v1, p0, LX/GKy;->b:LX/2U3;

    const-class v3, LX/GKy;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Got null intent for uri: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2341874
    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2341866
    iget-object v0, p0, LX/GKy;->a:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, LX/GKy;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2341865
    iget-object v0, p0, LX/GKy;->a:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2341864
    iget-object v1, p0, LX/GKy;->a:LX/01T;

    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/GKy;->d:LX/0ad;

    sget-short v2, LX/GDK;->d:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
