.class public final enum LX/GNz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GNz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GNz;

.field public static final enum FETCH_PREPAY_DATA:LX/GNz;

.field public static final enum SAVE_CARD:LX/GNz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2346691
    new-instance v0, LX/GNz;

    const-string v1, "SAVE_CARD"

    invoke-direct {v0, v1, v2}, LX/GNz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GNz;->SAVE_CARD:LX/GNz;

    .line 2346692
    new-instance v0, LX/GNz;

    const-string v1, "FETCH_PREPAY_DATA"

    invoke-direct {v0, v1, v3}, LX/GNz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GNz;->FETCH_PREPAY_DATA:LX/GNz;

    .line 2346693
    const/4 v0, 0x2

    new-array v0, v0, [LX/GNz;

    sget-object v1, LX/GNz;->SAVE_CARD:LX/GNz;

    aput-object v1, v0, v2

    sget-object v1, LX/GNz;->FETCH_PREPAY_DATA:LX/GNz;

    aput-object v1, v0, v3

    sput-object v0, LX/GNz;->$VALUES:[LX/GNz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2346694
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GNz;
    .locals 1

    .prologue
    .line 2346695
    const-class v0, LX/GNz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GNz;

    return-object v0
.end method

.method public static values()[LX/GNz;
    .locals 1

    .prologue
    .line 2346696
    sget-object v0, LX/GNz;->$VALUES:[LX/GNz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GNz;

    return-object v0
.end method
