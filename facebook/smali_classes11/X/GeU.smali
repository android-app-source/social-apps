.class public LX/GeU;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GeS;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GeV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2375717
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/GeU;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/GeV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375714
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2375715
    iput-object p1, p0, LX/GeU;->b:LX/0Ot;

    .line 2375716
    return-void
.end method

.method public static a(LX/0QB;)LX/GeU;
    .locals 4

    .prologue
    .line 2375718
    const-class v1, LX/GeU;

    monitor-enter v1

    .line 2375719
    :try_start_0
    sget-object v0, LX/GeU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375720
    sput-object v2, LX/GeU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375721
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375722
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375723
    new-instance v3, LX/GeU;

    const/16 p0, 0x20fe

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GeU;-><init>(LX/0Ot;)V

    .line 2375724
    move-object v0, v3

    .line 2375725
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375726
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GeU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375727
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375728
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2375690
    check-cast p2, LX/GeT;

    .line 2375691
    iget-object v0, p0, LX/GeU;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GeV;

    iget-object v1, p2, LX/GeT;->a:Lcom/facebook/java2js/JSValue;

    iget-object v2, p2, LX/GeT;->b:LX/5KI;

    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x6

    const/4 v8, 0x2

    .line 2375692
    const-string v3, "secondaryActionsComponent"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v3

    .line 2375693
    if-nez v3, :cond_2

    move-object v3, v4

    .line 2375694
    :goto_0
    const-string v5, "truncationMode"

    invoke-virtual {v1, v5}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v5

    .line 2375695
    const-string v6, "type"

    invoke-virtual {v5, v6}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2375696
    const-string v7, "max-lines"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "limit"

    invoke-virtual {v5, v7}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/java2js/JSValue;->asNumber()D

    .line 2375697
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    const v7, 0x7f0b0917

    invoke-interface {v5, v10, v7}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    const-string v8, "explanation"

    invoke-virtual {v1, v8}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v8

    const-string v9, "trackingCodes"

    invoke-virtual {v1, v9}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v9

    .line 2375698
    invoke-virtual {v8}, Lcom/facebook/java2js/JSValue;->isString()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2375699
    invoke-virtual {v8}, Lcom/facebook/java2js/JSValue;->asString()Ljava/lang/String;

    move-result-object v12

    .line 2375700
    :goto_1
    move-object v8, v12

    .line 2375701
    invoke-virtual {v7, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v11}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v7

    const-string v8, "max-lines"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    :cond_1
    invoke-virtual {v7, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b0050

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v4, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v11, v10}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    const/4 v6, 0x3

    invoke-interface {v4, v6, v10}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2375702
    return-object v0

    .line 2375703
    :cond_2
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x0

    const v6, 0x7f0b00eb

    invoke-interface {v3, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    goto/16 :goto_0

    .line 2375704
    :cond_3
    invoke-virtual {v8}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v12

    if-nez v12, :cond_4

    .line 2375705
    const-string v12, ""

    goto :goto_1

    .line 2375706
    :cond_4
    const-class v12, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v8, v12}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2375707
    if-nez v12, :cond_5

    .line 2375708
    const-string v12, ""

    goto :goto_1

    .line 2375709
    :cond_5
    const-class p0, LX/162;

    invoke-virtual {v9, p0}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/162;

    .line 2375710
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object p2

    .line 2375711
    iget-object p1, v0, LX/GeV;->a:LX/1Uf;

    invoke-static {v12}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v1

    sget-object v2, LX/1eK;->SUBTITLE:LX/1eK;

    invoke-virtual {p1, v1, v2, p2, p0}, LX/1Uf;->a(LX/1eE;LX/1eK;Landroid/text/Spannable;LX/0lF;)V

    .line 2375712
    iget-object p1, v0, LX/GeV;->a:LX/1Uf;

    invoke-static {v12}, LX/1eD;->d(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1y8;

    move-result-object v12

    invoke-virtual {p1, v12, p2, p0}, LX/1Uf;->a(LX/1y8;Landroid/text/Spannable;LX/0lF;)V

    move-object v12, p2

    .line 2375713
    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2375688
    invoke-static {}, LX/1dS;->b()V

    .line 2375689
    const/4 v0, 0x0

    return-object v0
.end method
