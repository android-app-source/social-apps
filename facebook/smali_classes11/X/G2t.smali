.class public final LX/G2t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pc;

.field public final synthetic b:LX/2en;

.field public final synthetic c:LX/G2v;

.field public final synthetic d:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;LX/1Pc;LX/2en;LX/G2v;)V
    .locals 0

    .prologue
    .line 2314594
    iput-object p1, p0, LX/G2t;->d:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;

    iput-object p2, p0, LX/G2t;->a:LX/1Pc;

    iput-object p3, p0, LX/G2t;->b:LX/2en;

    iput-object p4, p0, LX/G2t;->c:LX/G2v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const v0, 0x21e21d77

    invoke-static {v9, v8, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2314595
    iget-object v0, p0, LX/G2t;->a:LX/1Pc;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/G2t;->b:LX/2en;

    iget-object v2, p0, LX/G2t;->c:LX/G2v;

    iget-object v2, v2, LX/G2v;->d:LX/0jW;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/2ep;

    .line 2314596
    iget-object v0, v6, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_0

    .line 2314597
    iget-object v0, p0, LX/G2t;->a:LX/1Pc;

    check-cast v0, LX/1Pr;

    new-instance v1, LX/DEo;

    iget-object v2, p0, LX/G2t;->c:LX/G2v;

    iget-object v2, v2, LX/G2v;->d:LX/0jW;

    invoke-interface {v2}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/DEo;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2314598
    :cond_0
    iget-object v0, p0, LX/G2t;->a:LX/1Pc;

    iget-object v1, p0, LX/G2t;->c:LX/G2v;

    iget-object v1, v1, LX/G2v;->a:Ljava/lang/String;

    iget-object v2, p0, LX/G2t;->c:LX/G2v;

    iget-object v2, v2, LX/G2v;->b:Ljava/lang/String;

    sget-object v3, LX/2h7;->PYMK_TIMELINE:LX/2h7;

    iget-object v4, v6, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    new-instance v5, LX/G2s;

    invoke-direct {v5, p0, v6}, LX/G2s;-><init>(LX/G2t;LX/2ep;)V

    invoke-interface/range {v0 .. v5}, LX/1Pc;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v1

    .line 2314599
    iget-object v0, p0, LX/G2t;->a:LX/1Pc;

    check-cast v0, LX/1Pr;

    iget-object v2, p0, LX/G2t;->b:LX/2en;

    new-instance v3, LX/2ep;

    iget-object v4, v1, LX/5Oh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-boolean v1, v1, LX/5Oh;->b:Z

    invoke-direct {v3, v4, v1}, LX/2ep;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-interface {v0, v2, v3}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2314600
    iget-object v0, p0, LX/G2t;->a:LX/1Pc;

    check-cast v0, LX/1Pq;

    new-array v1, v8, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/G2t;->c:LX/G2v;

    iget-object v3, v3, LX/G2v;->d:LX/0jW;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2314601
    const v0, -0x4f2d1f2a

    invoke-static {v9, v9, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
