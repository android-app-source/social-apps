.class public final enum LX/Gjp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gjp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gjp;

.field public static final enum ERROR:LX/Gjp;

.field public static final enum LOADING:LX/Gjp;

.field public static final enum LOAD_COMPLETE:LX/Gjp;

.field public static final enum NO_GREETING_CARD:LX/Gjp;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2388550
    new-instance v0, LX/Gjp;

    const-string v1, "NO_GREETING_CARD"

    invoke-direct {v0, v1, v2}, LX/Gjp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gjp;->NO_GREETING_CARD:LX/Gjp;

    .line 2388551
    new-instance v0, LX/Gjp;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/Gjp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gjp;->LOADING:LX/Gjp;

    .line 2388552
    new-instance v0, LX/Gjp;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/Gjp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gjp;->ERROR:LX/Gjp;

    .line 2388553
    new-instance v0, LX/Gjp;

    const-string v1, "LOAD_COMPLETE"

    invoke-direct {v0, v1, v5}, LX/Gjp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gjp;->LOAD_COMPLETE:LX/Gjp;

    .line 2388554
    const/4 v0, 0x4

    new-array v0, v0, [LX/Gjp;

    sget-object v1, LX/Gjp;->NO_GREETING_CARD:LX/Gjp;

    aput-object v1, v0, v2

    sget-object v1, LX/Gjp;->LOADING:LX/Gjp;

    aput-object v1, v0, v3

    sget-object v1, LX/Gjp;->ERROR:LX/Gjp;

    aput-object v1, v0, v4

    sget-object v1, LX/Gjp;->LOAD_COMPLETE:LX/Gjp;

    aput-object v1, v0, v5

    sput-object v0, LX/Gjp;->$VALUES:[LX/Gjp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2388549
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gjp;
    .locals 1

    .prologue
    .line 2388555
    const-class v0, LX/Gjp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gjp;

    return-object v0
.end method

.method public static values()[LX/Gjp;
    .locals 1

    .prologue
    .line 2388548
    sget-object v0, LX/Gjp;->$VALUES:[LX/Gjp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gjp;

    return-object v0
.end method
