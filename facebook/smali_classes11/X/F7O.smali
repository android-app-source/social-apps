.class public final LX/F7O;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 0

    .prologue
    .line 2201749
    iput-object p1, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 8

    .prologue
    .line 2201750
    iget-object v0, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v2}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    iget-object v4, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v4, v4, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    .line 2201751
    iget-object v5, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v6, LX/9Tj;->PYMK_FETCH_FAILED:LX/9Tj;

    invoke-virtual {v6}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "ci_flow"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "time_since_creation"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "fetched_candidates_size"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201752
    iget-object v0, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-eq v0, v1, :cond_0

    .line 2201753
    iget-object v0, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    const/4 v1, 0x0

    .line 2201754
    iput-boolean v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->L:Z

    .line 2201755
    :cond_0
    iget-object v0, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2201756
    iget-object v0, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2201757
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2201758
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2201759
    iget-object v1, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 2201760
    :goto_0
    iget-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v9

    .line 2201761
    if-nez v0, :cond_2

    .line 2201762
    iget-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v3, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v3, v3, LX/89v;->value:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v4

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->v(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v6

    const/4 v8, -0x1

    invoke-virtual/range {v2 .. v9}, LX/9Tk;->a(Ljava/lang/String;JJII)V

    .line 2201763
    :cond_0
    :goto_1
    iget-object v0, p0, LX/F7O;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    .line 2201764
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->C:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2201765
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2201766
    return-void

    .line 2201767
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;

    goto :goto_0

    .line 2201768
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->a()Ljava/util/List;

    move-result-object v10

    .line 2201769
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v8

    .line 2201770
    iget-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v3, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v3, v3, LX/89v;->value:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v4

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->v(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v6

    invoke-virtual/range {v2 .. v9}, LX/9Tk;->a(Ljava/lang/String;JJII)V

    .line 2201771
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2201772
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 2201773
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;

    .line 2201774
    iget-wide v6, v2, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 2201775
    iget-object v6, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 2201776
    invoke-static {v2}, LX/F7L;->b(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;)LX/F7L;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2201777
    iget-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2201778
    :cond_4
    iget-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v2, v3}, LX/F73;->b(Ljava/util/List;)V

    goto :goto_1
.end method
