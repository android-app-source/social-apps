.class public abstract LX/GJH;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "LX/GJD;",
        "D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "LX/GHg",
        "<TV;TD;>;"
    }
.end annotation


# instance fields
.field public d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

.field public e:Landroid/view/inputmethod/InputMethodManager;

.field public f:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    .prologue
    .line 2338079
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2338080
    iput-object p1, p0, LX/GJH;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 2338081
    return-void
.end method

.method private a(LX/GMA;)V
    .locals 2

    .prologue
    .line 2338140
    sget-object v0, LX/GM9;->b:[I

    invoke-virtual {p1}, LX/GMA;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2338141
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setHint(Ljava/lang/CharSequence;)V

    .line 2338142
    :goto_0
    return-void

    .line 2338143
    :pswitch_0
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {p0}, LX/GJH;->w()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2338144
    :pswitch_1
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {p0}, LX/GJH;->x()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/GMB;
    .locals 2

    .prologue
    .line 2338132
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2338133
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/GMB;->EMPTY_SELECTED:LX/GMB;

    .line 2338134
    :goto_0
    return-object v0

    .line 2338135
    :cond_0
    sget-object v0, LX/GMB;->EMPTY_UNSELECTED:LX/GMB;

    goto :goto_0

    .line 2338136
    :cond_1
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 2338137
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2338138
    sget-object v0, LX/GMB;->INVALID:LX/GMB;

    goto :goto_0

    .line 2338139
    :cond_2
    sget-object v0, LX/GMB;->VALID:LX/GMB;

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 2338123
    invoke-super {p0}, LX/GHg;->a()V

    .line 2338124
    const/4 v1, 0x0

    .line 2338125
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setOnFocusChangeListenerEditText(Landroid/view/View$OnFocusChangeListener;)V

    .line 2338126
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2338127
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setOnEditorActionListenerEditText(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2338128
    iget-object v0, p0, LX/GJH;->f:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 2338129
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v1, p0, LX/GJH;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->b(Landroid/text/TextWatcher;)V

    .line 2338130
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    .line 2338131
    return-void
.end method

.method public a(LX/GJD;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2338111
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2338112
    iget-object v0, p1, LX/GJD;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    move-object v0, v0

    .line 2338113
    iput-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    .line 2338114
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    new-instance p1, LX/GM7;

    invoke-direct {p1, p0}, LX/GM7;-><init>(LX/GJH;)V

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setOnEditorActionListenerEditText(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2338115
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    new-instance p1, LX/GM8;

    invoke-direct {p1, p0}, LX/GM8;-><init>(LX/GJH;)V

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2338116
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    .line 2338117
    new-instance p1, LX/GM6;

    invoke-direct {p1, p0}, LX/GM6;-><init>(LX/GJH;)V

    move-object p1, p1

    .line 2338118
    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setOnFocusChangeListenerEditText(Landroid/view/View$OnFocusChangeListener;)V

    .line 2338119
    invoke-virtual {p0}, LX/GJH;->v()Landroid/text/TextWatcher;

    move-result-object v0

    iput-object v0, p0, LX/GJH;->f:Landroid/text/TextWatcher;

    .line 2338120
    iget-object v0, p0, LX/GJH;->f:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 2338121
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object p1, p0, LX/GJH;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->a(Landroid/text/TextWatcher;)V

    .line 2338122
    :cond_0
    return-void
.end method

.method public final a(LX/GMB;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2338092
    sget-object v0, LX/GM9;->a:[I

    invoke-virtual {p1}, LX/GMB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2338093
    :goto_0
    return-void

    .line 2338094
    :pswitch_0
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibility(I)V

    goto :goto_0

    .line 2338095
    :pswitch_1
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibility(I)V

    .line 2338096
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilityPrefixTextView(I)V

    .line 2338097
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilitySuffixTextView(I)V

    .line 2338098
    sget-object v0, LX/GMA;->GENERIC:LX/GMA;

    invoke-direct {p0, v0}, LX/GJH;->a(LX/GMA;)V

    goto :goto_0

    .line 2338099
    :pswitch_2
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibility(I)V

    .line 2338100
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilityPrefixTextView(I)V

    .line 2338101
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilitySuffixTextView(I)V

    .line 2338102
    sget-object v0, LX/GMA;->DETAIL:LX/GMA;

    invoke-direct {p0, v0}, LX/GJH;->a(LX/GMA;)V

    goto :goto_0

    .line 2338103
    :pswitch_3
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibility(I)V

    .line 2338104
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilityPrefixTextView(I)V

    .line 2338105
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilitySuffixTextView(I)V

    .line 2338106
    sget-object v0, LX/GMA;->NONE:LX/GMA;

    invoke-direct {p0, v0}, LX/GJH;->a(LX/GMA;)V

    goto :goto_0

    .line 2338107
    :pswitch_4
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibility(I)V

    .line 2338108
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilityPrefixTextView(I)V

    .line 2338109
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibilitySuffixTextView(I)V

    .line 2338110
    sget-object v0, LX/GMA;->NONE:LX/GMA;

    invoke-direct {p0, v0}, LX/GJH;->a(LX/GMA;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2338088
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 2338089
    iget-object v0, p0, LX/GJH;->e:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 2338090
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setCursorVisible(Z)V

    .line 2338091
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2338087
    check-cast p1, LX/GJD;

    invoke-virtual {p0, p1, p2}, LX/GJH;->a(LX/GJD;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public abstract v()Landroid/text/TextWatcher;
.end method

.method public abstract w()Ljava/lang/CharSequence;
.end method

.method public abstract x()Ljava/lang/CharSequence;
.end method

.method public final y()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2338084
    iget-object v0, p0, LX/GJH;->e:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2338085
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setCursorVisible(Z)V

    .line 2338086
    return-void
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 2338082
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTextEditText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GJH;->a(Ljava/lang/String;)LX/GMB;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GJH;->a(LX/GMB;)V

    .line 2338083
    return-void
.end method
