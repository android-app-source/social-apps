.class public LX/GbB;
.super Landroid/preference/PreferenceCategory;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2369219
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2369220
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 2

    .prologue
    .line 2369221
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2369222
    const-string v0, "Device Based Login - internal"

    invoke-virtual {p0, v0}, LX/GbB;->setTitle(Ljava/lang/CharSequence;)V

    .line 2369223
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, LX/GbB;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2369224
    const-string v1, "Launch NUX"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2369225
    new-instance v1, LX/GbA;

    invoke-direct {v1, p0}, LX/GbA;-><init>(LX/GbB;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2369226
    invoke-virtual {p0, v0}, LX/GbB;->addPreference(Landroid/preference/Preference;)Z

    .line 2369227
    new-instance v0, LX/4ok;

    invoke-virtual {p0}, LX/GbB;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2369228
    sget-object v1, LX/26p;->p:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2369229
    const-string v1, "Use Face Recognition"

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2369230
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2369231
    invoke-virtual {p0, v0}, LX/GbB;->addPreference(Landroid/preference/Preference;)Z

    .line 2369232
    return-void
.end method
