.class public final LX/GC4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/LogoutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/LogoutFragment;)V
    .locals 0

    .prologue
    .line 2327970
    iput-object p1, p0, LX/GC4;->a:Lcom/facebook/account/recovery/fragment/LogoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x7920984d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327971
    iget-object v1, p0, LX/GC4;->a:Lcom/facebook/account/recovery/fragment/LogoutFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/LogoutFragment;->c:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    move-result v1

    .line 2327972
    iget-object v2, p0, LX/GC4;->a:Lcom/facebook/account/recovery/fragment/LogoutFragment;

    iget-object v2, v2, Lcom/facebook/account/recovery/fragment/LogoutFragment;->a:LX/2Ne;

    iget-object v3, p0, LX/GC4;->a:Lcom/facebook/account/recovery/fragment/LogoutFragment;

    iget-object v3, v3, Lcom/facebook/account/recovery/fragment/LogoutFragment;->f:Ljava/lang/String;

    .line 2327973
    iget-object v5, v2, LX/2Ne;->a:LX/0Zb;

    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/GB4;->LOGOUT_OPTION_SELECTED:LX/GB4;

    invoke-virtual {p1}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "account_recovery"

    .line 2327974
    iput-object p1, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327975
    move-object v6, v6

    .line 2327976
    const-string p1, "crypted_id"

    invoke-virtual {v6, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "logout"

    invoke-virtual {v6, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const/4 p1, 0x1

    invoke-interface {v5, v6, p1}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327977
    iget-object v2, p0, LX/GC4;->a:Lcom/facebook/account/recovery/fragment/LogoutFragment;

    iget-object v2, v2, Lcom/facebook/account/recovery/fragment/LogoutFragment;->b:LX/2Nj;

    invoke-interface {v2, v1}, LX/2Nj;->b(Z)V

    .line 2327978
    const v1, 0x34b2785

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
