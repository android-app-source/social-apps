.class public LX/Gwy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Zb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2Zb",
        "<",
        "LX/Gwt;",
        "LX/Gww;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0lB;

.field private b:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2407601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407602
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2407603
    invoke-static {v1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lB;

    iput-object v0, p0, LX/Gwy;->a:LX/0lB;

    .line 2407604
    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/Gwy;->b:LX/03V;

    .line 2407605
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2407579
    :try_start_0
    new-instance v0, LX/Gww;

    iget-object v1, p0, LX/Gwy;->a:LX/0lB;

    invoke-static {p1, v1}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->a(Ljava/lang/String;LX/0lC;)Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Gww;-><init>(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2407580
    :goto_0
    return-object v0

    .line 2407581
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2407582
    iget-object v0, p0, LX/Gwy;->b:LX/03V;

    const-string v2, "FacewebComponentStore"

    const-string v3, "Failed to deserialize"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2407583
    new-instance v0, LX/Gww;

    sget-object v2, LX/Gwv;->DESERIALIZATION_ERROR:LX/Gwv;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/Gww;-><init>(LX/Gwv;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2407584
    const-class v0, LX/Gwx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2407585
    check-cast p1, LX/Gwt;

    .line 2407586
    invoke-virtual {p1}, LX/Gwt;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;LX/2Zg;)V
    .locals 6

    .prologue
    .line 2407587
    check-cast p2, LX/Gwt;

    .line 2407588
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 2407589
    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    .line 2407590
    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2407591
    new-instance v5, LX/Gww;

    sget-object v0, LX/Gwv;->NO_SESSION_ERROR:LX/Gwv;

    const-string v1, ""

    invoke-direct {v5, v0, v1}, LX/Gww;-><init>(LX/Gwv;Ljava/lang/String;)V

    .line 2407592
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/2Zg;->a(Landroid/content/Context;ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2407593
    :goto_0
    monitor-exit p0

    return-void

    .line 2407594
    :cond_0
    :try_start_1
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    .line 2407595
    const-string v1, "fetchFwComponents"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const v3, -0x1c0ba5d1

    invoke-static {v0, v1, v2, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 2407596
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2407597
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2407598
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2407599
    const/16 v0, 0xe10

    return v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2407600
    const/16 v0, 0xe10

    return v0
.end method
