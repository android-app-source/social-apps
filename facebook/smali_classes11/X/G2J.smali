.class public final LX/G2J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/timeline/protiles/model/ProtileModel;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/model/ProtileModel;)V
    .locals 0

    .prologue
    .line 2313865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2313866
    iput-object p1, p0, LX/G2J;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313867
    return-void
.end method


# virtual methods
.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 6

    .prologue
    .line 2313868
    iget-object v0, p0, LX/G2J;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313869
    iget-object v1, v0, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v3, v1

    .line 2313870
    const/4 v1, 0x0

    .line 2313871
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2313872
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2313873
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->s()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2313874
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2313875
    :cond_0
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method
