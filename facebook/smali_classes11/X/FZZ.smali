.class public final LX/FZZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/633;


# instance fields
.field public final synthetic a:Lcom/facebook/search/fragment/GraphSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/fragment/GraphSearchFragment;)V
    .locals 0

    .prologue
    .line 2257392
    iput-object p1, p0, LX/FZZ;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)V
    .locals 6

    .prologue
    .line 2257393
    iget-object v0, p0, LX/FZZ;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v0, v0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2257394
    iget-object v1, v0, LX/FZe;->d:LX/FZW;

    iget-object v2, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v1, v2}, LX/FZW;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/suggestions/SuggestionsFragment;

    move-result-object v1

    .line 2257395
    iget-object v2, v0, LX/FZe;->u:LX/2Sd;

    sget-object v3, LX/FZe;->F:Ljava/lang/String;

    sget-object v4, LX/7CQ;->NEW_TYPEAHEAD_TEXT_TYPED:LX/7CQ;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "\""

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string p0, "\""

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2257396
    iget-object v2, v0, LX/FZe;->s:LX/0Uh;

    sget v3, LX/2SU;->q:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/FZe;->A:Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/FZe;->e:LX/FZX;

    iget-object v3, v0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v3}, LX/FZW;->b()Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/FZX;->a(Landroid/support/v4/app/Fragment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2257397
    iget-object v2, v0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v2}, LX/FZW;->b()Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    move-result-object v2

    .line 2257398
    iget-object v3, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v3, v2}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    .line 2257399
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2257400
    invoke-static {v1, v2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->b$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V

    .line 2257401
    return-void

    .line 2257402
    :cond_1
    iget-object v2, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v2, v1}, LX/FZX;->a(Landroid/support/v4/app/Fragment;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2257403
    iget-object v2, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v2, v1}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    .line 2257404
    iget-object v2, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v1, v2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    goto :goto_0
.end method
