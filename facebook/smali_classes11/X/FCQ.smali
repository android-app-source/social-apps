.class public final LX/FCQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public appPubackMonotonicTimeMs:J

.field public appReceiveDrMonotonicTimeMs:J

.field public final appSendMonotonicTimeMs:J

.field public mqttReceiveDrMonotonicTimeMs:J

.field public mqttSendMonotonicTimeMs:J

.field public sameMqttConnection:Z

.field public final sendAttemptDeviceTimestampMs:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 5

    .prologue
    const-wide/16 v2, -0x1

    .line 2210215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210216
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FCQ;->sameMqttConnection:Z

    .line 2210217
    iput-wide v2, p0, LX/FCQ;->appReceiveDrMonotonicTimeMs:J

    .line 2210218
    iput-wide v2, p0, LX/FCQ;->appPubackMonotonicTimeMs:J

    .line 2210219
    iput-wide v2, p0, LX/FCQ;->mqttSendMonotonicTimeMs:J

    .line 2210220
    iput-wide v2, p0, LX/FCQ;->mqttReceiveDrMonotonicTimeMs:J

    .line 2210221
    iput-wide p1, p0, LX/FCQ;->sendAttemptDeviceTimestampMs:J

    .line 2210222
    iput-wide p3, p0, LX/FCQ;->appSendMonotonicTimeMs:J

    .line 2210223
    return-void
.end method
