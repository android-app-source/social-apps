.class public final LX/FXh;
.super LX/FXV;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2255314
    invoke-direct {p0}, LX/FXV;-><init>()V

    return-void
.end method

.method private a(LX/FXj;ILjava/lang/String;)Z
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2255303
    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    .line 2255304
    iget-object v0, p1, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    new-instance v1, LX/FXg;

    invoke-direct {v1, p0, p1}, LX/FXg;-><init>(LX/FXh;LX/FXj;)V

    invoke-virtual {v0, p3, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2255305
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    if-eqz v0, :cond_0

    .line 2255306
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;->setVisibility(I)V

    .line 2255307
    :cond_0
    const/4 v0, 0x1

    .line 2255308
    :cond_1
    :goto_0
    return v0

    .line 2255309
    :cond_2
    iget-object v1, p1, LX/FXj;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/FXj;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2255310
    iget-boolean v2, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    move v1, v2

    .line 2255311
    if-eqz v1, :cond_1

    .line 2255312
    iget-object v1, p1, LX/FXj;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2255313
    iget-object v1, p1, LX/FXj;->e:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f083428

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public static b(LX/FXh;LX/FXj;I)V
    .locals 3

    .prologue
    .line 2255293
    iget-object v0, p1, LX/FXj;->d:LX/FWt;

    invoke-virtual {v0}, LX/FWt;->d()V

    .line 2255294
    iget-object v0, p1, LX/FXj;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p1, LX/FXj;->f:Ljava/lang/String;

    invoke-static {v1}, LX/FXO;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2255295
    iget-object v0, p1, LX/FXj;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p1, LX/FXj;->f:Ljava/lang/String;

    invoke-static {v1}, LX/FXO;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2255296
    :cond_0
    if-nez p2, :cond_1

    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    .line 2255297
    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/FXj;->g:LX/FXU;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 2255298
    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p1, LX/FXj;->m:LX/FYi;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/FXj;->m:LX/FYi;

    .line 2255299
    iget-object v1, v0, LX/FYi;->a:LX/FYr;

    move-object v0, v1

    .line 2255300
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-nez v0, :cond_2

    .line 2255301
    invoke-direct {p0, p1}, LX/FXh;->f(LX/FXj;)V

    .line 2255302
    :cond_2
    return-void
.end method

.method private c(LX/FXj;LX/BO1;)V
    .locals 5

    .prologue
    .line 2255284
    iget-object v0, p1, LX/FXj;->g:LX/FXU;

    iget-object v1, p1, LX/FXj;->f:Ljava/lang/String;

    .line 2255285
    iput-object v1, v0, LX/FXU;->c:Ljava/lang/String;

    .line 2255286
    iget-object v2, v0, LX/FXU;->b:LX/01J;

    iget-object v3, v0, LX/FXU;->c:Ljava/lang/String;

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v4}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2255287
    iget-object v0, p1, LX/FXj;->m:LX/FYi;

    invoke-interface {p2}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/FYi;->a(Landroid/database/Cursor;LX/BO1;)V

    .line 2255288
    const/4 v0, 0x0

    .line 2255289
    invoke-interface {p2}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, LX/FXj;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p1, LX/FXj;->f:Ljava/lang/String;

    invoke-static {v2}, LX/FXO;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 2255290
    if-eqz v0, :cond_2

    .line 2255291
    invoke-direct {p0, p1}, LX/FXh;->f(LX/FXj;)V

    .line 2255292
    :cond_2
    return-void
.end method

.method private f(LX/FXj;)V
    .locals 4

    .prologue
    .line 2255244
    iget-object v0, p1, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2255245
    iget-object v0, p1, LX/FXj;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2255246
    iget-object v0, p1, LX/FXj;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2255247
    :cond_0
    iget-object v0, p1, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget v1, p1, LX/FXj;->o:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2255248
    iput-object v0, p1, LX/FXj;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2255249
    iget-object v0, p1, LX/FXj;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, LX/FXc;

    invoke-direct {v1, p0, p1}, LX/FXc;-><init>(LX/FXh;LX/FXj;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2255250
    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    .line 2255251
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2255252
    if-nez v0, :cond_1

    .line 2255253
    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/FXj;->m:LX/FYi;

    .line 2255254
    iget-object v2, v1, LX/FYi;->a:LX/FYr;

    move-object v1, v2

    .line 2255255
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2255256
    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/FXj;->g:LX/FXU;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2255257
    :cond_1
    iget-object v0, p1, LX/FXj;->m:LX/FYi;

    .line 2255258
    iget-object v1, v0, LX/FYi;->a:LX/FYr;

    move-object v0, v1

    .line 2255259
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-nez v0, :cond_3

    .line 2255260
    const/4 v3, 0x0

    .line 2255261
    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2255262
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    if-nez v0, :cond_2

    .line 2255263
    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p1, LX/FXj;->q:I

    iget-object v2, p1, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    .line 2255264
    iput-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    .line 2255265
    iget-object v0, p1, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->addView(Landroid/view/View;)V

    .line 2255266
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    new-instance v1, LX/FXe;

    invoke-direct {v1, p0, p1}, LX/FXe;-><init>(LX/FXh;LX/FXj;)V

    .line 2255267
    iput-object v1, v0, Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;->a:LX/FXd;

    .line 2255268
    :cond_2
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    invoke-virtual {v0, v3}, Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;->setVisibility(I)V

    .line 2255269
    :goto_0
    return-void

    .line 2255270
    :cond_3
    iget-object v0, p1, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2255271
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    if-eqz v0, :cond_4

    .line 2255272
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;->setVisibility(I)V

    .line 2255273
    :cond_4
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/FXj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2255276
    iget-object v0, p1, LX/FXj;->m:LX/FYi;

    .line 2255277
    iget-object v1, v0, LX/FYi;->a:LX/FYr;

    move-object v0, v1

    .line 2255278
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    iget-object v1, p1, LX/FXj;->s:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, LX/FXh;->a(LX/FXj;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2255279
    iget-object v0, p1, LX/FXj;->r:LX/BO1;

    invoke-interface {v0}, LX/AU0;->c()V

    .line 2255280
    :goto_0
    iput-object v2, p1, LX/FXj;->r:LX/BO1;

    .line 2255281
    iput-object v2, p1, LX/FXj;->s:Ljava/lang/String;

    .line 2255282
    return-void

    .line 2255283
    :cond_0
    iget-object v0, p1, LX/FXj;->r:LX/BO1;

    invoke-direct {p0, p1, v0}, LX/FXh;->c(LX/FXj;LX/BO1;)V

    goto :goto_0
.end method

.method public final a(LX/FXj;I)V
    .locals 0

    .prologue
    .line 2255315
    invoke-static {p0, p1, p2}, LX/FXh;->b(LX/FXh;LX/FXj;I)V

    .line 2255316
    return-void
.end method

.method public final a(LX/FXj;LX/BO1;)V
    .locals 0

    .prologue
    .line 2255274
    invoke-direct {p0, p1, p2}, LX/FXh;->c(LX/FXj;LX/BO1;)V

    .line 2255275
    return-void
.end method

.method public final a(LX/FXj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2255238
    iget-object v0, p1, LX/FXj;->d:LX/FWt;

    invoke-virtual {v0}, LX/FWt;->d()V

    .line 2255239
    invoke-static {p2}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2255240
    iget-object v0, p1, LX/FXj;->m:LX/FYi;

    .line 2255241
    iget-object v1, v0, LX/FYi;->a:LX/FYr;

    move-object v0, v1

    .line 2255242
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    invoke-direct {p0, p1, v0, p2}, LX/FXh;->a(LX/FXj;ILjava/lang/String;)Z

    .line 2255243
    return-void
.end method

.method public final c(LX/FXj;)V
    .locals 2

    .prologue
    .line 2255228
    iget-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    if-eqz v0, :cond_0

    .line 2255229
    iget-object v0, p1, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->removeView(Landroid/view/View;)V

    .line 2255230
    const/4 v0, 0x0

    .line 2255231
    iput-object v0, p1, LX/FXj;->n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

    .line 2255232
    :cond_0
    iget-object v0, p1, LX/FXj;->c:LX/AUC;

    iget v1, p1, LX/FXj;->i:I

    .line 2255233
    iget-object p0, v0, LX/AUC;->a:LX/0k5;

    invoke-virtual {p0, v1}, LX/0k5;->a(I)V

    .line 2255234
    const/4 v0, -0x1

    .line 2255235
    iput v0, p1, LX/FXj;->i:I

    .line 2255236
    sget-object v0, LX/FXV;->a:LX/FXV;

    invoke-static {p1, v0}, LX/FXj;->a$redex0(LX/FXj;LX/FXV;)V

    .line 2255237
    return-void
.end method

.method public final d(LX/FXj;)V
    .locals 0

    .prologue
    .line 2255227
    return-void
.end method

.method public final e(LX/FXj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2255225
    iget-object v0, p1, LX/FXj;->m:LX/FYi;

    invoke-virtual {v0, v1, v1}, LX/FYi;->a(Landroid/database/Cursor;LX/BO1;)V

    .line 2255226
    return-void
.end method
