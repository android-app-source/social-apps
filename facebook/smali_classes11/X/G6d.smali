.class public final LX/G6d;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2320538
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :pswitch_0
    const-string v0, "STATE_NOT_STARTED"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "STATE_STARTING"

    goto :goto_0

    :pswitch_2
    const-string v0, "STATE_DOWNLOADING"

    goto :goto_0

    :pswitch_3
    const-string v0, "STATE_DIFF_PATCHING"

    goto :goto_0

    :pswitch_4
    const-string v0, "STATE_VERIFYING"

    goto :goto_0

    :pswitch_5
    const-string v0, "STATE_UNINSTALL_REQUIRED"

    goto :goto_0

    :pswitch_6
    const-string v0, "STATE_SUCCEEDED"

    goto :goto_0

    :pswitch_7
    const-string v0, "STATE_FAILED"

    goto :goto_0

    :pswitch_8
    const-string v0, "STATE_DISCARDED"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 2320595
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2320596
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2320597
    if-eqz v0, :cond_0

    .line 2320598
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2320599
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2320600
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2320601
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2320582
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2320583
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2320584
    :goto_0
    return v1

    .line 2320585
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2320586
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2320587
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2320588
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2320589
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2320590
    const-string v3, "uri"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2320591
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2320592
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2320593
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2320594
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/15w;)LX/15i;
    .locals 6

    .prologue
    .line 2320602
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2320603
    const/4 v2, 0x0

    .line 2320604
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_3

    .line 2320605
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2320606
    :goto_0
    move v1, v2

    .line 2320607
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2320608
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 2320609
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2320610
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2320611
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2320612
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2320613
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2320614
    const-string v4, "saved_items"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2320615
    invoke-static {p0, v0}, LX/FX6;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 2320616
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2320617
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2320618
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public static a$redex0(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2320559
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2320560
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2320561
    if-eqz v0, :cond_0

    .line 2320562
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2320563
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2320564
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2320565
    if-eqz v0, :cond_1

    .line 2320566
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2320567
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2320568
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2320569
    if-eqz v0, :cond_2

    .line 2320570
    const-string v1, "new_activity_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2320571
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2320572
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2320573
    if-eqz v0, :cond_3

    .line 2320574
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2320575
    invoke-static {p0, v0, p2}, LX/G6d;->a(LX/15i;ILX/0nX;)V

    .line 2320576
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2320577
    if-eqz v0, :cond_4

    .line 2320578
    const-string v1, "unread_count_string"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2320579
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2320580
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2320581
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2320546
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2320547
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2320548
    :goto_0
    return v1

    .line 2320549
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2320550
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2320551
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2320552
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2320553
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2320554
    const-string v3, "node"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2320555
    invoke-static {p0, p1}, LX/Eh5;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2320556
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2320557
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2320558
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2320539
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2320540
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2320541
    if-eqz v0, :cond_0

    .line 2320542
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2320543
    invoke-static {p0, v0, p2, p3}, LX/Eh5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2320544
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2320545
    return-void
.end method
