.class public final LX/FfA;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)V
    .locals 0

    .prologue
    .line 2267602
    iput-object p1, p0, LX/FfA;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 5

    .prologue
    .line 2267603
    iget-object v0, p0, LX/FfA;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    .line 2267604
    invoke-static {p1}, LX/Cw8;->getCoreFilterTypeAt(I)LX/Cw8;

    move-result-object v1

    .line 2267605
    iget-object v2, v0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    .line 2267606
    iget-object v3, v2, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Fje;

    .line 2267607
    if-nez v3, :cond_1

    const/4 v3, 0x0

    :goto_0
    move-object v2, v3

    .line 2267608
    if-eqz v2, :cond_0

    sget-object v3, LX/EQG;->LOADING:LX/EQG;

    if-eq v2, v3, :cond_0

    .line 2267609
    iget-object v2, v0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    iget-object v3, v0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    invoke-virtual {v3}, LX/FfG;->f()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4}, LX/Cvm;->a(Ljava/lang/Boolean;LX/Cw8;Ljava/lang/String;)V

    .line 2267610
    :cond_0
    iget-object v0, p0, LX/FfA;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    .line 2267611
    invoke-static {p1}, LX/Cw8;->getCoreFilterTypeAt(I)LX/Cw8;

    move-result-object v1

    .line 2267612
    invoke-static {v0, v1}, LX/FfG;->b(LX/FfG;LX/Cw8;)V

    .line 2267613
    iget-object v0, p0, LX/FfA;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    .line 2267614
    iput p1, v0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->k:I

    .line 2267615
    return-void

    .line 2267616
    :cond_1
    iget-object v2, v3, LX/Fje;->B:LX/EQG;

    move-object v3, v2

    .line 2267617
    goto :goto_0
.end method
