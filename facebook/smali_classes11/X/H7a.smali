.class public final LX/H7a;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 2431014
    const/16 v18, 0x0

    .line 2431015
    const-wide/16 v16, 0x0

    .line 2431016
    const/4 v15, 0x0

    .line 2431017
    const/4 v14, 0x0

    .line 2431018
    const/4 v13, 0x0

    .line 2431019
    const/4 v12, 0x0

    .line 2431020
    const/4 v11, 0x0

    .line 2431021
    const/4 v10, 0x0

    .line 2431022
    const/4 v9, 0x0

    .line 2431023
    const/4 v8, 0x0

    .line 2431024
    const/4 v7, 0x0

    .line 2431025
    const/4 v6, 0x0

    .line 2431026
    const/4 v5, 0x0

    .line 2431027
    const/4 v4, 0x0

    .line 2431028
    const/4 v3, 0x0

    .line 2431029
    const/4 v2, 0x0

    .line 2431030
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_12

    .line 2431031
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2431032
    const/4 v2, 0x0

    .line 2431033
    :goto_0
    return v2

    .line 2431034
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 2431035
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2431036
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2431037
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 2431038
    const-string v6, "claim_status"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2431039
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 2431040
    :cond_1
    const-string v6, "claim_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2431041
    const/4 v2, 0x1

    .line 2431042
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2431043
    :cond_2
    const-string v6, "discount_barcode_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2431044
    invoke-static/range {p0 .. p1}, LX/H7x;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 2431045
    :cond_3
    const-string v6, "discount_barcode_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2431046
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 2431047
    :cond_4
    const-string v6, "discount_barcode_value"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2431048
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2431049
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2431050
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2431051
    :cond_6
    const-string v6, "instore_offer_code"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2431052
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2431053
    :cond_7
    const-string v6, "is_used"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2431054
    const/4 v2, 0x1

    .line 2431055
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v15, v6

    goto/16 :goto_1

    .line 2431056
    :cond_8
    const-string v6, "notification_email"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2431057
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2431058
    :cond_9
    const-string v6, "notifications_enabled"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2431059
    const/4 v2, 0x1

    .line 2431060
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move v13, v6

    goto/16 :goto_1

    .line 2431061
    :cond_a
    const-string v6, "offer_view"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2431062
    invoke-static/range {p0 .. p1}, LX/H7r;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2431063
    :cond_b
    const-string v6, "online_offer_code"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2431064
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 2431065
    :cond_c
    const-string v6, "share_story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2431066
    invoke-static/range {p0 .. p1}, LX/H7q;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 2431067
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2431068
    :cond_e
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2431069
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2431070
    if-eqz v3, :cond_f

    .line 2431071
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2431072
    :cond_f
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2431073
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2431074
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2431075
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2431076
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2431077
    if-eqz v9, :cond_10

    .line 2431078
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 2431079
    :cond_10
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2431080
    if-eqz v8, :cond_11

    .line 2431081
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 2431082
    :cond_11
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2431083
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2431084
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2431085
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_12
    move/from16 v19, v14

    move/from16 v20, v15

    move v14, v9

    move v15, v10

    move v10, v5

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v11, v6

    move v12, v7

    move/from16 v7, v18

    move/from16 v18, v13

    move v13, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2431086
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2431087
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2431088
    if-eqz v0, :cond_0

    .line 2431089
    const-string v1, "claim_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431090
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2431091
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2431092
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 2431093
    const-string v2, "claim_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431094
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2431095
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2431096
    if-eqz v0, :cond_2

    .line 2431097
    const-string v1, "discount_barcode_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431098
    invoke-static {p0, v0, p2, p3}, LX/H7x;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2431099
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2431100
    if-eqz v0, :cond_3

    .line 2431101
    const-string v1, "discount_barcode_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431102
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2431103
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2431104
    if-eqz v0, :cond_4

    .line 2431105
    const-string v1, "discount_barcode_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431106
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2431107
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2431108
    if-eqz v0, :cond_5

    .line 2431109
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431110
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2431111
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2431112
    if-eqz v0, :cond_6

    .line 2431113
    const-string v1, "instore_offer_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431114
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2431115
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2431116
    if-eqz v0, :cond_7

    .line 2431117
    const-string v1, "is_used"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431118
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2431119
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2431120
    if-eqz v0, :cond_8

    .line 2431121
    const-string v1, "notification_email"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431122
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2431123
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2431124
    if-eqz v0, :cond_9

    .line 2431125
    const-string v1, "notifications_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431126
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2431127
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2431128
    if-eqz v0, :cond_a

    .line 2431129
    const-string v1, "offer_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431130
    invoke-static {p0, v0, p2, p3}, LX/H7r;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2431131
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2431132
    if-eqz v0, :cond_b

    .line 2431133
    const-string v1, "online_offer_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431134
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2431135
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2431136
    if-eqz v0, :cond_c

    .line 2431137
    const-string v1, "share_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2431138
    invoke-static {p0, v0, p2, p3}, LX/H7q;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2431139
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2431140
    return-void
.end method
