.class public LX/HEU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0tX;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0Ot;LX/0Ot;LX/0tX;LX/0Ot;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2442078
    iput-object p1, p0, LX/HEU;->a:Landroid/content/Context;

    .line 2442079
    iput-object p2, p0, LX/HEU;->b:Ljava/util/concurrent/Executor;

    .line 2442080
    iput-object p3, p0, LX/HEU;->c:LX/0Ot;

    .line 2442081
    iput-object p4, p0, LX/HEU;->d:LX/0Ot;

    .line 2442082
    iput-object p5, p0, LX/HEU;->e:LX/0tX;

    .line 2442083
    iput-object p6, p0, LX/HEU;->f:LX/0Ot;

    .line 2442084
    return-void
.end method

.method public static a(LX/0QB;)LX/HEU;
    .locals 10

    .prologue
    .line 2442085
    const-class v1, LX/HEU;

    monitor-enter v1

    .line 2442086
    :try_start_0
    sget-object v0, LX/HEU;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2442087
    sput-object v2, LX/HEU;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2442088
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2442089
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2442090
    new-instance v3, LX/HEU;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    const/16 v6, 0x2eb

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    const/16 v9, 0x455

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/HEU;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0Ot;LX/0Ot;LX/0tX;LX/0Ot;)V

    .line 2442091
    move-object v0, v3

    .line 2442092
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2442093
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HEU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2442094
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2442095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;)Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;
    .locals 6
    .param p0    # Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2442096
    if-eqz p0, :cond_0

    .line 2442097
    invoke-virtual {p0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;->k()LX/0Px;

    move-result-object v1

    .line 2442098
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2442099
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2442100
    invoke-virtual {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->n()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2442101
    if-eqz v3, :cond_1

    move v3, v4

    :goto_0
    if-eqz v3, :cond_4

    .line 2442102
    invoke-virtual {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->n()LX/1vs;

    move-result-object v3

    iget-object p0, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2442103
    invoke-virtual {p0, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    :goto_1
    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    :goto_2
    move v0, v4

    .line 2442104
    if-eqz v0, :cond_0

    .line 2442105
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    .line 2442106
    :goto_3
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_3

    :cond_1
    move v3, v5

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_0

    :cond_3
    move v3, v5

    goto :goto_1

    :cond_4
    move v3, v5

    goto :goto_1

    :cond_5
    move v4, v5

    goto :goto_2
.end method

.method public static a(LX/HEU;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AYMTTipEventType;
        .end annotation
    .end param

    .prologue
    .line 2442107
    new-instance v0, LX/HEZ;

    invoke-direct {v0}, LX/HEZ;-><init>()V

    move-object v0, v0

    .line 2442108
    new-instance v1, LX/4D5;

    invoke-direct {v1}, LX/4D5;-><init>()V

    invoke-virtual {v1, p2}, LX/4D5;->a(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/4D5;->b(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/4D5;->c(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    const-string v2, "page_admin_megaphone"

    invoke-virtual {v1, v2}, LX/4D5;->d(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    .line 2442109
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2442110
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2442111
    iget-object v1, p0, LX/HEU;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2442112
    new-instance v1, LX/HET;

    invoke-direct {v1, p0, p3, p2, p1}, LX/HET;-><init>(LX/HEU;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/HEU;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2442113
    return-void
.end method
