.class public final LX/G1z;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/G20;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/timeline/protiles/model/ProtileModel;

.field public b:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/G20;


# direct methods
.method public constructor <init>(LX/G20;)V
    .locals 1

    .prologue
    .line 2313233
    iput-object p1, p0, LX/G1z;->c:LX/G20;

    .line 2313234
    move-object v0, p1

    .line 2313235
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2313236
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2313237
    const-string v0, "ProtilesCallToActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2313238
    if-ne p0, p1, :cond_1

    .line 2313239
    :cond_0
    :goto_0
    return v0

    .line 2313240
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2313241
    goto :goto_0

    .line 2313242
    :cond_3
    check-cast p1, LX/G1z;

    .line 2313243
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2313244
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2313245
    if-eq v2, v3, :cond_0

    .line 2313246
    iget-object v2, p0, LX/G1z;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/G1z;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    iget-object v3, p1, LX/G1z;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2313247
    goto :goto_0

    .line 2313248
    :cond_5
    iget-object v2, p1, LX/G1z;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    if-nez v2, :cond_4

    .line 2313249
    :cond_6
    iget-object v2, p0, LX/G1z;->b:LX/1Pp;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/G1z;->b:LX/1Pp;

    iget-object v3, p1, LX/G1z;->b:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2313250
    goto :goto_0

    .line 2313251
    :cond_7
    iget-object v2, p1, LX/G1z;->b:LX/1Pp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
