.class public LX/H5P;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H5N;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2424299
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/H5P;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H5Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424300
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2424301
    iput-object p1, p0, LX/H5P;->b:LX/0Ot;

    .line 2424302
    return-void
.end method

.method public static a(LX/0QB;)LX/H5P;
    .locals 4

    .prologue
    .line 2424303
    const-class v1, LX/H5P;

    monitor-enter v1

    .line 2424304
    :try_start_0
    sget-object v0, LX/H5P;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424305
    sput-object v2, LX/H5P;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424306
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424307
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424308
    new-instance v3, LX/H5P;

    const/16 p0, 0x2ada

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H5P;-><init>(LX/0Ot;)V

    .line 2424309
    move-object v0, v3

    .line 2424310
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424311
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424312
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424313
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2424314
    iget-object v0, p0, LX/H5P;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5Q;

    .line 2424315
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/H5Q;->b(LX/1De;)LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    .line 2424316
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/16 p0, 0x8

    const/4 p2, 0x0

    invoke-interface {v1, p0, p2}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x2

    invoke-interface {v1, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x1

    invoke-interface {v1, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const p0, 0x7f0a009c

    invoke-interface {v1, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const p0, 0x7f0b0a97

    invoke-interface {v1, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 2424317
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/H5Q;->b(LX/1De;)LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2424318
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2424319
    invoke-static {}, LX/1dS;->b()V

    .line 2424320
    const/4 v0, 0x0

    return-object v0
.end method
