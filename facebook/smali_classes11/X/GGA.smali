.class public LX/GGA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GG9;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2333336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333337
    iget v0, p1, LX/GG9;->g:I

    if-eq v0, v2, :cond_0

    iget v0, p1, LX/GG9;->g:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    iget v0, p1, LX/GG9;->g:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2333338
    iget v0, p1, LX/GG9;->g:I

    iput v0, p0, LX/GGA;->a:I

    .line 2333339
    iget-object v0, p1, LX/GG9;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2333340
    iget v3, p0, LX/GGA;->a:I

    move v3, v3

    .line 2333341
    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_1
    const-string v3, "The number of components does not match the size of the label list"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2333342
    iget-object v0, p1, LX/GG9;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2333343
    iget v3, p0, LX/GGA;->a:I

    move v3, v3

    .line 2333344
    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_2
    const-string v3, "The number of components does not match the size of the data text list"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2333345
    iget-object v0, p1, LX/GG9;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2333346
    iget v3, p0, LX/GGA;->a:I

    move v3, v3

    .line 2333347
    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    const-string v3, "The number of components does not match the size of the weight list"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2333348
    iget-object v0, p1, LX/GG9;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2333349
    iget v3, p0, LX/GGA;->a:I

    move v3, v3

    .line 2333350
    if-ne v0, v3, :cond_5

    :goto_4
    const-string v0, "The number of components does not match the size of the color list"

    invoke-static {v2, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2333351
    iget-object v0, p1, LX/GG9;->d:LX/0Px;

    iput-object v0, p0, LX/GGA;->e:LX/0Px;

    .line 2333352
    iget-object v0, p1, LX/GG9;->c:LX/0Px;

    iput-object v0, p0, LX/GGA;->d:LX/0Px;

    .line 2333353
    iget-object v0, p1, LX/GG9;->e:LX/0Px;

    iput-object v0, p0, LX/GGA;->f:LX/0Px;

    .line 2333354
    iget-object v0, p1, LX/GG9;->f:LX/0Px;

    iput-object v0, p0, LX/GGA;->g:LX/0Px;

    .line 2333355
    iget-object v0, p1, LX/GG9;->a:Ljava/lang/String;

    iput-object v0, p0, LX/GGA;->b:Ljava/lang/String;

    .line 2333356
    iget-object v0, p1, LX/GG9;->b:Ljava/lang/String;

    iput-object v0, p0, LX/GGA;->c:Ljava/lang/String;

    .line 2333357
    return-void

    :cond_1
    move v0, v1

    .line 2333358
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2333359
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2333360
    goto :goto_2

    :cond_4
    move v0, v1

    .line 2333361
    goto :goto_3

    :cond_5
    move v2, v1

    .line 2333362
    goto :goto_4
.end method
