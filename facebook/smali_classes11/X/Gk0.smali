.class public LX/Gk0;
.super LX/2Vx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Vx",
        "<",
        "LX/Gk3;",
        "Lcom/facebook/greetingcards/verve/model/VMDeck;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Gk0;


# direct methods
.method public constructor <init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;LX/0lp;)V
    .locals 10
    .param p5    # LX/0rb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1Ha;
        .annotation build Lcom/facebook/greetingcards/render/templatefetch/TemplateFileCache;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2388707
    const-string v0, "template"

    const-string v1, "template"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/Gk0;->a(Ljava/lang/String;Ljava/lang/String;Z)LX/2W2;

    move-result-object v5

    invoke-static/range {p8 .. p8}, LX/Gk0;->a(LX/0lp;)LX/2W4;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, LX/2Vx;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/2W2;LX/0rb;LX/1Ha;LX/2W4;LX/1GQ;)V

    .line 2388708
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Z)LX/2W2;
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 2388709
    new-instance v0, LX/2W2;

    invoke-direct {v0}, LX/2W2;-><init>()V

    .line 2388710
    iput-object p0, v0, LX/2W2;->a:Ljava/lang/String;

    .line 2388711
    move-object v0, v0

    .line 2388712
    iput-object p1, v0, LX/2W2;->b:Ljava/lang/String;

    .line 2388713
    move-object v0, v0

    .line 2388714
    iput-boolean p2, v0, LX/2W2;->c:Z

    .line 2388715
    move-object v0, v0

    .line 2388716
    iput v2, v0, LX/2W2;->f:I

    .line 2388717
    move-object v0, v0

    .line 2388718
    iput v2, v0, LX/2W2;->g:I

    .line 2388719
    move-object v0, v0

    .line 2388720
    iput v1, v0, LX/2W2;->d:I

    .line 2388721
    move-object v0, v0

    .line 2388722
    iput v1, v0, LX/2W2;->e:I

    .line 2388723
    move-object v0, v0

    .line 2388724
    return-object v0
.end method

.method private static a(LX/0lp;)LX/2W4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lp;",
            ")",
            "LX/2W4",
            "<",
            "LX/Gk3;",
            "Lcom/facebook/greetingcards/verve/model/VMDeck;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2388725
    new-instance v0, LX/Gjz;

    invoke-direct {v0, p0}, LX/Gjz;-><init>(LX/0lp;)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Gk0;
    .locals 12

    .prologue
    .line 2388726
    sget-object v0, LX/Gk0;->a:LX/Gk0;

    if-nez v0, :cond_1

    .line 2388727
    const-class v1, LX/Gk0;

    monitor-enter v1

    .line 2388728
    :try_start_0
    sget-object v0, LX/Gk0;->a:LX/Gk0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2388729
    if-eqz v2, :cond_0

    .line 2388730
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2388731
    new-instance v3, LX/Gk0;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static {v0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v8

    check-cast v8, LX/0rb;

    invoke-static {v0}, LX/GjW;->a(LX/0QB;)LX/1Ha;

    move-result-object v9

    check-cast v9, LX/1Ha;

    invoke-static {v0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v10

    check-cast v10, LX/1GQ;

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v11

    check-cast v11, LX/0lp;

    invoke-direct/range {v3 .. v11}, LX/Gk0;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;LX/0lp;)V

    .line 2388732
    move-object v0, v3

    .line 2388733
    sput-object v0, LX/Gk0;->a:LX/Gk0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2388734
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2388735
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2388736
    :cond_1
    sget-object v0, LX/Gk0;->a:LX/Gk0;

    return-object v0

    .line 2388737
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2388738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2388739
    const v0, 0xc800

    return v0
.end method
