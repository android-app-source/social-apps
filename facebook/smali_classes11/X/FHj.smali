.class public LX/FHj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2220903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220904
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    .line 2220905
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "num_fetching_offset"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220906
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "num_send_bytes"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220907
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2220900
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "num_fetching_offset"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2220901
    iget-object v1, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v2, "num_fetching_offset"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220902
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2220898
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "media_size"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220899
    return-void
.end method

.method public final a(LX/FHi;)V
    .locals 3

    .prologue
    .line 2220889
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "upload_state"

    invoke-virtual {p1}, LX/FHi;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220890
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2220895
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "upload_fail_exception"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220896
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "upload_fail_cause"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220897
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 2220892
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v1, "num_send_bytes"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2220893
    iget-object v1, p0, LX/FHj;->a:Ljava/util/Map;

    const-string v2, "num_send_bytes"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220894
    return-void
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2220891
    iget-object v0, p0, LX/FHj;->a:Ljava/util/Map;

    return-object v0
.end method
