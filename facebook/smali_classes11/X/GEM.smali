.class public final LX/GEM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GD8;

.field public final synthetic b:LX/GEO;


# direct methods
.method public constructor <init>(LX/GEO;LX/GD8;)V
    .locals 0

    .prologue
    .line 2331628
    iput-object p1, p0, LX/GEM;->b:LX/GEO;

    iput-object p2, p0, LX/GEM;->a:LX/GD8;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2331629
    iget-object v0, p0, LX/GEM;->a:LX/GD8;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/GD8;->a(I)V

    .line 2331630
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2331631
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2331632
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331633
    check-cast v0, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;

    .line 2331634
    if-nez v0, :cond_1

    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    const/4 v0, -0x1

    .line 2331635
    :goto_1
    iget-object v1, p0, LX/GEM;->a:LX/GD8;

    invoke-virtual {v1, v0}, LX/GD8;->a(I)V

    .line 2331636
    return-void

    .line 2331637
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;->a()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 2331638
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2331639
    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    goto :goto_1
.end method
