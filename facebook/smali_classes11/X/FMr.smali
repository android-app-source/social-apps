.class public LX/FMr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/FMw;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/FN4;

.field private final e:LX/FMu;

.field private final f:LX/1rd;

.field private final g:LX/FN6;

.field private final h:LX/FMG;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2231201
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "address"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    sput-object v0, LX/FMr;->i:[Ljava/lang/String;

    .line 2231202
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "address"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    const-string v1, "sub_id"

    aput-object v1, v0, v6

    sput-object v0, LX/FMr;->j:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/FMw;LX/0Ot;LX/FN4;LX/FMu;LX/1rd;LX/FN6;LX/FMG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/FMw;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/FN4;",
            "LX/FMu;",
            "LX/1rd;",
            "LX/FN6;",
            "LX/FMG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231192
    iput-object p1, p0, LX/FMr;->a:Landroid/content/Context;

    .line 2231193
    iput-object p2, p0, LX/FMr;->b:LX/FMw;

    .line 2231194
    iput-object p3, p0, LX/FMr;->c:LX/0Ot;

    .line 2231195
    iput-object p4, p0, LX/FMr;->d:LX/FN4;

    .line 2231196
    iput-object p5, p0, LX/FMr;->e:LX/FMu;

    .line 2231197
    iput-object p6, p0, LX/FMr;->f:LX/1rd;

    .line 2231198
    iput-object p7, p0, LX/FMr;->g:LX/FN6;

    .line 2231199
    iput-object p8, p0, LX/FMr;->h:LX/FMG;

    .line 2231200
    return-void
.end method

.method private static a(LX/FMr;ILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;Ljava/lang/String;)V
    .locals 13
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2231203
    iget-object v1, p0, LX/FMr;->f:LX/1rd;

    invoke-virtual {v1}, LX/1rd;->b()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    if-ltz p1, :cond_3

    .line 2231204
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x16

    if-lt v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 2231205
    iget-object v1, p0, LX/FMr;->f:LX/1rd;

    iget-object v2, p0, LX/FMr;->g:LX/FN6;

    invoke-virtual {v2}, LX/FN6;->a()I

    move-result v2

    invoke-virtual {v1, p1, v2}, LX/1rd;->a(II)I

    move-result v1

    invoke-static {v1}, Landroid/telephony/SmsManager;->getSmsManagerForSubscriptionId(I)Landroid/telephony/SmsManager;

    move-result-object v1

    .line 2231206
    :goto_1
    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2231207
    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2231208
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2231209
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_4

    .line 2231210
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.facebook.messaging.sms.MESSAGE_SENT"

    iget-object v8, p0, LX/FMr;->a:Landroid/content/Context;

    const-class v9, LX/FMm;

    move-object/from16 v0, p4

    invoke-direct {v6, v7, v0, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2231211
    if-eqz p5, :cond_0

    .line 2231212
    move-object/from16 v0, p5

    invoke-static {v6, v0}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(Landroid/content/Intent;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231213
    :cond_0
    const-string v7, "number_of_parts"

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2231214
    if-eqz p6, :cond_1

    .line 2231215
    const-string v7, "offline_id"

    move-object/from16 v0, p6

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2231216
    :cond_1
    iget-object v7, p0, LX/FMr;->a:Landroid/content/Context;

    const/high16 v8, 0x8000000

    invoke-static {v7, v3, v6, v8}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2231217
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2231218
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 2231219
    :cond_3
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v1

    goto :goto_1

    .line 2231220
    :cond_4
    const/4 v3, 0x0

    .line 2231221
    if-eqz p5, :cond_5

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d()I

    move-result v6

    if-nez v6, :cond_5

    .line 2231222
    iget-object v3, p0, LX/FMr;->d:LX/FN4;

    invoke-virtual {v3, v2}, LX/FN4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2231223
    :cond_5
    iget-object v6, p0, LX/FMr;->e:LX/FMu;

    invoke-virtual {v6, p1}, LX/FMu;->g(I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2231224
    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 2231225
    :cond_6
    return-void

    .line 2231226
    :cond_7
    const/4 v6, 0x0

    move v12, v6

    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v12, v6, :cond_6

    .line 2231227
    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/PendingIntent;

    const/4 v11, 0x0

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    invoke-virtual/range {v6 .. v11}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 2231228
    add-int/lit8 v6, v12, 0x1

    move v12, v6

    goto :goto_3
.end method

.method private static a(LX/FMr;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 4
    .param p2    # LX/FMM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2231184
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.messaging.sms.MESSAGE_SENT"

    iget-object v2, p0, LX/FMr;->a:Landroid/content/Context;

    const-class v3, LX/FMm;

    invoke-direct {v0, v1, p1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2231185
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2231186
    const-string v1, "mmssms_quickfail_msg"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2231187
    :cond_0
    const-string v1, "mmssms_quickfail_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2231188
    invoke-static {v0, p4}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(Landroid/content/Intent;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231189
    iget-object v1, p0, LX/FMr;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2231190
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 2231152
    iget-object v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2231153
    invoke-static {v0}, LX/FMQ;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2231154
    iget-boolean v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->f:Z

    move v0, v0

    .line 2231155
    if-eqz v0, :cond_1

    .line 2231156
    sget-object v2, LX/FMM;->EXPIRED_MESSAGE:LX/FMM;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "age: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/FMr;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 2231157
    iget-wide v8, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->c:J

    move-wide v6, v8

    .line 2231158
    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v2, v0, p1}, LX/FMr;->a(LX/FMr;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231159
    :cond_0
    :goto_0
    return-void

    .line 2231160
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x16

    if-lt v0, v2, :cond_2

    .line 2231161
    iget v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    move v0, v0

    .line 2231162
    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2231163
    :goto_1
    if-nez v0, :cond_3

    iget-object v0, p0, LX/FMr;->a:Landroid/content/Context;

    invoke-static {v0}, LX/FNi;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2231164
    sget-object v0, LX/FMM;->NO_CONNECTION:LX/FMM;

    invoke-static {p0, v1, v0, v7, p1}, LX/FMr;->a(LX/FMr;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    goto :goto_0

    .line 2231165
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2231166
    :cond_3
    iget-object v0, p0, LX/FMr;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 2231167
    :try_start_0
    iget-object v0, p0, LX/FMr;->h:LX/FMG;

    sget-object v2, LX/FMr;->i:[Ljava/lang/String;

    sget-object v3, LX/FMr;->j:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    .line 2231168
    if-eqz v7, :cond_4

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2231169
    const-string v0, "body"

    invoke-static {v7, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2231170
    const-string v0, "address"

    invoke-static {v7, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2231171
    const-string v0, "_id"

    invoke-static {v7, v0}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 2231172
    sget-object v1, LX/554;->a:Landroid/net/Uri;

    int-to-long v4, v0

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 2231173
    iget-object v0, p0, LX/FMr;->h:LX/FMG;

    invoke-virtual {v0, v7}, LX/FMG;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 2231174
    const/4 v6, 0x0

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, LX/FMr;->a(LX/FMr;ILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231175
    :goto_2
    if-eqz v7, :cond_0

    .line 2231176
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2231177
    :cond_4
    :try_start_2
    iget-object v0, p0, LX/FMr;->b:LX/FMw;

    .line 2231178
    iget-object v1, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2231179
    iget-wide v8, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    move-wide v2, v8

    .line 2231180
    invoke-virtual {v0, v1, v2, v3}, LX/FMw;->b(Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2231181
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_5

    .line 2231182
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 2231183
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_3
.end method
