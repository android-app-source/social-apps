.class public LX/FJv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final k:Ljava/lang/Object;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/2Of;

.field public final c:LX/3Rb;

.field private final d:LX/2Ou;

.field private final e:LX/2OT;

.field public final f:LX/FJw;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/3QW;

.field private final j:LX/3Rc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2223925
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FJv;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/2Of;LX/3Rb;LX/2Ou;LX/2OT;LX/FJw;LX/0Or;LX/0Ot;LX/3QW;LX/3Rc;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/2Of;",
            "LX/3Rb;",
            "LX/2Ou;",
            "LX/2OT;",
            "LX/FJw;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/3QW;",
            "LX/3Rc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2223926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223927
    iput-object p2, p0, LX/FJv;->b:LX/2Of;

    .line 2223928
    iput-object p3, p0, LX/FJv;->c:LX/3Rb;

    .line 2223929
    iput-object p4, p0, LX/FJv;->d:LX/2Ou;

    .line 2223930
    iput-object p5, p0, LX/FJv;->e:LX/2OT;

    .line 2223931
    iput-object p6, p0, LX/FJv;->f:LX/FJw;

    .line 2223932
    iput-object p7, p0, LX/FJv;->g:LX/0Or;

    .line 2223933
    iput-object p8, p0, LX/FJv;->h:LX/0Ot;

    .line 2223934
    iput-object p9, p0, LX/FJv;->i:LX/3QW;

    .line 2223935
    iput-object p1, p0, LX/FJv;->a:Landroid/content/res/Resources;

    .line 2223936
    iput-object p10, p0, LX/FJv;->j:LX/3Rc;

    .line 2223937
    return-void
.end method

.method public static a(LX/0QB;)LX/FJv;
    .locals 7

    .prologue
    .line 2223938
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2223939
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2223940
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2223941
    if-nez v1, :cond_0

    .line 2223942
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2223943
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2223944
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2223945
    sget-object v1, LX/FJv;->k:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2223946
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2223947
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2223948
    :cond_1
    if-nez v1, :cond_4

    .line 2223949
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2223950
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2223951
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FJv;->b(LX/0QB;)LX/FJv;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2223952
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2223953
    if-nez v1, :cond_2

    .line 2223954
    sget-object v0, LX/FJv;->k:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2223955
    :goto_1
    if-eqz v0, :cond_3

    .line 2223956
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2223957
    :goto_3
    check-cast v0, LX/FJv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2223958
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2223959
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2223960
    :catchall_1
    move-exception v0

    .line 2223961
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2223962
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2223963
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2223964
    :cond_2
    :try_start_8
    sget-object v0, LX/FJv;->k:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Ljava/util/List;I)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;I)",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;"
        }
    .end annotation

    .prologue
    .line 2223965
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 2223966
    :cond_0
    new-instance v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, p0, LX/FJv;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    move-object v0, v1

    .line 2223967
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/FJv;
    .locals 11

    .prologue
    .line 2223968
    new-instance v0, LX/FJv;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/2Of;->a(LX/0QB;)LX/2Of;

    move-result-object v2

    check-cast v2, LX/2Of;

    invoke-static {p0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v3

    check-cast v3, LX/3Rb;

    invoke-static {p0}, LX/2Ou;->a(LX/0QB;)LX/2Ou;

    move-result-object v4

    check-cast v4, LX/2Ou;

    invoke-static {p0}, LX/2OT;->b(LX/0QB;)LX/2OT;

    move-result-object v5

    check-cast v5, LX/2OT;

    invoke-static {p0}, LX/FJw;->a(LX/0QB;)LX/FJw;

    move-result-object v6

    check-cast v6, LX/FJw;

    const/16 v7, 0x12cd

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x259

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/3QW;->a(LX/0QB;)LX/3QW;

    move-result-object v9

    check-cast v9, LX/3QW;

    invoke-static {p0}, LX/3Rc;->a(LX/0QB;)LX/3Rc;

    move-result-object v10

    check-cast v10, LX/3Rc;

    invoke-direct/range {v0 .. v10}, LX/FJv;-><init>(Landroid/content/res/Resources;LX/2Of;LX/3Rb;LX/2Ou;LX/2OT;LX/FJw;LX/0Or;LX/0Ot;LX/3QW;LX/3Rc;)V

    .line 2223969
    return-object v0
.end method

.method private b(LX/CN8;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 2

    .prologue
    .line 2223970
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2223971
    iget v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    move v0, v1

    .line 2223972
    iget-object v1, p0, LX/FJv;->j:LX/3Rc;

    invoke-virtual {v1}, LX/3Rc;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2223973
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2223974
    iget v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    move v0, v1

    .line 2223975
    iput v0, p1, LX/CN8;->h:I

    .line 2223976
    :cond_0
    return-void
.end method

.method public static c(LX/FJv;Lcom/facebook/messaging/model/threads/ThreadSummary;)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 2223977
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2223978
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->s:Landroid/net/Uri;

    .line 2223979
    :goto_0
    return-object v0

    .line 2223980
    :cond_0
    iget-object v0, p0, LX/FJv;->e:LX/2OT;

    invoke-virtual {v0}, LX/2OT;->a()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2223981
    const-string v1, "tid"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "t_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2223982
    const-string v1, "hash"

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2223983
    const-string v1, "format"

    const-string v2, "binary"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2223984
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 2223985
    new-instance v0, LX/CN8;

    invoke-direct {v0}, LX/CN8;-><init>()V

    iget-object v1, p0, LX/FJv;->c:LX/3Rb;

    .line 2223986
    iput-object v1, v0, LX/CN8;->a:LX/3Rb;

    .line 2223987
    move-object v0, v0

    .line 2223988
    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2223989
    iput-object v1, v0, LX/CN8;->b:Ljava/lang/String;

    .line 2223990
    move-object v1, v0

    .line 2223991
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2223992
    :cond_0
    invoke-static {p0, p1}, LX/FJv;->c(LX/FJv;Lcom/facebook/messaging/model/threads/ThreadSummary;)Landroid/net/Uri;

    move-result-object v0

    .line 2223993
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2223994
    invoke-static {p0, p1}, LX/FJv;->c(LX/FJv;Lcom/facebook/messaging/model/threads/ThreadSummary;)Landroid/net/Uri;

    move-result-object v0

    .line 2223995
    iput-object v0, v1, LX/CN8;->c:Landroid/net/Uri;

    .line 2223996
    move-object v0, v1

    .line 2223997
    invoke-virtual {v0}, LX/CN8;->a()LX/CN9;

    move-result-object v0

    .line 2223998
    :goto_0
    return-object v0

    .line 2223999
    :cond_1
    const-string v2, "MessengerThreadTileViewDataFactory"

    const-string v3, "Uri is not absolute - Uri: %s TK: %s"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2224000
    :cond_2
    iget-object v0, p0, LX/FJv;->i:LX/3QW;

    invoke-virtual {v0, p1}, LX/3QW;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2224001
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    if-nez v0, :cond_f

    iget-object v0, p0, LX/FJv;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a019a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2224002
    :goto_1
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    .line 2224003
    iput-object v2, v1, LX/CN8;->g:Ljava/lang/String;

    .line 2224004
    move-object v2, v1

    .line 2224005
    invoke-virtual {v2}, LX/CN8;->a()LX/CN9;

    move-result-object v2

    .line 2224006
    new-instance v3, LX/8ud;

    iget-object v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    const/4 v5, -0x1

    invoke-direct {v3, v2, v4, v5, v0}, LX/8ud;-><init>(LX/8Vc;Ljava/lang/String;II)V

    move-object v0, v3

    .line 2224007
    goto :goto_0

    .line 2224008
    :cond_3
    iget-object v0, p0, LX/FJv;->b:LX/2Of;

    invoke-virtual {v0, p1}, LX/2Of;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/util/List;

    move-result-object v0

    .line 2224009
    invoke-static {p1}, LX/2Of;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;)I

    move-result v2

    .line 2224010
    if-ne v2, v5, :cond_4

    .line 2224011
    invoke-direct {p0, v0, v7}, LX/FJv;->a(Ljava/util/List;I)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    .line 2224012
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1, v2}, LX/CN8;->a(Lcom/facebook/user/model/UserKey;)LX/CN8;

    move-result-object v1

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    .line 2224013
    iput-object v0, v1, LX/CN8;->g:Ljava/lang/String;

    .line 2224014
    move-object v0, v1

    .line 2224015
    invoke-virtual {v0}, LX/CN8;->a()LX/CN9;

    move-result-object v0

    goto :goto_0

    .line 2224016
    :cond_4
    if-ne v2, v6, :cond_c

    .line 2224017
    iget-object v0, p0, LX/FJv;->d:LX/2Ou;

    invoke-virtual {v0, p1}, LX/2Ou;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    .line 2224018
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/CN8;->a(Lcom/facebook/user/model/UserKey;)LX/CN8;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f()Ljava/lang/String;

    move-result-object v3

    .line 2224019
    iput-object v3, v2, LX/CN8;->g:Ljava/lang/String;

    .line 2224020
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2224021
    iget-object v3, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v4, LX/5e9;->GROUP:LX/5e9;

    if-eq v3, v4, :cond_5

    .line 2224022
    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2224023
    sget-object v0, LX/8ue;->TINCAN:LX/8ue;

    .line 2224024
    iput-object v0, v1, LX/CN8;->f:LX/8ue;

    .line 2224025
    :cond_5
    :goto_2
    invoke-virtual {v1}, LX/CN8;->a()LX/CN9;

    move-result-object v0

    goto/16 :goto_0

    .line 2224026
    :cond_6
    invoke-static {v2}, LX/FNg;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2224027
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    .line 2224028
    iput-object v0, v1, LX/CN8;->f:LX/8ue;

    .line 2224029
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "res:///"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f021847

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2224030
    iput-object v0, v1, LX/CN8;->c:Landroid/net/Uri;

    .line 2224031
    iget-object v0, p0, LX/FJv;->j:LX/3Rc;

    invoke-virtual {v0}, LX/3Rc;->b()I

    move-result v0

    .line 2224032
    iput v0, v1, LX/CN8;->h:I

    .line 2224033
    goto :goto_2

    .line 2224034
    :cond_7
    invoke-static {v2}, LX/FNg;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2224035
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    .line 2224036
    iput-object v0, v1, LX/CN8;->f:LX/8ue;

    .line 2224037
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "res:///"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f02184b

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2224038
    iput-object v0, v1, LX/CN8;->c:Landroid/net/Uri;

    .line 2224039
    iget-object v0, p0, LX/FJv;->j:LX/3Rc;

    invoke-virtual {v0}, LX/3Rc;->b()I

    move-result v0

    .line 2224040
    iput v0, v1, LX/CN8;->h:I

    .line 2224041
    goto :goto_2

    .line 2224042
    :cond_8
    invoke-static {v2}, LX/FNg;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2224043
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    .line 2224044
    iput-object v0, v1, LX/CN8;->f:LX/8ue;

    .line 2224045
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "res:///"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f021848

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2224046
    iput-object v0, v1, LX/CN8;->c:Landroid/net/Uri;

    .line 2224047
    iget-object v0, p0, LX/FJv;->j:LX/3Rc;

    invoke-virtual {v0}, LX/3Rc;->b()I

    move-result v0

    .line 2224048
    iput v0, v1, LX/CN8;->h:I

    .line 2224049
    goto/16 :goto_2

    .line 2224050
    :cond_9
    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2224051
    sget-object v2, LX/8ue;->SMS:LX/8ue;

    .line 2224052
    iput-object v2, v1, LX/CN8;->f:LX/8ue;

    .line 2224053
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 2224054
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/CN8;->a(Lcom/facebook/user/model/UserKey;)LX/CN8;

    .line 2224055
    :cond_a
    invoke-direct {p0, v1, p1}, LX/FJv;->b(LX/CN8;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    goto/16 :goto_2

    .line 2224056
    :cond_b
    iget-object v2, p0, LX/FJv;->f:LX/FJw;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    .line 2224057
    iget-object v3, v2, LX/FJw;->d:LX/2Oi;

    invoke-virtual {v3, v0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v3

    .line 2224058
    if-eqz v3, :cond_10

    .line 2224059
    invoke-virtual {v2, v3}, LX/FJw;->b(Lcom/facebook/user/model/User;)LX/8ue;

    move-result-object v3

    .line 2224060
    :goto_3
    move-object v0, v3

    .line 2224061
    iput-object v0, v1, LX/CN8;->f:LX/8ue;

    .line 2224062
    goto/16 :goto_2

    .line 2224063
    :cond_c
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v6, :cond_d

    .line 2224064
    iget-object v0, p0, LX/FJv;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "MessengerThreadTileViewDataFactory_no_participants"

    const-string v3, "Creating a ThreadTileViewData for threadsummary with no participants!"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2224065
    iput-boolean v5, v1, LX/CN8;->d:Z

    .line 2224066
    move-object v0, v1

    .line 2224067
    invoke-virtual {v0}, LX/CN8;->a()LX/CN9;

    move-result-object v0

    goto/16 :goto_0

    .line 2224068
    :cond_d
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v3, LX/5e9;->SMS:LX/5e9;

    if-ne v2, v3, :cond_e

    .line 2224069
    sget-object v2, LX/8ue;->SMS:LX/8ue;

    .line 2224070
    iput-object v2, v1, LX/CN8;->f:LX/8ue;

    .line 2224071
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "res:///"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f021849

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2224072
    iput-object v2, v1, LX/CN8;->c:Landroid/net/Uri;

    .line 2224073
    invoke-direct {p0, v1, p1}, LX/FJv;->b(LX/CN8;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2224074
    :cond_e
    invoke-direct {p0, v0, v7}, LX/FJv;->a(Ljava/util/List;I)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0, v5}, LX/FJv;->a(Ljava/util/List;I)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0, v6}, LX/FJv;->a(Ljava/util/List;I)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v2, v3, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2224075
    iput-object v0, v1, LX/CN8;->e:LX/0Px;

    .line 2224076
    move-object v0, v1

    .line 2224077
    invoke-virtual {v0}, LX/CN8;->a()LX/CN9;

    move-result-object v0

    goto/16 :goto_0

    .line 2224078
    :cond_f
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    goto/16 :goto_1

    .line 2224079
    :cond_10
    iget-object v3, v2, LX/FJw;->a:LX/Dd0;

    invoke-virtual {v3}, LX/Dd0;->a()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v3

    if-nez v3, :cond_11

    .line 2224080
    sget-object v3, LX/8ue;->NONE:LX/8ue;

    goto/16 :goto_3

    .line 2224081
    :cond_11
    iget-object v3, v2, LX/FJw;->b:LX/3Kw;

    .line 2224082
    iget-object v4, v3, LX/3Kw;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2CH;

    invoke-virtual {v4, v0}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v4

    .line 2224083
    if-eqz v4, :cond_16

    .line 2224084
    iget-object v5, v4, LX/3Ox;->d:LX/03R;

    move-object v4, v5

    .line 2224085
    :goto_4
    invoke-virtual {v4}, LX/03R;->isSet()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 2224086
    invoke-virtual {v4}, LX/03R;->asBoolean()Z

    move-result v4

    .line 2224087
    :goto_5
    move v3, v4

    .line 2224088
    if-eqz v3, :cond_12

    .line 2224089
    sget-object v3, LX/8ue;->MESSENGER:LX/8ue;

    goto/16 :goto_3

    .line 2224090
    :cond_12
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 2224091
    sget-object v3, LX/8ue;->SMS:LX/8ue;

    goto/16 :goto_3

    .line 2224092
    :cond_13
    iget-object v3, v2, LX/FJw;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, v2, LX/FJw;->b:LX/3Kw;

    .line 2224093
    iget-object v4, v3, LX/3Kw;->c:LX/2Oi;

    invoke-virtual {v4, v0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v4

    .line 2224094
    if-nez v4, :cond_1a

    .line 2224095
    const/4 v4, 0x0

    .line 2224096
    :goto_6
    move v3, v4

    .line 2224097
    if-eqz v3, :cond_15

    :cond_14
    sget-object v3, LX/8ue;->NONE:LX/8ue;

    goto/16 :goto_3

    :cond_15
    sget-object v3, LX/8ue;->FACEBOOK:LX/8ue;

    goto/16 :goto_3

    .line 2224098
    :cond_16
    sget-object v4, LX/03R;->UNSET:LX/03R;

    goto :goto_4

    .line 2224099
    :cond_17
    iget-object v4, v3, LX/3Kw;->c:LX/2Oi;

    invoke-virtual {v4, v0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v4

    .line 2224100
    if-eqz v4, :cond_19

    .line 2224101
    iget-boolean v5, v4, Lcom/facebook/user/model/User;->s:Z

    move v5, v5

    .line 2224102
    if-nez v5, :cond_18

    .line 2224103
    iget-boolean v5, v4, Lcom/facebook/user/model/User;->t:Z

    move v4, v5

    .line 2224104
    if-eqz v4, :cond_19

    :cond_18
    const/4 v4, 0x1

    goto :goto_5

    :cond_19
    const/4 v4, 0x0

    goto :goto_5

    .line 2224105
    :cond_1a
    iget-boolean v3, v4, Lcom/facebook/user/model/User;->F:Z

    move v4, v3

    .line 2224106
    goto :goto_6
.end method

.method public final a(Lcom/facebook/user/model/User;)LX/8Vc;
    .locals 1

    .prologue
    .line 2224107
    iget-object v0, p0, LX/FJv;->f:LX/FJw;

    invoke-virtual {v0, p1}, LX/FJw;->b(Lcom/facebook/user/model/User;)LX/8ue;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/FJv;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8Vc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/user/model/User;LX/8ue;)LX/8Vc;
    .locals 3

    .prologue
    .line 2224108
    iget-object v0, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 2224109
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2224110
    iget-object v1, p1, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v1, v1

    .line 2224111
    if-eqz v1, :cond_0

    .line 2224112
    iget-object v0, p1, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 2224113
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v1

    .line 2224114
    :cond_0
    new-instance v1, LX/CN8;

    invoke-direct {v1}, LX/CN8;-><init>()V

    iget-object v2, p0, LX/FJv;->c:LX/3Rb;

    .line 2224115
    iput-object v2, v1, LX/CN8;->a:LX/3Rb;

    .line 2224116
    move-object v1, v1

    .line 2224117
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v2

    .line 2224118
    iput-object v2, v1, LX/CN8;->b:Ljava/lang/String;

    .line 2224119
    move-object v1, v1

    .line 2224120
    iget-object v2, p0, LX/FJv;->f:LX/FJw;

    invoke-virtual {v2, p1}, LX/FJw;->b(Lcom/facebook/user/model/User;)LX/8ue;

    move-result-object v2

    .line 2224121
    iput-object v2, v1, LX/CN8;->f:LX/8ue;

    .line 2224122
    move-object v1, v1

    .line 2224123
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2224124
    iput-object v0, v1, LX/CN8;->e:LX/0Px;

    .line 2224125
    move-object v0, v1

    .line 2224126
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    .line 2224127
    iput-object v1, v0, LX/CN8;->g:Ljava/lang/String;

    .line 2224128
    move-object v0, v0

    .line 2224129
    move-object v0, v0

    .line 2224130
    iput-object p2, v0, LX/CN8;->f:LX/8ue;

    .line 2224131
    move-object v0, v0

    .line 2224132
    invoke-virtual {v0}, LX/CN8;->a()LX/CN9;

    move-result-object v0

    return-object v0
.end method
