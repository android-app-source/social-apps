.class public abstract LX/Got;
.super LX/Goq;
.source ""

# interfaces
.implements LX/Gon;


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:Z

.field private final k:Ljava/lang/String;

.field private final l:LX/CHX;

.field private final m:LX/GoE;

.field private final n:LX/CHZ;

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLInterfaces$InstantShoppingAnnotationsFragment$;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/2uF;


# direct methods
.method public constructor <init>(LX/Gos;)V
    .locals 1

    .prologue
    .line 2394614
    iget-object v0, p1, LX/Gos;->d:LX/CHa;

    invoke-direct {p0, p1, v0}, LX/Goq;-><init>(LX/Gor;LX/CHa;)V

    .line 2394615
    iget-object v0, p1, LX/Gos;->h:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/Got;->a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394616
    iget-boolean v0, p1, LX/Gos;->i:Z

    iput-boolean v0, p0, LX/Got;->b:Z

    .line 2394617
    iget-boolean v0, p1, LX/Gos;->j:Z

    iput-boolean v0, p0, LX/Got;->c:Z

    .line 2394618
    iget-boolean v0, p1, LX/Gos;->k:Z

    iput-boolean v0, p0, LX/Got;->d:Z

    .line 2394619
    iget-boolean v0, p1, LX/Gos;->l:Z

    iput-boolean v0, p0, LX/Got;->e:Z

    .line 2394620
    iget-boolean v0, p1, LX/Gos;->m:Z

    iput-boolean v0, p0, LX/Got;->f:Z

    .line 2394621
    iget-boolean v0, p1, LX/Gos;->n:Z

    iput-boolean v0, p0, LX/Got;->g:Z

    .line 2394622
    iget-boolean v0, p1, LX/Gos;->o:Z

    iput-boolean v0, p0, LX/Got;->h:Z

    .line 2394623
    iget-boolean v0, p1, LX/Gos;->p:Z

    iput-boolean v0, p0, LX/Got;->i:Z

    .line 2394624
    iget-boolean v0, p1, LX/Gos;->q:Z

    iput-boolean v0, p0, LX/Got;->j:Z

    .line 2394625
    iget-object v0, p1, LX/Gos;->a:Ljava/lang/String;

    iput-object v0, p0, LX/Got;->k:Ljava/lang/String;

    .line 2394626
    iget-object v0, p1, LX/Gos;->b:LX/CHX;

    iput-object v0, p0, LX/Got;->l:LX/CHX;

    .line 2394627
    iget-object v0, p1, LX/Gos;->c:LX/GoE;

    iput-object v0, p0, LX/Got;->m:LX/GoE;

    .line 2394628
    iget-object v0, p1, LX/Gos;->r:LX/CHZ;

    iput-object v0, p0, LX/Got;->n:LX/CHZ;

    .line 2394629
    iget-object v0, p1, LX/Gos;->e:Ljava/util/List;

    iput-object v0, p0, LX/Got;->o:Ljava/util/List;

    .line 2394630
    iget-object v0, p1, LX/Gos;->f:Ljava/util/List;

    iput-object v0, p0, LX/Got;->p:Ljava/util/List;

    .line 2394631
    iget-object v0, p1, LX/Gos;->g:LX/2uF;

    iput-object v0, p0, LX/Got;->q:LX/2uF;

    .line 2394632
    return-void
.end method

.method public static b(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2394633
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final D()LX/GoE;
    .locals 1

    .prologue
    .line 2394634
    iget-object v0, p0, LX/Got;->m:LX/GoE;

    return-object v0
.end method

.method public final K()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLInterfaces$InstantShoppingAnnotationsFragment$;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2394635
    iget-object v0, p0, LX/Got;->o:Ljava/util/List;

    return-object v0
.end method

.method public final L()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2394636
    iget-object v0, p0, LX/Got;->p:Ljava/util/List;

    return-object v0
.end method

.method public final M()Z
    .locals 1

    .prologue
    .line 2394637
    iget-boolean v0, p0, LX/Got;->j:Z

    return v0
.end method

.method public final N()Z
    .locals 1

    .prologue
    .line 2394638
    iget-boolean v0, p0, LX/Got;->i:Z

    return v0
.end method

.method public final c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 2394639
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 2394640
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;
    .locals 1

    .prologue
    .line 2394641
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2394642
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAction()LX/CHX;
    .locals 1

    .prologue
    .line 2394599
    iget-object v0, p0, LX/Got;->l:LX/CHX;

    return-object v0
.end method

.method public final h()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .locals 1

    .prologue
    .line 2394643
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 2394644
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 2394613
    const/4 v0, 0x0

    return-object v0
.end method

.method public iY_()Z
    .locals 1

    .prologue
    .line 2394645
    iget-boolean v0, p0, LX/Got;->h:Z

    return v0
.end method

.method public final iZ_()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 2394598
    iget-object v0, p0, LX/Got;->a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 1

    .prologue
    .line 2394600
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1

    .prologue
    .line 2394601
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2394602
    const/4 v0, 0x0

    return v0
.end method

.method public final lA_()Z
    .locals 1

    .prologue
    .line 2394603
    iget-boolean v0, p0, LX/Got;->g:Z

    return v0
.end method

.method public final lB_()Z
    .locals 1

    .prologue
    .line 2394604
    iget-boolean v0, p0, LX/Got;->c:Z

    return v0
.end method

.method public final lC_()Z
    .locals 1

    .prologue
    .line 2394605
    iget-boolean v0, p0, LX/Got;->d:Z

    return v0
.end method

.method public final lF_()LX/2uF;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTouchTargets"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2394606
    iget-object v0, p0, LX/Got;->q:LX/2uF;

    return-object v0
.end method

.method public final ly_()Z
    .locals 1

    .prologue
    .line 2394607
    iget-boolean v0, p0, LX/Got;->e:Z

    return v0
.end method

.method public final lz_()Z
    .locals 1

    .prologue
    .line 2394608
    iget-boolean v0, p0, LX/Got;->f:Z

    return v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 2394609
    iget-object v0, p0, LX/Got;->a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2394610
    iget-boolean v0, p0, LX/Got;->b:Z

    return v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394611
    iget-object v0, p0, LX/Got;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final z()LX/CHZ;
    .locals 1

    .prologue
    .line 2394612
    iget-object v0, p0, LX/Got;->n:LX/CHZ;

    return-object v0
.end method
