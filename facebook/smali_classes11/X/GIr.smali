.class public LX/GIr;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/GIa;",
        "D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "LX/GHg",
        "<TT;TD;>;"
    }
.end annotation


# static fields
.field public static final n:LX/1jt;

.field public static final o:LX/1jt;

.field public static final p:LX/1jt;


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/GIa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/GG6;

.field public i:LX/GL2;

.field public j:Z

.field public k:LX/0W9;

.field public l:LX/GKy;

.field public m:LX/2U3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2337220
    new-instance v0, LX/GIi;

    invoke-direct {v0}, LX/GIi;-><init>()V

    sput-object v0, LX/GIr;->n:LX/1jt;

    .line 2337221
    new-instance v0, LX/GIj;

    invoke-direct {v0}, LX/GIj;-><init>()V

    sput-object v0, LX/GIr;->o:LX/1jt;

    .line 2337222
    new-instance v0, LX/GIk;

    invoke-direct {v0}, LX/GIk;-><init>()V

    sput-object v0, LX/GIr;->p:LX/1jt;

    return-void
.end method

.method public constructor <init>(LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337223
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2337224
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2337225
    iput-object v0, p0, LX/GIr;->b:LX/0Px;

    .line 2337226
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2337227
    iput-object v0, p0, LX/GIr;->c:LX/0Px;

    .line 2337228
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2337229
    iput-object v0, p0, LX/GIr;->d:LX/0Px;

    .line 2337230
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2337231
    iput-object v0, p0, LX/GIr;->e:LX/0Px;

    .line 2337232
    iput-object p1, p0, LX/GIr;->h:LX/GG6;

    .line 2337233
    iput-object p2, p0, LX/GIr;->i:LX/GL2;

    .line 2337234
    iput-object p5, p0, LX/GIr;->k:LX/0W9;

    .line 2337235
    iput-object p3, p0, LX/GIr;->l:LX/GKy;

    .line 2337236
    iput-object p4, p0, LX/GIr;->m:LX/2U3;

    .line 2337237
    return-void
.end method

.method public static a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2

    .prologue
    .line 2337238
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080a49

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f021961

    invoke-virtual {p0, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    .line 2337239
    return-void
.end method

.method private f()Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;
    .locals 2

    .prologue
    .line 2337240
    sget-object v0, LX/GIh;->b:[I

    iget-object v1, p0, LX/GIr;->f:LX/GIa;

    .line 2337241
    iget-object p0, v1, LX/GIa;->i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    move-object v1, p0

    .line 2337242
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->getSelectedGender()LX/ACy;

    move-result-object v1

    invoke-virtual {v1}, LX/ACy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2337243
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    :goto_0
    return-object v0

    .line 2337244
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->MALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    goto :goto_0

    .line 2337245
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2337246
    invoke-super {p0}, LX/GHg;->a()V

    .line 2337247
    iput-object v0, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2337248
    iput-object v0, p0, LX/GIr;->f:LX/GIa;

    .line 2337249
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_0

    .line 2337250
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2337251
    :cond_0
    return-void
.end method

.method public a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2337252
    invoke-super {p0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2337253
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2337254
    return-void
.end method

.method public a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2337255
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337256
    iput-object p1, p0, LX/GIr;->f:LX/GIa;

    .line 2337257
    iput-object p2, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2337258
    iget-object v0, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz v0, :cond_0

    .line 2337259
    iget-object v0, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0}, LX/GIr;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337260
    :cond_0
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_1

    .line 2337261
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    iget-object v1, p0, LX/GIr;->f:LX/GIa;

    .line 2337262
    iget-object p0, v1, LX/GIa;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    move-object v1, p0

    .line 2337263
    invoke-virtual {v0, v1, p2}, LX/GL2;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337264
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2337265
    const-string v0, "targeting_data"

    invoke-virtual {p0}, LX/GIr;->c()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2337266
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_0

    .line 2337267
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    invoke-virtual {v0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2337268
    :cond_0
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2337269
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337270
    check-cast p1, LX/GIa;

    invoke-virtual {p0, p1, p2}, LX/GIr;->a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 2337271
    iput-object p1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2337272
    const/4 v0, 0x0

    .line 2337273
    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->PROMOTE_CTA:LX/8wL;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v1, v2, :cond_1

    .line 2337274
    :cond_0
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2337275
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2337276
    sget-short v2, LX/GDK;->h:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 2337277
    iput-boolean v0, p0, LX/GIr;->j:Z

    .line 2337278
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_2

    .line 2337279
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    invoke-virtual {v0, p1}, LX/GL2;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2337280
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V
    .locals 3

    .prologue
    .line 2337161
    if-nez p1, :cond_0

    .line 2337162
    :goto_0
    return-void

    .line 2337163
    :cond_0
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2337164
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->j:LX/0Px;

    move-object v0, v0

    .line 2337165
    if-eqz v0, :cond_1

    .line 2337166
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->j:LX/0Px;

    move-object v0, v0

    .line 2337167
    iput-object v0, p0, LX/GIr;->c:LX/0Px;

    .line 2337168
    :cond_1
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v0, v0

    .line 2337169
    if-eqz v0, :cond_2

    .line 2337170
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v0, v0

    .line 2337171
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GIr;->c:LX/0Px;

    iput-object v0, p0, LX/GIr;->b:LX/0Px;

    .line 2337172
    iget-object v0, p0, LX/GIr;->c:LX/0Px;

    sget-object v1, LX/GIr;->n:LX/1jt;

    invoke-virtual {p0, v0, v1}, LX/GIr;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2337173
    :cond_2
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_3

    .line 2337174
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v0, v0

    .line 2337175
    if-eqz v0, :cond_3

    .line 2337176
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v0, v0

    .line 2337177
    sget-object v1, LX/GGG;->REGION:LX/GGG;

    if-ne v0, v1, :cond_6

    .line 2337178
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    sget-object v1, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a:LX/Hed;

    invoke-virtual {v0, v1}, LX/GL2;->a(LX/Hed;)V

    .line 2337179
    :cond_3
    :goto_1
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    move-object v0, v0

    .line 2337180
    if-eqz v0, :cond_4

    .line 2337181
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    move-object v0, v0

    .line 2337182
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GIr;->d:LX/0Px;

    .line 2337183
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    iget-object v1, p0, LX/GIr;->d:LX/0Px;

    sget-object v2, LX/GIr;->o:LX/1jt;

    invoke-virtual {v0, v1, v2}, LX/GIa;->b(Ljava/lang/Iterable;LX/1jt;)V

    .line 2337184
    :cond_4
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    move-object v0, v0

    .line 2337185
    if-eqz v0, :cond_5

    .line 2337186
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    move-object v0, v0

    .line 2337187
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GIr;->e:LX/0Px;

    .line 2337188
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    iget-object v1, p0, LX/GIr;->e:LX/0Px;

    sget-object v2, LX/GIr;->p:LX/1jt;

    invoke-virtual {v0, v1, v2}, LX/GIa;->c(Ljava/lang/Iterable;LX/1jt;)V

    .line 2337189
    :cond_5
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    .line 2337190
    iget-object v1, v0, LX/GIa;->c:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    move-object v0, v1

    .line 2337191
    const/16 v1, 0xd

    const/16 v2, 0x41

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a(II)V

    .line 2337192
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    .line 2337193
    iget-object v1, v0, LX/GIa;->c:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    move-object v0, v1

    .line 2337194
    iget v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    move v1, v1

    .line 2337195
    iget v2, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v2, v2

    .line 2337196
    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b(II)V

    .line 2337197
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-object v0, v0

    .line 2337198
    sget-object v1, LX/GIh;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2337199
    :goto_2
    goto/16 :goto_0

    .line 2337200
    :cond_6
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    sget-object v1, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->b:LX/Hed;

    invoke-virtual {v0, v1}, LX/GL2;->a(LX/Hed;)V

    .line 2337201
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    .line 2337202
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v1

    .line 2337203
    invoke-virtual {v0, v1}, LX/GL2;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V

    goto :goto_1

    .line 2337204
    :pswitch_0
    iget-object v1, p0, LX/GIr;->f:LX/GIa;

    .line 2337205
    iget-object v2, v1, LX/GIa;->i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    move-object v1, v2

    .line 2337206
    sget-object v2, LX/ACy;->ALL:LX/ACy;

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->setGender(LX/ACy;)V

    goto :goto_2

    .line 2337207
    :pswitch_1
    iget-object v1, p0, LX/GIr;->f:LX/GIa;

    .line 2337208
    iget-object v2, v1, LX/GIa;->i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    move-object v1, v2

    .line 2337209
    sget-object v2, LX/ACy;->MALE:LX/ACy;

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->setGender(LX/ACy;)V

    goto :goto_2

    .line 2337210
    :pswitch_2
    iget-object v1, p0, LX/GIr;->f:LX/GIa;

    .line 2337211
    iget-object v2, v1, LX/GIa;->i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    move-object v1, v2

    .line 2337212
    sget-object v2, LX/ACy;->FEMALE:LX/ACy;

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->setGender(LX/ACy;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Iterable;LX/1jt;)V
    .locals 2

    .prologue
    .line 2337213
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    .line 2337214
    iget-object v1, v0, LX/GIa;->l:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {v1, p1, p2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2337215
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_0

    .line 2337216
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    .line 2337217
    iget-object v1, v0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    move-object v0, v1

    .line 2337218
    invoke-virtual {v0, p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2337219
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2337154
    if-nez p1, :cond_0

    .line 2337155
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/GIa;->setGenderViewVisibility(I)V

    .line 2337156
    :goto_0
    return-void

    .line 2337157
    :cond_0
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/GIa;->setGenderViewVisibility(I)V

    .line 2337158
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    .line 2337159
    iget-object v1, v0, LX/GIa;->i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    move-object v0, v1

    .line 2337160
    new-instance v1, LX/GIl;

    invoke-direct {v1, p0}, LX/GIl;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2337144
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {p0}, LX/GIr;->c()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2337145
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337146
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2337147
    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2337148
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337149
    new-instance v1, LX/GFr;

    iget-object v2, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GFr;-><init>(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2337150
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337151
    new-instance v1, LX/GFp;

    invoke-direct {v1}, LX/GFp;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2337152
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/GIr;->f(Z)V

    .line 2337153
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2337138
    if-nez p1, :cond_0

    .line 2337139
    :goto_0
    return-void

    .line 2337140
    :cond_0
    const-string v0, "targeting_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-virtual {p0, v0}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2337141
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_1

    .line 2337142
    iget-object v0, p0, LX/GIr;->i:LX/GL2;

    invoke-virtual {v0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2337143
    :cond_1
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 2337122
    if-nez p1, :cond_1

    .line 2337123
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {v0, v2}, LX/GIa;->setLocationsSelectorVisibility(I)V

    .line 2337124
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {v0, v2}, LX/GIa;->setRegionSelectorVisibility(I)V

    .line 2337125
    :cond_0
    :goto_0
    return-void

    .line 2337126
    :cond_1
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_2

    .line 2337127
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {v0, v2}, LX/GIa;->setLocationsSelectorVisibility(I)V

    .line 2337128
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {v0, v1}, LX/GIa;->setRegionSelectorVisibility(I)V

    .line 2337129
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2337130
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v0, v1

    .line 2337131
    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    if-ne v0, v1, :cond_0

    .line 2337132
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {v0, v2}, LX/GIa;->setInterestsDividerVisibility(I)V

    goto :goto_0

    .line 2337133
    :cond_2
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {v0, v1}, LX/GIa;->setLocationsSelectorVisibility(I)V

    .line 2337134
    new-instance v0, LX/GIm;

    invoke-direct {v0, p0}, LX/GIm;-><init>(LX/GIr;)V

    .line 2337135
    iget-object v1, p0, LX/GIr;->f:LX/GIa;

    .line 2337136
    iget-object p0, v1, LX/GIa;->l:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2337137
    goto :goto_0
.end method

.method public c()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2337102
    new-instance v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-direct {p0}, LX/GIr;->f()Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-result-object v1

    iget-object v2, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2337103
    iget v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    move v2, v3

    .line 2337104
    iget-object v3, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    .line 2337105
    iget v4, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v3, v4

    .line 2337106
    iget-object v4, p0, LX/GIr;->c:LX/0Px;

    iget-object v5, p0, LX/GIr;->b:LX/0Px;

    iget-object v6, p0, LX/GIr;->d:LX/0Px;

    iget-object v7, p0, LX/GIr;->e:LX/0Px;

    iget-object v8, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v8}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v8

    .line 2337107
    iget-object v9, v8, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v8, v9

    .line 2337108
    iget-object v9, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v9}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v9

    .line 2337109
    iget-object v10, v9, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v9, v10

    .line 2337110
    iget-object v10, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v10}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v10

    .line 2337111
    iget-object v11, v10, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v10, v11

    .line 2337112
    iget-object v11, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v11}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v11

    .line 2337113
    iget-object v12, v11, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v11, v12

    .line 2337114
    invoke-direct/range {v0 .. v11}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;-><init>(Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;IILX/0Px;LX/0Px;LX/0Px;LX/0Px;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;LX/GGG;LX/0Px;)V

    .line 2337115
    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2337116
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v2

    .line 2337117
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2337118
    iget-object v1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2337119
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v1, v2

    .line 2337120
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    .line 2337121
    return-object v0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 2337095
    if-nez p1, :cond_0

    .line 2337096
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/GIa;->setInterestsSelectorVisibility(I)V

    .line 2337097
    :goto_0
    return-void

    .line 2337098
    :cond_0
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/GIa;->setInterestsSelectorVisibility(I)V

    .line 2337099
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    new-instance v1, LX/GIn;

    invoke-direct {v1, p0}, LX/GIn;-><init>(LX/GIr;)V

    .line 2337100
    iget-object p0, v0, LX/GIa;->n:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2337101
    goto :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 2337079
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337080
    const/4 v1, 0x0

    new-instance v2, LX/GIq;

    invoke-direct {v2, p0}, LX/GIq;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2337081
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337082
    const/4 v1, 0x1

    new-instance v2, LX/GIb;

    invoke-direct {v2, p0}, LX/GIb;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2337083
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337084
    const/4 v1, 0x2

    new-instance v2, LX/GIc;

    invoke-direct {v2, p0}, LX/GIc;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2337085
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337086
    new-instance v1, LX/GId;

    invoke-direct {v1, p0}, LX/GId;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2337087
    iget-boolean v0, p0, LX/GIr;->j:Z

    if-eqz v0, :cond_0

    .line 2337088
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337089
    const/4 v1, 0x0

    new-instance v2, LX/GIe;

    invoke-direct {v2, p0}, LX/GIe;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2337090
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337091
    new-instance v1, LX/GIf;

    invoke-direct {v1, p0}, LX/GIf;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2337092
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337093
    new-instance v1, LX/GIg;

    invoke-direct {v1, p0}, LX/GIg;-><init>(LX/GIr;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2337094
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 2337072
    if-nez p1, :cond_0

    .line 2337073
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/GIa;->setDetailedTargetingSelectorVisibility(I)V

    .line 2337074
    :goto_0
    return-void

    .line 2337075
    :cond_0
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/GIa;->setDetailedTargetingSelectorVisibility(I)V

    .line 2337076
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    new-instance v1, LX/GIo;

    invoke-direct {v1, p0}, LX/GIo;-><init>(LX/GIr;)V

    .line 2337077
    iget-object p0, v0, LX/GIa;->h:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2337078
    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 2337069
    iget-object v0, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz v0, :cond_0

    .line 2337070
    iget-object v0, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0}, LX/GMo;->a(Landroid/view/View;)V

    .line 2337071
    :cond_0
    return-void
.end method

.method public final e(Z)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2337056
    iget-object v3, p0, LX/GIr;->f:LX/GIa;

    if-eqz p1, :cond_0

    move v0, v1

    .line 2337057
    :goto_0
    iget-object v4, v3, LX/GIa;->c:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    invoke-virtual {v4, v0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->setVisibility(I)V

    .line 2337058
    iget-object v4, v3, LX/GIa;->e:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2337059
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, LX/GIa;->setAgeViewTopDividerVisibility(I)V

    .line 2337060
    if-nez p1, :cond_2

    .line 2337061
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 2337062
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2337063
    goto :goto_1

    .line 2337064
    :cond_2
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    .line 2337065
    iget-object v1, v0, LX/GIa;->c:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    move-object v0, v1

    .line 2337066
    new-instance v1, LX/GIp;

    invoke-direct {v1, p0}, LX/GIp;-><init>(LX/GIr;)V

    .line 2337067
    iput-object v1, v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->h:LX/ACw;

    .line 2337068
    goto :goto_2
.end method

.method public final f(Z)V
    .locals 5

    .prologue
    .line 2337036
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    instance-of v0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-eq v0, v1, :cond_1

    .line 2337037
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v0

    .line 2337038
    sget-object v2, LX/GG8;->UNEDITED_DATA:LX/GG8;

    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0, p1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)Z

    move-result v0

    invoke-virtual {v1, v2, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2337039
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v0

    .line 2337040
    sget-object v2, LX/GG8;->INVALID_LOCATION:LX/GG8;

    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2337041
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object p0

    .line 2337042
    iget-object p1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object p0, p1

    .line 2337043
    sget-object p1, LX/GGG;->ADDRESS:LX/GGG;

    if-ne p0, p1, :cond_3

    .line 2337044
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object p0

    .line 2337045
    iget-object p1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object p0, p1

    .line 2337046
    if-eqz p0, :cond_2

    .line 2337047
    :cond_0
    :goto_0
    move v0, v3

    .line 2337048
    invoke-virtual {v1, v2, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2337049
    :cond_1
    return-void

    :cond_2
    move v3, v4

    .line 2337050
    goto :goto_0

    .line 2337051
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object p0

    .line 2337052
    iget-object p1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object p0, p1

    .line 2337053
    if-eqz p0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object p0

    .line 2337054
    iget-object p1, p0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object p0, p1

    .line 2337055
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    :cond_4
    move v3, v4

    goto :goto_0
.end method
