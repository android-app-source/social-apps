.class public LX/GmS;
.super LX/0ht;
.source ""

# interfaces
.implements LX/0kr;
.implements LX/02k;


# instance fields
.field public a:LX/0dx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2392039
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2392040
    const-class v0, LX/GmS;

    invoke-static {v0, p0}, LX/GmS;->a(Ljava/lang/Class;LX/02k;)V

    .line 2392041
    const/4 p1, 0x0

    .line 2392042
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03060c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2392043
    invoke-virtual {p0, v1}, LX/0ht;->d(Landroid/view/View;)V

    .line 2392044
    const v0, 0x7f0d10c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GmS;->l:Landroid/widget/TextView;

    .line 2392045
    const v0, 0x7f0d10c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GmS;->m:Landroid/widget/TextView;

    .line 2392046
    const v0, 0x7f0d054d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GmS;->n:Landroid/view/View;

    .line 2392047
    const v0, 0x7f0d10c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2392048
    new-instance v1, LX/GmR;

    invoke-direct {v1, p0}, LX/GmR;-><init>(LX/GmS;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2392049
    sget-object v0, LX/3AV;->CENTER:LX/3AV;

    invoke-virtual {p0, v0}, LX/0ht;->a(LX/3AV;)V

    .line 2392050
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0ht;->d(Z)V

    .line 2392051
    iput-boolean p1, p0, LX/0ht;->w:Z

    .line 2392052
    iput-boolean p1, p0, LX/0ht;->x:Z

    .line 2392053
    iget-object v0, p0, LX/GmS;->a:LX/0dx;

    invoke-interface {v0, p0}, LX/0dx;->a(LX/0kr;)V

    .line 2392054
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GmS;

    invoke-static {p0}, LX/0dt;->b(LX/0QB;)LX/0dx;

    move-result-object p0

    check-cast p0, LX/0dx;

    iput-object p0, p1, LX/GmS;->a:LX/0dx;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2392038
    return-void
.end method

.method public final a(LX/4ce;LX/4cd;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2392022
    const v0, 0x7f08007b

    .line 2392023
    if-eqz p1, :cond_0

    .line 2392024
    iget v0, p1, LX/4ce;->resId:I

    .line 2392025
    :cond_0
    const v1, 0x7f08007b

    .line 2392026
    if-eqz p2, :cond_1

    .line 2392027
    iget v1, p2, LX/4cd;->resId:I

    .line 2392028
    :cond_1
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2392029
    iget-object v4, p0, LX/GmS;->l:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Orbot: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2392030
    iget-object v0, p0, LX/GmS;->m:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Tor: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2392031
    invoke-virtual {p1}, LX/4ce;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p2, :cond_2

    invoke-virtual {p2}, LX/4cd;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    .line 2392032
    :goto_0
    iget-object v1, p0, LX/GmS;->n:Landroid/view/View;

    if-eqz v0, :cond_3

    const/16 v2, 0x8

    :cond_3
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2392033
    return-void

    :cond_4
    move v0, v2

    .line 2392034
    goto :goto_0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2392035
    invoke-super {p0}, LX/0ht;->l()V

    .line 2392036
    iget-object v0, p0, LX/GmS;->a:LX/0dx;

    invoke-interface {v0, p0}, LX/0dx;->b(LX/0kr;)V

    .line 2392037
    return-void
.end method
