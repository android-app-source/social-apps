.class public final LX/F6G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/react/GroupsReactDataFetcher$1;)V
    .locals 0

    .prologue
    .line 2200319
    iput-object p1, p0, LX/F6G;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2200320
    iget-object v0, p0, LX/F6G;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    iget-object v0, v0, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;->d:LX/F6J;

    iget-object v0, v0, LX/F6J;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3H7;

    iget-object v1, p0, LX/F6G;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    iget-object v1, v1, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;->a:Ljava/lang/String;

    iget-object v2, p0, LX/F6G;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    iget-object v2, v2, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;->b:LX/5Go;

    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v0, v1, v2, v3}, LX/3H7;->a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
