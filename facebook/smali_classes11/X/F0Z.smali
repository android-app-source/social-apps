.class public final LX/F0Z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2189964
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2189965
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189966
    :goto_0
    return v1

    .line 2189967
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189968
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2189969
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2189970
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2189971
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2189972
    const-string v5, "key"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2189973
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2189974
    :cond_2
    const-string v5, "subtitle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2189975
    const/4 v4, 0x0

    .line 2189976
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_9

    .line 2189977
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189978
    :goto_2
    move v2, v4

    .line 2189979
    goto :goto_1

    .line 2189980
    :cond_3
    const-string v5, "title"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2189981
    const/4 v4, 0x0

    .line 2189982
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_d

    .line 2189983
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189984
    :goto_3
    move v0, v4

    .line 2189985
    goto :goto_1

    .line 2189986
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2189987
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2189988
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2189989
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2189990
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 2189991
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189992
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2189993
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2189994
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2189995
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 2189996
    const-string v6, "text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2189997
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_4

    .line 2189998
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2189999
    invoke-virtual {p1, v4, v2}, LX/186;->b(II)V

    .line 2190000
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_9
    move v2, v4

    goto :goto_4

    .line 2190001
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2190002
    :cond_b
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_c

    .line 2190003
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2190004
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2190005
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_b

    if-eqz v5, :cond_b

    .line 2190006
    const-string v6, "text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2190007
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 2190008
    :cond_c
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2190009
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2190010
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_d
    move v0, v4

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2190011
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2190012
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2190013
    if-eqz v0, :cond_0

    .line 2190014
    const-string v1, "key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190015
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2190016
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2190017
    if-eqz v0, :cond_2

    .line 2190018
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190019
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2190020
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2190021
    if-eqz v1, :cond_1

    .line 2190022
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190023
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2190024
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2190025
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2190026
    if-eqz v0, :cond_4

    .line 2190027
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190028
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2190029
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2190030
    if-eqz v1, :cond_3

    .line 2190031
    const-string p1, "text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2190032
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2190033
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2190034
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2190035
    return-void
.end method
