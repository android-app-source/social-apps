.class public LX/F5M;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKContactsManager"
.end annotation


# instance fields
.field public a:LX/3Oq;

.field public b:LX/3LP;

.field public c:LX/0TD;

.field public d:LX/1Ck;

.field public e:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;LX/3Oq;LX/3LP;LX/0TD;LX/1Ck;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198491
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2198492
    iput-object p2, p0, LX/F5M;->a:LX/3Oq;

    .line 2198493
    iput-object p3, p0, LX/F5M;->b:LX/3LP;

    .line 2198494
    iput-object p4, p0, LX/F5M;->c:LX/0TD;

    .line 2198495
    iput-object p5, p0, LX/F5M;->d:LX/1Ck;

    .line 2198496
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2198497
    const-string v0, "RKContactsManager"

    return-object v0
.end method

.method public readAll(Lcom/facebook/react/bridge/Callback;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198498
    iget-object v0, p0, LX/F5M;->d:LX/1Ck;

    sget-object v1, LX/F5L;->READ_CONTACTS_DB:LX/F5L;

    iget-object v2, p0, LX/F5M;->c:LX/0TD;

    new-instance v3, LX/F5J;

    invoke-direct {v3, p0, p1}, LX/F5J;-><init>(LX/F5M;Lcom/facebook/react/bridge/Callback;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/F5K;

    invoke-direct {v3, p0, p1}, LX/F5K;-><init>(LX/F5M;Lcom/facebook/react/bridge/Callback;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2198499
    return-void
.end method
