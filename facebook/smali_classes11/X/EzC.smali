.class public final LX/EzC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/PersonYouMayKnow;

.field public final synthetic b:LX/2iS;


# direct methods
.method public constructor <init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 0

    .prologue
    .line 2186937
    iput-object p1, p0, LX/EzC;->b:LX/2iS;

    iput-object p2, p0, LX/EzC;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x552bb56e

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186938
    iget-object v1, p0, LX/EzC;->b:LX/2iS;

    iget-object v1, v1, LX/2iS;->j:LX/2hd;

    iget-object v2, p0, LX/EzC;->b:LX/2iS;

    iget-object v2, v2, LX/2iS;->o:Ljava/lang/String;

    iget-object v3, p0, LX/EzC;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 2186939
    iget-object v4, v3, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    move-object v3, v4

    .line 2186940
    iget-object v4, p0, LX/EzC;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v4}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    iget-object v6, p0, LX/EzC;->b:LX/2iS;

    iget-object v6, v6, LX/2iS;->l:LX/2h7;

    iget-object v6, v6, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->c(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2186941
    iget-object v1, p0, LX/EzC;->b:LX/2iS;

    iget-object v2, p0, LX/EzC;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 2186942
    invoke-static {v1}, LX/2iS;->c(LX/2iS;)V

    .line 2186943
    iget-object v8, v1, LX/2iS;->m:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2186944
    iget-object v8, v1, LX/2iS;->n:LX/2ia;

    invoke-virtual {v2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v10

    invoke-interface {v8, v10, v11}, LX/2ia;->b(J)V

    .line 2186945
    iget-object v8, v1, LX/2iS;->d:LX/2dj;

    invoke-virtual {v2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v10

    iget-object v9, v1, LX/2iS;->l:LX/2h7;

    iget-object v9, v9, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual {v8, v10, v11, v9}, LX/2dj;->a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2186946
    iget-object v8, v1, LX/2iS;->e:LX/2do;

    new-instance v9, LX/2iB;

    invoke-virtual {v2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, LX/2iB;-><init>(J)V

    invoke-virtual {v8, v9}, LX/0b4;->a(LX/0b7;)V

    .line 2186947
    const v1, 0x284f942f

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
