.class public LX/GTg;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile b:LX/GTg;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2355761
    sget-object v0, LX/0ax;->ea:Ljava/lang/String;

    const-string v1, "{source unknown}"

    const-string v2, "{!redirect_after_accept false}"

    const-string v3, "{nux_type nearby_friends_undecided}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GTg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2355762
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2355763
    sget-object v0, LX/GTg;->a:Ljava/lang/String;

    const-class v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2355764
    return-void
.end method

.method public static a(LX/0QB;)LX/GTg;
    .locals 3

    .prologue
    .line 2355765
    sget-object v0, LX/GTg;->b:LX/GTg;

    if-nez v0, :cond_1

    .line 2355766
    const-class v1, LX/GTg;

    monitor-enter v1

    .line 2355767
    :try_start_0
    sget-object v0, LX/GTg;->b:LX/GTg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2355768
    if-eqz v2, :cond_0

    .line 2355769
    :try_start_1
    new-instance v0, LX/GTg;

    invoke-direct {v0}, LX/GTg;-><init>()V

    .line 2355770
    move-object v0, v0

    .line 2355771
    sput-object v0, LX/GTg;->b:LX/GTg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2355772
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2355773
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2355774
    :cond_1
    sget-object v0, LX/GTg;->b:LX/GTg;

    return-object v0

    .line 2355775
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2355776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
