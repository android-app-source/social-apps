.class public abstract LX/Goq;
.super LX/Gop;
.source ""

# interfaces
.implements LX/GoX;
.implements LX/Goa;
.implements LX/Gob;


# instance fields
.field private final a:LX/CHa;


# direct methods
.method public constructor <init>(LX/CHa;II)V
    .locals 0

    .prologue
    .line 2394553
    invoke-direct {p0, p2, p3}, LX/Gop;-><init>(II)V

    .line 2394554
    iput-object p1, p0, LX/Goq;->a:LX/CHa;

    .line 2394555
    return-void
.end method

.method public constructor <init>(LX/Gor;LX/CHa;)V
    .locals 0

    .prologue
    .line 2394550
    invoke-direct {p0, p1}, LX/Gop;-><init>(LX/Gor;)V

    .line 2394551
    iput-object p2, p0, LX/Goq;->a:LX/CHa;

    .line 2394552
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394549
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    invoke-interface {v0}, LX/CHa;->ju_()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394548
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    invoke-interface {v0}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lD_()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394545
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    if-eqz v0, :cond_0

    .line 2394546
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    invoke-interface {v0}, LX/CHa;->d()Ljava/lang/String;

    move-result-object v0

    .line 2394547
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lE_()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394556
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    if-eqz v0, :cond_2

    .line 2394557
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    invoke-interface {v0}, LX/CHa;->a()Ljava/lang/String;

    move-result-object v0

    .line 2394558
    if-eqz v0, :cond_0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2394559
    :cond_0
    :goto_0
    return-object v0

    .line 2394560
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2394561
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394544
    const/4 v0, 0x0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394541
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    if-eqz v0, :cond_0

    .line 2394542
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    invoke-interface {v0}, LX/CHa;->c()Ljava/lang/String;

    move-result-object v0

    .line 2394543
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394538
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    if-eqz v0, :cond_0

    .line 2394539
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    invoke-interface {v0}, LX/CHa;->e()Ljava/lang/String;

    move-result-object v0

    .line 2394540
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394535
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    if-eqz v0, :cond_0

    .line 2394536
    iget-object v0, p0, LX/Goq;->a:LX/CHa;

    invoke-interface {v0}, LX/CHa;->b()Ljava/lang/String;

    move-result-object v0

    .line 2394537
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
