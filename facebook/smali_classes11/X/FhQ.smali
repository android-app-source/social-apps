.class public LX/FhQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/7BK;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/7BK;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile j:LX/FhQ;


# instance fields
.field public final a:LX/0TV;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FhW;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FhT;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z

.field private final e:LX/8ht;

.field private final f:LX/0Uh;

.field private final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2272891
    sget-object v0, LX/7BK;->SHORTCUT:LX/7BK;

    sget-object v1, LX/7BK;->APP:LX/7BK;

    sget-object v2, LX/7BK;->USER:LX/7BK;

    sget-object v3, LX/7BK;->PAGE:LX/7BK;

    sget-object v4, LX/7BK;->GROUP:LX/7BK;

    sget-object v5, LX/7BK;->EVENT:LX/7BK;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/FhQ;->h:LX/0Px;

    .line 2272892
    sget-object v0, LX/7BK;->USER:LX/7BK;

    sget-object v1, LX/7BK;->GROUP:LX/7BK;

    sget-object v2, LX/7BK;->EVENT:LX/7BK;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/FhQ;->i:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0TV;Ljava/lang/Boolean;LX/8ht;LX/0Uh;LX/0ad;LX/0Or;LX/0Or;)V
    .locals 1
    .param p1    # LX/0TV;
        .annotation runtime Lcom/facebook/common/executors/SearchTypeaheadNetworkExecutor;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TV;",
            "Ljava/lang/Boolean;",
            "LX/8ht;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/FhW;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FhT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273009
    iput-object p1, p0, LX/FhQ;->a:LX/0TV;

    .line 2273010
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/FhQ;->d:Z

    .line 2273011
    iput-object p3, p0, LX/FhQ;->e:LX/8ht;

    .line 2273012
    iput-object p4, p0, LX/FhQ;->f:LX/0Uh;

    .line 2273013
    iput-object p5, p0, LX/FhQ;->g:LX/0ad;

    .line 2273014
    iput-object p6, p0, LX/FhQ;->b:LX/0Or;

    .line 2273015
    iput-object p7, p0, LX/FhQ;->c:LX/0Or;

    .line 2273016
    return-void
.end method

.method private static a(LX/7BZ;I)I
    .locals 1

    .prologue
    .line 2273007
    sget-object v0, LX/7BZ;->VIDEO_HOME_SEARCH_KEYWORD_ONLY_MODE:LX/7BZ;

    if-ne p0, v0, :cond_0

    const/4 p1, 0x6

    :cond_0
    return p1
.end method

.method private a(LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;)LX/7BZ;
    .locals 3

    .prologue
    .line 2272994
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 2272995
    sget-object v1, LX/103;->URL:LX/103;

    if-ne v0, v1, :cond_1

    .line 2272996
    sget-object p1, LX/7BZ;->WEB_VIEW_SINGLE_STATE_MODE:LX/7BZ;

    .line 2272997
    :cond_0
    :goto_0
    return-object p1

    .line 2272998
    :cond_1
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 2272999
    sget-object v1, LX/103;->VIDEO:LX/103;

    if-ne v0, v1, :cond_2

    .line 2273000
    sget-object p1, LX/7BZ;->VIDEO_HOME_SEARCH_KEYWORD_ONLY_MODE:LX/7BZ;

    goto :goto_0

    .line 2273001
    :cond_2
    iget-object v0, p0, LX/FhQ;->e:LX/8ht;

    invoke-virtual {v0, p2}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2273002
    sget-object p1, LX/7BZ;->SCOPED:LX/7BZ;

    goto :goto_0

    .line 2273003
    :cond_3
    iget-object v0, p0, LX/FhQ;->e:LX/8ht;

    invoke-virtual {v0, p2}, LX/8ht;->i(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2273004
    sget-object p1, LX/7BZ;->SINGLE_STATE_MODE:LX/7BZ;

    goto :goto_0

    .line 2273005
    :cond_4
    iget-object v0, p0, LX/FhQ;->f:LX/0Uh;

    sget v1, LX/2SU;->y:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273006
    sget-object p1, LX/7BZ;->ENTITY_ONLY_MODE:LX/7BZ;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FhQ;
    .locals 11

    .prologue
    .line 2272981
    sget-object v0, LX/FhQ;->j:LX/FhQ;

    if-nez v0, :cond_1

    .line 2272982
    const-class v1, LX/FhQ;

    monitor-enter v1

    .line 2272983
    :try_start_0
    sget-object v0, LX/FhQ;->j:LX/FhQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2272984
    if-eqz v2, :cond_0

    .line 2272985
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2272986
    new-instance v3, LX/FhQ;

    invoke-static {v0}, LX/44k;->a(LX/0QB;)LX/0TV;

    move-result-object v4

    check-cast v4, LX/0TV;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-static {v0}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v6

    check-cast v6, LX/8ht;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    const/16 v9, 0x34c1

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x34c0

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/FhQ;-><init>(LX/0TV;Ljava/lang/Boolean;LX/8ht;LX/0Uh;LX/0ad;LX/0Or;LX/0Or;)V

    .line 2272987
    move-object v0, v3

    .line 2272988
    sput-object v0, LX/FhQ;->j:LX/FhQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2272989
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2272990
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2272991
    :cond_1
    sget-object v0, LX/FhQ;->j:LX/FhQ;

    return-object v0

    .line 2272992
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2272993
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/7BS;Lcom/facebook/search/api/GraphSearchQuery;ILjava/lang/String;LX/7BZ;Ljava/lang/String;)V
    .locals 2
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2272961
    iput-object p2, p1, LX/7BS;->a:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272962
    move-object v0, p1

    .line 2272963
    iput p3, v0, LX/7BS;->e:I

    .line 2272964
    move-object v0, v0

    .line 2272965
    iput-object p4, v0, LX/7BS;->b:Ljava/lang/String;

    .line 2272966
    move-object v1, v0

    .line 2272967
    iget-boolean v0, p0, LX/FhQ;->d:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/FhQ;->i:LX/0Px;

    .line 2272968
    :goto_0
    iput-object v0, v1, LX/7BS;->d:Ljava/util/List;

    .line 2272969
    move-object v0, v1

    .line 2272970
    const/4 v1, 0x0

    .line 2272971
    iput-boolean v1, v0, LX/7BS;->g:Z

    .line 2272972
    move-object v0, v0

    .line 2272973
    invoke-static {}, LX/0wB;->b()I

    move-result v1

    .line 2272974
    iput v1, v0, LX/7BS;->c:I

    .line 2272975
    move-object v0, v0

    .line 2272976
    iput-object p5, v0, LX/7BS;->i:LX/7BZ;

    .line 2272977
    if-eqz p6, :cond_0

    .line 2272978
    iput-object p6, p1, LX/7BS;->h:Ljava/lang/String;

    .line 2272979
    :cond_0
    return-void

    .line 2272980
    :cond_1
    sget-object v0, LX/FhQ;->h:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7BZ;LX/7B6;ILjava/lang/String;Ljava/lang/String;LX/4d9;LX/7HY;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7BZ;",
            "LX/7B6;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/4d9;",
            "LX/7HY;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2272905
    move-object v2, p2

    check-cast v2, Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272906
    invoke-direct {p0, p1, v2}, LX/FhQ;->a(LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;)LX/7BZ;

    move-result-object v5

    .line 2272907
    invoke-static {v5, p3}, LX/FhQ;->a(LX/7BZ;I)I

    move-result v3

    .line 2272908
    new-instance v1, LX/7BT;

    invoke-direct {v1}, LX/7BT;-><init>()V

    move-object v0, p0

    move-object v4, p4

    move-object v6, p5

    .line 2272909
    invoke-direct/range {v0 .. v6}, LX/FhQ;->a(LX/7BS;Lcom/facebook/search/api/GraphSearchQuery;ILjava/lang/String;LX/7BZ;Ljava/lang/String;)V

    .line 2272910
    iget-object v0, p0, LX/FhQ;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhT;

    .line 2272911
    iget-object v2, v0, LX/FhT;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    iget-object v3, v0, LX/FhT;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v1, v2, v3}, LX/FhX;->a(LX/7BS;LX/0Uh;LX/0ad;)V

    .line 2272912
    iget-object v2, v1, LX/7BS;->i:LX/7BZ;

    move-object v6, v2

    .line 2272913
    invoke-static {v0, v6}, LX/FhT;->a(LX/FhT;LX/7BZ;)Ljava/util/ArrayList;

    move-result-object p0

    .line 2272914
    iget-object v2, v0, LX/FhT;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/18V;

    invoke-virtual {v2}, LX/18V;->a()LX/2VK;

    move-result-object v2

    sget-object v3, LX/2VL;->STREAMING:LX/2VL;

    invoke-interface {v2, v3}, LX/2VK;->a(LX/2VL;)LX/2VK;

    move-result-object p1

    .line 2272915
    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    .line 2272916
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2272917
    iput v2, p6, LX/4d9;->a:I

    .line 2272918
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_5

    .line 2272919
    iget-object v2, v0, LX/FhT;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fhc;

    invoke-virtual {v2, p7, p2, v4}, LX/Fhc;->b(LX/7HY;LX/7B6;I)V

    .line 2272920
    iput v4, v1, LX/7BT;->a:I

    .line 2272921
    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FhS;

    .line 2272922
    iget v3, v2, LX/FhS;->a:I

    move p4, v3

    .line 2272923
    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FhS;

    .line 2272924
    iget-object v3, v2, LX/FhS;->b:LX/7BZ;

    move-object v5, v3

    .line 2272925
    if-eqz v5, :cond_3

    .line 2272926
    iput-object v5, v1, LX/7BS;->i:LX/7BZ;

    .line 2272927
    if-eq v5, v6, :cond_6

    .line 2272928
    iget-object v2, v0, LX/FhT;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    iget-object v3, v0, LX/FhT;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v1, v2, v3}, LX/FhX;->a(LX/7BS;LX/0Uh;LX/0ad;)V

    move-object v3, v5

    .line 2272929
    :goto_1
    iget-object v2, v1, LX/7BS;->i:LX/7BZ;

    move-object v2, v2

    .line 2272930
    sget-object v5, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    if-ne v2, v5, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v4, v2, :cond_0

    .line 2272931
    const/4 v2, 0x1

    .line 2272932
    iput-boolean v2, v1, LX/7BT;->d:Z

    .line 2272933
    :cond_0
    iget-object v2, v1, LX/7BS;->i:LX/7BZ;

    move-object v2, v2

    .line 2272934
    sget-object v5, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    if-ne v2, v5, :cond_1

    .line 2272935
    iget-object v2, v0, LX/FhT;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-char v5, LX/100;->co:C

    .line 2272936
    iget-object p5, v1, LX/7BS;->j:Ljava/lang/String;

    move-object p5, p5

    .line 2272937
    invoke-interface {v2, v5, p5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2272938
    iput-object v2, v1, LX/7BS;->j:Ljava/lang/String;

    .line 2272939
    :cond_1
    iput p4, v1, LX/7BT;->b:I

    .line 2272940
    invoke-interface {p3, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2272941
    :goto_2
    iput v2, v1, LX/7BT;->c:I

    .line 2272942
    add-int/2addr v2, p4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p3, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2272943
    invoke-virtual {v1}, LX/7BT;->a()Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;

    move-result-object v3

    .line 2272944
    iget-object v2, v0, LX/FhT;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-static {v2, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "graphsearch_typeahead"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2272945
    iput-object v3, v2, LX/2Vk;->c:Ljava/lang/String;

    .line 2272946
    move-object v2, v2

    .line 2272947
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/2Vk;->a(Z)LX/2Vk;

    move-result-object v2

    .line 2272948
    iput-object p6, v2, LX/2Vk;->f:LX/4cn;

    .line 2272949
    move-object v2, v2

    .line 2272950
    if-lez v4, :cond_2

    .line 2272951
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "graphsearch_typeahead"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v4, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2272952
    iput-object v3, v2, LX/2Vk;->d:Ljava/lang/String;

    .line 2272953
    :cond_2
    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    invoke-interface {p1, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 2272954
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    :cond_3
    move-object v3, v6

    .line 2272955
    goto/16 :goto_1

    .line 2272956
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 2272957
    :cond_5
    move-object v3, p1

    .line 2272958
    iget-object v2, v0, LX/FhT;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0TV;

    new-instance v4, Lcom/facebook/search/suggestions/fetchers/RemoteTypeaheadLoaderBatchMethod$1;

    invoke-direct {v4, v0, v3}, Lcom/facebook/search/suggestions/fetchers/RemoteTypeaheadLoaderBatchMethod$1;-><init>(LX/FhT;LX/2VK;)V

    invoke-interface {v2, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2272959
    new-instance v3, LX/FhR;

    invoke-direct {v3, v0, v1}, LX/FhR;-><init>(LX/FhT;LX/7BS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2272960
    return-object v0

    :cond_6
    move-object v3, v5

    goto/16 :goto_1
.end method

.method public final a(LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;ILjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7BZ;",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2272893
    invoke-direct {p0, p1, p2}, LX/FhQ;->a(LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;)LX/7BZ;

    move-result-object v5

    .line 2272894
    invoke-static {v5, p3}, LX/FhQ;->a(LX/7BZ;I)I

    move-result v3

    .line 2272895
    new-instance v1, LX/7BS;

    invoke-direct {v1}, LX/7BS;-><init>()V

    move-object v0, p0

    move-object v2, p2

    move-object v4, p4

    move-object v6, p5

    .line 2272896
    invoke-direct/range {v0 .. v6}, LX/FhQ;->a(LX/7BS;Lcom/facebook/search/api/GraphSearchQuery;ILjava/lang/String;LX/7BZ;Ljava/lang/String;)V

    .line 2272897
    iget-object v0, p0, LX/FhQ;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhW;

    .line 2272898
    iget-object v2, v0, LX/FhW;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    iget-object v3, v0, LX/FhW;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v1, v2, v3}, LX/FhX;->a(LX/7BS;LX/0Uh;LX/0ad;)V

    .line 2272899
    invoke-virtual {v1}, LX/7BS;->f()Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    move-result-object v3

    .line 2272900
    iget-object v2, v0, LX/FhW;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, v0, LX/FhW;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    iget-object v4, v0, LX/FhW;->f:LX/0Or;

    iget-object v5, v0, LX/FhW;->g:LX/0Or;

    .line 2272901
    sget v6, LX/2SU;->e:I

    invoke-virtual {v2, v6}, LX/0Uh;->a(I)LX/03R;

    move-result-object v6

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, LX/03R;->asBoolean(Z)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    move-object v4, v5

    .line 2272902
    iget-object v2, v0, LX/FhW;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0TV;

    new-instance v5, LX/FhU;

    invoke-direct {v5, v0, v4, v3}, LX/FhU;-><init>(LX/FhW;LX/0Or;Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)V

    invoke-interface {v2, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2272903
    new-instance v4, LX/FhV;

    invoke-direct {v4, v0, v3}, LX/FhV;-><init>(LX/FhW;Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v2, v4, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2272904
    return-object v0

    :cond_0
    move-object v5, v4

    goto :goto_0
.end method
