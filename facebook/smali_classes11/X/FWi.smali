.class public LX/FWi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/195;

.field private b:I


# direct methods
.method public constructor <init>(LX/193;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2252481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252482
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "saved_dash_items_scroll_perf"

    invoke-virtual {p1, v0, v1}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, LX/FWi;->a:LX/195;

    .line 2252483
    iput v2, p0, LX/FWi;->b:I

    .line 2252484
    return-void
.end method

.method public static a(LX/0QB;)LX/FWi;
    .locals 4

    .prologue
    .line 2252470
    const-class v1, LX/FWi;

    monitor-enter v1

    .line 2252471
    :try_start_0
    sget-object v0, LX/FWi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2252472
    sput-object v2, LX/FWi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2252473
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2252474
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2252475
    new-instance p0, LX/FWi;

    const-class v3, LX/193;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/193;

    invoke-direct {p0, v3}, LX/FWi;-><init>(LX/193;)V

    .line 2252476
    move-object v0, p0

    .line 2252477
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2252478
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FWi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252479
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2252480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2252462
    iget v0, p0, LX/FWi;->b:I

    if-nez v0, :cond_1

    .line 2252463
    iget-object v0, p0, LX/FWi;->a:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    .line 2252464
    :cond_0
    :goto_0
    iput p2, p0, LX/FWi;->b:I

    .line 2252465
    return-void

    .line 2252466
    :cond_1
    if-nez p2, :cond_0

    .line 2252467
    iget-object v0, p0, LX/FWi;->a:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 2252468
    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 0

    .prologue
    .line 2252469
    return-void
.end method
