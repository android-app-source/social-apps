.class public LX/FKP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/AddPinnedThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225081
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 2225070
    check-cast p1, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;

    .line 2225071
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2225072
    const-string v0, "t_%s/pin"

    .line 2225073
    iget-object v6, p1, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v6, :cond_0

    .line 2225074
    iget-object v6, p1, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->b:Ljava/lang/String;

    .line 2225075
    :goto_0
    move-object v1, v6

    .line 2225076
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2225077
    new-instance v0, LX/14N;

    const-string v1, "addPinnedThread"

    const-string v2, "POST"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "t_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p1, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v7}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225078
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225079
    const/4 v0, 0x0

    return-object v0
.end method
