.class public final enum LX/Gru;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gru;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gru;

.field public static final enum CUSTOM_APP_EVENTS:LX/Gru;

.field public static final enum MOBILE_INSTALL_EVENT:LX/Gru;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2398843
    new-instance v0, LX/Gru;

    const-string v1, "MOBILE_INSTALL_EVENT"

    invoke-direct {v0, v1, v2}, LX/Gru;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gru;->MOBILE_INSTALL_EVENT:LX/Gru;

    .line 2398844
    new-instance v0, LX/Gru;

    const-string v1, "CUSTOM_APP_EVENTS"

    invoke-direct {v0, v1, v3}, LX/Gru;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gru;->CUSTOM_APP_EVENTS:LX/Gru;

    .line 2398845
    const/4 v0, 0x2

    new-array v0, v0, [LX/Gru;

    sget-object v1, LX/Gru;->MOBILE_INSTALL_EVENT:LX/Gru;

    aput-object v1, v0, v2

    sget-object v1, LX/Gru;->CUSTOM_APP_EVENTS:LX/Gru;

    aput-object v1, v0, v3

    sput-object v0, LX/Gru;->$VALUES:[LX/Gru;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2398842
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gru;
    .locals 1

    .prologue
    .line 2398841
    const-class v0, LX/Gru;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gru;

    return-object v0
.end method

.method public static values()[LX/Gru;
    .locals 1

    .prologue
    .line 2398840
    sget-object v0, LX/Gru;->$VALUES:[LX/Gru;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gru;

    return-object v0
.end method
