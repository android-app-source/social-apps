.class public LX/FY4;
.super LX/FXy;
.source ""


# instance fields
.field public final a:LX/19w;

.field public final b:LX/0tQ;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/1A0;

.field private final e:Landroid/view/View;

.field private f:D


# direct methods
.method public constructor <init>(LX/19w;LX/0tQ;LX/0Ot;Landroid/view/View;)V
    .locals 2
    .param p4    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            "LX/0tQ;",
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255739
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255740
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/FY4;->f:D

    .line 2255741
    iput-object p1, p0, LX/FY4;->a:LX/19w;

    .line 2255742
    iput-object p2, p0, LX/FY4;->b:LX/0tQ;

    .line 2255743
    iput-object p3, p0, LX/FY4;->c:LX/0Ot;

    .line 2255744
    iput-object p4, p0, LX/FY4;->e:Landroid/view/View;

    .line 2255745
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2255746
    const v0, 0x7f020818

    return v0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 6

    .prologue
    .line 2255747
    invoke-interface {p1}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v0

    .line 2255748
    iget-object v1, p0, LX/FY4;->a:LX/19w;

    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    .line 2255749
    iget-object v2, v1, LX/2fs;->c:LX/1A0;

    iput-object v2, p0, LX/FY4;->d:LX/1A0;

    .line 2255750
    iget-wide v2, v1, LX/2fs;->a:J

    const-wide/16 v4, 0xa

    mul-long/2addr v2, v4

    long-to-double v2, v2

    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    div-double/2addr v2, v4

    iput-wide v2, p0, LX/FY4;->f:D

    .line 2255751
    invoke-interface {p1}, LX/BO1;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2255752
    :goto_0
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255753
    return-object p0

    .line 2255754
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2255755
    sget-object v0, LX/FY3;->a:[I

    iget-object v1, p0, LX/FY4;->d:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2255756
    const v0, 0x7f081ae2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2255757
    :pswitch_0
    sget-object v0, LX/FY3;->b:[I

    iget-object v1, p0, LX/FY4;->b:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2255758
    const v0, 0x7f081add

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v0, v0

    .line 2255759
    goto :goto_0

    .line 2255760
    :pswitch_1
    sget-object v0, LX/FY3;->b:[I

    iget-object v1, p0, LX/FY4;->b:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 2255761
    const v0, 0x7f081ad0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 2255762
    goto :goto_0

    .line 2255763
    :pswitch_2
    const v0, 0x7f081adc

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2255764
    :pswitch_3
    const v0, 0x7f081ade

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2255765
    :pswitch_4
    const v0, 0x7f081ace

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2255766
    :pswitch_5
    const v0, 0x7f081acf

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255767
    const-string v0, "delete_button"

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2255768
    iget-object v0, p0, LX/FY4;->d:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/FY4;->d:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-wide v0, p0, LX/FY4;->f:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    .line 2255769
    :cond_0
    invoke-super {p0, p1}, LX/FXy;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2255770
    :goto_0
    return-object v0

    :cond_1
    const v0, 0x7f081ad1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, LX/FY4;->f:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/BO1;)Z
    .locals 12

    .prologue
    .line 2255771
    iget-object v0, p0, LX/FY4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FZ3;

    invoke-interface {p1}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v3

    iget-object v4, p0, LX/FY4;->e:Landroid/view/View;

    .line 2255772
    iget-object v5, v0, LX/FZ3;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    move-object v11, v5

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/saved2/ui/listener/Saved2ItemActionHelper$2;

    move-object v6, v0

    move-object v7, v2

    move-object v8, v4

    move v9, v3

    move-object v10, v1

    invoke-direct/range {v5 .. v10}, Lcom/facebook/saved2/ui/listener/Saved2ItemActionHelper$2;-><init>(LX/FZ3;Ljava/lang/String;Landroid/view/View;ZLjava/lang/String;)V

    const v6, -0x4fddea14

    invoke-static {v11, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2255773
    const/4 v0, 0x1

    return v0
.end method
