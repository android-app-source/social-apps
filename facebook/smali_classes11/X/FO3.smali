.class public final enum LX/FO3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FO3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FO3;

.field public static final enum BABYGIGGLE:LX/FO3;

.field public static final enum BIRDIE:LX/FO3;

.field public static final enum BLING:LX/FO3;

.field public static final enum BLIPBEEP:LX/FO3;

.field public static final enum CHIME:LX/FO3;

.field public static final enum CRICKETS:LX/FO3;

.field public static final enum DOGBARK:LX/FO3;

.field public static final enum DOUBLEKNOCK:LX/FO3;

.field public static final enum DOUBLEPOP:LX/FO3;

.field public static final enum DREAMY:LX/FO3;

.field public static final enum FANFARE:LX/FO3;

.field public static final enum HELLO:LX/FO3;

.field public static final enum MESSAGEKID:LX/FO3;

.field public static final enum OPENUP:LX/FO3;

.field public static final enum ORCHESTRAHIT:LX/FO3;

.field public static final enum PING:LX/FO3;

.field public static final enum PULSE:LX/FO3;

.field public static final enum RESONATE:LX/FO3;

.field public static final enum RIMSHOT:LX/FO3;

.field public static final enum RINGRING:LX/FO3;

.field public static final enum RIPPLE:LX/FO3;

.field public static final enum SINGLEPOP:LX/FO3;

.field public static final enum SIZZLE:LX/FO3;

.field public static final enum TAP:LX/FO3;

.field public static final enum TRIPLEPOP:LX/FO3;

.field public static final enum VIBRATION:LX/FO3;

.field public static final enum WHISTLE:LX/FO3;

.field public static final enum ZIPZAP:LX/FO3;


# instance fields
.field public nameResId:I

.field public rawResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2234148
    new-instance v0, LX/FO3;

    const-string v1, "BABYGIGGLE"

    const v2, 0x7f0803af

    const v3, 0x7f070002

    invoke-direct {v0, v1, v5, v2, v3}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->BABYGIGGLE:LX/FO3;

    .line 2234149
    new-instance v0, LX/FO3;

    const-string v1, "BIRDIE"

    const v2, 0x7f0803b0

    const v3, 0x7f070003

    invoke-direct {v0, v1, v6, v2, v3}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->BIRDIE:LX/FO3;

    .line 2234150
    new-instance v0, LX/FO3;

    const-string v1, "BLING"

    const v2, 0x7f0803b1

    const v3, 0x7f070006

    invoke-direct {v0, v1, v7, v2, v3}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->BLING:LX/FO3;

    .line 2234151
    new-instance v0, LX/FO3;

    const-string v1, "BLIPBEEP"

    const v2, 0x7f0803b2

    const v3, 0x7f070007

    invoke-direct {v0, v1, v8, v2, v3}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->BLIPBEEP:LX/FO3;

    .line 2234152
    new-instance v0, LX/FO3;

    const-string v1, "CHIME"

    const v2, 0x7f0803b3

    const v3, 0x7f07000d

    invoke-direct {v0, v1, v9, v2, v3}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->CHIME:LX/FO3;

    .line 2234153
    new-instance v0, LX/FO3;

    const-string v1, "CRICKETS"

    const/4 v2, 0x5

    const v3, 0x7f0803b4

    const v4, 0x7f07001a

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->CRICKETS:LX/FO3;

    .line 2234154
    new-instance v0, LX/FO3;

    const-string v1, "DOGBARK"

    const/4 v2, 0x6

    const v3, 0x7f0803b5

    const v4, 0x7f070022

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->DOGBARK:LX/FO3;

    .line 2234155
    new-instance v0, LX/FO3;

    const-string v1, "DOUBLEKNOCK"

    const/4 v2, 0x7

    const v3, 0x7f0803b6

    const v4, 0x7f070025

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->DOUBLEKNOCK:LX/FO3;

    .line 2234156
    new-instance v0, LX/FO3;

    const-string v1, "DOUBLEPOP"

    const/16 v2, 0x8

    const v3, 0x7f0803b7

    const v4, 0x7f070026

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->DOUBLEPOP:LX/FO3;

    .line 2234157
    new-instance v0, LX/FO3;

    const-string v1, "DREAMY"

    const/16 v2, 0x9

    const v3, 0x7f0803b8

    const v4, 0x7f070027

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->DREAMY:LX/FO3;

    .line 2234158
    new-instance v0, LX/FO3;

    const-string v1, "FANFARE"

    const/16 v2, 0xa

    const v3, 0x7f0803b9

    const v4, 0x7f07002d

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->FANFARE:LX/FO3;

    .line 2234159
    new-instance v0, LX/FO3;

    const-string v1, "HELLO"

    const/16 v2, 0xb

    const v3, 0x7f0803ba

    const v4, 0x7f070035

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->HELLO:LX/FO3;

    .line 2234160
    new-instance v0, LX/FO3;

    const-string v1, "MESSAGEKID"

    const/16 v2, 0xc

    const v3, 0x7f0803bb

    const v4, 0x7f07003c

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->MESSAGEKID:LX/FO3;

    .line 2234161
    new-instance v0, LX/FO3;

    const-string v1, "OPENUP"

    const/16 v2, 0xd

    const v3, 0x7f0803bc

    const v4, 0x7f070042

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->OPENUP:LX/FO3;

    .line 2234162
    new-instance v0, LX/FO3;

    const-string v1, "ORCHESTRAHIT"

    const/16 v2, 0xe

    const v3, 0x7f0803bd

    const v4, 0x7f070046

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->ORCHESTRAHIT:LX/FO3;

    .line 2234163
    new-instance v0, LX/FO3;

    const-string v1, "PING"

    const/16 v2, 0xf

    const v3, 0x7f0803be

    const v4, 0x7f070052

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->PING:LX/FO3;

    .line 2234164
    new-instance v0, LX/FO3;

    const-string v1, "PULSE"

    const/16 v2, 0x10

    const v3, 0x7f0803bf

    const v4, 0x7f07005c

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->PULSE:LX/FO3;

    .line 2234165
    new-instance v0, LX/FO3;

    const-string v1, "RESONATE"

    const/16 v2, 0x11

    const v3, 0x7f0803c0

    const v4, 0x7f07006c

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->RESONATE:LX/FO3;

    .line 2234166
    new-instance v0, LX/FO3;

    const-string v1, "RIMSHOT"

    const/16 v2, 0x12

    const v3, 0x7f0803c1

    const v4, 0x7f07006d

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->RIMSHOT:LX/FO3;

    .line 2234167
    new-instance v0, LX/FO3;

    const-string v1, "RINGRING"

    const/16 v2, 0x13

    const v3, 0x7f0803c2

    const v4, 0x7f07006e

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->RINGRING:LX/FO3;

    .line 2234168
    new-instance v0, LX/FO3;

    const-string v1, "RIPPLE"

    const/16 v2, 0x14

    const v3, 0x7f0803c3

    const v4, 0x7f07006f

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->RIPPLE:LX/FO3;

    .line 2234169
    new-instance v0, LX/FO3;

    const-string v1, "SINGLEPOP"

    const/16 v2, 0x15

    const v3, 0x7f0803c4

    const v4, 0x7f07007e

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->SINGLEPOP:LX/FO3;

    .line 2234170
    new-instance v0, LX/FO3;

    const-string v1, "SIZZLE"

    const/16 v2, 0x16

    const v3, 0x7f0803c5

    const v4, 0x7f07007f

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->SIZZLE:LX/FO3;

    .line 2234171
    new-instance v0, LX/FO3;

    const-string v1, "TAP"

    const/16 v2, 0x17

    const v3, 0x7f0803c6

    const v4, 0x7f070087

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->TAP:LX/FO3;

    .line 2234172
    new-instance v0, LX/FO3;

    const-string v1, "TRIPLEPOP"

    const/16 v2, 0x18

    const v3, 0x7f0803c7

    const v4, 0x7f07008e

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->TRIPLEPOP:LX/FO3;

    .line 2234173
    new-instance v0, LX/FO3;

    const-string v1, "VIBRATION"

    const/16 v2, 0x19

    const v3, 0x7f0803c8

    const v4, 0x7f070090

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->VIBRATION:LX/FO3;

    .line 2234174
    new-instance v0, LX/FO3;

    const-string v1, "WHISTLE"

    const/16 v2, 0x1a

    const v3, 0x7f0803c9

    const v4, 0x7f07009e

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->WHISTLE:LX/FO3;

    .line 2234175
    new-instance v0, LX/FO3;

    const-string v1, "ZIPZAP"

    const/16 v2, 0x1b

    const v3, 0x7f0803ca

    const v4, 0x7f07009f

    invoke-direct {v0, v1, v2, v3, v4}, LX/FO3;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FO3;->ZIPZAP:LX/FO3;

    .line 2234176
    const/16 v0, 0x1c

    new-array v0, v0, [LX/FO3;

    sget-object v1, LX/FO3;->BABYGIGGLE:LX/FO3;

    aput-object v1, v0, v5

    sget-object v1, LX/FO3;->BIRDIE:LX/FO3;

    aput-object v1, v0, v6

    sget-object v1, LX/FO3;->BLING:LX/FO3;

    aput-object v1, v0, v7

    sget-object v1, LX/FO3;->BLIPBEEP:LX/FO3;

    aput-object v1, v0, v8

    sget-object v1, LX/FO3;->CHIME:LX/FO3;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/FO3;->CRICKETS:LX/FO3;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FO3;->DOGBARK:LX/FO3;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FO3;->DOUBLEKNOCK:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FO3;->DOUBLEPOP:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FO3;->DREAMY:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/FO3;->FANFARE:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/FO3;->HELLO:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/FO3;->MESSAGEKID:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/FO3;->OPENUP:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/FO3;->ORCHESTRAHIT:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/FO3;->PING:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/FO3;->PULSE:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/FO3;->RESONATE:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/FO3;->RIMSHOT:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/FO3;->RINGRING:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/FO3;->RIPPLE:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/FO3;->SINGLEPOP:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/FO3;->SIZZLE:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/FO3;->TAP:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/FO3;->TRIPLEPOP:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/FO3;->VIBRATION:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/FO3;->WHISTLE:LX/FO3;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/FO3;->ZIPZAP:LX/FO3;

    aput-object v2, v0, v1

    sput-object v0, LX/FO3;->$VALUES:[LX/FO3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2234177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2234178
    iput p3, p0, LX/FO3;->nameResId:I

    .line 2234179
    iput p4, p0, LX/FO3;->rawResId:I

    .line 2234180
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FO3;
    .locals 1

    .prologue
    .line 2234181
    const-class v0, LX/FO3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FO3;

    return-object v0
.end method

.method public static values()[LX/FO3;
    .locals 1

    .prologue
    .line 2234182
    sget-object v0, LX/FO3;->$VALUES:[LX/FO3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FO3;

    return-object v0
.end method


# virtual methods
.method public final getUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x2f

    .line 2234183
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android.resource://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, LX/FO3;->rawResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, LX/FO3;->rawResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, LX/FO3;->rawResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
