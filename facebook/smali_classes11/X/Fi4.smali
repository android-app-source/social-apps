.class public final LX/Fi4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Cwv;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fi5;


# direct methods
.method public constructor <init>(LX/Fi5;)V
    .locals 0

    .prologue
    .line 2274122
    iput-object p1, p0, LX/Fi4;->a:LX/Fi5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2274123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fi4;->a:LX/Fi5;

    const/4 v1, 0x0

    .line 2274124
    iput-object v1, v0, LX/Fi5;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2274125
    iget-object v0, p0, LX/Fi4;->a:LX/Fi5;

    iget-object v0, v0, LX/Fi5;->a:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_WEBVIEW_SCOPED_SEARCH_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2274126
    monitor-exit p0

    return-void

    .line 2274127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2274128
    check-cast p1, LX/Cwv;

    .line 2274129
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fi4;->a:LX/Fi5;

    const/4 v1, 0x0

    .line 2274130
    iput-object v1, v0, LX/Fi5;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2274131
    if-nez p1, :cond_0

    .line 2274132
    iget-object v0, p0, LX/Fi4;->a:LX/Fi5;

    iget-object v0, v0, LX/Fi5;->a:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_WEBVIEW_SCOPED_SEARCH_FAIL:LX/3Ql;

    const-string v2, "The result for single state search in web view is null"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->b(LX/3Ql;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2274133
    :goto_0
    monitor-exit p0

    return-void

    .line 2274134
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Fi4;->a:LX/Fi5;

    .line 2274135
    iput-object p1, v0, LX/Fi5;->d:LX/Cwv;

    .line 2274136
    iget-object v1, v0, LX/Fi5;->f:LX/2SR;

    if-eqz v1, :cond_1

    .line 2274137
    iget-object v1, v0, LX/Fi5;->f:LX/2SR;

    sget-object v2, LX/7BE;->READY:LX/7BE;

    invoke-interface {v1, v2}, LX/2SR;->a(LX/7BE;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2274138
    :cond_1
    goto :goto_0

    .line 2274139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
