.class public LX/FUo;
.super Landroid/widget/HorizontalScrollView;
.source ""

# interfaces
.implements LX/5r7;


# instance fields
.field private final a:LX/FUn;

.field public b:Z

.field private c:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Z

.field public e:Z

.field public f:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field public h:Z

.field public i:Z

.field private j:LX/F6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/F6L;)V
    .locals 2
    .param p2    # LX/F6L;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2249674
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 2249675
    new-instance v0, LX/FUn;

    invoke-direct {v0}, LX/FUn;-><init>()V

    iput-object v0, p0, LX/FUo;->a:LX/FUn;

    .line 2249676
    iput-boolean v1, p0, LX/FUo;->e:Z

    .line 2249677
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FUo;->h:Z

    .line 2249678
    const/4 v0, 0x0

    iput-object v0, p0, LX/FUo;->j:LX/F6L;

    .line 2249679
    iput v1, p0, LX/FUo;->m:I

    .line 2249680
    iput-object p2, p0, LX/FUo;->j:LX/F6L;

    .line 2249681
    return-void
.end method

.method public static a$redex0(LX/FUo;I)V
    .locals 5

    .prologue
    .line 2249736
    invoke-virtual {p0}, LX/FUo;->getWidth()I

    move-result v1

    .line 2249737
    invoke-virtual {p0}, LX/FUo;->getScrollX()I

    move-result v0

    .line 2249738
    add-int v2, v0, p1

    .line 2249739
    div-int/2addr v0, v1

    .line 2249740
    mul-int v3, v0, v1

    div-int/lit8 v4, v1, 0x2

    add-int/2addr v3, v4

    if-le v2, v3, :cond_0

    .line 2249741
    add-int/lit8 v0, v0, 0x1

    .line 2249742
    :cond_0
    mul-int/2addr v0, v1

    invoke-virtual {p0}, LX/FUo;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/FUo;->smoothScrollTo(II)V

    .line 2249743
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2249744
    invoke-direct {p0}, LX/FUo;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2249745
    iget-object v0, p0, LX/FUo;->j:LX/F6L;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249746
    iget-object v0, p0, LX/FUo;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249747
    iget-object v0, p0, LX/FUo;->j:LX/F6L;

    iget-object v1, p0, LX/FUo;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/F6L;->a(Ljava/lang/String;)V

    .line 2249748
    :cond_0
    return-void
.end method

.method public static c(LX/FUo;)V
    .locals 2

    .prologue
    .line 2249749
    invoke-direct {p0}, LX/FUo;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2249750
    iget-object v0, p0, LX/FUo;->j:LX/F6L;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249751
    iget-object v0, p0, LX/FUo;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249752
    iget-object v0, p0, LX/FUo;->j:LX/F6L;

    iget-object v1, p0, LX/FUo;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/F6L;->b(Ljava/lang/String;)V

    .line 2249753
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 2249754
    iget-object v0, p0, LX/FUo;->j:LX/F6L;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FUo;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FUo;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 2249755
    iget-boolean v0, p0, LX/FUo;->i:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/FUo;->e:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, LX/FUo;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2249756
    :cond_0
    :goto_0
    return-void

    .line 2249757
    :cond_1
    iget-object v0, p0, LX/FUo;->f:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 2249758
    iget-boolean v0, p0, LX/FUo;->i:Z

    if-eqz v0, :cond_2

    .line 2249759
    invoke-static {p0}, LX/FUt;->d(Landroid/view/ViewGroup;)V

    .line 2249760
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FUo;->b:Z

    .line 2249761
    new-instance v0, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;

    invoke-direct {v0, p0}, Lcom/facebook/react/views/scroll/ReactHorizontalScrollView$1;-><init>(LX/FUo;)V

    iput-object v0, p0, LX/FUo;->f:Ljava/lang/Runnable;

    .line 2249762
    iget-object v0, p0, LX/FUo;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x14

    invoke-virtual {p0, v0, v2, v3}, LX/FUo;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2249778
    iget-boolean v0, p0, LX/FUo;->g:Z

    if-nez v0, :cond_1

    .line 2249779
    :cond_0
    :goto_0
    return-void

    .line 2249780
    :cond_1
    iget-object v0, p0, LX/FUo;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249781
    iget-object v0, p0, LX/FUo;->c:Landroid/graphics/Rect;

    invoke-static {p0, v0}, LX/5r8;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2249782
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/FUo;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2249783
    instance-of v1, v0, LX/5r7;

    if-eqz v1, :cond_0

    .line 2249784
    check-cast v0, LX/5r7;

    invoke-interface {v0}, LX/5r7;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2249763
    iget-object v0, p0, LX/FUo;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2249764
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2249765
    iget v0, p0, LX/FUo;->m:I

    if-eqz v0, :cond_0

    .line 2249766
    invoke-virtual {p0, v4}, LX/FUo;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2249767
    iget-object v1, p0, LX/FUo;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p0}, LX/FUo;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2249768
    iget-object v1, p0, LX/FUo;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, LX/FUo;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/FUo;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2249769
    iget-object v0, p0, LX/FUo;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2249770
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->draw(Landroid/graphics/Canvas;)V

    .line 2249771
    return-void
.end method

.method public final fling(I)V
    .locals 1

    .prologue
    .line 2249772
    iget-boolean v0, p0, LX/FUo;->e:Z

    if-eqz v0, :cond_0

    .line 2249773
    invoke-static {p0, p1}, LX/FUo;->a$redex0(LX/FUo;I)V

    .line 2249774
    :goto_0
    invoke-direct {p0}, LX/FUo;->e()V

    .line 2249775
    return-void

    .line 2249776
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->fling(I)V

    goto :goto_0
.end method

.method public getRemoveClippedSubviews()Z
    .locals 1

    .prologue
    .line 2249777
    iget-boolean v0, p0, LX/FUo;->g:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x30ce687a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2249724
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    .line 2249725
    iget-boolean v1, p0, LX/FUo;->g:Z

    if-eqz v1, :cond_0

    .line 2249726
    invoke-virtual {p0}, LX/FUo;->a()V

    .line 2249727
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x4d398e4a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2249728
    iget-boolean v2, p0, LX/FUo;->h:Z

    if-nez v2, :cond_1

    .line 2249729
    :cond_0
    :goto_0
    return v0

    .line 2249730
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2249731
    invoke-static {p0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2249732
    invoke-static {p0}, LX/FUt;->b(Landroid/view/ViewGroup;)V

    .line 2249733
    iput-boolean v1, p0, LX/FUo;->d:Z

    .line 2249734
    invoke-direct {p0}, LX/FUo;->b()V

    move v0, v1

    .line 2249735
    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 2249722
    invoke-virtual {p0}, LX/FUo;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, LX/FUo;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/FUo;->scrollTo(II)V

    .line 2249723
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2249719
    invoke-static {p1, p2}, LX/5qs;->a(II)V

    .line 2249720
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/FUo;->setMeasuredDimension(II)V

    .line 2249721
    return-void
.end method

.method public final onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 2249712
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 2249713
    iget-object v0, p0, LX/FUo;->a:LX/FUn;

    invoke-virtual {v0, p1, p2}, LX/FUn;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2249714
    iget-boolean v0, p0, LX/FUo;->g:Z

    if-eqz v0, :cond_0

    .line 2249715
    invoke-virtual {p0}, LX/FUo;->a()V

    .line 2249716
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FUo;->b:Z

    .line 2249717
    invoke-static {p0}, LX/FUt;->a(Landroid/view/ViewGroup;)V

    .line 2249718
    :cond_1
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2631fd4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2249708
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onSizeChanged(IIII)V

    .line 2249709
    iget-boolean v1, p0, LX/FUo;->g:Z

    if-eqz v1, :cond_0

    .line 2249710
    invoke-virtual {p0}, LX/FUo;->a()V

    .line 2249711
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x7b7a8686

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    const v1, -0x3f0bdb4b

    invoke-static {v3, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2249699
    iget-boolean v2, p0, LX/FUo;->h:Z

    if-nez v2, :cond_0

    .line 2249700
    const v2, 0x3c1c362c    # 0.0095344f

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2249701
    :goto_0
    return v0

    .line 2249702
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    .line 2249703
    if-ne v2, v4, :cond_1

    iget-boolean v2, p0, LX/FUo;->d:Z

    if-eqz v2, :cond_1

    .line 2249704
    invoke-static {p0}, LX/FUt;->c(Landroid/view/ViewGroup;)V

    .line 2249705
    iput-boolean v0, p0, LX/FUo;->d:Z

    .line 2249706
    invoke-direct {p0}, LX/FUo;->e()V

    .line 2249707
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x16a9d8ef

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setEndFillColor(I)V
    .locals 2

    .prologue
    .line 2249695
    iget v0, p0, LX/FUo;->m:I

    if-eq p1, v0, :cond_0

    .line 2249696
    iput p1, p0, LX/FUo;->m:I

    .line 2249697
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, LX/FUo;->m:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/FUo;->l:Landroid/graphics/drawable/Drawable;

    .line 2249698
    :cond_0
    return-void
.end method

.method public setPagingEnabled(Z)V
    .locals 0

    .prologue
    .line 2249693
    iput-boolean p1, p0, LX/FUo;->e:Z

    .line 2249694
    return-void
.end method

.method public setRemoveClippedSubviews(Z)V
    .locals 1

    .prologue
    .line 2249688
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/FUo;->c:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 2249689
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/FUo;->c:Landroid/graphics/Rect;

    .line 2249690
    :cond_0
    iput-boolean p1, p0, LX/FUo;->g:Z

    .line 2249691
    invoke-virtual {p0}, LX/FUo;->a()V

    .line 2249692
    return-void
.end method

.method public setScrollEnabled(Z)V
    .locals 0

    .prologue
    .line 2249686
    iput-boolean p1, p0, LX/FUo;->h:Z

    .line 2249687
    return-void
.end method

.method public setScrollPerfTag(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2249684
    iput-object p1, p0, LX/FUo;->k:Ljava/lang/String;

    .line 2249685
    return-void
.end method

.method public setSendMomentumEvents(Z)V
    .locals 0

    .prologue
    .line 2249682
    iput-boolean p1, p0, LX/FUo;->i:Z

    .line 2249683
    return-void
.end method
