.class public LX/GnF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static t:LX/0Xm;


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/18V;

.field public c:LX/11M;

.field public d:LX/0Sh;

.field public e:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field public f:LX/Go2;

.field private g:Ljava/util/concurrent/ExecutorService;

.field public final h:LX/03V;

.field public i:LX/GnG;

.field public j:LX/Chv;

.field public final k:LX/1xP;

.field private l:LX/GnL;

.field public m:LX/Gn7;

.field public final n:LX/Go0;

.field private o:LX/Gn2;

.field private p:LX/CIj;

.field private q:LX/Go4;

.field public r:LX/CIb;

.field public s:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/18V;LX/11M;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Sh;LX/Go2;Ljava/util/concurrent/ExecutorService;LX/03V;LX/Chv;LX/1xP;LX/GnL;LX/Gn7;LX/Go0;LX/Gn2;LX/CIj;LX/Go4;LX/CIb;Lcom/facebook/content/SecureContextHelper;)V
    .locals 2
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393545
    iput-object p1, p0, LX/GnF;->a:Landroid/content/Context;

    .line 2393546
    iput-object p2, p0, LX/GnF;->b:LX/18V;

    .line 2393547
    iput-object p3, p0, LX/GnF;->c:LX/11M;

    .line 2393548
    iput-object p4, p0, LX/GnF;->e:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2393549
    iput-object p5, p0, LX/GnF;->d:LX/0Sh;

    .line 2393550
    iput-object p6, p0, LX/GnF;->f:LX/Go2;

    .line 2393551
    iput-object p7, p0, LX/GnF;->g:Ljava/util/concurrent/ExecutorService;

    .line 2393552
    iput-object p8, p0, LX/GnF;->h:LX/03V;

    .line 2393553
    new-instance v1, LX/GnG;

    invoke-direct {v1}, LX/GnG;-><init>()V

    iput-object v1, p0, LX/GnF;->i:LX/GnG;

    .line 2393554
    iput-object p9, p0, LX/GnF;->j:LX/Chv;

    .line 2393555
    iput-object p10, p0, LX/GnF;->k:LX/1xP;

    .line 2393556
    iput-object p11, p0, LX/GnF;->l:LX/GnL;

    .line 2393557
    iput-object p12, p0, LX/GnF;->m:LX/Gn7;

    .line 2393558
    iput-object p13, p0, LX/GnF;->n:LX/Go0;

    .line 2393559
    move-object/from16 v0, p14

    iput-object v0, p0, LX/GnF;->o:LX/Gn2;

    .line 2393560
    move-object/from16 v0, p15

    iput-object v0, p0, LX/GnF;->p:LX/CIj;

    .line 2393561
    move-object/from16 v0, p16

    iput-object v0, p0, LX/GnF;->q:LX/Go4;

    .line 2393562
    move-object/from16 v0, p17

    iput-object v0, p0, LX/GnF;->r:LX/CIb;

    .line 2393563
    move-object/from16 v0, p18

    iput-object v0, p0, LX/GnF;->s:Lcom/facebook/content/SecureContextHelper;

    .line 2393564
    return-void
.end method

.method public static a(LX/0QB;)LX/GnF;
    .locals 3

    .prologue
    .line 2393565
    const-class v1, LX/GnF;

    monitor-enter v1

    .line 2393566
    :try_start_0
    sget-object v0, LX/GnF;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2393567
    sput-object v2, LX/GnF;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2393568
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393569
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/GnF;->b(LX/0QB;)LX/GnF;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2393570
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GnF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393571
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2393572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/162;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2393573
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2393574
    if-eqz p0, :cond_0

    .line 2393575
    const-string v1, "tracking_codes"

    invoke-virtual {p0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2393576
    :cond_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2393577
    const-string v1, "iab_click_source"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2393578
    :cond_1
    return-object v0
.end method

.method public static a(LX/GnF;)V
    .locals 3

    .prologue
    .line 2393579
    iget-object v0, p0, LX/GnF;->f:LX/Go2;

    .line 2393580
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, LX/Go2;->d:J

    .line 2393581
    iget-object v0, p0, LX/GnF;->q:LX/Go4;

    invoke-virtual {v0}, LX/Go4;->a()V

    .line 2393582
    return-void
.end method

.method public static a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GoE;",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2393583
    iget-object v6, p0, LX/GnF;->n:LX/Go0;

    const-string v7, "instant_shopping_element_click"

    new-instance v0, LX/GnC;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/GnC;-><init>(LX/GnF;LX/GoE;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/util/Map;)V

    invoke-virtual {v6, v7, v0}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2393584
    iget-object v0, p0, LX/GnF;->o:LX/Gn2;

    .line 2393585
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    if-ne p2, v1, :cond_0

    if-nez p3, :cond_1

    .line 2393586
    :cond_0
    :goto_0
    return-void

    .line 2393587
    :cond_1
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2393588
    invoke-static {v1}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2393589
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Gn2;->e:Z

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/GnF;
    .locals 20

    .prologue
    .line 2393590
    new-instance v1, LX/GnF;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static/range {p0 .. p0}, LX/11M;->a(LX/0QB;)LX/11M;

    move-result-object v4

    check-cast v4, LX/11M;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v5

    check-cast v5, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/Go2;->a(LX/0QB;)LX/Go2;

    move-result-object v7

    check-cast v7, LX/Go2;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {p0 .. p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v10

    check-cast v10, LX/Chv;

    invoke-static/range {p0 .. p0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v11

    check-cast v11, LX/1xP;

    invoke-static/range {p0 .. p0}, LX/GnL;->a(LX/0QB;)LX/GnL;

    move-result-object v12

    check-cast v12, LX/GnL;

    invoke-static/range {p0 .. p0}, LX/Gn7;->a(LX/0QB;)LX/Gn7;

    move-result-object v13

    check-cast v13, LX/Gn7;

    invoke-static/range {p0 .. p0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v14

    check-cast v14, LX/Go0;

    invoke-static/range {p0 .. p0}, LX/Gn2;->a(LX/0QB;)LX/Gn2;

    move-result-object v15

    check-cast v15, LX/Gn2;

    invoke-static/range {p0 .. p0}, LX/CIj;->a(LX/0QB;)LX/CIj;

    move-result-object v16

    check-cast v16, LX/CIj;

    invoke-static/range {p0 .. p0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v17

    check-cast v17, LX/Go4;

    invoke-static/range {p0 .. p0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v18

    check-cast v18, LX/CIb;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v19

    check-cast v19, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v1 .. v19}, LX/GnF;-><init>(Landroid/content/Context;LX/18V;LX/11M;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Sh;LX/Go2;Ljava/util/concurrent/ExecutorService;LX/03V;LX/Chv;LX/1xP;LX/GnL;LX/Gn7;LX/Go0;LX/Gn2;LX/CIj;LX/Go4;LX/CIb;Lcom/facebook/content/SecureContextHelper;)V

    .line 2393591
    return-object v1
.end method

.method public static b(LX/GnF;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2393592
    iget-object v0, p0, LX/GnF;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$1;-><init>(LX/GnF;Ljava/lang/String;)V

    const v2, -0x256d7ae9

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2393593
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/CHX;LX/GoE;Ljava/util/Map;)V
    .locals 5
    .param p3    # LX/GoE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/CHX;",
            "LX/GoE;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2393594
    sget-object v0, LX/GnD;->a:[I

    invoke-interface {p2}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2393595
    :goto_0
    return-void

    .line 2393596
    :pswitch_0
    invoke-interface {p2}, LX/CHW;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1xP;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393597
    iget-object v0, p0, LX/GnF;->j:LX/Chv;

    new-instance v1, LX/GnX;

    iget-object v2, p0, LX/GnF;->m:LX/Gn7;

    invoke-interface {p2}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GnX;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2393598
    :goto_1
    invoke-interface {p2}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v0

    invoke-interface {p2}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p3, v0, v1, p4}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    .line 2393599
    invoke-static {p0}, LX/GnF;->a(LX/GnF;)V

    goto :goto_0

    .line 2393600
    :cond_0
    iget-object v1, p0, LX/GnF;->k:LX/1xP;

    invoke-interface {p2}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/GnF;->r:LX/CIb;

    .line 2393601
    iget-object v3, v0, LX/CIb;->c:LX/0lF;

    move-object v0, v3

    .line 2393602
    check-cast v0, LX/162;

    const-string v3, "canvas_ads"

    invoke-static {v0, v3}, LX/GnF;->a(LX/162;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, p1, v2, v0, v4}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    goto :goto_1

    .line 2393603
    :pswitch_1
    iget-object v0, p0, LX/GnF;->p:LX/CIj;

    .line 2393604
    iget-boolean v1, v0, LX/CIj;->a:Z

    move v0, v1

    .line 2393605
    if-nez v0, :cond_1

    .line 2393606
    iget-object v0, p0, LX/GnF;->a:Landroid/content/Context;

    iget-object v1, p0, LX/GnF;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0828ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2393607
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2393608
    :cond_1
    iget-object v0, p0, LX/GnF;->j:LX/Chv;

    new-instance v1, LX/GnY;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, LX/GnY;-><init>(J)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2393609
    invoke-interface {p2}, LX/CHW;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/GnF;->b(LX/GnF;Ljava/lang/String;)V

    .line 2393610
    invoke-interface {p2}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v0

    invoke-interface {p2}, LX/CHW;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p3, v0, v1, p4}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 2393611
    :pswitch_2
    iget-object v1, p0, LX/GnF;->k:LX/1xP;

    invoke-interface {p2}, LX/CHW;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/GnF;->r:LX/CIb;

    .line 2393612
    iget-object v3, v0, LX/CIb;->c:LX/0lF;

    move-object v0, v3

    .line 2393613
    check-cast v0, LX/162;

    const-string v3, "canvas_ads"

    invoke-static {v0, v3}, LX/GnF;->a(LX/162;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, p1, v2, v0, v4}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 2393614
    invoke-interface {p2}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v0

    invoke-interface {p2}, LX/CHW;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p3, v0, v1, p4}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 2393615
    :pswitch_3
    invoke-interface {p2}, LX/CHW;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/GnF;->b(LX/GnF;Ljava/lang/String;)V

    .line 2393616
    invoke-interface {p2}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v0

    invoke-interface {p2}, LX/CHW;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p3, v0, v1, p4}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 2393617
    :pswitch_4
    iget-object v0, p0, LX/GnF;->l:LX/GnL;

    .line 2393618
    invoke-interface {p2}, LX/CHW;->js_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/CHX;->jr_()LX/2uF;

    move-result-object v2

    .line 2393619
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2393620
    new-instance v4, LX/GnJ;

    invoke-direct {v4, v0, v1}, LX/GnJ;-><init>(LX/GnL;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2393621
    new-instance v4, LX/GnK;

    invoke-direct {v4, v0}, LX/GnK;-><init>(LX/GnL;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2393622
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result p0

    if-eqz p0, :cond_2

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object p0

    iget-object p3, p0, LX/1vs;->a:LX/15i;

    iget p0, p0, LX/1vs;->b:I

    .line 2393623
    new-instance p2, LX/GnI;

    invoke-direct {p2, v0, p3, p0}, LX/GnI;-><init>(LX/GnL;LX/15i;I)V

    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2393624
    :cond_2
    move-object v1, v3

    .line 2393625
    new-instance v2, LX/3Af;

    invoke-direct {v2, p1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2393626
    const/4 p0, 0x1

    .line 2393627
    new-instance p3, LX/GnN;

    invoke-direct {p3, p1}, LX/GnN;-><init>(Landroid/content/Context;)V

    .line 2393628
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_3
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GnI;

    .line 2393629
    invoke-virtual {v3}, LX/GnI;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object p2

    invoke-virtual {v3}, LX/GnI;->c()Z

    move-result v4

    if-nez v4, :cond_3

    move v4, p0

    :goto_4
    invoke-interface {p2, v4}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v4

    new-instance p2, LX/GnH;

    invoke-direct {p2, v0, v3}, LX/GnH;-><init>(LX/GnL;LX/GnI;)V

    invoke-interface {v4, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    goto :goto_4

    .line 2393630
    :cond_4
    move-object v1, p3

    .line 2393631
    invoke-virtual {v2, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2393632
    invoke-virtual {v2}, LX/3Af;->show()V

    .line 2393633
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
