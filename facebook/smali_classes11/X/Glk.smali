.class public LX/Glk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:D

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Landroid/content/res/Resources;


# direct methods
.method private constructor <init>(DIIILandroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 2391220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2391221
    iput-wide p1, p0, LX/Glk;->a:D

    .line 2391222
    iput p3, p0, LX/Glk;->b:I

    .line 2391223
    iput p4, p0, LX/Glk;->c:I

    .line 2391224
    iput p5, p0, LX/Glk;->d:I

    .line 2391225
    iput-object p6, p0, LX/Glk;->e:Landroid/content/res/Resources;

    .line 2391226
    return-void
.end method

.method public static a(Ljava/lang/Integer;Landroid/content/Context;Landroid/content/res/Resources;)LX/Glk;
    .locals 20
    .param p0    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2391227
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2391228
    const-string v2, "window"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 2391229
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2391230
    const v2, 0x7f0b1faa

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v4, 0x7f0b1fb9

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v4, v2

    .line 2391231
    const v2, 0x7f0b1fa6

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2391232
    const v2, 0x7f0b1fa4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 2391233
    const v2, 0x7f0b1fa5    # 1.84927E38f

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 2391234
    iget v6, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2391235
    if-nez p0, :cond_1

    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2391236
    :goto_0
    const v3, 0x7f0b11e1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    .line 2391237
    const v3, 0x7f0b2370

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 2391238
    mul-int/lit8 v7, v8, 0x2

    sub-int v11, v2, v7

    .line 2391239
    sub-int v3, v6, v3

    .line 2391240
    const-wide/high16 v6, 0x3fe8000000000000L    # 0.75

    int-to-double v12, v9

    mul-double/2addr v6, v12

    .line 2391241
    const-wide/high16 v12, 0x3fe8000000000000L    # 0.75

    int-to-double v14, v5

    mul-double/2addr v12, v14

    .line 2391242
    const v5, 0x7f0b1fa8

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 2391243
    const v14, 0x7f0b1fa7

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v14

    .line 2391244
    add-int v15, v11, v5

    int-to-double v0, v15

    move-wide/from16 v16, v0

    int-to-double v0, v5

    move-wide/from16 v18, v0

    add-double v6, v6, v18

    div-double v6, v16, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v6, v6

    .line 2391245
    add-int/2addr v3, v14

    int-to-double v0, v3

    move-wide/from16 v16, v0

    int-to-double v14, v14

    add-double/2addr v12, v14

    int-to-double v14, v4

    add-double/2addr v12, v14

    div-double v12, v16, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v7, v12

    .line 2391246
    int-to-double v12, v11

    int-to-double v14, v5

    add-double/2addr v12, v14

    int-to-double v14, v6

    div-double/2addr v12, v14

    add-int v3, v9, v5

    int-to-double v4, v3

    div-double v4, v12, v4

    .line 2391247
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    invoke-static {v12, v13, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 2391248
    const/4 v3, 0x1

    if-le v6, v3, :cond_0

    .line 2391249
    mul-int/lit8 v3, v8, 0x2

    add-int/2addr v3, v10

    mul-int/2addr v3, v6

    sub-int v2, v3, v2

    mul-int/lit8 v3, v6, 0x2

    add-int/lit8 v3, v3, -0x2

    div-int v8, v2, v3

    .line 2391250
    :cond_0
    new-instance v3, LX/Glk;

    move-object/from16 v9, p2

    invoke-direct/range {v3 .. v9}, LX/Glk;-><init>(DIIILandroid/content/res/Resources;)V

    return-object v3

    .line 2391251
    :cond_1
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method
