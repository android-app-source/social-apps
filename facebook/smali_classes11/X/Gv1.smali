.class public final LX/Gv1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;)V
    .locals 0

    .prologue
    .line 2404689
    iput-object p1, p0, LX/Gv1;->a:Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2404690
    iget-object v0, p0, LX/Gv1;->a:Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2404691
    iget-object v0, p0, LX/Gv1;->a:Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gv1;->a:Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 2404692
    iget-object v0, p0, LX/Gv1;->a:Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->j:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/faceweb/ActionSheetButton;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/ActionSheetButton;->callback:Ljava/lang/String;

    .line 2404693
    if-eqz v0, :cond_0

    .line 2404694
    iget-object v1, p0, LX/Gv1;->a:Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->m:Lcom/facebook/webview/FacebookWebView;

    const/4 v2, 0x0

    .line 2404695
    invoke-virtual {v1, v0, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2404696
    :cond_0
    return-void
.end method
