.class public LX/Fhh;
.super LX/Fhd;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/Fhh;


# instance fields
.field private final e:LX/Fhg;

.field public final f:LX/0Sh;


# direct methods
.method public constructor <init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;LX/0Sh;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273617
    invoke-direct {p0, p1, p2, p5}, LX/Fhd;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2273618
    sget v0, LX/2SU;->e:I

    invoke-virtual {p3, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273619
    new-instance v0, LX/Fhg;

    const v1, 0x7001f

    const-string p1, "GraphSearchMemorySuggestionsTypeahead"

    invoke-direct {v0, v1, p1}, LX/Fhg;-><init>(ILjava/lang/String;)V

    move-object v0, v0

    .line 2273620
    :goto_0
    iput-object v0, p0, LX/Fhh;->e:LX/Fhg;

    .line 2273621
    iput-object p4, p0, LX/Fhh;->f:LX/0Sh;

    .line 2273622
    return-void

    .line 2273623
    :cond_0
    new-instance v0, LX/Fhg;

    const v1, 0x70012

    const-string p1, "SimpleSearchMemorySuggestionsTypeahead"

    invoke-direct {v0, v1, p1}, LX/Fhg;-><init>(ILjava/lang/String;)V

    move-object v0, v0

    .line 2273624
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Fhh;
    .locals 9

    .prologue
    .line 2273586
    sget-object v0, LX/Fhh;->g:LX/Fhh;

    if-nez v0, :cond_1

    .line 2273587
    const-class v1, LX/Fhh;

    monitor-enter v1

    .line 2273588
    :try_start_0
    sget-object v0, LX/Fhh;->g:LX/Fhh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273589
    if-eqz v2, :cond_0

    .line 2273590
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273591
    new-instance v3, LX/Fhh;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v5

    check-cast v5, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/Fhh;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;LX/0Sh;LX/0ad;)V

    .line 2273592
    move-object v0, v3

    .line 2273593
    sput-object v0, LX/Fhh;->g:LX/Fhh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273594
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273595
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273596
    :cond_1
    sget-object v0, LX/Fhh;->g:LX/Fhh;

    return-object v0

    .line 2273597
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273598
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/Fhg;
    .locals 1

    .prologue
    .line 2273599
    iget-object v0, p0, LX/Fhh;->e:LX/Fhg;

    return-object v0
.end method

.method public final a(LX/7B6;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2273600
    iget-object v0, p0, LX/Fhh;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2273601
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v0

    const-string v1, "end_to_end"

    iget v2, p0, LX/Fhd;->d:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const v3, -0x2dffd09e

    invoke-static {v0, v1, v2, v4, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273602
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v0

    const-string v1, "end_to_end_prerendering"

    iget v2, p0, LX/Fhd;->d:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const v3, -0x596c61d7

    invoke-static {v0, v1, v2, v4, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273603
    return-void
.end method

.method public final a(LX/7B6;I)V
    .locals 1

    .prologue
    .line 2273604
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(LX/7B6;Ljava/util/List;LX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2273605
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2273606
    iget-object v0, p0, LX/Fhh;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2273607
    iget-object v0, p0, LX/Fhd;->a:LX/11i;

    invoke-virtual {p0}, LX/Fhh;->a()LX/Fhg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2273608
    if-nez v0, :cond_1

    .line 2273609
    :cond_0
    :goto_0
    return-void

    .line 2273610
    :cond_1
    iget v1, p0, LX/Fhd;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 2273611
    const-string v2, "end_to_end"

    invoke-interface {v0, v2, v1}, LX/11o;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2273612
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2273613
    const-string v2, "end_to_end"

    const/4 v3, 0x0

    const v4, -0x16236606

    invoke-static {v0, v2, v1, v3, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    goto :goto_0

    .line 2273614
    :cond_2
    const-string v2, "end_to_end"

    const v3, -0x129ce2ae

    invoke-static {v0, v2, v1, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public final c(LX/7B6;)V
    .locals 1

    .prologue
    .line 2273615
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d(LX/7B6;)V
    .locals 1

    .prologue
    .line 2273616
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
