.class public LX/FfW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FfW;


# instance fields
.field private final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/CyI;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/search/results/fragment/spec/GraphSearchResultFragmentSpecification;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/FfR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffe;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ffj;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2268581
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->APPS:LX/CyI;

    invoke-virtual {v1, v2, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->GROUPS:LX/CyI;

    invoke-virtual {v1, v2, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->PAGES:LX/CyI;

    invoke-virtual {v1, v2, p3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->PEOPLE:LX/CyI;

    invoke-virtual {v1, v2, p4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->PLACES:LX/CyI;

    invoke-virtual {v1, v2, p5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->TOP:LX/CyI;

    invoke-virtual {v1, v2, p6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->TOP_REACTION:LX/CyI;

    invoke-virtual {v1, v2, p7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->TOP_ENTITIES:LX/CyI;

    invoke-virtual {v1, v2, p8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->LATEST:LX/CyI;

    invoke-virtual {v1, v2, p9}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->PHOTOS:LX/CyI;

    move-object/from16 v0, p14

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->POSTS:LX/CyI;

    invoke-virtual {v1, v2, p10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->BLENDED_POSTS:LX/CyI;

    invoke-virtual {v1, v2, p11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->EVENTS:LX/CyI;

    invoke-virtual {v1, v2, p12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->MARKETPLACE:LX/CyI;

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->VIDEOS:LX/CyI;

    move-object/from16 v0, p16

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->BLENDED_VIDEOS:LX/CyI;

    move-object/from16 v0, p15

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/CyI;->VIDEO_CHANNELS:LX/CyI;

    move-object/from16 v0, p17

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    iput-object v1, p0, LX/FfW;->a:LX/0P1;

    .line 2268582
    return-void
.end method

.method public static a(LX/0QB;)LX/FfW;
    .locals 3

    .prologue
    .line 2268570
    sget-object v0, LX/FfW;->b:LX/FfW;

    if-nez v0, :cond_1

    .line 2268571
    const-class v1, LX/FfW;

    monitor-enter v1

    .line 2268572
    :try_start_0
    sget-object v0, LX/FfW;->b:LX/FfW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268573
    if-eqz v2, :cond_0

    .line 2268574
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FfW;->b(LX/0QB;)LX/FfW;

    move-result-object v0

    sput-object v0, LX/FfW;->b:LX/FfW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268575
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268576
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268577
    :cond_1
    sget-object v0, LX/FfW;->b:LX/FfW;

    return-object v0

    .line 2268578
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268579
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/FfW;
    .locals 20

    .prologue
    .line 2268583
    new-instance v2, LX/FfW;

    const/16 v3, 0x3347

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x334d

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3350

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3351

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x3353

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3357

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3358

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3356

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x334e

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3354

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3349

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x334b

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x334f

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x3348

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x334a

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x335a

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x3359

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-direct/range {v2 .. v19}, LX/FfW;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2268584
    return-object v2
.end method


# virtual methods
.method public final a(LX/CyI;)LX/FfQ;
    .locals 1

    .prologue
    .line 2268569
    iget-object v0, p0, LX/FfW;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FfQ;

    return-object v0
.end method
