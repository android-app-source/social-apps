.class public final LX/H69;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 0

    .prologue
    .line 2425898
    iput-object p1, p0, LX/H69;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0xbff9bbb

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2425899
    iget-object v1, p0, LX/H69;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v2, p0, LX/H69;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/H69;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v3, v3, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425900
    invoke-static {v3}, LX/H6T;->c(LX/H83;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2425901
    sget-object v7, LX/0ax;->gH:Ljava/lang/String;

    invoke-static {v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2425902
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2425903
    const-string v9, "latitude"

    invoke-virtual {v3}, LX/H83;->A()D

    move-result-wide v11

    invoke-virtual {v8, v9, v11, v12}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2425904
    const-string v9, "longitude"

    invoke-virtual {v3}, LX/H83;->B()D

    move-result-wide v11

    invoke-virtual {v8, v9, v11, v12}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2425905
    const-string v9, "place_name"

    invoke-virtual {v3}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425906
    const-string v9, "address"

    invoke-virtual {v3}, LX/H83;->C()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425907
    const-string v9, "zoom"

    const/high16 v10, 0x41200000    # 10.0f

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 2425908
    const-string v9, "curation_surface"

    const-string v10, "OFFERS"

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425909
    iget-object v9, v1, LX/H6T;->j:LX/17Y;

    invoke-interface {v9, v2, v7}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 2425910
    invoke-virtual {v7, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2425911
    iget-object v8, v1, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v8, v7, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2425912
    :goto_0
    iget-object v1, p0, LX/H69;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    iget-object v2, p0, LX/H69;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    const-string v3, "opened_map"

    iget-object v4, p0, LX/H69;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v4, v4, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2425913
    const v1, 0x260c90cb

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2425914
    :cond_0
    const-string v7, "OfferRenderingUtils"

    const-string v8, "Offer doesn\'t have sufficient location information to show"

    invoke-static {v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
