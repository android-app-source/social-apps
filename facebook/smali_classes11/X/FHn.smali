.class public LX/FHn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field public final a:LX/0Xl;

.field public final b:LX/0TD;

.field public final c:LX/6ef;

.field public final d:LX/FHe;

.field public final e:LX/2Mo;

.field public final f:LX/0ad;

.field public final g:LX/FGm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2220982
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FHn;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/0TD;LX/6ef;LX/FHe;LX/2Mo;LX/0ad;LX/FGm;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2220983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220984
    iput-object p1, p0, LX/FHn;->a:LX/0Xl;

    .line 2220985
    iput-object p2, p0, LX/FHn;->b:LX/0TD;

    .line 2220986
    iput-object p3, p0, LX/FHn;->c:LX/6ef;

    .line 2220987
    iput-object p4, p0, LX/FHn;->d:LX/FHe;

    .line 2220988
    iput-object p5, p0, LX/FHn;->e:LX/2Mo;

    .line 2220989
    iput-object p6, p0, LX/FHn;->f:LX/0ad;

    .line 2220990
    iput-object p7, p0, LX/FHn;->g:LX/FGm;

    .line 2220991
    return-void
.end method

.method public static a(LX/0QB;)LX/FHn;
    .locals 15

    .prologue
    .line 2220992
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2220993
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2220994
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2220995
    if-nez v1, :cond_0

    .line 2220996
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2220997
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2220998
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2220999
    sget-object v1, LX/FHn;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2221000
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2221001
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2221002
    :cond_1
    if-nez v1, :cond_4

    .line 2221003
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2221004
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2221005
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2221006
    new-instance v7, LX/FHn;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static {v0}, LX/6ef;->a(LX/0QB;)LX/6ef;

    move-result-object v10

    check-cast v10, LX/6ef;

    invoke-static {v0}, LX/FHe;->a(LX/0QB;)LX/FHe;

    move-result-object v11

    check-cast v11, LX/FHe;

    invoke-static {v0}, LX/2Mo;->a(LX/0QB;)LX/2Mo;

    move-result-object v12

    check-cast v12, LX/2Mo;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static {v0}, LX/FGm;->a(LX/0QB;)LX/FGm;

    move-result-object v14

    check-cast v14, LX/FGm;

    invoke-direct/range {v7 .. v14}, LX/FHn;-><init>(LX/0Xl;LX/0TD;LX/6ef;LX/FHe;LX/2Mo;LX/0ad;LX/FGm;)V

    .line 2221007
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2221008
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2221009
    if-nez v1, :cond_2

    .line 2221010
    sget-object v0, LX/FHn;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHn;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2221011
    :goto_1
    if-eqz v0, :cond_3

    .line 2221012
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2221013
    :goto_3
    check-cast v0, LX/FHn;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2221014
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2221015
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2221016
    :catchall_1
    move-exception v0

    .line 2221017
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2221018
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2221019
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2221020
    :cond_2
    :try_start_8
    sget-object v0, LX/FHn;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHn;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/FHl;)LX/FHm;
    .locals 1

    .prologue
    .line 2221021
    new-instance v0, LX/FHm;

    invoke-direct {v0, p0, p1}, LX/FHm;-><init>(LX/FHn;LX/FHl;)V

    return-object v0
.end method
