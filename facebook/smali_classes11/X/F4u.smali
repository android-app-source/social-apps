.class public LX/F4u;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public final b:LX/1Ck;

.field public final c:LX/F53;

.field public final d:LX/0tX;

.field public final e:LX/3iH;

.field public final f:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2197758
    const-class v0, LX/F4u;

    sput-object v0, LX/F4u;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0tX;LX/3iH;LX/03V;LX/F53;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2197751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2197752
    iput-object p1, p0, LX/F4u;->b:LX/1Ck;

    .line 2197753
    iput-object p5, p0, LX/F4u;->c:LX/F53;

    .line 2197754
    iput-object p2, p0, LX/F4u;->d:LX/0tX;

    .line 2197755
    iput-object p3, p0, LX/F4u;->e:LX/3iH;

    .line 2197756
    iput-object p4, p0, LX/F4u;->f:LX/03V;

    .line 2197757
    return-void
.end method

.method public static a(LX/0QB;)LX/F4u;
    .locals 7

    .prologue
    .line 2197759
    new-instance v1, LX/F4u;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v4

    check-cast v4, LX/3iH;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/F53;->a(LX/0QB;)LX/F53;

    move-result-object v6

    check-cast v6, LX/F53;

    invoke-direct/range {v1 .. v6}, LX/F4u;-><init>(LX/1Ck;LX/0tX;LX/3iH;LX/03V;LX/F53;)V

    .line 2197760
    move-object v0, v1

    .line 2197761
    return-object v0
.end method
