.class public final LX/FzI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FzH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FzH",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoCurrentCityPredictionQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FzK;


# direct methods
.method public constructor <init>(LX/FzK;)V
    .locals 0

    .prologue
    .line 2307797
    iput-object p1, p0, LX/FzI;->a:LX/FzK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2307798
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2307799
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2307800
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2307801
    check-cast v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoCurrentCityPredictionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoCurrentCityPredictionQueryModel;->a()Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoCurrentCityPredictionQueryModel$CurrentCityPredictionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoCurrentCityPredictionQueryModel$CurrentCityPredictionsModel;->a()LX/0Px;

    move-result-object v3

    .line 2307802
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoCurrentCityPredictionQueryModel$CurrentCityPredictionsModel$EdgesModel;

    .line 2307803
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoCurrentCityPredictionQueryModel$CurrentCityPredictionsModel$EdgesModel;->a()Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2307804
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2307805
    :cond_0
    return-object v2
.end method
