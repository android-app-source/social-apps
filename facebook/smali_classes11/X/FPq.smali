.class public LX/FPq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2238056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(J)J
    .locals 2

    .prologue
    .line 2238055
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    return-wide v0
.end method

.method private static final a(JLjava/util/TimeZone;)J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 2238050
    if-eqz p2, :cond_0

    .line 2238051
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 2238052
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v0, v0

    .line 2238053
    :goto_0
    const-string v4, "US/Pacific"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    .line 2238054
    invoke-virtual {v4, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    int-to-long v2, v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, LX/FPq;->a(J)J

    move-result-wide v0

    add-long/2addr v0, p0

    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method private static final a(JJ)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2238000
    invoke-static {p0, p1}, LX/FPq;->b(J)J

    move-result-wide v4

    .line 2238001
    invoke-static {p2, p3}, LX/FPq;->b(J)J

    move-result-wide v6

    .line 2238002
    const-wide/32 v8, 0x7e900

    cmp-long v0, v8, v4

    if-gtz v0, :cond_0

    const-wide/32 v8, 0x93a7f

    cmp-long v0, v4, v8

    if-gtz v0, :cond_0

    move v0, v1

    .line 2238003
    :goto_0
    invoke-static {v6, v7}, LX/FPq;->c(J)Z

    move-result v3

    .line 2238004
    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 2238005
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2238006
    goto :goto_1
.end method

.method private static final a(JJJ)Z
    .locals 8

    .prologue
    .line 2238043
    invoke-static {p0, p1}, LX/FPq;->b(J)J

    move-result-wide v0

    .line 2238044
    invoke-static {p2, p3}, LX/FPq;->b(J)J

    move-result-wide v2

    .line 2238045
    invoke-static {p4, p5}, LX/FPq;->b(J)J

    move-result-wide v4

    .line 2238046
    invoke-static {v2, v3, v4, v5}, LX/FPq;->b(JJ)J

    move-result-wide v4

    .line 2238047
    invoke-static {v0, v1}, LX/FPq;->c(J)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {v2, v3, v4, v5}, LX/FPq;->a(JJ)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2238048
    const-wide/32 v6, 0x93a80

    add-long/2addr v0, v6

    .line 2238049
    :cond_0
    cmp-long v2, v2, v0

    if-gtz v2, :cond_1

    cmp-long v0, v0, v4

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(LX/0Px;Ljava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$NearbyPagePlaceInfoFragment$Hours;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2238036
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 2238037
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/FPq;->a(J)J

    move-result-wide v0

    .line 2238038
    invoke-static {p0, p1}, LX/FPq;->b(LX/0Px;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 2238039
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;

    .line 2238040
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;->b()J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;->a()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/FPq;->a(JJJ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2238041
    const/4 v0, 0x1

    .line 2238042
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final b(J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/32 v2, 0x93a80

    .line 2238030
    cmp-long v0, p0, v4

    if-ltz v0, :cond_0

    cmp-long v0, p0, v2

    if-gez v0, :cond_0

    .line 2238031
    :goto_0
    return-wide p0

    .line 2238032
    :cond_0
    cmp-long v0, p0, v4

    if-gez v0, :cond_1

    .line 2238033
    rem-long v0, p0, v2

    add-long/2addr v0, v2

    rem-long/2addr v0, v2

    :goto_1
    move-wide p0, v0

    .line 2238034
    goto :goto_0

    .line 2238035
    :cond_1
    rem-long v0, p0, v2

    goto :goto_1
.end method

.method private static final b(JJ)J
    .locals 2

    .prologue
    .line 2238025
    cmp-long v0, p2, p0

    if-gez v0, :cond_0

    .line 2238026
    invoke-static {p0, p1, p2, p3}, LX/FPq;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2238027
    const-wide/32 v0, 0x93a80

    add-long/2addr p2, v0

    .line 2238028
    :cond_0
    :goto_0
    return-wide p2

    .line 2238029
    :cond_1
    const-wide/32 v0, 0x15180

    add-long/2addr p2, v0

    goto :goto_0
.end method

.method private static final b(LX/0Px;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$NearbyPagePlaceInfoFragment$Hours;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$NearbyPagePlaceInfoFragment$Hours;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2238008
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2238009
    :cond_0
    const/4 v0, 0x0

    .line 2238010
    :goto_0
    return-object v0

    .line 2238011
    :cond_1
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    move-object v1, v0

    .line 2238012
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2238013
    new-instance v4, LX/CQU;

    invoke-direct {v4}, LX/CQU;-><init>()V

    .line 2238014
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v5, :cond_3

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;

    .line 2238015
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;->b()J

    move-result-wide v6

    invoke-static {v6, v7, v1}, LX/FPq;->a(JLjava/util/TimeZone;)J

    move-result-wide v6

    .line 2238016
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;->a()J

    move-result-wide v8

    invoke-static {v8, v9, v1}, LX/FPq;->a(JLjava/util/TimeZone;)J

    move-result-wide v8

    .line 2238017
    iput-wide v6, v4, LX/CQU;->b:J

    .line 2238018
    move-object v0, v4

    .line 2238019
    iput-wide v8, v0, LX/CQU;->a:J

    .line 2238020
    move-object v0, v0

    .line 2238021
    invoke-virtual {v0}, LX/CQU;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2238022
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 2238023
    :cond_2
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 2238024
    goto :goto_0
.end method

.method private static final c(J)Z
    .locals 2

    .prologue
    .line 2238007
    const-wide/16 v0, 0x0

    cmp-long v0, v0, p0

    if-gtz v0, :cond_0

    const-wide/32 v0, 0x1517f

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
