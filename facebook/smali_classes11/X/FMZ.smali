.class public LX/FMZ;
.super LX/3hi;
.source ""


# static fields
.field private static final b:Landroid/graphics/Rect;

.field private static final c:Landroid/graphics/RectF;

.field private static final d:Landroid/graphics/RectF;

.field private static final e:Landroid/graphics/Matrix;


# instance fields
.field private final f:I

.field private final g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2229946
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/FMZ;->b:Landroid/graphics/Rect;

    .line 2229947
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, LX/FMZ;->c:Landroid/graphics/RectF;

    .line 2229948
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, LX/FMZ;->d:Landroid/graphics/RectF;

    .line 2229949
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, LX/FMZ;->e:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 2229971
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 2229972
    if-nez p1, :cond_0

    const/16 p1, 0x280

    :cond_0
    iput p1, p0, LX/FMZ;->f:I

    .line 2229973
    if-nez p2, :cond_1

    const/16 p2, 0x1e0

    :cond_1
    iput p2, p0, LX/FMZ;->g:I

    .line 2229974
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;LX/1FZ;)LX/1FJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "LX/1FZ;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2229950
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 2229951
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 2229952
    iget v2, p0, LX/FMZ;->f:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    iget v3, p0, LX/FMZ;->g:I

    int-to-float v3, v3

    div-float/2addr v3, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2229953
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2229954
    sget-object v3, LX/FMZ;->e:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 2229955
    sget-object v2, LX/FMZ;->b:Landroid/graphics/Rect;

    float-to-int v3, v0

    float-to-int v4, v1

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2229956
    sget-object v2, LX/FMZ;->c:Landroid/graphics/RectF;

    invoke-virtual {v2, v6, v6, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2229957
    sget-object v0, LX/FMZ;->e:Landroid/graphics/Matrix;

    sget-object v1, LX/FMZ;->d:Landroid/graphics/RectF;

    sget-object v2, LX/FMZ;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 2229958
    sget-object v0, LX/FMZ;->d:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    .line 2229959
    sget-object v1, LX/FMZ;->d:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    .line 2229960
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-virtual {p2, v0, v1, v2}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v1

    .line 2229961
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2229962
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2229963
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 2229964
    :cond_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    .line 2229965
    sget-object v3, LX/FMZ;->e:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 2229966
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 2229967
    invoke-virtual {v0, v5}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 2229968
    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2229969
    sget-object v0, LX/FMZ;->b:Landroid/graphics/Rect;

    sget-object v3, LX/FMZ;->c:Landroid/graphics/RectF;

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v0, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 2229970
    return-object v1
.end method
