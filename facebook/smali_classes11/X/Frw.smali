.class public LX/Frw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/common/callercontext/CallerContext;

.field public final b:LX/5SB;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fsf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsU;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FrS;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsX;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fsj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/common/callercontext/CallerContext;LX/5SB;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/5SB;",
            "LX/0Or",
            "<",
            "LX/Fsf;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FsU;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FrS;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fsj;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FsX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2296389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296390
    iput-object p1, p0, LX/Frw;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2296391
    iput-object p2, p0, LX/Frw;->b:LX/5SB;

    .line 2296392
    iput-object p3, p0, LX/Frw;->c:LX/0Or;

    .line 2296393
    iput-object p4, p0, LX/Frw;->d:LX/0Or;

    .line 2296394
    iput-object p5, p0, LX/Frw;->e:LX/0Or;

    .line 2296395
    iput-object p6, p0, LX/Frw;->g:LX/0Or;

    .line 2296396
    iput-object p7, p0, LX/Frw;->f:LX/0Or;

    .line 2296397
    return-void
.end method

.method public static a(LX/Frw;LX/FrR;Z)LX/FsJ;
    .locals 7

    .prologue
    .line 2296398
    new-instance v1, LX/G12;

    iget-object v0, p0, LX/Frw;->b:LX/5SB;

    .line 2296399
    iget-wide v5, v0, LX/5SB;->b:J

    move-wide v2, v5

    .line 2296400
    iget-object v0, p0, LX/Frw;->b:LX/5SB;

    invoke-virtual {v0}, LX/5SB;->e()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LX/Frw;->b:LX/5SB;

    invoke-virtual {v4}, LX/5SB;->d()Z

    move-result v4

    invoke-direct {v1, v2, v3, v0, v4}, LX/G12;-><init>(JLjava/lang/String;Z)V

    .line 2296401
    iget-object v0, p0, LX/Frw;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p1, p2, v1, v0}, LX/FrR;->a(ZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v0

    return-object v0
.end method
