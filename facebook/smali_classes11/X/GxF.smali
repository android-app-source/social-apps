.class public LX/GxF;
.super Lcom/facebook/webview/FacebookWebView;
.source ""


# static fields
.field public static i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6Z1",
            "<",
            "LX/GxF;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final j:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field public B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BWN;",
            ">;"
        }
    .end annotation
.end field

.field public C:Z

.field public D:LX/Gu2;

.field public k:LX/Gwu;

.field public l:LX/BWV;

.field public m:LX/GxQ;

.field public n:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field public o:LX/17X;

.field public p:Lcom/facebook/content/SecureContextHelper;

.field public q:Lcom/facebook/katana/urimap/IntentHandlerUtil;

.field public r:LX/BWW;

.field public s:LX/2Lx;

.field public t:LX/15W;

.field public u:LX/03R;

.field public v:Z

.field public w:Z

.field public x:LX/GxE;

.field public y:LX/GxA;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2407987
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/GxF;->i:Ljava/util/Set;

    .line 2407988
    const-class v0, LX/GxF;

    sput-object v0, LX/GxF;->z:Ljava/lang/Class;

    .line 2407989
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "dialtone"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "fb"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "fbinternal"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "fbrpc"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "market"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/GxF;->j:LX/2QP;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/GxQ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2407975
    invoke-direct {p0, p1}, Lcom/facebook/webview/FacebookWebView;-><init>(Landroid/content/Context;)V

    .line 2407976
    iput-boolean v1, p0, LX/GxF;->A:Z

    .line 2407977
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GxF;->B:Ljava/util/List;

    .line 2407978
    sget-object v0, LX/GxE;->PAGE_STATE_UINITIALIZED:LX/GxE;

    iput-object v0, p0, LX/GxF;->x:LX/GxE;

    .line 2407979
    iput-boolean v1, p0, LX/GxF;->C:Z

    .line 2407980
    invoke-static {p0}, LX/GxF;->n(LX/GxF;)V

    .line 2407981
    iput-object p2, p0, LX/GxF;->m:LX/GxQ;

    .line 2407982
    new-instance v0, LX/Gx6;

    invoke-direct {v0, p0}, LX/Gx6;-><init>(LX/GxF;)V

    iput-object v0, p0, LX/GxF;->k:LX/Gwu;

    .line 2407983
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GxF;->v:Z

    .line 2407984
    iput-boolean v1, p0, LX/GxF;->w:Z

    .line 2407985
    new-instance v0, LX/Gx7;

    invoke-direct {v0, p0}, LX/Gx7;-><init>(LX/GxF;)V

    invoke-virtual {p0, v0}, LX/GxF;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2407986
    return-void
.end method

.method public static a(LX/GxF;)V
    .locals 3

    .prologue
    .line 2407971
    sget-object v1, LX/GxF;->i:Ljava/util/Set;

    monitor-enter v1

    .line 2407972
    :try_start_0
    sget-object v0, LX/GxF;->i:Ljava/util/Set;

    new-instance v2, LX/6Z1;

    invoke-direct {v2, p0}, LX/6Z1;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2407973
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mRegisteredWebviews has "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/GxF;->i:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " items"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2407974
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getRegisteredFacewebWebViews()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/GxF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407898
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2407899
    sget-object v2, LX/GxF;->i:Ljava/util/Set;

    monitor-enter v2

    .line 2407900
    :try_start_0
    sget-object v0, LX/GxF;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    .line 2407901
    sget-object v0, LX/GxF;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 2407902
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2407903
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Z1;

    .line 2407904
    invoke-virtual {v0}, LX/6Z1;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GxF;

    .line 2407905
    if-nez v0, :cond_0

    .line 2407906
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2407907
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2407908
    :cond_0
    :try_start_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2407909
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "mRegisteredWebviews gc\'ed from "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, LX/GxF;->i:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " items"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2407910
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2407911
    return-object v1
.end method

.method public static n(LX/GxF;)V
    .locals 1

    .prologue
    .line 2407968
    sget-object v0, LX/GxE;->PAGE_STATE_UINITIALIZED:LX/GxE;

    iput-object v0, p0, LX/GxF;->x:LX/GxE;

    .line 2407969
    const/4 v0, 0x0

    iput-object v0, p0, LX/GxF;->y:LX/GxA;

    .line 2407970
    return-void
.end method

.method public static p(LX/GxF;)V
    .locals 3

    .prologue
    .line 2407964
    sget-object v1, LX/GxF;->i:Ljava/util/Set;

    monitor-enter v1

    .line 2407965
    :try_start_0
    sget-object v0, LX/GxF;->i:Ljava/util/Set;

    new-instance v2, LX/6Z1;

    invoke-direct {v2, p0}, LX/6Z1;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2407966
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mRegisteredWebviews has "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/GxF;->i:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " items"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2407967
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/GxA;LX/279;)V
    .locals 3

    .prologue
    .line 2407952
    invoke-virtual {p0}, LX/GxF;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2407953
    sget-object v1, LX/GxA;->AUTHENTICATION_NETWORK_ERROR:LX/GxA;

    if-ne p1, v1, :cond_1

    .line 2407954
    invoke-virtual {p1}, LX/GxA;->getErrorMessageId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 2407955
    :cond_0
    :goto_0
    return-void

    .line 2407956
    :cond_1
    sget-object v1, LX/GxA;->AUTHENTICATION_ERROR:LX/GxA;

    if-ne p1, v1, :cond_0

    .line 2407957
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v1

    .line 2407958
    if-eqz v1, :cond_0

    .line 2407959
    iget-object v2, v1, Lcom/facebook/katana/service/AppSession;->l:LX/03V;

    const-string p0, "FORCED_LOGOUT"

    invoke-virtual {p2}, LX/279;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407960
    const/4 v2, 0x1

    .line 2407961
    iput-boolean v2, v1, Lcom/facebook/katana/service/AppSession;->h:Z

    .line 2407962
    invoke-virtual {v1, v0, p2}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;LX/279;)V

    .line 2407963
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    .line 2407935
    invoke-super {p0, p1}, Lcom/facebook/webview/FacebookWebView;->a(Landroid/content/Context;)V

    .line 2407936
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/GxF;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v5

    check-cast v5, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v6

    check-cast v6, LX/17X;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(LX/0QB;)Lcom/facebook/katana/urimap/IntentHandlerUtil;

    move-result-object v8

    check-cast v8, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-static {v0}, LX/Gws;->a(LX/0QB;)LX/Gws;

    move-result-object v9

    check-cast v9, LX/BWW;

    invoke-static {v0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v10

    check-cast v10, LX/03R;

    invoke-static {v0}, LX/2Lx;->b(LX/0QB;)LX/2Lx;

    move-result-object v11

    check-cast v11, LX/2Lx;

    invoke-static {v0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v0

    check-cast v0, LX/15W;

    iput-object v4, v2, LX/GxF;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, v2, LX/GxF;->n:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    iput-object v6, v2, LX/GxF;->o:LX/17X;

    iput-object v7, v2, LX/GxF;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, LX/GxF;->q:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iput-object v9, v2, LX/GxF;->r:LX/BWW;

    iput-object v10, v2, LX/GxF;->u:LX/03R;

    iput-object v11, v2, LX/GxF;->s:LX/2Lx;

    iput-object v0, v2, LX/GxF;->t:LX/15W;

    .line 2407937
    invoke-virtual {p0}, LX/GxF;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 2407938
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 2407939
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2407940
    sget-object v2, Lcom/facebook/webview/BasicWebView;->i:Ljava/lang/String;

    move-object v2, v2

    .line 2407941
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p1, v2}, LX/0sG;->a(Landroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 2407942
    new-instance v0, LX/Gx1;

    invoke-direct {v0, p0}, LX/Gx1;-><init>(LX/GxF;)V

    iput-object v0, p0, LX/GxF;->l:LX/BWV;

    .line 2407943
    new-instance v0, LX/GxD;

    iget-object v3, p0, LX/GxF;->r:LX/BWW;

    iget-object v4, p0, LX/GxF;->l:LX/BWV;

    iget-object v5, p0, Lcom/facebook/webview/FacebookWebView;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v6, p0, Lcom/facebook/webview/BasicWebView;->a:LX/44G;

    iget-object v7, p0, LX/GxF;->u:LX/03R;

    iget-object v8, p0, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, LX/GxD;-><init>(LX/GxF;Landroid/content/Context;LX/BWW;LX/BWV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/44G;LX/03R;LX/48V;)V

    invoke-virtual {p0, v0}, LX/GxF;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2407944
    new-instance v0, LX/GxB;

    invoke-direct {v0, p0}, LX/GxB;-><init>(LX/GxF;)V

    invoke-virtual {p0, v0}, LX/GxF;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 2407945
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 2407946
    const-string v1, "resetCache"

    new-instance v2, LX/Gx2;

    invoke-direct {v2, p0, p1}, LX/Gx2;-><init>(LX/GxF;Landroid/content/Context;)V

    invoke-virtual {p0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2407947
    const-string v1, "startSyncFriends"

    new-instance v2, LX/Gx3;

    invoke-direct {v2, p0}, LX/Gx3;-><init>(LX/GxF;)V

    invoke-virtual {p0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2407948
    const-string v1, "setRootVersion"

    new-instance v2, LX/Gx4;

    invoke-direct {v2, p0}, LX/Gx4;-><init>(LX/GxF;)V

    invoke-virtual {p0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2407949
    const-string v1, "reloadCurrent"

    new-instance v2, LX/Gx5;

    invoke-direct {v2, p0}, LX/Gx5;-><init>(LX/GxF;)V

    invoke-virtual {p0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2407950
    const-string v1, "broadcastScript"

    new-instance v2, LX/Gx9;

    invoke-direct {v2, p0, v0}, LX/Gx9;-><init>(LX/GxF;Landroid/os/Handler;)V

    invoke-virtual {p0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2407951
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 2407990
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 2407991
    if-eqz v0, :cond_0

    .line 2407992
    iget-object v0, p0, LX/GxF;->s:LX/2Lx;

    invoke-virtual {p0}, LX/GxF;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/2Lx;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2407993
    :cond_0
    move-object v0, p1

    .line 2407994
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2407995
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2407996
    if-nez v1, :cond_1

    .line 2407997
    :goto_0
    return-void

    .line 2407998
    :cond_1
    invoke-static {p0}, LX/GxF;->n(LX/GxF;)V

    .line 2407999
    invoke-static {v0}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2408000
    if-eqz v2, :cond_2

    .line 2408001
    iget-object v3, p0, Lcom/facebook/webview/FacebookWebView;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x240003

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p1, "FacewebPageNetworkLoad:"

    invoke-direct {v5, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2408002
    iget-object v3, p0, Lcom/facebook/webview/FacebookWebView;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x240004

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p1, "FacewebPageRPCLoadCompleted:"

    invoke-direct {v5, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2408003
    :cond_2
    if-eqz p2, :cond_3

    .line 2408004
    iput-object v1, p0, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    .line 2408005
    :cond_3
    invoke-virtual {p0}, LX/GxF;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/Gx0;

    invoke-direct {v3, p0, v1}, LX/Gx0;-><init>(LX/GxF;Ljava/lang/String;)V

    .line 2408006
    invoke-static {v2}, LX/GxG;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 2408007
    if-nez v4, :cond_4

    .line 2408008
    const-string v4, ""

    .line 2408009
    :cond_4
    new-instance v5, LX/Gwt;

    invoke-static {v2}, LX/38I;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v5, p1, v4}, LX/Gwt;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2408010
    invoke-static {v2}, LX/Gwx;->d(Landroid/content/Context;)LX/Gwz;

    move-result-object p1

    invoke-virtual {p1, v5, v3}, LX/Gwz;->a(LX/Gwt;LX/Gwu;)LX/Gww;

    move-result-object v5

    .line 2408011
    if-nez v5, :cond_6

    .line 2408012
    :goto_1
    if-eqz v1, :cond_8

    const-string v2, "/ads/manage/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2408013
    const/4 v2, 0x1

    .line 2408014
    :goto_2
    move v1, v2

    .line 2408015
    if-eqz v1, :cond_5

    .line 2408016
    iget-object v1, p0, LX/GxF;->t:LX/15W;

    invoke-virtual {p0}, LX/GxF;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TO_ADS_MANAGER_M_SITE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2408017
    const-class v4, LX/0i1;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2408018
    :cond_5
    iget-object v1, p0, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    invoke-virtual {v1, p0, v0}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2408019
    :cond_6
    invoke-virtual {v5}, LX/Gww;->c()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 2408020
    iget-object v5, v5, LX/44w;->a:Ljava/lang/Object;

    check-cast v5, LX/44w;

    .line 2408021
    iget-object p1, v5, LX/44w;->a:Ljava/lang/Object;

    check-cast p1, LX/Gwv;

    iget-object v5, v5, LX/44w;->b:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-interface {v3, p1, v5}, LX/Gwu;->a(LX/Gwv;Ljava/lang/String;)V

    goto :goto_1

    .line 2408022
    :cond_7
    invoke-virtual {v5}, LX/Gww;->b()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v5

    invoke-interface {v3, v5}, LX/Gwu;->a(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V

    goto :goto_1

    :cond_8
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 2407930
    invoke-static {p0}, LX/GxF;->n(LX/GxF;)V

    .line 2407931
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2407932
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "fb4ar"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2407933
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/GxF;->a(Ljava/lang/String;Z)V

    .line 2407934
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2407927
    invoke-static {p0}, LX/GxF;->n(LX/GxF;)V

    .line 2407928
    invoke-super {p0}, Lcom/facebook/webview/FacebookWebView;->reload()V

    .line 2407929
    return-void
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 2407918
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GxF;->w:Z

    .line 2407919
    iget-boolean v0, p0, Lcom/facebook/webview/FacebookWebView;->j:Z

    move v0, v0

    .line 2407920
    if-eqz v0, :cond_0

    .line 2407921
    invoke-virtual {p0}, LX/GxF;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 2407922
    if-eqz v0, :cond_0

    .line 2407923
    iget-object v0, p0, LX/GxF;->n:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 2407924
    :cond_0
    invoke-static {p0}, LX/GxF;->p(LX/GxF;)V

    .line 2407925
    invoke-super {p0}, Lcom/facebook/webview/FacebookWebView;->destroy()V

    .line 2407926
    return-void
.end method

.method public final declared-synchronized e()V
    .locals 3

    .prologue
    .line 2407912
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/GxF;->A:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 2407913
    :goto_0
    monitor-exit p0

    return-void

    .line 2407914
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/GxF;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWN;

    .line 2407915
    invoke-virtual {p0}, LX/GxF;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/facebook/webview/FacebookWebView;->a(Landroid/content/Context;LX/BWN;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2407916
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2407917
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/GxF;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 2407894
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/GxF;->A:Z

    .line 2407895
    invoke-virtual {p0}, LX/GxF;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2407896
    monitor-exit p0

    return-void

    .line 2407897
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2407892
    const-string v0, "(function(){try {if (window.fwHaveLoadedPage && fwHaveLoadedPage()) {return \'1\';}} catch (e) {return \'0\';}})()"

    new-instance v1, LX/Gx8;

    invoke-direct {v1, p0}, LX/Gx8;-><init>(LX/GxF;)V

    invoke-virtual {p0, v0, v1}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2407893
    return-void
.end method

.method public final loadUrl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2407889
    iget-boolean v0, p0, LX/GxF;->w:Z

    if-nez v0, :cond_0

    .line 2407890
    invoke-super {p0, p1}, Lcom/facebook/webview/FacebookWebView;->loadUrl(Ljava/lang/String;)V

    .line 2407891
    :cond_0
    return-void
.end method
