.class public LX/Gpf;
.super LX/CnT;
.source ""

# interfaces
.implements LX/Cny;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingFeedVideoBlockView;",
        "LX/Gp4;",
        ">;",
        "LX/Cny;"
    }
.end annotation


# instance fields
.field public d:LX/GqR;

.field private e:Z


# direct methods
.method public constructor <init>(LX/GqR;)V
    .locals 1

    .prologue
    .line 2395372
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395373
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gpf;->e:Z

    .line 2395374
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 8

    .prologue
    .line 2395375
    check-cast p1, LX/Gp4;

    .line 2395376
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395377
    check-cast v0, LX/GqR;

    iput-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395378
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2395379
    iget-object v0, p1, LX/Gp4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2395380
    iget-object v1, p1, LX/Gp4;->b:LX/3FT;

    move-object v1, v1

    .line 2395381
    iget v2, p1, LX/Gp4;->c:I

    move v2, v2

    .line 2395382
    iget v3, p1, LX/Gp4;->d:I

    move v3, v3

    .line 2395383
    iget-object v4, p1, LX/Gp4;->e:LX/04D;

    move-object v4, v4

    .line 2395384
    iget-object v5, p1, LX/Gp4;->f:LX/04g;

    move-object v5, v5

    .line 2395385
    iget-object v6, p1, LX/Gp4;->g:LX/394;

    move-object v6, v6

    .line 2395386
    iget-object v7, p0, LX/Gpf;->d:LX/GqR;

    .line 2395387
    iput-object v0, v7, LX/GqR;->q:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2395388
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395389
    iput-object v1, v0, LX/GqR;->r:LX/3FT;

    .line 2395390
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395391
    iput v2, v0, LX/GqR;->s:I

    .line 2395392
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395393
    iput v3, v0, LX/GqR;->t:I

    .line 2395394
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395395
    iput-object v4, v0, LX/GqR;->u:LX/04D;

    .line 2395396
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395397
    iput-object v5, v0, LX/GqR;->v:LX/04g;

    .line 2395398
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395399
    iput-object v6, v0, LX/GqR;->m:LX/394;

    .line 2395400
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    .line 2395401
    sget-object v1, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {v0, v1}, LX/Cos;->a(LX/Cqw;)V

    .line 2395402
    iget-boolean v0, p0, LX/Gpf;->e:Z

    if-nez v0, :cond_0

    .line 2395403
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    invoke-virtual {v0}, LX/GqR;->g()V

    .line 2395404
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gpf;->e:Z

    .line 2395405
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2395406
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2395407
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 2395408
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    if-eqz v0, :cond_4

    .line 2395409
    iget-object v0, p0, LX/Gpf;->d:LX/GqR;

    const/4 p0, 0x1

    .line 2395410
    iget-object v1, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_3

    .line 2395411
    iget-object v1, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, p0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2395412
    const/4 v2, 0x0

    .line 2395413
    iget-object v1, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v1

    .line 2395414
    if-gez v1, :cond_0

    move v1, v2

    .line 2395415
    :cond_0
    iget-object v3, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v3

    .line 2395416
    if-gez v3, :cond_1

    move v3, v2

    .line 2395417
    :cond_1
    if-le v3, v1, :cond_2

    move v3, v1

    .line 2395418
    :cond_2
    new-instance v4, LX/7Ju;

    invoke-direct {v4}, LX/7Ju;-><init>()V

    .line 2395419
    iput-boolean v2, v4, LX/7Ju;->a:Z

    .line 2395420
    move-object v4, v4

    .line 2395421
    iget-object v5, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->s()Z

    move-result v5

    .line 2395422
    iput-boolean v5, v4, LX/7Ju;->b:Z

    .line 2395423
    move-object v4, v4

    .line 2395424
    iput v1, v4, LX/7Ju;->c:I

    .line 2395425
    move-object v1, v4

    .line 2395426
    iput v3, v1, LX/7Ju;->d:I

    .line 2395427
    move-object v1, v1

    .line 2395428
    iput-boolean v2, v1, LX/7Ju;->e:Z

    .line 2395429
    move-object v1, v1

    .line 2395430
    sget-object v2, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 2395431
    iput-object v2, v1, LX/7Ju;->h:LX/04g;

    .line 2395432
    move-object v1, v1

    .line 2395433
    invoke-virtual {v1}, LX/7Ju;->a()LX/7Jv;

    move-result-object v1

    move-object v1, v1

    .line 2395434
    iget-object v2, v0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    sget-object v4, LX/04G;->CANVAS:LX/04G;

    iget-object v5, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(LX/04G;LX/04G;I)V

    .line 2395435
    iget-object v2, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v2, p0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2395436
    iget-object v2, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v3, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2395437
    iget-object v2, v0, LX/GqR;->a:LX/Bug;

    invoke-virtual {v2}, LX/Bug;->a()V

    .line 2395438
    iget-object v2, v0, LX/GqR;->m:LX/394;

    if-eqz v2, :cond_3

    .line 2395439
    iget-object v2, v0, LX/GqR;->m:LX/394;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-interface {v2, v3, v1}, LX/394;->a(LX/04g;LX/7Jv;)V

    .line 2395440
    :cond_3
    const/4 v1, 0x0

    .line 2395441
    iput-object v1, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2395442
    iput-object v1, v0, LX/GqR;->v:LX/04g;

    .line 2395443
    iput-object v1, v0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2395444
    iput-object v1, v0, LX/GqR;->q:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2395445
    iput-object v1, v0, LX/GqR;->p:LX/2pa;

    .line 2395446
    :cond_4
    return-void
.end method
