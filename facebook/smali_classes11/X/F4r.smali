.class public final LX/F4r;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/groups/fb4a/create/protocol/FetchCreatorPageInfoModels$FetchCreatorPageNameQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F4u;


# direct methods
.method public constructor <init>(LX/F4u;)V
    .locals 0

    .prologue
    .line 2197730
    iput-object p1, p0, LX/F4r;->a:LX/F4u;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2197731
    iget-object v0, p0, LX/F4r;->a:LX/F4u;

    iget-object v0, v0, LX/F4u;->f:LX/03V;

    sget-object v1, LX/F4u;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fetch page name failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2197732
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2197733
    check-cast p1, Lcom/facebook/groups/fb4a/create/protocol/FetchCreatorPageInfoModels$FetchCreatorPageNameQueryModel;

    .line 2197734
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/fb4a/create/protocol/FetchCreatorPageInfoModels$FetchCreatorPageNameQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2197735
    :cond_0
    :goto_0
    return-void

    .line 2197736
    :cond_1
    iget-object v0, p0, LX/F4r;->a:LX/F4u;

    iget-object v0, v0, LX/F4u;->c:LX/F53;

    invoke-virtual {p1}, Lcom/facebook/groups/fb4a/create/protocol/FetchCreatorPageInfoModels$FetchCreatorPageNameQueryModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F53;->g(Ljava/lang/String;)V

    goto :goto_0
.end method
