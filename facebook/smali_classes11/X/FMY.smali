.class public LX/FMY;
.super LX/0Om;
.source ""


# static fields
.field private static final a:Ljava/util/Random;

.field private static final b:Landroid/content/UriMatcher;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2229835
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, LX/FMY;->a:Ljava/util/Random;

    .line 2229836
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, LX/FMY;->b:Landroid/content/UriMatcher;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2229837
    invoke-direct {p0}, LX/0Om;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/EdM;)Landroid/net/Uri;
    .locals 7

    .prologue
    .line 2229838
    invoke-static {}, LX/FMY;->d()Landroid/net/Uri;

    move-result-object v0

    .line 2229839
    invoke-static {p0, v0}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 2229840
    const/4 v2, 0x0

    .line 2229841
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2229842
    :try_start_1
    new-instance v2, LX/Edc;

    invoke-direct {v2, p0, p1}, LX/Edc;-><init>(Landroid/content/Context;LX/EdM;)V

    const/4 v4, 0x0

    .line 2229843
    iget-object v5, v2, LX/Edc;->d:LX/EdM;

    invoke-virtual {v5}, LX/EdM;->b()I

    move-result v5

    .line 2229844
    packed-switch v5, :pswitch_data_0

    .line 2229845
    :cond_0
    :goto_0
    :pswitch_0
    move v2, v4

    .line 2229846
    if-nez v2, :cond_3

    .line 2229847
    new-instance v0, LX/EdT;

    const-string v2, "Failure writing pdu to file"

    invoke-direct {v0, v2}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2229848
    :catch_0
    move-exception v0

    .line 2229849
    :goto_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2229850
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 2229851
    :cond_1
    const-string v2, "MmsFileProvider"

    const-string v4, "Cannot create temporary file %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v2, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2229852
    new-instance v0, LX/EdT;

    const-string v2, "Cannot create raw mms file"

    invoke-direct {v0, v2}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2229853
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 2229854
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 2229855
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2229856
    :cond_2
    :goto_3
    throw v0

    .line 2229857
    :cond_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 2229858
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 2229859
    :goto_4
    return-object v0

    .line 2229860
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 2229861
    :goto_5
    :try_start_5
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2229862
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 2229863
    :cond_4
    const-string v2, "MmsFileProvider"

    const-string v3, "Out of memory in composing PDU"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2229864
    new-instance v0, LX/EdT;

    const-string v2, "Cannot create raw mms file because out of memory"

    invoke-direct {v0, v2}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception v1

    goto :goto_3

    .line 2229865
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 2229866
    :catch_3
    move-exception v0

    goto :goto_5

    .line 2229867
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 2229868
    :catch_5
    goto :goto_4

    .line 2229869
    :pswitch_1
    invoke-static {v2}, LX/Edc;->e(LX/Edc;)I

    move-result v5

    if-nez v5, :cond_0

    .line 2229870
    :cond_5
    iget-object v4, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4, v1}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 2229871
    const/4 v4, 0x1

    goto :goto_0

    .line 2229872
    :pswitch_2
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2229873
    iget-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    if-nez p0, :cond_6

    .line 2229874
    new-instance p0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2229875
    iput v6, v2, LX/Edc;->b:I

    .line 2229876
    :cond_6
    const/16 p0, 0x8c

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229877
    const/16 p0, 0x83

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229878
    const/16 p0, 0x98

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-eqz p0, :cond_e

    .line 2229879
    :cond_7
    :goto_6
    move v5, v5

    .line 2229880
    if-eqz v5, :cond_5

    goto/16 :goto_0

    .line 2229881
    :pswitch_3
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2229882
    iget-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    if-nez p0, :cond_8

    .line 2229883
    new-instance p0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2229884
    iput v6, v2, LX/Edc;->b:I

    .line 2229885
    :cond_8
    const/16 p0, 0x8c

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229886
    const/16 p0, 0x85

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229887
    const/16 p0, 0x98

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-eqz p0, :cond_f

    .line 2229888
    :cond_9
    :goto_7
    move v5, v5

    .line 2229889
    if-eqz v5, :cond_5

    goto/16 :goto_0

    .line 2229890
    :pswitch_4
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2229891
    iget-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    if-nez p0, :cond_a

    .line 2229892
    new-instance p0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2229893
    iput v6, v2, LX/Edc;->b:I

    .line 2229894
    :cond_a
    const/16 p0, 0x8c

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229895
    const/16 p0, 0x87

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229896
    const/16 p0, 0x8d

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-eqz p0, :cond_10

    .line 2229897
    :cond_b
    :goto_8
    move v5, v5

    .line 2229898
    if-eqz v5, :cond_5

    goto/16 :goto_0

    .line 2229899
    :pswitch_5
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2229900
    iget-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    if-nez p0, :cond_c

    .line 2229901
    new-instance p0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object p0, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2229902
    iput v6, v2, LX/Edc;->b:I

    .line 2229903
    :cond_c
    const/16 p0, 0x8c

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229904
    const/16 p0, 0x82

    invoke-static {v2, p0}, LX/Edc;->a(LX/Edc;I)V

    .line 2229905
    const/16 p0, 0x98

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-eqz p0, :cond_11

    .line 2229906
    :cond_d
    :goto_9
    move v5, v5

    .line 2229907
    if-eqz v5, :cond_5

    goto/16 :goto_0

    .line 2229908
    :cond_e
    const/16 p0, 0x8d

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_7

    .line 2229909
    const/16 p0, 0x95

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_7

    move v5, v6

    .line 2229910
    goto/16 :goto_6

    .line 2229911
    :cond_f
    const/16 p0, 0x8d

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_9

    .line 2229912
    const/16 v5, 0x91

    invoke-static {v2, v5}, LX/Edc;->e(LX/Edc;I)I

    move v5, v6

    .line 2229913
    goto :goto_7

    .line 2229914
    :cond_10
    const/16 p0, 0x8b

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_b

    .line 2229915
    const/16 p0, 0x97

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_b

    .line 2229916
    const/16 p0, 0x89

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_b

    .line 2229917
    const/16 p0, 0x85

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    .line 2229918
    const/16 p0, 0x9b

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_b

    move v5, v6

    .line 2229919
    goto :goto_8

    .line 2229920
    :cond_11
    const/16 p0, 0x8d

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_d

    .line 2229921
    const/16 p0, 0x89

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_d

    .line 2229922
    const/16 p0, 0x96

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    .line 2229923
    const/16 p0, 0x8a

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_d

    .line 2229924
    const/16 p0, 0x8e

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_d

    .line 2229925
    const/16 p0, 0x88

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_d

    .line 2229926
    const/16 p0, 0x83

    invoke-static {v2, p0}, LX/Edc;->e(LX/Edc;I)I

    move-result p0

    if-nez p0, :cond_d

    move v5, v6

    .line 2229927
    goto/16 :goto_9

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;
    .locals 4

    .prologue
    .line 2229938
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "rawmms"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2229939
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 2229940
    move-object v0, v0

    .line 2229941
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2229942
    invoke-static {v0, v1}, LX/FMY;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2229943
    return-object v1
.end method

.method private static a(Ljava/io/File;Ljava/io/File;)Z
    .locals 3

    .prologue
    .line 2229928
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v1

    .line 2229929
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    .line 2229930
    :goto_0
    if-eqz v0, :cond_1

    .line 2229931
    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2229932
    const/4 v0, 0x1

    .line 2229933
    :goto_1
    return v0

    .line 2229934
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2229935
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2229936
    :catch_0
    move-exception v0

    .line 2229937
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error checking paths"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static d()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 2229944
    sget-object v0, LX/FMY;->a:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 2229945
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "com.facebook.messaging.sms.MmsFileProvider"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2229831
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2229832
    sget-object v1, LX/FMY;->b:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2229833
    :cond_0
    :goto_0
    return v0

    .line 2229834
    :pswitch_0
    invoke-virtual {p0}, LX/FMY;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 6

    .prologue
    .line 2229829
    invoke-virtual {p0, p1, p2}, LX/FMY;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 2229830
    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 2229828
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2229827
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2229826
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 2229824
    sget-object v0, LX/FMY;->b:Landroid/content/UriMatcher;

    const-string v1, "com.facebook.messaging.sms.MmsFileProvider"

    const-string v2, "#"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2229825
    return-void
.end method

.method public final b(Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;
    .locals 1

    .prologue
    .line 2229823
    const-string v0, "r"

    invoke-virtual {p0, p1, v0}, LX/FMY;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 2

    .prologue
    .line 2229816
    sget-object v0, LX/FMY;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2229817
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 2229818
    :pswitch_0
    invoke-virtual {p0}, LX/FMY;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    .line 2229819
    const-string v0, "r"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2229820
    const/high16 v0, 0x10000000

    .line 2229821
    :goto_0
    invoke-static {v1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0

    .line 2229822
    :cond_0
    const/high16 v0, 0x2c000000

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
