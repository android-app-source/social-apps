.class public final LX/Gsy;
.super LX/4oq;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2400372
    iput-object p1, p0, LX/Gsy;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0, p2}, LX/4oq;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2400373
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2400374
    invoke-super {p0, p1, p2}, LX/4oq;->a(Landroid/view/ViewGroup;Ljava/lang/String;)V

    .line 2400375
    :cond_0
    return-void
.end method

.method public final getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2400376
    const-string v0, ""

    return-object v0
.end method

.method public final persistString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2400377
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2400378
    const/4 v0, 0x1

    .line 2400379
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/4oq;->persistString(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
