.class public LX/F5Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2198907
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2198908
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2198909
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2198910
    const-string v2, "group"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198911
    invoke-static {}, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->m()LX/F5b;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v2

    const-string v3, "FBGroupsAdminActivityRoute"

    .line 2198912
    iput-object v3, v2, LX/98r;->b:Ljava/lang/String;

    .line 2198913
    move-object v2, v2

    .line 2198914
    const-string v3, "/groups_admin_activity"

    .line 2198915
    iput-object v3, v2, LX/98r;->a:Ljava/lang/String;

    .line 2198916
    move-object v2, v2

    .line 2198917
    invoke-virtual {v2}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/F5b;->a(Landroid/os/Bundle;)LX/F5b;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/F5b;->b(Landroid/os/Bundle;)LX/F5b;

    move-result-object v0

    invoke-virtual {v0}, LX/F5b;->a()Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    return-object v0
.end method
