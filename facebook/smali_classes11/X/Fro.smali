.class public final LX/Fro;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic b:LX/G4x;

.field public final synthetic c:LX/FqO;

.field public final synthetic d:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

.field public final synthetic e:LX/Frv;


# direct methods
.method public constructor <init>(LX/Frv;Ljava/util/concurrent/atomic/AtomicBoolean;LX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 0

    .prologue
    .line 2296091
    iput-object p1, p0, LX/Fro;->e:LX/Frv;

    iput-object p2, p0, LX/Fro;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, LX/Fro;->b:LX/G4x;

    iput-object p4, p0, LX/Fro;->c:LX/FqO;

    iput-object p5, p0, LX/Fro;->d:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2296093
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2296094
    check-cast p1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 2296095
    iget-object v0, p0, LX/Fro;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v1, p0, LX/Fro;->b:LX/G4x;

    iget-object v2, p0, LX/Fro;->c:LX/FqO;

    iget-object v3, p0, LX/Fro;->d:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-static {p1, v0, v1, v2, v3}, LX/Frv;->a(Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;Ljava/util/concurrent/atomic/AtomicBoolean;LX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296096
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2296092
    return-void
.end method
