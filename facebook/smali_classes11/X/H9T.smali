.class public final LX/H9T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/H9V;


# direct methods
.method public constructor <init>(LX/H9V;)V
    .locals 0

    .prologue
    .line 2434370
    iput-object p1, p0, LX/H9T;->a:LX/H9V;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    .line 2434371
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2434372
    iget-object v0, p0, LX/H9T;->a:LX/H9V;

    .line 2434373
    iget-object v1, v0, LX/H9V;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9XE;

    sget-object v2, LX/9X4;->EVENT_ADMIN_SHARE_PHOTO:LX/9X4;

    iget-object v3, v0, LX/H9V;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, LX/9XE;->b(LX/9X2;J)V

    .line 2434374
    iget-object v1, v0, LX/H9V;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H8W;

    iget-object v2, v0, LX/H9V;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->p()Z

    move-result v2

    iget-object v3, v0, LX/H9V;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->n()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/H8W;->a(ZZ)LX/CSL;

    move-result-object v2

    iget-object v1, v0, LX/H9V;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-object v1, v0, LX/H9V;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v0, LX/H9V;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->x()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v6

    move-object v7, p1

    invoke-virtual/range {v2 .. v7}, LX/CSL;->b(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v3

    .line 2434375
    iget-object v1, v0, LX/H9V;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ay;

    const/16 v4, 0x6dc

    iget-object v2, v0, LX/H9V;->j:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v1, v3, v4, v2}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2434376
    const/4 v0, 0x1

    return v0
.end method
