.class public LX/GE9;
.super Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2U3;

.field private final b:LX/0W9;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/2U3;LX/0W9;LX/0ad;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331121
    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p1

    move-object v5, p5

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;-><init>(LX/0tX;LX/0rq;LX/0sa;LX/GF4;LX/1Ck;LX/GG6;LX/0ad;)V

    .line 2331122
    move-object/from16 v0, p7

    iput-object v0, p0, LX/GE9;->a:LX/2U3;

    .line 2331123
    move-object/from16 v0, p8

    iput-object v0, p0, LX/GE9;->b:LX/0W9;

    .line 2331124
    return-void
.end method

.method public static a(LX/0QB;)LX/GE9;
    .locals 11

    .prologue
    .line 2331118
    new-instance v1, LX/GE9;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v7

    check-cast v7, LX/GG6;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v8

    check-cast v8, LX/2U3;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v9

    check-cast v9, LX/0W9;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v1 .. v10}, LX/GE9;-><init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/2U3;LX/0W9;LX/0ad;)V

    .line 2331119
    move-object v0, v1

    .line 2331120
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .locals 4

    .prologue
    .line 2331084
    new-instance v0, LX/GGN;

    invoke-direct {v0}, LX/GGN;-><init>()V

    invoke-virtual {p0, p1}, LX/GE9;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v1

    .line 2331085
    iput-object v1, v0, LX/GGN;->a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2331086
    move-object v0, v0

    .line 2331087
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, LX/GE9;->b:LX/0W9;

    invoke-static {p1, v2, v3, v1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;DLX/0W9;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v1

    .line 2331088
    iput-object v1, v0, LX/GGN;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2331089
    move-object v0, v0

    .line 2331090
    invoke-virtual {p0, p1}, LX/GE9;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v1

    .line 2331091
    iput-object v1, v0, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2331092
    move-object v0, v0

    .line 2331093
    const-string v1, "boosted_page_like_mobile"

    .line 2331094
    iput-object v1, v0, LX/GGI;->m:Ljava/lang/String;

    .line 2331095
    move-object v0, v0

    .line 2331096
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    .line 2331097
    iput-object v1, v0, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2331098
    move-object v0, v0

    .line 2331099
    iput-object p2, v0, LX/GGI;->c:Ljava/lang/String;

    .line 2331100
    move-object v0, v0

    .line 2331101
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/GGB;

    move-result-object v1

    .line 2331102
    iput-object v1, v0, LX/GGI;->e:LX/GGB;

    .line 2331103
    move-object v0, v0

    .line 2331104
    sget-object v1, LX/8wL;->PAGE_LIKE:LX/8wL;

    .line 2331105
    iput-object v1, v0, LX/GGI;->b:LX/8wL;

    .line 2331106
    move-object v0, v0

    .line 2331107
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->d(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/0Px;

    move-result-object v1

    .line 2331108
    iput-object v1, v0, LX/GGI;->n:LX/0Px;

    .line 2331109
    move-object v0, v0

    .line 2331110
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->e(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)I

    move-result v1

    .line 2331111
    iput v1, v0, LX/GGI;->o:I

    .line 2331112
    move-object v0, v0

    .line 2331113
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2331114
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2331115
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2331116
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    .line 2331117
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2331125
    invoke-virtual {p0, p1}, LX/GE9;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/GE9;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2331126
    :cond_0
    iget-object v0, p0, LX/GE9;->a:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Default Spec is null"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2331127
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2331128
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v0

    .line 2331129
    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v5, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2331130
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2331131
    new-instance v3, LX/GGK;

    invoke-direct {v3}, LX/GGK;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->p()Ljava/lang/String;

    move-result-object v4

    .line 2331132
    iput-object v4, v3, LX/GGK;->c:Ljava/lang/String;

    .line 2331133
    move-object v3, v3

    .line 2331134
    iput-object v0, v3, LX/GGK;->h:Ljava/lang/String;

    .line 2331135
    move-object v0, v3

    .line 2331136
    invoke-virtual {v2, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2331137
    iput-object v1, v0, LX/GGK;->e:Ljava/lang/String;

    .line 2331138
    move-object v0, v0

    .line 2331139
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2331140
    iput-object v1, v0, LX/GGK;->a:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2331141
    move-object v0, v0

    .line 2331142
    invoke-virtual {v0}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    .line 2331143
    :goto_1
    return-object v0

    .line 2331144
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2331145
    :cond_2
    invoke-virtual {p0, p1}, LX/GE9;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2331146
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2331147
    new-instance v2, LX/GGK;

    invoke-direct {v2}, LX/GGK;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->p()Ljava/lang/String;

    move-result-object v3

    .line 2331148
    iput-object v3, v2, LX/GGK;->c:Ljava/lang/String;

    .line 2331149
    move-object v2, v2

    .line 2331150
    const/4 v3, 0x2

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2331151
    iput-object v3, v2, LX/GGK;->h:Ljava/lang/String;

    .line 2331152
    move-object v2, v2

    .line 2331153
    invoke-virtual {v1, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2331154
    iput-object v0, v2, LX/GGK;->e:Ljava/lang/String;

    .line 2331155
    move-object v0, v2

    .line 2331156
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2331157
    iput-object v1, v0, LX/GGK;->a:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2331158
    move-object v0, v0

    .line 2331159
    invoke-virtual {v0}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    goto :goto_1

    .line 2331160
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2331083
    const-string v0, "page_like_promotion_key"

    return-object v0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2331080
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2331081
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2331082
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2331079
    const-string v0, "boosted_page_like_mobile"

    return-object v0
.end method
