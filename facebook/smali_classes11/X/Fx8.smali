.class public LX/Fx8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fx7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Fx7",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
        ">;"
    }
.end annotation


# instance fields
.field private a:[LX/5vn;

.field private b:[LX/5vn;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2304319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304320
    return-void
.end method

.method private static a(IIII)LX/5vn;
    .locals 5

    .prologue
    .line 2304306
    new-instance v0, LX/5vq;

    invoke-direct {v0}, LX/5vq;-><init>()V

    int-to-double v2, p0

    .line 2304307
    iput-wide v2, v0, LX/5vq;->c:D

    .line 2304308
    move-object v0, v0

    .line 2304309
    int-to-double v2, p1

    .line 2304310
    iput-wide v2, v0, LX/5vq;->d:D

    .line 2304311
    move-object v0, v0

    .line 2304312
    int-to-double v2, p2

    .line 2304313
    iput-wide v2, v0, LX/5vq;->b:D

    .line 2304314
    move-object v0, v0

    .line 2304315
    int-to-double v2, p3

    .line 2304316
    iput-wide v2, v0, LX/5vq;->a:D

    .line 2304317
    move-object v0, v0

    .line 2304318
    invoke-virtual {v0}, LX/5vq;->a()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method private a(I)[LX/5vn;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 2304321
    packed-switch p1, :pswitch_data_0

    .line 2304322
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "suggested photos should be between 1 and 2. actual is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2304323
    :pswitch_0
    iget-object v0, p0, LX/Fx8;->a:[LX/5vn;

    if-nez v0, :cond_0

    .line 2304324
    new-array v0, v4, [LX/5vn;

    const/4 v1, 0x6

    invoke-static {v2, v2, v1, v3}, LX/Fx8;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, LX/Fx8;->a:[LX/5vn;

    .line 2304325
    :cond_0
    iget-object v0, p0, LX/Fx8;->a:[LX/5vn;

    .line 2304326
    :goto_0
    return-object v0

    .line 2304327
    :pswitch_1
    iget-object v0, p0, LX/Fx8;->b:[LX/5vn;

    if-nez v0, :cond_1

    .line 2304328
    const/4 v0, 0x2

    new-array v0, v0, [LX/5vn;

    invoke-static {v2, v2, v3, v3}, LX/Fx8;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3, v2, v3, v3}, LX/Fx8;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, LX/Fx8;->b:[LX/5vn;

    .line 2304329
    :cond_1
    iget-object v0, p0, LX/Fx8;->b:[LX/5vn;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/0Px;I)LX/5vo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
            ">;I)",
            "LX/5vo;"
        }
    .end annotation

    .prologue
    .line 2304305
    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$SuggestedPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$SuggestedPhotoModel;->a()LX/5vo;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0Px;I)LX/5vn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
            ">;I)",
            "LX/5vn;"
        }
    .end annotation

    .prologue
    .line 2304303
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-direct {p0, v0}, LX/Fx8;->a(I)[LX/5vn;

    move-result-object v0

    .line 2304304
    aget-object v0, v0, p2

    return-object v0
.end method
