.class public LX/HEW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/HEW;


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2442118
    iput-object p1, p0, LX/HEW;->a:LX/0ad;

    .line 2442119
    iput-object p2, p0, LX/HEW;->b:LX/0Uh;

    .line 2442120
    return-void
.end method

.method public static a(LX/0QB;)LX/HEW;
    .locals 5

    .prologue
    .line 2442121
    sget-object v0, LX/HEW;->c:LX/HEW;

    if-nez v0, :cond_1

    .line 2442122
    const-class v1, LX/HEW;

    monitor-enter v1

    .line 2442123
    :try_start_0
    sget-object v0, LX/HEW;->c:LX/HEW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2442124
    if-eqz v2, :cond_0

    .line 2442125
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2442126
    new-instance p0, LX/HEW;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/HEW;-><init>(LX/0ad;LX/0Uh;)V

    .line 2442127
    move-object v0, p0

    .line 2442128
    sput-object v0, LX/HEW;->c:LX/HEW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2442129
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2442130
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2442131
    :cond_1
    sget-object v0, LX/HEW;->c:LX/HEW;

    return-object v0

    .line 2442132
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2442133
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 2442116
    iget-object v0, p0, LX/HEW;->b:LX/0Uh;

    sget v1, LX/HEX;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
