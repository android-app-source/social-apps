.class public final LX/Gzn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/model/FormData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V
    .locals 0

    .prologue
    .line 2412347
    iput-object p1, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2412334
    iget-object v0, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Z)V

    .line 2412335
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2412336
    check-cast p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412337
    iget-object v0, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Z)V

    .line 2412338
    if-nez p1, :cond_0

    .line 2412339
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "something is wrong"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/Gzn;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2412340
    :cond_0
    iget-object v0, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    .line 2412341
    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412342
    iget-object v0, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->f()Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    move-result-object v1

    .line 2412343
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->o:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412344
    iget-object v0, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    iget-object v1, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-virtual {v0, v1}, LX/H0P;->a(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V

    .line 2412345
    iget-object v0, p0, LX/Gzn;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->h:LX/H0P;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2412346
    return-void
.end method
