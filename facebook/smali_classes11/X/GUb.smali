.class public final LX/GUb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GUa;


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 2357471
    iput-object p1, p0, LX/GUb;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;)V
    .locals 4

    .prologue
    .line 2357472
    iget-object v0, p0, LX/GUb;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->W:LX/GUi;

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getDesignName()Ljava/lang/String;

    move-result-object v1

    .line 2357473
    iget v2, p1, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->a:I

    move v2, v2

    .line 2357474
    const-string v3, "friends_nearby_settings_upsell_click"

    invoke-static {v3}, LX/GUi;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2357475
    const-string p1, "design_name"

    invoke-virtual {v3, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2357476
    const-string p1, "total_friends_sharing"

    invoke-virtual {v3, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2357477
    iget-object p1, v0, LX/GUi;->a:LX/0Zb;

    invoke-interface {p1, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2357478
    iget-object v0, p0, LX/GUb;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    .line 2357479
    return-void
.end method
