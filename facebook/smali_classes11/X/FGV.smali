.class public LX/FGV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FGT;",
        "LX/FGU;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FGV;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2219056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219057
    iput-object p1, p0, LX/FGV;->a:LX/0ad;

    .line 2219058
    return-void
.end method

.method public static a(LX/0QB;)LX/FGV;
    .locals 4

    .prologue
    .line 2219043
    sget-object v0, LX/FGV;->b:LX/FGV;

    if-nez v0, :cond_1

    .line 2219044
    const-class v1, LX/FGV;

    monitor-enter v1

    .line 2219045
    :try_start_0
    sget-object v0, LX/FGV;->b:LX/FGV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2219046
    if-eqz v2, :cond_0

    .line 2219047
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2219048
    new-instance p0, LX/FGV;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/FGV;-><init>(LX/0ad;)V

    .line 2219049
    move-object v0, p0

    .line 2219050
    sput-object v0, LX/FGV;->b:LX/FGV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2219051
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2219052
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2219053
    :cond_1
    sget-object v0, LX/FGV;->b:LX/FGV;

    return-object v0

    .line 2219054
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2219055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/FGT;LX/0Pz;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FGT;",
            "LX/0Pz",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2219033
    invoke-static {p0}, LX/FGV;->a(LX/FGV;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/FGT;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2219034
    if-eqz v0, :cond_1

    .line 2219035
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string p0, "X-Rupload-Edge-Routing"

    const-string p1, "LATENCY"

    invoke-direct {v0, p0, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v0

    .line 2219036
    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219037
    :cond_0
    :goto_1
    return-void

    .line 2219038
    :cond_1
    invoke-static {p0}, LX/FGV;->a(LX/FGV;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/FGT;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2219039
    :cond_2
    const/4 v0, 0x0

    .line 2219040
    :goto_2
    move-object v0, v0

    .line 2219041
    if-eqz v0, :cond_0

    .line 2219042
    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "X-Rupload-Edge-Routing-Dc"

    iget-object v2, p1, LX/FGT;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static a(LX/FGV;)Z
    .locals 3

    .prologue
    .line 2219002
    iget-object v0, p0, LX/FGV;->a:LX/0ad;

    sget-short v1, LX/FGS;->o:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2219007
    check-cast p1, LX/FGT;

    .line 2219008
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2219009
    iget-object v1, p1, LX/FGT;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2219010
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "X-Entity-Digest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, LX/FGT;->e:LX/7yr;

    invoke-virtual {v4}, LX/7yr;->getHeaderPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, LX/FGT;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219011
    :cond_0
    invoke-direct {p0, p1, v0}, LX/FGV;->a(LX/FGT;LX/0Pz;)V

    .line 2219012
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "get_resumable_upload_session"

    .line 2219013
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2219014
    move-object v1, v1

    .line 2219015
    const-string v2, "GET"

    .line 2219016
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2219017
    move-object v1, v1

    .line 2219018
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, LX/FGT;->b:LX/FHg;

    .line 2219019
    iget-object v4, v3, LX/FHg;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2219020
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, LX/FGT;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2219021
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2219022
    move-object v1, v1

    .line 2219023
    const/4 v2, 0x1

    .line 2219024
    iput-boolean v2, v1, LX/14O;->o:Z

    .line 2219025
    move-object v1, v1

    .line 2219026
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2219027
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2219028
    move-object v0, v1

    .line 2219029
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2219030
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2219031
    move-object v0, v0

    .line 2219032
    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v0, v1}, LX/14O;->a(LX/14P;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2219003
    const/4 v4, 0x0

    .line 2219004
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2219005
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2219006
    new-instance v1, LX/FGU;

    const-string v2, "offset"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2, v4}, LX/16N;->a(LX/0lF;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "duplicate"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3, v4}, LX/16N;->a(LX/0lF;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "dc"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v4, ""

    invoke-static {v0, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/FGU;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;)V

    return-object v1
.end method
