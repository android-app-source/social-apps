.class public final LX/FWG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FWF;


# instance fields
.field public final synthetic a:Lcom/facebook/saved/fragment/SavedItemsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/saved/fragment/SavedItemsListFragment;)V
    .locals 0

    .prologue
    .line 2251797
    iput-object p1, p0, LX/FWG;->a:Lcom/facebook/saved/fragment/SavedItemsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/FVt;)V
    .locals 1

    .prologue
    .line 2251798
    iget-object v0, p0, LX/FWG;->a:Lcom/facebook/saved/fragment/SavedItemsListFragment;

    iget-object v0, v0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->d:LX/FVL;

    invoke-virtual {v0, p1}, LX/FVL;->a(LX/FVt;)V

    .line 2251799
    return-void
.end method

.method public final a(LX/FVt;LX/8bb;)V
    .locals 3

    .prologue
    .line 2251800
    iget-object v0, p0, LX/FWG;->a:Lcom/facebook/saved/fragment/SavedItemsListFragment;

    iget-object v0, v0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->d:LX/FVL;

    .line 2251801
    iget-object v1, v0, LX/FVL;->i:LX/16H;

    .line 2251802
    iget-object v2, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2251803
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v2

    .line 2251804
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "saved_dashboard_undo_button_imp"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "saved_dashboard"

    .line 2251805
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2251806
    move-object p0, p0

    .line 2251807
    const-string v0, "object_id"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v0, "undo_action"

    invoke-virtual {p0, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v0, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2251808
    iget-object v0, v1, LX/16H;->a:LX/0Zb;

    invoke-interface {v0, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2251809
    return-void
.end method

.method public final a(LX/FVt;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2251810
    iget-object v0, p0, LX/FWG;->a:Lcom/facebook/saved/fragment/SavedItemsListFragment;

    iget-object v0, v0, Lcom/facebook/saved/fragment/SavedItemsListFragment;->d:LX/FVL;

    iget-object v1, p0, LX/FWG;->a:Lcom/facebook/saved/fragment/SavedItemsListFragment;

    const-string v2, "more_button"

    invoke-virtual {v0, p1, p2, v1, v2}, LX/FVL;->a(LX/FVt;Landroid/view/View;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 2251811
    return-void
.end method
