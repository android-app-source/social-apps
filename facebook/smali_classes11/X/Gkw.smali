.class public final LX/Gkw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;",
        ">;>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gl6;


# direct methods
.method public constructor <init>(LX/Gl6;)V
    .locals 0

    .prologue
    .line 2389712
    iput-object p1, p0, LX/Gkw;->a:LX/Gl6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2389713
    new-instance v1, LX/0v6;

    const-string v0, "GroupsSection"

    invoke-direct {v1, v0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2389714
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2389715
    iget-object v0, p0, LX/Gkw;->a:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389716
    instance-of v4, v0, LX/Gkp;

    if-eqz v4, :cond_0

    .line 2389717
    check-cast v0, LX/Gkp;

    .line 2389718
    iget-object v4, p0, LX/Gkw;->a:LX/Gl6;

    iget v4, v4, LX/Gl6;->e:I

    invoke-virtual {v0, v4}, LX/Gkp;->b(I)LX/GlC;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v4, 0x1

    .line 2389719
    iput-boolean v4, v0, LX/0zO;->p:Z

    .line 2389720
    move-object v0, v0

    .line 2389721
    const-wide/16 v4, 0x708

    invoke-virtual {v0, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2389722
    invoke-virtual {v1, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2389723
    :cond_1
    iget-object v0, p0, LX/Gkw;->a:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->l:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0v6;)V

    .line 2389724
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
