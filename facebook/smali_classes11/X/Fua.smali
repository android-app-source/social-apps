.class public final LX/Fua;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fue;


# direct methods
.method public constructor <init>(LX/Fue;)V
    .locals 0

    .prologue
    .line 2300143
    iput-object p1, p0, LX/Fua;->a:LX/Fue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7711aedd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2300144
    iget-object v0, p0, LX/Fua;->a:LX/Fue;

    iget-object v0, v0, LX/Fue;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    iget-object v2, p0, LX/Fua;->a:LX/Fue;

    iget-object v2, v2, LX/Fue;->o:LX/5SB;

    .line 2300145
    iget-wide v5, v2, LX/5SB;->b:J

    move-wide v2, v5

    .line 2300146
    const/4 v9, 0x0

    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "context_item_edit_click"

    move-object v6, v0

    move-wide v7, v2

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2300147
    if-eqz v5, :cond_0

    .line 2300148
    iget-object v6, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300149
    :cond_0
    iget-object v0, p0, LX/Fua;->a:LX/Fue;

    invoke-static {v0}, LX/Fue;->d(LX/Fue;)V

    .line 2300150
    const v0, -0x4219427e

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
