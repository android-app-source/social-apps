.class public final LX/GH8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGh;


# instance fields
.field public final synthetic a:LX/GHB;


# direct methods
.method public constructor <init>(LX/GHB;)V
    .locals 0

    .prologue
    .line 2334740
    iput-object p1, p0, LX/GH8;->a:LX/GHB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/Context;)V
    .locals 14

    .prologue
    .line 2334741
    iget-object v0, p0, LX/GH8;->a:LX/GHB;

    iget-object v0, v0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334742
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2334743
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2334744
    if-nez v0, :cond_0

    .line 2334745
    iget-object v0, p0, LX/GH8;->a:LX/GHB;

    .line 2334746
    iget-object v1, v0, LX/GHB;->l:LX/GG3;

    iget-object v2, v0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v9, 0x0

    .line 2334747
    const-string v7, "exit_flow"

    const-string v8, "creative_edit"

    const-string v11, "promote_dialog"

    const/4 v13, 0x1

    move-object v5, v1

    move-object v6, v2

    move-object v10, v9

    move-object v12, v9

    invoke-static/range {v5 .. v13}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2334748
    iget-object v1, v0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2334749
    iget-object v1, v0, LX/GHB;->k:LX/GDm;

    iget-object v2, v0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1, v2, p1}, LX/GDm;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    .line 2334750
    :goto_0
    return-void

    .line 2334751
    :cond_0
    iget-object v0, p0, LX/GH8;->a:LX/GHB;

    .line 2334752
    new-instance v3, LX/4BY;

    invoke-direct {v3, p1}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 2334753
    const v1, 0x7f080bb7

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2334754
    invoke-virtual {v3}, LX/4BY;->show()V

    .line 2334755
    iget-object v1, v0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334756
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v4, v2

    .line 2334757
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2334758
    const/4 v2, 0x5

    iget-object v5, v0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v5

    invoke-virtual {v1, v2, v5}, Ljava/util/Calendar;->add(II)V

    .line 2334759
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    const-wide/16 v5, 0x3e8

    div-long/2addr v1, v5

    long-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 2334760
    iget-object v1, v0, LX/GHB;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const-string v6, "TASK_KEY_CREATE_OFFER"

    iget-object v2, v0, LX/GHB;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GEL;

    .line 2334761
    iget-object v7, v4, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v7, v7

    .line 2334762
    iget-object v8, v4, Lcom/facebook/adinterfaces/model/CreativeAdModel;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2334763
    iget-object v9, v4, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    move-object v4, v9

    .line 2334764
    invoke-static {v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334765
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334766
    new-instance v9, LX/4Gs;

    invoke-direct {v9}, LX/4Gs;-><init>()V

    .line 2334767
    const-string v10, "BOGO"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2334768
    const-string v10, "BOGO"

    .line 2334769
    :goto_1
    move-object v10, v10

    .line 2334770
    const-string v11, "discount_type"

    invoke-virtual {v9, v11, v10}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334771
    move-object v9, v9

    .line 2334772
    const-string v10, "expiration_time"

    invoke-virtual {v9, v10, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2334773
    move-object v9, v9

    .line 2334774
    const-string v10, "OFFLINE"

    .line 2334775
    const-string v11, "location_type"

    invoke-virtual {v9, v11, v10}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334776
    move-object v9, v9

    .line 2334777
    const-string v10, "page_id"

    invoke-virtual {v9, v10, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334778
    move-object v9, v9

    .line 2334779
    const-string v10, "title"

    invoke-virtual {v9, v10, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334780
    move-object v9, v9

    .line 2334781
    new-instance v10, LX/AC5;

    invoke-direct {v10}, LX/AC5;-><init>()V

    move-object v10, v10

    .line 2334782
    const-string v11, "input"

    invoke-virtual {v10, v11, v9}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2334783
    invoke-static {v10}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v9

    .line 2334784
    iget-object v10, v2, LX/GEL;->a:LX/0tX;

    invoke-virtual {v10, v9}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    move-object v2, v9

    .line 2334785
    new-instance v4, LX/GH9;

    invoke-direct {v4, v0, p1, v3}, LX/GH9;-><init>(LX/GHB;Landroid/content/Context;LX/4BY;)V

    invoke-virtual {v1, v6, v2, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2334786
    goto/16 :goto_0

    .line 2334787
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2334788
    const-string v2, "CREATIVE_EDIT_DATA"

    iget-object v3, v0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2334789
    iget-object v2, v0, LX/GHB;->i:LX/GF4;

    new-instance v3, LX/GFS;

    const/4 v4, 0x1

    invoke-direct {v3, v1, v4}, LX/GFS;-><init>(Landroid/content/Intent;Z)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0

    .line 2334790
    :cond_2
    const-string v10, "PERCENTAGE_OFF"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2334791
    const-string v10, "PERCENTAGE_OFF"

    goto :goto_1

    .line 2334792
    :cond_3
    const-string v10, "CASH_DISCOUNT"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2334793
    const-string v10, "CASH_DISCOUNT"

    goto :goto_1

    .line 2334794
    :cond_4
    const-string v10, "FREE_STUFF"

    goto :goto_1
.end method
