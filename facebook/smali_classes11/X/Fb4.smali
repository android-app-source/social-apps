.class public LX/Fb4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FaB;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/13B;

.field public final c:LX/03V;

.field private final d:LX/CwT;

.field public final e:Landroid/view/View;

.field private f:LX/FaY;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;LX/CwT;LX/FaY;LX/13B;LX/03V;)V
    .locals 0
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/CwT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2259524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259525
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259526
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259527
    iput-object p1, p0, LX/Fb4;->a:Landroid/content/Context;

    .line 2259528
    iput-object p4, p0, LX/Fb4;->f:LX/FaY;

    .line 2259529
    iput-object p5, p0, LX/Fb4;->b:LX/13B;

    .line 2259530
    iput-object p3, p0, LX/Fb4;->d:LX/CwT;

    .line 2259531
    iput-object p2, p0, LX/Fb4;->e:Landroid/view/View;

    .line 2259532
    iput-object p6, p0, LX/Fb4;->c:LX/03V;

    .line 2259533
    return-void
.end method


# virtual methods
.method public final a(LX/0P1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2259534
    iget-object v0, p0, LX/Fb4;->d:LX/CwT;

    .line 2259535
    iget-object v1, v0, LX/CwT;->a:LX/A0Z;

    move-object v0, v1

    .line 2259536
    invoke-interface {v0}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Fb4;->d:LX/CwT;

    .line 2259537
    iget-object v2, v1, LX/CwT;->a:LX/A0Z;

    move-object v1, v2

    .line 2259538
    invoke-interface {v1}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v1

    .line 2259539
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2259540
    iget-object v2, p0, LX/Fb4;->c:LX/03V;

    const-string v3, "SearchAwareness"

    const-string v4, "title and description are empty for tooltip."

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2259541
    const/4 v2, 0x0

    .line 2259542
    :goto_0
    move-object v0, v2

    .line 2259543
    if-eqz v0, :cond_0

    .line 2259544
    new-instance v1, LX/Fb3;

    invoke-direct {v1, p0}, LX/Fb3;-><init>(LX/Fb4;)V

    .line 2259545
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 2259546
    :cond_0
    iget-object v0, p0, LX/Fb4;->b:LX/13B;

    iget-object v1, p0, LX/Fb4;->d:LX/CwT;

    invoke-virtual {v0, v1, p1}, LX/13B;->a(LX/CwT;LX/0P1;)V

    .line 2259547
    return-void

    .line 2259548
    :cond_1
    new-instance v2, LX/0hs;

    iget-object v3, p0, LX/Fb4;->a:Landroid/content/Context;

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2259549
    invoke-virtual {v2, v0}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2259550
    invoke-virtual {v2, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2259551
    sget-object v3, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v2, v3}, LX/0ht;->a(LX/3AV;)V

    .line 2259552
    const/4 v3, -0x1

    .line 2259553
    iput v3, v2, LX/0hs;->t:I

    .line 2259554
    iget-object v3, p0, LX/Fb4;->e:Landroid/view/View;

    invoke-virtual {v2, v3}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2259555
    iget-object v0, p0, LX/Fb4;->f:LX/FaY;

    sget-object v1, LX/FaF;->LEARNING_NUX:LX/FaF;

    invoke-virtual {v0, v1}, LX/FaY;->a(LX/FaF;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fb4;->d:LX/CwT;

    .line 2259556
    iget-boolean v1, v0, LX/CwT;->e:Z

    move v0, v1

    .line 2259557
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2259558
    iget-object v0, p0, LX/Fb4;->d:LX/CwT;

    const/4 v1, 0x0

    .line 2259559
    iput-boolean v1, v0, LX/CwT;->e:Z

    .line 2259560
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2259561
    iget-object v0, p0, LX/Fb4;->b:LX/13B;

    iget-object v1, p0, LX/Fb4;->d:LX/CwT;

    invoke-virtual {v0, v1}, LX/13B;->a(LX/CwT;)V

    .line 2259562
    return-void
.end method
