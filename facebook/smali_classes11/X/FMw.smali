.class public LX/FMw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:Landroid/net/Uri;

.field private static final f:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile p:LX/FMw;


# instance fields
.field private final g:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/content/Context;

.field private final j:LX/0SG;

.field private final k:LX/2J0;

.field private final l:LX/0W3;

.field private final m:LX/FMG;

.field private n:Z

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2231633
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    sput-object v0, LX/FMw;->a:[Ljava/lang/String;

    .line 2231634
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    const-string v1, "sub_id"

    aput-object v1, v0, v6

    sput-object v0, LX/FMw;->b:[Ljava/lang/String;

    .line 2231635
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const-string v1, "m_size"

    aput-object v1, v0, v5

    sput-object v0, LX/FMw;->c:[Ljava/lang/String;

    .line 2231636
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const-string v1, "m_size"

    aput-object v1, v0, v5

    const-string v1, "sub_id"

    aput-object v1, v0, v6

    sput-object v0, LX/FMw;->d:[Ljava/lang/String;

    .line 2231637
    const-string v0, "content://sms/queued"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/FMw;->e:Landroid/net/Uri;

    .line 2231638
    new-instance v0, LX/FMv;

    invoke-direct {v0}, LX/FMv;-><init>()V

    sput-object v0, LX/FMw;->f:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/2J0;LX/0W3;LX/FMG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231624
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/FMw;->g:LX/0tf;

    .line 2231625
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    .line 2231626
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FMw;->n:Z

    .line 2231627
    iput-object p1, p0, LX/FMw;->i:Landroid/content/Context;

    .line 2231628
    iput-object p2, p0, LX/FMw;->j:LX/0SG;

    .line 2231629
    iput-object p3, p0, LX/FMw;->k:LX/2J0;

    .line 2231630
    iput-object p4, p0, LX/FMw;->l:LX/0W3;

    .line 2231631
    iput-object p5, p0, LX/FMw;->m:LX/FMG;

    .line 2231632
    return-void
.end method

.method public static a(LX/0QB;)LX/FMw;
    .locals 9

    .prologue
    .line 2231497
    sget-object v0, LX/FMw;->p:LX/FMw;

    if-nez v0, :cond_1

    .line 2231498
    const-class v1, LX/FMw;

    monitor-enter v1

    .line 2231499
    :try_start_0
    sget-object v0, LX/FMw;->p:LX/FMw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2231500
    if-eqz v2, :cond_0

    .line 2231501
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2231502
    new-instance v3, LX/FMw;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v6, LX/2J0;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2J0;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/FMG;->b(LX/0QB;)LX/FMG;

    move-result-object v8

    check-cast v8, LX/FMG;

    invoke-direct/range {v3 .. v8}, LX/FMw;-><init>(Landroid/content/Context;LX/0SG;LX/2J0;LX/0W3;LX/FMG;)V

    .line 2231503
    move-object v0, v3

    .line 2231504
    sput-object v0, LX/FMw;->p:LX/FMw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231505
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2231506
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2231507
    :cond_1
    sget-object v0, LX/FMw;->p:LX/FMw;

    return-object v0

    .line 2231508
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2231509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(LX/FMw;)V
    .locals 5

    .prologue
    .line 2231620
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.messaging.sms.REQUEST_SEND_MESSAGE"

    const/4 v2, 0x0

    iget-object v3, p0, LX/FMw;->i:Landroid/content/Context;

    const-class v4, LX/FMm;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2231621
    iget-object v1, p0, LX/FMw;->i:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2231622
    return-void
.end method

.method private static d(LX/FMw;)V
    .locals 8

    .prologue
    .line 2231609
    invoke-virtual {p0}, LX/FMw;->a()V

    .line 2231610
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FMw;->n:Z

    .line 2231611
    invoke-static {p0}, LX/FMw;->e(LX/FMw;)V

    .line 2231612
    invoke-static {p0}, LX/FMw;->f(LX/FMw;)V

    .line 2231613
    iget-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    check-cast v0, Ljava/util/LinkedList;

    sget-object v1, LX/FMw;->f:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2231614
    iget-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    .line 2231615
    iget-object v2, p0, LX/FMw;->g:LX/0tf;

    .line 2231616
    iget-wide v6, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    move-wide v4, v6

    .line 2231617
    iget-object v3, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2231618
    invoke-virtual {v2, v4, v5, v0}, LX/0tf;->b(JLjava/lang/Object;)V

    goto :goto_0

    .line 2231619
    :cond_0
    return-void
.end method

.method private static e(LX/FMw;)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x0

    .line 2231586
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "date>="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/FMw;->j:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2231587
    :try_start_0
    iget-object v0, p0, LX/FMw;->m:LX/FMG;

    sget-object v1, LX/FMw;->e:Landroid/net/Uri;

    sget-object v2, LX/FMw;->a:[Ljava/lang/String;

    sget-object v3, LX/FMw;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "date ASC"

    invoke-virtual/range {v0 .. v6}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2231588
    if-eqz v1, :cond_2

    .line 2231589
    :try_start_1
    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    .line 2231590
    :goto_0
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2231591
    const-string v0, "_id"

    invoke-static {v9, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 2231592
    const-string v0, "thread_id"

    invoke-static {v9, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 2231593
    const-string v0, "date"

    invoke-static {v9, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2231594
    const-string v0, "body"

    invoke-static {v9, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2231595
    iget-object v0, p0, LX/FMw;->m:LX/FMG;

    invoke-virtual {v0, v9}, LX/FMG;->a(Landroid/database/Cursor;)I

    move-result v8

    .line 2231596
    iget-object v12, p0, LX/FMw;->h:Ljava/util/Queue;

    new-instance v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    invoke-static {v6, v7}, LX/FMQ;->a(J)Ljava/lang/String;

    move-result-object v1

    if-nez v11, :cond_1

    move v6, v10

    :goto_1
    const/4 v7, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;-><init>(Ljava/lang/String;JJIII)V

    invoke-interface {v12, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 2231597
    :catch_0
    move-exception v0

    move-object v1, v9

    .line 2231598
    :goto_2
    :try_start_3
    const-string v2, "MmsSmsPendingSendQueue"

    const-string v3, "Exception in constructing queue for sms pending messages."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2231599
    if-eqz v1, :cond_0

    .line 2231600
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2231601
    :cond_0
    :goto_3
    return-void

    .line 2231602
    :cond_1
    :try_start_4
    invoke-virtual {v11}, Ljava/lang/String;->length()I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v6

    goto :goto_1

    :cond_2
    move-object v9, v1

    .line 2231603
    :cond_3
    if-eqz v9, :cond_0

    .line 2231604
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 2231605
    :catchall_0
    move-exception v0

    move-object v9, v7

    :goto_4
    if-eqz v9, :cond_4

    .line 2231606
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 2231607
    :catchall_1
    move-exception v0

    move-object v9, v1

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v9, v1

    goto :goto_4

    .line 2231608
    :catch_1
    move-exception v0

    move-object v1, v7

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method private static f(LX/FMw;)V
    .locals 13

    .prologue
    .line 2231564
    const/4 v7, 0x0

    .line 2231565
    :try_start_0
    iget-object v0, p0, LX/FMw;->m:LX/FMG;

    sget-object v1, LX/551;->a:Landroid/net/Uri;

    sget-object v2, LX/FMw;->c:[Ljava/lang/String;

    sget-object v3, LX/FMw;->d:[Ljava/lang/String;

    const-string v4, "(st IS NULL OR st!=135) AND date>=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v8, p0, LX/FMw;->j:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    const-wide/32 v10, 0x5265c00

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const-string v6, "date ASC"

    invoke-virtual/range {v0 .. v6}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2231566
    if-eqz v1, :cond_1

    .line 2231567
    :try_start_1
    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    .line 2231568
    :goto_0
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2231569
    const-string v0, "_id"

    invoke-static {v9, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    .line 2231570
    const-string v0, "thread_id"

    invoke-static {v9, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 2231571
    const-string v0, "date"

    invoke-static {v9, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v0

    .line 2231572
    const-string v0, "m_size"

    invoke-static {v9, v0}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v6

    .line 2231573
    iget-object v0, p0, LX/FMw;->m:LX/FMG;

    invoke-virtual {v0, v9}, LX/FMG;->a(Landroid/database/Cursor;)I

    move-result v8

    .line 2231574
    iget-object v12, p0, LX/FMw;->h:Ljava/util/Queue;

    new-instance v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    invoke-static {v10, v11}, LX/FMQ;->b(J)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;-><init>(Ljava/lang/String;JJIII)V

    invoke-interface {v12, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 2231575
    :catch_0
    move-exception v0

    move-object v1, v9

    .line 2231576
    :goto_1
    :try_start_3
    const-string v2, "MmsSmsPendingSendQueue"

    const-string v3, "Exception in constructing queue for mms pending messages."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2231577
    if-eqz v1, :cond_0

    .line 2231578
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2231579
    :cond_0
    :goto_2
    return-void

    :cond_1
    move-object v9, v1

    .line 2231580
    :cond_2
    if-eqz v9, :cond_0

    .line 2231581
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 2231582
    :catchall_0
    move-exception v0

    move-object v9, v7

    :goto_3
    if-eqz v9, :cond_3

    .line 2231583
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2231584
    :catchall_1
    move-exception v0

    move-object v9, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v9, v1

    goto :goto_3

    .line 2231585
    :catch_1
    move-exception v0

    move-object v1, v7

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 2231558
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2231559
    iget-object v0, p0, LX/FMw;->g:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->b()V

    .line 2231560
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FMw;->n:Z

    .line 2231561
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FMw;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2231562
    monitor-exit p0

    return-void

    .line 2231563
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 3

    .prologue
    .line 2231544
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/FMw;->o:Z

    .line 2231545
    iget-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    .line 2231546
    if-eqz v0, :cond_0

    .line 2231547
    iget-object v1, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2231548
    iget-object v2, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2231549
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2231550
    :cond_0
    invoke-virtual {p0}, LX/FMw;->a()V

    .line 2231551
    :goto_0
    invoke-static {p0}, LX/FMw;->c(LX/FMw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2231552
    monitor-exit p0

    return-void

    .line 2231553
    :cond_1
    :try_start_1
    iget v1, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    .line 2231554
    iget-object v1, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    move-object v1, v1

    .line 2231555
    iput-object v1, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231556
    goto :goto_0

    .line 2231557
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 2231539
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/FMw;->n:Z

    .line 2231540
    iget-object v0, p0, LX/FMw;->g:LX/0tf;

    invoke-virtual {v0, p2, p3, p1}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 2231541
    invoke-static {p0}, LX/FMw;->c(LX/FMw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2231542
    monitor-exit p0

    return-void

    .line 2231543
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2231524
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/FMw;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2231525
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2231526
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    .line 2231527
    if-nez v0, :cond_2

    iget-boolean v2, p0, LX/FMw;->n:Z

    if-eqz v2, :cond_2

    .line 2231528
    invoke-static {p0}, LX/FMw;->d(LX/FMw;)V

    .line 2231529
    iget-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    .line 2231530
    :cond_2
    if-eqz v0, :cond_3

    .line 2231531
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/FMw;->o:Z

    .line 2231532
    iget-object v1, p0, LX/FMw;->j:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2231533
    iget-wide v8, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->c:J

    move-wide v4, v8

    .line 2231534
    sub-long/2addr v2, v4

    iget-object v1, p0, LX/FMw;->l:LX/0W3;

    sget-wide v4, LX/0X5;->hr:J

    const-wide/32 v6, 0x36ee80

    invoke-interface {v1, v4, v5, v6, v7}, LX/0W4;->a(JJ)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 2231535
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231536
    goto :goto_0

    .line 2231537
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v0, v1

    .line 2231538
    goto :goto_0
.end method

.method public final declared-synchronized b(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 2231510
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/FMw;->o:Z

    .line 2231511
    iget-object v0, p0, LX/FMw;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    .line 2231512
    if-eqz v0, :cond_0

    .line 2231513
    iget-object v1, v0, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2231514
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2231515
    :cond_0
    invoke-virtual {p0}, LX/FMw;->a()V

    .line 2231516
    :cond_1
    :goto_0
    invoke-static {p0}, LX/FMw;->c(LX/FMw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2231517
    monitor-exit p0

    return-void

    .line 2231518
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/FMw;->g:LX/0tf;

    invoke-virtual {v0, p2, p3}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2231519
    if-eqz v0, :cond_1

    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2231520
    iget-object v0, p0, LX/FMw;->g:LX/0tf;

    .line 2231521
    invoke-virtual {v0, p2, p3}, LX/0tf;->b(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231522
    goto :goto_0

    .line 2231523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
