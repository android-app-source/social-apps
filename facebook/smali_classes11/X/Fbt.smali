.class public LX/Fbt;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261290
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261291
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbt;
    .locals 3

    .prologue
    .line 2261292
    const-class v1, LX/Fbt;

    monitor-enter v1

    .line 2261293
    :try_start_0
    sget-object v0, LX/Fbt;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261294
    sput-object v2, LX/Fbt;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261295
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261296
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261297
    new-instance v0, LX/Fbt;

    invoke-direct {v0}, LX/Fbt;-><init>()V

    .line 2261298
    move-object v0, v0

    .line 2261299
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261300
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261301
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261303
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261304
    if-nez v0, :cond_0

    .line 2261305
    const/4 v0, 0x0

    .line 2261306
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    new-instance v2, LX/8dX;

    invoke-direct {v2}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lA()Ljava/lang/String;

    move-result-object v3

    .line 2261307
    iput-object v3, v2, LX/8dX;->D:Ljava/lang/String;

    .line 2261308
    move-object v2, v2

    .line 2261309
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lB()Ljava/lang/String;

    move-result-object v3

    .line 2261310
    iput-object v3, v2, LX/8dX;->K:Ljava/lang/String;

    .line 2261311
    move-object v2, v2

    .line 2261312
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    .line 2261313
    iput-object v3, v2, LX/8dX;->L:Ljava/lang/String;

    .line 2261314
    move-object v2, v2

    .line 2261315
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, 0x28d5023a

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2261316
    iput-object v3, v2, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261317
    move-object v2, v2

    .line 2261318
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v3

    .line 2261319
    iput-object v3, v2, LX/8dX;->ad:Ljava/lang/String;

    .line 2261320
    move-object v2, v2

    .line 2261321
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lC()Ljava/lang/String;

    move-result-object v3

    .line 2261322
    iput-object v3, v2, LX/8dX;->ak:Ljava/lang/String;

    .line 2261323
    move-object v2, v2

    .line 2261324
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lD()Ljava/lang/String;

    move-result-object v0

    .line 2261325
    iput-object v0, v2, LX/8dX;->am:Ljava/lang/String;

    .line 2261326
    move-object v0, v2

    .line 2261327
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2261328
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2261329
    move-object v0, v1

    .line 2261330
    goto :goto_0
.end method
