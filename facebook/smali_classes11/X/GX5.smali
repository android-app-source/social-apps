.class public final LX/GX5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GX0;

.field public final synthetic b:LX/GXD;


# direct methods
.method public constructor <init>(LX/GXD;LX/GX0;)V
    .locals 0

    .prologue
    .line 2363009
    iput-object p1, p0, LX/GX5;->b:LX/GXD;

    iput-object p2, p0, LX/GX5;->a:LX/GX0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x38eb3d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2363010
    iget-object v1, p0, LX/GX5;->b:LX/GXD;

    iget-object v2, p0, LX/GX5;->a:LX/GX0;

    .line 2363011
    iget-object v4, v1, LX/GXD;->f:LX/GXH;

    new-instance v5, LX/GX8;

    invoke-direct {v5, v1, v2}, LX/GX8;-><init>(LX/GXD;LX/GX0;)V

    .line 2363012
    iget-object v6, v2, LX/GX0;->f:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    move-object v7, v6

    .line 2363013
    if-nez v7, :cond_0

    .line 2363014
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    .line 2363015
    :goto_0
    move-object v4, v6

    .line 2363016
    invoke-virtual {v1, v4}, LX/GXD;->a(LX/0am;)V

    .line 2363017
    const v1, 0x30cbb996

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2363018
    :cond_0
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j()Ljava/lang/String;

    move-result-object v8

    .line 2363019
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mA_()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x1

    .line 2363020
    :goto_1
    invoke-static {v7, v6}, LX/GXN;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;Z)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    move-result-object v7

    .line 2363021
    invoke-static {v7}, LX/GXH;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;)LX/0am;

    move-result-object v7

    .line 2363022
    iget-object v9, v4, LX/GXH;->b:LX/GW4;

    .line 2363023
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    const/4 p0, 0x1

    :goto_2
    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 2363024
    iget-object p0, v9, LX/GW4;->b:LX/0SI;

    invoke-interface {p0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p0

    if-eqz p0, :cond_3

    iget-object p0, v9, LX/GW4;->b:LX/0SI;

    invoke-interface {p0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p0

    .line 2363025
    iget-object p1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2363026
    :goto_3
    if-eqz v6, :cond_4

    .line 2363027
    invoke-static {}, LX/5Bo;->a()LX/5Bk;

    move-result-object p1

    .line 2363028
    new-instance v2, LX/4En;

    invoke-direct {v2}, LX/4En;-><init>()V

    invoke-virtual {v2, v8}, LX/4En;->b(Ljava/lang/String;)LX/4En;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/4En;->a(Ljava/lang/String;)LX/4En;

    move-result-object p0

    .line 2363029
    :goto_4
    const-string v2, "input"

    invoke-virtual {p1, v2, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2363030
    iget-object p0, v9, LX/GW4;->c:LX/0tX;

    new-instance v2, LX/399;

    invoke-direct {v2, p1}, LX/399;-><init>(LX/0zP;)V

    invoke-virtual {p0, v2}, LX/0tX;->b(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v6, p0

    .line 2363031
    new-instance v8, LX/GXF;

    invoke-direct {v8, v4, v5}, LX/GXF;-><init>(LX/GXH;Ljava/util/concurrent/Callable;)V

    iget-object v9, v4, LX/GXH;->c:LX/0TD;

    invoke-static {v6, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move-object v6, v7

    .line 2363032
    goto :goto_0

    .line 2363033
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 2363034
    :cond_2
    const/4 p0, 0x0

    goto :goto_2

    .line 2363035
    :cond_3
    iget-object p0, v9, LX/GW4;->b:LX/0SI;

    invoke-interface {p0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p0

    .line 2363036
    iget-object p1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2363037
    goto :goto_3

    .line 2363038
    :cond_4
    invoke-static {}, LX/5Bo;->b()LX/5Bl;

    move-result-object p1

    .line 2363039
    new-instance v2, LX/4Es;

    invoke-direct {v2}, LX/4Es;-><init>()V

    invoke-virtual {v2, v8}, LX/4Es;->b(Ljava/lang/String;)LX/4Es;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/4Es;->a(Ljava/lang/String;)LX/4Es;

    move-result-object p0

    goto :goto_4
.end method
