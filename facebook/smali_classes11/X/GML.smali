.class public final LX/GML;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GMM;


# direct methods
.method public constructor <init>(LX/GMM;)V
    .locals 0

    .prologue
    .line 2344368
    iput-object p1, p0, LX/GML;->a:LX/GMM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x69819828

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2344369
    iget-object v1, p0, LX/GML;->a:LX/GMM;

    .line 2344370
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2344371
    iget-object v2, v1, LX/GCE;->f:LX/GG3;

    move-object v1, v2

    .line 2344372
    iget-object v2, p0, LX/GML;->a:LX/GMM;

    iget-object v2, v2, LX/GMM;->j:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const/4 v12, 0x0

    .line 2344373
    const-string v10, "submit_flow_click"

    const-string v11, "insights"

    move-object v8, v1

    move-object v9, v2

    move-object v13, v12

    invoke-static/range {v8 .. v13}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2344374
    iget-object v1, p0, LX/GML;->a:LX/GMM;

    iget-object v1, v1, LX/GMM;->j:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->s()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2344375
    iget-object v2, p0, LX/GML;->a:LX/GMM;

    iget-object v2, v2, LX/GMM;->j:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v3, p0, LX/GML;->a:LX/GMM;

    iget-object v3, v3, LX/GMM;->j:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v2

    .line 2344376
    iget-object v3, p0, LX/GML;->a:LX/GMM;

    .line 2344377
    iget-object v4, v3, LX/GHg;->b:LX/GCE;

    move-object v3, v4

    .line 2344378
    new-instance v4, LX/GFS;

    iget-object v5, p0, LX/GML;->a:LX/GMM;

    iget-object v5, v5, LX/GMM;->a:LX/GKy;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 2344379
    sget-object v8, LX/0ax;->o:Ljava/lang/String;

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    const/4 v10, 0x1

    aput-object v1, v9, v10

    invoke-static {v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2344380
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 2344381
    const-string v10, "campaignGroup"

    invoke-virtual {v9, v10, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344382
    const-string v10, "account"

    invoke-virtual {v9, v10, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344383
    iget-object v10, v5, LX/GKy;->c:LX/17Y;

    invoke-interface {v10, v6, v8}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v10, "init_props"

    invoke-virtual {v8, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    move-object v1, v8

    .line 2344384
    invoke-direct {v4, v1}, LX/GFS;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v3, v4}, LX/GCE;->a(LX/8wN;)V

    .line 2344385
    const v1, 0x45a4704d

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
