.class public LX/G4X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/G4X;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317801
    return-void
.end method

.method public static a(LX/0QB;)LX/G4X;
    .locals 3

    .prologue
    .line 2317802
    sget-object v0, LX/G4X;->a:LX/G4X;

    if-nez v0, :cond_1

    .line 2317803
    const-class v1, LX/G4X;

    monitor-enter v1

    .line 2317804
    :try_start_0
    sget-object v0, LX/G4X;->a:LX/G4X;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2317805
    if-eqz v2, :cond_0

    .line 2317806
    :try_start_1
    new-instance v0, LX/G4X;

    invoke-direct {v0}, LX/G4X;-><init>()V

    .line 2317807
    move-object v0, v0

    .line 2317808
    sput-object v0, LX/G4X;->a:LX/G4X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317809
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2317810
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2317811
    :cond_1
    sget-object v0, LX/G4X;->a:LX/G4X;

    return-object v0

    .line 2317812
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2317813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "LX/0TF",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2317814
    new-instance v0, LX/G4W;

    invoke-direct {v0, p0, p1, p2}, LX/G4W;-><init>(LX/G4X;Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method
