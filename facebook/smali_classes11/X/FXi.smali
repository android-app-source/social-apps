.class public final LX/FXi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AUN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AUN",
        "<",
        "LX/BO4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FXj;

.field public final synthetic b:LX/FXY;


# direct methods
.method public constructor <init>(LX/FXY;LX/FXj;)V
    .locals 0

    .prologue
    .line 2255317
    iput-object p1, p0, LX/FXi;->b:LX/FXY;

    iput-object p2, p0, LX/FXi;->a:LX/FXj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2255318
    iget-object v0, p0, LX/FXi;->a:LX/FXj;

    .line 2255319
    iget-object v1, v0, LX/FXj;->a:LX/FXV;

    move-object v0, v1

    .line 2255320
    iget-object v1, p0, LX/FXi;->a:LX/FXj;

    invoke-virtual {v0, v1}, LX/FXV;->e(LX/FXj;)V

    .line 2255321
    return-void
.end method

.method public final a(LX/AU0;)V
    .locals 4

    .prologue
    .line 2255322
    check-cast p1, LX/BO4;

    .line 2255323
    iget-object v0, p0, LX/FXi;->a:LX/FXj;

    iget-object v0, v0, LX/FXj;->d:LX/FWt;

    invoke-interface {p1}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 2255324
    iget v2, v0, LX/FWt;->b:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 2255325
    iput v1, v0, LX/FWt;->b:I

    .line 2255326
    :goto_0
    const-string v2, "DASH_ITEMS_LOAD_FROM_CACHE"

    invoke-static {v0, v2}, LX/FWt;->b(LX/FWt;Ljava/lang/String;)V

    .line 2255327
    iget-object v0, p0, LX/FXi;->a:LX/FXj;

    .line 2255328
    iget-object v1, v0, LX/FXj;->a:LX/FXV;

    move-object v0, v1

    .line 2255329
    iget-object v1, p0, LX/FXi;->a:LX/FXj;

    invoke-virtual {v0, v1, p1}, LX/FXV;->a(LX/FXj;LX/BO1;)V

    .line 2255330
    iget-object v0, p0, LX/FXi;->a:LX/FXj;

    iget-object v0, v0, LX/FXj;->b:LX/FXs;

    iget-object v1, p0, LX/FXi;->a:LX/FXj;

    iget-object v1, v1, LX/FXj;->f:Ljava/lang/String;

    const/4 v2, 0x0

    .line 2255331
    iget-object v3, v0, LX/FXs;->c:LX/01J;

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, v1, p1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-eq v3, p1, :cond_2

    .line 2255332
    invoke-static {v0, v1, v2}, LX/FXs;->d(LX/FXs;Ljava/lang/String;LX/FXS;)V

    .line 2255333
    const/4 v3, 0x1

    .line 2255334
    :goto_1
    move v0, v3

    .line 2255335
    if-eqz v0, :cond_0

    .line 2255336
    iget-object v0, p0, LX/FXi;->a:LX/FXj;

    iget-object v0, v0, LX/FXj;->d:LX/FWt;

    .line 2255337
    const-string v1, "DASH_ITEMS_LOAD_FROM_NETWORK"

    invoke-static {v0, v1}, LX/FWt;->a(LX/FWt;Ljava/lang/String;)V

    .line 2255338
    :cond_0
    return-void

    .line 2255339
    :cond_1
    iput v1, v0, LX/FWt;->c:I

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
