.class public final LX/Ffr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$FetchLiveFeedStoriesGraphQLModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/Integer;

.field public final synthetic c:LX/Ffv;


# direct methods
.method public constructor <init>(LX/Ffv;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 2269117
    iput-object p1, p0, LX/Ffr;->c:LX/Ffv;

    iput-object p2, p0, LX/Ffr;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Ffr;->b:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2269118
    iget-object v0, p0, LX/Ffr;->c:LX/Ffv;

    const-string v1, "android:live-feed:head"

    iget-object v2, p0, LX/Ffr;->c:LX/Ffv;

    iget-object v2, v2, LX/Ffv;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v3, p0, LX/Ffr;->a:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LX/Ffr;->b:Ljava/lang/Integer;

    invoke-static/range {v0 .. v5}, LX/Ffv;->a(LX/Ffv;Ljava/lang/String;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
