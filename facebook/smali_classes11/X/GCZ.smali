.class public final LX/GCZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GCY;


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

.field public final synthetic c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Landroid/os/Bundle;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V
    .locals 0

    .prologue
    .line 2328380
    iput-object p1, p0, LX/GCZ;->c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iput-object p2, p0, LX/GCZ;->a:Landroid/os/Bundle;

    iput-object p3, p0, LX/GCZ;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 3

    .prologue
    .line 2328381
    iget-object v0, p0, LX/GCZ;->c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v1, p0, LX/GCZ;->a:Landroid/os/Bundle;

    invoke-static {v0, p1, v1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->b(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/os/Bundle;)V

    .line 2328382
    iget-object v0, p0, LX/GCZ;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    if-eqz v0, :cond_0

    .line 2328383
    iget-object v0, p0, LX/GCZ;->c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q:LX/GF4;

    new-instance v1, LX/GFn;

    iget-object v2, p0, LX/GCZ;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v1, v2}, LX/GFn;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2328384
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2328385
    iget-object v0, p0, LX/GCZ;->c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->m(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2328386
    :goto_0
    return-void

    .line 2328387
    :cond_0
    iget-object v0, p0, LX/GCZ;->c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2328388
    iget-object v1, p0, LX/GCZ;->c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->A:LX/GCE;

    .line 2328389
    iget-object v2, v1, LX/GCE;->f:LX/GG3;

    move-object v1, v2

    .line 2328390
    const-string v2, "enter_flow_error"

    const/4 v8, 0x1

    .line 2328391
    if-nez p2, :cond_2

    .line 2328392
    :cond_1
    :goto_1
    iget-object v0, p0, LX/GCZ;->c:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->v(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    goto :goto_0

    .line 2328393
    :cond_2
    :try_start_0
    iget-object v3, v1, LX/GG3;->a:LX/0Zb;

    const/4 v4, 0x1

    invoke-interface {v3, v2, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2328394
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2328395
    invoke-virtual {v3, p2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2328396
    invoke-static {v3, p1}, LX/GG3;->a(LX/0oG;Ljava/lang/Throwable;)V

    .line 2328397
    const-string v4, "page_id"

    invoke-virtual {v3, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2328398
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2328399
    sget-boolean v3, LX/GG3;->k:Z

    if-eqz v3, :cond_1

    .line 2328400
    iget-object v3, v1, LX/GG3;->e:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EVENT:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2328401
    :catch_0
    move-exception v3

    .line 2328402
    iget-object v4, v1, LX/GG3;->c:LX/2U3;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to log event "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2328403
    sget-boolean v3, LX/GG3;->k:Z

    if-eqz v3, :cond_1

    .line 2328404
    iget-object v3, v1, LX/GG3;->e:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to log event "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
