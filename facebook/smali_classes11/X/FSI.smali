.class public LX/FSI;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/0rm;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2241736
    const-class v0, LX/FSI;

    sput-object v0, LX/FSI;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0rm;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsFlatBufferFromServerEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0rm;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241738
    iput-object p1, p0, LX/FSI;->b:LX/0tX;

    .line 2241739
    iput-object p2, p0, LX/FSI;->c:LX/0rm;

    .line 2241740
    iput-object p3, p0, LX/FSI;->d:LX/0Or;

    .line 2241741
    return-void
.end method

.method public static a(LX/0QB;)LX/FSI;
    .locals 4

    .prologue
    .line 2241742
    new-instance v2, LX/FSI;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0rm;->b(LX/0QB;)LX/0rm;

    move-result-object v1

    check-cast v1, LX/0rm;

    const/16 v3, 0x149a

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/FSI;-><init>(LX/0tX;LX/0rm;LX/0Or;)V

    .line 2241743
    move-object v0, v2

    .line 2241744
    return-object v0
.end method
