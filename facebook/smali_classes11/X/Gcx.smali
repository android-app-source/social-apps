.class public final LX/Gcx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/carousel/EventCardViewBinder;

.field public final synthetic b:I

.field public final synthetic c:LX/Gcz;


# direct methods
.method public constructor <init>(LX/Gcz;Lcom/facebook/events/carousel/EventCardViewBinder;I)V
    .locals 0

    .prologue
    .line 2372833
    iput-object p1, p0, LX/Gcx;->c:LX/Gcz;

    iput-object p2, p0, LX/Gcx;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iput p3, p0, LX/Gcx;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x73d5a00a

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2372834
    iget-object v1, p0, LX/Gcx;->c:LX/Gcz;

    iget-object v1, v1, LX/Gcz;->e:LX/1nQ;

    iget-object v2, p0, LX/Gcx;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372835
    iget-object v3, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object v2, v3

    .line 2372836
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2372837
    iget-object v3, p0, LX/Gcx;->c:LX/Gcz;

    .line 2372838
    sget-object v4, LX/Gcy;->a:[I

    iget-object v5, v3, LX/Gcz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2372839
    iget-object v3, v5, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v5, v3

    .line 2372840
    invoke-virtual {v5}, Lcom/facebook/events/common/ActionSource;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2372841
    const-string v4, "unknown"

    :goto_0
    move-object v3, v4

    .line 2372842
    iget v4, p0, LX/Gcx;->b:I

    iget-object v5, p0, LX/Gcx;->c:LX/Gcz;

    iget-object v5, v5, LX/Gcz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2372843
    iget-object p1, v5, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v5, p1

    .line 2372844
    invoke-virtual {v5}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/1nQ;->b(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2372845
    iget-object v1, p0, LX/Gcx;->c:LX/Gcz;

    iget-object v1, v1, LX/Gcz;->i:LX/Blh;

    iget-object v2, p0, LX/Gcx;->c:LX/Gcz;

    iget-object v2, v2, LX/Gcz;->a:Landroid/content/Context;

    iget-object v3, p0, LX/Gcx;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372846
    iget-object v4, v3, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object v3, v4

    .line 2372847
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2372848
    iget-object v4, p0, LX/Gcx;->c:LX/Gcz;

    iget-object v4, v4, LX/Gcz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    iget-object v5, p0, LX/Gcx;->c:LX/Gcz;

    iget-object v5, v5, LX/Gcz;->c:Lcom/facebook/events/common/ActionMechanism;

    .line 2372849
    new-instance v7, Lcom/facebook/events/common/EventActionContext;

    iget-object v8, v4, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    iget-object v10, v4, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    iget-object v11, v4, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    iget-boolean v12, v4, Lcom/facebook/events/common/EventActionContext;->i:Z

    move-object v9, v5

    invoke-direct/range {v7 .. v12}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Z)V

    move-object v4, v7

    .line 2372850
    invoke-virtual {v1, v2, v3, v4}, LX/Blh;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;)V

    .line 2372851
    const v1, -0x5e58d996

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2372852
    :pswitch_0
    const-string v4, "event_subscriptions"

    goto :goto_0

    .line 2372853
    :pswitch_1
    const-string v4, "event_suggestions"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
