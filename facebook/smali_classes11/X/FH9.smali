.class public final LX/FH9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final d:I

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/ui/media/attachments/MediaResource;I)V
    .locals 2

    .prologue
    .line 2219893
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/FH9;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;IZLjava/lang/String;)V

    .line 2219894
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ui/media/attachments/MediaResource;IZLjava/lang/String;)V
    .locals 5

    .prologue
    .line 2219878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219879
    if-eqz p3, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Chunked upload used but session id not provided."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2219880
    iput-object p1, p0, LX/FH9;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219881
    iput p2, p0, LX/FH9;->d:I

    .line 2219882
    iput-boolean p3, p0, LX/FH9;->a:Z

    .line 2219883
    iput-object p4, p0, LX/FH9;->b:Ljava/lang/String;

    .line 2219884
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    invoke-static {v0}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/FH9;->e:Ljava/util/Map;

    .line 2219885
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->isQuickCamSource()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2219886
    const-string v1, "selfie_cam"

    .line 2219887
    iget-object v2, p0, LX/FH9;->e:Ljava/util/Map;

    const-string v3, "camera_position"

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v4, LX/5zj;->QUICKCAM_BACK:LX/5zj;

    if-ne v0, v4, :cond_2

    const-string v0, "back_facing"

    :goto_1
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 2219888
    :goto_2
    iget-object v1, p0, LX/FH9;->e:Ljava/util/Map;

    const-string v2, "image_send_source"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219889
    return-void

    .line 2219890
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2219891
    :cond_2
    const-string v0, "front_facing"

    goto :goto_1

    .line 2219892
    :cond_3
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    iget-object v0, v0, LX/5zj;->DBSerialValue:Ljava/lang/String;

    goto :goto_2
.end method
