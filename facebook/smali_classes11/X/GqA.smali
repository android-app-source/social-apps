.class public LX/GqA;
.super LX/Cod;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpZ;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/23R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Landroid/widget/LinearLayout;

.field public final d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2396219
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396220
    const-class v0, LX/GqA;

    invoke-static {v0, p0}, LX/GqA;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396221
    const v0, 0x7f0d0bc1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/GqA;->c:Landroid/widget/LinearLayout;

    .line 2396222
    iget-object v0, p0, LX/GqA;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0bc2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GqA;->e:Landroid/widget/TextView;

    .line 2396223
    iget-object v0, p0, LX/GqA;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0bc4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GqA;->f:Landroid/widget/TextView;

    .line 2396224
    iget-object v0, p0, LX/GqA;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0bc3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;

    iput-object v0, p0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    .line 2396225
    iget-object v0, p0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    const/4 v1, 0x1

    .line 2396226
    iput-boolean v1, v0, Lcom/facebook/widget/mosaic/MosaicGridLayout;->c:Z

    .line 2396227
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c41

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2396228
    iget-object v1, p0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-virtual {v1, v0, v0}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->b(II)V

    .line 2396229
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GqA;

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v1

    check-cast v1, LX/23R;

    invoke-static {p0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object p0

    check-cast p0, LX/GnF;

    iput-object v1, p1, LX/GqA;->a:LX/23R;

    iput-object p0, p1, LX/GqA;->b:LX/GnF;

    return-void
.end method
