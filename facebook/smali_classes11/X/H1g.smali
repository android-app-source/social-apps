.class public final LX/H1g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;)V
    .locals 0

    .prologue
    .line 2415729
    iput-object p1, p0, LX/H1g;->a:Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2415730
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2415731
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2415732
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2415733
    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;

    .line 2415734
    iget-object v1, p0, LX/H1g;->a:Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;

    .line 2415735
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;->a()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->g:Ljava/lang/String;

    .line 2415736
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;->k()Z

    move-result p0

    iput-boolean p0, v1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->i:Z

    .line 2415737
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;->j()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->h:Ljava/lang/String;

    .line 2415738
    invoke-static {v1}, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->b(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;)V

    .line 2415739
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;->l()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object p0

    iget-object p1, v1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->e:LX/CNc;

    invoke-static {p0, p1}, LX/CNu;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/CNc;)LX/0Px;

    move-result-object p0

    .line 2415740
    iget-object p1, v1, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->e:LX/CNc;

    iget-object p1, p1, LX/CNc;->b:LX/CNS;

    invoke-virtual {p1, p0}, LX/CNS;->a(LX/0Px;)V

    .line 2415741
    return-void
.end method
