.class public LX/Fgq;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Fgp;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2271092
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2271093
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;)LX/Fgp;
    .locals 13

    .prologue
    .line 2271094
    new-instance v0, LX/Fgp;

    const/16 v1, 0x34e0

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2271095
    const/16 v1, 0x34b6

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v1}, LX/FZu;->a(LX/0Or;)LX/FgU;

    move-result-object v1

    move-object v4, v1

    .line 2271096
    check-cast v4, LX/FgU;

    .line 2271097
    new-instance v1, LX/Fid;

    invoke-direct {v1}, LX/Fid;-><init>()V

    .line 2271098
    move-object v1, v1

    .line 2271099
    move-object v5, v1

    .line 2271100
    check-cast v5, LX/Fid;

    const-class v1, LX/FhF;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/FhF;

    invoke-static {p0}, LX/Fi6;->a(LX/0QB;)LX/Fi6;

    move-result-object v7

    check-cast v7, LX/Fi6;

    const/16 v1, 0x34f6

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v9

    check-cast v9, LX/1DS;

    invoke-static {p0}, LX/Cvy;->a(LX/0QB;)LX/Cvy;

    move-result-object v10

    check-cast v10, LX/Cvy;

    invoke-static {p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v11

    check-cast v11, LX/1Db;

    const-class v1, LX/Fht;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/Fht;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v12}, LX/Fgp;-><init>(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;LX/0Ot;LX/FgU;LX/Fid;LX/FhF;LX/Fi6;LX/0Ot;LX/1DS;LX/Cvy;LX/1Db;LX/Fht;)V

    .line 2271101
    const/16 v1, 0x32fb

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2271102
    iput-object v1, v0, LX/Fgp;->l:LX/0Ot;

    .line 2271103
    return-object v0
.end method
