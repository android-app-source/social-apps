.class public LX/Gg7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/Gfb;

.field public final b:LX/Gfq;

.field public final c:LX/Gfl;


# direct methods
.method public constructor <init>(LX/Gfq;LX/Gfb;LX/Gfl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378553
    iput-object p1, p0, LX/Gg7;->b:LX/Gfq;

    .line 2378554
    iput-object p2, p0, LX/Gg7;->a:LX/Gfb;

    .line 2378555
    iput-object p3, p0, LX/Gg7;->c:LX/Gfl;

    .line 2378556
    return-void
.end method

.method public static a(LX/0QB;)LX/Gg7;
    .locals 6

    .prologue
    .line 2378557
    const-class v1, LX/Gg7;

    monitor-enter v1

    .line 2378558
    :try_start_0
    sget-object v0, LX/Gg7;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378559
    sput-object v2, LX/Gg7;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378560
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378561
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378562
    new-instance p0, LX/Gg7;

    invoke-static {v0}, LX/Gfq;->a(LX/0QB;)LX/Gfq;

    move-result-object v3

    check-cast v3, LX/Gfq;

    invoke-static {v0}, LX/Gfb;->a(LX/0QB;)LX/Gfb;

    move-result-object v4

    check-cast v4, LX/Gfb;

    invoke-static {v0}, LX/Gfl;->a(LX/0QB;)LX/Gfl;

    move-result-object v5

    check-cast v5, LX/Gfl;

    invoke-direct {p0, v3, v4, v5}, LX/Gg7;-><init>(LX/Gfq;LX/Gfb;LX/Gfl;)V

    .line 2378563
    move-object v0, p0

    .line 2378564
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378565
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gg7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378566
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378567
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
