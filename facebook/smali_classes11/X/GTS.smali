.class public final LX/GTS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/GTP;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GTT;


# direct methods
.method public constructor <init>(LX/GTT;)V
    .locals 0

    .prologue
    .line 2355526
    iput-object p1, p0, LX/GTS;->a:LX/GTT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2355527
    const-string v0, "Failed with exception: %s"

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2355528
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/GTS;->a:LX/GTT;

    iget-object v2, v2, LX/GTT;->a:LX/GTU;

    invoke-virtual {v2}, LX/GTU;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, LX/GTR;

    invoke-direct {v2, p0}, LX/GTR;-><init>(LX/GTS;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2355529
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2355530
    check-cast p1, LX/GTP;

    .line 2355531
    if-eqz p1, :cond_0

    const-string v0, "Received reauth token \'%s\' with age \'%d\' and ttl \'%d\'"

    .line 2355532
    iget-object v1, p1, LX/GTP;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2355533
    const-string v2, "^.{0,24}"

    const-string v3, "************************"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2355534
    iget-object v6, p1, LX/GTP;->d:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    iget-wide v8, p1, LX/GTP;->b:J

    sub-long/2addr v6, v8

    move-wide v2, v6

    .line 2355535
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, LX/GTP;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2355536
    :goto_0
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/GTS;->a:LX/GTT;

    iget-object v2, v2, LX/GTT;->a:LX/GTU;

    invoke-virtual {v2}, LX/GTU;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, LX/GTQ;

    invoke-direct {v2, p0}, LX/GTQ;-><init>(LX/GTS;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2355537
    return-void

    .line 2355538
    :cond_0
    const-string v0, "Received a null result"

    goto :goto_0
.end method
