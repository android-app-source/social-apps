.class public final LX/GYn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GYk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GYk",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GYo;


# direct methods
.method public constructor <init>(LX/GYo;)V
    .locals 0

    .prologue
    .line 2365634
    iput-object p1, p0, LX/GYn;->a:LX/GYo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365635
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365636
    if-eqz p1, :cond_0

    .line 2365637
    iget-object v0, p0, LX/GYn;->a:LX/GYo;

    .line 2365638
    iget-object v1, v0, LX/GYo;->c:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    .line 2365639
    iput-object p1, v1, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365640
    iget-object v0, p0, LX/GYn;->a:LX/GYo;

    invoke-static {v0}, LX/GYo;->c(LX/GYo;)V

    .line 2365641
    :goto_0
    return-void

    .line 2365642
    :cond_0
    const-string v0, "PageViewerContextResultHandler, Page ViewerContext is null"

    .line 2365643
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, LX/GYn;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p2    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365644
    iget-object v0, p0, LX/GYn;->a:LX/GYo;

    invoke-static {v0, p1, p2}, LX/GYo;->a$redex0(LX/GYo;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2365645
    return-void
.end method
