.class public LX/GUA;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/fbui/glyph/GlyphView;

.field public l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2357047
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2357048
    const/16 v1, 0x10

    const/4 p1, 0x0

    .line 2357049
    const-class v0, LX/GUA;

    invoke-static {v0, p0}, LX/GUA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2357050
    const v0, 0x7f03015d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2357051
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 2357052
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2357053
    invoke-virtual {p0}, LX/GUA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0, p1, p1, p1}, LX/GUA;->setPadding(IIII)V

    .line 2357054
    const v0, 0x7f0d0653

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/GUA;->k:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2357055
    const v0, 0x7f0d0654

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GUA;->l:Landroid/widget/TextView;

    .line 2357056
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GUA;

    invoke-static {p0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object p0

    check-cast p0, LX/8Sa;

    iput-object p0, p1, LX/GUA;->j:LX/8Sa;

    return-void
.end method
