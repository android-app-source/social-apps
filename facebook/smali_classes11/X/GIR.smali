.class public final LX/GIR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GIT;


# direct methods
.method public constructor <init>(LX/GIT;)V
    .locals 0

    .prologue
    .line 2336613
    iput-object p1, p0, LX/GIR;->a:LX/GIT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2336614
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 2336615
    :goto_0
    return-void

    .line 2336616
    :cond_0
    const-string v0, "audience_extra"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2336617
    const-string v1, "targeting_data_extra"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2336618
    iget-object v3, p0, LX/GIR;->a:LX/GIT;

    iget-object v3, v3, LX/GIT;->d:LX/GLd;

    invoke-virtual {v3, v1}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2336619
    iget-object v1, p0, LX/GIR;->a:LX/GIT;

    const/4 v3, 0x4

    .line 2336620
    iput v3, v1, LX/GIT;->i:I

    .line 2336621
    iget-object v1, p0, LX/GIR;->a:LX/GIT;

    iget-object v3, v1, LX/GIT;->b:LX/GLb;

    iget-object v1, p0, LX/GIR;->a:LX/GIT;

    iget-boolean v1, v1, LX/GIT;->h:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/GIR;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2336622
    iget-object v4, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v4

    .line 2336623
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v1, v4, :cond_2

    :cond_1
    move v1, v2

    .line 2336624
    :goto_1
    iput-boolean v1, v3, LX/GLb;->n:Z

    .line 2336625
    iget-object v1, p0, LX/GIR;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->b:LX/GLb;

    iget-object v3, p0, LX/GIR;->a:LX/GIT;

    iget v3, v3, LX/GIT;->i:I

    invoke-virtual {v1, v2, v3}, LX/GLb;->a(II)V

    .line 2336626
    iget-object v1, p0, LX/GIR;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->b:LX/GLb;

    iget-object v2, p0, LX/GIR;->a:LX/GIT;

    iget-object v2, v2, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v2}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v2

    const-string v3, "selector_unified_audiences_task_key"

    invoke-virtual {v1, v2, v3, v0}, LX/GLb;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    goto :goto_0

    .line 2336627
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
