.class public final LX/GiM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GiO;


# direct methods
.method public constructor <init>(LX/GiO;)V
    .locals 0

    .prologue
    .line 2385709
    iput-object p1, p0, LX/GiM;->a:LX/GiO;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2385710
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2385711
    check-cast p1, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel;

    .line 2385712
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2385713
    invoke-virtual {p1}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel;->a()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;

    move-result-object v0

    .line 2385714
    iget-object v1, p0, LX/GiM;->a:LX/GiO;

    iget-object v1, v1, LX/GiO;->a:LX/0Sh;

    new-instance v2, Lcom/facebook/gametime/ui/reaction/GametimePlaySubscriber$1$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/gametime/ui/reaction/GametimePlaySubscriber$1$1;-><init>(LX/GiM;Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimePlaySubscriptionModel$PlayModel;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2385715
    :cond_0
    return-void
.end method
