.class public final LX/Fyl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2307151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2307152
    const/4 v1, 0x1

    .line 2307153
    new-instance v2, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-direct {v2}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;-><init>()V

    .line 2307154
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2, v0}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2307155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 2307157
    :goto_0
    iput-boolean v0, v2, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    .line 2307158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;)V

    .line 2307159
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 2307160
    iput-object v0, v2, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->e:Ljava/lang/String;

    .line 2307161
    return-object v2

    .line 2307162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2307163
    new-array v0, p1, [Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    return-object v0
.end method
