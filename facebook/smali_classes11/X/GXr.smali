.class public final enum LX/GXr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GXr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GXr;

.field public static final enum SELLER_PROFILE_ID:LX/GXr;

.field public static final enum TARGET_ID:LX/GXr;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2364612
    new-instance v0, LX/GXr;

    const-string v1, "SELLER_PROFILE_ID"

    const-string v2, "seller_profile_id"

    invoke-direct {v0, v1, v3, v2}, LX/GXr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXr;->SELLER_PROFILE_ID:LX/GXr;

    .line 2364613
    new-instance v0, LX/GXr;

    const-string v1, "TARGET_ID"

    const-string v2, "target_id"

    invoke-direct {v0, v1, v4, v2}, LX/GXr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXr;->TARGET_ID:LX/GXr;

    .line 2364614
    const/4 v0, 0x2

    new-array v0, v0, [LX/GXr;

    sget-object v1, LX/GXr;->SELLER_PROFILE_ID:LX/GXr;

    aput-object v1, v0, v3

    sget-object v1, LX/GXr;->TARGET_ID:LX/GXr;

    aput-object v1, v0, v4

    sput-object v0, LX/GXr;->$VALUES:[LX/GXr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2364615
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2364616
    iput-object p3, p0, LX/GXr;->value:Ljava/lang/String;

    .line 2364617
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GXr;
    .locals 1

    .prologue
    .line 2364618
    const-class v0, LX/GXr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GXr;

    return-object v0
.end method

.method public static values()[LX/GXr;
    .locals 1

    .prologue
    .line 2364619
    sget-object v0, LX/GXr;->$VALUES:[LX/GXr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GXr;

    return-object v0
.end method
