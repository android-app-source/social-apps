.class public interface abstract LX/H70;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H6y;
.implements LX/H6z;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "OfferClaimData"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscountBarcodeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract D()LX/H72;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferView"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract E()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
