.class public final LX/GO0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/AddressActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/AddressActivity;)V
    .locals 0

    .prologue
    .line 2347101
    iput-object p1, p0, LX/GO0;->a:Lcom/facebook/adspayments/activity/AddressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 2347102
    if-nez p2, :cond_0

    iget-object v0, p0, LX/GO0;->a:Lcom/facebook/adspayments/activity/AddressActivity;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/AddressActivity;->C(Lcom/facebook/adspayments/activity/AddressActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2347103
    iget-object v0, p0, LX/GO0;->a:Lcom/facebook/adspayments/activity/AddressActivity;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/GO0;->a:Lcom/facebook/adspayments/activity/AddressActivity;

    const v2, 0x7f0827f3

    invoke-virtual {v1, v2}, Lcom/facebook/adspayments/activity/AddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Ljava/lang/String;)V

    .line 2347104
    :goto_0
    iget-object v0, p0, LX/GO0;->a:Lcom/facebook/adspayments/activity/AddressActivity;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/AddressActivity;->D(Lcom/facebook/adspayments/activity/AddressActivity;)V

    .line 2347105
    return-void

    .line 2347106
    :cond_0
    iget-object v0, p0, LX/GO0;->a:Lcom/facebook/adspayments/activity/AddressActivity;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/AddressActivity;->K:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->d()V

    goto :goto_0
.end method
