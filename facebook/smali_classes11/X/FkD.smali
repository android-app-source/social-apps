.class public LX/FkD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[J

.field private static volatile i:LX/FkD;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/0Sh;

.field private final f:Landroid/content/res/Resources;

.field private final g:Landroid/app/NotificationManager;

.field private final h:LX/Fk5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2278294
    const-class v0, LX/FkD;

    sput-object v0, LX/FkD;->a:Ljava/lang/Class;

    .line 2278295
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, LX/FkD;->b:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0xfa
        0xc8
        0xfa
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Sh;Landroid/content/res/Resources;Landroid/app/NotificationManager;LX/Fk5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278297
    iput-object p1, p0, LX/FkD;->c:Landroid/content/Context;

    .line 2278298
    iput-object p2, p0, LX/FkD;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2278299
    iput-object p3, p0, LX/FkD;->e:LX/0Sh;

    .line 2278300
    iput-object p4, p0, LX/FkD;->f:Landroid/content/res/Resources;

    .line 2278301
    iput-object p5, p0, LX/FkD;->g:Landroid/app/NotificationManager;

    .line 2278302
    iput-object p6, p0, LX/FkD;->h:LX/Fk5;

    .line 2278303
    return-void
.end method

.method public static a(LX/0QB;)LX/FkD;
    .locals 10

    .prologue
    .line 2278304
    sget-object v0, LX/FkD;->i:LX/FkD;

    if-nez v0, :cond_1

    .line 2278305
    const-class v1, LX/FkD;

    monitor-enter v1

    .line 2278306
    :try_start_0
    sget-object v0, LX/FkD;->i:LX/FkD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2278307
    if-eqz v2, :cond_0

    .line 2278308
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2278309
    new-instance v3, LX/FkD;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    invoke-static {v0}, LX/Fk5;->b(LX/0QB;)LX/Fk5;

    move-result-object v9

    check-cast v9, LX/Fk5;

    invoke-direct/range {v3 .. v9}, LX/FkD;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Sh;Landroid/content/res/Resources;Landroid/app/NotificationManager;LX/Fk5;)V

    .line 2278310
    move-object v0, v3

    .line 2278311
    sput-object v0, LX/FkD;->i:LX/FkD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2278312
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2278313
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2278314
    :cond_1
    sget-object v0, LX/FkD;->i:LX/FkD;

    return-object v0

    .line 2278315
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2278316
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized a(LX/FkD;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2278317
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FkD;->c:Landroid/content/Context;

    const/4 v1, 0x0

    const-string v2, "notification"

    invoke-static {p0, p1, p2, v2}, LX/FkD;->b(LX/FkD;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2278318
    new-instance v1, LX/3pe;

    invoke-direct {v1}, LX/3pe;-><init>()V

    invoke-virtual {v1, p5}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v1

    .line 2278319
    invoke-static {p4}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v1, LX/3pe;->e:Ljava/lang/CharSequence;

    .line 2278320
    move-object v1, v1

    .line 2278321
    const/4 v2, 0x2

    invoke-static {v2}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v2

    .line 2278322
    new-instance v3, LX/2HB;

    iget-object v4, p0, LX/FkD;->c:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, p3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, p4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, p5}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    .line 2278323
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2278324
    move-object v0, v1

    .line 2278325
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    sget-object v1, LX/FkD;->b:[J

    invoke-virtual {v0, v1}, LX/2HB;->a([J)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2HB;->a(Landroid/net/Uri;)LX/2HB;

    move-result-object v0

    .line 2278326
    iget-object v1, p0, LX/FkD;->g:Landroid/app/NotificationManager;

    const/16 v2, 0x442

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2278327
    iget-object v0, p0, LX/FkD;->h:LX/Fk5;

    const-string v1, "sideloading_post_ready_to_install_notification"

    invoke-virtual {v0, v1}, LX/Fk5;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2278328
    monitor-exit p0

    return-void

    .line 2278329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/FkD;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2278330
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/FkD;->c:Landroid/content/Context;

    const-class v2, Lcom/facebook/sideloading/SideloadingInstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2278331
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2278332
    const-string v1, "local_uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2278333
    const-string v1, "package_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2278334
    const-string v1, "triggered_from"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2278335
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2278336
    iget-object v0, p0, LX/FkD;->f:Landroid/content/res/Resources;

    const v1, 0x7f0832cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2278337
    iget-object v0, p0, LX/FkD;->f:Landroid/content/res/Resources;

    const v1, 0x7f0832cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2278338
    iget-object v0, p0, LX/FkD;->f:Landroid/content/res/Resources;

    const v1, 0x7f0832cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2278339
    const v3, 0x7f0212e2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, LX/FkD;->a(LX/FkD;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2278340
    return-void
.end method
