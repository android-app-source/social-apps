.class public LX/FDq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final h:[Ljava/lang/String;

.field private static volatile i:LX/FDq;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/FDp;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/3Mq;

.field private final e:LX/2N6;

.field private final f:LX/2N5;

.field private final g:LX/FKD;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2213598
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "unread_count"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "unseen_count"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "last_seen_time"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "last_action_id"

    aput-object v2, v0, v1

    sput-object v0, LX/FDq;->h:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/FDp;LX/0Or;LX/3Mq;LX/2N6;LX/2N5;LX/FKD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/FDp;",
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;",
            "LX/3Mq;",
            "LX/2N6;",
            "LX/2N5;",
            "LX/FKD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2213331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2213332
    iput-object p1, p0, LX/FDq;->a:LX/0Or;

    .line 2213333
    iput-object p2, p0, LX/FDq;->b:LX/FDp;

    .line 2213334
    iput-object p3, p0, LX/FDq;->c:LX/0Or;

    .line 2213335
    iput-object p4, p0, LX/FDq;->d:LX/3Mq;

    .line 2213336
    iput-object p5, p0, LX/FDq;->e:LX/2N6;

    .line 2213337
    iput-object p6, p0, LX/FDq;->f:LX/2N5;

    .line 2213338
    iput-object p7, p0, LX/FDq;->g:LX/FKD;

    .line 2213339
    return-void
.end method

.method public static a(Ljava/util/Collection;I)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213578
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2213579
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2213580
    const/4 v0, 0x0

    .line 2213581
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    if-ge v0, p1, :cond_0

    .line 2213582
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2213583
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2213584
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/FDq;
    .locals 11

    .prologue
    .line 2213565
    sget-object v0, LX/FDq;->i:LX/FDq;

    if-nez v0, :cond_1

    .line 2213566
    const-class v1, LX/FDq;

    monitor-enter v1

    .line 2213567
    :try_start_0
    sget-object v0, LX/FDq;->i:LX/FDq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2213568
    if-eqz v2, :cond_0

    .line 2213569
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2213570
    new-instance v3, LX/FDq;

    const/16 v4, 0x274b

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/FDp;->a(LX/0QB;)LX/FDp;

    move-result-object v5

    check-cast v5, LX/FDp;

    const/16 v6, 0x2744

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/3Mq;->a(LX/0QB;)LX/3Mq;

    move-result-object v7

    check-cast v7, LX/3Mq;

    invoke-static {v0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v8

    check-cast v8, LX/2N6;

    invoke-static {v0}, LX/2N5;->a(LX/0QB;)LX/2N5;

    move-result-object v9

    check-cast v9, LX/2N5;

    invoke-static {v0}, LX/FKD;->b(LX/0QB;)LX/FKD;

    move-result-object v10

    check-cast v10, LX/FKD;

    invoke-direct/range {v3 .. v10}, LX/FDq;-><init>(LX/0Or;LX/FDp;LX/0Or;LX/3Mq;LX/2N6;LX/2N5;LX/FKD;)V

    .line 2213571
    move-object v0, v3

    .line 2213572
    sput-object v0, LX/FDq;->i:LX/FDq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213573
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2213574
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213575
    :cond_1
    sget-object v0, LX/FDq;->i:LX/FDq;

    return-object v0

    .line 2213576
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2213577
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/LinkedHashMap;)Ljava/util/Set;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2213554
    new-instance v4, LX/0UE;

    invoke-direct {v4}, LX/0UE;-><init>()V

    .line 2213555
    invoke-virtual {p0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2213556
    iget-object v6, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2213557
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2213558
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2213559
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2213560
    :cond_2
    iget-object v3, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2213561
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 2213562
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2213563
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2213564
    :cond_4
    return-object v4
.end method

.method private static a(LX/0Pz;LX/6dO;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "LX/6dO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2213551
    :goto_0
    invoke-virtual {p1}, LX/6dO;->b()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2213552
    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2213553
    :cond_0
    return-void
.end method

.method private b(LX/6ek;JI)Ljava/util/LinkedHashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "JI)",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/6g6;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213535
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 2213536
    iget-object v1, p0, LX/FDq;->d:LX/3Mq;

    .line 2213537
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v8

    .line 2213538
    const-string v4, "folder"

    iget-object v5, p1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    invoke-virtual {v8, v4}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2213539
    const-wide/16 v4, 0x0

    cmp-long v4, p2, v4

    if-lez v4, :cond_0

    .line 2213540
    const-string v4, "timestamp_in_folder_ms"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    invoke-virtual {v8, v4}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2213541
    :cond_0
    const-string v9, "timestamp_in_folder_ms DESC"

    .line 2213542
    if-lez p4, :cond_1

    .line 2213543
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2213544
    :cond_1
    iget-object v4, v1, LX/3Mq;->b:Landroid/content/ContentResolver;

    iget-object v5, v1, LX/3Mq;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/6dB;

    iget-object v5, v5, LX/6dB;->c:LX/6dA;

    invoke-virtual {v5}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v5

    sget-object v6, LX/3Mq;->e:[Ljava/lang/String;

    invoke-virtual {v8}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 2213545
    iget-object v5, v1, LX/3Mq;->d:LX/2N5;

    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, LX/2N5;->a(Landroid/database/Cursor;Z)LX/6dO;

    move-result-object v4

    move-object v1, v4

    .line 2213546
    :goto_0
    :try_start_0
    invoke-virtual {v1}, LX/6dO;->a()LX/6g6;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2213547
    iget-object v3, v2, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v3

    .line 2213548
    invoke-virtual {v0, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2213549
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/6dO;->close()V

    throw v0

    :cond_2
    invoke-virtual {v1}, LX/6dO;->close()V

    .line 2213550
    return-object v0
.end method

.method private b(Ljava/util/LinkedHashMap;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/6g6;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v1, 0x1

    .line 2213514
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2213515
    new-instance v4, LX/0UE;

    invoke-direct {v4}, LX/0UE;-><init>()V

    .line 2213516
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6g6;

    .line 2213517
    iget-wide v12, v0, LX/6g6;->j:J

    move-wide v8, v12

    .line 2213518
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2213519
    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [LX/0ux;

    const-string v5, "thread_key"

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-static {v5, v7}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v5

    aput-object v5, v0, v10

    const-string v5, "msg_type"

    sget-object v7, LX/2uW;->FAILED_SEND:LX/2uW;

    iget v7, v7, LX/2uW;->dbKeyValue:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v0, v1

    new-array v5, v11, [LX/0ux;

    const-string v7, "timestamp_ms"

    const-wide/32 v8, 0x5265c00

    sub-long/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v5, v10

    const-string v2, "timestamp_ms"

    invoke-static {v2, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v5}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v2

    aput-object v2, v0, v11

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v5

    .line 2213520
    iget-object v0, p0, LX/FDq;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2213521
    const-string v2, "messages"

    new-array v3, v11, [Ljava/lang/String;

    const-string v4, "thread_key"

    aput-object v4, v3, v10

    const-string v4, "timestamp_ms"

    aput-object v4, v3, v1

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2213522
    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2213523
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2213524
    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6g6;

    .line 2213525
    const/4 v2, 0x1

    .line 2213526
    iput-boolean v2, v0, LX/6g6;->x:Z

    .line 2213527
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2213528
    iget-wide v12, v0, LX/6g6;->j:J

    move-wide v4, v12

    .line 2213529
    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 2213530
    const/4 v2, 0x1

    .line 2213531
    iput-boolean v2, v0, LX/6g6;->z:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2213532
    goto :goto_1

    .line 2213533
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2213534
    return-void
.end method

.method public static c(LX/FDq;LX/6ek;)Z
    .locals 8

    .prologue
    .line 2213505
    const-string v0, "DbFetchThreadsHandler.containsFirstThreadSentinalForFolder"

    const v1, 0x73cf241f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213506
    :try_start_0
    const-string v0, "thread_key"

    invoke-static {p1}, LX/6cr;->a(LX/6ek;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2213507
    iget-object v0, p0, LX/FDq;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2213508
    const-string v1, "folders"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "thread_key"

    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 2213509
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 2213510
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213511
    const v0, 0x48e15027

    invoke-static {v0}, LX/02m;->a(I)V

    return v1

    .line 2213512
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2213513
    :catchall_1
    move-exception v0

    const v1, 0x1f4f0486

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private d()LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213585
    const-string v0, "DbFetchThreadsHandler.doPinnedThreadsQuery"

    const v1, 0x7182b457

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213586
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2213587
    iget-object v1, p0, LX/FDq;->d:LX/3Mq;

    const/4 v6, 0x0

    .line 2213588
    const-string v8, "pinned_threads_display_order"

    .line 2213589
    iget-object v3, v1, LX/3Mq;->b:Landroid/content/ContentResolver;

    iget-object v4, v1, LX/3Mq;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6dB;

    iget-object v4, v4, LX/6dB;->c:LX/6dA;

    invoke-virtual {v4}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v4

    sget-object v5, LX/3Mq;->f:[Ljava/lang/String;

    move-object v7, v6

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2213590
    iget-object v4, v1, LX/3Mq;->d:LX/2N5;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/2N5;->a(Landroid/database/Cursor;Z)LX/6dO;

    move-result-object v3

    move-object v1, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2213591
    :goto_0
    :try_start_1
    invoke-virtual {v1}, LX/6dO;->a()LX/6g6;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2213592
    invoke-virtual {v2}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2213593
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, LX/6dO;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213594
    :catchall_1
    move-exception v0

    const v1, -0x72930212

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2213595
    :cond_0
    :try_start_3
    invoke-virtual {v1}, LX/6dO;->close()V

    .line 2213596
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 2213597
    const v1, 0x2b5d7f0d

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/6ek;)J
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2213496
    iget-object v0, p0, LX/FDq;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2213497
    const-string v1, "SELECT MIN(timestamp_ms) FROM folders WHERE folder=? AND thread_key != ?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p1, LX/6ek;->dbName:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {p1}, LX/6cr;->a(LX/6ek;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2213498
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2213499
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2213500
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2213501
    :goto_0
    return-wide v0

    .line 2213502
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2213503
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2213504
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(LX/0Rf;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 2213471
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 2213472
    invoke-virtual {p1}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2213473
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2213474
    :goto_0
    return-object v0

    .line 2213475
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "count(*) = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0Rf;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2213476
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/6cw;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/6cv;->PARTICIPANT:LX/6cv;

    iget-object v2, v2, LX/6cv;->dbValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2213477
    const-string v1, "thread_participants"

    new-array v2, v8, [Ljava/lang/String;

    sget-object v4, LX/6cw;->a:Ljava/lang/String;

    aput-object v4, v2, v0

    sget-object v4, LX/6cw;->a:Ljava/lang/String;

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2213478
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2213479
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, LX/6cw;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2213480
    invoke-virtual {p1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    .line 2213481
    const-string v7, "\'"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "\',"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2213482
    :cond_1
    const-string v1, ","

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, LX/6cw;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2213483
    const-string v1, "thread_participants"

    new-array v2, v8, [Ljava/lang/String;

    sget-object v3, LX/6cw;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/6cw;->a:Ljava/lang/String;

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2213484
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2213485
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") NATURAL JOIN threads"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2213486
    iget-object v0, p0, LX/FDq;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 2213487
    const-string v11, "timestamp_ms DESC"

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    move-object v10, v6

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2213488
    :try_start_0
    iget-object v0, p0, LX/FDq;->f:LX/2N5;

    invoke-virtual {v0, v1}, LX/2N5;->a(Landroid/database/Cursor;)LX/6dO;

    move-result-object v2

    .line 2213489
    invoke-virtual {v2}, LX/6dO;->b()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2213490
    :goto_2
    if-eqz v0, :cond_2

    .line 2213491
    invoke-virtual {v12, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2213492
    invoke-virtual {v2}, LX/6dO;->b()Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 2213493
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2213494
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 2213495
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Ljava/util/Set;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213461
    const-string v0, "DbFetchThreadsHandler.doCustomThreadSetQuery"

    const v1, -0x2f9b7bba

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213462
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2213463
    iget-object v1, p0, LX/FDq;->d:LX/3Mq;

    invoke-virtual {v1, p1}, LX/3Mq;->a(Ljava/util/Set;)LX/6dO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2213464
    :goto_0
    :try_start_1
    invoke-virtual {v1}, LX/6dO;->a()LX/6g6;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2213465
    invoke-virtual {v2}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2213466
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, LX/6dO;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213467
    :catchall_1
    move-exception v0

    const v1, -0x14d26c21

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2213468
    :cond_0
    :try_start_3
    invoke-virtual {v1}, LX/6dO;->close()V

    .line 2213469
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 2213470
    const v1, -0x3996cfc5

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0
.end method

.method public final a()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
    .locals 7

    .prologue
    .line 2213442
    const-string v0, "DbFetchThreadsHandler.fetchPinnedThreadResultFromDb"

    const v1, -0x57e0b583

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213443
    :try_start_0
    iget-object v0, p0, LX/FDq;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    .line 2213444
    sget-object v1, LX/6cx;->b:LX/2bA;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/48u;->a(LX/0To;J)J

    move-result-wide v2

    .line 2213445
    sget-object v1, LX/6cx;->c:LX/2bA;

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, LX/48u;->a(LX/0To;J)J

    move-result-wide v4

    .line 2213446
    invoke-direct {p0}, LX/FDq;->d()LX/0Px;

    move-result-object v0

    .line 2213447
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v1

    .line 2213448
    iput-object v0, v1, LX/6hv;->a:Ljava/util/List;

    .line 2213449
    move-object v1, v1

    .line 2213450
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2213451
    :goto_0
    iput-boolean v0, v1, LX/6hv;->b:Z

    .line 2213452
    move-object v0, v1

    .line 2213453
    iput-wide v2, v0, LX/6hv;->c:J

    .line 2213454
    move-object v0, v0

    .line 2213455
    iput-wide v4, v0, LX/6hv;->d:J

    .line 2213456
    move-object v0, v0

    .line 2213457
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2213458
    const v1, 0x6e39a4bb

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2213459
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2213460
    :catchall_0
    move-exception v0

    const v1, 0x6d798787

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 13
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v10, -0x1

    .line 2213395
    const-string v0, "DbFetchThreadsHandler.fetchThreadListFromDb"

    const v3, 0x121e77bb    # 5.000359E-28f

    invoke-static {v0, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213396
    :try_start_0
    iget-object v0, p0, LX/FDq;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    .line 2213397
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v4, v3

    .line 2213398
    invoke-static {v4}, LX/6cx;->a(LX/6ek;)LX/2bA;

    move-result-object v3

    const-wide/16 v6, -0x1

    invoke-virtual {v0, v3, v6, v7}, LX/48u;->a(LX/0To;J)J

    move-result-wide v6

    .line 2213399
    invoke-static {v4}, LX/6cx;->c(LX/6ek;)LX/2bA;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v0, v3, v5}, LX/48u;->a(LX/0To;Z)Z

    move-result v3

    .line 2213400
    invoke-static {v4}, LX/6cx;->b(LX/6ek;)LX/2bA;

    move-result-object v5

    const-wide/16 v8, -0x1

    invoke-virtual {v0, v5, v8, v9}, LX/48u;->a(LX/0To;J)J

    move-result-wide v8

    .line 2213401
    cmp-long v0, v6, v10

    if-nez v0, :cond_0

    .line 2213402
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v0

    sget-object v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2213403
    iput-object v1, v0, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2213404
    move-object v0, v0

    .line 2213405
    iput-object v4, v0, LX/6iK;->b:LX/6ek;

    .line 2213406
    move-object v0, v0

    .line 2213407
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2213408
    const v1, 0x48e1dc4e

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 2213409
    :cond_0
    if-eqz v3, :cond_2

    .line 2213410
    :try_start_1
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-object v3, v0

    .line 2213411
    :goto_1
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v0, v0

    .line 2213412
    const-wide/16 v10, -0x1

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v5

    invoke-virtual {p0, v0, v10, v11, v5}, LX/FDq;->a(LX/6ek;JI)Ljava/util/LinkedHashMap;

    move-result-object v5

    .line 2213413
    invoke-static {v5}, LX/FDq;->a(Ljava/util/LinkedHashMap;)Ljava/util/Set;

    move-result-object v0

    .line 2213414
    iget-object v10, p0, LX/FDq;->b:LX/FDp;

    invoke-virtual {v10, v0}, LX/FDp;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v10

    .line 2213415
    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v11

    if-ge v0, v11, :cond_3

    invoke-static {p0, v4}, LX/FDq;->c(LX/FDq;LX/6ek;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2213416
    :goto_2
    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v2

    invoke-static {v1, v2}, LX/FDq;->a(Ljava/util/Collection;I)LX/0Px;

    move-result-object v1

    .line 2213417
    new-instance v2, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-direct {v2, v1, v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2213418
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v0, v0

    .line 2213419
    invoke-virtual {p0, v0}, LX/FDq;->b(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;

    move-result-object v5

    .line 2213420
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2213421
    iget-object v11, p0, LX/FDq;->e:LX/2N6;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-wide v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-virtual {v11, v0, v1}, LX/2N6;->a(J)V

    .line 2213422
    :cond_1
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v0

    .line 2213423
    iput-object v3, v0, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2213424
    move-object v0, v0

    .line 2213425
    iput-object v4, v0, LX/6iK;->b:LX/6ek;

    .line 2213426
    move-object v0, v0

    .line 2213427
    iput-object v2, v0, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2213428
    move-object v0, v0

    .line 2213429
    iput-object v10, v0, LX/6iK;->d:Ljava/util/List;

    .line 2213430
    move-object v0, v0

    .line 2213431
    iput-object v5, v0, LX/6iK;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 2213432
    move-object v0, v0

    .line 2213433
    iput-wide v6, v0, LX/6iK;->j:J

    .line 2213434
    move-object v0, v0

    .line 2213435
    iput-wide v8, v0, LX/6iK;->k:J

    .line 2213436
    move-object v0, v0

    .line 2213437
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2213438
    const v1, 0x7fddafd9

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2213439
    :cond_2
    :try_start_2
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v3, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2213440
    goto :goto_2

    .line 2213441
    :catchall_0
    move-exception v0

    const v1, 0x33015a37

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/6ek;JI)Ljava/util/LinkedHashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "JI)",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213387
    const-string v0, "DbFetchThreadsHandler.doThreadListQuery"

    const v1, -0x7ebb536

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213388
    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, LX/FDq;->b(LX/6ek;JI)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 2213389
    invoke-direct {p0, v0}, LX/FDq;->b(Ljava/util/LinkedHashMap;)V

    .line 2213390
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 2213391
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6g6;

    .line 2213392
    iget-object v3, v0, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v3

    .line 2213393
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2213394
    :catchall_0
    move-exception v0

    const v1, 0x96cfca6

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, -0x3bb2dffe

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v1
.end method

.method public final b()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213367
    const-string v0, "DbFetchThreadsHandler.fetchGroupThreadsList"

    const v1, -0x1088917e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213368
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2213369
    iget-object v0, p0, LX/FDq;->g:LX/FKD;

    const/4 v2, 0x0

    .line 2213370
    iget-object v3, v0, LX/FKD;->c:LX/3QW;

    invoke-virtual {v3}, LX/3QW;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/FKD;->b:LX/0ad;

    sget-short v4, LX/FKC;->b:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 2213371
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FDq;->d:LX/3Mq;

    const-string v2, "timestamp_ms"

    const/16 v3, 0x3c

    sget-object v4, LX/6ek;->INBOX:LX/6ek;

    .line 2213372
    sget-object v5, LX/5e9;->GROUP:LX/5e9;

    invoke-static {v5, v4}, LX/3Mq;->a(LX/5e9;LX/6ek;)LX/0uw;

    move-result-object v5

    .line 2213373
    const-string v6, "is_joinable"

    const-string p0, "1"

    invoke-static {v6, p0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    invoke-static {v6}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2213374
    invoke-static {v0, v5, v2, v3}, LX/3Mq;->a(LX/3Mq;LX/0ux;Ljava/lang/String;I)LX/6dO;

    move-result-object v5

    move-object v0, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2213375
    :goto_0
    const/4 v2, 0x0

    .line 2213376
    :try_start_1
    invoke-static {v1, v0}, LX/FDq;->a(LX/0Pz;LX/6dO;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213377
    if-eqz v0, :cond_1

    :try_start_2
    invoke-virtual {v0}, LX/6dO;->close()V

    .line 2213378
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 2213379
    const v1, -0x76be207c

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2213380
    :cond_2
    :try_start_3
    iget-object v0, p0, LX/FDq;->d:LX/3Mq;

    const-string v2, "timestamp_ms"

    const/16 v3, 0x3c

    sget-object v4, LX/6ek;->INBOX:LX/6ek;

    .line 2213381
    sget-object v5, LX/5e9;->GROUP:LX/5e9;

    invoke-static {v5, v4}, LX/3Mq;->a(LX/5e9;LX/6ek;)LX/0uw;

    move-result-object v5

    .line 2213382
    invoke-static {v0, v5, v2, v3}, LX/3Mq;->a(LX/3Mq;LX/0ux;Ljava/lang/String;I)LX/6dO;

    move-result-object v5

    move-object v0, v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2213383
    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2213384
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_3

    if-eqz v2, :cond_4

    :try_start_5
    invoke-virtual {v0}, LX/6dO;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_3
    :goto_1
    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2213385
    :catchall_1
    move-exception v0

    const v1, -0x62711981

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2213386
    :catch_1
    move-exception v0

    :try_start_7
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, LX/6dO;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1
.end method

.method public final b(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2213355
    const-string v0, "DbFetchThreadsHandler.getFolderCounts"

    const v1, 0x737cf7b8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213356
    :try_start_0
    const-string v0, "folder"

    iget-object v1, p1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2213357
    iget-object v0, p0, LX/FDq;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2213358
    const-string v1, "folder_counts"

    sget-object v2, LX/FDq;->h:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    .line 2213359
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2213360
    new-instance v1, Lcom/facebook/messaging/model/folders/FolderCounts;

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v0, 0x3

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/model/folders/FolderCounts;-><init>(IIJJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213361
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213362
    const v0, 0x202e83c3

    invoke-static {v0}, LX/02m;->a(I)V

    :goto_0
    return-object v1

    .line 2213363
    :cond_0
    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2213364
    const v0, 0x73fc1f73

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v1, v8

    goto :goto_0

    .line 2213365
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2213366
    :catchall_1
    move-exception v0

    const v1, 0x1cc3eb5b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213340
    const-string v0, "DbFetchThreadsHandler.fetchRoomThreadsList"

    const v1, -0x16af2752

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213341
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2213342
    iget-object v1, p0, LX/FDq;->d:LX/3Mq;

    const-string v2, "timestamp_ms"

    const/16 v3, 0x3c

    sget-object v4, LX/6ek;->INBOX:LX/6ek;

    .line 2213343
    sget-object v5, LX/5e9;->GROUP:LX/5e9;

    invoke-static {v5, v4}, LX/3Mq;->a(LX/5e9;LX/6ek;)LX/0uw;

    move-result-object v5

    .line 2213344
    const-string v6, "is_joinable"

    const-string p0, "1"

    invoke-static {v6, p0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2213345
    invoke-static {v1, v5, v2, v3}, LX/3Mq;->a(LX/3Mq;LX/0ux;Ljava/lang/String;I)LX/6dO;

    move-result-object v5

    move-object v2, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2213346
    const/4 v1, 0x0

    .line 2213347
    :try_start_1
    invoke-static {v0, v2}, LX/FDq;->a(LX/0Pz;LX/6dO;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213348
    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, LX/6dO;->close()V

    .line 2213349
    :cond_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 2213350
    const v1, -0x31b35e54

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2213351
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2213352
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v2}, LX/6dO;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_1
    :goto_0
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2213353
    :catchall_1
    move-exception v0

    const v1, -0x52ebd2d3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2213354
    :catch_1
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, LX/6dO;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0
.end method
