.class public LX/Gpd;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/FooterBlockView;",
        "Lcom/facebook/instantshopping/model/data/FooterBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:LX/GoP;

.field private e:LX/GoN;

.field public f:Z


# direct methods
.method public constructor <init>(LX/GqK;LX/GoP;)V
    .locals 0

    .prologue
    .line 2395252
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395253
    iput-object p2, p0, LX/Gpd;->d:LX/GoP;

    .line 2395254
    return-void
.end method

.method private b(LX/8Z4;)LX/Clf;
    .locals 2

    .prologue
    .line 2395367
    new-instance v0, LX/Cle;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object v0

    const v1, 0x7f0e0a15

    invoke-virtual {v0, v1}, LX/Cle;->a(I)LX/Cle;

    move-result-object v0

    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2395368
    invoke-virtual {p0}, LX/Gpd;->b()V

    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2395348
    iput-boolean p1, p0, LX/Gpd;->f:Z

    .line 2395349
    iget-boolean v0, p0, LX/Gpd;->f:Z

    if-eqz v0, :cond_0

    .line 2395350
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395351
    check-cast v0, LX/GqK;

    iget-object v1, p0, LX/Gpd;->e:LX/GoN;

    .line 2395352
    iget-object v2, v1, LX/GoN;->a:LX/CHm;

    invoke-interface {v2}, LX/CHl;->z()LX/8Z4;

    move-result-object v2

    move-object v1, v2

    .line 2395353
    invoke-direct {p0, v1}, LX/Gpd;->b(LX/8Z4;)LX/Clf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GqK;->a(LX/Clf;)V

    .line 2395354
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395355
    check-cast v0, LX/GqK;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/GqK;->a(I)V

    .line 2395356
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395357
    check-cast v0, LX/GqK;

    const v1, 0x7f020db6

    invoke-virtual {v0, v1}, LX/GqK;->b(I)V

    .line 2395358
    :goto_0
    return-void

    .line 2395359
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395360
    check-cast v0, LX/GqK;

    iget-object v1, p0, LX/Gpd;->e:LX/GoN;

    .line 2395361
    iget-object v2, v1, LX/GoN;->a:LX/CHm;

    invoke-interface {v2}, LX/CHl;->y()LX/8Z4;

    move-result-object v2

    move-object v1, v2

    .line 2395362
    invoke-direct {p0, v1}, LX/Gpd;->b(LX/8Z4;)LX/Clf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GqK;->a(LX/Clf;)V

    .line 2395363
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395364
    check-cast v0, LX/GqK;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a07b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/GqK;->a(I)V

    .line 2395365
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395366
    check-cast v0, LX/GqK;

    const v1, 0x7f020db5

    invoke-virtual {v0, v1}, LX/GqK;->b(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 2395255
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395256
    check-cast v0, LX/GqK;

    .line 2395257
    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2395258
    iget-object v0, p0, LX/Gpd;->d:LX/GoP;

    .line 2395259
    iget-object v1, v0, LX/GoP;->a:LX/0Px;

    if-eqz v1, :cond_3

    .line 2395260
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2395261
    iget-object v1, v0, LX/GoP;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v1, v0, LX/GoP;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$FooterElementsModel;

    .line 2395262
    sget-object v5, LX/GoO;->a:[I

    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$FooterElementsModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2395263
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2395264
    :pswitch_0
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    new-instance v6, LX/GoM;

    invoke-direct {v6, v1}, LX/GoM;-><init>(LX/CHc;)V

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2395265
    :pswitch_1
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    new-instance v6, LX/GoN;

    invoke-direct {v6, v1}, LX/GoN;-><init>(LX/CHm;)V

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2395266
    :cond_0
    move-object v1, v3

    .line 2395267
    :goto_2
    move-object v2, v1

    .line 2395268
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2395269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GoM;

    .line 2395270
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 2395271
    check-cast v1, LX/GqK;

    .line 2395272
    iget-object v3, v1, LX/GqK;->l:Landroid/widget/FrameLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2395273
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 2395274
    check-cast v1, LX/GqK;

    .line 2395275
    iget-object v3, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v3}, LX/CHb;->d()LX/8Z4;

    move-result-object v3

    move-object v3, v3

    .line 2395276
    new-instance v4, LX/Cle;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object v4

    const v5, 0x7f0e0a15

    invoke-virtual {v4, v5}, LX/Cle;->a(I)LX/Cle;

    move-result-object v4

    invoke-virtual {v4}, LX/Cle;->a()LX/Clf;

    move-result-object v4

    move-object v3, v4

    .line 2395277
    invoke-virtual {v0}, LX/GoM;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v4

    .line 2395278
    if-eqz v3, :cond_5

    .line 2395279
    iget-object v5, v1, LX/GqK;->n:LX/CtG;

    invoke-virtual {v5, v3}, LX/CtG;->setText(LX/Clf;)V

    .line 2395280
    iget-object v5, v1, LX/GqK;->n:LX/CtG;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/CtG;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2395281
    iget-object v5, v1, LX/GqK;->j:LX/CIh;

    iget-object v6, v1, LX/GqK;->n:LX/CtG;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;->CENTER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    invoke-virtual {v5, v6, v7, v4}, LX/CIh;->a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V

    .line 2395282
    iget-object v5, v1, LX/GqK;->n:LX/CtG;

    invoke-virtual {v5}, LX/CtG;->setSingleLine()V

    .line 2395283
    :goto_3
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 2395284
    check-cast v1, LX/GqK;

    invoke-virtual {v0}, LX/GoM;->getAction()LX/CHX;

    move-result-object v3

    .line 2395285
    new-instance v4, LX/GoE;

    iget-object v5, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v5}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v6}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v4

    .line 2395286
    new-instance v5, LX/GqH;

    invoke-direct {v5, v1, v3, v4}, LX/GqH;-><init>(LX/GqK;LX/CHX;LX/GoE;)V

    .line 2395287
    iget-object v6, v1, LX/GqK;->l:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v5}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395288
    iget-object v6, v1, LX/GqK;->n:LX/CtG;

    invoke-virtual {v6, v5}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395289
    iget-object v1, v0, LX/GoM;->a:LX/CHc;

    if-eqz v1, :cond_6

    iget-object v1, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v1}, LX/CHc;->j()LX/CHX;

    move-result-object v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2395290
    if-eqz v1, :cond_1

    .line 2395291
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 2395292
    check-cast v1, LX/GqK;

    .line 2395293
    iget-object v3, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v3}, LX/CHb;->jw_()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v3, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v3}, LX/CHb;->jw_()LX/0Px;

    move-result-object v3

    :goto_5
    move-object v3, v3

    .line 2395294
    iget-object v4, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v4}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    .line 2395295
    const/4 v4, -0x1

    .line 2395296
    :goto_6
    move v0, v4

    .line 2395297
    iget-object v4, v1, LX/GqK;->l:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3, v0}, LX/Gpx;->a(Landroid/content/Context;LX/0Px;I)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v5

    invoke-static {v4, v5}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2395298
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2395299
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GoN;

    iput-object v0, p0, LX/Gpd;->e:LX/GoN;

    .line 2395300
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395301
    check-cast v0, LX/GqK;

    .line 2395302
    iget-object v1, v0, LX/GqK;->m:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2395303
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395304
    check-cast v0, LX/GqK;

    iget-object v1, p0, LX/Gpd;->e:LX/GoN;

    .line 2395305
    iget-object v2, v0, LX/GqK;->e:LX/CIb;

    .line 2395306
    iget-object v3, v2, LX/CIb;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2395307
    iget-object v3, v0, LX/GqK;->d:LX/CIe;

    invoke-virtual {v3, v2}, LX/CIe;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2395308
    iget-object v3, v0, LX/GqK;->d:LX/CIe;

    invoke-virtual {v3, v2}, LX/CIe;->a(Ljava/lang/String;)Z

    move-result v2

    .line 2395309
    :goto_7
    move v0, v2

    .line 2395310
    iput-boolean v0, p0, LX/Gpd;->f:Z

    .line 2395311
    iget-boolean v0, p0, LX/Gpd;->f:Z

    invoke-virtual {p0, v0}, LX/Gpd;->a(Z)V

    .line 2395312
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395313
    check-cast v0, LX/GqK;

    iget-object v1, p0, LX/Gpd;->e:LX/GoN;

    .line 2395314
    iget-object v2, v1, LX/GoN;->a:LX/CHm;

    invoke-interface {v2}, LX/CHm;->A()LX/CHX;

    move-result-object v2

    move-object v1, v2

    .line 2395315
    iget-object v2, p0, LX/Gpd;->e:LX/GoN;

    .line 2395316
    iget-object v3, v2, LX/GoN;->a:LX/CHm;

    invoke-interface {v3}, LX/CHm;->B()LX/CHX;

    move-result-object v3

    move-object v2, v3

    .line 2395317
    iget-object v3, p0, LX/Gpd;->e:LX/GoN;

    .line 2395318
    new-instance v4, LX/GoE;

    iget-object v5, v3, LX/GoN;->a:LX/CHm;

    invoke-interface {v5}, LX/CHl;->jt_()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v3, LX/GoN;->a:LX/CHm;

    invoke-interface {v6}, LX/CHl;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    .line 2395319
    new-instance v4, LX/GqJ;

    invoke-direct {v4, v0, v2, v1, v3}, LX/GqJ;-><init>(LX/GqK;LX/CHX;LX/CHX;LX/GoE;)V

    .line 2395320
    iget-object v5, v0, LX/GqK;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395321
    iget-object v5, v0, LX/GqK;->o:LX/CtG;

    invoke-virtual {v5, v4}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395322
    iget-boolean v0, p0, LX/Gpd;->f:Z

    if-nez v0, :cond_2

    .line 2395323
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395324
    check-cast v0, LX/GqK;

    .line 2395325
    iget-object v1, v0, LX/GqK;->g:LX/0ad;

    sget-char v2, LX/CHN;->r:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2395326
    iget-object v2, v0, LX/GqK;->k:LX/Gqm;

    if-eqz v2, :cond_c

    iget-object v2, v0, LX/GqK;->k:LX/Gqm;

    invoke-virtual {v2}, LX/Gqm;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "3903"

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_8
    move v2, v2

    .line 2395327
    if-eqz v2, :cond_2

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2395328
    :cond_2
    :goto_9
    return-void

    .line 2395329
    :cond_3
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2395330
    iget-object v1, v0, LX/GoP;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v3, v1

    :goto_a
    if-ge v3, v4, :cond_4

    iget-object v1, v0, LX/GoP;->b:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$FooterElementsModel;

    .line 2395331
    sget-object v5, LX/GoO;->a:[I

    invoke-interface {v1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 2395332
    :goto_b
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_a

    .line 2395333
    :pswitch_2
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    new-instance v6, LX/GoM;

    invoke-direct {v6, v1}, LX/GoM;-><init>(LX/CHc;)V

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 2395334
    :pswitch_3
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    new-instance v6, LX/GoN;

    invoke-direct {v6, v1}, LX/GoN;-><init>(LX/CHm;)V

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    :cond_4
    move-object v1, v2

    .line 2395335
    goto/16 :goto_2

    .line 2395336
    :cond_5
    iget-object v5, v1, LX/GqK;->l:Landroid/widget/FrameLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2395337
    goto/16 :goto_3

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_8
    iget-object v4, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v4}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v4}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_6

    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, LX/GoM;->a:LX/CHc;

    invoke-interface {v5}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_6

    .line 2395338
    :cond_a
    iget-object v2, v1, LX/GoN;->a:LX/CHm;

    invoke-interface {v2}, LX/CHl;->x()Z

    move-result v2

    move v2, v2

    .line 2395339
    goto/16 :goto_7

    .line 2395340
    :cond_b
    new-instance v2, LX/0hs;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2395341
    invoke-virtual {v2, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2395342
    const/16 v1, 0x1388

    .line 2395343
    iput v1, v2, LX/0hs;->t:I

    .line 2395344
    iget-object v1, v0, LX/GqK;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2395345
    iget-object v1, v0, LX/GqK;->k:LX/Gqm;

    .line 2395346
    iget-object v2, v1, LX/Gqm;->a:LX/3kp;

    invoke-virtual {v2}, LX/3kp;->a()V

    .line 2395347
    iget-object v1, v0, LX/GqK;->i:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    const-string v2, "3903"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
