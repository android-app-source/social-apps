.class public interface abstract LX/FcI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Header:",
        "Landroid/view/View;",
        "Child:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a()LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<THeader;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/CyH;)LX/FcT;
.end method

.method public abstract a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uu;",
            "THeader;",
            "LX/CyH;",
            "Lcom/facebook/search/results/filters/ui/SearchResultFilterExpandableListAdapter$FilterPersistentStateCallback;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uu;",
            "TChild;",
            "LX/CyH;",
            "Lcom/facebook/search/results/filters/definition/SearchResultPageRadioGroupFilterDefinition$OnChooseMoreSelectedListener;",
            "Lcom/facebook/search/results/filters/ui/SearchResultFilterExpandableListAdapter$FilterPersistentStateCallback;",
            ")V"
        }
    .end annotation
.end method

.method public abstract b()LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<TChild;>;"
        }
    .end annotation
.end method
