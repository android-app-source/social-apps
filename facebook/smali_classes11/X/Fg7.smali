.class public LX/Fg7;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final k:LX/Cz4;

.field private static final l:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fba;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fbg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FbR;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cve;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:LX/FbC;

.field private j:LX/FbB;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public m:Z

.field public n:Z

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2269638
    new-instance v0, LX/Cz4;

    .line 2269639
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2269640
    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/Cz4;-><init>(LX/0Px;LX/0us;)V

    sput-object v0, LX/Fg7;->k:LX/Cz4;

    .line 2269641
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sput-object v0, LX/Fg7;->l:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-void
.end method

.method public constructor <init>(LX/FbC;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2269620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2269621
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2269622
    iput-object v0, p0, LX/Fg7;->a:LX/0Ot;

    .line 2269623
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2269624
    iput-object v0, p0, LX/Fg7;->b:LX/0Ot;

    .line 2269625
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2269626
    iput-object v0, p0, LX/Fg7;->c:LX/0Ot;

    .line 2269627
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2269628
    iput-object v0, p0, LX/Fg7;->d:LX/0Ot;

    .line 2269629
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2269630
    iput-object v0, p0, LX/Fg7;->e:LX/0Ot;

    .line 2269631
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2269632
    iput-object v0, p0, LX/Fg7;->f:LX/0Ot;

    .line 2269633
    iput-boolean v1, p0, LX/Fg7;->m:Z

    .line 2269634
    iput-boolean v1, p0, LX/Fg7;->n:Z

    .line 2269635
    iput-boolean v1, p0, LX/Fg7;->o:Z

    .line 2269636
    iput-object p1, p0, LX/Fg7;->i:LX/FbC;

    .line 2269637
    return-void
.end method

.method private static a(LX/Fg7;LX/0Px;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2269616
    iget-boolean v0, p0, LX/Fg7;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;

    if-eqz v0, :cond_0

    .line 2269617
    iput-boolean v2, p0, LX/Fg7;->n:Z

    .line 2269618
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object p1, v0

    .line 2269619
    :cond_0
    return-object p1
.end method

.method private static a(LX/Fg7;LX/CwB;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2269611
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BG;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fg7;->h:LX/0Uh;

    sget v1, LX/2SU;->P:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Fg7;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/Fg7;->n:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2269612
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2269613
    :goto_0
    return-object v0

    .line 2269614
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fg7;->n:Z

    .line 2269615
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;

    invoke-direct {v0, p1}, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;-><init>(LX/CwB;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static declared-synchronized a(LX/Fg7;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2269642
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/Fg7;->b()V

    .line 2269643
    iget-object v0, p0, LX/Fg7;->j:LX/FbB;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-static {p1}, LX/Fbf;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/FbB;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/0RL;

    move-result-object v0

    .line 2269644
    if-eqz v0, :cond_0

    sget-object v1, LX/D0F;->W:LX/CzO;

    iget-object v1, v1, LX/CzO;->d:LX/0RL;

    invoke-virtual {v0, v1}, LX/0RL;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2269645
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2269646
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2269647
    :cond_1
    :try_start_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2269648
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v0

    .line 2269649
    if-eqz v0, :cond_3

    .line 2269650
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;

    .line 2269651
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2269652
    if-eqz v0, :cond_2

    .line 2269653
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aC()Ljava/lang/String;

    move-result-object v0

    .line 2269654
    if-eqz v0, :cond_2

    .line 2269655
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2269656
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2269657
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 2269658
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/Fg7;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;Ljava/lang/String;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchQuery$FilteredQuery$Modules;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2269594
    iget-object v0, p0, LX/Fg7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    sget-object v2, LX/Fg7;->l:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v2}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, LX/7BG;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2269595
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2269596
    :goto_0
    return-object v0

    .line 2269597
    :cond_1
    iget-object v0, p0, LX/Fg7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    sget-object v2, LX/Fg7;->l:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v3, "browse_session_id"

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;LX/0P1;)V

    .line 2269598
    iget-object v0, p0, LX/Fg7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    sget-object v2, LX/Fg7;->l:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0, v2, v1}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 2269599
    iget-object v0, p0, LX/Fg7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    .line 2269600
    iget-object v2, v0, LX/0x9;->i:Ljava/util/Map;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FaZ;

    .line 2269601
    if-eqz v2, :cond_3

    .line 2269602
    new-instance v3, Lcom/facebook/search/results/model/unit/SearchResultsAwarenessUnit;

    iget-object v0, v2, LX/FaZ;->a:LX/CwT;

    .line 2269603
    iget-object v2, v0, LX/CwT;->a:LX/A0Z;

    move-object v0, v2

    .line 2269604
    invoke-direct {v3, v0}, Lcom/facebook/search/results/model/unit/SearchResultsAwarenessUnit;-><init>(LX/A0Z;)V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 2269605
    :goto_1
    move-object v2, v2

    .line 2269606
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, LX/Fg7;->o:Z

    move-object v0, v2

    .line 2269607
    goto :goto_0

    .line 2269608
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 2269609
    :cond_3
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2269610
    goto :goto_1
.end method

.method private static a(LX/Fg7;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;Ljava/lang/String;LX/8ci;Ljava/lang/String;LX/CwB;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchQuery$FilteredQuery$Modules;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "Ljava/lang/String;",
            "LX/CwB;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2269519
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->fH_()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v1

    .line 2269520
    iget-boolean v0, p0, LX/Fg7;->m:Z

    if-nez v0, :cond_1

    .line 2269521
    sget-object v0, LX/8ci;->c:LX/8ci;

    if-eq p3, v0, :cond_0

    sget-object v0, LX/8ci;->e:LX/8ci;

    if-eq p3, v0, :cond_0

    sget-object v0, LX/8ci;->d:LX/8ci;

    if-ne p3, v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2269522
    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-eq v1, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-eq v1, v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;->b()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel$QueryTitleModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2269523
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2269524
    :goto_1
    return-object v0

    .line 2269525
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fg7;->m:Z

    .line 2269526
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;->b()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel$QueryTitleModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel$QueryTitleModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;->a()Ljava/lang/String;

    move-result-object v5

    move-object v2, p2

    move-object v3, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CwB;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Fg7;
    .locals 9

    .prologue
    .line 2269590
    new-instance v0, LX/Fg7;

    const-class v1, LX/FbC;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/FbC;

    invoke-direct {v0, v1}, LX/Fg7;-><init>(LX/FbC;)V

    .line 2269591
    const/16 v1, 0x3312

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3317

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x330c

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1147

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x113f

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32d5

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    .line 2269592
    iput-object v1, v0, LX/Fg7;->a:LX/0Ot;

    iput-object v2, v0, LX/Fg7;->b:LX/0Ot;

    iput-object v3, v0, LX/Fg7;->c:LX/0Ot;

    iput-object v4, v0, LX/Fg7;->d:LX/0Ot;

    iput-object v5, v0, LX/Fg7;->e:LX/0Ot;

    iput-object v6, v0, LX/Fg7;->f:LX/0Ot;

    iput-object v7, v0, LX/Fg7;->g:LX/0ad;

    iput-object v8, v0, LX/Fg7;->h:LX/0Uh;

    .line 2269593
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;LX/CwW;LX/8ci;LX/CwB;)LX/Cz4;
    .locals 19

    .prologue
    .line 2269538
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;

    move-result-object v9

    .line 2269539
    if-nez v9, :cond_0

    .line 2269540
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fg7;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Sc;

    sget-object v3, LX/3Ql;->FETCH_KEYWORD_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v4, "filtered query was null"

    invoke-virtual {v2, v3, v4}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2269541
    sget-object v2, LX/Fg7;->k:LX/Cz4;

    .line 2269542
    :goto_0
    return-object v2

    .line 2269543
    :cond_0
    invoke-virtual {v9}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->d()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    move-result-object v3

    .line 2269544
    if-nez v3, :cond_1

    .line 2269545
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fg7;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Sc;

    sget-object v3, LX/3Ql;->FETCH_KEYWORD_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v4, "modules was null"

    invoke-virtual {v2, v3, v4}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2269546
    sget-object v2, LX/Fg7;->k:LX/Cz4;

    goto :goto_0

    .line 2269547
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->c()LX/0Px;

    move-result-object v10

    .line 2269548
    new-instance v11, LX/17L;

    invoke-direct {v11}, LX/17L;-><init>()V

    .line 2269549
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->d()LX/80n;

    move-result-object v2

    .line 2269550
    if-nez v2, :cond_5

    .line 2269551
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, LX/17L;->a(Z)LX/17L;

    .line 2269552
    :goto_1
    invoke-virtual {v9}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->fG_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$QueryTitleModel;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v9}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->fG_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$QueryTitleModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$QueryTitleModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2269553
    :goto_2
    new-instance v12, LX/0Pz;

    invoke-direct {v12}, LX/0Pz;-><init>()V

    .line 2269554
    invoke-virtual {v9}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->fF_()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2}, LX/Fg7;->a(LX/Fg7;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2269555
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, LX/Fg7;->a(LX/Fg7;LX/CwB;)LX/0Px;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2269556
    invoke-virtual {v9}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->fF_()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    move-object/from16 v7, p4

    invoke-static/range {v2 .. v7}, LX/Fg7;->a(LX/Fg7;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;Ljava/lang/String;LX/8ci;Ljava/lang/String;LX/CwB;)LX/0Px;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2269557
    new-instance v13, LX/0P2;

    invoke-direct {v13}, LX/0P2;-><init>()V

    .line 2269558
    new-instance v14, LX/0P2;

    invoke-direct {v14}, LX/0P2;-><init>()V

    .line 2269559
    new-instance v15, LX/0P2;

    invoke-direct {v15}, LX/0P2;-><init>()V

    .line 2269560
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 2269561
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v17

    const/4 v2, 0x0

    move v8, v2

    :goto_3
    move/from16 v0, v17

    if-ge v8, v0, :cond_d

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;

    .line 2269562
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->e()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v4

    .line 2269563
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->fJ_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    move-result-object v18

    .line 2269564
    if-nez v4, :cond_2

    if-eqz v18, :cond_4

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2269565
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2269566
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fg7;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v3}, LX/FbR;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;)LX/0Px;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2269567
    :cond_3
    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fg7;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v3}, LX/Fbg;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;)LX/0Px;

    move-result-object v7

    .line 2269568
    :goto_4
    invoke-virtual {v12, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2269569
    if-eqz v4, :cond_8

    .line 2269570
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fg7;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cve;

    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->d()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->c()LX/0Px;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, LX/Cve;->a(LX/CwW;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;ILX/0Px;)V

    .line 2269571
    :cond_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_3

    .line 2269572
    :cond_5
    invoke-interface {v2}, LX/80n;->b()Z

    move-result v4

    invoke-virtual {v11, v4}, LX/17L;->a(Z)LX/17L;

    move-result-object v4

    invoke-interface {v2}, LX/80n;->c()Z

    move-result v5

    invoke-virtual {v4, v5}, LX/17L;->b(Z)LX/17L;

    move-result-object v4

    invoke-interface {v2}, LX/80n;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/17L;->b(Ljava/lang/String;)LX/17L;

    move-result-object v4

    invoke-interface {v2}, LX/80n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/17L;->a(Ljava/lang/String;)LX/17L;

    goto/16 :goto_1

    .line 2269573
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 2269574
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fg7;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fba;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, LX/Fba;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;

    move-result-object v7

    goto :goto_4

    .line 2269575
    :cond_8
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    .line 2269576
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fg7;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cve;

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v5

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, LX/Cve;->a(LX/CwW;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;ILX/0Px;)V

    .line 2269577
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/Fg7;->a(LX/Fg7;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;

    move-result-object v4

    .line 2269578
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2269579
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 2269580
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    invoke-virtual {v13, v2, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2269581
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 2269582
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v14, v2, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2269583
    :cond_9
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->k()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 2269584
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v2, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2269585
    :cond_a
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2269586
    :cond_b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 2269587
    :cond_c
    const/4 v6, 0x0

    goto :goto_5

    .line 2269588
    :cond_d
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2269589
    new-instance v2, LX/Cz4;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/Fg7;->a(LX/Fg7;LX/0Px;)LX/0Px;

    move-result-object v3

    invoke-virtual {v11}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-virtual {v13}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    invoke-virtual {v14}, LX/0P2;->b()LX/0P1;

    move-result-object v6

    invoke-virtual {v15}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    invoke-virtual {v9}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, LX/Cz4;-><init>(LX/0Px;LX/0us;LX/0P1;LX/0P1;LX/0P1;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 2269527
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fg7;->j:LX/FbB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2269528
    :goto_0
    monitor-exit p0

    return-void

    .line 2269529
    :cond_0
    :try_start_1
    const-string v0, "Initializing-SearchResultsSupportDeclaration"

    const v1, 0x266c3f8b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2269530
    sget-object v0, LX/D0F;->W:LX/CzO;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/FbC;->a(LX/0Rf;LX/0RL;)LX/FbB;

    move-result-object v0

    iput-object v0, p0, LX/Fg7;->j:LX/FbB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2269531
    const v0, -0xd35871f

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2269532
    :try_start_3
    const-string v0, "Initializing-FeedUnitMetaFactory"

    const v1, -0x3d5573f6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2269533
    iget-object v0, p0, LX/Fg7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fba;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2269534
    const v0, 0x37746f86

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2269535
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2269536
    :catchall_1
    move-exception v0

    const v1, 0x3438d4f5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2269537
    :catchall_2
    move-exception v0

    const v1, -0x4cd6ece8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
