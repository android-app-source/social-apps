.class public final LX/FkA;
.super LX/EmY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EmY",
        "<",
        "LX/Ema;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FkC;


# direct methods
.method public constructor <init>(LX/FkC;)V
    .locals 0

    .prologue
    .line 2278198
    iput-object p1, p0, LX/FkA;->a:LX/FkC;

    invoke-direct {p0}, LX/EmY;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/Ema;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2278209
    const-class v0, LX/Ema;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2278199
    check-cast p1, LX/Ema;

    .line 2278200
    iget-object v0, p1, LX/EmS;->a:LX/EmM;

    move-object v0, v0

    .line 2278201
    sget-object v1, LX/EmM;->APP_UPDATE:LX/EmM;

    if-eq v0, v1, :cond_0

    .line 2278202
    :goto_0
    return-void

    .line 2278203
    :cond_0
    iget-object v0, p0, LX/FkA;->a:LX/FkC;

    .line 2278204
    iget-object v1, p1, LX/Ema;->c:LX/EmZ;

    move-object v1, v1

    .line 2278205
    invoke-virtual {v1}, LX/EmZ;->name()Ljava/lang/String;

    .line 2278206
    sget-object p0, LX/EmZ;->CREATED_FILE:LX/EmZ;

    if-ne v1, p0, :cond_1

    .line 2278207
    iget-object v1, v0, LX/FkC;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object p0, v0, LX/FkC;->s:Ljava/lang/String;

    invoke-static {p0}, LX/FkE;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2278208
    :cond_1
    goto :goto_0
.end method
