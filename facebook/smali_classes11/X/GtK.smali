.class public final LX/GtK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400524
    iput-object p1, p0, LX/GtK;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2400525
    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2400526
    const/4 v0, 0x0

    .line 2400527
    :goto_0
    return v0

    .line 2400528
    :cond_0
    iget-object v0, p0, LX/GtK;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->N:LX/GiU;

    check-cast p2, Ljava/lang/String;

    sget-object v1, LX/87b;->INTERN_SETTINGS:LX/87b;

    iget-object v2, p0, LX/GtK;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v2, v2, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    .line 2400529
    sget-object p0, LX/0ax;->iw:Ljava/lang/String;

    invoke-static {p0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v1, v2}, LX/GiU;->b(Ljava/lang/String;LX/87b;Landroid/content/Context;)V

    .line 2400530
    const/4 v0, 0x1

    goto :goto_0
.end method
