.class public final LX/FzS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLInterfaces$TypeaheadResultPage$;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2307990
    iput-object p1, p0, LX/FzS;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2307986
    iget-object v0, p0, LX/FzS;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->i:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08003a

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2307987
    iget-object v0, p0, LX/FzS;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    const/4 v1, 0x0

    .line 2307988
    iput-boolean v1, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->r:Z

    .line 2307989
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2307980
    check-cast p1, Ljava/util/List;

    .line 2307981
    iget-object v0, p0, LX/FzS;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2307982
    iget-object v0, p0, LX/FzS;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    invoke-static {v0, p1}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->a$redex0(Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;Ljava/util/List;)V

    .line 2307983
    iget-object v0, p0, LX/FzS;->a:Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;

    const/4 v1, 0x0

    .line 2307984
    iput-boolean v1, v0, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadFragment;->r:Z

    .line 2307985
    :cond_0
    return-void
.end method
