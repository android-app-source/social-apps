.class public final enum LX/FJ8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FJ8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FJ8;

.field public static final enum FADE_IN:LX/FJ8;

.field public static final enum FADE_OUT:LX/FJ8;

.field public static final enum FINISHED:LX/FJ8;

.field public static final enum NORMAL:LX/FJ8;

.field public static final enum NOT_READY:LX/FJ8;

.field public static final enum READY:LX/FJ8;

.field public static final enum WAIT_FOR_NEXT_READY:LX/FJ8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2223111
    new-instance v0, LX/FJ8;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, LX/FJ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FJ8;->NOT_READY:LX/FJ8;

    .line 2223112
    new-instance v0, LX/FJ8;

    const-string v1, "READY"

    invoke-direct {v0, v1, v4}, LX/FJ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FJ8;->READY:LX/FJ8;

    .line 2223113
    new-instance v0, LX/FJ8;

    const-string v1, "FADE_IN"

    invoke-direct {v0, v1, v5}, LX/FJ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FJ8;->FADE_IN:LX/FJ8;

    .line 2223114
    new-instance v0, LX/FJ8;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v6}, LX/FJ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FJ8;->NORMAL:LX/FJ8;

    .line 2223115
    new-instance v0, LX/FJ8;

    const-string v1, "WAIT_FOR_NEXT_READY"

    invoke-direct {v0, v1, v7}, LX/FJ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FJ8;->WAIT_FOR_NEXT_READY:LX/FJ8;

    .line 2223116
    new-instance v0, LX/FJ8;

    const-string v1, "FADE_OUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FJ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FJ8;->FADE_OUT:LX/FJ8;

    .line 2223117
    new-instance v0, LX/FJ8;

    const-string v1, "FINISHED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FJ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FJ8;->FINISHED:LX/FJ8;

    .line 2223118
    const/4 v0, 0x7

    new-array v0, v0, [LX/FJ8;

    sget-object v1, LX/FJ8;->NOT_READY:LX/FJ8;

    aput-object v1, v0, v3

    sget-object v1, LX/FJ8;->READY:LX/FJ8;

    aput-object v1, v0, v4

    sget-object v1, LX/FJ8;->FADE_IN:LX/FJ8;

    aput-object v1, v0, v5

    sget-object v1, LX/FJ8;->NORMAL:LX/FJ8;

    aput-object v1, v0, v6

    sget-object v1, LX/FJ8;->WAIT_FOR_NEXT_READY:LX/FJ8;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FJ8;->FADE_OUT:LX/FJ8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FJ8;->FINISHED:LX/FJ8;

    aput-object v2, v0, v1

    sput-object v0, LX/FJ8;->$VALUES:[LX/FJ8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2223119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FJ8;
    .locals 1

    .prologue
    .line 2223120
    const-class v0, LX/FJ8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FJ8;

    return-object v0
.end method

.method public static values()[LX/FJ8;
    .locals 1

    .prologue
    .line 2223121
    sget-object v0, LX/FJ8;->$VALUES:[LX/FJ8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FJ8;

    return-object v0
.end method
