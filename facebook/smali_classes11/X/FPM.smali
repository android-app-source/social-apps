.class public final LX/FPM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;)V
    .locals 0

    .prologue
    .line 2237029
    iput-object p1, p0, LX/FPM;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 2237030
    iget-object v0, p0, LX/FPM;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/fab/FabView;->getHeight()I

    move-result v1

    .line 2237031
    if-nez v1, :cond_0

    .line 2237032
    :goto_0
    return-void

    .line 2237033
    :cond_0
    iget-object v0, p0, LX/FPM;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/fab/FabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 2237034
    iget-object v0, p0, LX/FPM;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/fab/FabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 2237035
    :goto_1
    iget-object v2, p0, LX/FPM;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->u:Ljava/util/Map;

    sget-object v3, LX/FPN;->HIDE_FAB:LX/FPN;

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237036
    iget-object v0, p0, LX/FPM;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    invoke-static {v0, p0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 2237037
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
