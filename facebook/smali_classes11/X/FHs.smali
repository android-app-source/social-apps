.class public LX/FHs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FHq;",
        "LX/FHr;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FHs;


# instance fields
.field public final a:LX/2Mp;

.field public final b:LX/2ML;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/2Mp;LX/2ML;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221229
    iput-object p1, p0, LX/FHs;->a:LX/2Mp;

    .line 2221230
    iput-object p2, p0, LX/FHs;->b:LX/2ML;

    .line 2221231
    iput-object p3, p0, LX/FHs;->c:LX/0ad;

    .line 2221232
    return-void
.end method

.method public static a(LX/0QB;)LX/FHs;
    .locals 6

    .prologue
    .line 2221141
    sget-object v0, LX/FHs;->d:LX/FHs;

    if-nez v0, :cond_1

    .line 2221142
    const-class v1, LX/FHs;

    monitor-enter v1

    .line 2221143
    :try_start_0
    sget-object v0, LX/FHs;->d:LX/FHs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221144
    if-eqz v2, :cond_0

    .line 2221145
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2221146
    new-instance p0, LX/FHs;

    invoke-static {v0}, LX/2Mp;->a(LX/0QB;)LX/2Mp;

    move-result-object v3

    check-cast v3, LX/2Mp;

    invoke-static {v0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v4

    check-cast v4, LX/2ML;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/FHs;-><init>(LX/2Mp;LX/2ML;LX/0ad;)V

    .line 2221147
    move-object v0, p0

    .line 2221148
    sput-object v0, LX/FHs;->d:LX/FHs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221149
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221150
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221151
    :cond_1
    sget-object v0, LX/FHs;->d:LX/FHs;

    return-object v0

    .line 2221152
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221153
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2221233
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->isQuickCamSource()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/5dT;->QUICKCAM:LX/5dT;

    iget-object v0, v0, LX/5dT;->apiStringValue:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/5dT;->NONQUICKCAM:LX/5dT;

    iget-object v0, v0, LX/5dT;->apiStringValue:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 2221154
    check-cast p1, LX/FHq;

    .line 2221155
    iget-object v0, p1, LX/FHq;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p1, LX/FHq;->f:I

    sub-int/2addr v0, v1

    .line 2221156
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "post_resumable_upload_session"

    .line 2221157
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2221158
    move-object v1, v1

    .line 2221159
    const-string v2, "POST"

    .line 2221160
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2221161
    move-object v1, v1

    .line 2221162
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, LX/FHq;->g:LX/FHg;

    .line 2221163
    iget-object v4, v3, LX/FHg;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2221164
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, LX/FHq;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2221165
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2221166
    move-object v1, v1

    .line 2221167
    const/4 v2, 0x1

    .line 2221168
    iput-boolean v2, v1, LX/14O;->o:Z

    .line 2221169
    move-object v1, v1

    .line 2221170
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2221171
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "Offset"

    iget v7, p1, LX/FHq;->f:I

    int-to-long v8, v7

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221172
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "X-Entity-Length"

    iget-object v7, p1, LX/FHq;->a:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221173
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "X-Entity-Name"

    iget-object v7, p1, LX/FHq;->a:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221174
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "X-Entity-Type"

    iget-object v7, p1, LX/FHq;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v7, v7, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221175
    iget-object v4, p1, LX/FHq;->h:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 2221176
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "X-Entity-Digest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p1, LX/FHq;->i:LX/7yr;

    invoke-virtual {v8}, LX/7yr;->getHeaderPrefix()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, LX/FHq;->h:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221177
    :cond_0
    iget-object v4, p0, LX/FHs;->c:LX/0ad;

    sget-short v6, LX/FGS;->o:S

    const/4 v7, 0x0

    invoke-interface {v4, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    move v4, v4

    .line 2221178
    if-eqz v4, :cond_1

    iget-object v4, p1, LX/FHq;->j:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2221179
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "X-Rupload-Edge-Routing-Dc"

    iget-object v7, p1, LX/FHq;->j:Ljava/lang/String;

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221180
    :cond_1
    iget v4, p1, LX/FHq;->f:I

    if-nez v4, :cond_5

    .line 2221181
    iget-object v6, p1, LX/FHq;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2221182
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->PHOTO:LX/2MK;

    if-ne v4, v7, :cond_6

    .line 2221183
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "image_type"

    invoke-static {v6}, LX/FHs;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221184
    :cond_2
    :goto_0
    sget-object v4, LX/FHf;->PRE_UPLOAD:LX/FHf;

    iget-object v7, p1, LX/FHq;->g:LX/FHg;

    .line 2221185
    iget-object v8, v7, LX/FHg;->a:LX/FHf;

    move-object v7, v8

    .line 2221186
    if-ne v4, v7, :cond_3

    .line 2221187
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "is_temporary"

    const-string v8, "1"

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221188
    :cond_3
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->VIDEO:LX/2MK;

    if-ne v4, v7, :cond_4

    iget-object v4, p1, LX/FHq;->e:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 2221189
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "video_full_duration"

    iget-object v8, p1, LX/FHq;->e:Ljava/lang/String;

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221190
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "video_content_mode"

    const-string v8, "preview"

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221191
    :cond_4
    iget-object v4, p1, LX/FHq;->d:Ljava/lang/String;

    if-eqz v4, :cond_c

    .line 2221192
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v6, LX/2MK;->VIDEO:LX/2MK;

    if-ne v4, v6, :cond_b

    .line 2221193
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "video_original_fbid"

    iget-object v7, p1, LX/FHq;->d:Ljava/lang/String;

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221194
    :cond_5
    :goto_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 2221195
    iput-object v2, v1, LX/14O;->g:Ljava/util/List;

    .line 2221196
    move-object v1, v1

    .line 2221197
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 2221198
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 2221199
    move-object v1, v1

    .line 2221200
    sget-object v2, LX/14R;->FILE_PART_ENTITY:LX/14R;

    .line 2221201
    iput-object v2, v1, LX/14O;->w:LX/14R;

    .line 2221202
    move-object v1, v1

    .line 2221203
    iget-object v2, p1, LX/FHq;->a:Ljava/io/File;

    iget v3, p1, LX/FHq;->f:I

    invoke-virtual {v1, v2, v3, v0}, LX/14O;->a(Ljava/io/File;II)LX/14O;

    move-result-object v0

    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v0, v1}, LX/14O;->a(LX/14P;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2221204
    :cond_6
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->VIDEO:LX/2MK;

    if-ne v4, v7, :cond_7

    .line 2221205
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "video_type"

    .line 2221206
    invoke-static {v6}, LX/6eg;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5dX;

    move-result-object v8

    iget-object v8, v8, LX/5dX;->apiStringValue:Ljava/lang/String;

    move-object v8, v8

    .line 2221207
    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2221208
    :cond_7
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    if-ne v4, v7, :cond_8

    .line 2221209
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "encrypted_blob"

    const-string v8, "FILE_ATTACHMENT"

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2221210
    :cond_8
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->ENT_PHOTO:LX/2MK;

    if-ne v4, v7, :cond_9

    .line 2221211
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "image_type"

    invoke-static {v6}, LX/FHs;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221212
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "use_ent_photo"

    const-string v8, "1"

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2221213
    :cond_9
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->AUDIO:LX/2MK;

    if-ne v4, v7, :cond_2

    .line 2221214
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "audio_type"

    const-string v8, "VOICE_MESSAGE"

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221215
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "duration"

    iget-wide v8, v6, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221216
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "is_voicemail"

    iget-boolean v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    if-eqz v4, :cond_a

    const-string v4, "1"

    :goto_2
    invoke-direct {v7, v8, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221217
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "call_id"

    iget-object v8, v6, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2221218
    :cond_a
    const-string v4, "0"

    goto :goto_2

    .line 2221219
    :cond_b
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "original_fbid"

    iget-object v7, p1, LX/FHq;->d:Ljava/lang/String;

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 2221220
    :cond_c
    iget-object v4, p0, LX/FHs;->a:LX/2Mp;

    invoke-virtual {v4, v6}, LX/2Mp;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v4

    if-eqz v4, :cond_d

    iget-object v4, p0, LX/FHs;->b:LX/2ML;

    invoke-virtual {v4, v6}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2221221
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "media_hash"

    iget-object v8, p0, LX/FHs;->b:LX/2ML;

    invoke-virtual {v8, v6}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221222
    :cond_d
    iget-object v4, v6, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 2221223
    if-eqz v4, :cond_5

    .line 2221224
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "attribution_app_id"

    iget-object v8, v4, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221225
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "android_key_hash"

    iget-object v8, v4, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221226
    iget-object v6, v4, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 2221227
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "attribution_app_metadata"

    iget-object v4, v4, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    invoke-direct {v6, v7, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2221133
    check-cast p1, LX/FHq;

    .line 2221134
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2221135
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2221136
    new-instance v1, LX/FHr;

    const-string v2, "id"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2221137
    iget-object v3, p1, LX/FHq;->g:LX/FHg;

    .line 2221138
    iget-object p1, v3, LX/FHg;->c:Ljava/lang/String;

    move-object v3, p1

    .line 2221139
    move-object v3, v3

    .line 2221140
    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mac"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v0, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/FHr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method
