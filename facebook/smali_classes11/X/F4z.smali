.class public LX/F4z;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field public a:LX/F53;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;

.field public c:Lcom/facebook/fig/listitem/FigListItem;

.field public d:Lcom/facebook/fig/listitem/FigListItem;

.field public e:Lcom/facebook/fig/listitem/FigListItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2197832
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2197833
    const-class v0, LX/F4z;

    invoke-static {v0, p0}, LX/F4z;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2197834
    invoke-virtual {p0}, LX/F4z;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030600

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2197835
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/F4z;->setOrientation(I)V

    .line 2197836
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, LX/F4z;->setBackgroundResource(I)V

    .line 2197837
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {p0, v0}, LX/F4z;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 2197838
    iget-object v0, p0, LX/F4z;->a:LX/F53;

    invoke-virtual {v0, p0}, LX/F53;->addObserver(Ljava/util/Observer;)V

    .line 2197839
    const v0, 0x7f0d107b

    invoke-virtual {p0, v0}, LX/F4z;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;

    iput-object v0, p0, LX/F4z;->b:Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;

    .line 2197840
    const v0, 0x7f0d1079

    invoke-virtual {p0, v0}, LX/F4z;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/F4z;->c:Lcom/facebook/fig/listitem/FigListItem;

    .line 2197841
    const v0, 0x7f0d107a

    invoke-virtual {p0, v0}, LX/F4z;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/F4z;->d:Lcom/facebook/fig/listitem/FigListItem;

    .line 2197842
    const v0, 0x7f0d107c

    invoke-virtual {p0, v0}, LX/F4z;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/F4z;->e:Lcom/facebook/fig/listitem/FigListItem;

    .line 2197843
    iget-object v0, p0, LX/F4z;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, LX/F4z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020328

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2197844
    iget-object v0, p0, LX/F4z;->d:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, LX/F4z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020bfd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2197845
    iget-object v0, p0, LX/F4z;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, LX/F4z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2197846
    new-instance v0, LX/F4w;

    invoke-direct {v0, p0}, LX/F4w;-><init>(LX/F4z;)V

    .line 2197847
    new-instance v1, LX/F4x;

    invoke-direct {v1, p0}, LX/F4x;-><init>(LX/F4z;)V

    .line 2197848
    new-instance v2, LX/F4y;

    invoke-direct {v2, p0}, LX/F4y;-><init>(LX/F4z;)V

    .line 2197849
    iget-object p1, p0, LX/F4z;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-static {p1, v0}, LX/F4z;->a(Lcom/facebook/fig/listitem/FigListItem;Landroid/view/View$OnClickListener;)V

    .line 2197850
    iget-object v0, p0, LX/F4z;->d:Lcom/facebook/fig/listitem/FigListItem;

    invoke-static {v0, v1}, LX/F4z;->a(Lcom/facebook/fig/listitem/FigListItem;Landroid/view/View$OnClickListener;)V

    .line 2197851
    iget-object v0, p0, LX/F4z;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-static {v0, v2}, LX/F4z;->a(Lcom/facebook/fig/listitem/FigListItem;Landroid/view/View$OnClickListener;)V

    .line 2197852
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2197853
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_0

    .line 2197854
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2197855
    :cond_0
    return-object p0
.end method

.method private a(Lcom/facebook/fig/listitem/FigListItem;)V
    .locals 4

    .prologue
    .line 2197856
    invoke-virtual {p1}, Lcom/facebook/fig/listitem/FigListItem;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/F4z;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2197857
    invoke-virtual {p1}, Lcom/facebook/fig/listitem/FigListItem;->getMetaText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/F4z;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2197858
    invoke-virtual {p0}, LX/F4z;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/fig/listitem/FigListItem;->getActionState()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f082f75

    :goto_0
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2197859
    const-string v3, "%s %s %s"

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2197860
    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2197861
    return-void

    .line 2197862
    :cond_0
    const v0, 0x7f082f76

    goto :goto_0
.end method

.method public static a(Lcom/facebook/fig/listitem/FigListItem;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2197863
    invoke-virtual {p0, p1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197864
    invoke-virtual {p0, p1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2197865
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/F4z;

    invoke-static {p0}, LX/F53;->a(LX/0QB;)LX/F53;

    move-result-object p0

    check-cast p0, LX/F53;

    iput-object p0, p1, LX/F4z;->a:LX/F53;

    return-void
.end method


# virtual methods
.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2197866
    iget-object v0, p0, LX/F4z;->a:LX/F53;

    .line 2197867
    iget-object v2, v0, LX/F53;->j:Ljava/lang/String;

    move-object v2, v2

    .line 2197868
    const-string v0, "REAL_WORLD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2197869
    if-eqz v3, :cond_1

    move v0, v1

    .line 2197870
    :goto_0
    iget-object v4, p0, LX/F4z;->b:Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;

    invoke-virtual {v4, v0}, Lcom/facebook/groups/reliable/ReliableGroupExpectationsView;->setVisibility(I)V

    .line 2197871
    iget-object v0, p0, LX/F4z;->c:Lcom/facebook/fig/listitem/FigListItem;

    sget-object v4, LX/F53;->a:Ljava/lang/String;

    if-ne v4, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2197872
    iget-object v0, p0, LX/F4z;->d:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2197873
    iget-object v0, p0, LX/F4z;->e:Lcom/facebook/fig/listitem/FigListItem;

    const-string v1, "FOR_SALE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2197874
    iget-object v0, p0, LX/F4z;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-direct {p0, v0}, LX/F4z;->a(Lcom/facebook/fig/listitem/FigListItem;)V

    .line 2197875
    iget-object v0, p0, LX/F4z;->d:Lcom/facebook/fig/listitem/FigListItem;

    invoke-direct {p0, v0}, LX/F4z;->a(Lcom/facebook/fig/listitem/FigListItem;)V

    .line 2197876
    iget-object v0, p0, LX/F4z;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-direct {p0, v0}, LX/F4z;->a(Lcom/facebook/fig/listitem/FigListItem;)V

    .line 2197877
    return-void

    .line 2197878
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method
