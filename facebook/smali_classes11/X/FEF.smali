.class public final LX/FEF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2216154
    iput-object p1, p0, LX/FEF;->b:Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;

    iput-object p2, p0, LX/FEF;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 2216155
    iget-object v0, p0, LX/FEF;->b:Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;->n:LX/FEG;

    if-nez v0, :cond_0

    .line 2216156
    :goto_0
    return-void

    .line 2216157
    :cond_0
    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/event/sending/EventMessageParams;)V
    .locals 2

    .prologue
    .line 2216158
    iget-object v0, p0, LX/FEF;->b:Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;

    .line 2216159
    iput-object p1, v0, Lcom/facebook/messaging/event/sending/EventSendingDialogFragment;->o:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216160
    iget-object v1, p0, LX/FEF;->a:Landroid/view/View;

    .line 2216161
    iget-object v0, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2216162
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2216163
    iget-object v0, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->c:Ljava/util/Calendar;

    move-object v0, v0

    .line 2216164
    if-eqz v0, :cond_1

    .line 2216165
    iget-object v0, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->d:Ljava/util/Calendar;

    move-object v0, v0

    .line 2216166
    if-eqz v0, :cond_1

    .line 2216167
    iget-object v0, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->e:Lcom/facebook/android/maps/model/LatLng;

    move-object v0, v0

    .line 2216168
    if-nez v0, :cond_0

    .line 2216169
    iget-object v0, p1, Lcom/facebook/messaging/event/sending/EventMessageParams;->f:Lcom/facebook/messaging/location/sending/NearbyPlace;

    move-object v0, v0

    .line 2216170
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 2216171
    return-void

    .line 2216172
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
