.class public final LX/Fdf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;)V
    .locals 0

    .prologue
    .line 2263873
    iput-object p1, p0, LX/Fdf;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x66f16531

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263874
    iget-object v1, p0, LX/Fdf;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    .line 2263875
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v2

    .line 2263876
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v2, p0, LX/Fdf;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    iget-object v2, v2, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->o:LX/5uu;

    iget-object v3, p0, LX/Fdf;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    .line 2263877
    new-instance v5, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    invoke-direct {v5}, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;-><init>()V

    .line 2263878
    const/4 p0, 0x2

    const p1, 0x7f0e0895

    invoke-virtual {v5, p0, p1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2263879
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2263880
    const-string p1, "main_filter"

    invoke-static {p0, p1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2263881
    invoke-virtual {v5, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2263882
    iput-object v3, v5, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->q:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    .line 2263883
    move-object v2, v5

    .line 2263884
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2263885
    const v1, -0x5a1fbbc6

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
