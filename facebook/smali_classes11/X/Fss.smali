.class public final LX/Fss;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FsE;

.field public final synthetic b:Ljava/lang/ref/WeakReference;

.field public final synthetic c:LX/Fsw;


# direct methods
.method public constructor <init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .prologue
    .line 2297693
    iput-object p1, p0, LX/Fss;->c:LX/Fsw;

    iput-object p2, p0, LX/Fss;->a:LX/FsE;

    iput-object p3, p0, LX/Fss;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2297682
    iget-object v0, p0, LX/Fss;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297683
    iput-boolean v1, v0, LX/FsE;->f:Z

    .line 2297684
    iget-object v0, p0, LX/Fss;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/TimelineFragment;

    .line 2297685
    if-eqz v0, :cond_0

    .line 2297686
    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->J()V

    .line 2297687
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2297688
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297689
    iget-object v0, p0, LX/Fss;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297690
    iput-boolean v1, v0, LX/FsE;->f:Z

    .line 2297691
    iget-object v0, p0, LX/Fss;->b:Ljava/lang/ref/WeakReference;

    invoke-static {p1, v0}, LX/Fsw;->d(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/ref/WeakReference;)V

    .line 2297692
    return-void
.end method
