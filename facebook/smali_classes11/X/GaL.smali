.class public LX/GaL;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/common/internalprefhelpers/GkManualUpdater;

.field private final b:LX/0kL;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/common/internalprefhelpers/GkManualUpdater;LX/0kL;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/common/internalprefhelpers/IsGkRefresherSessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/internalprefhelpers/GkManualUpdater;",
            "LX/0kL;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2368210
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 2368211
    iput-object p1, p0, LX/GaL;->a:Lcom/facebook/common/internalprefhelpers/GkManualUpdater;

    .line 2368212
    iput-object p2, p0, LX/GaL;->b:LX/0kL;

    .line 2368213
    iput-object p3, p0, LX/GaL;->c:LX/0Or;

    .line 2368214
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2368215
    :try_start_0
    iget-object v1, p0, LX/GaL;->a:Lcom/facebook/common/internalprefhelpers/GkManualUpdater;

    iget-object v0, p0, LX/GaL;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/common/internalprefhelpers/GkManualUpdater;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2368216
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2368217
    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2368218
    check-cast p1, Ljava/lang/Boolean;

    .line 2368219
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2368220
    iget-object v0, p0, LX/GaL;->b:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Gks refreshed"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2368221
    :goto_0
    return-void

    .line 2368222
    :cond_0
    iget-object v0, p0, LX/GaL;->b:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Failure refreshing gks"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public final onPreExecute()V
    .locals 3

    .prologue
    .line 2368223
    iget-object v0, p0, LX/GaL;->b:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Refreshing gks"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2368224
    return-void
.end method
