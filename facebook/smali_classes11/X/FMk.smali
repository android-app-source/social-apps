.class public LX/FMk;
.super LX/0te;
.source ""


# instance fields
.field public a:LX/FMj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2230588
    invoke-direct {p0}, LX/0te;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/FMk;

    invoke-static {v0}, LX/FMj;->a(LX/0QB;)LX/FMj;

    move-result-object v0

    check-cast v0, LX/FMj;

    iput-object v0, p0, LX/FMk;->a:LX/FMj;

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2230589
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x5ba1c962

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2230590
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2230591
    invoke-static {p0, p0}, LX/FMk;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2230592
    iget-object v1, p0, LX/FMk;->a:LX/FMj;

    .line 2230593
    iput-object p0, v1, LX/FMj;->x:Landroid/app/Service;

    .line 2230594
    const/16 v1, 0x25

    const v2, -0x2aa927a9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x792f9826

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2230595
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2230596
    const/16 v1, 0x25

    const v2, 0x4bbe91c5    # 2.4978314E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x170c1c50

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2230597
    iget-object v1, p0, LX/FMk;->a:LX/FMj;

    invoke-virtual {v1}, LX/FMj;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 2230598
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2230599
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 2230600
    iget-object v2, p0, LX/FMk;->a:LX/FMj;

    invoke-virtual {v2, v1}, LX/FMj;->sendMessage(Landroid/os/Message;)Z

    .line 2230601
    const/16 v1, 0x25

    const v2, -0x32e3e12b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method
