.class public LX/H9v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field public e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2434720
    const v0, 0x7f0209c5

    sput v0, LX/H9v;->a:I

    .line 2434721
    const v0, 0x7f0817a3

    sput v0, LX/H9v;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p2    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434723
    iput-object p1, p0, LX/H9v;->c:LX/0Ot;

    .line 2434724
    iput-object p2, p0, LX/H9v;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434725
    iput-object p3, p0, LX/H9v;->d:Landroid/content/Context;

    .line 2434726
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    .line 2434727
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9v;->b:I

    sget v3, LX/H9v;->a:I

    const/4 v4, 0x1

    .line 2434728
    iget-object v5, p0, LX/H9v;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/8A4;->a(LX/0Px;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, LX/H9v;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->r()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    move v5, v5

    .line 2434729
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434730
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9v;->b:I

    sget v3, LX/H9v;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2434731
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2434732
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2434733
    const-string v0, "android.intent.extra.TEXT"

    iget-object v2, p0, LX/H9v;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2434734
    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2434735
    iget-object v0, p0, LX/H9v;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H9v;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0817a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, LX/H9v;->d:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2434736
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434737
    const/4 v0, 0x0

    return-object v0
.end method
