.class public final LX/FgB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:I

.field public final synthetic b:I

.field public final synthetic c:LX/CwB;

.field public final synthetic d:LX/8ci;

.field public final synthetic e:LX/FgF;


# direct methods
.method public constructor <init>(LX/FgF;ILX/CwB;LX/8ci;)V
    .locals 1

    .prologue
    .line 2269687
    iput-object p1, p0, LX/FgB;->e:LX/FgF;

    iput p2, p0, LX/FgB;->b:I

    iput-object p3, p0, LX/FgB;->c:LX/CwB;

    iput-object p4, p0, LX/FgB;->d:LX/8ci;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2269688
    iget v0, p0, LX/FgB;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/FgB;->a:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2269689
    iget-object v0, p0, LX/FgB;->e:LX/FgF;

    invoke-virtual {v0}, LX/FgF;->d()V

    .line 2269690
    iget-object v0, p0, LX/FgB;->e:LX/FgF;

    invoke-static {v0}, LX/FgF;->h(LX/FgF;)V

    .line 2269691
    iget-object v0, p0, LX/FgB;->e:LX/FgF;

    .line 2269692
    invoke-virtual {v0}, LX/FgF;->c()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2269693
    :goto_0
    return-void

    .line 2269694
    :cond_0
    iget-object p0, v0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-virtual {p0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->c()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2269695
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2269696
    iget v0, p0, LX/FgB;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/FgB;->a:I

    .line 2269697
    iget v0, p0, LX/FgB;->a:I

    if-nez v0, :cond_0

    .line 2269698
    iget-object v0, p0, LX/FgB;->e:LX/FgF;

    const/4 v1, 0x0

    .line 2269699
    iput-boolean v1, v0, LX/FgF;->y:Z

    .line 2269700
    :cond_0
    iget-object v0, p0, LX/FgB;->e:LX/FgF;

    const-string v1, "batch_request_task_key_not_used"

    iget-object v2, p0, LX/FgB;->c:LX/CwB;

    const/4 v3, 0x0

    .line 2269701
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2269702
    check-cast v4, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;

    sget-object v5, LX/CwW;->STREAMING:LX/CwW;

    iget-object v6, p0, LX/FgB;->d:LX/8ci;

    .line 2269703
    iget-object v7, p1, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v7, v7

    .line 2269704
    invoke-static/range {v0 .. v7}, LX/FgF;->a$redex0(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;LX/CwW;LX/8ci;Ljava/lang/String;)V

    .line 2269705
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2269706
    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    .line 2269707
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2269708
    iget-object v0, p0, LX/FgB;->e:LX/FgF;

    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->RESULTS_DATA_LOADER_ERROR:LX/3Ql;

    invoke-direct {v1, v2, p1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/Throwable;)V

    invoke-static {v0, v1}, LX/FgF;->a$redex0(LX/FgF;LX/7C4;)V

    .line 2269709
    return-void
.end method
