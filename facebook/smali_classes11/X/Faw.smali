.class public LX/Faw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FaB;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13B;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/CwT;

.field public e:Landroid/view/View;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public constructor <init>(LX/CwT;)V
    .locals 1
    .param p1    # LX/CwT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259408
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259409
    iput-object v0, p0, LX/Faw;->a:LX/0Ot;

    .line 2259410
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259411
    iput-object v0, p0, LX/Faw;->b:LX/0Ot;

    .line 2259412
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259413
    iput-object v0, p0, LX/Faw;->c:LX/0Ot;

    .line 2259414
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259415
    iput-object p1, p0, LX/Faw;->d:LX/CwT;

    .line 2259416
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Faw;->f:Z

    .line 2259417
    return-void
.end method


# virtual methods
.method public final a(LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2259387
    iget-object v0, p0, LX/Faw;->e:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2259388
    :goto_0
    return-void

    .line 2259389
    :cond_0
    new-instance v1, LX/Fau;

    iget-object v0, p0, LX/Faw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/Fau;-><init>(Landroid/content/Context;)V

    .line 2259390
    iget-object v0, p0, LX/Faw;->d:LX/CwT;

    .line 2259391
    iget-object v2, v0, LX/CwT;->a:LX/A0Z;

    move-object v0, v2

    .line 2259392
    invoke-interface {v0}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2259393
    invoke-interface {v0}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2259394
    sget-object v0, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v0}, LX/0ht;->a(LX/3AV;)V

    .line 2259395
    const/4 v0, -0x1

    .line 2259396
    iput v0, v1, LX/0hs;->t:I

    .line 2259397
    iget-object v0, p0, LX/Faw;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2259398
    new-instance v0, LX/Fav;

    invoke-direct {v0, p0}, LX/Fav;-><init>(LX/Faw;)V

    .line 2259399
    iput-object v0, v1, LX/0ht;->H:LX/2dD;

    .line 2259400
    iget-object v0, p0, LX/Faw;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    iget-object v1, p0, LX/Faw;->d:LX/CwT;

    invoke-virtual {v0, v1, p1}, LX/13B;->a(LX/CwT;LX/0P1;)V

    .line 2259401
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Faw;->f:Z

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 2259402
    const/4 v1, 0x0

    .line 2259403
    iget-object v0, p0, LX/Faw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->ae:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Faw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->af:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2259404
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Faw;->d:LX/CwT;

    .line 2259405
    iget-boolean v1, v0, LX/CwT;->e:Z

    move v0, v1

    .line 2259406
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2259384
    iget-object v0, p0, LX/Faw;->d:LX/CwT;

    const/4 v1, 0x0

    .line 2259385
    iput-boolean v1, v0, LX/CwT;->e:Z

    .line 2259386
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2259380
    iget-boolean v0, p0, LX/Faw;->f:Z

    if-eqz v0, :cond_0

    .line 2259381
    iget-object v0, p0, LX/Faw;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    iget-object v1, p0, LX/Faw;->d:LX/CwT;

    invoke-virtual {v0, v1}, LX/13B;->a(LX/CwT;)V

    .line 2259382
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Faw;->f:Z

    .line 2259383
    :cond_0
    return-void
.end method
