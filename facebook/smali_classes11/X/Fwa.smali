.class public final LX/Fwa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fwb;


# direct methods
.method public constructor <init>(LX/Fwb;)V
    .locals 0

    .prologue
    .line 2303338
    iput-object p1, p0, LX/Fwa;->a:LX/Fwb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x76d02ee8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2303339
    iget-object v0, p0, LX/Fwa;->a:LX/Fwb;

    iget-object v0, v0, LX/Fwb;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    iget-object v2, p0, LX/Fwa;->a:LX/Fwb;

    iget-object v2, v2, LX/Fwb;->j:LX/5SB;

    .line 2303340
    iget-wide v5, v2, LX/5SB;->b:J

    move-wide v2, v5

    .line 2303341
    const/4 v9, 0x0

    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "links_edit_click"

    move-object v6, v0

    move-wide v7, v2

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2303342
    if-eqz v5, :cond_0

    .line 2303343
    iget-object v6, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2303344
    :cond_0
    iget-object v0, p0, LX/Fwa;->a:LX/Fwb;

    iget-object v0, v0, LX/Fwb;->c:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->ll_()V

    .line 2303345
    iget-object v0, p0, LX/Fwa;->a:LX/Fwb;

    iget-object v0, v0, LX/Fwb;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->bK:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2303346
    const v0, 0x727aa63a

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
