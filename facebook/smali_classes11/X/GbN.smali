.class public final LX/GbN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Z

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Lcom/facebook/auth/credentials/DBLFacebookCredentials;ZLjava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2369426
    iput-object p1, p0, LX/GbN;->f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iput-object p2, p0, LX/GbN;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-boolean p3, p0, LX/GbN;->b:Z

    iput-object p4, p0, LX/GbN;->c:Ljava/lang/String;

    iput-boolean p5, p0, LX/GbN;->d:Z

    iput-object p6, p0, LX/GbN;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2369427
    iget-object v0, p0, LX/GbN;->f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-boolean v1, p0, LX/GbN;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, LX/GbN;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a$redex0(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2369428
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2369429
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2369430
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369431
    if-eqz v0, :cond_4

    .line 2369432
    iget-object v1, p0, LX/GbN;->f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->l:LX/10M;

    invoke-virtual {v1, v0}, LX/10M;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2369433
    iget-object v1, p0, LX/GbN;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    if-eqz v1, :cond_0

    .line 2369434
    iget-object v1, p0, LX/GbN;->f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v2, p0, LX/GbN;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a$redex0(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Z)V

    .line 2369435
    :cond_0
    iget-boolean v1, p0, LX/GbN;->b:Z

    if-eqz v1, :cond_2

    .line 2369436
    iget-object v1, p0, LX/GbN;->f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v2, p0, LX/GbN;->c:Ljava/lang/String;

    .line 2369437
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2369438
    iget-object v4, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2369439
    const-string v4, "has_pin"

    const/4 p1, 0x1

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2369440
    :goto_0
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2369441
    const-string v4, "source"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2369442
    :cond_1
    iget-object v4, v1, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->k:LX/10O;

    const-string p1, "dbl_nux_save_account"

    invoke-virtual {v4, p1, v3}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2369443
    :cond_2
    :goto_1
    iget-boolean v0, p0, LX/GbN;->d:Z

    if-nez v0, :cond_3

    .line 2369444
    iget-object v0, p0, LX/GbN;->f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, p0, LX/GbN;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;)V

    .line 2369445
    :cond_3
    return-void

    .line 2369446
    :cond_4
    sget-object v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->d:Ljava/lang/Class;

    const-string v1, "Fetched result was null"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2369447
    iget-object v0, p0, LX/GbN;->a:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    if-nez v0, :cond_2

    .line 2369448
    iget-object v0, p0, LX/GbN;->f:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-boolean v1, p0, LX/GbN;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, LX/GbN;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a$redex0(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_1

    .line 2369449
    :cond_5
    const-string v4, "has_pin"

    const/4 p1, 0x0

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method
