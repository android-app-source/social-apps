.class public final LX/H92;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic b:LX/H94;


# direct methods
.method public constructor <init>(LX/H94;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 0

    .prologue
    .line 2433875
    iput-object p1, p0, LX/H92;->b:LX/H94;

    iput-object p2, p0, LX/H92;->a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2433876
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 2433877
    :goto_0
    return-void

    .line 2433878
    :cond_0
    iget-object v0, p0, LX/H92;->b:LX/H94;

    iget-object v1, p0, LX/H92;->a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNFOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/H94;->a$redex0(LX/H94;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2433879
    check-cast p1, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel;

    .line 2433880
    iget-object v0, p0, LX/H92;->b:LX/H94;

    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel$SubscribeeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel$SubscribeeModel;->l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel$SubscribeeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel$SubscribeeModel;->k()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel$SubscribeeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel$SubscribeeModel;->j()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, LX/H94;->a$redex0(LX/H94;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    .line 2433881
    return-void
.end method
