.class public final LX/FH4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:Z

.field public final synthetic d:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic e:Z

.field public final synthetic f:Z

.field public final synthetic g:LX/FHf;

.field public final synthetic h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/google/common/util/concurrent/SettableFuture;ZLcom/google/common/util/concurrent/ListenableFuture;ZZLX/FHf;)V
    .locals 0

    .prologue
    .line 2219767
    iput-object p1, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p3, p0, LX/FH4;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iput-boolean p4, p0, LX/FH4;->c:Z

    iput-object p5, p0, LX/FH4;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-boolean p6, p0, LX/FH4;->e:Z

    iput-boolean p7, p0, LX/FH4;->f:Z

    iput-object p8, p0, LX/FH4;->g:LX/FHf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2219768
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2219769
    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    .line 2219770
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v1

    .line 2219771
    sget-object v1, LX/1nY;->SEGMENTED_TRANSCODE_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2219772
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2219773
    iput-boolean v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->P:Z

    .line 2219774
    iget-object v0, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 2219775
    iget-object v1, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v1, v0}, LX/2MR;->b(LX/6ed;)V

    .line 2219776
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, p0, LX/FH4;->g:LX/FHf;

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2219777
    :goto_0
    return-void

    .line 2219778
    :cond_0
    iget-object v0, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    if-ne v0, v1, :cond_1

    .line 2219779
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2219780
    :cond_1
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v0

    .line 2219781
    iget-object v1, v0, LX/FGc;->b:LX/FGb;

    sget-object v2, LX/FGb;->SUCCEEDED:LX/FGb;

    invoke-virtual {v1, v2}, LX/FGb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2219782
    goto :goto_0

    .line 2219783
    :cond_2
    sget-object v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v2, "MediaResource upload failed: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v4, v4, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v4, v3, v5

    invoke-static {v1, p1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2219784
    instance-of v1, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v1, :cond_3

    .line 2219785
    iget-object v1, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->q:LX/2Mm;

    iget-object v2, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    sget-object v3, LX/FH8;->UPLOAD:LX/FH8;

    invoke-virtual {v3}, LX/FH8;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, LX/FGc;->e:LX/FGa;

    invoke-virtual {v0}, LX/FGa;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2219786
    invoke-static {v2}, LX/2Mm;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v4

    .line 2219787
    const-string v5, "result_path"

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219788
    const-string v5, "canceled_stage"

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219789
    const-string v5, "messenger_media_upload_request_canceled"

    invoke-static {v1, v5, v4}, LX/2Mm;->a(LX/2Mm;Ljava/lang/String;Ljava/util/Map;)V

    .line 2219790
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->r:LX/2Mn;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219791
    invoke-static {v1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v3

    .line 2219792
    iget-object v2, v0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v2, v3}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FHF;

    .line 2219793
    if-nez v2, :cond_4

    .line 2219794
    :goto_1
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219795
    invoke-static {v1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v3

    .line 2219796
    iget-object v2, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v2, v3}, LX/2MR;->a(LX/6ed;)LX/FGc;

    move-result-object v2

    .line 2219797
    if-nez v2, :cond_5

    .line 2219798
    iget-object v2, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->o:LX/03V;

    const-string v4, "MediaUploadManagerImpl_MISSING_UPLOAD_STATUS"

    const-string v5, "Missing status for failed media resource"

    invoke-virtual {v2, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2219799
    sget-object v2, LX/FGa;->UNKNOWN:LX/FGa;

    invoke-static {v2, p1}, LX/FGc;->a(LX/FGa;Ljava/lang/Throwable;)LX/FGc;

    move-result-object v2

    .line 2219800
    :goto_2
    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v4, v3, v2}, LX/2MR;->a(LX/6ed;LX/FGc;)V

    .line 2219801
    invoke-static {v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    .line 2219802
    move-object v0, v2

    .line 2219803
    iget-object v1, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    invoke-static {}, LX/FGn;->a()Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2219804
    iget-object v1, p0, LX/FH4;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x18a12ad6

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto/16 :goto_0

    .line 2219805
    :cond_3
    iget-object v1, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->q:LX/2Mm;

    iget-object v2, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    sget-object v3, LX/FH8;->UPLOAD:LX/FH8;

    invoke-virtual {v3}, LX/FH8;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, LX/FGc;->e:LX/FGa;

    invoke-virtual {v0}, LX/FGa;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2219806
    invoke-static {v2}, LX/2Mm;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v4

    .line 2219807
    const-string v5, "result_path"

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219808
    const-string v5, "failed_stage"

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219809
    const-string v5, "messenger_media_upload_request_failed"

    invoke-static {v1, v5, v4}, LX/2Mm;->a(LX/2Mm;Ljava/lang/String;Ljava/util/Map;)V

    .line 2219810
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->r:LX/2Mn;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219811
    invoke-static {v1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v3

    .line 2219812
    iget-object v2, v0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v2, v3}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FHF;

    .line 2219813
    if-nez v2, :cond_6

    .line 2219814
    :goto_3
    goto :goto_1

    .line 2219815
    :cond_4
    iget-object v4, v2, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2219816
    iget-object v2, v2, LX/FHF;->b:LX/0SW;

    .line 2219817
    const-string v5, "preparation_finish"

    invoke-static {v4, v5, v2}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 2219818
    const-string v2, "completion_status"

    sget-object v5, LX/FHH;->cancelation:LX/FHH;

    invoke-virtual {v5}, LX/FHH;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2219819
    invoke-static {v0, v3, v4}, LX/2Mn;->a(LX/2Mn;LX/FHE;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_1

    .line 2219820
    :cond_5
    iget-object v2, v2, LX/FGc;->e:LX/FGa;

    invoke-static {v2, p1}, LX/FGc;->a(LX/FGa;Ljava/lang/Throwable;)LX/FGc;

    move-result-object v2

    goto :goto_2

    .line 2219821
    :cond_6
    iget-object v4, v2, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2219822
    iget-object v2, v2, LX/FHF;->b:LX/0SW;

    .line 2219823
    const-string v5, "preparation_finish"

    invoke-static {v4, v5, v2}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 2219824
    const-string v2, "completion_status"

    sget-object v5, LX/FHH;->failure:LX/FHH;

    invoke-virtual {v5}, LX/FHH;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2219825
    invoke-static {v0, v3, v4}, LX/2Mn;->a(LX/2Mn;LX/FHE;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_3
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2219826
    check-cast p1, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 2219827
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->g:LX/2ML;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/2ML;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    .line 2219828
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH4;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v2, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    sget-object v3, LX/FH8;->UPLOAD:LX/FH8;

    invoke-static {v0, v1, v2, p1, v3}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FH8;)Z

    .line 2219829
    iget-boolean v0, p0, LX/FH4;->c:Z

    if-eqz v0, :cond_0

    .line 2219830
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH4;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, p0, LX/FH4;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-boolean v3, p0, LX/FH4;->e:Z

    iget-boolean v4, p0, LX/FH4;->f:Z

    const/4 v7, 0x1

    .line 2219831
    sget-object v5, LX/FHx;->PHASE_TWO:LX/FHx;

    invoke-static {v1, v5}, LX/FHy;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHx;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v5

    move-object v6, v5

    .line 2219832
    const/4 v9, 0x0

    sget-object v10, LX/FHx;->PHASE_TWO:LX/FHx;

    move-object v5, v0

    move v8, v3

    move v11, v4

    move v12, v7

    invoke-static/range {v5 .. v12}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZZLcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2219833
    invoke-static {v0, v6, v5, v2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2219834
    :cond_0
    iget-object v0, p0, LX/FH4;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    new-instance v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$6$1;-><init>(LX/FH4;)V

    const v2, -0x6131f025

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2219835
    return-void
.end method
