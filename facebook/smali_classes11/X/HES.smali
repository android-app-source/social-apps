.class public LX/HES;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final c:LX/03V;

.field public final d:LX/8E2;

.field public e:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;LX/03V;LX/8E2;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2442062
    iput-object p1, p0, LX/HES;->a:LX/0tX;

    .line 2442063
    iput-object p2, p0, LX/HES;->b:Ljava/util/concurrent/Executor;

    .line 2442064
    iput-object p3, p0, LX/HES;->c:LX/03V;

    .line 2442065
    iput-object p4, p0, LX/HES;->d:LX/8E2;

    .line 2442066
    return-void
.end method

.method public static a(LX/0QB;)LX/HES;
    .locals 5

    .prologue
    .line 2442058
    new-instance v4, LX/HES;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/8E2;->a(LX/0QB;)LX/8E2;

    move-result-object v3

    check-cast v3, LX/8E2;

    invoke-direct {v4, v0, v1, v2, v3}, LX/HES;-><init>(LX/0tX;Ljava/util/concurrent/Executor;LX/03V;LX/8E2;)V

    .line 2442059
    move-object v0, v4

    .line 2442060
    return-object v0
.end method

.method public static b$redex0(LX/HES;)V
    .locals 1

    .prologue
    .line 2442056
    const/4 v0, 0x0

    iput-object v0, p0, LX/HES;->e:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;

    .line 2442057
    return-void
.end method
