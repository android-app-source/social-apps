.class public LX/H4N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/9kE;


# direct methods
.method public constructor <init>(LX/0tX;LX/9kE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2422673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2422674
    iput-object p1, p0, LX/H4N;->a:LX/0tX;

    .line 2422675
    iput-object p2, p0, LX/H4N;->b:LX/9kE;

    .line 2422676
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2422677
    iget-object v0, p0, LX/H4N;->b:LX/9kE;

    invoke-virtual {v0}, LX/9kE;->c()V

    .line 2422678
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;LX/0TF;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2422679
    invoke-virtual {p0}, LX/H4N;->a()V

    .line 2422680
    const-wide/16 v7, 0x0

    .line 2422681
    new-instance v3, LX/CRs;

    invoke-direct {v3}, LX/CRs;-><init>()V

    move-object v3, v3

    .line 2422682
    const-string v4, "search_query"

    .line 2422683
    iget-object v5, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2422684
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2422685
    const-string v4, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2422686
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->a:D

    move-wide v5, v9

    .line 2422687
    cmpl-double v4, v5, v7

    if-eqz v4, :cond_0

    .line 2422688
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->b:D

    move-wide v5, v9

    .line 2422689
    cmpl-double v4, v5, v7

    if-eqz v4, :cond_0

    .line 2422690
    const-string v4, "latitude"

    .line 2422691
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->a:D

    move-wide v5, v9

    .line 2422692
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2422693
    const-string v4, "longitude"

    .line 2422694
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->b:D

    move-wide v5, v9

    .line 2422695
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2422696
    :cond_0
    move-object v0, v3

    .line 2422697
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2422698
    iget-object v1, p0, LX/H4N;->b:LX/9kE;

    iget-object v2, p0, LX/H4N;->a:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/H4M;

    invoke-direct {v2, p0, p2, p1}, LX/H4M;-><init>(LX/H4N;LX/0TF;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;)V

    invoke-virtual {v1, v0, v2}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2422699
    return-void
.end method
