.class public final LX/G47;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;)V
    .locals 0

    .prologue
    .line 2317096
    iput-object p1, p0, LX/G47;->a:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 2317097
    iget-object v0, p0, LX/G47;->a:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    iget-object v1, p0, LX/G47;->a:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    iget v1, v1, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->f:F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float/2addr v1, v2

    .line 2317098
    iput v1, v0, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->j:F

    .line 2317099
    iget-object v0, p0, LX/G47;->a:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->invalidate()V

    .line 2317100
    return-void
.end method
