.class public final LX/Fdi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;)V
    .locals 0

    .prologue
    .line 2263892
    iput-object p1, p0, LX/Fdi;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x1222b471

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263893
    iget-object v1, p0, LX/Fdi;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->u:LX/FdZ;

    if-eqz v1, :cond_0

    .line 2263894
    iget-object v1, p0, LX/Fdi;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->u:LX/FdZ;

    iget-object v2, p0, LX/Fdi;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    iget-object v2, v2, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->o:LX/5uu;

    iget-object v3, p0, LX/Fdi;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    iget-object v3, v3, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->m:LX/Fdl;

    .line 2263895
    iget-object v5, v3, LX/Fdl;->b:LX/0ad;

    sget-short v6, LX/100;->I:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 2263896
    invoke-static {v3}, LX/Fdl;->b(LX/Fdl;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, v3, LX/Fdl;->d:LX/5uu;

    invoke-interface {v6}, LX/5uu;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, LX/Fdl;->b(LX/Fdl;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v7

    .line 2263897
    iget-object v8, v7, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2263898
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2263899
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 2263900
    :goto_0
    move-object v3, v5

    .line 2263901
    invoke-interface {v1, v2, v3}, LX/FdZ;->a(LX/5uu;LX/0Px;)V

    .line 2263902
    :cond_0
    iget-object v1, p0, LX/Fdi;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2263903
    const v1, 0x5c8fa8a9

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2263904
    :cond_1
    if-nez v5, :cond_2

    iget-object v5, v3, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 2263905
    new-instance v5, LX/CyH;

    iget-object v6, v3, LX/Fdl;->d:LX/5uu;

    invoke-interface {v6}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, LX/Fdl;->b(LX/Fdl;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v7

    .line 2263906
    iget-object v8, v7, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2263907
    invoke-static {v3}, LX/Fdl;->b(LX/Fdl;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v8

    .line 2263908
    iget-object v9, v8, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2263909
    invoke-direct {v5, v6, v7, v8}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    goto :goto_0

    .line 2263910
    :cond_2
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2263911
    iget-object v5, v3, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263912
    new-instance v8, LX/CyH;

    iget-object v9, v3, LX/Fdl;->d:LX/5uu;

    invoke-interface {v9}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v9

    .line 2263913
    iget-object v10, v5, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v10, v10

    .line 2263914
    iget-object p1, v5, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    move-object v5, p1

    .line 2263915
    invoke-direct {v8, v9, v10, v5}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2263916
    :cond_3
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    goto :goto_0
.end method
