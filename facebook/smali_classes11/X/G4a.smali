.class public LX/G4a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/G4a;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;

.field private final d:LX/0W9;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0ad;LX/0W9;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0ad;",
            "LX/0W9;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317838
    iput-object p1, p0, LX/G4a;->a:LX/0Or;

    .line 2317839
    iput-object p2, p0, LX/G4a;->b:LX/0Or;

    .line 2317840
    iput-object p3, p0, LX/G4a;->c:LX/0ad;

    .line 2317841
    iput-object p4, p0, LX/G4a;->d:LX/0W9;

    .line 2317842
    iput-object p5, p0, LX/G4a;->e:LX/0Ot;

    .line 2317843
    return-void
.end method

.method private a(Ljava/lang/String;)LX/2rw;
    .locals 1

    .prologue
    .line 2317891
    iget-object v0, p0, LX/G4a;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2rw;->USER:LX/2rw;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/G4a;
    .locals 9

    .prologue
    .line 2317854
    sget-object v0, LX/G4a;->f:LX/G4a;

    if-nez v0, :cond_1

    .line 2317855
    const-class v1, LX/G4a;

    monitor-enter v1

    .line 2317856
    :try_start_0
    sget-object v0, LX/G4a;->f:LX/G4a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2317857
    if-eqz v2, :cond_0

    .line 2317858
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2317859
    new-instance v3, LX/G4a;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1a1

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v7

    check-cast v7, LX/0W9;

    const/16 v8, 0x3572

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/G4a;-><init>(LX/0Or;LX/0Or;LX/0ad;LX/0W9;LX/0Ot;)V

    .line 2317860
    move-object v0, v3

    .line 2317861
    sput-object v0, LX/G4a;->f:LX/G4a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317862
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2317863
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2317864
    :cond_1
    sget-object v0, LX/G4a;->f:LX/G4a;

    return-object v0

    .line 2317865
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2317866
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)LX/8AA;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2317867
    iget-object v0, p0, LX/G4a;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, LX/89I;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, p1}, LX/G4a;->a(Ljava/lang/String;)LX/2rw;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    .line 2317868
    iput-object p2, v0, LX/89I;->c:Ljava/lang/String;

    .line 2317869
    move-object v0, v0

    .line 2317870
    iput-object p3, v0, LX/89I;->d:Ljava/lang/String;

    .line 2317871
    move-object v0, v0

    .line 2317872
    invoke-virtual {v0, p4}, LX/89I;->a(LX/2rX;)LX/89I;

    move-result-object v0

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 2317873
    :goto_0
    sget-object v1, LX/21D;->TIMELINE:LX/21D;

    const-string v2, "photoFromTimeline"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const-string v2, "ANDROID_TIMELINE_COMPOSER"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShouldPickerSupportLiveCamera(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/G4a;->c:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2317874
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->TIMELINE:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2317875
    iput-object v0, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2317876
    move-object v0, v1

    .line 2317877
    invoke-virtual {v0}, LX/8AA;->p()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->q()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->e()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->s()LX/8AA;

    move-result-object v1

    .line 2317878
    iget-object v0, p0, LX/G4a;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317879
    invoke-virtual {v1}, LX/8AA;->g()LX/8AA;

    .line 2317880
    iget-object v0, p0, LX/G4a;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317881
    invoke-virtual {v1}, LX/8AA;->h()LX/8AA;

    .line 2317882
    :cond_0
    iget-object v0, p0, LX/G4a;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2317883
    iget-object v0, p0, LX/G4a;->c:LX/0ad;

    sget-short v2, LX/1EB;->C:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2317884
    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    .line 2317885
    :cond_1
    invoke-direct {p0, p1}, LX/G4a;->a(Ljava/lang/String;)LX/2rw;

    move-result-object v0

    sget-object v2, LX/2rw;->USER:LX/2rw;

    if-ne v0, v2, :cond_2

    .line 2317886
    invoke-virtual {v1}, LX/8AA;->b()LX/8AA;

    .line 2317887
    :cond_2
    :goto_1
    return-object v1

    .line 2317888
    :cond_3
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    goto/16 :goto_0

    .line 2317889
    :cond_4
    invoke-virtual {v1}, LX/8AA;->c()LX/8AA;

    .line 2317890
    invoke-virtual {v1}, LX/8AA;->b()LX/8AA;

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2317852
    sget-object v0, LX/21D;->TIMELINE:LX/21D;

    const-string v1, "lifeEventIntent"

    invoke-static {v0, v1}, LX/1nC;->b(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89K;

    invoke-direct {v1}, LX/89K;-><init>()V

    invoke-static {}, LX/BQ4;->c()LX/BQ4;

    move-result-object v1

    invoke-static {v1}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "timeline_composer"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_TIMELINE_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/G4a;->c:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2317853
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.facebook.composer.lifeevent.type.ComposerLifeEventTypeActivity"

    invoke-direct {v2, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v0, p0, LX/G4a;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2317850
    invoke-direct {p0, p2, p3, p4, p5}, LX/G4a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)LX/8AA;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2317851
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2317844
    sget-object v0, LX/21D;->TIMELINE:LX/21D;

    const-string v1, "statusFromTimeline"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, p1}, LX/G4a;->a(Ljava/lang/String;)LX/2rw;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2317845
    iput-object p2, v1, LX/89I;->c:Ljava/lang/String;

    .line 2317846
    move-object v1, v1

    .line 2317847
    iput-object p3, v1, LX/89I;->d:Ljava/lang/String;

    .line 2317848
    move-object v1, v1

    .line 2317849
    invoke-virtual {v1, p4}, LX/89I;->a(LX/2rX;)LX/89I;

    move-result-object v1

    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "timeline_composer"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_TIMELINE_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShouldPickerSupportLiveCamera(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowDynamicTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowRichTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowRichTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/G4a;->c:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method
