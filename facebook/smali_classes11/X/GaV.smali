.class public LX/GaV;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2368311
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2368312
    const-string v0, "composer/?link={%s}&app_id={%s}&name={%s}&caption={%s}&description={%s}&picture={%s}&quote={%s}&next={%s}&host_url={%s}&%s={!%s false}&%s={!%s false}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "link_for_share"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "com.facebook.platform.extra.APPLICATION_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "caption"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "picture"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "quote"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "next"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "host_url"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "is_web_share"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "is_web_share"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "is_in_app_web_share"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "is_in_app_web_share"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/GaS;

    invoke-direct {v1, p0}, LX/GaS;-><init>(LX/GaV;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2368313
    const-string v0, "composer/?link={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "link_for_share"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/GaT;

    invoke-direct {v1, p0}, LX/GaT;-><init>(LX/GaV;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2368314
    const-string v0, "composer/draft/?story_id={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "story_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/GaU;

    invoke-direct {v1, p0}, LX/GaU;-><init>(LX/GaV;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2368315
    return-void
.end method

.method public static a(LX/0QB;)LX/GaV;
    .locals 1

    .prologue
    .line 2368308
    new-instance v0, LX/GaV;

    invoke-direct {v0}, LX/GaV;-><init>()V

    .line 2368309
    move-object v0, v0

    .line 2368310
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2368307
    if-eqz p0, :cond_0

    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
