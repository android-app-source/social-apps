.class public LX/Fup;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/Fx5;

.field public final e:LX/BQ1;

.field public final f:LX/G4m;

.field public final g:LX/5SB;

.field private h:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/Fx5;LX/BQ1;LX/G4m;LX/BP0;)V
    .locals 0
    .param p5    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/Fx5;",
            "LX/BQ1;",
            "LX/G4m;",
            "LX/BP0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300554
    iput-object p1, p0, LX/Fup;->a:Landroid/content/Context;

    .line 2300555
    iput-object p2, p0, LX/Fup;->b:LX/0Or;

    .line 2300556
    iput-object p3, p0, LX/Fup;->c:LX/0Or;

    .line 2300557
    iput-object p4, p0, LX/Fup;->d:LX/Fx5;

    .line 2300558
    iput-object p5, p0, LX/Fup;->e:LX/BQ1;

    .line 2300559
    iput-object p6, p0, LX/Fup;->f:LX/G4m;

    .line 2300560
    iput-object p7, p0, LX/Fup;->g:LX/5SB;

    .line 2300561
    return-void
.end method

.method public static a(LX/Fup;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2300550
    iget-object v0, p0, LX/Fup;->h:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2300551
    new-instance v0, LX/Fum;

    invoke-direct {v0, p0}, LX/Fum;-><init>(LX/Fup;)V

    iput-object v0, p0, LX/Fup;->h:Landroid/view/View$OnClickListener;

    .line 2300552
    :cond_0
    iget-object v0, p0, LX/Fup;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/5SB;LX/BQ1;LX/G4m;LX/0Or;LX/0Or;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/5SB;",
            "LX/BQ1;",
            "LX/G4m;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2300539
    invoke-static {p2}, LX/Fup;->a(LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300540
    invoke-virtual {p2}, LX/BQ1;->ad()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2300541
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    .line 2300542
    iget-wide v4, p1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2300543
    invoke-virtual {v0, v2, v3}, LX/BQ9;->g(J)V

    .line 2300544
    invoke-virtual {p2}, LX/BQ1;->ad()LX/0Px;

    move-result-object v0

    invoke-static {p0, p2, p3, p4, v0}, LX/Fup;->a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/0Or;LX/0Px;)V

    .line 2300545
    :cond_0
    :goto_0
    return-void

    .line 2300546
    :cond_1
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    .line 2300547
    iget-wide v4, p1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2300548
    invoke-virtual {v0, v2, v3}, LX/BQ9;->f(J)V

    .line 2300549
    const/4 v0, 0x0

    invoke-static {p0, p2, p3, p4, v0}, LX/Fup;->a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/0Or;LX/0Px;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/0Or;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BQ1;",
            "LX/G4m;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2300518
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosActivity;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2300519
    iget-object v0, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v0

    .line 2300520
    if-eqz v0, :cond_0

    .line 2300521
    const-string v0, "show_feed_sharing_switch_extra"

    .line 2300522
    iget-object v1, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v1, v1

    .line 2300523
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->ci_()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2300524
    const-string v0, "initial_is_feed_sharing_switch_checked"

    .line 2300525
    iget-object v1, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v1, v1

    .line 2300526
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->n()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2300527
    :cond_0
    if-eqz p4, :cond_1

    .line 2300528
    const-string v0, "fav_photos_extra"

    invoke-static {v2, v0, p4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 2300529
    :cond_1
    const/4 v1, 0x1

    .line 2300530
    invoke-virtual {p2}, LX/BPB;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2300531
    invoke-virtual {p2}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    .line 2300532
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2300533
    :goto_0
    const-string v1, "has_tagged_mediaset"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2300534
    :goto_1
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x71e

    const-class v1, Landroid/app/Activity;

    invoke-static {p0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2300535
    return-void

    .line 2300536
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2300537
    :cond_3
    const-string v0, "has_tagged_mediaset"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2300538
    const-string v0, "land_on_uploads_tab"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method

.method private static a(LX/BQ1;)Z
    .locals 1

    .prologue
    .line 2300515
    if-eqz p0, :cond_0

    .line 2300516
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v0

    .line 2300517
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;LX/5SB;LX/BQ1;LX/G4m;LX/0Or;LX/0Or;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/5SB;",
            "LX/BQ1;",
            "LX/G4m;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2300509
    invoke-static {p2}, LX/Fup;->a(LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300510
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    .line 2300511
    iget-wide v4, p1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2300512
    invoke-virtual {p2}, LX/BQ1;->af()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, LX/BQ9;->a(JI)V

    .line 2300513
    invoke-virtual {p2}, LX/BQ1;->af()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Fx4;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {p0, p2, p3, p4, v0}, LX/Fup;->a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/0Or;LX/0Px;)V

    .line 2300514
    :cond_0
    return-void
.end method

.method public static d(Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;Z)V
    .locals 0

    .prologue
    .line 2300505
    if-eqz p1, :cond_0

    .line 2300506
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->e()V

    .line 2300507
    :goto_0
    return-void

    .line 2300508
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->f()V

    goto :goto_0
.end method
