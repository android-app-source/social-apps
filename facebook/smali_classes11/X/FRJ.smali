.class public final enum LX/FRJ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6vZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FRJ;",
        ">;",
        "LX/6vZ;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FRJ;

.field public static final enum PAYMENT_HISTORY:LX/FRJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2240238
    new-instance v0, LX/FRJ;

    const-string v1, "PAYMENT_HISTORY"

    invoke-direct {v0, v1, v2}, LX/FRJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRJ;->PAYMENT_HISTORY:LX/FRJ;

    .line 2240239
    const/4 v0, 0x1

    new-array v0, v0, [LX/FRJ;

    sget-object v1, LX/FRJ;->PAYMENT_HISTORY:LX/FRJ;

    aput-object v1, v0, v2

    sput-object v0, LX/FRJ;->$VALUES:[LX/FRJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2240240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FRJ;
    .locals 1

    .prologue
    .line 2240237
    const-class v0, LX/FRJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FRJ;

    return-object v0
.end method

.method public static values()[LX/FRJ;
    .locals 1

    .prologue
    .line 2240236
    sget-object v0, LX/FRJ;->$VALUES:[LX/FRJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FRJ;

    return-object v0
.end method
