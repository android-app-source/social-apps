.class public LX/Fju;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3y7;


# instance fields
.field private final a:LX/1sf;

.field private final b:LX/Fjp;


# direct methods
.method public constructor <init>(LX/1sf;LX/Fjp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2277971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2277972
    iput-object p1, p0, LX/Fju;->a:LX/1sf;

    .line 2277973
    iput-object p2, p0, LX/Fju;->b:LX/Fjp;

    .line 2277974
    return-void
.end method

.method public static a(LX/0QB;)LX/Fju;
    .locals 3

    .prologue
    .line 2277976
    new-instance v2, LX/Fju;

    invoke-static {p0}, LX/1sf;->b(LX/0QB;)LX/1sf;

    move-result-object v0

    check-cast v0, LX/1sf;

    invoke-static {p0}, LX/Fjp;->a(LX/0QB;)LX/Fjp;

    move-result-object v1

    check-cast v1, LX/Fjp;

    invoke-direct {v2, v0, v1}, LX/Fju;-><init>(LX/1sf;LX/Fjp;)V

    .line 2277977
    move-object v0, v2

    .line 2277978
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2277975
    new-instance v0, LX/Fjt;

    iget-object v1, p0, LX/Fju;->b:LX/Fjp;

    iget-object v2, p0, LX/Fju;->a:LX/1sf;

    invoke-direct {v0, p1, v1, v2}, LX/Fjt;-><init>(Landroid/content/Context;LX/Fjp;LX/1sf;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
