.class public LX/FmM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2282329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282330
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2282331
    iput-object v0, p0, LX/FmM;->a:LX/0Ot;

    .line 2282332
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2282333
    iput-object v0, p0, LX/FmM;->b:LX/0Ot;

    .line 2282334
    return-void
.end method

.method public static a(LX/0QB;)LX/FmM;
    .locals 3

    .prologue
    .line 2282335
    new-instance v0, LX/FmM;

    invoke-direct {v0}, LX/FmM;-><init>()V

    .line 2282336
    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x455

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 2282337
    iput-object v1, v0, LX/FmM;->a:LX/0Ot;

    iput-object v2, v0, LX/FmM;->b:LX/0Ot;

    .line 2282338
    move-object v0, v0

    .line 2282339
    return-object v0
.end method
