.class public LX/GVJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;
.implements LX/13G;


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Tn;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/0y3;

.field public final f:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2358980
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GVJ;->a:LX/0Px;

    .line 2358981
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "on_login_nf_location_settings_resurrection_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/GVJ;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0y3;LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0y3;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2358982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2358983
    iput-object p1, p0, LX/GVJ;->c:LX/0Or;

    .line 2358984
    iput-object p2, p0, LX/GVJ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2358985
    iput-object p3, p0, LX/GVJ;->e:LX/0y3;

    .line 2358986
    iput-object p4, p0, LX/GVJ;->f:LX/0Uh;

    .line 2358987
    return-void
.end method

.method public static d(LX/GVJ;)LX/0Tn;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358976
    iget-object v0, p0, LX/GVJ;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2358977
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2358978
    const/4 v0, 0x0

    .line 2358979
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, LX/GVJ;->b:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2358975
    const-wide/32 v0, 0x240c8400

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2358974
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 2358988
    iget-object v0, p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .line 2358989
    invoke-static {p0}, LX/GVJ;->d(LX/GVJ;)LX/0Tn;

    move-result-object v1

    .line 2358990
    if-nez v1, :cond_2

    .line 2358991
    :cond_0
    :goto_0
    move v0, v0

    .line 2358992
    if-eqz v0, :cond_1

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_1
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_1

    .line 2358993
    :cond_2
    iget-object p1, p0, LX/GVJ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 2358994
    if-nez v1, :cond_0

    iget-object v1, p0, LX/GVJ;->e:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->a()LX/0yG;

    move-result-object v1

    sget-object p1, LX/0yG;->OKAY:LX/0yG;

    if-eq v1, p1, :cond_0

    iget-object v1, p0, LX/GVJ;->f:LX/0Uh;

    const/16 p1, 0x582

    invoke-virtual {v1, p1, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2358968
    invoke-static {p0}, LX/GVJ;->d(LX/GVJ;)LX/0Tn;

    move-result-object v0

    .line 2358969
    if-nez v0, :cond_0

    .line 2358970
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2358971
    const-string v1, "source"

    const-string v2, "login"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2358972
    return-object v0

    .line 2358973
    :cond_0
    iget-object v1, p0, LX/GVJ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2358967
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2358966
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2358964
    const-string v0, "4280"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2358965
    sget-object v0, LX/GVJ;->a:LX/0Px;

    return-object v0
.end method
