.class public final LX/G8z;
.super LX/G8w;
.source ""


# instance fields
.field private final a:[B

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(II[I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2321381
    invoke-direct {p0, p1, p2}, LX/G8w;-><init>(II)V

    .line 2321382
    iput p1, p0, LX/G8z;->b:I

    .line 2321383
    iput p2, p0, LX/G8z;->c:I

    .line 2321384
    iput v1, p0, LX/G8z;->d:I

    .line 2321385
    iput v1, p0, LX/G8z;->e:I

    .line 2321386
    mul-int v0, p1, p2

    new-array v0, v0, [B

    iput-object v0, p0, LX/G8z;->a:[B

    move v2, v1

    .line 2321387
    :goto_0
    if-ge v2, p2, :cond_2

    .line 2321388
    mul-int v3, v2, p1

    move v0, v1

    .line 2321389
    :goto_1
    if-ge v0, p1, :cond_1

    .line 2321390
    add-int v4, v3, v0

    aget v4, p3, v4

    .line 2321391
    shr-int/lit8 v5, v4, 0x10

    and-int/lit16 v5, v5, 0xff

    .line 2321392
    shr-int/lit8 v6, v4, 0x8

    and-int/lit16 v6, v6, 0xff

    .line 2321393
    and-int/lit16 v4, v4, 0xff

    .line 2321394
    if-ne v5, v6, :cond_0

    if-ne v6, v4, :cond_0

    .line 2321395
    iget-object v4, p0, LX/G8z;->a:[B

    add-int v6, v3, v0

    int-to-byte v5, v5

    aput-byte v5, v4, v6

    .line 2321396
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2321397
    :cond_0
    iget-object v7, p0, LX/G8z;->a:[B

    add-int v8, v3, v0

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x4

    int-to-byte v4, v4

    aput-byte v4, v7, v8

    goto :goto_2

    .line 2321398
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2321399
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2321400
    iget v0, p0, LX/G8w;->a:I

    move v3, v0

    .line 2321401
    iget v0, p0, LX/G8w;->b:I

    move v4, v0

    .line 2321402
    iget v0, p0, LX/G8z;->b:I

    if-ne v3, v0, :cond_1

    iget v0, p0, LX/G8z;->c:I

    if-ne v4, v0, :cond_1

    .line 2321403
    iget-object v0, p0, LX/G8z;->a:[B

    .line 2321404
    :cond_0
    :goto_0
    return-object v0

    .line 2321405
    :cond_1
    mul-int v5, v3, v4

    .line 2321406
    new-array v0, v5, [B

    .line 2321407
    iget v2, p0, LX/G8z;->e:I

    iget v6, p0, LX/G8z;->b:I

    mul-int/2addr v2, v6

    iget v6, p0, LX/G8z;->d:I

    add-int/2addr v2, v6

    .line 2321408
    iget v6, p0, LX/G8z;->b:I

    if-ne v3, v6, :cond_2

    .line 2321409
    iget-object v3, p0, LX/G8z;->a:[B

    invoke-static {v3, v2, v0, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 2321410
    :cond_2
    iget-object v5, p0, LX/G8z;->a:[B

    .line 2321411
    :goto_1
    if-ge v1, v4, :cond_0

    .line 2321412
    mul-int v6, v1, v3

    .line 2321413
    invoke-static {v5, v2, v0, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2321414
    iget v6, p0, LX/G8z;->b:I

    add-int/2addr v2, v6

    .line 2321415
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final a(I[B)[B
    .locals 4

    .prologue
    .line 2321416
    if-ltz p1, :cond_0

    .line 2321417
    iget v0, p0, LX/G8w;->b:I

    move v0, v0

    .line 2321418
    if-lt p1, v0, :cond_1

    .line 2321419
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested row is outside the image: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321420
    :cond_1
    iget v0, p0, LX/G8w;->a:I

    move v0, v0

    .line 2321421
    if-eqz p2, :cond_2

    array-length v1, p2

    if-ge v1, v0, :cond_3

    .line 2321422
    :cond_2
    new-array p2, v0, [B

    .line 2321423
    :cond_3
    iget v1, p0, LX/G8z;->e:I

    add-int/2addr v1, p1

    iget v2, p0, LX/G8z;->b:I

    mul-int/2addr v1, v2

    iget v2, p0, LX/G8z;->d:I

    add-int/2addr v1, v2

    .line 2321424
    iget-object v2, p0, LX/G8z;->a:[B

    const/4 v3, 0x0

    invoke-static {v2, v1, p2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2321425
    return-object p2
.end method
