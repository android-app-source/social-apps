.class public LX/F9s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1h1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/F9s;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:LX/F9s;


# instance fields
.field public b:Z

.field private c:LX/0s5;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/http/onion/OnionRewriteRule;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1hZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206289
    const-class v0, LX/F9s;

    sput-object v0, LX/F9s;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0s5;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/F9s;->g:Ljava/util/List;

    .line 2206287
    iput-object p1, p0, LX/F9s;->c:LX/0s5;

    .line 2206288
    return-void
.end method

.method public static a(LX/0QB;)LX/F9s;
    .locals 4

    .prologue
    .line 2206272
    sget-object v0, LX/F9s;->h:LX/F9s;

    if-nez v0, :cond_1

    .line 2206273
    const-class v1, LX/F9s;

    monitor-enter v1

    .line 2206274
    :try_start_0
    sget-object v0, LX/F9s;->h:LX/F9s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206275
    if-eqz v2, :cond_0

    .line 2206276
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206277
    new-instance p0, LX/F9s;

    invoke-static {v0}, LX/0s5;->a(LX/0QB;)LX/0s5;

    move-result-object v3

    check-cast v3, LX/0s5;

    invoke-direct {p0, v3}, LX/F9s;-><init>(LX/0s5;)V

    .line 2206278
    move-object v0, p0

    .line 2206279
    sput-object v0, LX/F9s;->h:LX/F9s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206280
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206281
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206282
    :cond_1
    sget-object v0, LX/F9s;->h:LX/F9s;

    return-object v0

    .line 2206283
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206284
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2206259
    iget-object v0, p0, LX/F9s;->f:LX/0Px;

    if-nez v0, :cond_1

    .line 2206260
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2206261
    invoke-static {p0}, LX/F9s;->f(LX/F9s;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2206262
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2206263
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2206264
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/F9s;->f:LX/0Px;

    .line 2206265
    :cond_1
    iget-object v0, p0, LX/F9s;->f:LX/0Px;

    move-object v3, v0

    .line 2206266
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    .line 2206267
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2206268
    const/4 v0, 0x1

    .line 2206269
    :goto_2
    return v0

    .line 2206270
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2206271
    goto :goto_2
.end method

.method private e()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/http/onion/OnionRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2206254
    iget-object v0, p0, LX/F9s;->d:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F9s;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206255
    :cond_0
    iget-object v0, p0, LX/F9s;->c:LX/0s5;

    .line 2206256
    iget-object v1, v0, LX/0s5;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {v1}, LX/4ci;->a(LX/0W3;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2206257
    iput-object v0, p0, LX/F9s;->d:LX/0Px;

    .line 2206258
    :cond_1
    iget-object v0, p0, LX/F9s;->d:LX/0Px;

    return-object v0
.end method

.method public static f(LX/F9s;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2206290
    iget-object v0, p0, LX/F9s;->e:LX/0Px;

    if-nez v0, :cond_0

    .line 2206291
    iget-object v0, p0, LX/F9s;->c:LX/0s5;

    .line 2206292
    iget-object v1, v0, LX/0s5;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {v1}, LX/4ci;->b(LX/0W3;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2206293
    iput-object v0, p0, LX/F9s;->e:LX/0Px;

    .line 2206294
    :cond_0
    iget-object v0, p0, LX/F9s;->e:LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/http/onion/OnionRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2206248
    iget-boolean v0, p0, LX/F9s;->b:Z

    move v0, v0

    .line 2206249
    if-eqz v0, :cond_0

    .line 2206250
    invoke-direct {p0}, LX/F9s;->e()LX/0Px;

    move-result-object v0

    .line 2206251
    :goto_0
    return-object v0

    .line 2206252
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2206253
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2206241
    iget-boolean v0, p0, LX/F9s;->b:Z

    move v0, v0

    .line 2206242
    if-eqz v0, :cond_0

    .line 2206243
    invoke-direct {p0, p1}, LX/F9s;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206244
    :cond_0
    return-object p1

    .line 2206245
    :cond_1
    invoke-direct {p0}, LX/F9s;->e()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/onion/OnionRewriteRule;

    .line 2206246
    invoke-virtual {v0, p1}, Lcom/facebook/http/onion/OnionRewriteRule;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2206247
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 5

    .prologue
    .line 2206226
    iget-boolean v0, p0, LX/F9s;->b:Z

    move v0, v0

    .line 2206227
    if-eqz v0, :cond_0

    .line 2206228
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2206229
    invoke-direct {p0, v1}, LX/F9s;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206230
    :cond_0
    :goto_0
    return-object p1

    .line 2206231
    :cond_1
    invoke-direct {p0}, LX/F9s;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/onion/OnionRewriteRule;

    .line 2206232
    invoke-virtual {v0, v2}, Lcom/facebook/http/onion/OnionRewriteRule;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2206233
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2206234
    :cond_2
    :try_start_0
    new-instance v0, Lorg/apache/http/impl/client/RequestWrapper;

    invoke-direct {v0, p1}, Lorg/apache/http/impl/client/RequestWrapper;-><init>(Lorg/apache/http/HttpRequest;)V

    .line 2206235
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/RequestWrapper;->setURI(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/ProtocolException; {:try_start_0 .. :try_end_0} :catch_1

    move-object p1, v0

    .line 2206236
    goto :goto_0

    .line 2206237
    :catch_0
    move-exception v0

    .line 2206238
    sget-object v1, LX/F9s;->a:Ljava/lang/Class;

    const-string v2, "Failed to parse rewritten URI"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2206239
    :catch_1
    move-exception v0

    .line 2206240
    sget-object v1, LX/F9s;->a:Ljava/lang/Class;

    const-string v2, "Failed to create wrapped request"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(LX/1hZ;)V
    .locals 1

    .prologue
    .line 2206223
    if-eqz p1, :cond_0

    .line 2206224
    iget-object v0, p0, LX/F9s;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2206225
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2206219
    iput-boolean p1, p0, LX/F9s;->b:Z

    .line 2206220
    iget-object v0, p0, LX/F9s;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hZ;

    .line 2206221
    invoke-interface {v0}, LX/1hZ;->g()V

    goto :goto_0

    .line 2206222
    :cond_0
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2206213
    iget-boolean v0, p0, LX/F9s;->b:Z

    move v0, v0

    .line 2206214
    if-eqz v0, :cond_0

    .line 2206215
    invoke-static {p0}, LX/F9s;->f(LX/F9s;)LX/0Px;

    move-result-object v0

    .line 2206216
    :goto_0
    return-object v0

    .line 2206217
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2206218
    goto :goto_0
.end method
