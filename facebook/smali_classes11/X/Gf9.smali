.class public final LX/Gf9;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GfA;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/99i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/99i",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Z

.field public final synthetic e:LX/GfA;


# direct methods
.method public constructor <init>(LX/GfA;)V
    .locals 1

    .prologue
    .line 2376909
    iput-object p1, p0, LX/Gf9;->e:LX/GfA;

    .line 2376910
    move-object v0, p1

    .line 2376911
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2376912
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2376908
    const-string v0, "PagesYouMayLikeComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376890
    if-ne p0, p1, :cond_1

    .line 2376891
    :cond_0
    :goto_0
    return v0

    .line 2376892
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2376893
    goto :goto_0

    .line 2376894
    :cond_3
    check-cast p1, LX/Gf9;

    .line 2376895
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2376896
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2376897
    if-eq v2, v3, :cond_0

    .line 2376898
    iget-object v2, p0, LX/Gf9;->a:LX/1Pp;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gf9;->a:LX/1Pp;

    iget-object v3, p1, LX/Gf9;->a:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2376899
    goto :goto_0

    .line 2376900
    :cond_5
    iget-object v2, p1, LX/Gf9;->a:LX/1Pp;

    if-nez v2, :cond_4

    .line 2376901
    :cond_6
    iget-object v2, p0, LX/Gf9;->b:LX/99i;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Gf9;->b:LX/99i;

    iget-object v3, p1, LX/Gf9;->b:LX/99i;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2376902
    goto :goto_0

    .line 2376903
    :cond_8
    iget-object v2, p1, LX/Gf9;->b:LX/99i;

    if-nez v2, :cond_7

    .line 2376904
    :cond_9
    iget v2, p0, LX/Gf9;->c:I

    iget v3, p1, LX/Gf9;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2376905
    goto :goto_0

    .line 2376906
    :cond_a
    iget-boolean v2, p0, LX/Gf9;->d:Z

    iget-boolean v3, p1, LX/Gf9;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2376907
    goto :goto_0
.end method
