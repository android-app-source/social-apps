.class public LX/GHW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;


# instance fields
.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/GEH;


# direct methods
.method public constructor <init>(LX/GEH;LX/GF0;LX/GJ2;LX/GIA;LX/GEY;LX/GJi;LX/GKQ;LX/GMR;LX/GMS;LX/GKo;LX/GMK;LX/GLH;LX/GKn;)V
    .locals 7
    .param p7    # LX/GKQ;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2335046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2335047
    iput-object p1, p0, LX/GHW;->j:LX/GEH;

    .line 2335048
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030067

    sget-object v4, LX/GHW;->b:LX/GGX;

    sget-object v5, LX/8wK;->INFO_CARD:LX/8wK;

    invoke-direct {v2, v3, p3, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03005a

    sget-object v4, LX/GHW;->a:LX/GGX;

    sget-object v5, LX/8wK;->ERROR_CARD:LX/8wK;

    invoke-direct {v2, v3, p6, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03006e

    sget-object v4, LX/GHW;->e:LX/GGX;

    sget-object v5, LX/8wK;->AD_PREVIEW:LX/8wK;

    invoke-direct {v2, v3, p7, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030077

    sget-object v4, LX/GHW;->d:LX/GGX;

    sget-object v5, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    invoke-direct {v2, v3, p8, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03008d

    sget-object v4, LX/GHW;->d:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING_DESCRIPTION:LX/8wK;

    move-object/from16 v0, p9

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03008c

    sget-object v4, LX/GHW;->g:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING:LX/8wK;

    move-object/from16 v0, p10

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03004e

    sget-object v4, LX/GHW;->g:LX/GGX;

    sget-object v5, LX/8wK;->BUDGET:LX/8wK;

    move-object/from16 v0, p11

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030082

    sget-object v4, LX/GHW;->g:LX/GGX;

    sget-object v5, LX/8wK;->DURATION:LX/8wK;

    move-object/from16 v0, p12

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03003d

    sget-object v4, LX/GHW;->a:LX/GGX;

    sget-object v5, LX/8wK;->ACCOUNT:LX/8wK;

    invoke-direct {v2, v3, p4, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03005c

    sget-object v4, LX/GHW;->c:LX/GGX;

    sget-object v5, LX/8wK;->FOOTER:LX/8wK;

    move-object/from16 v0, p13

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030087

    const/4 v4, 0x0

    sget-object v5, LX/GHW;->a:LX/GGX;

    sget-object v6, LX/8wK;->SPACER:LX/8wK;

    invoke-direct {v2, v3, v4, v5, v6}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/GHW;->i:LX/0Px;

    .line 2335049
    return-void
.end method

.method public static b(LX/0QB;)LX/GHW;
    .locals 14

    .prologue
    .line 2335050
    new-instance v0, LX/GHW;

    invoke-static {p0}, LX/GEH;->b(LX/0QB;)LX/GEH;

    move-result-object v1

    check-cast v1, LX/GEH;

    invoke-static {p0}, LX/GF0;->b(LX/0QB;)LX/GF0;

    move-result-object v2

    check-cast v2, LX/GF0;

    invoke-static {p0}, LX/GJ2;->a(LX/0QB;)LX/GJ2;

    move-result-object v3

    check-cast v3, LX/GJ2;

    invoke-static {p0}, LX/GIA;->b(LX/0QB;)LX/GIA;

    move-result-object v4

    check-cast v4, LX/GIA;

    invoke-static {p0}, LX/GEY;->b(LX/0QB;)LX/GEY;

    move-result-object v5

    check-cast v5, LX/GEY;

    invoke-static {p0}, LX/GJi;->c(LX/0QB;)LX/GJi;

    move-result-object v6

    check-cast v6, LX/GJi;

    invoke-static {p0}, LX/GCJ;->b(LX/0QB;)LX/GKQ;

    move-result-object v7

    check-cast v7, LX/GKQ;

    invoke-static {p0}, LX/GMR;->b(LX/0QB;)LX/GMR;

    move-result-object v8

    check-cast v8, LX/GMR;

    invoke-static {p0}, LX/GMS;->b(LX/0QB;)LX/GMS;

    move-result-object v9

    check-cast v9, LX/GMS;

    invoke-static {p0}, LX/GKo;->d(LX/0QB;)LX/GKo;

    move-result-object v10

    check-cast v10, LX/GKo;

    invoke-static {p0}, LX/GMK;->b(LX/0QB;)LX/GMK;

    move-result-object v11

    check-cast v11, LX/GMK;

    invoke-static {p0}, LX/GLH;->b(LX/0QB;)LX/GLH;

    move-result-object v12

    check-cast v12, LX/GLH;

    invoke-static {p0}, LX/GKn;->c(LX/0QB;)LX/GKn;

    move-result-object v13

    check-cast v13, LX/GKn;

    invoke-direct/range {v0 .. v13}, LX/GHW;-><init>(LX/GEH;LX/GF0;LX/GJ2;LX/GIA;LX/GEY;LX/GJi;LX/GKQ;LX/GMR;LX/GMS;LX/GKo;LX/GMK;LX/GLH;LX/GKn;)V

    .line 2335051
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2335045
    iget-object v0, p0, LX/GHW;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 7

    .prologue
    .line 2335040
    const-string v0, "page_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2335041
    const-string v0, "promotion_target_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2335042
    invoke-static {p1}, LX/8wJ;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    .line 2335043
    iget-object v0, p0, LX/GHW;->j:LX/GEH;

    sget-object v2, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    const/4 v6, 0x0

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Ljava/lang/String;LX/8wL;LX/GCY;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2335044
    return-void
.end method
