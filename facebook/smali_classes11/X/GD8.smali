.class public final LX/GD8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V
    .locals 0

    .prologue
    .line 2329440
    iput-object p1, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 2329441
    if-ne p1, v4, :cond_0

    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->N:Landroid/text/SpannableString;

    move-object v1, v0

    .line 2329442
    :goto_0
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->y:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    .line 2329443
    if-ne p1, v4, :cond_1

    move v0, v2

    .line 2329444
    :goto_1
    iput-boolean v0, v3, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 2329445
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->v:LX/01T;

    sget-object v4, LX/01T;->PAA:LX/01T;

    if-ne v0, v4, :cond_2

    .line 2329446
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->O:LX/63L;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/63L;->setButtonSpecs(Ljava/util/List;)V

    .line 2329447
    :goto_2
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2329448
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2329449
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->K:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2329450
    return-void

    .line 2329451
    :cond_0
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->E:Ljava/lang/String;

    iget-object v1, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->p:LX/GG6;

    invoke-virtual {v1, p1}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2329452
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 2329453
    :cond_2
    iget-object v0, p0, LX/GD8;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    const v4, 0x7f0d00bc

    invoke-static {v0, v4}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->a(Lcom/facebook/adinterfaces/MapAreaPickerActivity;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2329454
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    goto :goto_2
.end method
