.class public LX/Fv9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:J

.field public B:Z

.field public C:J

.field private final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:LX/BQ1;

.field public final e:LX/BQ9;

.field public final f:LX/BPC;

.field public final g:LX/0hB;

.field public final h:LX/0SG;

.field private final i:LX/Fx5;

.field public final j:LX/1BK;

.field public k:LX/9lQ;

.field private l:Z

.field private m:Z

.field private n:Z

.field public o:Z

.field private p:Z

.field private q:Z

.field public r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;LX/BQ1;LX/BQ9;LX/BPC;LX/0hB;LX/0SG;LX/Fx5;LX/1BK;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2301027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2301028
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Fv9;->a:Ljava/util/HashSet;

    .line 2301029
    sget-object v0, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    iput-object v0, p0, LX/Fv9;->k:LX/9lQ;

    .line 2301030
    iput-boolean v1, p0, LX/Fv9;->m:Z

    .line 2301031
    iput-boolean v1, p0, LX/Fv9;->p:Z

    .line 2301032
    iput-boolean v1, p0, LX/Fv9;->q:Z

    .line 2301033
    iput-boolean v1, p0, LX/Fv9;->s:Z

    .line 2301034
    iput-boolean v1, p0, LX/Fv9;->t:Z

    .line 2301035
    iput-boolean v1, p0, LX/Fv9;->u:Z

    .line 2301036
    iput-boolean v1, p0, LX/Fv9;->v:Z

    .line 2301037
    iput-boolean v1, p0, LX/Fv9;->w:Z

    .line 2301038
    iput-boolean v1, p0, LX/Fv9;->x:Z

    .line 2301039
    iput-boolean v1, p0, LX/Fv9;->y:Z

    .line 2301040
    iput-boolean v1, p0, LX/Fv9;->z:Z

    .line 2301041
    iput-wide v2, p0, LX/Fv9;->A:J

    .line 2301042
    iput-boolean v1, p0, LX/Fv9;->B:Z

    .line 2301043
    iput-wide v2, p0, LX/Fv9;->C:J

    .line 2301044
    iput-object p1, p0, LX/Fv9;->b:Ljava/lang/String;

    .line 2301045
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/Fv9;->c:J

    .line 2301046
    iput-object p3, p0, LX/Fv9;->d:LX/BQ1;

    .line 2301047
    iput-object p4, p0, LX/Fv9;->e:LX/BQ9;

    .line 2301048
    iput-object p5, p0, LX/Fv9;->f:LX/BPC;

    .line 2301049
    iput-object p6, p0, LX/Fv9;->g:LX/0hB;

    .line 2301050
    iput-object p7, p0, LX/Fv9;->h:LX/0SG;

    .line 2301051
    iput-object p8, p0, LX/Fv9;->i:LX/Fx5;

    .line 2301052
    iput-object p9, p0, LX/Fv9;->j:LX/1BK;

    .line 2301053
    return-void
.end method

.method private a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;Z)V
    .locals 7

    .prologue
    .line 2301054
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v0

    .line 2301055
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    if-ne v0, v1, :cond_1

    .line 2301056
    :cond_0
    :goto_0
    return-void

    .line 2301057
    :cond_1
    iget-object v1, p0, LX/Fv9;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2301058
    iget-object v1, p0, LX/Fv9;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2301059
    iget-object v1, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v4, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v5, p0, LX/Fv9;->k:LX/9lQ;

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, LX/BQ9;->a(JLjava/lang/String;LX/9lQ;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V

    .line 2301060
    if-eqz p2, :cond_0

    .line 2301061
    iget-object v1, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v4, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v5, p0, LX/Fv9;->k:LX/9lQ;

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, LX/BQ9;->b(JLjava/lang/String;LX/9lQ;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;LX/FrH;Z)Z
    .locals 6
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/FrH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2300980
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2300981
    :cond_0
    :goto_0
    return v4

    .line 2300982
    :cond_1
    invoke-virtual {p2}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr5;

    .line 2300983
    iget-object v1, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, v1

    .line 2300984
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    move v0, v2

    .line 2300985
    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    if-ge v1, v5, :cond_3

    .line 2300986
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, LX/FyX;

    if-nez v3, :cond_5

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, LX/Fv9;->e(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2300987
    invoke-virtual {p2}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr5;

    .line 2300988
    iget-object v3, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, v3

    .line 2300989
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    .line 2300990
    invoke-direct {p0, v0, p3}, LX/Fv9;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;Z)V

    .line 2300991
    add-int/lit8 v0, v5, -0x1

    if-ne v1, v0, :cond_2

    move v0, v4

    :goto_2
    move v3, v0

    .line 2300992
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2300993
    goto :goto_2

    .line 2300994
    :cond_3
    if-eqz v5, :cond_0

    if-ne v1, v5, :cond_4

    if-nez v0, :cond_0

    :cond_4
    move v4, v2

    goto :goto_0

    :cond_5
    move v3, v0

    goto :goto_3
.end method

.method private e(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2301062
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 2301063
    :cond_0
    :goto_0
    return v0

    .line 2301064
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 2301065
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 2301066
    aget v3, v2, v1

    if-lez v3, :cond_0

    aget v2, v2, v1

    iget-object v3, p0, LX/Fv9;->g:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 15

    .prologue
    .line 2301086
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fv9;->z:Z

    .line 2301087
    iget-object v0, p0, LX/Fv9;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/Fv9;->A:J

    sub-long/2addr v0, v2

    .line 2301088
    const-wide/16 v2, 0x64

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 2301089
    :goto_0
    return-void

    .line 2301090
    :cond_0
    iget-object v5, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v6, p0, LX/Fv9;->c:J

    iget-object v8, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v9, p0, LX/Fv9;->k:LX/9lQ;

    iget-object v4, p0, LX/Fv9;->d:LX/BQ1;

    .line 2301091
    iget-object v10, v4, LX/BQ1;->j:LX/BQ6;

    move-object v10, v10

    .line 2301092
    iget-object v4, p0, LX/Fv9;->f:LX/BPC;

    .line 2301093
    iget-object v11, v4, LX/BPC;->e:LX/BPA;

    move-object v11, v11

    .line 2301094
    move-wide v12, v0

    invoke-virtual/range {v5 .. v13}, LX/BQ9;->b(JLjava/lang/String;LX/9lQ;LX/BQ6;LX/BPA;J)V

    .line 2301095
    iget-object v5, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v6, p0, LX/Fv9;->c:J

    iget-object v8, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v9, p0, LX/Fv9;->k:LX/9lQ;

    iget-object v4, p0, LX/Fv9;->d:LX/BQ1;

    .line 2301096
    iget-object v10, v4, LX/BQ1;->i:LX/BQ6;

    move-object v10, v10

    .line 2301097
    iget-object v4, p0, LX/Fv9;->f:LX/BPC;

    .line 2301098
    iget-object v11, v4, LX/BPC;->d:LX/BPA;

    move-object v11, v11

    .line 2301099
    move-wide v12, v0

    invoke-virtual/range {v5 .. v13}, LX/BQ9;->a(JLjava/lang/String;LX/9lQ;LX/BQ6;LX/BPA;J)V

    .line 2301100
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2301067
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->z:Z

    .line 2301068
    iget-object v0, p0, LX/Fv9;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Fv9;->A:J

    .line 2301069
    return-void
.end method

.method public final a(IZ)V
    .locals 10

    .prologue
    .line 2301070
    if-eqz p2, :cond_1

    iget-boolean v0, p0, LX/Fv9;->B:Z

    if-nez v0, :cond_1

    .line 2301071
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Fv9;->B:Z

    .line 2301072
    iget-object v1, p0, LX/Fv9;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iput-wide v1, p0, LX/Fv9;->C:J

    .line 2301073
    :cond_0
    :goto_0
    return-void

    .line 2301074
    :cond_1
    if-nez p2, :cond_0

    iget-boolean v0, p0, LX/Fv9;->B:Z

    if-eqz v0, :cond_0

    .line 2301075
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Fv9;->B:Z

    .line 2301076
    iget-object v1, p0, LX/Fv9;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iget-wide v3, p0, LX/Fv9;->C:J

    sub-long v7, v1, v3

    .line 2301077
    const-wide/16 v1, 0x64

    cmp-long v1, v7, v1

    if-gez v1, :cond_2

    .line 2301078
    :goto_1
    goto :goto_0

    .line 2301079
    :cond_2
    iget-object v1, p0, LX/Fv9;->j:LX/1BK;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, LX/Fv9;->c:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "featured_photos_unit"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1BL;->a(Ljava/lang/String;)I

    move-result v9

    .line 2301080
    iget-object v2, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v3, p0, LX/Fv9;->c:J

    iget-object v5, p0, LX/Fv9;->k:LX/9lQ;

    move v6, p1

    invoke-virtual/range {v2 .. v9}, LX/BQ9;->a(JLX/9lQ;IJI)V

    goto :goto_1
.end method

.method public final a(LX/Fw6;)V
    .locals 3

    .prologue
    .line 2301081
    iget-boolean v0, p0, LX/Fv9;->p:Z

    if-nez v0, :cond_0

    .line 2301082
    iget-object v0, p1, LX/Fw6;->i:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    move-object v0, v0

    .line 2301083
    iget-object v1, p1, LX/Fw6;->o:LX/FrH;

    move-object v1, v1

    .line 2301084
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LX/Fv9;->a(Landroid/view/ViewGroup;LX/FrH;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Fv9;->p:Z

    .line 2301085
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2301011
    iget-boolean v0, p0, LX/Fv9;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ad()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, LX/Fv9;->e(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2301012
    :cond_0
    :goto_0
    return-void

    .line 2301013
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->s:Z

    .line 2301014
    iget-object v0, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v1, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v4, p0, LX/Fv9;->k:LX/9lQ;

    .line 2301015
    const-string v11, "fav_photos_impression"

    move-object v6, v0

    move-wide v7, v2

    move-object v9, v1

    move-object v10, v4

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2301016
    if-eqz v5, :cond_2

    .line 2301017
    iget-object v6, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2301018
    :cond_2
    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/BQ1;)V
    .locals 12
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/BQ1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301019
    iget-boolean v0, p0, LX/Fv9;->m:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/BQ1;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2301020
    :cond_0
    :goto_0
    return-void

    .line 2301021
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->m:Z

    .line 2301022
    iget-object v0, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v1, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v4, p0, LX/Fv9;->k:LX/9lQ;

    .line 2301023
    const-string v11, "bio_impression"

    move-object v6, v0

    move-wide v7, v2

    move-object v9, v1

    move-object v10, v4

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2301024
    if-eqz v5, :cond_2

    .line 2301025
    iget-object v6, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2301026
    :cond_2
    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/header/TimelineIntroCardView;)V
    .locals 3

    .prologue
    .line 2300915
    iget-boolean v0, p0, LX/Fv9;->q:Z

    if-nez v0, :cond_0

    .line 2300916
    iget-object v0, p1, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    move-object v0, v0

    .line 2300917
    check-cast v0, Lcom/facebook/timeline/header/TimelineContextItemsSection;

    .line 2300918
    iget-object v1, v0, Lcom/facebook/timeline/header/TimelineContextItemsSection;->a:LX/FrA;

    move-object v0, v1

    .line 2300919
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2300920
    :goto_0
    iget-object v1, p1, Lcom/facebook/timeline/header/TimelineIntroCardView;->g:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    move-object v1, v1

    .line 2300921
    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, LX/Fv9;->a(Landroid/view/ViewGroup;LX/FrH;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Fv9;->q:Z

    .line 2300922
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosEmptyView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Fv9;->b(Landroid/view/View;)V

    .line 2300923
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Fv9;->a(Landroid/view/View;)V

    .line 2300924
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getSuggestedPhotosView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Fv9;->a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;)V

    .line 2300925
    return-void

    .line 2300926
    :cond_1
    iget-object v1, v0, LX/FrA;->g:LX/FrH;

    move-object v0, v1

    .line 2300927
    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;)V
    .locals 14
    .param p1    # Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2300928
    iget-boolean v0, p0, LX/Fv9;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, LX/Fv9;->e(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2300929
    :cond_0
    :goto_0
    return-void

    .line 2300930
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->u:Z

    .line 2300931
    iget-object v1, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->af()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    iget-object v5, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v6, p0, LX/Fv9;->k:LX/9lQ;

    .line 2300932
    const-string v13, "fav_photos_add_prompt_suggested_impression"

    move-object v8, v1

    move-wide v9, v2

    move-object v11, v5

    move-object v12, v6

    invoke-static/range {v8 .. v13}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2300933
    if-eqz v7, :cond_2

    .line 2300934
    invoke-static {v7, v4}, LX/BQ9;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;I)V

    .line 2300935
    iget-object v8, v1, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300936
    :cond_2
    iget-object v0, p0, LX/Fv9;->i:LX/Fx5;

    invoke-virtual {v0}, LX/Fwg;->d()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2300937
    if-eqz p1, :cond_1

    iget-boolean v0, p0, LX/Fv9;->z:Z

    if-nez v0, :cond_1

    .line 2300938
    invoke-virtual {p0}, LX/Fv9;->a()V

    .line 2300939
    :cond_0
    :goto_0
    return-void

    .line 2300940
    :cond_1
    if-nez p1, :cond_0

    iget-boolean v0, p0, LX/Fv9;->z:Z

    if-eqz v0, :cond_0

    .line 2300941
    invoke-direct {p0}, LX/Fv9;->g()V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2300942
    const/4 v1, 0x1

    .line 2300943
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 2300944
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 2300945
    aget v3, v2, v1

    if-lez v3, :cond_2

    aget v2, v2, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, LX/Fv9;->g:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    if-ge v2, v3, :cond_2

    :goto_0
    move v1, v1

    .line 2300946
    if-eqz v1, :cond_1

    .line 2300947
    :cond_0
    :goto_1
    return v0

    .line 2300948
    :cond_1
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2300949
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 2300950
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 2300951
    aget v2, v1, v4

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2300952
    iget-object v3, p0, LX/Fv9;->g:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    aget v1, v1, v4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v1, v4

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2300953
    sub-int/2addr v1, v2

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v1, v1

    .line 2300954
    div-int/lit8 v2, p2, 0x2

    .line 2300955
    if-ge v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 11

    .prologue
    .line 2300956
    iget-boolean v0, p0, LX/Fv9;->n:Z

    if-eqz v0, :cond_0

    .line 2300957
    :goto_0
    return-void

    .line 2300958
    :cond_0
    iget-object v0, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v1, p0, LX/Fv9;->b:Ljava/lang/String;

    .line 2300959
    sget-object v9, LX/9lQ;->SELF:LX/9lQ;

    const-string v10, "bio_add_prompt_impression"

    move-object v5, v0

    move-wide v6, v2

    move-object v8, v1

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2300960
    if-eqz v4, :cond_1

    .line 2300961
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300962
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->n:Z

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 11

    .prologue
    .line 2300963
    iget-boolean v0, p0, LX/Fv9;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fv9;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ad()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, LX/Fv9;->e(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2300964
    :cond_0
    :goto_0
    return-void

    .line 2300965
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->t:Z

    .line 2300966
    iget-object v0, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v1, p0, LX/Fv9;->b:Ljava/lang/String;

    .line 2300967
    sget-object v9, LX/9lQ;->SELF:LX/9lQ;

    const-string v10, "fav_photos_add_prompt_impression"

    move-object v5, v0

    move-wide v6, v2

    move-object v8, v1

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2300968
    if-eqz v4, :cond_2

    .line 2300969
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300970
    :cond_2
    goto :goto_0
.end method

.method public final b(Z)V
    .locals 14

    .prologue
    .line 2300971
    iget-boolean v0, p0, LX/Fv9;->l:Z

    if-eqz v0, :cond_0

    .line 2300972
    :goto_0
    return-void

    .line 2300973
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->l:Z

    .line 2300974
    iget-object v0, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v4, p0, LX/Fv9;->b:Ljava/lang/String;

    iget-object v5, p0, LX/Fv9;->k:LX/9lQ;

    move v1, p1

    .line 2300975
    const-string v12, "intro_card_impression"

    move-object v7, v0

    move-wide v8, v2

    move-object v10, v4

    move-object v11, v5

    invoke-static/range {v7 .. v12}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2300976
    if-eqz v6, :cond_1

    .line 2300977
    const-string v7, "event_metadata"

    new-instance v8, LX/0P2;

    invoke-direct {v8}, LX/0P2;-><init>()V

    const-string v9, "has_content"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2300978
    iget-object v7, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300979
    :cond_1
    goto :goto_0
.end method

.method public final e()V
    .locals 11

    .prologue
    .line 2300995
    iget-boolean v0, p0, LX/Fv9;->v:Z

    if-eqz v0, :cond_0

    .line 2300996
    :goto_0
    return-void

    .line 2300997
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->v:Z

    .line 2300998
    iget-object v0, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v1, p0, LX/Fv9;->b:Ljava/lang/String;

    .line 2300999
    sget-object v9, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    const-string v10, "non_self_add_intro_upsell_click"

    move-object v5, v0

    move-wide v6, v2

    move-object v8, v1

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2301000
    if-eqz v4, :cond_1

    .line 2301001
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2301002
    :cond_1
    goto :goto_0
.end method

.method public final f()V
    .locals 11

    .prologue
    .line 2301003
    iget-boolean v0, p0, LX/Fv9;->w:Z

    if-eqz v0, :cond_0

    .line 2301004
    :goto_0
    return-void

    .line 2301005
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fv9;->w:Z

    .line 2301006
    iget-object v0, p0, LX/Fv9;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fv9;->c:J

    iget-object v1, p0, LX/Fv9;->b:Ljava/lang/String;

    .line 2301007
    sget-object v9, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    const-string v10, "non_self_add_intro_upsell_impression"

    move-object v5, v0

    move-wide v6, v2

    move-object v8, v1

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2301008
    if-eqz v4, :cond_1

    .line 2301009
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2301010
    :cond_1
    goto :goto_0
.end method
