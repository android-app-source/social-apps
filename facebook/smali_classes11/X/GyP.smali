.class public final LX/GyP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GyQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

.field public final synthetic b:LX/GyQ;


# direct methods
.method public constructor <init>(LX/GyQ;)V
    .locals 1

    .prologue
    .line 2410075
    iput-object p1, p0, LX/GyP;->b:LX/GyQ;

    .line 2410076
    move-object v0, p1

    .line 2410077
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2410078
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2410063
    const-string v0, "PlaceComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2410064
    if-ne p0, p1, :cond_1

    .line 2410065
    :cond_0
    :goto_0
    return v0

    .line 2410066
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2410067
    goto :goto_0

    .line 2410068
    :cond_3
    check-cast p1, LX/GyP;

    .line 2410069
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2410070
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2410071
    if-eq v2, v3, :cond_0

    .line 2410072
    iget-object v2, p0, LX/GyP;->a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/GyP;->a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    iget-object v3, p1, LX/GyP;->a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2410073
    goto :goto_0

    .line 2410074
    :cond_4
    iget-object v2, p1, LX/GyP;->a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
