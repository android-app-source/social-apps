.class public final LX/F85;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2203285
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2203286
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203287
    :goto_0
    return v1

    .line 2203288
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203289
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2203290
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2203291
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203292
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2203293
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2203294
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2203295
    :cond_2
    const-string v6, "mutual_friends"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2203296
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2203297
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_b

    .line 2203298
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203299
    :goto_2
    move v3, v5

    .line 2203300
    goto :goto_1

    .line 2203301
    :cond_3
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2203302
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2203303
    :cond_4
    const-string v6, "profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2203304
    const/4 v5, 0x0

    .line 2203305
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_f

    .line 2203306
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203307
    :goto_3
    move v0, v5

    .line 2203308
    goto :goto_1

    .line 2203309
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2203310
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2203311
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2203312
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2203313
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2203314
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 2203315
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 2203316
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2203317
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203318
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_7

    if-eqz v8, :cond_7

    .line 2203319
    const-string v9, "count"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2203320
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v6

    goto :goto_4

    .line 2203321
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2203322
    :cond_9
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2203323
    if-eqz v3, :cond_a

    .line 2203324
    invoke-virtual {p1, v5, v7, v5}, LX/186;->a(III)V

    .line 2203325
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_b
    move v3, v5

    move v7, v5

    goto :goto_4

    .line 2203326
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203327
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_e

    .line 2203328
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2203329
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203330
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_d

    if-eqz v6, :cond_d

    .line 2203331
    const-string v7, "uri"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2203332
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 2203333
    :cond_e
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2203334
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2203335
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_f
    move v0, v5

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2203336
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2203337
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2203338
    if-eqz v0, :cond_0

    .line 2203339
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203340
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2203341
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2203342
    if-eqz v0, :cond_2

    .line 2203343
    const-string v1, "mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203344
    const/4 v1, 0x0

    .line 2203345
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2203346
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2203347
    if-eqz v1, :cond_1

    .line 2203348
    const-string p3, "count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203349
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2203350
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2203351
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2203352
    if-eqz v0, :cond_3

    .line 2203353
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203354
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2203355
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2203356
    if-eqz v0, :cond_5

    .line 2203357
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203358
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2203359
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2203360
    if-eqz v1, :cond_4

    .line 2203361
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203362
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2203363
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2203364
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2203365
    return-void
.end method
