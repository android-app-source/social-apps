.class public LX/GKQ;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Landroid/text/style/ForegroundColorSpan;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/GKQ;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/GG8;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private e:Ljava/lang/String;

.field public f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

.field public g:Z

.field public final h:LX/GGW;

.field private final i:LX/2U3;

.field private final j:LX/1Ck;

.field private final k:LX/GG6;

.field private final l:LX/GKO;

.field public m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 2340688
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const v1, 0x7f0a00d3

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v0, LX/GKQ;->a:Landroid/text/style/ForegroundColorSpan;

    .line 2340689
    const-class v0, LX/GKQ;

    sput-object v0, LX/GKQ;->b:Ljava/lang/Class;

    .line 2340690
    sget-object v0, LX/GG8;->INVALID_URL:LX/GG8;

    sget-object v1, LX/GG8;->PHONE_NUMBER:LX/GG8;

    sget-object v2, LX/GG8;->PAGE_LIKE_BODY_TEXT:LX/GG8;

    sget-object v3, LX/GG8;->ADDRESS:LX/GG8;

    sget-object v4, LX/GG8;->PHOTO_NOT_UPLOADED:LX/GG8;

    sget-object v5, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    const/4 v6, 0x0

    new-array v6, v6, [LX/GG8;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/GKQ;->d:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/GG6;LX/2U3;LX/1Ck;LX/GGW;LX/GKO;)V
    .locals 0
    .param p5    # LX/GKO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340853
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340854
    iput-object p1, p0, LX/GKQ;->k:LX/GG6;

    .line 2340855
    iput-object p2, p0, LX/GKQ;->i:LX/2U3;

    .line 2340856
    iput-object p3, p0, LX/GKQ;->j:LX/1Ck;

    .line 2340857
    iput-object p4, p0, LX/GKQ;->h:LX/GGW;

    .line 2340858
    iput-object p5, p0, LX/GKQ;->l:LX/GKO;

    .line 2340859
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 1

    .prologue
    .line 2340849
    invoke-direct {p0}, LX/GKQ;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2340850
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b(Z)V

    .line 2340851
    new-instance v0, LX/GKN;

    invoke-direct {v0, p0}, LX/GKN;-><init>(LX/GKQ;)V

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setPencilOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340852
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3

    .prologue
    .line 2340826
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340827
    iput-object p2, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2340828
    iput-object p1, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    .line 2340829
    new-instance v0, LX/GKF;

    invoke-direct {v0, p0}, LX/GKF;-><init>(LX/GKQ;)V

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2340830
    invoke-direct {p0}, LX/GKQ;->c()V

    .line 2340831
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->I()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_3

    .line 2340832
    :cond_0
    iget-object v0, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->setIsLoading(Z)V

    .line 2340833
    iget-object v0, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    .line 2340834
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->b:Landroid/view/ViewGroup;

    move-object v0, v1

    .line 2340835
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2340836
    iget-object v0, p0, LX/GKQ;->h:LX/GGW;

    iget-object v1, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    .line 2340837
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->b:Landroid/view/ViewGroup;

    move-object v1, v2

    .line 2340838
    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->I()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/GGW;->a(Landroid/view/ViewGroup;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2340839
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2340840
    invoke-direct {p0}, LX/GKQ;->f()V

    .line 2340841
    :cond_1
    invoke-direct {p0, p2}, LX/GKQ;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340842
    :cond_2
    :goto_0
    return-void

    .line 2340843
    :cond_3
    invoke-direct {p0, p2}, LX/GKQ;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340844
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2340845
    invoke-static {p0}, LX/GKQ;->n(LX/GKQ;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, LX/GKQ;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2340846
    invoke-static {p0}, LX/GKQ;->h(LX/GKQ;)V

    goto :goto_0

    .line 2340847
    :cond_4
    invoke-direct {p0}, LX/GKQ;->f()V

    .line 2340848
    invoke-static {p0}, LX/GKQ;->h(LX/GKQ;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/GKQ;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2340822
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2340823
    sget-object v1, LX/GKQ;->a:Landroid/text/style/ForegroundColorSpan;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2340824
    iget-object v1, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2340825
    return-void
.end method

.method public static b(LX/GKQ;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 2

    .prologue
    .line 2340813
    iput-object p1, p0, LX/GKQ;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340814
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340815
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v1

    .line 2340816
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2340817
    invoke-direct {p0, p1}, LX/GKQ;->c(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2340818
    invoke-static {p0}, LX/GKQ;->h(LX/GKQ;)V

    .line 2340819
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340820
    new-instance v1, LX/GFp;

    invoke-direct {v1}, LX/GFp;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2340821
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2340785
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2340786
    :cond_0
    :goto_0
    return-void

    .line 2340787
    :cond_1
    sget-object v0, LX/GKE;->a:[I

    iget-object v1, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2340788
    :pswitch_0
    iget-object v0, p0, LX/GKQ;->k:LX/GG6;

    iget-object v1, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v2, 0x0

    .line 2340789
    instance-of v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-nez v3, :cond_3

    .line 2340790
    :cond_2
    :goto_1
    move-object v0, v2

    .line 2340791
    if-eqz v0, :cond_0

    .line 2340792
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2340793
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2340794
    sget-short v2, LX/GDK;->v:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2340795
    iget-object v1, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v2, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080b9a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(Ljava/lang/String;)V

    .line 2340796
    iget-object v1, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    new-instance v2, LX/GKG;

    invoke-direct {v2, p0, v0}, LX/GKG;-><init>(LX/GKQ;Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2340797
    :cond_3
    check-cast v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340798
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2340799
    if-eqz v3, :cond_2

    .line 2340800
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2340801
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2340802
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2340803
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2340804
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2340805
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2340806
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2340807
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2340808
    if-eqz v3, :cond_2

    .line 2340809
    new-instance v2, LX/89k;

    invoke-direct {v2}, LX/89k;-><init>()V

    .line 2340810
    iput-object v3, v2, LX/89k;->b:Ljava/lang/String;

    .line 2340811
    move-object v3, v2

    .line 2340812
    iget-object v4, v0, LX/GG6;->f:LX/0hy;

    iget-object v2, v0, LX/GG6;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {v3}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v3

    invoke-interface {v4, v2, v3}, LX/0hy;->a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private c(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 2

    .prologue
    .line 2340774
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h(Ljava/lang/String;)V

    .line 2340775
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340776
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    move-object v1, v1

    .line 2340777
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    .line 2340778
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340779
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v1

    .line 2340780
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2340781
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340782
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v1

    .line 2340783
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2340784
    return-void
.end method

.method public static d(LX/GKQ;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2340771
    iget-object v0, p0, LX/GKQ;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2340772
    iget-object v0, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080adf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GKQ;->e:Ljava/lang/String;

    .line 2340773
    :cond_0
    iget-object v0, p0, LX/GKQ;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static e$redex0(LX/GKQ;)V
    .locals 7

    .prologue
    .line 2340759
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340760
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2340761
    iget-object v2, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2340762
    iget-object v3, p0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2340763
    iget-object p0, v3, LX/GCE;->f:LX/GG3;

    move-object v3, p0

    .line 2340764
    invoke-virtual {v3, v0}, LX/GG3;->v(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2340765
    invoke-static {v0}, LX/GHB;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)LX/8wL;

    move-result-object v4

    .line 2340766
    const v5, 0x7f080afa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v4, v5, v6}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2340767
    const-string v5, "data"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2340768
    move-object v4, v4

    .line 2340769
    new-instance v5, LX/GFS;

    const/16 v6, 0x9

    const/4 p0, 0x1

    invoke-direct {v5, v4, v6, p0}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v1, v5}, LX/GCE;->a(LX/8wN;)V

    .line 2340770
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2340745
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340746
    new-instance v1, LX/GKH;

    invoke-direct {v1, p0}, LX/GKH;-><init>(LX/GKQ;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2340747
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340748
    const/16 v1, 0x9

    new-instance v2, LX/GKI;

    invoke-direct {v2, p0}, LX/GKI;-><init>(LX/GKQ;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2340749
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340750
    new-instance v1, LX/GKJ;

    invoke-direct {v1, p0}, LX/GKJ;-><init>(LX/GKQ;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2340751
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340752
    new-instance v1, LX/GKK;

    invoke-direct {v1, p0}, LX/GKK;-><init>(LX/GKQ;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2340753
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340754
    new-instance v1, LX/GKL;

    invoke-direct {v1, p0}, LX/GKL;-><init>(LX/GKQ;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2340755
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340756
    iget-object v1, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340757
    new-instance v2, LX/GMe;

    invoke-direct {v2, v1, v0}, LX/GMe;-><init>(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;)V

    invoke-virtual {v0, v2}, LX/GCE;->a(LX/8wQ;)V

    .line 2340758
    return-void
.end method

.method public static g(LX/GKQ;)V
    .locals 4

    .prologue
    .line 2340860
    const-string v0, "\n"

    .line 2340861
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2340862
    const-string v2, "Got null after returning from creative edit activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Objective: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v3

    invoke-virtual {v3}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Page: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Account Id: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2340863
    iget-object v0, p0, LX/GKQ;->i:LX/2U3;

    sget-object v2, LX/GKQ;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2340864
    return-void
.end method

.method public static h(LX/GKQ;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2340736
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340737
    sget-object v1, LX/GKQ;->d:LX/0Rf;

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/0Rf;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2340738
    invoke-static {p0}, LX/GKQ;->i(LX/GKQ;)V

    .line 2340739
    :goto_0
    return-void

    .line 2340740
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GKQ;->g:Z

    .line 2340741
    iget-object v0, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2340742
    iget-object v0, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->setIsLoading(Z)V

    .line 2340743
    iget-object v0, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2340744
    iget-object v0, p0, LX/GKQ;->j:LX/1Ck;

    sget-object v1, LX/GKP;->FETCH_AD_PREVIEW:LX/GKP;

    iget-object v2, p0, LX/GKQ;->l:LX/GKO;

    iget-object v3, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {v2, v3}, LX/GKO;->a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/GKM;

    invoke-direct {v3, p0}, LX/GKM;-><init>(LX/GKQ;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public static i(LX/GKQ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2340727
    iget-object v0, p0, LX/GKQ;->j:LX/1Ck;

    sget-object v1, LX/GKP;->FETCH_AD_PREVIEW:LX/GKP;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2340728
    iget-object v0, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->setIsLoading(Z)V

    .line 2340729
    iget-object v0, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2340730
    iget-object v0, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    .line 2340731
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->b:Landroid/view/ViewGroup;

    move-object v0, v1

    .line 2340732
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2340733
    invoke-static {p0}, LX/GKQ;->d(LX/GKQ;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/GKQ;->a$redex0(LX/GKQ;Ljava/lang/String;)V

    .line 2340734
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GKQ;->g:Z

    .line 2340735
    return-void
.end method

.method public static j$redex0(LX/GKQ;)V
    .locals 4

    .prologue
    .line 2340722
    invoke-static {p0}, LX/GKQ;->d(LX/GKQ;)Ljava/lang/String;

    move-result-object v0

    .line 2340723
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2340724
    new-instance v2, LX/GFO;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0}, LX/GFO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2340725
    invoke-static {p0, v0}, LX/GKQ;->a$redex0(LX/GKQ;Ljava/lang/String;)V

    .line 2340726
    return-void
.end method

.method private m()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2340714
    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v2}, LX/GG6;->k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2340715
    :cond_0
    :goto_0
    return v0

    .line 2340716
    :cond_1
    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v2, v3, :cond_2

    .line 2340717
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2340718
    iget-object v3, v2, LX/GCE;->e:LX/0ad;

    move-object v2, v3

    .line 2340719
    sget-short v3, LX/GDK;->e:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 2340720
    goto :goto_0

    .line 2340721
    :cond_2
    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static n(LX/GKQ;)Z
    .locals 2

    .prologue
    .line 2340713
    iget-object v0, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Z
    .locals 3

    .prologue
    .line 2340710
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340711
    iget-object v1, v0, LX/GCE;->e:LX/0ad;

    move-object v0, v1

    .line 2340712
    sget-short v1, LX/GDK;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2340705
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340706
    iget-object v0, p0, LX/GKQ;->j:LX/1Ck;

    sget-object v1, LX/GKP;->FETCH_AD_PREVIEW:LX/GKP;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2340707
    iput-object v2, p0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    .line 2340708
    iput-object v2, p0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2340709
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2340701
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2340702
    iget-object v0, p0, LX/GKQ;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2340703
    const-string v0, "edited_creative_data"

    iget-object v1, p0, LX/GKQ;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2340704
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2340700
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    invoke-direct {p0, p1, p2}, LX/GKQ;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2340697
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340698
    iput-object p1, p0, LX/GKQ;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340699
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2340691
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2340692
    if-eqz p1, :cond_0

    .line 2340693
    const-string v0, "edited_creative_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GKQ;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340694
    iget-object v0, p0, LX/GKQ;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2340695
    iget-object v0, p0, LX/GKQ;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {p0, v0}, LX/GKQ;->b(LX/GKQ;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2340696
    :cond_0
    return-void
.end method
