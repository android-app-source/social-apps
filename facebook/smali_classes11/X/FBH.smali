.class public final LX/FBH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/orca/DiodeLoginPromptFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/orca/DiodeLoginPromptFragment;)V
    .locals 0

    .prologue
    .line 2209092
    iput-object p1, p0, LX/FBH;->a:Lcom/facebook/katana/orca/DiodeLoginPromptFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x49912346    # 1188968.8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209093
    iget-object v1, p0, LX/FBH;->a:Lcom/facebook/katana/orca/DiodeLoginPromptFragment;

    const-string v2, "login_prompt_open_messenger_button"

    .line 2209094
    iget-object v3, v1, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->c:LX/0Zb;

    const-string v5, "click"

    const/4 p1, 0x1

    invoke-interface {v3, v5, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2209095
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2209096
    const-string v5, "diode_qp_module"

    invoke-virtual {v3, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2209097
    const-string v5, "button"

    invoke-virtual {v3, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2209098
    invoke-virtual {v3, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2209099
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2209100
    :cond_0
    iget-object v1, p0, LX/FBH;->a:Lcom/facebook/katana/orca/DiodeLoginPromptFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.facebook.orca"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2209101
    iget-object v2, p0, LX/FBH;->a:Lcom/facebook/katana/orca/DiodeLoginPromptFragment;

    iget-object v2, v2, Lcom/facebook/katana/orca/DiodeLoginPromptFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/FBH;->a:Lcom/facebook/katana/orca/DiodeLoginPromptFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2209102
    const v1, 0x4d252cf2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
