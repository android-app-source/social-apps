.class public LX/GBD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2326624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326625
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2326626
    check-cast p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;

    .line 2326627
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2326628
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "sfdid"

    .line 2326629
    iget-object v3, p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2326630
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2326631
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fdid"

    .line 2326632
    iget-object v3, p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoveryActivationsParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2326633
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2326634
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fdrActivations"

    .line 2326635
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2326636
    move-object v1, v1

    .line 2326637
    const-string v2, "POST"

    .line 2326638
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2326639
    move-object v1, v1

    .line 2326640
    const-string v2, "fdr/activations"

    .line 2326641
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2326642
    move-object v1, v1

    .line 2326643
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2326644
    move-object v0, v1

    .line 2326645
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2326646
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2326647
    move-object v0, v0

    .line 2326648
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2326649
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2326650
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2326651
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
