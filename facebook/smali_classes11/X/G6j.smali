.class public LX/G6j;
.super LX/1OM;
.source ""

# interfaces
.implements LX/FYf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<IA:",
        "LX/1OM",
        "<-",
        "LX/1a1;",
        ">;:",
        "LX/FYf",
        "<-",
        "LX/1a1;",
        ">;>",
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/FYf",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/G6i;

.field private final b:Landroid/util/SparseIntArray;

.field public final c:[LX/1OM;

.field private final d:LX/G6h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/G6j",
            "<TIA;>.ForwardingDataObserver;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1OM;LX/1OM;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TIA;",
            "LX/1OM;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2320731
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2320732
    new-instance v0, LX/G6i;

    invoke-direct {v0}, LX/G6i;-><init>()V

    iput-object v0, p0, LX/G6j;->a:LX/G6i;

    .line 2320733
    new-instance v0, LX/G6h;

    invoke-direct {v0, p0}, LX/G6h;-><init>(LX/G6j;)V

    iput-object v0, p0, LX/G6j;->d:LX/G6h;

    .line 2320734
    invoke-virtual {p1}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-super {p0, v0}, LX/1OM;->a(Z)V

    .line 2320735
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/G6j;->b:Landroid/util/SparseIntArray;

    .line 2320736
    const/4 v0, 0x2

    new-array v0, v0, [LX/1OM;

    iput-object v0, p0, LX/G6j;->c:[LX/1OM;

    .line 2320737
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aput-object p1, v0, v2

    .line 2320738
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aput-object p2, v0, v1

    .line 2320739
    return-void

    :cond_0
    move v0, v2

    .line 2320740
    goto :goto_0
.end method

.method public static a(LX/FYf;I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2320725
    move v1, v0

    .line 2320726
    :goto_0
    if-ge v1, p1, :cond_1

    .line 2320727
    invoke-interface {p0, v1}, LX/FYf;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2320728
    add-int/lit8 v0, v0, 0x1

    .line 2320729
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2320730
    :cond_1
    return v0
.end method

.method private a(LX/G6i;I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2320705
    if-gez p2, :cond_0

    .line 2320706
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2320707
    :cond_0
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v3

    check-cast v0, LX/FYf;

    .line 2320708
    invoke-interface {v0, p2}, LX/FYf;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2320709
    iput v3, p1, LX/G6i;->a:I

    .line 2320710
    iget-object v1, p0, LX/G6j;->c:[LX/1OM;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    check-cast v1, LX/FYf;

    invoke-static {v1, p2}, LX/G6j;->a(LX/FYf;I)I

    move-result v1

    move v1, v1

    .line 2320711
    iput v1, p1, LX/G6i;->b:I

    .line 2320712
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 2320713
    iput v5, p1, LX/G6i;->c:I

    .line 2320714
    :goto_0
    return-void

    .line 2320715
    :cond_1
    add-int/lit8 v1, p2, 0x1

    invoke-interface {v0, v1}, LX/FYf;->b(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2320716
    iput v4, p1, LX/G6i;->c:I

    goto :goto_0

    .line 2320717
    :cond_2
    iput v4, p1, LX/G6i;->a:I

    .line 2320718
    iget-object v1, p0, LX/G6j;->c:[LX/1OM;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    check-cast v1, LX/FYf;

    invoke-static {v1, p2}, LX/G6j;->a(LX/FYf;I)I

    move-result v1

    sub-int v1, p2, v1

    move v1, v1

    .line 2320719
    iput v1, p1, LX/G6i;->b:I

    .line 2320720
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 2320721
    iput v5, p1, LX/G6i;->c:I

    goto :goto_0

    .line 2320722
    :cond_3
    add-int/lit8 v1, p2, 0x1

    invoke-interface {v0, v1}, LX/FYf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2320723
    iput v4, p1, LX/G6i;->c:I

    goto :goto_0

    .line 2320724
    :cond_4
    iput v3, p1, LX/G6i;->c:I

    goto :goto_0
.end method

.method private g(I)LX/G6i;
    .locals 1

    .prologue
    .line 2320703
    iget-object v0, p0, LX/G6j;->a:LX/G6i;

    invoke-direct {p0, v0, p1}, LX/G6j;->a(LX/G6i;I)V

    .line 2320704
    iget-object v0, p0, LX/G6j;->a:LX/G6i;

    return-object v0
.end method


# virtual methods
.method public final C_(I)J
    .locals 9

    .prologue
    .line 2320650
    invoke-direct {p0, p1}, LX/G6j;->g(I)LX/G6i;

    move-result-object v0

    .line 2320651
    iget-object v1, p0, LX/G6j;->c:[LX/1OM;

    iget v2, v0, LX/G6i;->a:I

    aget-object v1, v1, v2

    .line 2320652
    iget v0, v0, LX/G6i;->b:I

    invoke-virtual {v1, v0}, LX/1OM;->C_(I)J

    move-result-wide v2

    .line 2320653
    const-wide/16 v4, 0x1f

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    add-long/2addr v4, v6

    move-wide v0, v4

    .line 2320654
    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2320700
    iget-object v0, p0, LX/G6j;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 2320701
    iget-object v1, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    .line 2320702
    return-object v0
.end method

.method public final a(LX/1a1;)V
    .locals 3

    .prologue
    .line 2320696
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    iget-object v1, p0, LX/G6j;->b:Landroid/util/SparseIntArray;

    .line 2320697
    iget v2, p1, LX/1a1;->e:I

    move v2, v2

    .line 2320698
    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2320699
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2320685
    invoke-direct {p0, p2}, LX/G6j;->g(I)LX/G6i;

    move-result-object v1

    .line 2320686
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    iget v2, v1, LX/G6i;->a:I

    aget-object v0, v0, v2

    .line 2320687
    instance-of v2, v0, LX/FYR;

    if-eqz v2, :cond_1

    .line 2320688
    check-cast v0, LX/FYR;

    iget v2, v1, LX/G6i;->b:I

    iget v1, v1, LX/G6i;->c:I

    .line 2320689
    iget-object p0, v0, LX/EkN;->b:Landroid/database/Cursor;

    move-object p0, p0

    .line 2320690
    if-eqz p0, :cond_0

    .line 2320691
    iget-object p0, v0, LX/EkN;->b:Landroid/database/Cursor;

    move-object p0, p0

    .line 2320692
    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2320693
    check-cast p1, LX/FYb;

    invoke-virtual {v0}, LX/EkO;->d()LX/AU0;

    move-result-object p0

    check-cast p0, LX/BO1;

    invoke-virtual {p1, p0, v2, v1}, LX/FYb;->a(LX/BO1;II)V

    .line 2320694
    :cond_0
    :goto_0
    return-void

    .line 2320695
    :cond_1
    iget v1, v1, LX/G6i;->b:I

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2320682
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/1OM;->a(Z)V

    .line 2320683
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/1OM;->a(Z)V

    .line 2320684
    return-void
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2320677
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v2

    iget-object v1, p0, LX/G6j;->d:LX/G6h;

    invoke-virtual {v0, v1}, LX/1OM;->a(LX/1OD;)V

    .line 2320678
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, LX/1OM;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 2320679
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v3

    iget-object v1, p0, LX/G6j;->d:LX/G6h;

    invoke-virtual {v0, v1}, LX/1OM;->a(LX/1OD;)V

    .line 2320680
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, LX/1OM;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 2320681
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2320672
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2320673
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v1

    iget-object v1, p0, LX/G6j;->d:LX/G6h;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1OD;)V

    .line 2320674
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2320675
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    aget-object v0, v0, v2

    iget-object v1, p0, LX/G6j;->d:LX/G6h;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1OD;)V

    .line 2320676
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 2320671
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1a1;)Z
    .locals 3

    .prologue
    .line 2320668
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    iget-object v1, p0, LX/G6j;->b:Landroid/util/SparseIntArray;

    .line 2320669
    iget v2, p1, LX/1a1;->e:I

    move v2, v2

    .line 2320670
    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/1OM;->b(LX/1a1;)Z

    move-result v0

    return v0
.end method

.method public final c(LX/1a1;)V
    .locals 3

    .prologue
    .line 2320664
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    iget-object v1, p0, LX/G6j;->b:Landroid/util/SparseIntArray;

    .line 2320665
    iget v2, p1, LX/1a1;->e:I

    move v2, v2

    .line 2320666
    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/1OM;->c(LX/1a1;)V

    .line 2320667
    return-void
.end method

.method public final d(LX/1a1;)V
    .locals 3

    .prologue
    .line 2320660
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    iget-object v1, p0, LX/G6j;->b:Landroid/util/SparseIntArray;

    .line 2320661
    iget v2, p1, LX/1a1;->e:I

    move v2, v2

    .line 2320662
    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/1OM;->d(LX/1a1;)V

    .line 2320663
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 2320656
    invoke-direct {p0, p1}, LX/G6j;->g(I)LX/G6i;

    move-result-object v0

    .line 2320657
    iget-object v1, p0, LX/G6j;->c:[LX/1OM;

    iget v2, v0, LX/G6i;->a:I

    aget-object v1, v1, v2

    iget v2, v0, LX/G6i;->b:I

    invoke-virtual {v1, v2}, LX/1OM;->getItemViewType(I)I

    move-result v1

    .line 2320658
    iget-object v2, p0, LX/G6j;->b:Landroid/util/SparseIntArray;

    iget v0, v0, LX/G6i;->a:I

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 2320659
    return v1
.end method

.method public final ij_()I
    .locals 3

    .prologue
    .line 2320655
    iget-object v0, p0, LX/G6j;->c:[LX/1OM;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    iget-object v1, p0, LX/G6j;->c:[LX/1OM;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
