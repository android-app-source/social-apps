.class public final LX/GCR;
.super LX/GCQ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V
    .locals 0

    .prologue
    .line 2328329
    iput-object p1, p0, LX/GCR;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-direct {p0}, LX/GCQ;-><init>()V

    return-void
.end method

.method private a(LX/GFO;)V
    .locals 2

    .prologue
    .line 2328330
    iget-object v0, p0, LX/GCR;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    new-instance v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$13$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity$13$1;-><init>(LX/GCR;LX/GFO;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2328331
    return-void
.end method

.method public static a$redex0(LX/GCR;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0    # LX/GCR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2328332
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2328333
    iget-object v0, p0, LX/GCR;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2328334
    :cond_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2328335
    iget-object v0, p0, LX/GCR;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, LX/GCR;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->s(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/0kb;

    move-result-object v0

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f080039

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 2328336
    :cond_1
    iget-object v0, p0, LX/GCR;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->t(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)LX/1CX;

    move-result-object v0

    iget-object v1, p0, LX/GCR;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v1

    .line 2328337
    iput-object p1, v1, LX/4mn;->b:Ljava/lang/String;

    .line 2328338
    move-object v1, v1

    .line 2328339
    iput-object p2, v1, LX/4mn;->c:Ljava/lang/String;

    .line 2328340
    move-object v1, v1

    .line 2328341
    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 2328342
    return-void

    .line 2328343
    :cond_2
    const v0, 0x7f08003a

    goto :goto_0
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2328344
    check-cast p1, LX/GFO;

    invoke-direct {p0, p1}, LX/GCR;->a(LX/GFO;)V

    return-void
.end method
