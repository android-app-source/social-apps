.class public LX/GOy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/GNs;

.field private final b:LX/0Uh;

.field private final c:LX/ADT;

.field private final d:LX/0W9;

.field private final e:LX/ADW;


# direct methods
.method public constructor <init>(LX/GNs;LX/0Uh;LX/0W9;LX/ADW;LX/ADT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2348270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2348271
    iput-object p1, p0, LX/GOy;->a:LX/GNs;

    .line 2348272
    iput-object p3, p0, LX/GOy;->d:LX/0W9;

    .line 2348273
    iput-object p4, p0, LX/GOy;->e:LX/ADW;

    .line 2348274
    iput-object p2, p0, LX/GOy;->b:LX/0Uh;

    .line 2348275
    iput-object p5, p0, LX/GOy;->c:LX/ADT;

    .line 2348276
    return-void
.end method

.method public static a(Lcom/facebook/common/locale/Country;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2348277
    const-string v0, "BRL"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0, p0}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GOy;
    .locals 6

    .prologue
    .line 2348278
    new-instance v0, LX/GOy;

    invoke-static {p0}, LX/GNs;->a(LX/0QB;)LX/GNs;

    move-result-object v1

    check-cast v1, LX/GNs;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {p0}, LX/ADW;->a(LX/0QB;)LX/ADW;

    move-result-object v4

    check-cast v4, LX/ADW;

    invoke-static {p0}, LX/ADT;->b(LX/0QB;)LX/ADT;

    move-result-object v5

    check-cast v5, LX/ADT;

    invoke-direct/range {v0 .. v5}, LX/GOy;-><init>(LX/GNs;LX/0Uh;LX/0W9;LX/ADW;LX/ADT;)V

    .line 2348279
    return-object v0
.end method


# virtual methods
.method public final a(ZLcom/facebook/common/locale/Country;Ljava/lang/String;)LX/GOx;
    .locals 3
    .param p2    # Lcom/facebook/common/locale/Country;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348280
    iget-object v0, p0, LX/GOy;->b:LX/0Uh;

    const/16 v1, 0x5be

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2348281
    sget-object v0, LX/GOx;->DIALOG_US:LX/GOx;

    .line 2348282
    :goto_0
    return-object v0

    .line 2348283
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, p2}, LX/GOy;->a(Lcom/facebook/common/locale/Country;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    invoke-static {v0, p3}, LX/GOy;->a(Lcom/facebook/common/locale/Country;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2348284
    sget-object v0, LX/GOx;->BRAZILIAN_TAX_ID:LX/GOx;

    goto :goto_0

    .line 2348285
    :cond_1
    sget-object v0, LX/GOx;->SCREEN_INTERNATIONAL:LX/GOx;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/common/locale/Country;)Lcom/facebook/common/locale/Country;
    .locals 1
    .param p1    # Lcom/facebook/common/locale/Country;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348286
    iget-object v0, p0, LX/GOy;->d:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/util/Locale;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    invoke-static {p1, v0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    return-object v0
.end method

.method public final a(Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;ZLcom/facebook/common/locale/Country;)V
    .locals 2

    .prologue
    .line 2348287
    new-instance v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    const-string v1, "payments_android_initiate_ads_payments_flow"

    invoke-direct {v0, v1, p1}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    invoke-virtual {p1}, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v1}, LX/GOy;->a(ZLcom/facebook/common/locale/Country;Ljava/lang/String;)LX/GOx;

    move-result-object v1

    invoke-virtual {v1}, LX/GOx;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2348288
    const-string p1, "ads_payments_experience"

    invoke-virtual {v0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2348289
    move-object v0, v0

    .line 2348290
    iget-object v1, p0, LX/GOy;->e:LX/ADW;

    invoke-virtual {v1, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2348291
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 10

    .prologue
    .line 2348292
    iget-object v0, p0, LX/GOy;->c:LX/ADT;

    .line 2348293
    iget-object v1, v0, LX/ADT;->a:LX/0Uh;

    const/16 v2, 0x56f

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v7

    .line 2348294
    iget-object v8, v0, LX/ADT;->b:LX/0Zb;

    const-string v9, "payments_gk_check"

    const-string v1, "payment_account_id"

    const-string v3, "gk_name"

    const-string v4, "mobile_ads_payments_holdout_2016_h2"

    const-string v5, "gk_value"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object v2, p1

    invoke-static/range {v1 .. v6}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    invoke-interface {v8, v9, v1}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2348295
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v7, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2348296
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
