.class public final LX/Grk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Grn;


# direct methods
.method public constructor <init>(LX/Grn;)V
    .locals 0

    .prologue
    .line 2398672
    iput-object p1, p0, LX/Grk;->a:LX/Grn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4f1da843

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2398673
    iget-object v0, p0, LX/Grk;->a:LX/Grn;

    iget-boolean v0, v0, LX/Grn;->o:Z

    if-eqz v0, :cond_0

    .line 2398674
    iget-object v0, p0, LX/Grk;->a:LX/Grn;

    const/4 p1, 0x1

    .line 2398675
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/Grn;->o:Z

    .line 2398676
    iput-boolean p1, v0, LX/Grn;->h:Z

    .line 2398677
    iget-object v2, v0, LX/Grn;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget v3, v0, LX/Grn;->e:I

    invoke-static {v0, v2, v3}, LX/Grn;->a(LX/Grn;II)V

    .line 2398678
    iget-object v2, v0, LX/Grn;->l:Lcom/facebook/fbui/glyph/GlyphView;

    const v3, 0x7f0209b3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2398679
    invoke-static {v0, p1}, LX/Grn;->a(LX/Grn;Z)V

    .line 2398680
    iget-object v0, p0, LX/Grk;->a:LX/Grn;

    iget-object v0, v0, LX/Grn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0y;

    iget-object v2, p0, LX/Grk;->a:LX/Grn;

    iget-object v2, v2, LX/Grn;->i:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v2}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->f()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/D0y;->b(Ljava/util/Map;)V

    .line 2398681
    :goto_0
    const v0, -0x1ae327e8

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2398682
    :cond_0
    iget-object v0, p0, LX/Grk;->a:LX/Grn;

    const/4 p1, 0x0

    .line 2398683
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Grn;->o:Z

    .line 2398684
    iput-boolean p1, v0, LX/Grn;->h:Z

    .line 2398685
    iget-object v2, v0, LX/Grn;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iput v2, v0, LX/Grn;->e:I

    .line 2398686
    iget v2, v0, LX/Grn;->e:I

    iget v3, v0, LX/Grn;->m:I

    invoke-static {v0, v2, v3}, LX/Grn;->a(LX/Grn;II)V

    .line 2398687
    iget-object v2, v0, LX/Grn;->l:Lcom/facebook/fbui/glyph/GlyphView;

    const v3, 0x7f0209b0

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2398688
    invoke-static {v0, p1}, LX/Grn;->a(LX/Grn;Z)V

    .line 2398689
    iget-object v0, p0, LX/Grk;->a:LX/Grn;

    iget-object v0, v0, LX/Grn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0y;

    iget-object v2, p0, LX/Grk;->a:LX/Grn;

    iget-object v2, v2, LX/Grn;->i:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v2}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->f()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/D0y;->a(Ljava/util/Map;)V

    goto :goto_0
.end method
