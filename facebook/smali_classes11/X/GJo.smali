.class public LX/GJo;
.super LX/GJJ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GJJ",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;",
        ">;"
    }
.end annotation


# instance fields
.field private g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

.field private h:I

.field private i:LX/2U3;


# direct methods
.method public constructor <init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339675
    invoke-direct/range {p0 .. p11}, LX/GJJ;-><init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2339676
    const/4 v0, 0x1

    iput v0, p0, LX/GJo;->c:I

    .line 2339677
    iput-object p2, p0, LX/GJo;->i:LX/2U3;

    .line 2339678
    return-void
.end method

.method public static c(LX/0QB;)LX/GJo;
    .locals 12

    .prologue
    .line 2339769
    new-instance v0, LX/GJo;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v1

    check-cast v1, LX/GG6;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v2

    check-cast v2, LX/2U3;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p0}, LX/GMU;->a(LX/0QB;)LX/GMU;

    move-result-object v4

    check-cast v4, LX/GMU;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/GE2;->b(LX/0QB;)LX/GE2;

    move-result-object v6

    check-cast v6, LX/GE2;

    const-class v7, LX/GLN;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/GLN;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v0 .. v11}, LX/GJo;-><init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2339770
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 2339765
    invoke-super {p0, p1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2339766
    if-nez p1, :cond_0

    .line 2339767
    const/4 v0, 0x0

    .line 2339768
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080b92

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2339764
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {p0, p1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V
    .locals 1

    .prologue
    .line 2339760
    invoke-super {p0, p1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V

    .line 2339761
    iput-object p1, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2339762
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    iput v0, p0, LX/GJo;->h:I

    .line 2339763
    return-void
.end method

.method public final b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 3

    .prologue
    .line 2339754
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    invoke-virtual {v0}, LX/GJD;->getSelectedIndex()I

    move-result v0

    .line 2339755
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    .line 2339756
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-object v0, v2

    .line 2339757
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne v1, v0, :cond_0

    .line 2339758
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2339759
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/GJJ;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2339747
    invoke-super {p0}, LX/GJJ;->c()V

    .line 2339748
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080b91

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->setLeaveUnchangedButtonText(Ljava/lang/CharSequence;)V

    .line 2339749
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget v1, p0, LX/GJo;->h:I

    invoke-static {v0, v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;I)I

    move-result v0

    if-gtz v0, :cond_0

    .line 2339750
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    .line 2339751
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-object v0, v1

    .line 2339752
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2339753
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2339726
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    invoke-virtual {v0}, LX/GJD;->e()V

    .line 2339727
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    move v1, v2

    .line 2339728
    :goto_0
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2339729
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2339730
    invoke-static {v3}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2339731
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->c(I)V

    .line 2339732
    :goto_1
    return-void

    .line 2339733
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2339734
    :cond_1
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2339735
    invoke-static {v3}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2339736
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-gtz v1, :cond_2

    .line 2339737
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->c()V

    goto :goto_1

    .line 2339738
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    if-gtz v1, :cond_3

    .line 2339739
    iget-object v0, p0, LX/GJo;->i:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v3, "AmountOffset is not positive"

    invoke-virtual {v0, v1, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2339740
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->c(I)V

    goto :goto_1

    .line 2339741
    :cond_3
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v3

    int-to-long v4, v3

    div-long/2addr v0, v4

    long-to-int v0, v0

    .line 2339742
    if-lez v0, :cond_4

    .line 2339743
    int-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    double-to-int v1, v2

    add-int/lit8 v2, v1, 0x1

    .line 2339744
    :cond_4
    iget-object v1, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextEditText(Ljava/lang/CharSequence;)V

    .line 2339745
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setSelectionOnEditText(I)V

    .line 2339746
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->d()V

    goto :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2339722
    invoke-super {p0}, LX/GJJ;->e()V

    .line 2339723
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339724
    new-instance v1, LX/GJn;

    invoke-direct {v1, p0}, LX/GJn;-><init>(LX/GJo;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2339725
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 2339714
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2339715
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v0

    .line 2339716
    sget-object v2, LX/GG8;->UNEDITED_DATA:LX/GG8;

    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339717
    invoke-static {v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result p0

    if-nez p0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result p0

    if-nez p0, :cond_1

    .line 2339718
    const/4 p0, 0x0

    .line 2339719
    :goto_0
    move v0, p0

    .line 2339720
    invoke-virtual {v1, v2, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2339721
    :cond_0
    return-void

    :cond_1
    const/4 p0, 0x1

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2339707
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2339708
    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    .line 2339709
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2339710
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2339711
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2339712
    invoke-static {v0}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2339713
    :cond_0
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final h()LX/GMT;
    .locals 6

    .prologue
    .line 2339687
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2339688
    sget-object v0, LX/GMT;->NONE:LX/GMT;

    .line 2339689
    :goto_0
    return-object v0

    .line 2339690
    :cond_0
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)I

    move-result v1

    .line 2339691
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    .line 2339692
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-object v0, v2

    .line 2339693
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2339694
    iget v0, p0, LX/GJo;->h:I

    iget-object v2, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v2

    add-int/2addr v0, v2

    if-ge v1, v0, :cond_1

    sget-object v0, LX/GMT;->MIN:LX/GMT;

    goto :goto_0

    :cond_1
    sget-object v0, LX/GMT;->NONE:LX/GMT;

    goto :goto_0

    .line 2339695
    :cond_2
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-static {v0}, LX/GMU;->d(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2339696
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339697
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v2

    .line 2339698
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2339699
    iget-object v2, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 2339700
    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2339701
    invoke-virtual {v2, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2339702
    const/4 v3, 0x0

    sget-object v4, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v0, v1, v3, v4}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    .line 2339703
    iget-object v1, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v1

    if-le v1, v0, :cond_3

    .line 2339704
    sget-object v0, LX/GMT;->MIN:LX/GMT;

    goto :goto_0

    .line 2339705
    :cond_3
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-static {v0}, LX/GMU;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v1

    int-to-long v4, v1

    invoke-static {v4, v5}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2339706
    invoke-virtual {v2, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_4

    sget-object v0, LX/GMT;->MAX:LX/GMT;

    goto :goto_0

    :cond_4
    sget-object v0, LX/GMT;->NONE:LX/GMT;

    goto :goto_0
.end method

.method public final i()Landroid/text/Spanned;
    .locals 2

    .prologue
    .line 2339682
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;

    .line 2339683
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;->f:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-object v0, v1

    .line 2339684
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2339685
    invoke-super {p0}, LX/GJJ;->i()Landroid/text/Spanned;

    move-result-object v0

    .line 2339686
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 6

    .prologue
    .line 2339679
    iget-object v0, p0, LX/GJo;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-static {v0}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/NumberFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v0

    .line 2339680
    iget-object v1, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v2, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v3, 0x7f080b93

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextPrefixTextView(Ljava/lang/CharSequence;)V

    .line 2339681
    return-void
.end method
