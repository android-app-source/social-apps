.class public LX/FFT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2217452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;)I
    .locals 1
    .param p0    # Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2217472
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0Px;)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2217473
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/FFT;->a(LX/0Px;Z)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;Z)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;",
            ">;Z)",
            "Landroid/util/Pair",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2217461
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2217462
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2217463
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;

    .line 2217464
    invoke-virtual {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 2217465
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2217466
    :goto_1
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2217467
    invoke-static {v5}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2217468
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2217469
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2217470
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2217471
    :cond_2
    new-instance v0, Landroid/util/Pair;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;)Landroid/util/Pair;
    .locals 4
    .param p0    # Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2217453
    const/4 v0, 0x1

    .line 2217454
    if-nez p0, :cond_0

    .line 2217455
    new-instance v1, Landroid/util/Pair;

    .line 2217456
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2217457
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2217458
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2217459
    :goto_0
    move-object v0, v1

    .line 2217460
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, v0}, LX/FFT;->a(LX/0Px;Z)Landroid/util/Pair;

    move-result-object v1

    goto :goto_0
.end method
