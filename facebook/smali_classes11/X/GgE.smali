.class public LX/GgE;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/24a;
.implements LX/2eZ;


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ImageView;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2378800
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 2378801
    invoke-direct {p0}, LX/GgE;->b()V

    .line 2378802
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2378803
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2378804
    invoke-direct {p0}, LX/GgE;->b()V

    .line 2378805
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2378813
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    .line 2378814
    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2378815
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2378816
    return-void

    .line 2378817
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2378806
    const v0, 0x7f030eb3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2378807
    const v0, 0x7f0d23ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/GgE;->a:Landroid/widget/LinearLayout;

    .line 2378808
    const v0, 0x7f0d23eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GgE;->b:Landroid/widget/TextView;

    .line 2378809
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GgE;->d:Landroid/widget/ImageView;

    .line 2378810
    const v0, 0x7f0d23ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GgE;->c:Landroid/view/View;

    .line 2378811
    iget-object v0, p0, LX/GgE;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2378812
    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 2378798
    iget-object v0, p0, LX/GgE;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 0

    .prologue
    .line 2378799
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2378786
    iget-boolean v0, p0, LX/GgE;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2378785
    invoke-direct {p0}, LX/GgE;->e()Z

    move-result v0

    return v0
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 2378796
    iget-object v0, p0, LX/GgE;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2378797
    return-void
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 2378787
    iput-boolean p1, p0, LX/GgE;->e:Z

    .line 2378788
    return-void
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2378789
    iget-object v0, p0, LX/GgE;->b:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/GgE;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 2378790
    iget-object v1, p0, LX/GgE;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2378791
    return-void

    .line 2378792
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 2378793
    iget-object v1, p0, LX/GgE;->d:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2378794
    return-void

    .line 2378795
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
