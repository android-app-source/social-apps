.class public LX/GJz;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

.field public b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

.field private c:LX/GG6;


# direct methods
.method public constructor <init>(LX/GG6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340074
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340075
    iput-object p1, p0, LX/GJz;->c:LX/GG6;

    .line 2340076
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2340077
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340078
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340079
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 6

    .prologue
    .line 2340080
    check-cast p1, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340081
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340082
    const v0, 0x7f080aa7

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2340083
    iput-object p1, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340084
    iget-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2340085
    iget-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2340086
    iget-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->c()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2340087
    iget-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->d()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2340088
    iget-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a()I

    move-result v0

    iget-object v1, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->c()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->d()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    add-int/2addr v1, v0

    .line 2340089
    iget-object v2, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    new-instance v0, LX/GG9;

    invoke-direct {v0}, LX/GG9;-><init>()V

    const/4 v3, 0x3

    .line 2340090
    iput v3, v0, LX/GG9;->g:I

    .line 2340091
    move-object v0, v0

    .line 2340092
    iget-object v3, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v3}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 2340093
    iput-object v3, v0, LX/GG9;->a:Ljava/lang/String;

    .line 2340094
    move-object v0, v0

    .line 2340095
    iget-object v3, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080aaf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2340096
    iput-object v3, v0, LX/GG9;->b:Ljava/lang/String;

    .line 2340097
    move-object v0, v0

    .line 2340098
    iget-object v3, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0357

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0358

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0a0359

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2340099
    iput-object v3, v0, LX/GG9;->f:LX/0Px;

    .line 2340100
    move-object v3, v0

    .line 2340101
    iget-object v0, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f0044

    :goto_0
    iget-object v5, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a()I

    move-result v5

    invoke-virtual {v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0045

    iget-object p1, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->d()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a()I

    move-result p1

    invoke-virtual {v4, v5, p1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0f0046

    iget-object p2, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->c()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a()I

    move-result p2

    invoke-virtual {v5, p1, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2340102
    iput-object v0, v3, LX/GG9;->d:LX/0Px;

    .line 2340103
    move-object v0, v3

    .line 2340104
    iget-object v3, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a()I

    move-result v3

    iget-object v4, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->d()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v4

    iget-object v5, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->c()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a()I

    move-result v5

    iget-object p1, p0, LX/GJz;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v5, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2340105
    iput-object v3, v0, LX/GG9;->c:LX/0Px;

    .line 2340106
    move-object v0, v0

    .line 2340107
    iget-object v3, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iget-object v4, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->d()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v4

    int-to-float v4, v4

    int-to-float v5, v1

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iget-object v5, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->c()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a()I

    move-result v5

    int-to-float v5, v5

    int-to-float v1, v1

    div-float v1, v5, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v3, v4, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2340108
    iput-object v1, v0, LX/GG9;->e:LX/0Px;

    .line 2340109
    move-object v0, v0

    .line 2340110
    invoke-virtual {v0}, LX/GG9;->a()LX/GGA;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setViewModel(LX/GGA;)V

    .line 2340111
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 2340112
    return-void

    .line 2340113
    :cond_0
    const v0, 0x7f0f0043

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2340114
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    iput-object v0, p0, LX/GJz;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 2340115
    return-void
.end method
