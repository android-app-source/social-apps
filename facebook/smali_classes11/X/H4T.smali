.class public final LX/H4T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2422763
    iput-object p1, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2422764
    iget-object v0, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    invoke-virtual {v0, p3}, LX/H4L;->getItemViewType(I)I

    move-result v0

    .line 2422765
    invoke-static {}, LX/H4K;->values()[LX/H4K;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2422766
    sget-object v1, LX/H4K;->QUERY_SUGGESTION_CELL:LX/H4K;

    if-ne v0, v1, :cond_2

    .line 2422767
    iget-object v0, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    invoke-virtual {v0, p3}, LX/H4L;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2422768
    iget-object v1, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    .line 2422769
    const-string v4, "search_result_user_selection"

    invoke-static {v1, v4}, LX/H4a;->a(LX/H4a;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2422770
    sget-object v6, LX/H4Z;->PLACES_TOPICS:LX/H4Z;

    const/4 v7, 0x0

    move-object v4, v1

    move v8, p3

    move v9, p3

    invoke-static/range {v4 .. v9}, LX/H4a;->a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;Ljava/lang/String;II)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2422771
    const-string v5, "result_name"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "selection_name"

    invoke-virtual {v5, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2422772
    iget-object v5, v1, LX/H4a;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2422773
    new-instance v1, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-direct {v1, v0, v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2422774
    iget-object v2, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422775
    iput-object v1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2422776
    iget-object v1, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2422777
    iget-object v0, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->p:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2422778
    iget-object v0, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->p:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    iget-object v1, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    .line 2422779
    :cond_0
    :goto_0
    return-void

    .line 2422780
    :cond_1
    iget-object v0, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->requestFocus()Z

    goto :goto_0

    .line 2422781
    :cond_2
    sget-object v1, LX/H4K;->PLACE_CELL:LX/H4K;

    if-ne v0, v1, :cond_0

    .line 2422782
    iget-object v0, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    invoke-virtual {v0, p3}, LX/H4L;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;

    .line 2422783
    iget-object v1, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v3, v3, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    invoke-virtual {v3, p3}, LX/H4L;->a(I)I

    move-result v3

    .line 2422784
    const-string v4, "search_result_user_selection"

    invoke-static {v1, v4}, LX/H4a;->a(LX/H4a;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2422785
    sget-object v6, LX/H4Z;->PLACES_TOPICS:LX/H4Z;

    move-object v4, v1

    move-object v7, v2

    move v8, p3

    move v9, v3

    invoke-static/range {v4 .. v9}, LX/H4a;->a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;Ljava/lang/String;II)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2422786
    iget-object v5, v1, LX/H4a;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2422787
    iget-object v1, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aE:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2422788
    iget-object v1, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H4T;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
