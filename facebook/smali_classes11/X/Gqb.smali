.class public final LX/Gqb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

.field public final synthetic b:LX/GoE;

.field public final synthetic c:LX/Gqc;


# direct methods
.method public constructor <init>(LX/Gqc;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;LX/GoE;)V
    .locals 0

    .prologue
    .line 2396677
    iput-object p1, p0, LX/Gqb;->c:LX/Gqc;

    iput-object p2, p0, LX/Gqb;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    iput-object p3, p0, LX/Gqb;->b:LX/GoE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2f964dc4

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2396671
    iget-object v1, p0, LX/Gqb;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    if-eqz v1, :cond_0

    .line 2396672
    iget-object v1, p0, LX/Gqb;->c:LX/Gqc;

    iget-object v1, v1, LX/Gqc;->a:LX/Go0;

    iget-object v2, p0, LX/Gqb;->b:LX/GoE;

    new-instance v3, LX/GqZ;

    invoke-direct {v3, p0}, LX/GqZ;-><init>(LX/Gqb;)V

    invoke-virtual {v1, v2, v3}, LX/Go0;->a(LX/GoE;Ljava/util/Map;)V

    .line 2396673
    iget-object v1, p0, LX/Gqb;->c:LX/Gqc;

    iget-object v1, v1, LX/Gqc;->d:LX/Go7;

    const-string v2, "richtext_link_click"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/Gqb;->b:LX/GoE;

    invoke-virtual {v4}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/Gqb;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->G()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2396674
    iget-object v1, p0, LX/Gqb;->c:LX/Gqc;

    iget-object v1, v1, LX/Gqc;->b:LX/1xP;

    iget-object v2, p0, LX/Gqb;->c:LX/Gqc;

    invoke-virtual {v2}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Gqb;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->G()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v5, v5}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 2396675
    :goto_0
    const v1, -0x3cd35eec

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2396676
    :cond_0
    iget-object v1, p0, LX/Gqb;->c:LX/Gqc;

    iget-object v1, v1, LX/Gqc;->a:LX/Go0;

    iget-object v2, p0, LX/Gqb;->b:LX/GoE;

    new-instance v3, LX/Gqa;

    invoke-direct {v3, p0}, LX/Gqa;-><init>(LX/Gqb;)V

    invoke-virtual {v1, v2, v3}, LX/Go0;->a(LX/GoE;Ljava/util/Map;)V

    goto :goto_0
.end method
