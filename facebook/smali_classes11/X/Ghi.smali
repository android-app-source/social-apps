.class public LX/Ghi;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ghg;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Ghi;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2384625
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ghi;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384593
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2384594
    iput-object p1, p0, LX/Ghi;->b:LX/0Ot;

    .line 2384595
    return-void
.end method

.method public static a(LX/0QB;)LX/Ghi;
    .locals 4

    .prologue
    .line 2384596
    sget-object v0, LX/Ghi;->c:LX/Ghi;

    if-nez v0, :cond_1

    .line 2384597
    const-class v1, LX/Ghi;

    monitor-enter v1

    .line 2384598
    :try_start_0
    sget-object v0, LX/Ghi;->c:LX/Ghi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2384599
    if-eqz v2, :cond_0

    .line 2384600
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2384601
    new-instance v3, LX/Ghi;

    const/16 p0, 0x2306

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ghi;-><init>(LX/0Ot;)V

    .line 2384602
    move-object v0, v3

    .line 2384603
    sput-object v0, LX/Ghi;->c:LX/Ghi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384604
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2384605
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2384606
    :cond_1
    sget-object v0, LX/Ghi;->c:LX/Ghi;

    return-object v0

    .line 2384607
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2384608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2384609
    check-cast p2, LX/Ghh;

    .line 2384610
    iget-object v0, p0, LX/Ghi;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;

    iget-object v2, p2, LX/Ghh;->a:Ljava/lang/String;

    iget-object v3, p2, LX/Ghh;->b:Ljava/lang/String;

    iget-object v4, p2, LX/Ghh;->c:Ljava/lang/String;

    iget-object v5, p2, LX/Ghh;->d:Ljava/lang/String;

    iget-boolean v6, p2, LX/Ghh;->e:Z

    iget-boolean v7, p2, LX/Ghh;->f:Z

    move-object v1, p1

    const/4 p1, 0x2

    const/high16 p2, 0x3f800000    # 1.0f

    .line 2384611
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    if-eqz v6, :cond_5

    const v8, 0x7f020beb

    :goto_0
    invoke-interface {v9, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v8

    const/16 v9, 0x8

    const p0, 0x7f0b0060

    invoke-interface {v8, v9, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    .line 2384612
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2384613
    const v9, 0x7f0b1631

    const/4 p0, 0x5

    invoke-static {v0, v1, v2, v9, p0}, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->a(Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;LX/1De;Ljava/lang/String;II)LX/1Di;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2384614
    :cond_0
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 2384615
    :cond_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/4 p0, 0x0

    invoke-interface {v9, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const p1, 0x7f0b0050

    invoke-virtual {p0, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    invoke-interface {v9, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const p1, 0x7f0b004e

    invoke-virtual {p0, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p1, -0x777778

    invoke-virtual {p0, p1}, LX/1ne;->m(I)LX/1ne;

    move-result-object p0

    invoke-interface {v9, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2384616
    :cond_2
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 2384617
    const v9, 0x7f0b1633

    const/4 p0, 0x4

    invoke-static {v0, v1, v5, v9, p0}, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->a(Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;LX/1De;Ljava/lang/String;II)LX/1Di;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2384618
    :cond_3
    if-eqz v7, :cond_4

    .line 2384619
    const v9, 0x7f020bf0

    invoke-interface {v8, v9}, LX/1Dh;->X(I)LX/1Dh;

    .line 2384620
    :cond_4
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v0, v8

    .line 2384621
    return-object v0

    .line 2384622
    :cond_5
    const v8, 0x7f0a00d5

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2384623
    invoke-static {}, LX/1dS;->b()V

    .line 2384624
    const/4 v0, 0x0

    return-object v0
.end method
