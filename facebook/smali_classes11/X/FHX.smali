.class public LX/FHX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;
.implements LX/4B8;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final u:Ljava/lang/Object;


# instance fields
.field private final a:LX/2OB;

.field public final b:LX/G5l;

.field public final c:LX/7TG;

.field public final d:LX/G5o;

.field public final e:LX/1Er;

.field public final f:LX/2MM;

.field public final g:LX/2Mc;

.field public final h:LX/0Xl;

.field private final i:LX/FI0;

.field private final j:LX/2Mn;

.field private final k:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field private final l:LX/03V;

.field private final m:LX/FGW;

.field private final n:LX/2ML;

.field public final o:LX/FI1;

.field private final p:LX/FHp;

.field public final q:LX/7SH;

.field public final r:LX/0Uh;

.field public s:LX/2Mh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2Me;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2220449
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FHX;->u:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2OB;LX/G5l;LX/7TG;LX/G5o;LX/1Er;LX/2MM;LX/0Xl;LX/FI0;LX/2Mn;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/03V;LX/FHp;LX/FGW;LX/2ML;LX/0Uh;LX/7SH;)V
    .locals 2
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2220589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220590
    iput-object p1, p0, LX/FHX;->a:LX/2OB;

    .line 2220591
    iput-object p2, p0, LX/FHX;->b:LX/G5l;

    .line 2220592
    iput-object p3, p0, LX/FHX;->c:LX/7TG;

    .line 2220593
    iput-object p4, p0, LX/FHX;->d:LX/G5o;

    .line 2220594
    iput-object p5, p0, LX/FHX;->e:LX/1Er;

    .line 2220595
    iput-object p6, p0, LX/FHX;->f:LX/2MM;

    .line 2220596
    iput-object p7, p0, LX/FHX;->h:LX/0Xl;

    .line 2220597
    iput-object p8, p0, LX/FHX;->i:LX/FI0;

    .line 2220598
    iput-object p9, p0, LX/FHX;->j:LX/2Mn;

    .line 2220599
    iput-object p10, p0, LX/FHX;->k:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2220600
    iput-object p11, p0, LX/FHX;->l:LX/03V;

    .line 2220601
    iput-object p12, p0, LX/FHX;->p:LX/FHp;

    .line 2220602
    iput-object p13, p0, LX/FHX;->m:LX/FGW;

    .line 2220603
    move-object/from16 v0, p14

    iput-object v0, p0, LX/FHX;->n:LX/2ML;

    .line 2220604
    move-object/from16 v0, p16

    iput-object v0, p0, LX/FHX;->q:LX/7SH;

    .line 2220605
    new-instance v1, LX/FI1;

    invoke-direct {v1}, LX/FI1;-><init>()V

    iput-object v1, p0, LX/FHX;->o:LX/FI1;

    .line 2220606
    new-instance v1, LX/2Mc;

    invoke-direct {v1}, LX/2Mc;-><init>()V

    iput-object v1, p0, LX/FHX;->g:LX/2Mc;

    .line 2220607
    move-object/from16 v0, p15

    iput-object v0, p0, LX/FHX;->r:LX/0Uh;

    .line 2220608
    return-void
.end method

.method public static a(LX/0QB;)LX/FHX;
    .locals 7

    .prologue
    .line 2220562
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2220563
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2220564
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2220565
    if-nez v1, :cond_0

    .line 2220566
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2220567
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2220568
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2220569
    sget-object v1, LX/FHX;->u:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2220570
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2220571
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2220572
    :cond_1
    if-nez v1, :cond_4

    .line 2220573
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2220574
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2220575
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FHX;->b(LX/0QB;)LX/FHX;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2220576
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2220577
    if-nez v1, :cond_2

    .line 2220578
    sget-object v0, LX/FHX;->u:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHX;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2220579
    :goto_1
    if-eqz v0, :cond_3

    .line 2220580
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220581
    :goto_3
    check-cast v0, LX/FHX;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2220582
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2220583
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2220584
    :catchall_1
    move-exception v0

    .line 2220585
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220586
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2220587
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2220588
    :cond_2
    :try_start_8
    sget-object v0, LX/FHX;->u:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHX;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 21

    .prologue
    .line 2220468
    invoke-static {}, LX/0SW;->createStarted()LX/0SW;

    move-result-object v16

    .line 2220469
    invoke-virtual/range {p1 .. p1}, LX/1qK;->getId()Ljava/lang/String;

    move-result-object v17

    .line 2220470
    invoke-virtual/range {p1 .. p1}, LX/1qK;->getBundle()Landroid/os/Bundle;

    move-result-object v5

    .line 2220471
    const-string v2, "mediaResource"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2220472
    invoke-static {v2}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 2220473
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->k:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v3, v2}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v4

    .line 2220474
    :goto_0
    iget-object v2, v4, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    if-eq v2, v3, :cond_0

    .line 2220475
    sget-object v2, LX/1nY;->OTHER:LX/1nY;

    const-string v3, "MediaResource is not a video."

    invoke-static {v2, v3}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2220476
    :goto_1
    return-object v2

    .line 2220477
    :cond_0
    const-string v2, "transcode"

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 2220478
    const-string v2, "trimEndToPreviousSyncPoint"

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 2220479
    if-eqz v6, :cond_2

    .line 2220480
    new-instance v3, LX/FHU;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, LX/FHU;-><init>(LX/FHX;Z)V

    .line 2220481
    sget-object v2, LX/FHR;->transcode:LX/FHR;

    .line 2220482
    :goto_2
    const/4 v12, 0x0

    .line 2220483
    :try_start_0
    const-string v7, "estimatedBytes"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 2220484
    move-object/from16 v0, p0

    iget-object v8, v0, LX/FHX;->i:LX/FI0;

    invoke-virtual {v8, v4}, LX/FI0;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2220485
    move-object/from16 v0, p0

    iget-object v8, v0, LX/FHX;->j:LX/2Mn;

    invoke-virtual {v8, v4, v7, v6}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;IZ)V

    .line 2220486
    iget-wide v8, v4, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 2220487
    sget-object v13, LX/FHQ;->unknown:LX/FHQ;

    .line 2220488
    const-string v6, "isOutOfSpace"

    const/4 v10, 0x0

    invoke-virtual {v5, v6, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 2220489
    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/FHX;->b(LX/FHX;Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v6

    .line 2220490
    invoke-static {v4}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v15

    .line 2220491
    if-nez v15, :cond_1

    if-eqz v6, :cond_3

    :cond_1
    if-eqz v5, :cond_3

    .line 2220492
    const-string v11, "Not enough disk space to create new edited video."

    .line 2220493
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 2220494
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->i:LX/FI0;

    invoke-virtual {v2}, LX/FHR;->name()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/FHX;->n:LX/2ML;

    invoke-virtual {v10, v4}, LX/2ML;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v3 .. v10}, LX/FI0;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JLjava/lang/Throwable;ILjava/lang/String;)V

    .line 2220495
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->j:LX/2Mn;

    const/4 v5, 0x0

    invoke-virtual {v2}, LX/FHR;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, v8}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 2220496
    sget-object v3, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v3, v11}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v2

    goto :goto_1

    .line 2220497
    :cond_2
    new-instance v3, LX/FHW;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/FHW;-><init>(LX/FHX;)V

    .line 2220498
    sget-object v2, LX/FHR;->trim:LX/FHR;

    goto :goto_2

    .line 2220499
    :cond_3
    const/4 v11, 0x0

    .line 2220500
    :try_start_1
    const-string v6, "transcoded_video_larger"

    .line 2220501
    const/4 v10, 0x0

    .line 2220502
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FHX;->a:LX/2OB;

    invoke-virtual {v5, v4}, LX/2OB;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;

    move-result-object v5

    .line 2220503
    if-eqz v5, :cond_6

    const/4 v14, 0x1

    .line 2220504
    :goto_3
    if-eqz v14, :cond_7

    .line 2220505
    sget-object v3, LX/FHQ;->skipped:LX/FHQ;
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-object v14, v2

    move-object v2, v3

    move v3, v11

    .line 2220506
    :goto_4
    if-nez v3, :cond_4

    if-nez v15, :cond_e

    .line 2220507
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->f:LX/2MM;

    iget-object v10, v4, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v3, v10}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 2220508
    :goto_5
    if-eqz v3, :cond_5

    if-eqz v5, :cond_b

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v18

    cmp-long v3, v10, v18

    if-gez v3, :cond_5

    iget-object v3, v4, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    if-eqz v3, :cond_b

    .line 2220509
    :cond_5
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/5zn;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v3

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/5zn;->a(Landroid/net/Uri;)LX/5zn;

    move-result-object v3

    const-string v6, "video/mp4"

    invoke-virtual {v3, v6}, LX/5zn;->b(Ljava/lang/String;)LX/5zn;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/5zn;->a(I)LX/5zn;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/5zn;->b(I)LX/5zn;

    move-result-object v3

    sget-object v6, LX/47d;->UNDEFINED:LX/47d;

    invoke-virtual {v3, v6}, LX/5zn;->a(LX/47d;)LX/5zn;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/5zn;->c(Landroid/net/Uri;)LX/5zn;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/5zn;->b(Z)LX/5zn;

    move-result-object v3

    const-wide/16 v8, 0x0

    invoke-virtual {v3, v8, v9}, LX/5zn;->c(J)LX/5zn;

    move-result-object v3

    const/4 v6, -0x1

    invoke-virtual {v3, v6}, LX/5zn;->c(I)LX/5zn;

    move-result-object v3

    const/4 v6, -0x2

    invoke-virtual {v3, v6}, LX/5zn;->d(I)LX/5zn;

    move-result-object v3

    sget-object v6, Lcom/facebook/ui/media/attachments/MediaResource;->b:Landroid/graphics/RectF;

    invoke-virtual {v3, v6}, LX/5zn;->a(Landroid/graphics/RectF;)LX/5zn;

    move-result-object v3

    invoke-virtual {v3, v15}, LX/5zn;->c(Z)LX/5zn;

    move-result-object v3

    invoke-virtual {v3}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v3

    .line 2220510
    move-object/from16 v0, p0

    iget-object v6, v0, LX/FHX;->k:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v6, v3}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v3

    .line 2220511
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    move-object v15, v3

    .line 2220512
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->h:LX/0Xl;

    invoke-static {v4}, LX/FGn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Landroid/content/Intent;

    move-result-object v8

    invoke-interface {v3, v8}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2220513
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->o:LX/FI1;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, LX/FI1;->b(Ljava/lang/String;)V

    .line 2220514
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->i:LX/FI0;

    invoke-virtual {v14}, LX/FHR;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, LX/FHQ;->name()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v13, v0, LX/FHX;->n:LX/2ML;

    invoke-virtual {v13, v4}, LX/2ML;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v3 .. v13}, LX/FI0;->a(Lcom/facebook/ui/media/attachments/MediaResource;JILjava/lang/String;Ljava/lang/String;JILjava/lang/String;)V

    .line 2220515
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->j:LX/2Mn;

    invoke-virtual {v2}, LX/FHQ;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14}, LX/FHR;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v12, v2, v5}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/String;Ljava/lang/String;)V

    .line 2220516
    invoke-static {v15}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    move-result-object v2

    goto/16 :goto_1

    .line 2220517
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 2220518
    :cond_7
    :try_start_3
    invoke-static {}, LX/FHp;->a()I
    :try_end_3
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-result v14

    .line 2220519
    const/4 v12, 0x1

    move-object/from16 v20, v5

    move-object v5, v13

    move v13, v11

    move-object v11, v6

    move-object/from16 v6, v20

    :goto_7
    if-gt v12, v14, :cond_10

    .line 2220520
    :try_start_4
    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v4}, LX/FHS;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    :try_end_4
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v5

    .line 2220521
    :try_start_5
    sget-object v6, LX/FHQ;->completed:LX/FHQ;

    .line 2220522
    move-object/from16 v0, p0

    iget-object v0, v0, LX/FHX;->a:LX/2OB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, LX/2OB;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_5
    .catch Ljava/util/concurrent/CancellationException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    move-result-object v3

    .line 2220523
    if-eqz v3, :cond_f

    .line 2220524
    :try_start_6
    invoke-static {v3}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 2220525
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->a:LX/2OB;

    invoke-virtual {v3, v4}, LX/2OB;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;

    move-result-object v3

    .line 2220526
    if-eqz v3, :cond_f

    .line 2220527
    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_6 .. :try_end_6} :catch_1

    :goto_8
    move-object v5, v3

    move-object v14, v2

    move v3, v13

    move-object v2, v6

    move-object v6, v11

    .line 2220528
    goto/16 :goto_4

    :catch_0
    move v3, v13

    move-object v14, v2

    move-object v2, v6

    move-object v6, v11

    .line 2220529
    goto/16 :goto_4

    .line 2220530
    :catch_1
    move-exception v3

    .line 2220531
    :try_start_7
    throw v3
    :try_end_7
    .catch Ljava/util/concurrent/CancellationException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 2220532
    :catch_2
    move-exception v3

    move v8, v12

    move-object v5, v2

    move-object v2, v3

    .line 2220533
    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->i:LX/FI0;

    invoke-virtual {v5}, LX/FHR;->name()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, LX/FHX;->n:LX/2ML;

    invoke-virtual {v9, v4}, LX/2ML;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v3 .. v9}, LX/FI0;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JILjava/lang/String;)V

    .line 2220534
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->j:LX/2Mn;

    invoke-virtual {v3, v4, v8}, LX/2Mn;->b(Lcom/facebook/ui/media/attachments/MediaResource;I)V

    .line 2220535
    throw v2

    .line 2220536
    :catch_3
    move-exception v5

    .line 2220537
    :goto_a
    if-lt v12, v14, :cond_8

    .line 2220538
    :try_start_8
    move-object/from16 v0, p0

    iget-object v11, v0, LX/FHX;->p:LX/FHp;

    invoke-virtual {v11, v4}, LX/FHp;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 2220539
    const/4 v13, 0x1

    .line 2220540
    const-string v11, "transcoding_failed"

    .line 2220541
    :cond_8
    sget-object v5, LX/FHQ;->failure_ignored:LX/FHQ;

    .line 2220542
    if-eqz v15, :cond_9

    sget-object v18, LX/FHR;->transcode:LX/FHR;

    move-object/from16 v0, v18

    if-ne v2, v0, :cond_9

    .line 2220543
    move-object/from16 v0, p0

    iget-object v0, v0, LX/FHX;->p:LX/FHp;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, LX/FHp;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 2220544
    new-instance v3, LX/FHW;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/FHW;-><init>(LX/FHX;)V

    .line 2220545
    sget-object v2, LX/FHR;->trim:LX/FHR;

    .line 2220546
    :cond_9
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_7

    .line 2220547
    :cond_a
    throw v5
    :try_end_8
    .catch Ljava/util/concurrent/CancellationException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    .line 2220548
    :catch_4
    move-exception v8

    move v9, v12

    move-object v14, v2

    .line 2220549
    :goto_b
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FHX;->i:LX/FI0;

    invoke-virtual {v14}, LX/FHR;->name()Ljava/lang/String;

    move-result-object v5

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FHX;->n:LX/2ML;

    invoke-virtual {v2, v4}, LX/2ML;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v3 .. v10}, LX/FI0;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JLjava/lang/Throwable;ILjava/lang/String;)V

    .line 2220550
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FHX;->j:LX/2Mn;

    invoke-virtual {v14}, LX/FHR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v9, v3, v8}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 2220551
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FHX;->l:LX/03V;

    const-string v3, "MediaUploadVideoResizeHandler_Exception"

    invoke-virtual {v2, v3, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2220552
    throw v8

    .line 2220553
    :cond_b
    :try_start_9
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v3

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, LX/5zn;->c(Landroid/net/Uri;)LX/5zn;

    move-result-object v3

    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v6, v10}, LX/5zn;->a(Ljava/lang/String;Ljava/lang/String;)LX/5zn;

    move-result-object v3

    invoke-virtual {v3}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v3

    .line 2220554
    move-object/from16 v0, p0

    iget-object v6, v0, LX/FHX;->a:LX/2OB;

    invoke-virtual {v6, v3}, LX/2OB;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2220555
    if-eqz v5, :cond_c

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2220556
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 2220557
    :cond_c
    sget-object v5, LX/FHQ;->failure_ignored:LX/FHQ;

    if-eq v2, v5, :cond_d

    .line 2220558
    sget-object v2, LX/FHQ;->results_discarded:LX/FHQ;
    :try_end_9
    .catch Ljava/util/concurrent/CancellationException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    move-wide v5, v8

    move-object v15, v3

    goto/16 :goto_6

    .line 2220559
    :catch_5
    move-exception v8

    move v9, v12

    goto :goto_b

    .line 2220560
    :catch_6
    move-exception v2

    move v8, v12

    move-object v5, v14

    goto/16 :goto_9

    .line 2220561
    :catch_7
    move-exception v6

    move-object/from16 v20, v6

    move-object v6, v5

    move-object/from16 v5, v20

    goto/16 :goto_a

    :cond_d
    move-wide v5, v8

    move-object v15, v3

    goto/16 :goto_6

    :cond_e
    move-object v3, v10

    goto/16 :goto_5

    :cond_f
    move-object v3, v5

    goto/16 :goto_8

    :cond_10
    move v3, v13

    move-object v14, v2

    move-object v2, v5

    move-object v5, v6

    move-object v6, v11

    goto/16 :goto_4

    :cond_11
    move-object v4, v2

    goto/16 :goto_0
.end method

.method public static synthetic a(LX/FHX;LX/G5k;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2220460
    iget-object v0, p1, LX/G5k;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2220461
    const-string v0, ".mp4"

    .line 2220462
    :goto_0
    move-object v0, v0

    .line 2220463
    return-object v0

    .line 2220464
    :cond_0
    iget-object v0, p1, LX/G5k;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2220465
    const-string p0, "audio/3gpp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    const-string p0, "audio/amr-wb"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2220466
    :cond_1
    const-string v0, ".3gp"

    goto :goto_0

    .line 2220467
    :cond_2
    const-string v0, ".mp4"

    goto :goto_0
.end method

.method private static a(LX/FHX;LX/2Mh;LX/2Me;)V
    .locals 0

    .prologue
    .line 2220459
    iput-object p1, p0, LX/FHX;->s:LX/2Mh;

    iput-object p2, p0, LX/FHX;->t:LX/2Me;

    return-void
.end method

.method private static b(LX/0QB;)LX/FHX;
    .locals 19

    .prologue
    .line 2220456
    new-instance v2, LX/FHX;

    invoke-static/range {p0 .. p0}, LX/2OB;->a(LX/0QB;)LX/2OB;

    move-result-object v3

    check-cast v3, LX/2OB;

    invoke-static/range {p0 .. p0}, LX/G5l;->a(LX/0QB;)LX/G5l;

    move-result-object v4

    check-cast v4, LX/G5l;

    invoke-static/range {p0 .. p0}, LX/7TG;->a(LX/0QB;)LX/7TG;

    move-result-object v5

    check-cast v5, LX/7TG;

    invoke-static/range {p0 .. p0}, LX/G5o;->a(LX/0QB;)LX/G5o;

    move-result-object v6

    check-cast v6, LX/G5o;

    invoke-static/range {p0 .. p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v7

    check-cast v7, LX/1Er;

    invoke-static/range {p0 .. p0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v8

    check-cast v8, LX/2MM;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/FI0;->a(LX/0QB;)LX/FI0;

    move-result-object v10

    check-cast v10, LX/FI0;

    invoke-static/range {p0 .. p0}, LX/2Mn;->a(LX/0QB;)LX/2Mn;

    move-result-object v11

    check-cast v11, LX/2Mn;

    invoke-static/range {p0 .. p0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v12

    check-cast v12, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static/range {p0 .. p0}, LX/FHp;->a(LX/0QB;)LX/FHp;

    move-result-object v14

    check-cast v14, LX/FHp;

    invoke-static/range {p0 .. p0}, LX/FGW;->a(LX/0QB;)LX/FGW;

    move-result-object v15

    check-cast v15, LX/FGW;

    invoke-static/range {p0 .. p0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v16

    check-cast v16, LX/2ML;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v17

    check-cast v17, LX/0Uh;

    const-class v18, LX/7SH;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/7SH;

    invoke-direct/range {v2 .. v18}, LX/FHX;-><init>(LX/2OB;LX/G5l;LX/7TG;LX/G5o;LX/1Er;LX/2MM;LX/0Xl;LX/FI0;LX/2Mn;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/03V;LX/FHp;LX/FGW;LX/2ML;LX/0Uh;LX/7SH;)V

    .line 2220457
    invoke-static/range {p0 .. p0}, LX/2Mh;->a(LX/0QB;)LX/2Mh;

    move-result-object v3

    check-cast v3, LX/2Mh;

    invoke-static/range {p0 .. p0}, LX/2Me;->a(LX/0QB;)LX/2Me;

    move-result-object v4

    check-cast v4, LX/2Me;

    invoke-static {v2, v3, v4}, LX/FHX;->a(LX/FHX;LX/2Mh;LX/2Me;)V

    .line 2220458
    return-object v2
.end method

.method private static b(LX/FHX;Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 4

    .prologue
    .line 2220455
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    iget-object v2, p0, LX/FHX;->m:LX/FGW;

    invoke-virtual {v2}, LX/FGW;->a()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final cancelOperation(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2220454
    iget-object v0, p0, LX/FHX;->o:LX/FI1;

    invoke-virtual {v0, p1}, LX/FI1;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2220450
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2220451
    const-string v1, "video_transcode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2220452
    invoke-direct {p0, p1}, LX/FHX;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2220453
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
