.class public final LX/Gv5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2404731
    iput-object p1, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    iput p2, p0, LX/Gv5;->a:I

    iput-object p3, p0, LX/Gv5;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2404732
    iget-object v0, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2404733
    iget-object v0, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->m:Lcom/facebook/webview/FacebookWebView;

    if-nez v0, :cond_0

    .line 2404734
    :goto_0
    return-void

    .line 2404735
    :cond_0
    iget-object v0, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_1

    iget v0, p0, LX/Gv5;->a:I

    if-eq v0, p2, :cond_1

    .line 2404736
    iget-object v0, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->k:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/model/NewsFeedToggleOption;

    iget-object v0, v0, Lcom/facebook/katana/model/NewsFeedToggleOption;->script:Ljava/lang/String;

    .line 2404737
    if-eqz v0, :cond_1

    .line 2404738
    iget-object v1, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2404739
    invoke-virtual {v1, v0, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2404740
    :cond_1
    iget-object v0, p0, LX/Gv5;->c:Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->m:Lcom/facebook/webview/FacebookWebView;

    iget-object v1, p0, LX/Gv5;->b:Ljava/lang/String;

    .line 2404741
    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2404742
    goto :goto_0
.end method
