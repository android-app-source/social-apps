.class public final enum LX/FD3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FD3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FD3;

.field public static final enum SEARCH_NULL_STATE:LX/FD3;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2210833
    new-instance v0, LX/FD3;

    const-string v1, "SEARCH_NULL_STATE"

    const-string v2, "search_null_state"

    invoke-direct {v0, v1, v3, v2}, LX/FD3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FD3;->SEARCH_NULL_STATE:LX/FD3;

    .line 2210834
    const/4 v0, 0x1

    new-array v0, v0, [LX/FD3;

    sget-object v1, LX/FD3;->SEARCH_NULL_STATE:LX/FD3;

    aput-object v1, v0, v3

    sput-object v0, LX/FD3;->$VALUES:[LX/FD3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2210830
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2210831
    iput-object p3, p0, LX/FD3;->value:Ljava/lang/String;

    .line 2210832
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FD3;
    .locals 1

    .prologue
    .line 2210829
    const-class v0, LX/FD3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FD3;

    return-object v0
.end method

.method public static values()[LX/FD3;
    .locals 1

    .prologue
    .line 2210828
    sget-object v0, LX/FD3;->$VALUES:[LX/FD3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FD3;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2210827
    iget-object v0, p0, LX/FD3;->value:Ljava/lang/String;

    return-object v0
.end method
