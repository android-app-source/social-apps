.class public final LX/FBE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/net/HttpOperation;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/net/HttpOperation;)V
    .locals 0

    .prologue
    .line 2208952
    iput-object p1, p0, LX/FBE;->a:Lcom/facebook/katana/net/HttpOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2208935
    const/4 v1, 0x0

    .line 2208936
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2208937
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    .line 2208938
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 2208939
    const/16 v2, 0xc8

    if-ne v0, v2, :cond_2

    .line 2208940
    if-eqz v3, :cond_2

    .line 2208941
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2208942
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    .line 2208943
    :cond_0
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2208944
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 2208945
    :goto_0
    new-instance v2, LX/FBF;

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/FBF;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2208946
    :goto_1
    :try_start_1
    iget-object v0, p0, LX/FBE;->a:Lcom/facebook/katana/net/HttpOperation;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2208947
    iput-wide v4, v0, Lcom/facebook/katana/net/HttpOperation;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2208948
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 2208949
    return-object v1

    :cond_1
    move-object v0, v1

    .line 2208950
    goto :goto_0

    .line 2208951
    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :cond_2
    move-object v2, v1

    goto :goto_1
.end method
