.class public LX/FZu;
.super LX/0Q6;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NontrivialConfigureMethod"
    }
.end annotation

.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2258116
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2258117
    return-void
.end method

.method public static a()LX/3y7;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/search/prefs/ForGraphSearch;
    .end annotation

    .prologue
    .line 2258118
    new-instance v0, LX/FZt;

    invoke-direct {v0}, LX/FZt;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/facebook/common/perftest/PerfTestConfig;LX/0Zm;LX/0Ot;LX/0Ot;)LX/Cvs;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            "LX/0Ot",
            "<",
            "LX/Cvt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cvu;",
            ">;)",
            "LX/Cvs;"
        }
    .end annotation

    .prologue
    .line 2258119
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/0Zm;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvs;

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvs;

    goto :goto_0
.end method

.method public static a(LX/0Or;)LX/FgU;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/Fgh;",
            ">;)",
            "LX/FgU;"
        }
    .end annotation

    .prologue
    .line 2258120
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgU;

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2258121
    return-void
.end method
