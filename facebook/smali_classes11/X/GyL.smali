.class public LX/GyL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:F


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2409886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2409887
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/GyL;->a:F

    .line 2409888
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2409889
    new-instance v0, LX/Gy2;

    invoke-direct {v0}, LX/Gy2;-><init>()V

    move-object v0, v0

    .line 2409890
    const-string v1, "query_params"

    new-instance v2, LX/4EA;

    invoke-direct {v2}, LX/4EA;-><init>()V

    const-string v3, "keywords_places(cafes in manhattan, ny)"

    .line 2409891
    const-string p2, "query"

    invoke-virtual {v2, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2409892
    move-object v2, v2

    .line 2409893
    const-string v3, "SET_SEARCH"

    .line 2409894
    const-string p2, "surface_type"

    invoke-virtual {v2, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2409895
    move-object v2, v2

    .line 2409896
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_image_size"

    const/high16 v2, 0x42780000    # 62.0f

    iget v3, p0, LX/GyL;->a:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "places_before"

    .line 2409897
    iget-object v2, p1, LX/3DR;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2409898
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "places_after"

    .line 2409899
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2409900
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "places_first"

    .line 2409901
    iget v2, p1, LX/3DR;->e:I

    move v2, v2

    .line 2409902
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2409903
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2409904
    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel;->a()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;

    move-result-object v0

    .line 2409905
    invoke-virtual {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2409906
    new-instance v3, LX/5Mb;

    invoke-virtual {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->a()LX/0Px;

    move-result-object v0

    invoke-direct {v3, v0, v2, v1}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    return-object v3

    .line 2409907
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
