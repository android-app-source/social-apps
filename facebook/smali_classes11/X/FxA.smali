.class public final LX/FxA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1U8;

.field public final synthetic b:LX/5vn;

.field public final synthetic c:LX/0Px;

.field public final synthetic d:LX/Fx7;

.field public final synthetic e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/1U8;LX/5vn;LX/0Px;LX/Fx7;)V
    .locals 0

    .prologue
    .line 2304380
    iput-object p1, p0, LX/FxA;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iput-object p2, p0, LX/FxA;->a:LX/1U8;

    iput-object p3, p0, LX/FxA;->b:LX/5vn;

    iput-object p4, p0, LX/FxA;->c:LX/0Px;

    iput-object p5, p0, LX/FxA;->d:LX/Fx7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x4083d858

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2304381
    iget-object v0, p0, LX/FxA;->a:LX/1U8;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FxA;->a:LX/1U8;

    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2304382
    :cond_0
    const v0, 0x2b6049ff

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2304383
    :goto_0
    return-void

    .line 2304384
    :cond_1
    iget-object v0, p0, LX/FxA;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G0q;

    move-object v1, p1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/FxA;->a:LX/1U8;

    iget-object v3, p0, LX/FxA;->b:LX/5vn;

    iget-object v4, p0, LX/FxA;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iget-object v5, p0, LX/FxA;->c:LX/0Px;

    iget-object v7, p0, LX/FxA;->d:LX/Fx7;

    invoke-static {v4, v5, v7}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/0Px;LX/Fx7;)LX/0Px;

    move-result-object v4

    iget-object v5, p0, LX/FxA;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iget-object v7, p0, LX/FxA;->c:LX/0Px;

    iget-object v8, p0, LX/FxA;->d:LX/Fx7;

    invoke-static {v5, v7, v8}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->b(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/0Px;LX/Fx7;)Ljava/util/List;

    move-result-object v5

    .line 2304385
    invoke-interface {v2}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v9

    .line 2304386
    iget-object v7, v0, LX/G0q;->c:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/G0r;

    .line 2304387
    invoke-virtual {v7, v2, v3}, LX/G0r;->a(LX/1U8;LX/5vn;)LX/1bf;

    move-result-object p0

    .line 2304388
    iget-object v7, v0, LX/G0q;->b:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/23R;

    .line 2304389
    iget-object v8, v0, LX/G0q;->a:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    .line 2304390
    invoke-static {v0, v9, p0, v4, v5}, LX/G0q;->a(LX/G0q;Ljava/lang/String;LX/1bf;LX/0Px;Ljava/util/List;)Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object p1

    invoke-static {v9, v1, p0}, LX/FwM;->a(Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;)LX/9hN;

    move-result-object v9

    invoke-interface {v7, v8, p1, v9}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2304391
    const v0, 0x68876a66

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0
.end method
