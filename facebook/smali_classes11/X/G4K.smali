.class public LX/G4K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G4J;

.field public b:I


# direct methods
.method public constructor <init>(LX/G4J;)V
    .locals 0

    .prologue
    .line 2317417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317418
    iput-object p1, p0, LX/G4K;->a:LX/G4J;

    .line 2317419
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2317416
    iget-object v0, p0, LX/G4K;->a:LX/G4J;

    iget-wide v0, v0, LX/G4J;->a:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2317415
    iget-object v0, p0, LX/G4K;->a:LX/G4J;

    iget-object v0, v0, LX/G4J;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2317414
    iget-object v0, p0, LX/G4K;->a:LX/G4J;

    iget-object v0, v0, LX/G4J;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G4K;->a:LX/G4J;

    iget-object v0, v0, LX/G4J;->d:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2317408
    if-ne p0, p1, :cond_1

    .line 2317409
    :cond_0
    :goto_0
    return v0

    .line 2317410
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2317411
    goto :goto_0

    .line 2317412
    :cond_3
    invoke-virtual {p0}, LX/G4K;->a()J

    move-result-wide v2

    check-cast p1, LX/G4K;

    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2317413
    invoke-virtual {p0}, LX/G4K;->a()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method
