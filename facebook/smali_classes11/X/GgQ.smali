.class public final LX/GgQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/10s;


# instance fields
.field public final synthetic a:LX/10n;

.field private b:LX/0gs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/10n;)V
    .locals 0

    .prologue
    .line 2379372
    iput-object p1, p0, LX/GgQ;->a:LX/10n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0gs;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2379373
    iput-object p1, p0, LX/GgQ;->b:LX/0gs;

    .line 2379374
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/192;

    const v6, 0xa00ab

    .line 2379375
    sget-object v4, LX/ArE;->a:[I

    invoke-virtual {p1}, LX/0gs;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2379376
    :goto_0
    sget-object v0, LX/0gs;->OPENED:LX/0gs;

    if-ne p1, v0, :cond_1

    .line 2379377
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    invoke-static {v0, v3}, LX/10n;->b(LX/10n;Z)V

    .line 2379378
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ArS;

    sget-object v2, LX/ArQ;->CAMERA:LX/ArQ;

    iget-object v1, p0, LX/GgQ;->a:LX/10n;

    iget-boolean v1, v1, LX/10n;->i:Z

    if-eqz v1, :cond_0

    sget-object v1, LX/ArP;->BUTTON_TAP:LX/ArP;

    :goto_1
    invoke-virtual {v0, v2, v1}, LX/ArS;->a(LX/ArQ;LX/ArP;)V

    .line 2379379
    :goto_2
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    .line 2379380
    iput-boolean v3, v0, LX/10n;->h:Z

    .line 2379381
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    .line 2379382
    iput-boolean v3, v0, LX/10n;->i:Z

    .line 2379383
    return-void

    .line 2379384
    :cond_0
    sget-object v1, LX/ArP;->SWIPE:LX/ArP;

    goto :goto_1

    .line 2379385
    :cond_1
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/0fA;

    if-eqz v0, :cond_2

    .line 2379386
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fA;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0fA;->i_(Z)V

    .line 2379387
    :cond_2
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ArS;

    sget-object v2, LX/ArQ;->NEWSFEED:LX/ArQ;

    iget-object v1, p0, LX/GgQ;->a:LX/10n;

    iget-boolean v1, v1, LX/10n;->h:Z

    if-eqz v1, :cond_3

    sget-object v1, LX/ArP;->BUTTON_TAP:LX/ArP;

    :goto_3
    invoke-virtual {v0, v2, v1}, LX/ArS;->a(LX/ArQ;LX/ArP;)V

    goto :goto_2

    :cond_3
    sget-object v1, LX/ArP;->SWIPE:LX/ArP;

    goto :goto_3

    .line 2379388
    :pswitch_0
    iget-object v4, v0, LX/192;->d:LX/195;

    invoke-virtual {v4}, LX/195;->a()V

    .line 2379389
    const v4, 0xa00ac

    invoke-static {v0, v4}, LX/192;->b(LX/192;I)V

    goto :goto_0

    .line 2379390
    :pswitch_1
    iget-object v4, v0, LX/192;->c:LX/195;

    invoke-virtual {v4}, LX/195;->a()V

    .line 2379391
    const v4, 0xa00a9

    invoke-static {v0, v4}, LX/192;->b(LX/192;I)V

    .line 2379392
    invoke-static {v0, v6}, LX/192;->b(LX/192;I)V

    .line 2379393
    iget-wide v7, v0, LX/192;->g:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_4

    const-string v7, "INVALID"

    .line 2379394
    :goto_4
    iget-object v8, v0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v9, "time_since_main_tab"

    invoke-interface {v8, v6, v9, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2379395
    iget-object v7, v0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v8, "num_entered"

    iget v9, v0, LX/192;->h:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v6, v8, v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2379396
    goto/16 :goto_0

    .line 2379397
    :cond_4
    iget-object v7, v0, LX/192;->e:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v7

    iget-wide v9, v0, LX/192;->g:J

    sub-long/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/0gs;)V
    .locals 3

    .prologue
    .line 2379398
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/192;

    .line 2379399
    invoke-static {v0}, LX/192;->d(LX/192;)V

    .line 2379400
    sget-object v1, LX/ArE;->a:[I

    invoke-virtual {p1}, LX/0gs;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2379401
    :goto_0
    sget-object v0, LX/0gs;->OPENED:LX/0gs;

    if-ne p1, v0, :cond_1

    .line 2379402
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/10n;->b(LX/10n;Z)V

    .line 2379403
    :cond_0
    :goto_1
    iget-object v0, p0, LX/GgQ;->b:LX/0gs;

    sget-object v1, LX/0gs;->CLOSED:LX/0gs;

    if-ne v0, v1, :cond_2

    sget-object v0, LX/0gs;->OPENED:LX/0gs;

    if-ne p1, v0, :cond_2

    .line 2379404
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ArS;

    sget-object v1, LX/ArQ;->CAMERA:LX/ArQ;

    invoke-virtual {v0, v1}, LX/ArS;->a(LX/ArQ;)V

    .line 2379405
    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/GgQ;->b:LX/0gs;

    .line 2379406
    return-void

    .line 2379407
    :cond_1
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/0fA;

    if-eqz v0, :cond_0

    .line 2379408
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fA;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0fA;->i_(Z)V

    goto :goto_1

    .line 2379409
    :cond_2
    iget-object v0, p0, LX/GgQ;->b:LX/0gs;

    sget-object v1, LX/0gs;->OPENED:LX/0gs;

    if-ne v0, v1, :cond_3

    sget-object v0, LX/0gs;->CLOSED:LX/0gs;

    if-ne p1, v0, :cond_3

    .line 2379410
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ArS;

    sget-object v1, LX/ArQ;->NEWSFEED:LX/ArQ;

    invoke-virtual {v0, v1}, LX/ArS;->a(LX/ArQ;)V

    goto :goto_2

    .line 2379411
    :cond_3
    iget-object v0, p0, LX/GgQ;->a:LX/10n;

    iget-object v0, v0, LX/10n;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ArS;

    const v2, 0xb6000a

    .line 2379412
    invoke-static {v0}, LX/ArS;->f(LX/ArS;)V

    .line 2379413
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/ArS;->f:Z

    .line 2379414
    iget-object v1, v0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2379415
    :goto_3
    goto :goto_2

    .line 2379416
    :pswitch_0
    const v1, 0xa00ac

    invoke-static {v0, v1}, LX/192;->c(LX/192;I)V

    goto :goto_0

    .line 2379417
    :pswitch_1
    const v1, 0xa00a9

    invoke-static {v0, v1}, LX/192;->c(LX/192;I)V

    .line 2379418
    const v1, 0xa00ab

    invoke-static {v0, v1}, LX/192;->c(LX/192;I)V

    goto/16 :goto_0

    .line 2379419
    :cond_4
    iget-object v1, v0, LX/ArS;->c:LX/ArG;

    invoke-virtual {v1}, LX/ArG;->b()V

    .line 2379420
    iget-object v1, v0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
