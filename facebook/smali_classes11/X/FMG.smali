.class public LX/FMG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/FN6;

.field private final b:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(LX/FN6;Landroid/content/ContentResolver;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2229460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2229461
    iput-object p1, p0, LX/FMG;->a:LX/FN6;

    .line 2229462
    iput-object p2, p0, LX/FMG;->b:Landroid/content/ContentResolver;

    .line 2229463
    return-void
.end method

.method public static b(LX/0QB;)LX/FMG;
    .locals 3

    .prologue
    .line 2229464
    new-instance v2, LX/FMG;

    invoke-static {p0}, LX/FN6;->b(LX/0QB;)LX/FN6;

    move-result-object v0

    check-cast v0, LX/FN6;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    invoke-direct {v2, v0, v1}, LX/FMG;-><init>(LX/FN6;Landroid/content/ContentResolver;)V

    .line 2229465
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 2229466
    iget-object v0, p0, LX/FMG;->a:LX/FN6;

    invoke-virtual {v0}, LX/FN6;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sub_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 2229467
    const-string v0, "sub_id"

    invoke-static {p1, v0}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 2229468
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 2229469
    iget-object v0, p0, LX/FMG;->a:LX/FN6;

    invoke-virtual {v0}, LX/FN6;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2229470
    :try_start_0
    iget-object v0, p0, LX/FMG;->b:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2229471
    :goto_0
    return-object v0

    .line 2229472
    :catch_0
    move-exception v0

    .line 2229473
    iget-object v1, p0, LX/FMG;->a:LX/FN6;

    const/4 v2, 0x0

    .line 2229474
    iget-object v3, v1, LX/FN6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/0db;->aU:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2229475
    const-string v1, "SmsTakeoverCursorUtil"

    const-string v2, "Device with multi SIM APIs failed content query for column SUBSCRIPTION_ID"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2229476
    :cond_0
    iget-object v0, p0, LX/FMG;->b:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method
