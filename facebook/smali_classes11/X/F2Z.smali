.class public final LX/F2Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/F2r;


# direct methods
.method public constructor <init>(LX/F2r;)V
    .locals 0

    .prologue
    .line 2193196
    iput-object p1, p0, LX/F2Z;->a:LX/F2r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x739dd3e3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193187
    iget-object v1, p0, LX/F2Z;->a:LX/F2r;

    iget-object v1, v1, LX/F2r;->c:LX/F2N;

    iget-object v2, p0, LX/F2Z;->a:LX/F2r;

    iget-object v2, v2, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193188
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2193189
    const-string v5, "group_edit_name_description_data"

    invoke-static {v3, v5, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2193190
    invoke-static {v1}, LX/F2N;->b(LX/F2N;)Landroid/content/Intent;

    move-result-object v5

    .line 2193191
    invoke-virtual {v5, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2193192
    const-string v3, "target_fragment"

    sget-object v6, LX/0cQ;->GROUP_EDIT_PURPOSE_FRAGMENT:LX/0cQ;

    invoke-virtual {v6}, LX/0cQ;->ordinal()I

    move-result v6

    invoke-virtual {v5, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2193193
    move-object v1, v5

    .line 2193194
    iget-object v2, p0, LX/F2Z;->a:LX/F2r;

    iget-object v2, v2, LX/F2r;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2193195
    const v1, -0x715c93b0

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
