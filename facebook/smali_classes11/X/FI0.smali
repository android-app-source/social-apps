.class public LX/FI0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FI0;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0kb;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221341
    iput-object p1, p0, LX/FI0;->a:LX/0Zb;

    .line 2221342
    iput-object p2, p0, LX/FI0;->b:LX/0kb;

    .line 2221343
    return-void
.end method

.method public static a(LX/0QB;)LX/FI0;
    .locals 5

    .prologue
    .line 2221291
    sget-object v0, LX/FI0;->c:LX/FI0;

    if-nez v0, :cond_1

    .line 2221292
    const-class v1, LX/FI0;

    monitor-enter v1

    .line 2221293
    :try_start_0
    sget-object v0, LX/FI0;->c:LX/FI0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221294
    if-eqz v2, :cond_0

    .line 2221295
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2221296
    new-instance p0, LX/FI0;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-direct {p0, v3, v4}, LX/FI0;-><init>(LX/0Zb;LX/0kb;)V

    .line 2221297
    move-object v0, p0

    .line 2221298
    sput-object v0, LX/FI0;->c:LX/FI0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221299
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221300
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221301
    :cond_1
    sget-object v0, LX/FI0;->c:LX/FI0;

    return-object v0

    .line 2221302
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221303
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/FI0;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2221338
    iget-object v0, p0, LX/FI0;->a:LX/0Zb;

    invoke-interface {v0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2221339
    return-void
.end method

.method private static b(LX/FI0;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2221324
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 2221325
    iget-object v1, p0, LX/FI0;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2221326
    const-string v2, "connection_type"

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221327
    const-string v1, "connection_wifi_rssi"

    iget-object v2, p0, LX/FI0;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->r()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221328
    const-string v1, "is_connected"

    iget-object v2, p0, LX/FI0;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221329
    const-string v1, "is_network_metered"

    iget-object v2, p0, LX/FI0;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->h()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221330
    const-string v1, "video_duration_millis"

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221331
    const-string v1, "video_size"

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221332
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221333
    const-string v1, "video_attachment_type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v2}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221334
    const-string v1, "original_height"

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221335
    const-string v1, "original_width"

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221336
    return-object v0

    .line 2221337
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 2221344
    const-string v0, "messenger_transcode_started"

    invoke-static {p0, p1}, LX/FI0;->b(LX/FI0;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/FI0;->a(LX/FI0;Ljava/lang/String;Ljava/util/Map;)V

    .line 2221345
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;JILjava/lang/String;Ljava/lang/String;JILjava/lang/String;)V
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2221312
    invoke-static {p0, p1}, LX/FI0;->b(LX/FI0;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v0

    .line 2221313
    const-string v1, "elapsed_time"

    invoke-static {p7, p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221314
    const-string v1, "new_size"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221315
    const-string v1, "transcode_attempt"

    invoke-static {p9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221316
    const-string v1, "original_sha256"

    invoke-interface {v0, v1, p10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221317
    if-eqz p4, :cond_0

    .line 2221318
    const-string v1, "estimated_size"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221319
    :cond_0
    const-string v1, "resize_strategy"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221320
    const-string v1, "resize_status"

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221321
    const-string v1, "is_video_trimmed_or_cropped"

    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221322
    const-string v1, "messenger_transcode_finished"

    invoke-static {p0, v1, v0}, LX/FI0;->a(LX/FI0;Ljava/lang/String;Ljava/util/Map;)V

    .line 2221323
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JILjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2221304
    invoke-static {p0, p1}, LX/FI0;->b(LX/FI0;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v0

    .line 2221305
    const-string v1, "elapsed_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221306
    const-string v1, "resize_strategy"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221307
    const-string v1, "original_sha256"

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221308
    const-string v1, "transcode_attempt"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221309
    const-string v1, "is_video_trimmed_or_cropped"

    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221310
    const-string v1, "messenger_transcode_canceled"

    invoke-static {p0, v1, v0}, LX/FI0;->a(LX/FI0;Ljava/lang/String;Ljava/util/Map;)V

    .line 2221311
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JLjava/lang/Throwable;ILjava/lang/String;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2221277
    invoke-static {p0, p1}, LX/FI0;->b(LX/FI0;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v1

    .line 2221278
    const-string v0, "elapsed_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221279
    const-string v0, "resize_strategy"

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221280
    const-string v0, "transcode_attempt"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221281
    const-string v0, "original_sha256"

    invoke-interface {v1, v0, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221282
    const-string v0, "is_video_trimmed_or_cropped"

    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221283
    if-eqz p5, :cond_0

    .line 2221284
    invoke-static {p5}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v0

    .line 2221285
    invoke-static {v0}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 2221286
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 2221287
    new-instance v3, Ljava/io/PrintWriter;

    invoke-direct {v3, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v3}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 2221288
    const-string v0, "error_code"

    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221289
    :cond_0
    const-string v0, "messenger_transcode_failed"

    invoke-static {p0, v0, v1}, LX/FI0;->a(LX/FI0;Ljava/lang/String;Ljava/util/Map;)V

    .line 2221290
    return-void
.end method
