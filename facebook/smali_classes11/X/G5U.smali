.class public final LX/G5U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/search/api/SearchTypeaheadResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7BK;

.field public final synthetic b:LX/G5W;


# direct methods
.method public constructor <init>(LX/G5W;LX/7BK;)V
    .locals 0

    .prologue
    .line 2318878
    iput-object p1, p0, LX/G5U;->b:LX/G5W;

    iput-object p2, p0, LX/G5U;->a:LX/7BK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/G5U;)V
    .locals 2

    .prologue
    .line 2318879
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-boolean v0, v0, LX/G5W;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-boolean v0, v0, LX/G5W;->n:Z

    if-eqz v0, :cond_0

    .line 2318880
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318881
    sget-object v1, LX/G5O;->b:LX/G5M;

    .line 2318882
    iget-object p0, v0, LX/G5P;->a:LX/11i;

    invoke-interface {p0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 2318883
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2318884
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$2;-><init>(LX/G5U;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2318885
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2318886
    check-cast p1, Ljava/util/List;

    const/4 v2, 0x1

    .line 2318887
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-boolean v0, v0, LX/G5W;->l:Z

    if-eqz v0, :cond_0

    .line 2318888
    :goto_0
    return-void

    .line 2318889
    :cond_0
    sget-object v0, LX/G5V;->a:[I

    iget-object v1, p0, LX/G5U;->a:LX/7BK;

    invoke-virtual {v1}, LX/7BK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2318890
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->v:LX/03V;

    sget-object v1, LX/G5W;->a:Ljava/lang/String;

    const-string v2, "Unsupported search type found in creating future."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318891
    :goto_1
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$1;

    invoke-direct {v1, p0}, Lcom/facebook/uberbar/core/UberbarResultFetcher$3$1;-><init>(LX/G5U;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2318892
    invoke-static {p0}, LX/G5U;->a$redex0(LX/G5U;)V

    goto :goto_0

    .line 2318893
    :pswitch_0
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    .line 2318894
    iput-object p1, v0, LX/G5W;->p:Ljava/util/List;

    .line 2318895
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    .line 2318896
    iput-boolean v2, v0, LX/G5W;->m:Z

    .line 2318897
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318898
    sget-object v1, LX/G5O;->b:LX/G5M;

    const-string v2, "fetch_users_time"

    invoke-static {v0, v1, v2}, LX/G5P;->b(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318899
    goto :goto_1

    .line 2318900
    :pswitch_1
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    .line 2318901
    iput-object p1, v0, LX/G5W;->q:Ljava/util/List;

    .line 2318902
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    .line 2318903
    iput-boolean v2, v0, LX/G5W;->n:Z

    .line 2318904
    iget-object v0, p0, LX/G5U;->b:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318905
    sget-object v1, LX/G5O;->b:LX/G5M;

    const-string v2, "fetch_pages_time"

    invoke-static {v0, v1, v2}, LX/G5P;->b(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318906
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
