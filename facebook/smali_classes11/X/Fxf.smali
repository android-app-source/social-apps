.class public LX/Fxf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

.field public b:Landroid/view/View$OnLongClickListener;

.field public c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field public k:I

.field public l:F

.field public m:F

.field private n:I

.field public o:I

.field public p:Landroid/view/animation/Interpolator;

.field public q:Z


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;)V
    .locals 2
    .param p1    # Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2305799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2305800
    const/4 v0, -0x1

    iput v0, p0, LX/Fxf;->k:I

    .line 2305801
    iput-object p1, p0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305802
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e0b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Fxf;->n:I

    .line 2305803
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, LX/Fxf;->o:I

    .line 2305804
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/Fxf;->p:Landroid/view/animation/Interpolator;

    .line 2305805
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fxf;->q:Z

    .line 2305806
    new-instance v0, LX/Fxa;

    invoke-direct {v0, p0}, LX/Fxa;-><init>(LX/Fxf;)V

    iput-object v0, p0, LX/Fxf;->b:Landroid/view/View$OnLongClickListener;

    .line 2305807
    return-void
.end method

.method private static a(FFFFFF)F
    .locals 2

    .prologue
    .line 2305790
    add-float v0, p0, p1

    .line 2305791
    add-float/2addr v0, p2

    .line 2305792
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v1, p2

    .line 2305793
    sub-float/2addr v0, v1

    .line 2305794
    cmpg-float v1, v0, p3

    if-gez v1, :cond_0

    .line 2305795
    sub-float v0, p3, v0

    .line 2305796
    :goto_0
    return v0

    .line 2305797
    :cond_0
    add-float v0, p4, p1

    add-float/2addr v0, p2

    .line 2305798
    cmpl-float v1, v0, p5

    if-lez v1, :cond_1

    sub-float v0, p5, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/Fxf;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 2305787
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2305788
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p0, LX/Fxf;->n:I

    add-int/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p0, LX/Fxf;->n:I

    add-int/2addr v2, v3

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p0, LX/Fxf;->n:I

    sub-int/2addr v3, v4

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, LX/Fxf;->n:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2305789
    return-object v0
.end method

.method public static a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2305763
    invoke-static {p0, p1}, LX/Fxf;->e(LX/Fxf;Landroid/view/View;)LX/Fxe;

    move-result-object v0

    .line 2305764
    invoke-static {p1}, LX/Fxf;->d(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    .line 2305765
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, p2, Landroid/graphics/Rect;->left:I

    .line 2305766
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/Fxe;->p:Z

    .line 2305767
    iput v2, v0, LX/Fxe;->q:I

    .line 2305768
    iput v3, v0, LX/Fxe;->r:I

    .line 2305769
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    .line 2305770
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/Fxe;->s:Z

    .line 2305771
    iput v2, v0, LX/Fxe;->t:I

    .line 2305772
    iput v3, v0, LX/Fxe;->u:I

    .line 2305773
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/Fxe;->c(II)V

    .line 2305774
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/Fxe;->d(II)V

    .line 2305775
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    .line 2305776
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Fxe;->j:Z

    .line 2305777
    invoke-static {v0}, LX/Fxe;->a(LX/Fxe;)V

    .line 2305778
    iput v1, v0, LX/Fxe;->k:F

    .line 2305779
    iput v4, v0, LX/Fxe;->l:F

    .line 2305780
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    .line 2305781
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Fxe;->m:Z

    .line 2305782
    invoke-static {v0}, LX/Fxe;->b(LX/Fxe;)V

    .line 2305783
    iput v1, v0, LX/Fxe;->n:F

    .line 2305784
    iput v4, v0, LX/Fxe;->o:F

    .line 2305785
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2305786
    return-void
.end method

.method public static a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;FFII)V
    .locals 7

    .prologue
    .line 2305739
    invoke-static {p1}, LX/Fxf;->d(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    invoke-static {v0, p3}, LX/Fxf;->b(Landroid/widget/FrameLayout$LayoutParams;Landroid/graphics/Rect;)V

    .line 2305740
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 2305741
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2305742
    iget v0, p2, Landroid/graphics/Rect;->top:I

    iget v1, p3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 2305743
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 2305744
    :cond_0
    invoke-static {p0, p1}, LX/Fxf;->e(LX/Fxf;Landroid/view/View;)LX/Fxe;

    move-result-object v6

    .line 2305745
    invoke-static {p1}, LX/Fxf;->d(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v6, v0, v1}, LX/Fxe;->c(II)V

    .line 2305746
    invoke-static {p1}, LX/Fxf;->d(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v6, v0, v1}, LX/Fxe;->d(II)V

    .line 2305747
    iget v0, p4, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget v3, p3, Landroid/graphics/Rect;->top:I

    int-to-float v4, v3

    int-to-float v5, p7

    move v3, p6

    invoke-static/range {v0 .. v5}, LX/Fxf;->a(FFFFFF)F

    move-result v0

    .line 2305748
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    .line 2305749
    const/4 v1, 0x0

    .line 2305750
    const/4 v2, 0x1

    iput-boolean v2, v6, LX/Fxe;->g:Z

    .line 2305751
    invoke-static {v6}, LX/Fxe;->b(LX/Fxe;)V

    .line 2305752
    iput v1, v6, LX/Fxe;->h:F

    .line 2305753
    iput v0, v6, LX/Fxe;->i:F

    .line 2305754
    :cond_1
    iget v0, p4, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget v3, p3, Landroid/graphics/Rect;->left:I

    int-to-float v4, v3

    int-to-float v5, p8

    move v3, p5

    invoke-static/range {v0 .. v5}, LX/Fxf;->a(FFFFFF)F

    move-result v0

    .line 2305755
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    .line 2305756
    const/4 v1, 0x0

    .line 2305757
    const/4 v2, 0x1

    iput-boolean v2, v6, LX/Fxe;->d:Z

    .line 2305758
    invoke-static {v6}, LX/Fxe;->a(LX/Fxe;)V

    .line 2305759
    iput v1, v6, LX/Fxe;->e:F

    .line 2305760
    iput v0, v6, LX/Fxe;->f:F

    .line 2305761
    :cond_2
    invoke-virtual {p1, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2305762
    :cond_3
    return-void
.end method

.method public static a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2305661
    if-eqz p3, :cond_0

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2305662
    :goto_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 2305663
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 2305664
    invoke-static {v0, p2}, LX/Fxf;->b(Landroid/widget/FrameLayout$LayoutParams;Landroid/graphics/Rect;)V

    .line 2305665
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2305666
    return-void

    .line 2305667
    :cond_0
    invoke-static {p1}, LX/Fxf;->d(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic b(LX/Fxf;FF)V
    .locals 11

    .prologue
    .line 2305713
    const/4 v2, -0x1

    .line 2305714
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Fxf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2305715
    iget v0, p0, LX/Fxf;->k:I

    if-eq v0, v1, :cond_1

    .line 2305716
    iget-object v0, p0, LX/Fxf;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    float-to-int v3, p1

    float-to-int v4, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    move v9, v1

    .line 2305717
    :goto_1
    if-eq v9, v2, :cond_0

    .line 2305718
    invoke-static {p0}, LX/Fxf;->e(LX/Fxf;)Landroid/view/View;

    move-result-object v1

    .line 2305719
    iget-object v0, p0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305720
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v0, v2

    .line 2305721
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    iget-object v10, v0, LX/FxZ;->a:Landroid/view/View;

    .line 2305722
    invoke-virtual {v10}, Landroid/view/View;->bringToFront()V

    .line 2305723
    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 2305724
    iget-object v0, p0, LX/Fxf;->f:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    .line 2305725
    iget-object v0, p0, LX/Fxf;->f:Ljava/util/List;

    iget v2, p0, LX/Fxf;->k:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 2305726
    iget-object v0, p0, LX/Fxf;->g:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    iget-object v0, p0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->getMeasuredWidth()I

    move-result v7

    iget-object v0, p0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->getMeasuredHeight()I

    move-result v8

    move-object v0, p0

    move v5, p1

    move v6, p2

    invoke-static/range {v0 .. v8}, LX/Fxf;->a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;FFII)V

    .line 2305727
    invoke-static {p0, v10, v2}, LX/Fxf;->a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2305728
    iget-object v0, p0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    iget v1, p0, LX/Fxf;->k:I

    .line 2305729
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->C:LX/1R7;

    .line 2305730
    iget-object v3, v2, LX/1R7;->a:[I

    aget v3, v3, v1

    .line 2305731
    iget-object v4, v2, LX/1R7;->a:[I

    iget-object v5, v2, LX/1R7;->a:[I

    aget v5, v5, v9

    aput v5, v4, v1

    .line 2305732
    iget-object v4, v2, LX/1R7;->a:[I

    aput v3, v4, v9

    .line 2305733
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-static {v2, v1, v9}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 2305734
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    invoke-static {v2, v1, v9}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 2305735
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->l()V

    .line 2305736
    iput v9, p0, LX/Fxf;->k:I

    .line 2305737
    :cond_0
    return-void

    .line 2305738
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_2
    move v9, v2

    goto :goto_1
.end method

.method public static b(Landroid/widget/FrameLayout$LayoutParams;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2305710
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2305711
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, p0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 2305712
    return-void
.end method

.method public static c(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 2305702
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2305703
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 2305704
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 2305705
    const/4 v2, 0x0

    aget v2, v1, v2

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 2305706
    const/4 v2, 0x1

    aget v1, v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 2305707
    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 2305708
    iget v1, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 2305709
    return-object v0
.end method

.method public static d(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 2305701
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    return-object v0
.end method

.method private static e(LX/Fxf;Landroid/view/View;)LX/Fxe;
    .locals 2

    .prologue
    .line 2305694
    iget-object v0, p0, LX/Fxf;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fxe;

    .line 2305695
    if-nez v0, :cond_0

    .line 2305696
    new-instance v0, LX/Fxe;

    invoke-direct {v0, p0, p1}, LX/Fxe;-><init>(LX/Fxf;Landroid/view/View;)V

    .line 2305697
    iget-object v1, p0, LX/Fxf;->i:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2305698
    :goto_0
    return-object v0

    .line 2305699
    :cond_0
    invoke-virtual {v0}, LX/Fxe;->cancel()V

    .line 2305700
    invoke-virtual {v0}, LX/Fxe;->reset()V

    goto :goto_0
.end method

.method public static e(LX/Fxf;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2305691
    iget-object v0, p0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305692
    iget-object v1, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v0, v1

    .line 2305693
    iget v1, p0, LX/Fxf;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FxZ;

    iget-object v0, v0, LX/FxZ;->a:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final c()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2305668
    iget-boolean v1, p0, LX/Fxf;->c:Z

    if-nez v1, :cond_0

    .line 2305669
    :goto_0
    return v0

    .line 2305670
    :cond_0
    const/4 v1, 0x0

    .line 2305671
    iget-object v2, p0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305672
    iget-object v3, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v2, v3

    .line 2305673
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FxZ;

    .line 2305674
    iget-object v5, v1, LX/FxZ;->a:Landroid/view/View;

    iget-object v6, p0, LX/Fxf;->d:Ljava/util/List;

    add-int/lit8 v3, v2, 0x1

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    invoke-static {p0, v5, v2}, LX/Fxf;->a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2305675
    iget-object v2, v1, LX/FxZ;->d:Landroid/view/View;

    .line 2305676
    iget-object v5, p0, LX/Fxf;->j:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Fxd;

    .line 2305677
    if-nez v5, :cond_2

    .line 2305678
    new-instance v5, LX/Fxd;

    invoke-direct {v5, p0, v2}, LX/Fxd;-><init>(LX/Fxf;Landroid/view/View;)V

    .line 2305679
    iget-object v6, p0, LX/Fxf;->j:Ljava/util/Map;

    invoke-interface {v6, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2305680
    :goto_2
    move-object v2, v5

    .line 2305681
    const/4 v5, 0x1

    iput-boolean v5, v2, LX/Fxd;->c:Z

    .line 2305682
    invoke-virtual {v2}, LX/Fxd;->cancel()V

    .line 2305683
    invoke-virtual {v2}, LX/Fxd;->reset()V

    .line 2305684
    iget-object v1, v1, LX/FxZ;->d:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    move v2, v3

    .line 2305685
    goto :goto_1

    .line 2305686
    :cond_1
    const/4 v1, -0x1

    iput v1, p0, LX/Fxf;->k:I

    .line 2305687
    iput-boolean v0, p0, LX/Fxf;->c:Z

    .line 2305688
    const/4 v0, 0x1

    goto :goto_0

    .line 2305689
    :cond_2
    invoke-virtual {v5}, LX/Fxd;->cancel()V

    .line 2305690
    invoke-virtual {v5}, LX/Fxd;->reset()V

    goto :goto_2
.end method
