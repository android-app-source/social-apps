.class public LX/F9e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/growth/protocol/FriendFinderMethod$Params;",
        "Lcom/facebook/growth/protocol/FriendFinderMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2205647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205648
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2205649
    check-cast p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;

    .line 2205650
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2205651
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205652
    iget-object v0, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2205653
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2205654
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "country_code"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205655
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "pic_size"

    .line 2205656
    iget v2, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->c:I

    move v2, v2

    .line 2205657
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205658
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "session_ID"

    .line 2205659
    iget-object v2, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->e:Ljava/lang/String;

    move-object v2, v2

    .line 2205660
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205661
    iget v0, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->i:I

    move v0, v0

    .line 2205662
    if-lez v0, :cond_4

    .line 2205663
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "cursor"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205664
    :goto_0
    iget-object v0, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->d:LX/89v;

    move-object v0, v0

    .line 2205665
    if-eqz v0, :cond_1

    sget-object v1, LX/89v;->UNKNOWN:LX/89v;

    invoke-virtual {v0, v1}, LX/89v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2205666
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "flow"

    iget-object v0, v0, LX/89v;->value:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205667
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "batch_index"

    .line 2205668
    iget v2, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->f:I

    move v2, v2

    .line 2205669
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205670
    iget v0, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->g:I

    move v0, v0

    .line 2205671
    if-lez v0, :cond_2

    .line 2205672
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "batch_size"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205673
    :cond_2
    iget v0, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->h:I

    move v0, v0

    .line 2205674
    if-lez v0, :cond_3

    .line 2205675
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pagination_size"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205676
    :cond_3
    new-instance v0, LX/14N;

    const-string v1, "FriendFinderMobile"

    const-string v2, "POST"

    const-string v3, "method/friendfinder.mobile"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 2205677
    :cond_4
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "contacts"

    .line 2205678
    iget-object v2, p1, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->a:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2205679
    invoke-static {v2}, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2205680
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;

    return-object v0
.end method
