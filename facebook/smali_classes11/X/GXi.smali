.class public final LX/GXi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;)V
    .locals 0

    .prologue
    .line 2364448
    iput-object p1, p0, LX/GXi;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2364449
    iget-object v0, p0, LX/GXi;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->c(Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;I)V

    .line 2364450
    iget-object v0, p0, LX/GXi;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->e:LX/GXT;

    if-eqz v0, :cond_0

    .line 2364451
    iget-object v0, p0, LX/GXi;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/adapter/AdminShopIntroSummaryView;->e:LX/GXT;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/GXT;->a(Ljava/lang/String;)V

    .line 2364452
    :cond_0
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2364453
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2364454
    return-void
.end method
