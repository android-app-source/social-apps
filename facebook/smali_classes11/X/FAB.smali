.class public LX/FAB;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Z

.field private static b:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<",
            "Ljava/net/SocketImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<",
            "Ljava/net/SocketImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<",
            "Ljava/net/SocketImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<",
            "Ljava/net/SocketImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206771
    const/4 v0, 0x0

    sput-boolean v0, LX/FAB;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2206769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206770
    return-void
.end method

.method public static a()Ljava/net/SocketImpl;
    .locals 3

    .prologue
    .line 2206746
    invoke-static {}, LX/FAB;->b()V

    .line 2206747
    :try_start_0
    sget-object v0, LX/FAB;->d:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/SocketImpl;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v0

    .line 2206748
    :catch_0
    move-exception v0

    .line 2206749
    new-instance v1, LX/FAA;

    const-string v2, "Failed to instantiate PlainSocketImpl"

    invoke-direct {v1, v2, v0}, LX/FAA;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 2206750
    :catch_1
    move-exception v0

    .line 2206751
    new-instance v1, LX/FAA;

    const-string v2, "Failed to instantiate PlainSocketImpl"

    invoke-direct {v1, v2, v0}, LX/FAA;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 2206752
    :catch_2
    move-exception v0

    .line 2206753
    new-instance v1, LX/FAA;

    const-string v2, "Failed to instantiate PlainSocketImpl"

    invoke-direct {v1, v2, v0}, LX/FAA;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method private static declared-synchronized b()V
    .locals 5

    .prologue
    .line 2206754
    const-class v1, LX/FAB;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, LX/FAB;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2206755
    :goto_0
    monitor-exit v1

    return-void

    .line 2206756
    :cond_0
    :try_start_1
    const-string v0, "java.net.PlainSocketImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2206757
    const-class v2, Ljava/net/SocketImpl;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2206758
    new-instance v0, LX/FAA;

    const-string v2, "Class found but not instance of SocketImpl"

    invoke-direct {v0, v2}, LX/FAA;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206759
    :catch_0
    move-exception v0

    .line 2206760
    :try_start_2
    new-instance v2, LX/FAA;

    const-string v3, "Failed to initialise DefaultSocketImpl"

    invoke-direct {v2, v3, v0}, LX/FAA;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2206761
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2206762
    :cond_1
    const/4 v2, 0x1

    :try_start_3
    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/io/FileDescriptor;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    sput-object v2, LX/FAB;->b:Ljava/lang/reflect/Constructor;

    .line 2206763
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/net/Proxy;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    sput-object v2, LX/FAB;->c:Ljava/lang/reflect/Constructor;

    .line 2206764
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    sput-object v2, LX/FAB;->d:Ljava/lang/reflect/Constructor;

    .line 2206765
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/io/FileDescriptor;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-class v4, Ljava/net/InetAddress;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    sput-object v0, LX/FAB;->e:Ljava/lang/reflect/Constructor;

    .line 2206766
    const/4 v0, 0x1

    sput-boolean v0, LX/FAB;->a:Z
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2206767
    :catch_1
    move-exception v0

    .line 2206768
    new-instance v2, LX/FAA;

    const-string v3, "Failed to initialise DefaultSocketImpl"

    invoke-direct {v2, v3, v0}, LX/FAA;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method
