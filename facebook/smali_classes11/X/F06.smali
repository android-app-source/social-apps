.class public final LX/F06;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F08;


# direct methods
.method public constructor <init>(LX/F08;)V
    .locals 0

    .prologue
    .line 2187794
    iput-object p1, p0, LX/F06;->a:LX/F08;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2187795
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/F08;->a(LX/F08;Z)V

    .line 2187796
    iget-object v0, p0, LX/F06;->a:LX/F08;

    iget-object v0, v0, LX/F08;->j:LX/F1Y;

    .line 2187797
    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-static {v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v1

    invoke-interface {v1}, LX/1Qq;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 2187798
    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    .line 2187799
    iget-object p0, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->C:Landroid/view/View;

    const v0, 0x7f0d1108

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 2187800
    const v0, 0x7f083174

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2187801
    iget-object p0, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v0, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->C:Landroid/view/View;

    invoke-interface {p0, v0}, LX/0g8;->f(Landroid/view/View;)V

    .line 2187802
    iget-object p0, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->C:Landroid/view/View;

    const v0, 0x7f0d1106

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 2187803
    new-instance v0, LX/F1Z;

    invoke-direct {v0, v1}, LX/F1Z;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2187804
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2187805
    check-cast p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;

    .line 2187806
    if-nez p1, :cond_1

    .line 2187807
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x1

    .line 2187808
    iput-boolean v1, v0, LX/F08;->k:Z

    .line 2187809
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/F08;->a(LX/F08;Z)V

    .line 2187810
    :cond_0
    :goto_0
    return-void

    .line 2187811
    :cond_1
    iget-object v0, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->a:LX/0Px;

    move-object v0, v0

    .line 2187812
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/0Px;

    .line 2187813
    iget-object v0, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->b:LX/0Px;

    move-object v0, v0

    .line 2187814
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/0Px;

    .line 2187815
    iget-object v0, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->c:LX/0P1;

    move-object v0, v0

    .line 2187816
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/0P1;

    .line 2187817
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ThrowbackFeedPager: have a mismatch in stories.size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and storySectionKey size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2187818
    iget-object v0, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->e:LX/0us;

    move-object v10, v0

    .line 2187819
    if-eqz v10, :cond_3

    invoke-interface {v10}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, LX/0us;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 2187820
    :cond_2
    iget-object v0, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->f:LX/0ta;

    move-object v0, v0

    .line 2187821
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_9

    .line 2187822
    :cond_3
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x1

    .line 2187823
    iput-boolean v1, v0, LX/F08;->k:Z

    .line 2187824
    :goto_2
    const/4 v5, 0x0

    .line 2187825
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2187826
    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2187827
    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, LX/F08;->a:Ljava/util/Set;

    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2187828
    :cond_4
    const/4 v5, 0x1

    .line 2187829
    :cond_5
    iget-object v0, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v0, v0

    .line 2187830
    if-eqz v0, :cond_6

    .line 2187831
    iget-object v0, p0, LX/F06;->a:LX/F08;

    .line 2187832
    iget-object v1, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v1, v1

    .line 2187833
    iput-object v1, v0, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    .line 2187834
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2187835
    const/4 v0, 0x0

    move v9, v0

    :goto_4
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    if-ge v9, v0, :cond_19

    .line 2187836
    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;

    .line 2187837
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2187838
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "promotion"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2187839
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_b

    .line 2187840
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2187841
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2187842
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_c

    .line 2187843
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2187844
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2187845
    :goto_6
    new-instance v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge;

    iget-object v3, p0, LX/F06;->a:LX/F08;

    iget-object v3, v3, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    if-eqz v3, :cond_d

    iget-object v3, p0, LX/F06;->a:LX/F08;

    iget-object v3, v3, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    .line 2187846
    iget-object v4, v3, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;->q:LX/1Fb;

    move-object v3, v4

    .line 2187847
    :goto_7
    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge;-><init>(Ljava/lang/String;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/model/FeedUnit;Z)V

    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2187848
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x1

    .line 2187849
    iput-boolean v1, v0, LX/F08;->h:Z

    .line 2187850
    :goto_8
    iget-object v1, p0, LX/F06;->a:LX/F08;

    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2187851
    iput-object v0, v1, LX/F08;->e:Ljava/lang/String;

    .line 2187852
    :cond_7
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_4

    .line 2187853
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2187854
    :cond_9
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x0

    .line 2187855
    iput-boolean v1, v0, LX/F08;->k:Z

    .line 2187856
    goto/16 :goto_2

    .line 2187857
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    .line 2187858
    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    .line 2187859
    :cond_c
    const/4 v2, 0x0

    goto :goto_6

    .line 2187860
    :cond_d
    const/4 v3, 0x0

    goto :goto_7

    .line 2187861
    :cond_e
    const-string v1, "pinned"

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "missed_memories"

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "friendversary"

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2187862
    :cond_f
    const-string v1, "missed_memories"

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, LX/F06;->a:LX/F08;

    iget-object v1, v1, LX/F08;->e:Ljava/lang/String;

    const-string v2, "missed_memories"

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 2187863
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_11

    .line 2187864
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2187865
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2187866
    :goto_9
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_12

    .line 2187867
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2187868
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2187869
    :goto_a
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge;

    iget-object v2, p0, LX/F06;->a:LX/F08;

    iget-object v2, v2, LX/F08;->e:Ljava/lang/String;

    if-nez v2, :cond_13

    const/4 v2, 0x1

    :goto_b
    invoke-direct {v3, v1, v0, v2}, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v11, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2187870
    :cond_10
    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_8

    .line 2187871
    :cond_11
    const/4 v1, 0x0

    goto :goto_9

    .line 2187872
    :cond_12
    const/4 v0, 0x0

    goto :goto_a

    .line 2187873
    :cond_13
    const/4 v2, 0x0

    goto :goto_b

    .line 2187874
    :cond_14
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2187875
    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/F06;->a:LX/F08;

    iget-object v2, v2, LX/F08;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 2187876
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_16

    .line 2187877
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2187878
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2187879
    :goto_c
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_17

    .line 2187880
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2187881
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2187882
    :goto_d
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge;

    const/4 v4, 0x1

    iget-object v2, p0, LX/F06;->a:LX/F08;

    iget-object v2, v2, LX/F08;->e:Ljava/lang/String;

    if-nez v2, :cond_18

    const/4 v2, 0x1

    :goto_e
    invoke-direct {v3, v1, v0, v4, v2}, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2187883
    :cond_15
    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_8

    .line 2187884
    :cond_16
    const/4 v1, 0x0

    goto :goto_c

    .line 2187885
    :cond_17
    const/4 v0, 0x0

    goto :goto_d

    .line 2187886
    :cond_18
    const/4 v2, 0x0

    goto :goto_e

    .line 2187887
    :cond_19
    if-eqz v10, :cond_1b

    invoke-static {v10}, LX/9JZ;->a(LX/0us;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2187888
    :goto_f
    iget-object v1, p0, LX/F06;->a:LX/F08;

    iget-object v1, v1, LX/F08;->b:LX/0fz;

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    .line 2187889
    iget-object v0, p0, LX/F06;->a:LX/F08;

    iget-boolean v0, v0, LX/F08;->n:Z

    if-eqz v0, :cond_1c

    .line 2187890
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2187891
    iget-object v0, p0, LX/F06;->a:LX/F08;

    iget-object v0, v0, LX/F08;->j:LX/F1Y;

    const/4 v1, 0x1

    .line 2187892
    iget-object v2, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_1a

    if-eqz v1, :cond_1a

    .line 2187893
    iget-object v2, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v2, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    iget-object v3, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v3, v3, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2187894
    iget-object v4, v3, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v3, v4

    .line 2187895
    iget-object v4, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v4, v4, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v4}, LX/F0J;->e()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->b(Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;Z)V

    .line 2187896
    :cond_1a
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x0

    .line 2187897
    iput-boolean v1, v0, LX/F08;->n:Z

    .line 2187898
    goto/16 :goto_0

    .line 2187899
    :cond_1b
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    const/4 v1, 0x0

    .line 2187900
    iput-boolean v1, v0, LX/17L;->d:Z

    .line 2187901
    move-object v0, v0

    .line 2187902
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_f

    .line 2187903
    :cond_1c
    iget-object v0, p0, LX/F06;->a:LX/F08;

    iget-object v1, v0, LX/F08;->j:LX/F1Y;

    .line 2187904
    iget-object v0, p1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v0, v0

    .line 2187905
    if-eqz v0, :cond_21

    const/4 v0, 0x1

    .line 2187906
    :goto_10
    iget-object v2, v1, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2187907
    iget-object v2, v1, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    .line 2187908
    if-eqz v0, :cond_1d

    .line 2187909
    invoke-static {v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->d$redex0(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    .line 2187910
    :cond_1d
    invoke-static {v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v3

    invoke-interface {v3}, LX/1Cw;->notifyDataSetChanged()V

    .line 2187911
    iget-object v3, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->d:LX/1CY;

    invoke-static {v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->u(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/0fz;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 2187912
    invoke-static {v2}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v3

    invoke-interface {v3}, LX/1Qq;->getCount()I

    move-result v3

    if-lez v3, :cond_23

    .line 2187913
    iget-object v4, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->w:LX/F1r;

    iget-object v3, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v3}, LX/F0J;->e()Z

    move-result v3

    if-nez v3, :cond_22

    iget-object v3, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v3}, LX/F0J;->c()Z

    move-result v3

    if-eqz v3, :cond_22

    const/4 v3, 0x1

    .line 2187914
    :goto_11
    iget-boolean v1, v4, LX/F1r;->m:Z

    if-eqz v1, :cond_24

    iget-object v1, v4, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    move-object v0, v1

    :goto_12
    if-eqz v3, :cond_25

    const/4 v1, 0x0

    :goto_13
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2187915
    invoke-virtual {v4}, LX/F1r;->getContext()Landroid/content/Context;

    move-result-object v1

    const v0, 0x7f040044

    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 2187916
    new-instance v0, LX/F1p;

    invoke-direct {v0, v4}, LX/F1p;-><init>(LX/F1r;)V

    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2187917
    invoke-virtual {v4, v1}, LX/F1r;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2187918
    :goto_14
    iget-object v3, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->j:LX/CF2;

    .line 2187919
    iget-object v4, v3, LX/CF2;->b:LX/11i;

    sget-object v1, LX/CF2;->a:LX/CF1;

    invoke-interface {v4, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v4

    .line 2187920
    if-eqz v4, :cond_1e

    .line 2187921
    const-string v1, "ThrowbackFeedDataFetch"

    const v2, -0x357e82a3    # -4243118.5f

    invoke-static {v4, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2187922
    :cond_1e
    iget-object v4, v3, LX/CF2;->b:LX/11i;

    sget-object v1, LX/CF2;->a:LX/CF1;

    invoke-interface {v4, v1}, LX/11i;->b(LX/0Pq;)V

    .line 2187923
    :cond_1f
    iget-object v0, p0, LX/F06;->a:LX/F08;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/F08;->a(LX/F08;Z)V

    .line 2187924
    iget-object v0, p0, LX/F06;->a:LX/F08;

    iget-boolean v0, v0, LX/F08;->k:Z

    if-eqz v0, :cond_0

    .line 2187925
    iget-object v0, p0, LX/F06;->a:LX/F08;

    iget-object v0, v0, LX/F08;->j:LX/F1Y;

    .line 2187926
    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v1, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v2, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v2, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->x:Landroid/view/View;

    invoke-interface {v1, v2}, LX/0g8;->b(Landroid/view/View;)V

    .line 2187927
    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-static {v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v1

    invoke-interface {v1}, LX/1Qq;->getCount()I

    move-result v1

    if-lez v1, :cond_20

    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v1, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    invoke-interface {v1}, LX/0g8;->u()I

    move-result v1

    if-nez v1, :cond_20

    .line 2187928
    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v1, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v2, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v2, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->D:Landroid/view/View;

    invoke-interface {v1, v2}, LX/0g8;->e(Landroid/view/View;)V

    .line 2187929
    :cond_20
    goto/16 :goto_0

    .line 2187930
    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_10

    .line 2187931
    :cond_22
    const/4 v3, 0x0

    goto/16 :goto_11

    .line 2187932
    :cond_23
    iget-object v3, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    iget-object v4, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    .line 2187933
    iget-object v1, v4, LX/F08;->f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    move-object v4, v1

    .line 2187934
    iget-object v1, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v1}, LX/F0J;->e()Z

    move-result v1

    invoke-virtual {v3, v4, v1}, Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;->a(Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;Z)V

    .line 2187935
    iget-object v3, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->A:LX/0g8;

    iget-object v4, v2, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->E:Lcom/facebook/goodwill/feed/ui/ThrowbackNUXView;

    invoke-interface {v3, v4}, LX/0g8;->f(Landroid/view/View;)V

    goto :goto_14

    .line 2187936
    :cond_24
    iget-object v1, v4, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    move-object v0, v1

    goto/16 :goto_12

    :cond_25
    const/16 v1, 0x8

    goto/16 :goto_13
.end method
