.class public LX/Giv;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/B5j;Landroid/content/Context;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2386778
    invoke-direct {p0, p2, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 2386779
    return-void
.end method


# virtual methods
.method public final U()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386781
    new-instance v0, LX/Gis;

    invoke-direct {v0, p0}, LX/Gis;-><init>(LX/Giv;)V

    return-object v0
.end method

.method public final V()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386780
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final Y()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386777
    new-instance v0, LX/Git;

    invoke-direct {v0, p0}, LX/Git;-><init>(LX/Giv;)V

    return-object v0
.end method

.method public final aC()LX/ARN;
    .locals 1

    .prologue
    .line 2386782
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1

    .prologue
    .line 2386776
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386775
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ae()LX/ARN;
    .locals 1

    .prologue
    .line 2386772
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1

    .prologue
    .line 2386774
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final i()LX/ARN;
    .locals 1

    .prologue
    .line 2386773
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method
