.class public LX/Fwh;
.super LX/Fwg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Fwg",
        "<",
        "LX/3ku;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile c:LX/Fwh;


# instance fields
.field private final b:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2303449
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_SUGGESTED_BIO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Fwh;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303468
    invoke-direct {p0, p2}, LX/Fwg;-><init>(LX/0iA;)V

    .line 2303469
    iput-object p1, p0, LX/Fwh;->b:LX/0ad;

    .line 2303470
    return-void
.end method

.method public static a(LX/0QB;)LX/Fwh;
    .locals 5

    .prologue
    .line 2303455
    sget-object v0, LX/Fwh;->c:LX/Fwh;

    if-nez v0, :cond_1

    .line 2303456
    const-class v1, LX/Fwh;

    monitor-enter v1

    .line 2303457
    :try_start_0
    sget-object v0, LX/Fwh;->c:LX/Fwh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2303458
    if-eqz v2, :cond_0

    .line 2303459
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2303460
    new-instance p0, LX/Fwh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-direct {p0, v3, v4}, LX/Fwh;-><init>(LX/0ad;LX/0iA;)V

    .line 2303461
    move-object v0, p0

    .line 2303462
    sput-object v0, LX/Fwh;->c:LX/Fwh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2303463
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2303464
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2303465
    :cond_1
    sget-object v0, LX/Fwh;->c:LX/Fwh;

    return-object v0

    .line 2303466
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2303467
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2303454
    const-string v0, "4058"

    return-object v0
.end method

.method public final b()Lcom/facebook/interstitial/manager/InterstitialTrigger;
    .locals 1

    .prologue
    .line 2303453
    sget-object v0, LX/Fwh;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-object v0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 2303452
    const-class v0, LX/3ku;

    return-object v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2303450
    iget-boolean v1, p0, LX/Fwg;->b:Z

    move v1, v1

    .line 2303451
    if-nez v1, :cond_0

    iget-object v1, p0, LX/Fwh;->b:LX/0ad;

    sget-short v2, LX/0wf;->W:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/Fwg;->g()LX/0i1;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
