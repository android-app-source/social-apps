.class public final LX/GAI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GAJ;


# direct methods
.method public constructor <init>(LX/GAJ;)V
    .locals 0

    .prologue
    .line 2324925
    iput-object p1, p0, LX/GAI;->a:LX/GAJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2324926
    invoke-static {}, LX/GA8;->a()LX/GA8;

    move-result-object v0

    const/4 v1, 0x0

    .line 2324927
    iget-object p0, v0, LX/GA8;->c:LX/GA1;

    invoke-virtual {p0}, LX/GA1;->a()Lcom/facebook/AccessToken;

    move-result-object p0

    .line 2324928
    if-eqz p0, :cond_0

    .line 2324929
    invoke-static {v0, p0, v1}, LX/GA8;->a(LX/GA8;Lcom/facebook/AccessToken;Z)V

    .line 2324930
    :cond_0
    invoke-static {}, LX/GAg;->a()LX/GAg;

    move-result-object v0

    const/4 v1, 0x0

    .line 2324931
    iget-object v2, v0, LX/GAg;->c:LX/GAf;

    const/4 v4, 0x0

    .line 2324932
    iget-object v3, v2, LX/GAf;->a:Landroid/content/SharedPreferences;

    const-string p0, "com.facebook.ProfileManager.CachedProfile"

    invoke-interface {v3, p0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2324933
    if-eqz v3, :cond_3

    .line 2324934
    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2324935
    new-instance v3, Lcom/facebook/Profile;

    invoke-direct {v3, p0}, Lcom/facebook/Profile;-><init>(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2324936
    :goto_0
    move-object v2, v3

    .line 2324937
    if-eqz v2, :cond_1

    .line 2324938
    invoke-static {v0, v2, v1}, LX/GAg;->a(LX/GAg;Lcom/facebook/Profile;Z)V

    .line 2324939
    :cond_1
    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2324940
    invoke-static {}, LX/GAg;->a()LX/GAg;

    move-result-object v0

    .line 2324941
    iget-object v1, v0, LX/GAg;->d:Lcom/facebook/Profile;

    move-object v0, v1

    .line 2324942
    move-object v0, v0

    .line 2324943
    if-nez v0, :cond_2

    .line 2324944
    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v0

    .line 2324945
    if-nez v0, :cond_4

    .line 2324946
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/facebook/Profile;->a(Lcom/facebook/Profile;)V

    .line 2324947
    :cond_2
    :goto_1
    const/4 v0, 0x0

    return-object v0

    :catch_0
    :cond_3
    move-object v3, v4

    goto :goto_0

    .line 2324948
    :cond_4
    iget-object v1, v0, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v0, v1

    .line 2324949
    new-instance v1, LX/GAd;

    invoke-direct {v1}, LX/GAd;-><init>()V

    invoke-static {v0, v1}, LX/Gsc;->a(Ljava/lang/String;LX/GAc;)V

    goto :goto_1
.end method
