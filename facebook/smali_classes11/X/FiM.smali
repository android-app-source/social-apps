.class public LX/FiM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:I

.field public static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CwF;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:I

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CwF;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:I

.field public static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CwF;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CwF;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const v5, -0xeb425a

    const v4, -0xc5a768

    const v3, -0xca6e01

    .line 2275122
    invoke-static {}, LX/CwF;->values()[LX/CwF;

    move-result-object v0

    array-length v0, v0

    sput v0, LX/FiM;->a:I

    .line 2275123
    const v0, 0x7f0210d9

    sput v0, LX/FiM;->c:I

    .line 2275124
    const v0, 0x7f0217cd

    sput v0, LX/FiM;->e:I

    .line 2275125
    const v0, 0x7f0e087b

    sput v0, LX/FiM;->h:I

    .line 2275126
    new-instance v0, Ljava/util/HashMap;

    sget v1, LX/FiM;->a:I

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 2275127
    sput-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->keyword:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275128
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->trending:LX/CwF;

    const v2, 0x7f021101

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275129
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->celebrity:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275130
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->echo:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275131
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->escape:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275132
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->recent:LX/CwF;

    const v2, 0x7f021100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275133
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->am_football:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275134
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->photos:LX/CwF;

    const v2, 0x7f0210ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275135
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->videos:LX/CwF;

    const v2, 0x7f021103

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275136
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->hashtag:LX/CwF;

    const v2, 0x7f0210d8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275137
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->local:LX/CwF;

    const v2, 0x7f0210da

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275138
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->company:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275139
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->movie:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275140
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->happening_now:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275141
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->link:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275142
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->special_intent_gener:LX/CwF;

    const v2, 0x7f0210d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275143
    sget-object v0, LX/FiM;->b:Ljava/util/Map;

    sget-object v1, LX/CwF;->local_category:LX/CwF;

    const v2, 0x7f0210da

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275144
    new-instance v0, Ljava/util/HashMap;

    sget v1, LX/FiM;->a:I

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 2275145
    sput-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->keyword:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275146
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->trending:LX/CwF;

    const v2, 0x7f0217df

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275147
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->celebrity:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275148
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->echo:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275149
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->escape:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275150
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->recent:LX/CwF;

    const v2, 0x7f0217d4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275151
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->am_football:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275152
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->photos:LX/CwF;

    const v2, 0x7f0217d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275153
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->videos:LX/CwF;

    const v2, 0x7f0217e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275154
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->hashtag:LX/CwF;

    const v2, 0x7f0217cb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275155
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->local:LX/CwF;

    const v2, 0x7f0217ce

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275156
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->company:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275157
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->movie:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275158
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->happening_now:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275159
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->link:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275160
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->special_intent_gener:LX/CwF;

    const v2, 0x7f0217cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275161
    sget-object v0, LX/FiM;->d:Ljava/util/Map;

    sget-object v1, LX/CwF;->local_category:LX/CwF;

    const v2, 0x7f0217ce

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275162
    new-instance v0, Ljava/util/HashMap;

    sget v1, LX/FiM;->a:I

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 2275163
    sput-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->keyword:LX/CwF;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275164
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->trending:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275165
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->celebrity:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275166
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->echo:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275167
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->escape:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275168
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->recent:LX/CwF;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275169
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->am_football:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275170
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->photos:LX/CwF;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275171
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->videos:LX/CwF;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275172
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->hashtag:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275173
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->local:LX/CwF;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275174
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->company:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275175
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->movie:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275176
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->happening_now:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275177
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->link:LX/CwF;

    const v2, -0x55d279

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275178
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->special_intent_gener:LX/CwF;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275179
    sget-object v0, LX/FiM;->f:Ljava/util/Map;

    sget-object v1, LX/CwF;->local_category:LX/CwF;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275180
    new-instance v0, Ljava/util/HashMap;

    sget v1, LX/FiM;->a:I

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 2275181
    sput-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->keyword:LX/CwF;

    const v2, 0x7f0e087b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275182
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->trending:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275183
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->celebrity:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275184
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->echo:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275185
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->escape:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275186
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->recent:LX/CwF;

    const v2, 0x7f0e087b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275187
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->am_football:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275188
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->photos:LX/CwF;

    const v2, 0x7f0e087e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275189
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->videos:LX/CwF;

    const v2, 0x7f0e087e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275190
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->hashtag:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275191
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->local:LX/CwF;

    const v2, 0x7f0e087b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275192
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->company:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275193
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->movie:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275194
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->happening_now:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275195
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->link:LX/CwF;

    const v2, 0x7f0e087f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275196
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->special_intent_gener:LX/CwF;

    const v2, 0x7f0e087c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275197
    sget-object v0, LX/FiM;->g:Ljava/util/Map;

    sget-object v1, LX/CwF;->local_category:LX/CwF;

    const v2, 0x7f0e087b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275198
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2275199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
