.class public LX/GUm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public final b:LX/11S;


# direct methods
.method public constructor <init>(LX/0SG;LX/11S;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2357920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2357921
    iput-object p1, p0, LX/GUm;->a:LX/0SG;

    .line 2357922
    iput-object p2, p0, LX/GUm;->b:LX/11S;

    .line 2357923
    return-void
.end method

.method public static a(LX/0QB;)LX/GUm;
    .locals 1

    .prologue
    .line 2357924
    invoke-static {p0}, LX/GUm;->b(LX/0QB;)LX/GUm;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GUm;
    .locals 3

    .prologue
    .line 2357925
    new-instance v2, LX/GUm;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11S;

    invoke-direct {v2, v0, v1}, LX/GUm;-><init>(LX/0SG;LX/11S;)V

    .line 2357926
    return-object v2
.end method


# virtual methods
.method public final a()J
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2357927
    iget-object v0, p0, LX/GUm;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2357928
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2357929
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2357930
    const/16 v3, 0xb

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 2357931
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v5}, Ljava/util/Calendar;->set(II)V

    .line 2357932
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v5}, Ljava/util/Calendar;->set(II)V

    .line 2357933
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v0, v4, v0

    if-gez v0, :cond_0

    .line 2357934
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 2357935
    :cond_0
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)J
    .locals 3

    .prologue
    .line 2357936
    iget-object v0, p0, LX/GUm;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2357937
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2357938
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2357939
    const/16 v0, 0xe

    long-to-int v1, p1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 2357940
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 2357941
    invoke-virtual {p0}, LX/GUm;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/GUm;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method
