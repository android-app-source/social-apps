.class public LX/GK1;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

.field public b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

.field private c:LX/GG6;


# direct methods
.method public constructor <init>(LX/GG6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340153
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340154
    iput-object p1, p0, LX/GK1;->c:LX/GG6;

    .line 2340155
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2340156
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340157
    const/4 v0, 0x0

    iput-object v0, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340158
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3

    .prologue
    .line 2340159
    check-cast p1, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340160
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340161
    const v0, 0x7f080aa8

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2340162
    iput-object p1, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340163
    iget-object v0, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    new-instance v1, LX/GG9;

    invoke-direct {v1}, LX/GG9;-><init>()V

    const/4 v2, 0x2

    .line 2340164
    iput v2, v1, LX/GG9;->g:I

    .line 2340165
    move-object v1, v1

    .line 2340166
    iget-object v2, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j()I

    move-result v2

    iget-object p1, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v2, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 2340167
    iput-object v2, v1, LX/GG9;->a:Ljava/lang/String;

    .line 2340168
    move-object v1, v1

    .line 2340169
    iget-object v2, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f080aae

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2340170
    iput-object v2, v1, LX/GG9;->b:Ljava/lang/String;

    .line 2340171
    move-object v1, v1

    .line 2340172
    iget-object v2, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a0355

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object p1, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a0356

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v2, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2340173
    iput-object v2, v1, LX/GG9;->f:LX/0Px;

    .line 2340174
    move-object v1, v1

    .line 2340175
    iget-object v2, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f080aac

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object p1, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f080aad

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2340176
    iput-object v2, v1, LX/GG9;->d:LX/0Px;

    .line 2340177
    move-object v1, v1

    .line 2340178
    iget-object v2, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->c()I

    move-result v2

    iget-object p1, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v2, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object p1, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->e()I

    move-result p1

    iget-object p2, p0, LX/GK1;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p1, p2}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2340179
    iput-object v2, v1, LX/GG9;->c:LX/0Px;

    .line 2340180
    move-object v1, v1

    .line 2340181
    iget-object v2, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->c()I

    move-result v2

    int-to-float v2, v2

    iget-object p1, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iget-object p1, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->e()I

    move-result p1

    int-to-float p1, p1

    iget-object p2, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-static {v2, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2340182
    iput-object v2, v1, LX/GG9;->e:LX/0Px;

    .line 2340183
    move-object v1, v1

    .line 2340184
    invoke-virtual {v1}, LX/GG9;->a()LX/GGA;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setViewModel(LX/GGA;)V

    .line 2340185
    const/4 v0, 0x0

    iput-object v0, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2340186
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2340187
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    iput-object v0, p0, LX/GK1;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2340188
    return-void
.end method
