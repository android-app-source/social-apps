.class public LX/FM6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile f:LX/FM6;


# instance fields
.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/FM5;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2228353
    sget-object v0, LX/3Kr;->ag:LX/0Tn;

    const-string v1, "index"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/FM6;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2228349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2228350
    iput-object p1, p0, LX/FM6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2228351
    iput-object p2, p0, LX/FM6;->e:LX/0SG;

    .line 2228352
    return-void
.end method

.method public static a(I)LX/0Tn;
    .locals 3

    .prologue
    .line 2228316
    sget-object v0, LX/3Kr;->ag:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v2, p0, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "error_code"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/FM6;
    .locals 5

    .prologue
    .line 2228336
    sget-object v0, LX/FM6;->f:LX/FM6;

    if-nez v0, :cond_1

    .line 2228337
    const-class v1, LX/FM6;

    monitor-enter v1

    .line 2228338
    :try_start_0
    sget-object v0, LX/FM6;->f:LX/FM6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2228339
    if-eqz v2, :cond_0

    .line 2228340
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2228341
    new-instance p0, LX/FM6;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/FM6;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 2228342
    move-object v0, p0

    .line 2228343
    sput-object v0, LX/FM6;->f:LX/FM6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2228344
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2228345
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2228346
    :cond_1
    sget-object v0, LX/FM6;->f:LX/FM6;

    return-object v0

    .line 2228347
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2228348
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/FM6;)V
    .locals 10

    .prologue
    const/16 v9, 0x14

    const/4 v0, 0x0

    const/4 v8, -0x1

    .line 2228323
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/FM6;->b:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 2228324
    :cond_0
    monitor-exit p0

    return-void

    .line 2228325
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/HashMap;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, LX/FM6;->b:Ljava/util/Map;

    .line 2228326
    iget-object v1, p0, LX/FM6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/FM6;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    iput v1, p0, LX/FM6;->c:I

    .line 2228327
    :goto_0
    if-ge v0, v9, :cond_0

    .line 2228328
    iget-object v1, p0, LX/FM6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget v2, p0, LX/FM6;->c:I

    add-int/2addr v2, v0

    invoke-static {v2}, LX/FM6;->b(I)LX/0Tn;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2228329
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2228330
    iget-object v2, p0, LX/FM6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget v3, p0, LX/FM6;->c:I

    add-int/2addr v3, v0

    invoke-static {v3}, LX/FM6;->a(I)LX/0Tn;

    move-result-object v3

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 2228331
    iget-object v3, p0, LX/FM6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget v4, p0, LX/FM6;->c:I

    add-int/2addr v4, v0

    invoke-static {v4}, LX/FM6;->c(I)LX/0Tn;

    move-result-object v4

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 2228332
    if-eq v2, v8, :cond_2

    .line 2228333
    iget-object v3, p0, LX/FM6;->b:Ljava/util/Map;

    new-instance v6, LX/FM5;

    invoke-static {v2}, LX/FMM;->fromOrdinal(I)LX/FMM;

    move-result-object v2

    invoke-direct {v6, v2, v4, v5}, LX/FM5;-><init>(LX/FMM;J)V

    invoke-interface {v3, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2228334
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2228335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(I)LX/0Tn;
    .locals 3

    .prologue
    .line 2228322
    sget-object v0, LX/3Kr;->ag:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v2, p0, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "msg_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static c(I)LX/0Tn;
    .locals 3

    .prologue
    .line 2228321
    sget-object v0, LX/3Kr;->ag:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v2, p0, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "timestamp_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/FMM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2228317
    invoke-static {p0}, LX/FM6;->a(LX/FM6;)V

    .line 2228318
    iget-object v0, p0, LX/FM6;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2228319
    iget-object v0, p0, LX/FM6;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FM5;

    iget-object v0, v0, LX/FM5;->a:LX/FMM;

    .line 2228320
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
