.class public LX/GEi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/FBStepperWithLabel;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GJf;


# direct methods
.method public constructor <init>(LX/GJf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332029
    iput-object p1, p0, LX/GEi;->a:LX/GJf;

    .line 2332030
    return-void
.end method

.method public static a(LX/0QB;)LX/GEi;
    .locals 1

    .prologue
    .line 2332027
    invoke-static {p0}, LX/GEi;->b(LX/0QB;)LX/GEi;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GEi;
    .locals 3

    .prologue
    .line 2332022
    new-instance v1, LX/GEi;

    .line 2332023
    new-instance v2, LX/GJf;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v2, v0}, LX/GJf;-><init>(Landroid/content/res/Resources;)V

    .line 2332024
    move-object v0, v2

    .line 2332025
    check-cast v0, LX/GJf;

    invoke-direct {v1, v0}, LX/GEi;-><init>(LX/GJf;)V

    .line 2332026
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332031
    const v0, 0x7f030059

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2332019
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2332020
    :cond_0
    :goto_0
    return v0

    .line 2332021
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1

    .prologue
    .line 2332018
    iget-object v0, p0, LX/GEi;->a:LX/GJf;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332017
    sget-object v0, LX/8wK;->DURATION:LX/8wK;

    return-object v0
.end method
