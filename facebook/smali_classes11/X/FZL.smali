.class public final LX/FZL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/debug/SearchDebugActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/search/debug/SearchDebugActivity;)V
    .locals 0

    .prologue
    .line 2257025
    iput-object p1, p0, LX/FZL;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2257026
    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2257027
    iget-object v0, p0, LX/FZL;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v1, p0, LX/FZL;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v1, v1, Lcom/facebook/search/debug/SearchDebugActivity;->e:LX/1nD;

    sget-object v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    check-cast p2, Ljava/lang/String;

    const-string v3, "Injected ID"

    sget-object v4, LX/8ci;->g:LX/8ci;

    invoke-virtual {v1, v2, p2, v3, v4}, LX/1nD;->a(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/search/debug/SearchDebugActivity;->startActivity(Landroid/content/Intent;)V

    .line 2257028
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
