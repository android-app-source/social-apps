.class public abstract LX/GsU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/Handler;

.field public c:LX/Gz2;

.field public d:Z

.field public e:Landroid/os/Messenger;

.field public f:I

.field public g:I

.field public final h:Ljava/lang/String;

.field public final i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2399567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2399568
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2399569
    if-eqz v0, :cond_0

    move-object p1, v0

    :cond_0
    iput-object p1, p0, LX/GsU;->a:Landroid/content/Context;

    .line 2399570
    iput p2, p0, LX/GsU;->f:I

    .line 2399571
    iput p3, p0, LX/GsU;->g:I

    .line 2399572
    iput-object p5, p0, LX/GsU;->h:Ljava/lang/String;

    .line 2399573
    iput p4, p0, LX/GsU;->i:I

    .line 2399574
    new-instance v0, LX/GsT;

    invoke-direct {v0, p0}, LX/GsT;-><init>(LX/GsU;)V

    iput-object v0, p0, LX/GsU;->b:Landroid/os/Handler;

    .line 2399575
    return-void
.end method

.method public static a(LX/GsU;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2399576
    iget-boolean v0, p0, LX/GsU;->d:Z

    if-nez v0, :cond_1

    .line 2399577
    :cond_0
    :goto_0
    return-void

    .line 2399578
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GsU;->d:Z

    .line 2399579
    iget-object v0, p0, LX/GsU;->c:LX/Gz2;

    .line 2399580
    if-eqz v0, :cond_0

    .line 2399581
    iget-object v1, v0, LX/Gz2;->b:Lcom/facebook/login/GetTokenLoginMethodHandler;

    iget-object v2, v0, LX/Gz2;->a:Lcom/facebook/login/LoginClient$Request;

    const/4 v4, 0x0

    .line 2399582
    iget-object v3, v1, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    if-eqz v3, :cond_2

    .line 2399583
    iget-object v3, v1, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    .line 2399584
    iput-object v4, v3, LX/GsU;->c:LX/Gz2;

    .line 2399585
    :cond_2
    iput-object v4, v1, Lcom/facebook/login/GetTokenLoginMethodHandler;->c:LX/Gz1;

    .line 2399586
    iget-object v3, v1, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2399587
    iget-object v4, v3, Lcom/facebook/login/LoginClient;->e:LX/GzC;

    if-eqz v4, :cond_3

    .line 2399588
    iget-object v4, v3, Lcom/facebook/login/LoginClient;->e:LX/GzC;

    .line 2399589
    iget-object v5, v4, LX/GzC;->a:Landroid/view/View;

    const v3, 0x7f0d09aa

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/16 v3, 0x8

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2399590
    :cond_3
    if-eqz p1, :cond_a

    .line 2399591
    const-string v3, "com.facebook.platform.extra.PERMISSIONS"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2399592
    iget-object v3, v2, Lcom/facebook/login/LoginClient$Request;->b:Ljava/util/Set;

    move-object v3, v3

    .line 2399593
    if-eqz v4, :cond_6

    if-eqz v3, :cond_4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2399594
    :cond_4
    const-string v3, "com.facebook.platform.extra.USER_ID"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2399595
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2399596
    :cond_5
    iget-object v3, v1, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v3}, Lcom/facebook/login/LoginClient;->h()V

    .line 2399597
    const-string v3, "com.facebook.platform.extra.ACCESS_TOKEN"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2399598
    new-instance v4, LX/Gz3;

    invoke-direct {v4, v1, p1, v2}, LX/Gz3;-><init>(Lcom/facebook/login/GetTokenLoginMethodHandler;Landroid/os/Bundle;Lcom/facebook/login/LoginClient$Request;)V

    invoke-static {v3, v4}, LX/Gsc;->a(Ljava/lang/String;LX/GAc;)V

    .line 2399599
    :goto_1
    goto :goto_0

    .line 2399600
    :cond_6
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 2399601
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_7
    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2399602
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2399603
    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2399604
    :cond_8
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 2399605
    const-string v3, "new_permissions"

    const-string v4, ","

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2399606
    :cond_9
    const-string v3, "permissions"

    invoke-static {v5, v3}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2399607
    iput-object v5, v2, Lcom/facebook/login/LoginClient$Request;->b:Ljava/util/Set;

    .line 2399608
    :cond_a
    iget-object v3, v1, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v3}, Lcom/facebook/login/LoginClient;->g()V

    goto :goto_1

    .line 2399609
    :cond_b
    invoke-virtual {v1, v2, p1}, Lcom/facebook/login/GetTokenLoginMethodHandler;->b(Lcom/facebook/login/LoginClient$Request;Landroid/os/Bundle;)V

    goto :goto_1
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 2399610
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, LX/GsU;->e:Landroid/os/Messenger;

    .line 2399611
    const/4 p2, 0x0

    .line 2399612
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2399613
    const-string v1, "com.facebook.platform.extra.APPLICATION_ID"

    iget-object p1, p0, LX/GsU;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399614
    iget v1, p0, LX/GsU;->f:I

    invoke-static {p2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 2399615
    iget p1, p0, LX/GsU;->i:I

    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 2399616
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 2399617
    new-instance v0, Landroid/os/Messenger;

    iget-object p1, p0, LX/GsU;->b:Landroid/os/Handler;

    invoke-direct {v0, p1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 2399618
    :try_start_0
    iget-object v0, p0, LX/GsU;->e:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2399619
    :goto_0
    return-void

    .line 2399620
    :catch_0
    invoke-static {p0, p2}, LX/GsU;->a(LX/GsU;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2399621
    iput-object v2, p0, LX/GsU;->e:Landroid/os/Messenger;

    .line 2399622
    :try_start_0
    iget-object v0, p0, LX/GsU;->a:Landroid/content/Context;

    const v1, 0x5d47f11c

    invoke-static {v0, p0, v1}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2399623
    :goto_0
    invoke-static {p0, v2}, LX/GsU;->a(LX/GsU;Landroid/os/Bundle;)V

    .line 2399624
    return-void

    :catch_0
    goto :goto_0
.end method
