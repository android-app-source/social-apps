.class public final enum LX/FMK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FMK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FMK;

.field public static final enum ANONYMOUS_ROW:LX/FMK;

.field public static final enum BLOCKLIST_FROM_PEOPLE_SETTINGS:LX/FMK;

.field public static final enum BLOCKLIST_FROM_SMS_SETTINGS:LX/FMK;

.field public static final enum BLOCK_FROM_SPAM_THREAD_VIEW:LX/FMK;

.field public static final enum CALL_LOG_UPSELL:LX/FMK;

.field public static final enum CAMPAIGN_PAGE:LX/FMK;

.field public static final enum DELETE_MESSAGE:LX/FMK;

.field public static final enum DELETE_THREAD:LX/FMK;

.field public static final enum DOWNLOAD_MESSAGE:LX/FMK;

.field public static final enum INBOX_FILTER_OPTIN:LX/FMK;

.field public static final enum INFO_QP:LX/FMK;

.field public static final enum INTERNAL_SETTING:LX/FMK;

.field public static final enum KILL_SWITCH:LX/FMK;

.field public static final enum LONG_PRESS_BLOCK_OPTION:LX/FMK;

.field public static final enum MARK_READ:LX/FMK;

.field public static final enum MARK_UNREAD:LX/FMK;

.field public static final enum OPT_IN:LX/FMK;

.field public static final enum PERMISSION_CHANGE:LX/FMK;

.field public static final enum PHONEBOOK_INTEGRATION:LX/FMK;

.field public static final enum RETRY_MESSAGE:LX/FMK;

.field public static final enum RETURNING_RO_DELAYED_QP:LX/FMK;

.field public static final enum RETURNING_RO_QP:LX/FMK;

.field public static final enum RO2F_PROMO:LX/FMK;

.field public static final enum SEND_MESSAGE:LX/FMK;

.field public static final enum SETTINGS:LX/FMK;

.field public static final enum SETTINGS_FROM_FULL_THREAD_VIEW_QP:LX/FMK;

.field public static final enum SETTINGS_FROM_INFO_QP:LX/FMK;

.field public static final enum SETTINGS_FROM_NUX:LX/FMK;

.field public static final enum SETTINGS_FROM_RETURNING_RO_DELAYED_QP:LX/FMK;

.field public static final enum SETTINGS_FROM_RETURNING_RO_QP:LX/FMK;

.field public static final enum SETTINGS_FROM_THREAD_DELETE_CONFIRMATION_DIALOG:LX/FMK;

.field public static final enum SMS_BRIDGE_UPSELL:LX/FMK;

.field public static final enum SMS_LOG_UPSELL:LX/FMK;

.field public static final enum SMS_SHORTCUT:LX/FMK;

.field public static final enum SMS_THREAD_COMPOSER:LX/FMK;

.field public static final enum SWITCH_ACCOUNTS:LX/FMK;

.field public static final enum SYSTEM_CHANGE:LX/FMK;

.field public static final enum THREAD_SETTINGS:LX/FMK;

.field public static final enum THREAD_SETTINGS_UPSELL:LX/FMK;

.field public static final enum UNDEFINED:LX/FMK;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2229535
    new-instance v0, LX/FMK;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v3}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->UNDEFINED:LX/FMK;

    .line 2229536
    new-instance v0, LX/FMK;

    const-string v1, "SETTINGS"

    invoke-direct {v0, v1, v4}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SETTINGS:LX/FMK;

    .line 2229537
    new-instance v0, LX/FMK;

    const-string v1, "SETTINGS_FROM_NUX"

    invoke-direct {v0, v1, v5}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SETTINGS_FROM_NUX:LX/FMK;

    .line 2229538
    new-instance v0, LX/FMK;

    const-string v1, "SETTINGS_FROM_THREAD_DELETE_CONFIRMATION_DIALOG"

    invoke-direct {v0, v1, v6}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SETTINGS_FROM_THREAD_DELETE_CONFIRMATION_DIALOG:LX/FMK;

    .line 2229539
    new-instance v0, LX/FMK;

    const-string v1, "SETTINGS_FROM_FULL_THREAD_VIEW_QP"

    invoke-direct {v0, v1, v7}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SETTINGS_FROM_FULL_THREAD_VIEW_QP:LX/FMK;

    .line 2229540
    new-instance v0, LX/FMK;

    const-string v1, "SETTINGS_FROM_INFO_QP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SETTINGS_FROM_INFO_QP:LX/FMK;

    .line 2229541
    new-instance v0, LX/FMK;

    const-string v1, "SETTINGS_FROM_RETURNING_RO_QP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SETTINGS_FROM_RETURNING_RO_QP:LX/FMK;

    .line 2229542
    new-instance v0, LX/FMK;

    const-string v1, "SETTINGS_FROM_RETURNING_RO_DELAYED_QP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SETTINGS_FROM_RETURNING_RO_DELAYED_QP:LX/FMK;

    .line 2229543
    new-instance v0, LX/FMK;

    const-string v1, "OPT_IN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->OPT_IN:LX/FMK;

    .line 2229544
    new-instance v0, LX/FMK;

    const-string v1, "RO2F_PROMO"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->RO2F_PROMO:LX/FMK;

    .line 2229545
    new-instance v0, LX/FMK;

    const-string v1, "INFO_QP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->INFO_QP:LX/FMK;

    .line 2229546
    new-instance v0, LX/FMK;

    const-string v1, "RETURNING_RO_QP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->RETURNING_RO_QP:LX/FMK;

    .line 2229547
    new-instance v0, LX/FMK;

    const-string v1, "RETURNING_RO_DELAYED_QP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->RETURNING_RO_DELAYED_QP:LX/FMK;

    .line 2229548
    new-instance v0, LX/FMK;

    const-string v1, "KILL_SWITCH"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->KILL_SWITCH:LX/FMK;

    .line 2229549
    new-instance v0, LX/FMK;

    const-string v1, "SYSTEM_CHANGE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SYSTEM_CHANGE:LX/FMK;

    .line 2229550
    new-instance v0, LX/FMK;

    const-string v1, "PERMISSION_CHANGE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->PERMISSION_CHANGE:LX/FMK;

    .line 2229551
    new-instance v0, LX/FMK;

    const-string v1, "CALL_LOG_UPSELL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->CALL_LOG_UPSELL:LX/FMK;

    .line 2229552
    new-instance v0, LX/FMK;

    const-string v1, "SMS_LOG_UPSELL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SMS_LOG_UPSELL:LX/FMK;

    .line 2229553
    new-instance v0, LX/FMK;

    const-string v1, "ANONYMOUS_ROW"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->ANONYMOUS_ROW:LX/FMK;

    .line 2229554
    new-instance v0, LX/FMK;

    const-string v1, "SMS_BRIDGE_UPSELL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SMS_BRIDGE_UPSELL:LX/FMK;

    .line 2229555
    new-instance v0, LX/FMK;

    const-string v1, "THREAD_SETTINGS_UPSELL"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->THREAD_SETTINGS_UPSELL:LX/FMK;

    .line 2229556
    new-instance v0, LX/FMK;

    const-string v1, "DELETE_THREAD"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->DELETE_THREAD:LX/FMK;

    .line 2229557
    new-instance v0, LX/FMK;

    const-string v1, "SEND_MESSAGE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SEND_MESSAGE:LX/FMK;

    .line 2229558
    new-instance v0, LX/FMK;

    const-string v1, "RETRY_MESSAGE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->RETRY_MESSAGE:LX/FMK;

    .line 2229559
    new-instance v0, LX/FMK;

    const-string v1, "DELETE_MESSAGE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->DELETE_MESSAGE:LX/FMK;

    .line 2229560
    new-instance v0, LX/FMK;

    const-string v1, "DOWNLOAD_MESSAGE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->DOWNLOAD_MESSAGE:LX/FMK;

    .line 2229561
    new-instance v0, LX/FMK;

    const-string v1, "MARK_READ"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->MARK_READ:LX/FMK;

    .line 2229562
    new-instance v0, LX/FMK;

    const-string v1, "MARK_UNREAD"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->MARK_UNREAD:LX/FMK;

    .line 2229563
    new-instance v0, LX/FMK;

    const-string v1, "INBOX_FILTER_OPTIN"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->INBOX_FILTER_OPTIN:LX/FMK;

    .line 2229564
    new-instance v0, LX/FMK;

    const-string v1, "PHONEBOOK_INTEGRATION"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->PHONEBOOK_INTEGRATION:LX/FMK;

    .line 2229565
    new-instance v0, LX/FMK;

    const-string v1, "SMS_SHORTCUT"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SMS_SHORTCUT:LX/FMK;

    .line 2229566
    new-instance v0, LX/FMK;

    const-string v1, "INTERNAL_SETTING"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->INTERNAL_SETTING:LX/FMK;

    .line 2229567
    new-instance v0, LX/FMK;

    const-string v1, "LONG_PRESS_BLOCK_OPTION"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->LONG_PRESS_BLOCK_OPTION:LX/FMK;

    .line 2229568
    new-instance v0, LX/FMK;

    const-string v1, "BLOCKLIST_FROM_SMS_SETTINGS"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->BLOCKLIST_FROM_SMS_SETTINGS:LX/FMK;

    .line 2229569
    new-instance v0, LX/FMK;

    const-string v1, "BLOCKLIST_FROM_PEOPLE_SETTINGS"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->BLOCKLIST_FROM_PEOPLE_SETTINGS:LX/FMK;

    .line 2229570
    new-instance v0, LX/FMK;

    const-string v1, "BLOCK_FROM_SPAM_THREAD_VIEW"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->BLOCK_FROM_SPAM_THREAD_VIEW:LX/FMK;

    .line 2229571
    new-instance v0, LX/FMK;

    const-string v1, "THREAD_SETTINGS"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->THREAD_SETTINGS:LX/FMK;

    .line 2229572
    new-instance v0, LX/FMK;

    const-string v1, "CAMPAIGN_PAGE"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->CAMPAIGN_PAGE:LX/FMK;

    .line 2229573
    new-instance v0, LX/FMK;

    const-string v1, "SWITCH_ACCOUNTS"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SWITCH_ACCOUNTS:LX/FMK;

    .line 2229574
    new-instance v0, LX/FMK;

    const-string v1, "SMS_THREAD_COMPOSER"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, LX/FMK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMK;->SMS_THREAD_COMPOSER:LX/FMK;

    .line 2229575
    const/16 v0, 0x28

    new-array v0, v0, [LX/FMK;

    sget-object v1, LX/FMK;->UNDEFINED:LX/FMK;

    aput-object v1, v0, v3

    sget-object v1, LX/FMK;->SETTINGS:LX/FMK;

    aput-object v1, v0, v4

    sget-object v1, LX/FMK;->SETTINGS_FROM_NUX:LX/FMK;

    aput-object v1, v0, v5

    sget-object v1, LX/FMK;->SETTINGS_FROM_THREAD_DELETE_CONFIRMATION_DIALOG:LX/FMK;

    aput-object v1, v0, v6

    sget-object v1, LX/FMK;->SETTINGS_FROM_FULL_THREAD_VIEW_QP:LX/FMK;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FMK;->SETTINGS_FROM_INFO_QP:LX/FMK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FMK;->SETTINGS_FROM_RETURNING_RO_QP:LX/FMK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FMK;->SETTINGS_FROM_RETURNING_RO_DELAYED_QP:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FMK;->OPT_IN:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FMK;->RO2F_PROMO:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/FMK;->INFO_QP:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/FMK;->RETURNING_RO_QP:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/FMK;->RETURNING_RO_DELAYED_QP:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/FMK;->KILL_SWITCH:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/FMK;->SYSTEM_CHANGE:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/FMK;->PERMISSION_CHANGE:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/FMK;->CALL_LOG_UPSELL:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/FMK;->SMS_LOG_UPSELL:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/FMK;->ANONYMOUS_ROW:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/FMK;->SMS_BRIDGE_UPSELL:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/FMK;->THREAD_SETTINGS_UPSELL:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/FMK;->DELETE_THREAD:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/FMK;->SEND_MESSAGE:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/FMK;->RETRY_MESSAGE:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/FMK;->DELETE_MESSAGE:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/FMK;->DOWNLOAD_MESSAGE:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/FMK;->MARK_READ:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/FMK;->MARK_UNREAD:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/FMK;->INBOX_FILTER_OPTIN:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/FMK;->PHONEBOOK_INTEGRATION:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/FMK;->SMS_SHORTCUT:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/FMK;->INTERNAL_SETTING:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/FMK;->LONG_PRESS_BLOCK_OPTION:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/FMK;->BLOCKLIST_FROM_SMS_SETTINGS:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/FMK;->BLOCKLIST_FROM_PEOPLE_SETTINGS:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/FMK;->BLOCK_FROM_SPAM_THREAD_VIEW:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/FMK;->THREAD_SETTINGS:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/FMK;->CAMPAIGN_PAGE:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/FMK;->SWITCH_ACCOUNTS:LX/FMK;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/FMK;->SMS_THREAD_COMPOSER:LX/FMK;

    aput-object v2, v0, v1

    sput-object v0, LX/FMK;->$VALUES:[LX/FMK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2229576
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FMK;
    .locals 1

    .prologue
    .line 2229577
    const-class v0, LX/FMK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FMK;

    return-object v0
.end method

.method public static values()[LX/FMK;
    .locals 1

    .prologue
    .line 2229578
    sget-object v0, LX/FMK;->$VALUES:[LX/FMK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FMK;

    return-object v0
.end method
