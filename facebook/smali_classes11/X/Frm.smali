.class public final LX/Frm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fso;

.field public final synthetic b:J

.field public final synthetic c:LX/FqO;

.field public final synthetic d:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

.field public final synthetic e:LX/Frv;

.field private f:Lcom/facebook/graphql/model/GraphQLTimelineSection;


# direct methods
.method public constructor <init>(LX/Frv;LX/Fso;JLX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 1

    .prologue
    .line 2296057
    iput-object p1, p0, LX/Frm;->e:LX/Frv;

    iput-object p2, p0, LX/Frm;->a:LX/Fso;

    iput-wide p3, p0, LX/Frm;->b:J

    iput-object p5, p0, LX/Frm;->c:LX/FqO;

    iput-object p6, p0, LX/Frm;->d:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 2296058
    iget-object v2, p0, LX/Frm;->f:Lcom/facebook/graphql/model/GraphQLTimelineSection;

    iget-object v3, p0, LX/Frm;->a:LX/Fso;

    iget-wide v4, p0, LX/Frm;->b:J

    sget-object v6, LX/0ta;->FROM_SERVER:LX/0ta;

    sget-object v7, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    iget-object v8, p0, LX/Frm;->c:LX/FqO;

    iget-object v9, p0, LX/Frm;->d:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    .line 2296059
    invoke-static/range {v2 .. v9}, LX/Frv;->a(Lcom/facebook/graphql/model/GraphQLTimelineSection;LX/Fso;JLX/0ta;Lcom/facebook/timeline/protocol/ResultSource;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296060
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2296061
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTimelineSection;

    .line 2296062
    iget-object v0, p0, LX/Frm;->a:LX/Fso;

    iget-object v0, v0, LX/Fso;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2296063
    const-string v0, "Requested more units for section %s; got a GraphQLTimelineSection with ID %s"

    iget-object v1, p0, LX/Frm;->a:LX/Fso;

    iget-object v1, v1, LX/Fso;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2296064
    iget-object v1, p0, LX/Frm;->e:LX/Frv;

    iget-object v1, v1, LX/Frv;->f:LX/03V;

    const-string v2, "timeline_unexpected_section_id_from_server"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2296065
    :cond_0
    iget-object v1, p0, LX/Frm;->a:LX/Fso;

    iget-object v2, p0, LX/Frm;->c:LX/FqO;

    iget-object v3, p0, LX/Frm;->d:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    .line 2296066
    invoke-static {v1, p1, v2, v3}, LX/Frv;->a(LX/Fso;Lcom/facebook/graphql/model/GraphQLTimelineSection;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296067
    iput-object p1, p0, LX/Frm;->f:Lcom/facebook/graphql/model/GraphQLTimelineSection;

    .line 2296068
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2296069
    instance-of v0, p1, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Frm;->f:Lcom/facebook/graphql/model/GraphQLTimelineSection;

    if-eqz v0, :cond_0

    .line 2296070
    invoke-virtual {p0}, LX/Frm;->a()V

    .line 2296071
    :goto_0
    return-void

    .line 2296072
    :cond_0
    iget-object v0, p0, LX/Frm;->e:LX/Frv;

    iget-object v1, p0, LX/Frm;->a:LX/Fso;

    iget-object v2, p0, LX/Frm;->f:Lcom/facebook/graphql/model/GraphQLTimelineSection;

    invoke-static {v0, v1, v2}, LX/Frv;->a(LX/Frv;LX/Fso;Lcom/facebook/graphql/model/GraphQLTimelineSection;)LX/Fso;

    move-result-object v0

    .line 2296073
    iget-object v1, p0, LX/Frm;->c:LX/FqO;

    invoke-interface {v1, v0}, LX/FqO;->b(LX/Fso;)V

    goto :goto_0
.end method
