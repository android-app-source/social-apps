.class public LX/GVv;
.super LX/1Cv;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;",
            "LX/GVw;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

.field public f:LX/7iP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2360237
    const-class v0, LX/GVv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GVv;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2360218
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2360219
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2360220
    iput-object v0, p0, LX/GVv;->d:LX/0Px;

    .line 2360221
    iput-object p1, p0, LX/GVv;->c:LX/0Ot;

    .line 2360222
    new-instance v0, LX/GWH;

    invoke-direct {v0}, LX/GWH;-><init>()V

    invoke-virtual {v0}, LX/GWH;->a()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-result-object v0

    iput-object v0, p0, LX/GVv;->e:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2360223
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    invoke-static {v0}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, LX/GVv;->b:Ljava/util/EnumMap;

    .line 2360224
    sget-object v2, LX/GW2;->f:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GVw;

    .line 2360225
    iget-object v4, p0, LX/GVv;->b:Ljava/util/EnumMap;

    invoke-interface {v0}, LX/GVw;->a()Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2360226
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2360227
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2360228
    sget-object v0, LX/GW2;->f:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GVw;

    iget-object v1, p0, LX/GVv;->f:LX/7iP;

    invoke-interface {v0, p2, v1}, LX/GVw;->a(Landroid/view/ViewGroup;LX/7iP;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2360229
    check-cast p2, LX/DMB;

    .line 2360230
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2360231
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2360232
    iget-object v0, p0, LX/GVv;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2360233
    iget-object v0, p0, LX/GVv;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2360234
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2360235
    sget-object v1, LX/GW2;->f:LX/0Px;

    iget-object v0, p0, LX/GVv;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2360236
    sget-object v0, LX/GW2;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
