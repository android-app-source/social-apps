.class public final LX/F37;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V
    .locals 0

    .prologue
    .line 2193664
    iput-object p1, p0, LX/F37;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2193657
    iget-object v0, p0, LX/F37;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/F37;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->e:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2193658
    const-string v3, "https://m.facebook.com/help/312721188838183"

    move-object v3, v3

    .line 2193659
    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2193660
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2193661
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2193662
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2193663
    return-void
.end method
