.class public final LX/GrG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Cub;

.field public final synthetic b:LX/Cuw;

.field public final synthetic c:Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;LX/Cub;LX/Cuw;)V
    .locals 0

    .prologue
    .line 2397784
    iput-object p1, p0, LX/GrG;->c:Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;

    iput-object p2, p0, LX/GrG;->a:LX/Cub;

    iput-object p3, p0, LX/GrG;->b:LX/Cuw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x6c27a2b5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2397785
    iget-object v1, p0, LX/GrG;->a:LX/Cub;

    invoke-virtual {v1}, LX/Cub;->a()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    .line 2397786
    iget-object v2, p0, LX/GrG;->c:Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;

    .line 2397787
    iget-boolean p1, v2, LX/Cud;->e:Z

    move v2, p1

    .line 2397788
    if-eqz v2, :cond_2

    iget-object v2, p0, LX/GrG;->b:LX/Cuw;

    invoke-virtual {v2}, LX/Cuw;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/GrG;->b:LX/Cuw;

    invoke-virtual {v2}, LX/Cuw;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2397789
    :cond_0
    iget-object v1, p0, LX/GrG;->c:Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;

    invoke-virtual {v1}, LX/Cud;->a()V

    .line 2397790
    :cond_1
    :goto_0
    const v1, -0x6f4d7871

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2397791
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2397792
    iget-object v1, p0, LX/GrG;->b:LX/Cuw;

    invoke-virtual {v1}, LX/Cuw;->g()V

    .line 2397793
    iget-object v1, p0, LX/GrG;->a:LX/Cub;

    sget-object v2, LX/Crd;->CLICK_MEDIA:LX/Crd;

    invoke-virtual {v1, v2}, LX/Cts;->a(LX/Crd;)Z

    goto :goto_0
.end method
