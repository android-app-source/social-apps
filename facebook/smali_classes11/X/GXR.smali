.class public final LX/GXR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:LX/GXQ;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2364120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2364121
    if-nez p1, :cond_0

    .line 2364122
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Can\'t add NULL productImage"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2364123
    :cond_0
    instance-of v0, p1, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;

    if-eqz v0, :cond_1

    .line 2364124
    sget-object v0, LX/GXQ;->PRODUCT_IMAGE:LX/GXQ;

    iput-object v0, p0, LX/GXR;->b:LX/GXQ;

    .line 2364125
    :goto_0
    iput-object p1, p0, LX/GXR;->a:Ljava/lang/Object;

    .line 2364126
    return-void

    .line 2364127
    :cond_1
    instance-of v0, p1, Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_2

    .line 2364128
    sget-object v0, LX/GXQ;->MEDIA_ITEM:LX/GXQ;

    iput-object v0, p0, LX/GXR;->b:LX/GXQ;

    goto :goto_0

    .line 2364129
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Object type: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2364130
    iget-object v0, p0, LX/GXR;->b:LX/GXQ;

    sget-object v1, LX/GXQ;->PRODUCT_IMAGE:LX/GXQ;

    if-ne v0, v1, :cond_0

    .line 2364131
    iget-object v0, p0, LX/GXR;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;->c()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2364132
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GXR;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2364119
    invoke-virtual {p0}, LX/GXR;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
