.class public final LX/FHd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/FHr;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FHe;

.field private final b:LX/FHc;


# direct methods
.method public constructor <init>(LX/FHe;LX/FHc;)V
    .locals 0

    .prologue
    .line 2220778
    iput-object p1, p0, LX/FHd;->a:LX/FHe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220779
    iput-object p2, p0, LX/FHd;->b:LX/FHc;

    .line 2220780
    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 2220781
    const/4 v6, 0x0

    .line 2220782
    :try_start_0
    iget-object v0, p0, LX/FHd;->b:LX/FHc;

    iget-object v1, v0, LX/FHc;->a:Ljava/lang/String;

    .line 2220783
    iget-object v0, p0, LX/FHd;->a:LX/FHe;

    iget-object v0, v0, LX/FHe;->e:LX/0QI;

    invoke-interface {v0, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2220784
    iget-object v0, p0, LX/FHd;->b:LX/FHc;

    iget-object v0, v0, LX/FHc;->i:LX/7ys;

    .line 2220785
    iget-boolean v2, v0, LX/7ys;->b:Z

    move v0, v2

    .line 2220786
    if-eqz v0, :cond_8

    .line 2220787
    iget-object v0, p0, LX/FHd;->b:LX/FHc;

    iget-object v0, v0, LX/FHc;->i:LX/7ys;

    .line 2220788
    iget-object v2, v0, LX/7ys;->d:LX/7yr;

    move-object v3, v2
    :try_end_0
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2220789
    :try_start_1
    new-instance v0, LX/7yy;

    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->c:Ljava/io/File;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v4}, LX/7yy;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, LX/7yr;->getDigestInstanceString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v4, v4, LX/FHc;->i:LX/7ys;

    .line 2220790
    iget v7, v4, LX/7ys;->c:I

    move v4, v7

    .line 2220791
    invoke-virtual {v0, v2, v4}, LX/7yy;->a(Ljava/lang/String;I)LX/7yx;

    move-result-object v0

    .line 2220792
    if-eqz v0, :cond_7

    .line 2220793
    iget-object v2, v0, LX/7yx;->a:[B

    move-object v0, v2

    .line 2220794
    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 2220795
    :try_start_2
    new-instance v0, LX/FGT;

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v4, v4, LX/FHc;->g:LX/FHg;

    invoke-direct/range {v0 .. v5}, LX/FGT;-><init>(Ljava/lang/String;Ljava/lang/String;LX/7yr;LX/FHg;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    move-object v10, v2

    .line 2220796
    :goto_1
    if-nez v0, :cond_0

    .line 2220797
    :try_start_3
    new-instance v0, LX/FGT;

    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->g:LX/FHg;

    invoke-direct {v0, v1, v2, v5}, LX/FGT;-><init>(Ljava/lang/String;LX/FHg;Ljava/lang/String;)V

    .line 2220798
    :cond_0
    iget-object v2, p0, LX/FHd;->a:LX/FHe;

    iget-object v2, v2, LX/FHe;->b:LX/18V;

    iget-object v3, p0, LX/FHd;->a:LX/FHe;

    iget-object v3, v3, LX/FHe;->c:LX/FGV;

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v4, v4, LX/FHc;->b:LX/14U;

    invoke-virtual {v2, v3, v0, v4}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGU;

    .line 2220799
    iget-object v2, v0, LX/FGU;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 2220800
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/FGU;->c:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2220801
    iget-object v5, v0, LX/FGU;->c:Ljava/lang/String;

    .line 2220802
    iget-object v2, p0, LX/FHd;->a:LX/FHe;

    iget-object v2, v2, LX/FHe;->e:LX/0QI;

    invoke-interface {v2, v1, v5}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    move-object v12, v5

    .line 2220803
    iget-object v2, v0, LX/FGU;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_5

    move-object v10, v6

    .line 2220804
    :goto_2
    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->h:LX/FHj;

    if-eqz v2, :cond_2

    .line 2220805
    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->h:LX/FHj;

    invoke-virtual {v2}, LX/FHj;->a()V

    .line 2220806
    :cond_2
    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 2220807
    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->b:LX/14U;

    if-eqz v2, :cond_3

    .line 2220808
    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->b:LX/14U;

    .line 2220809
    iget-object v3, v2, LX/14U;->a:LX/4ck;

    move-object v2, v3

    .line 2220810
    if-eqz v2, :cond_3

    instance-of v3, v2, LX/FHk;

    if-eqz v3, :cond_3

    .line 2220811
    check-cast v2, LX/FHk;

    iget-object v0, v0, LX/FGU;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    .line 2220812
    iput-wide v4, v2, LX/FHk;->c:J
    :try_end_3
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2220813
    :cond_3
    :try_start_4
    iget-object v0, p0, LX/FHd;->a:LX/FHe;

    iget-object v0, v0, LX/FHe;->b:LX/18V;

    iget-object v2, p0, LX/FHd;->a:LX/FHe;

    iget-object v13, v2, LX/FHe;->d:LX/FHs;

    new-instance v2, LX/FHq;

    iget-object v3, p0, LX/FHd;->b:LX/FHc;

    iget-object v3, v3, LX/FHc;->c:Ljava/io/File;

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v5, v4, LX/FHc;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v6, v4, LX/FHc;->e:Ljava/lang/String;

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v7, v4, LX/FHc;->f:Ljava/lang/String;

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v9, v4, LX/FHc;->g:LX/FHg;

    iget-object v4, p0, LX/FHd;->b:LX/FHc;

    iget-object v4, v4, LX/FHc;->i:LX/7ys;

    .line 2220814
    iget-object v11, v4, LX/7ys;->d:LX/7yr;

    move-object v11, v11

    .line 2220815
    move-object v4, v1

    invoke-direct/range {v2 .. v12}, LX/FHq;-><init>(Ljava/io/File;Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;ILX/FHg;Ljava/lang/String;LX/7yr;Ljava/lang/String;)V

    iget-object v1, p0, LX/FHd;->b:LX/FHc;

    iget-object v1, v1, LX/FHc;->b:LX/14U;

    invoke-virtual {v0, v13, v2, v1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHr;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2220816
    :try_start_5
    iget-object v1, p0, LX/FHd;->b:LX/FHc;

    iget-object v1, v1, LX/FHc;->h:LX/FHj;

    if-eqz v1, :cond_4

    .line 2220817
    iget-object v1, p0, LX/FHd;->b:LX/FHc;

    iget-object v1, v1, LX/FHc;->h:LX/FHj;

    invoke-virtual {v1}, LX/FHj;->b()V

    :cond_4
    return-object v0

    :catch_0
    move-object v2, v6

    :goto_3
    move-object v0, v6

    move-object v10, v2

    goto/16 :goto_1

    .line 2220818
    :cond_5
    iget-object v2, p0, LX/FHd;->b:LX/FHc;

    iget-object v2, v2, LX/FHc;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    long-to-int v8, v2

    goto/16 :goto_2

    .line 2220819
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/FHd;->b:LX/FHc;

    iget-object v1, v1, LX/FHc;->h:LX/FHj;

    if-eqz v1, :cond_6

    .line 2220820
    iget-object v1, p0, LX/FHd;->b:LX/FHc;

    iget-object v1, v1, LX/FHc;->h:LX/FHj;

    invoke-virtual {v1}, LX/FHj;->b()V

    :cond_6
    throw v0
    :try_end_5
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_5 .. :try_end_5} :catch_1

    .line 2220821
    :catch_1
    move-exception v0

    .line 2220822
    invoke-static {v0}, LX/FHh;->a(Lorg/apache/http/client/HttpResponseException;)V

    .line 2220823
    throw v0

    :catch_2
    goto :goto_3

    :cond_7
    move-object v0, v6

    move-object v2, v6

    goto/16 :goto_0

    :cond_8
    move-object v0, v6

    move-object v10, v6

    goto/16 :goto_1
.end method
