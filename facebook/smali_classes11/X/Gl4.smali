.class public final LX/Gl4;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gkp;

.field public final synthetic b:LX/GkK;

.field public final synthetic c:LX/Gl6;


# direct methods
.method public constructor <init>(LX/Gl6;LX/Gkp;LX/GkK;)V
    .locals 0

    .prologue
    .line 2389784
    iput-object p1, p0, LX/Gl4;->c:LX/Gl6;

    iput-object p2, p0, LX/Gl4;->a:LX/Gkp;

    iput-object p3, p0, LX/Gl4;->b:LX/GkK;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2389785
    sget-object v0, LX/Gl6;->a:Ljava/lang/String;

    const-string v1, "Failed to load groups for section: "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Gl4;->a:LX/Gkp;

    invoke-virtual {v3}, LX/Gkp;->i()LX/Gkn;

    move-result-object v3

    invoke-virtual {v3}, LX/Gkn;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2389786
    iget-object v0, p0, LX/Gl4;->b:LX/GkK;

    iget-object v1, p0, LX/Gl4;->a:LX/Gkp;

    invoke-virtual {v1}, LX/Gkp;->i()LX/Gkn;

    move-result-object v1

    invoke-interface {v0, v1, v4}, LX/GkK;->a(LX/Gkn;Z)V

    .line 2389787
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2389788
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2389789
    iget-object v0, p0, LX/Gl4;->a:LX/Gkp;

    invoke-static {v0, p1}, LX/Gl6;->b(LX/Gkp;Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389790
    iget-object v0, p0, LX/Gl4;->c:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->g:Ljava/util/HashMap;

    iget-object v1, p0, LX/Gl4;->a:LX/Gkp;

    invoke-virtual {v1}, LX/Gkp;->i()LX/Gkn;

    move-result-object v1

    iget-object v2, p0, LX/Gl4;->a:LX/Gkp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2389791
    iget-object v0, p0, LX/Gl4;->c:LX/Gl6;

    invoke-static {v0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389792
    :cond_0
    iget-object v0, p0, LX/Gl4;->b:LX/GkK;

    iget-object v1, p0, LX/Gl4;->a:LX/Gkp;

    invoke-virtual {v1}, LX/Gkp;->i()LX/Gkn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/GkK;->a(LX/Gkn;Z)V

    .line 2389793
    return-void
.end method
