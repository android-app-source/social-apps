.class public final LX/GyA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2409517
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 2409518
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2409519
    :goto_0
    return v1

    .line 2409520
    :cond_0
    const-string v9, "is_nearby_search"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2409521
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    .line 2409522
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 2409523
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2409524
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2409525
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 2409526
    const-string v9, "display_region_hint"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2409527
    invoke-static {p0, p1}, LX/Gy7;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2409528
    :cond_2
    const-string v9, "filters"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2409529
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2409530
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_3

    .line 2409531
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_3

    .line 2409532
    invoke-static {p0, p1}, LX/Gy9;->b(LX/15w;LX/186;)I

    move-result v8

    .line 2409533
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2409534
    :cond_3
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 2409535
    goto :goto_1

    .line 2409536
    :cond_4
    const-string v9, "nodes"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2409537
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2409538
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_5

    .line 2409539
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_5

    .line 2409540
    invoke-static {p0, p1}, LX/GyF;->b(LX/15w;LX/186;)I

    move-result v8

    .line 2409541
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2409542
    :cond_5
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2409543
    goto/16 :goto_1

    .line 2409544
    :cond_6
    const-string v9, "page_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2409545
    invoke-static {p0, p1}, LX/5Mh;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2409546
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2409547
    :cond_8
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2409548
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2409549
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2409550
    if-eqz v0, :cond_9

    .line 2409551
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 2409552
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2409553
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2409554
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2409555
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2409556
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2409557
    if-eqz v0, :cond_0

    .line 2409558
    const-string v1, "display_region_hint"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2409559
    invoke-static {p0, v0, p2}, LX/Gy7;->a(LX/15i;ILX/0nX;)V

    .line 2409560
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2409561
    if-eqz v0, :cond_2

    .line 2409562
    const-string v1, "filters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2409563
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2409564
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2409565
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/Gy9;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2409566
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2409567
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2409568
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2409569
    if-eqz v0, :cond_3

    .line 2409570
    const-string v1, "is_nearby_search"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2409571
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2409572
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2409573
    if-eqz v0, :cond_5

    .line 2409574
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2409575
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2409576
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 2409577
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/GyF;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2409578
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2409579
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2409580
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2409581
    if-eqz v0, :cond_6

    .line 2409582
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2409583
    invoke-static {p0, v0, p2}, LX/5Mh;->a(LX/15i;ILX/0nX;)V

    .line 2409584
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2409585
    return-void
.end method
