.class public final LX/G6E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/wem/watermark/WatermarkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V
    .locals 0

    .prologue
    .line 2319830
    iput-object p1, p0, LX/G6E;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2319819
    if-eqz p1, :cond_0

    .line 2319820
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2319821
    if-eqz v0, :cond_0

    .line 2319822
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2319823
    check-cast v0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel;

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel;->a()Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel;->a()LX/0Px;

    move-result-object v0

    .line 2319824
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2319825
    :cond_0
    :goto_0
    return-void

    .line 2319826
    :cond_1
    iget-object v1, p0, LX/G6E;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v1, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->w:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2319827
    iget-object v1, p0, LX/G6E;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v1, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->w:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2319828
    iget-object v0, p0, LX/G6E;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->A:LX/G6J;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2319829
    iget-object v0, p0, LX/G6E;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/wem/watermark/WatermarkActivity;->b(Lcom/facebook/wem/watermark/WatermarkActivity;I)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2319832
    iget-object v0, p0, LX/G6E;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/G6Q;->c(Ljava/lang/String;)V

    .line 2319833
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2319831
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/G6E;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
