.class public LX/G4r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/G4r;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318032
    return-void
.end method

.method public static a(LX/0QB;)LX/G4r;
    .locals 3

    .prologue
    .line 2318033
    sget-object v0, LX/G4r;->a:LX/G4r;

    if-nez v0, :cond_1

    .line 2318034
    const-class v1, LX/G4r;

    monitor-enter v1

    .line 2318035
    :try_start_0
    sget-object v0, LX/G4r;->a:LX/G4r;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2318036
    if-eqz v2, :cond_0

    .line 2318037
    :try_start_1
    new-instance v0, LX/G4r;

    invoke-direct {v0}, LX/G4r;-><init>()V

    .line 2318038
    move-object v0, v0

    .line 2318039
    sput-object v0, LX/G4r;->a:LX/G4r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2318040
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2318041
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2318042
    :cond_1
    sget-object v0, LX/G4r;->a:LX/G4r;

    return-object v0

    .line 2318043
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2318044
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2318045
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318046
    invoke-static {v0}, LX/G4r;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2318047
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2318048
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 2318049
    invoke-static {p0}, Lcom/facebook/graphql/model/OrganicImpression;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/OrganicImpression;

    move-result-object v0

    .line 2318050
    if-nez v0, :cond_0

    .line 2318051
    :goto_0
    return-void

    .line 2318052
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/OrganicImpression;->l()V

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)V
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLTimelineSection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2318053
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2318054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    .line 2318055
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 2318056
    instance-of v4, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_0

    .line 2318057
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/G4r;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2318058
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2318059
    :cond_1
    return-void
.end method
