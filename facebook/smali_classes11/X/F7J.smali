.class public final LX/F7J;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V
    .locals 0

    .prologue
    .line 2201410
    iput-object p1, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2201396
    iget-object v0, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->d:LX/F9r;

    invoke-virtual {v0}, LX/F9r;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2201397
    check-cast p1, Ljava/util/List;

    .line 2201398
    iget-object v0, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2201399
    :cond_0
    :goto_0
    return-void

    .line 2201400
    :cond_1
    iget-object v0, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    const/4 v1, 0x1

    .line 2201401
    iput-boolean v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->N:Z

    .line 2201402
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    iget-wide v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    sub-long v2, v0, v2

    .line 2201403
    iget-object v0, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iget-object v1, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    const/16 v5, 0x32

    const/16 v6, 0xa

    iget-object v7, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    iget-object v7, v7, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    .line 2201404
    iget-object v8, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v9, LX/9Tj;->PHONEBOOK_READ:LX/9Tj;

    invoke-virtual {v9}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "ci_flow"

    invoke-virtual {v9, v10, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "time"

    invoke-virtual {v9, v10, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "phonebook_size"

    invoke-virtual {v9, v10, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "batch_size"

    invoke-virtual {v9, v10, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "pagination_size"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "session_id"

    invoke-virtual {v9, v10, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-interface {v8, v9}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201405
    iget-object v0, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    .line 2201406
    iput-object p1, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->E:Ljava/util/List;

    .line 2201407
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2201408
    iget-object v2, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->P:Ljava/util/Map;

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2201409
    :cond_2
    iget-object v0, p0, LX/F7J;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->e(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;I)V

    goto/16 :goto_0
.end method
