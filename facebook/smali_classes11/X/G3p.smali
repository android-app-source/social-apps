.class public LX/G3p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/G3p;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2316241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2316242
    return-void
.end method

.method public static a(LX/0QB;)LX/G3p;
    .locals 3

    .prologue
    .line 2316243
    sget-object v0, LX/G3p;->a:LX/G3p;

    if-nez v0, :cond_1

    .line 2316244
    const-class v1, LX/G3p;

    monitor-enter v1

    .line 2316245
    :try_start_0
    sget-object v0, LX/G3p;->a:LX/G3p;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2316246
    if-eqz v2, :cond_0

    .line 2316247
    :try_start_1
    new-instance v0, LX/G3p;

    invoke-direct {v0}, LX/G3p;-><init>()V

    .line 2316248
    move-object v0, v0

    .line 2316249
    sput-object v0, LX/G3p;->a:LX/G3p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2316250
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2316251
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2316252
    :cond_1
    sget-object v0, LX/G3p;->a:LX/G3p;

    return-object v0

    .line 2316253
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2316254
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
