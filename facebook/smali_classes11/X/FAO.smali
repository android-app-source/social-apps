.class public final synthetic LX/FAO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2206953
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/FAO;->b:[I

    :try_start_0
    sget-object v0, LX/FAO;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->CHECKOUT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, LX/FAO;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    .line 2206954
    :goto_1
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/FAO;->a:[I

    :try_start_2
    sget-object v0, LX/FAO;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, LX/FAO;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, LX/FAO;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_0
.end method
