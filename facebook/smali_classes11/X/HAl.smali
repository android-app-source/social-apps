.class public final LX/HAl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;)V
    .locals 0

    .prologue
    .line 2435891
    iput-object p1, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2435892
    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    move v1, v0

    .line 2435893
    :goto_0
    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->A:I

    if-eq v0, v1, :cond_1

    .line 2435894
    :cond_0
    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->s:Landroid/view/View;

    iget-object v3, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    sget-object v4, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->y:[I

    iget-object v5, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->h:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->d()I

    move-result v5

    invoke-static {v0, v3, p2, v4, v5}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2435895
    iget-object v4, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2435896
    iput-boolean v0, v4, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->z:Z

    .line 2435897
    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->z:Z

    if-eqz v0, :cond_1

    .line 2435898
    iget-object v4, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->E_(I)V

    .line 2435899
    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    .line 2435900
    iput v1, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->A:I

    .line 2435901
    :cond_1
    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2435902
    iget-object v0, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2435903
    iget-object v1, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->t:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget v1, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->v:I

    if-eq v1, v0, :cond_2

    iget-object v1, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2435904
    iget-object v1, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->t:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v1, v2, p2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2435905
    iget-object v1, p0, LX/HAl;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    .line 2435906
    iput v0, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->v:I

    .line 2435907
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 2435908
    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2435909
    return-void
.end method
