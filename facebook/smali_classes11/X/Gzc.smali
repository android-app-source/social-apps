.class public final enum LX/Gzc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gzc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gzc;

.field public static final enum CREATE:LX/Gzc;

.field public static final enum EDIT:LX/Gzc;


# instance fields
.field private final logValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2412162
    new-instance v0, LX/Gzc;

    const-string v1, "CREATE"

    const-string v2, "create"

    invoke-direct {v0, v1, v3, v2}, LX/Gzc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gzc;->CREATE:LX/Gzc;

    .line 2412163
    new-instance v0, LX/Gzc;

    const-string v1, "EDIT"

    const-string v2, "edit"

    invoke-direct {v0, v1, v4, v2}, LX/Gzc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gzc;->EDIT:LX/Gzc;

    .line 2412164
    const/4 v0, 0x2

    new-array v0, v0, [LX/Gzc;

    sget-object v1, LX/Gzc;->CREATE:LX/Gzc;

    aput-object v1, v0, v3

    sget-object v1, LX/Gzc;->EDIT:LX/Gzc;

    aput-object v1, v0, v4

    sput-object v0, LX/Gzc;->$VALUES:[LX/Gzc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2412165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2412166
    iput-object p3, p0, LX/Gzc;->logValue:Ljava/lang/String;

    .line 2412167
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gzc;
    .locals 1

    .prologue
    .line 2412168
    const-class v0, LX/Gzc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gzc;

    return-object v0
.end method

.method public static values()[LX/Gzc;
    .locals 1

    .prologue
    .line 2412169
    sget-object v0, LX/Gzc;->$VALUES:[LX/Gzc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gzc;

    return-object v0
.end method


# virtual methods
.method public final getLogValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2412170
    iget-object v0, p0, LX/Gzc;->logValue:Ljava/lang/String;

    return-object v0
.end method
