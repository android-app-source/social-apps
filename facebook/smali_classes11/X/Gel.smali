.class public final LX/Gel;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Gel;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gej;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Gem;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2376033
    const/4 v0, 0x0

    sput-object v0, LX/Gel;->a:LX/Gel;

    .line 2376034
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Gel;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2376030
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2376031
    new-instance v0, LX/Gem;

    invoke-direct {v0}, LX/Gem;-><init>()V

    iput-object v0, p0, LX/Gel;->c:LX/Gem;

    .line 2376032
    return-void
.end method

.method public static declared-synchronized q()LX/Gel;
    .locals 2

    .prologue
    .line 2376026
    const-class v1, LX/Gel;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Gel;->a:LX/Gel;

    if-nez v0, :cond_0

    .line 2376027
    new-instance v0, LX/Gel;

    invoke-direct {v0}, LX/Gel;-><init>()V

    sput-object v0, LX/Gel;->a:LX/Gel;

    .line 2376028
    :cond_0
    sget-object v0, LX/Gel;->a:LX/Gel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2376029
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2376020
    check-cast p2, LX/Gek;

    .line 2376021
    iget-object v0, p2, LX/Gek;->a:Lcom/facebook/java2js/JSValue;

    iget-object v1, p2, LX/Gek;->b:LX/5KI;

    const/4 p2, 0x3

    const/4 v4, 0x2

    const/4 p0, 0x0

    const/4 v7, 0x1

    .line 2376022
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b010d

    invoke-interface {v2, p2, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020a3c

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    invoke-interface {v2, v3, v4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    const-string v3, "imageComponent"

    invoke-virtual {v0, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0a0438

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    const v6, 0x7f0b0917

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b107f

    invoke-interface {v4, v7, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b1080

    invoke-interface {v4, p2, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v0, v5}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v5

    invoke-static {p1, v5}, LX/5Kv;->b(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const-string v5, "body"

    invoke-virtual {v0, v5}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v5

    invoke-static {p1, v5}, LX/5Kv;->b(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b1081

    invoke-virtual {v5, v6}, LX/1ne;->s(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const-string v5, "meta"

    invoke-virtual {v0, v5}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v5

    invoke-static {p1, v5}, LX/5Kv;->b(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a043b

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1082

    invoke-interface {v5, v7, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v5, 0x7f0a015a

    invoke-virtual {v4, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0033

    invoke-interface {v4, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v4, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    const v6, 0x7f0b0917

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const-string v4, "rightComponent"

    invoke-virtual {v0, v4}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2376023
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2376024
    invoke-static {}, LX/1dS;->b()V

    .line 2376025
    const/4 v0, 0x0

    return-object v0
.end method
