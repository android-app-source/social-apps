.class public LX/Flz;
.super LX/FlQ;
.source ""


# instance fields
.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2281891
    invoke-direct {p0}, LX/FlQ;-><init>()V

    .line 2281892
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Flz;->c:Z

    .line 2281893
    iput-object p1, p0, LX/Flz;->b:Landroid/content/Context;

    .line 2281894
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2281876
    if-nez p2, :cond_0

    .line 2281877
    new-instance p2, LX/Fm0;

    iget-object v0, p0, LX/Flz;->b:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Fm0;-><init>(Landroid/content/Context;)V

    .line 2281878
    :goto_0
    iget-object v0, p0, LX/FlQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Flv;

    iget-boolean v1, p0, LX/Flz;->c:Z

    .line 2281879
    invoke-virtual {v0}, LX/Flv;->d()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2281880
    invoke-virtual {p2}, LX/Fm0;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/Flv;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2281881
    invoke-virtual {v0}, LX/Flv;->e()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2281882
    iget-boolean p0, v0, LX/Flv;->b:Z

    move p0, p0

    .line 2281883
    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2281884
    if-eqz v1, :cond_1

    .line 2281885
    iget-boolean p0, v0, LX/Flv;->b:Z

    move p0, p0

    .line 2281886
    if-nez p0, :cond_1

    .line 2281887
    const/high16 p0, 0x3f000000    # 0.5f

    invoke-virtual {p2, p0}, LX/Fm0;->setAlpha(F)V

    .line 2281888
    :goto_1
    return-object p2

    .line 2281889
    :cond_0
    check-cast p2, LX/Fm0;

    goto :goto_0

    .line 2281890
    :cond_1
    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {p2, p0}, LX/Fm0;->setAlpha(F)V

    goto :goto_1
.end method
