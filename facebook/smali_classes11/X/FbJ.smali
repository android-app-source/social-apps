.class public LX/FbJ;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2259891
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2259892
    return-void
.end method

.method public static a(LX/0QB;)LX/FbJ;
    .locals 3

    .prologue
    .line 2259950
    const-class v1, LX/FbJ;

    monitor-enter v1

    .line 2259951
    :try_start_0
    sget-object v0, LX/FbJ;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2259952
    sput-object v2, LX/FbJ;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2259953
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2259954
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2259955
    new-instance v0, LX/FbJ;

    invoke-direct {v0}, LX/FbJ;-><init>()V

    .line 2259956
    move-object v0, v0

    .line 2259957
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2259958
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FbJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2259959
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2259960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLDate;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .locals 7
    .param p0    # Lcom/facebook/graphql/model/GraphQLDate;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259926
    if-nez p0, :cond_0

    .line 2259927
    const/4 v0, 0x0

    .line 2259928
    :goto_0
    return-object v0

    :cond_0
    const/4 v3, 0x0

    .line 2259929
    if-nez p0, :cond_2

    .line 2259930
    :cond_1
    :goto_1
    move-object v0, v3

    .line 2259931
    invoke-static {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    goto :goto_0

    .line 2259932
    :cond_2
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 2259933
    const/4 v2, 0x0

    .line 2259934
    if-nez p0, :cond_4

    .line 2259935
    :goto_2
    move v2, v2

    .line 2259936
    if-eqz v2, :cond_1

    .line 2259937
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 2259938
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 2259939
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2259940
    new-instance v1, LX/15i;

    const/4 v5, 0x1

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2259941
    instance-of v2, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v2, :cond_3

    .line 2259942
    const-string v2, "SearchModelConversionHelper.getDateFields"

    invoke-virtual {v1, v2, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2259943
    :cond_3
    new-instance v3, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    invoke-direct {v3, v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;-><init>(LX/15i;)V

    goto :goto_1

    .line 2259944
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, LX/186;->c(I)V

    .line 2259945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDate;->a()I

    move-result v4

    invoke-virtual {v1, v2, v4, v2}, LX/186;->a(III)V

    .line 2259946
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDate;->j()I

    move-result v5

    invoke-virtual {v1, v4, v5, v2}, LX/186;->a(III)V

    .line 2259947
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDate;->k()I

    move-result v5

    invoke-virtual {v1, v4, v5, v2}, LX/186;->a(III)V

    .line 2259948
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 2259949
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259893
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2259894
    if-nez v0, :cond_0

    .line 2259895
    const/4 v0, 0x0

    .line 2259896
    :goto_0
    return-object v0

    .line 2259897
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2259898
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2259899
    move-object v1, v1

    .line 2259900
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x6436c331

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2259901
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2259902
    move-object v1, v1

    .line 2259903
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    .line 2259904
    iput-object v2, v1, LX/8dX;->ad:Ljava/lang/String;

    .line 2259905
    move-object v1, v1

    .line 2259906
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iq()Ljava/lang/String;

    move-result-object v2

    .line 2259907
    iput-object v2, v1, LX/8dX;->aE:Ljava/lang/String;

    .line 2259908
    move-object v1, v1

    .line 2259909
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lt()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v2

    invoke-static {v2}, LX/FbJ;->a(Lcom/facebook/graphql/model/GraphQLDate;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v2

    .line 2259910
    iput-object v2, v1, LX/8dX;->i:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 2259911
    move-object v1, v1

    .line 2259912
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lu()Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;

    move-result-object v0

    .line 2259913
    if-nez v0, :cond_1

    .line 2259914
    const/4 v2, 0x0

    .line 2259915
    :goto_1
    move-object v0, v2

    .line 2259916
    iput-object v0, v1, LX/8dX;->n:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    .line 2259917
    move-object v0, v1

    .line 2259918
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2259919
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2259920
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2259921
    move-object v0, v1

    .line 2259922
    goto :goto_0

    :cond_1
    new-instance v2, LX/A27;

    invoke-direct {v2}, LX/A27;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->a()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v3

    invoke-static {v3}, LX/FbJ;->a(Lcom/facebook/graphql/model/GraphQLDate;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v3

    .line 2259923
    iput-object v3, v2, LX/A27;->a:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 2259924
    move-object v2, v2

    .line 2259925
    invoke-virtual {v2}, LX/A27;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v2

    goto :goto_1
.end method
