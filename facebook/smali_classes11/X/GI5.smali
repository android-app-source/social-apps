.class public final LX/GI5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GCE;

.field public final synthetic b:LX/GIA;


# direct methods
.method public constructor <init>(LX/GIA;LX/GCE;)V
    .locals 0

    .prologue
    .line 2335715
    iput-object p1, p0, LX/GI5;->b:LX/GIA;

    iput-object p2, p0, LX/GI5;->a:LX/GCE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2335716
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2335717
    const-string v0, "payments_flow_context_key"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2335718
    iget-object v1, p0, LX/GI5;->a:LX/GCE;

    .line 2335719
    iput-object v0, v1, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2335720
    iget-object v0, p0, LX/GI5;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const-string v1, "campaign_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2335721
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->p:Ljava/lang/String;

    .line 2335722
    iget-object v0, p0, LX/GI5;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const-string v1, "checkout_payment_ids"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2335723
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->q:[Ljava/lang/String;

    .line 2335724
    iget-object v0, p0, LX/GI5;->b:LX/GIA;

    iget-object v1, v0, LX/GIA;->h:LX/GDg;

    iget-object v0, p0, LX/GI5;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GI5;->b:LX/GIA;

    iget-object v2, v2, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/GDg;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    .line 2335725
    :cond_0
    return-void
.end method
