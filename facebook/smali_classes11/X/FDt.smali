.class public LX/FDt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/FDt;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/FDs;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cq;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/6fD;

.field private final f:LX/0ad;

.field private final g:LX/01T;

.field private final h:LX/2N4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2214974
    const-class v0, LX/FDt;

    sput-object v0, LX/FDt;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/FDs;LX/0Or;LX/6fD;LX/0ad;LX/01T;LX/2N4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/FDs;",
            "LX/0Or",
            "<",
            "LX/6cq;",
            ">;",
            "LX/6fD;",
            "LX/0ad;",
            "LX/01T;",
            "LX/2N4;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2215075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2215076
    iput-object p1, p0, LX/FDt;->b:LX/0Or;

    .line 2215077
    iput-object p2, p0, LX/FDt;->c:LX/FDs;

    .line 2215078
    iput-object p3, p0, LX/FDt;->d:LX/0Or;

    .line 2215079
    iput-object p4, p0, LX/FDt;->e:LX/6fD;

    .line 2215080
    iput-object p5, p0, LX/FDt;->f:LX/0ad;

    .line 2215081
    iput-object p6, p0, LX/FDt;->g:LX/01T;

    .line 2215082
    iput-object p7, p0, LX/FDt;->h:LX/2N4;

    .line 2215083
    return-void
.end method

.method public static a(LX/0QB;)LX/FDt;
    .locals 11

    .prologue
    .line 2215062
    sget-object v0, LX/FDt;->i:LX/FDt;

    if-nez v0, :cond_1

    .line 2215063
    const-class v1, LX/FDt;

    monitor-enter v1

    .line 2215064
    :try_start_0
    sget-object v0, LX/FDt;->i:LX/FDt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2215065
    if-eqz v2, :cond_0

    .line 2215066
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2215067
    new-instance v3, LX/FDt;

    const/16 v4, 0x274b

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v5

    check-cast v5, LX/FDs;

    const/16 v6, 0x273f

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/6fD;->a(LX/0QB;)LX/6fD;

    move-result-object v7

    check-cast v7, LX/6fD;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v9

    check-cast v9, LX/01T;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v10

    check-cast v10, LX/2N4;

    invoke-direct/range {v3 .. v10}, LX/FDt;-><init>(LX/0Or;LX/FDs;LX/0Or;LX/6fD;LX/0ad;LX/01T;LX/2N4;)V

    .line 2215068
    move-object v0, v3

    .line 2215069
    sput-object v0, LX/FDt;->i:LX/FDt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2215070
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2215071
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2215072
    :cond_1
    sget-object v0, LX/FDt;->i:LX/FDt;

    return-object v0

    .line 2215073
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2215074
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/FDt;LX/0ux;Lcom/facebook/messaging/model/send/SendError;)V
    .locals 9

    .prologue
    .line 2215042
    const-string v0, "DbSendHandler.updateMessageDatabase"

    const v1, 0x20dde859

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2215043
    :try_start_0
    iget-object v0, p0, LX/FDt;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2215044
    const v0, 0x74f9714c

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2215045
    :try_start_1
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2215046
    const-string v6, "msg_type"

    sget-object v7, LX/2uW;->FAILED_SEND:LX/2uW;

    iget v7, v7, LX/2uW;->dbKeyValue:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2215047
    const-string v6, "send_error"

    iget-object v7, p2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-object v7, v7, LX/6fP;->serializedString:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2215048
    const-string v6, "send_error_message"

    iget-object v7, p2, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2215049
    const-string v6, "send_error_timestamp_ms"

    iget-wide v7, p2, Lcom/facebook/messaging/model/send/SendError;->e:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2215050
    const-string v6, "send_error_error_url"

    iget-object v7, p2, Lcom/facebook/messaging/model/send/SendError;->f:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2215051
    move-object v0, v5

    .line 2215052
    const-string v2, "messages"

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2215053
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2215054
    const v0, -0xfe97f13

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2215055
    const v0, 0xc7d0302

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2215056
    return-void

    .line 2215057
    :catch_0
    move-exception v0

    .line 2215058
    :try_start_3
    sget-object v2, LX/FDt;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2215059
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2215060
    :catchall_0
    move-exception v0

    const v2, -0x8775db4

    :try_start_4
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2215061
    :catchall_1
    move-exception v0

    const v1, 0x655f21b7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(LX/FDt;Lcom/facebook/messaging/model/messages/Message;Z)V
    .locals 4

    .prologue
    .line 2215029
    const-string v0, "DbSendHandler.handleInsertPendingSentMessage"

    const v1, -0x2b5dc9c4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2215030
    :try_start_0
    iget-object v0, p0, LX/FDt;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2215031
    const v0, -0x647a4dc9

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2215032
    :try_start_1
    invoke-virtual {p0}, LX/FDt;->a()V

    .line 2215033
    iget-object v0, p0, LX/FDt;->c:LX/FDs;

    invoke-virtual {v0, p1}, LX/FDs;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2215034
    if-eqz p2, :cond_0

    .line 2215035
    iget-object v0, p0, LX/FDt;->c:LX/FDs;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/MessageDraft;)V

    .line 2215036
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2215037
    const v0, 0x3820e261

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2215038
    const v0, -0x52fa425d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2215039
    return-void

    .line 2215040
    :catchall_0
    move-exception v0

    const v2, -0x6f7cb5dc

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2215041
    :catchall_1
    move-exception v0

    const v1, 0x298efe3f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(Lcom/facebook/messaging/model/send/SendError;Lcom/facebook/messaging/model/send/PendingSendQueueKey;)V
    .locals 3
    .param p2    # Lcom/facebook/messaging/model/send/PendingSendQueueKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2215084
    const-string v0, "DbSendHandler.changePendingSendsToFailedSends(SendError, PendingSendQueueKey)"

    const v1, 0x1b8bf065

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2215085
    :try_start_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 2215086
    const-string v1, "msg_type"

    sget-object v2, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v2, v2, LX/2uW;->dbKeyValue:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2215087
    if-eqz p2, :cond_0

    .line 2215088
    const-string v1, "thread_key"

    iget-object v2, p2, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2215089
    const-string v1, "send_queue_type"

    iget-object v2, p2, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    iget-object v2, v2, LX/6fM;->serializedValue:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2215090
    :cond_0
    invoke-static {p0, v0, p1}, LX/FDt;->a(LX/FDt;LX/0ux;Lcom/facebook/messaging/model/send/SendError;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2215091
    const v0, 0x7a6b012

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2215092
    return-void

    .line 2215093
    :catchall_0
    move-exception v0

    const v1, -0x22ce9f51

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 10

    .prologue
    .line 2215012
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2215013
    iget-object v1, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v1, v1

    .line 2215014
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 2215015
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215016
    iget-object v3, p0, LX/FDt;->e:LX/6fD;

    iget-object v4, p0, LX/FDt;->h:LX/2N4;

    const/4 v5, 0x5

    invoke-virtual {v4, v2, v5}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v3, v1, v2}, LX/6fD;->c(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z

    move-result v1

    .line 2215017
    if-nez v1, :cond_0

    .line 2215018
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    const/4 v1, 0x1

    .line 2215019
    iput-boolean v1, v0, LX/6f7;->o:Z

    .line 2215020
    move-object v0, v0

    .line 2215021
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2215022
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2215023
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v0

    .line 2215024
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v4, v0

    .line 2215025
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v5, v0

    .line 2215026
    iget-wide v8, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v8

    .line 2215027
    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    move-object p1, v1

    .line 2215028
    :cond_0
    iget-object v0, p0, LX/FDt;->c:LX/FDs;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, p1, v2, v3}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2215001
    iget-object v0, p0, LX/FDt;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cq;

    .line 2215002
    iget-boolean v2, v0, LX/6cq;->e:Z

    move v0, v2

    .line 2215003
    if-eqz v0, :cond_0

    .line 2215004
    :goto_0
    return-void

    .line 2215005
    :cond_0
    iget-object v0, p0, LX/FDt;->g:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-eq v0, v2, :cond_2

    move v0, v1

    .line 2215006
    :goto_1
    if-nez v0, :cond_1

    .line 2215007
    sget-object v0, LX/6fP;->PENDING_SEND_ON_STARTUP:LX/6fP;

    invoke-static {v0}, Lcom/facebook/messaging/model/send/SendError;->a(LX/6fP;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, LX/FDt;->a(Lcom/facebook/messaging/model/send/SendError;Lcom/facebook/messaging/model/send/PendingSendQueueKey;)V

    .line 2215008
    :cond_1
    iget-object v0, p0, LX/FDt;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cq;

    .line 2215009
    iput-boolean v1, v0, LX/6cq;->e:Z

    .line 2215010
    goto :goto_0

    .line 2215011
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/send/SendError;J)V
    .locals 4

    .prologue
    .line 2214992
    const-string v0, "DbSendHandler.changePendingSendsToFailedSends(SendError, long)"

    const v1, -0x22e9aad6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214993
    :try_start_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 2214994
    const-string v1, "msg_type"

    sget-object v2, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v2, v2, LX/2uW;->dbKeyValue:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2214995
    const-wide/16 v2, -0x1

    cmp-long v1, p2, v2

    if-eqz v1, :cond_0

    .line 2214996
    const-string v1, "timestamp_ms"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->b(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2214997
    :cond_0
    invoke-static {p0, v0, p1}, LX/FDt;->a(LX/FDt;LX/0ux;Lcom/facebook/messaging/model/send/SendError;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214998
    const v0, 0x32820133

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214999
    return-void

    .line 2215000
    :catchall_0
    move-exception v0

    const v1, 0x4cf75106

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;)V
    .locals 3

    .prologue
    .line 2214985
    const-string v0, "DbSendHandler.updateFailedMessageSendError"

    const v1, -0x6da890a5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214986
    :try_start_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 2214987
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2214988
    iget-object v1, p1, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->a:Lcom/facebook/messaging/model/send/SendError;

    invoke-static {p0, v0, v1}, LX/FDt;->a(LX/FDt;LX/0ux;Lcom/facebook/messaging/model/send/SendError;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214989
    const v0, 0x1988cf5f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214990
    return-void

    .line 2214991
    :catchall_0
    move-exception v0

    const v1, 0x5a3d885a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 5

    .prologue
    .line 2214975
    invoke-static {}, Lcom/facebook/messaging/model/send/SendError;->newBuilder()LX/6fO;

    move-result-object v0

    sget-object v1, LX/6fP;->EARLIER_MESSAGE_FROM_THREAD_FAILED:LX/6fP;

    .line 2214976
    iput-object v1, v0, LX/6fO;->a:LX/6fP;

    .line 2214977
    move-object v0, v0

    .line 2214978
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-wide v2, v1, Lcom/facebook/messaging/model/send/SendError;->e:J

    .line 2214979
    iput-wide v2, v0, LX/6fO;->c:J

    .line 2214980
    move-object v0, v0

    .line 2214981
    invoke-virtual {v0}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    .line 2214982
    iget-object v1, p0, LX/FDt;->c:LX/FDs;

    invoke-virtual {v1, p1}, LX/FDs;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2214983
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-direct {p0, v0, v1}, LX/FDt;->a(Lcom/facebook/messaging/model/send/SendError;Lcom/facebook/messaging/model/send/PendingSendQueueKey;)V

    .line 2214984
    return-void
.end method
