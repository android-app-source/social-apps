.class public LX/GSw;
.super LX/2EJ;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/0tX;

.field private final d:LX/1Ck;

.field public final e:LX/GRQ;

.field public f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public g:Landroid/widget/ListView;

.field public final h:I

.field public i:LX/GRS;

.field public j:LX/GRP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2354926
    const-class v0, LX/GSw;

    sput-object v0, LX/GSw;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/1Ck;LX/GRQ;Landroid/content/Context;Ljava/lang/Integer;)V
    .locals 9
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2354927
    invoke-direct {p0, p4}, LX/2EJ;-><init>(Landroid/content/Context;)V

    .line 2354928
    iput-object p1, p0, LX/GSw;->c:LX/0tX;

    .line 2354929
    iput-object p2, p0, LX/GSw;->d:LX/1Ck;

    .line 2354930
    iput-object p3, p0, LX/GSw;->e:LX/GRQ;

    .line 2354931
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/GSw;->h:I

    .line 2354932
    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v3, 0x0

    .line 2354933
    invoke-virtual {p0}, LX/GSw;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300ef

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2354934
    invoke-virtual {p0}, LX/GSw;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0060

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v6, v1

    move-object v1, p0

    move v4, v3

    move v5, v3

    .line 2354935
    invoke-virtual/range {v1 .. v6}, LX/2EJ;->a(Landroid/view/View;IIII)V

    .line 2354936
    const v1, 0x7f0d055b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2354937
    iget v3, p0, LX/GSw;->h:I

    if-ne v3, v7, :cond_0

    .line 2354938
    const v3, 0x7f08378f

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2354939
    :goto_0
    const v1, 0x7f0d055c

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v1, p0, LX/GSw;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2354940
    new-instance v1, LX/GRS;

    invoke-direct {v1}, LX/GRS;-><init>()V

    iput-object v1, p0, LX/GSw;->i:LX/GRS;

    .line 2354941
    iget-object v1, p0, LX/GSw;->e:LX/GRQ;

    iget v3, p0, LX/GSw;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LX/GSw;->i:LX/GRS;

    .line 2354942
    new-instance v6, LX/GRP;

    invoke-static {v1}, LX/GRa;->a(LX/0QB;)LX/GRa;

    move-result-object v5

    check-cast v5, LX/GRa;

    invoke-direct {v6, v5, v3, v4}, LX/GRP;-><init>(LX/GRa;Ljava/lang/Integer;LX/GRS;)V

    .line 2354943
    move-object v1, v6

    .line 2354944
    iput-object v1, p0, LX/GSw;->j:LX/GRP;

    .line 2354945
    const v1, 0x7f0d055d

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/BetterListView;

    iput-object v1, p0, LX/GSw;->g:Landroid/widget/ListView;

    .line 2354946
    iget-object v1, p0, LX/GSw;->g:Landroid/widget/ListView;

    iget-object v3, p0, LX/GSw;->j:LX/GRP;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2354947
    iget-object v1, p0, LX/GSw;->g:Landroid/widget/ListView;

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->setVisibility(I)V

    .line 2354948
    const v1, 0x7f0d055e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2354949
    const v3, 0x7f0d055f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2354950
    const v4, 0x7f0d0560

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2354951
    iget v4, p0, LX/GSw;->h:I

    if-ne v4, v7, :cond_1

    .line 2354952
    const v4, 0x7f083795

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2354953
    const v3, 0x7f083796

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2354954
    :goto_1
    iget-object v2, p0, LX/GSw;->g:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 2354955
    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2354956
    invoke-static {p0}, LX/GSw;->d(LX/GSw;)V

    .line 2354957
    iget-object v1, p0, LX/GSw;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2354958
    return-void

    .line 2354959
    :cond_0
    const v3, 0x7f083790

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 2354960
    :cond_1
    const v4, 0x7f083793

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2354961
    const v3, 0x7f083794

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public static d(LX/GSw;)V
    .locals 5

    .prologue
    .line 2354962
    iget-object v1, p0, LX/GSw;->d:LX/1Ck;

    iget v0, p0, LX/GSw;->h:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    sget-object v0, LX/GSv;->FETCH_BLOCKED_USERS:LX/GSv;

    .line 2354963
    :goto_0
    iget v2, p0, LX/GSw;->h:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2354964
    new-instance v2, LX/GSM;

    invoke-direct {v2}, LX/GSM;-><init>()V

    move-object v2, v2

    .line 2354965
    const-string v3, "settings_blocked_size"

    const v4, 0x7f0b238c

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/GSM;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2354966
    :goto_1
    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 2354967
    iget-object v3, p0, LX/GSw;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v2, v2

    .line 2354968
    new-instance v3, LX/GSu;

    invoke-direct {v3, p0}, LX/GSu;-><init>(LX/GSw;)V

    move-object v3, v3

    .line 2354969
    invoke-virtual {v1, v0, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2354970
    return-void

    .line 2354971
    :cond_0
    sget-object v0, LX/GSv;->FETCH_BLOCKED_APPS:LX/GSv;

    goto :goto_0

    .line 2354972
    :cond_1
    new-instance v2, LX/GSN;

    invoke-direct {v2}, LX/GSN;-><init>()V

    move-object v2, v2

    .line 2354973
    const-string v3, "settings_blocked_size"

    const v4, 0x7f0b238c

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/GSN;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final dismiss()V
    .locals 1

    .prologue
    .line 2354974
    iget-object v0, p0, LX/GSw;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2354975
    invoke-super {p0}, LX/2EJ;->dismiss()V

    .line 2354976
    return-void
.end method
