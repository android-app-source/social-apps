.class public LX/H21;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/H21;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2416105
    const-class v0, LX/H21;

    sput-object v0, LX/H21;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2416106
    const-string v0, "nearby_tiles"

    const/4 v1, 0x2

    new-instance v2, LX/H1y;

    invoke-direct {v2}, LX/H1y;-><init>()V

    new-instance v3, LX/H20;

    invoke-direct {v3}, LX/H20;-><init>()V

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 2416107
    return-void
.end method

.method public static a(LX/0QB;)LX/H21;
    .locals 3

    .prologue
    .line 2416108
    sget-object v0, LX/H21;->b:LX/H21;

    if-nez v0, :cond_1

    .line 2416109
    const-class v1, LX/H21;

    monitor-enter v1

    .line 2416110
    :try_start_0
    sget-object v0, LX/H21;->b:LX/H21;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2416111
    if-eqz v2, :cond_0

    .line 2416112
    :try_start_1
    new-instance v0, LX/H21;

    invoke-direct {v0}, LX/H21;-><init>()V

    .line 2416113
    move-object v0, v0

    .line 2416114
    sput-object v0, LX/H21;->b:LX/H21;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2416115
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2416116
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2416117
    :cond_1
    sget-object v0, LX/H21;->b:LX/H21;

    return-object v0

    .line 2416118
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2416119
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
