.class public LX/FMI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static volatile f:LX/FMI;


# instance fields
.field public c:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/3MF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2229517
    sget-object v0, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/FMI;->a:Landroid/net/Uri;

    .line 2229518
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "recipient_ids"

    aput-object v2, v0, v1

    sput-object v0, LX/FMI;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2229514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2229515
    const/4 v0, 0x0

    iput-object v0, p0, LX/FMI;->c:LX/0tf;

    .line 2229516
    return-void
.end method

.method public static a(LX/0QB;)LX/FMI;
    .locals 5

    .prologue
    .line 2229519
    sget-object v0, LX/FMI;->f:LX/FMI;

    if-nez v0, :cond_1

    .line 2229520
    const-class v1, LX/FMI;

    monitor-enter v1

    .line 2229521
    :try_start_0
    sget-object v0, LX/FMI;->f:LX/FMI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2229522
    if-eqz v2, :cond_0

    .line 2229523
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2229524
    new-instance p0, LX/FMI;

    invoke-direct {p0}, LX/FMI;-><init>()V

    .line 2229525
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3MF;->a(LX/0QB;)LX/3MF;

    move-result-object v4

    check-cast v4, LX/3MF;

    .line 2229526
    iput-object v3, p0, LX/FMI;->d:Landroid/content/Context;

    iput-object v4, p0, LX/FMI;->e:LX/3MF;

    .line 2229527
    move-object v0, p0

    .line 2229528
    sput-object v0, LX/FMI;->f:LX/FMI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2229529
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2229530
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2229531
    :cond_1
    sget-object v0, LX/FMI;->f:LX/FMI;

    return-object v0

    .line 2229532
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2229533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/FMI;)V
    .locals 6

    .prologue
    .line 2229502
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FMI;->c:LX/0tf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_1

    .line 2229503
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2229504
    :cond_1
    :try_start_1
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/FMI;->c:LX/0tf;

    .line 2229505
    iget-object v0, p0, LX/FMI;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/FMI;->a:Landroid/net/Uri;

    sget-object v2, LX/FMI;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 2229506
    if-eqz v1, :cond_0

    .line 2229507
    :goto_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2229508
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2229509
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2229510
    iget-object v4, p0, LX/FMI;->c:LX/0tf;

    iget-object v5, p0, LX/FMI;->e:LX/3MF;

    invoke-virtual {v5, v0}, LX/3MF;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v4, v2, v3, v0}, LX/0tf;->b(JLjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2229511
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2229512
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2229513
    :cond_2
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Set;)LX/0Rf;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2229484
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 2229485
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2229486
    const/4 v6, 0x0

    .line 2229487
    const/4 v4, 0x0

    .line 2229488
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2229489
    invoke-static {p0}, LX/FMI;->a(LX/FMI;)V

    .line 2229490
    iget-object v3, p0, LX/FMI;->c:LX/0tf;

    invoke-virtual {v3}, LX/0tf;->a()I

    move-result v7

    move v5, v6

    .line 2229491
    :goto_1
    if-ge v5, v7, :cond_2

    .line 2229492
    iget-object v3, p0, LX/FMI;->c:LX/0tf;

    invoke-virtual {v3, v5}, LX/0tf;->b(I)J

    move-result-wide v9

    .line 2229493
    iget-object v3, p0, LX/FMI;->c:LX/0tf;

    invoke-virtual {v3, v9, v10}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 2229494
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    const/4 v11, 0x1

    if-ne v8, v11, :cond_4

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2229495
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    cmp-long v3, v11, v9

    if-gez v3, :cond_4

    .line 2229496
    :cond_1
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2229497
    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v3

    goto :goto_1

    .line 2229498
    :cond_2
    move-object v0, v4

    .line 2229499
    if-eqz v0, :cond_0

    .line 2229500
    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2229501
    :cond_3
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0

    :cond_4
    move-object v3, v4

    goto :goto_2
.end method

.method public final a(J)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2229482
    invoke-static {p0}, LX/FMI;->a(LX/FMI;)V

    .line 2229483
    iget-object v0, p0, LX/FMI;->c:LX/0tf;

    invoke-virtual {v0, p1, p2}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final a(JLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2229479
    invoke-static {p0}, LX/FMI;->a(LX/FMI;)V

    .line 2229480
    iget-object v0, p0, LX/FMI;->c:LX/0tf;

    invoke-virtual {v0, p1, p2, p3}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 2229481
    return-void
.end method
