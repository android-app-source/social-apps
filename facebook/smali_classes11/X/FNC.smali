.class public final LX/FNC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/FND;


# direct methods
.method public constructor <init>(LX/FND;)V
    .locals 0

    .prologue
    .line 2232301
    iput-object p1, p0, LX/FNC;->a:LX/FND;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    .line 2232302
    iget-object v0, p0, LX/FNC;->a:LX/FND;

    iget v0, v0, LX/FND;->i:I

    packed-switch v0, :pswitch_data_0

    .line 2232303
    const/4 v0, 0x3

    .line 2232304
    :goto_0
    iget-object v1, p0, LX/FNC;->a:LX/FND;

    .line 2232305
    iget-object v2, v1, LX/FND;->c:LX/FOd;

    iget-object v3, v1, LX/FND;->e:Ljava/lang/String;

    iget-object v4, v1, LX/FND;->f:Lcom/facebook/user/model/User;

    .line 2232306
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2232307
    iget-boolean v5, v1, LX/FND;->h:Z

    .line 2232308
    iget-object v12, v2, LX/FOd;->e:LX/0TD;

    new-instance v6, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    move v10, v0

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/facebook/messaging/users/phone/MessengerUserMatcher$6;-><init>(LX/FOd;Ljava/lang/String;Ljava/lang/String;IZ)V

    const v7, -0x69f9c5d2

    invoke-static {v12, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2232309
    iget-object v2, v1, LX/FND;->g:LX/FN9;

    if-eqz v2, :cond_1

    .line 2232310
    iget-object v2, v1, LX/FND;->g:LX/FN9;

    .line 2232311
    packed-switch v0, :pswitch_data_1

    .line 2232312
    const-string v3, "rejected"

    .line 2232313
    :goto_1
    iget-object v4, v2, LX/FN9;->a:LX/FNA;

    iget-object v4, v4, LX/FNA;->e:LX/30m;

    const-string v5, "rejected"

    invoke-virtual {v4, v5, v3}, LX/30m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2232314
    const/4 v3, 0x6

    if-ne v0, v3, :cond_0

    .line 2232315
    iget-object v3, v2, LX/FN9;->a:LX/FNA;

    iget-object v3, v3, LX/FNA;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/FN9;->a:LX/FNA;

    iget-object v3, v3, LX/FNA;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/FN9;->a:LX/FNA;

    iget-object v3, v3, LX/FNA;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2232316
    iget v4, v3, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    move v3, v4

    .line 2232317
    :goto_2
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 2232318
    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, v2, LX/FN9;->a:LX/FNA;

    iget-object v6, v6, LX/FNA;->a:Landroid/content/Context;

    const-string v7, "com.facebook.messaging.sms.matching.picker.MatchingContactPickerActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2232319
    const-string v5, "color"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2232320
    const-string v3, "address"

    iget-object v5, v2, LX/FN9;->a:LX/FNA;

    iget-object v5, v5, LX/FNA;->r:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2232321
    iget-object v3, v2, LX/FN9;->a:LX/FNA;

    iget-object v3, v3, LX/FNA;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v5, v2, LX/FN9;->a:LX/FNA;

    iget-object v5, v5, LX/FNA;->a:Landroid/content/Context;

    invoke-interface {v3, v4, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2232322
    :cond_0
    iget-object v3, v2, LX/FN9;->a:LX/FNA;

    .line 2232323
    invoke-static {v3}, LX/FNA;->f$redex0(LX/FNA;)V

    .line 2232324
    :cond_1
    return-void

    .line 2232325
    :pswitch_0
    const/4 v0, 0x5

    .line 2232326
    goto :goto_0

    .line 2232327
    :pswitch_1
    const/4 v0, 0x6

    goto/16 :goto_0

    .line 2232328
    :pswitch_2
    const-string v3, "incorrect"

    goto :goto_1

    .line 2232329
    :pswitch_3
    const-string v3, "manual"

    goto :goto_1

    .line 2232330
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
