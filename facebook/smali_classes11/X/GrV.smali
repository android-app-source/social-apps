.class public final LX/GrV;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V
    .locals 0

    .prologue
    .line 2398083
    iput-object p1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;B)V
    .locals 0

    .prologue
    .line 2398084
    invoke-direct {p0, p1}, LX/GrV;-><init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 14

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2398085
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v2, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v2, v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a:LX/BV0;

    iget-object v3, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v3, v3, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->g:Landroid/net/Uri;

    sget-object v4, LX/7Sv;->NONE:LX/7Sv;

    invoke-virtual {v2, v3, v5, v5, v4}, LX/BV0;->a(Landroid/net/Uri;Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;Ljava/util/List;LX/7Sv;)LX/BUz;

    move-result-object v2

    .line 2398086
    iput-object v2, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->s:LX/BUz;

    .line 2398087
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v0, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    if-nez v0, :cond_2

    .line 2398088
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    move-object v2, v0

    .line 2398089
    :goto_0
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget v0, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->n:I

    mul-int/lit16 v0, v0, 0x12c

    move v3, v0

    :goto_1
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget v0, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->h:I

    add-int/lit16 v0, v0, 0x12c

    if-ge v3, v0, :cond_4

    .line 2398090
    :try_start_0
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v0, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->s:LX/BUz;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 2398091
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v7, v7, v8, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2398092
    invoke-virtual {v0, v3, v4}, LX/BUz;->a(ILandroid/graphics/RectF;)LX/1FJ;

    move-result-object v4

    move-object v0, v4

    .line 2398093
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2398094
    if-eqz v0, :cond_6

    .line 2398095
    :try_start_1
    iget-object v4, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    .line 2398096
    invoke-static {v4, v0, v2}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->a$redex0(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 2398097
    iget-object v4, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-static {v4}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->i(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)I

    .line 2398098
    if-nez v1, :cond_6

    iget-object v4, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v4, v4, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->t:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    if-nez v4, :cond_6

    .line 2398099
    :try_start_2
    iget-object v4, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->t:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->setImageUsingFile(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v4, v6

    .line 2398100
    :goto_2
    :try_start_3
    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget v1, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->h:I

    div-int/lit8 v1, v1, 0x2

    if-le v3, v1, :cond_0

    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-boolean v1, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    if-nez v1, :cond_0

    .line 2398101
    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    const/4 v7, 0x1

    .line 2398102
    iput-boolean v7, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    .line 2398103
    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v7, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v7, v7, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v8

    iget-object v7, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-wide v10, v7, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->B:J

    sub-long/2addr v8, v10

    .line 2398104
    iput-wide v8, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->z:J

    .line 2398105
    iget-object v7, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v8, 0x7f0d1840

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 2398106
    iput-object v1, v7, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->y:Landroid/widget/ProgressBar;

    .line 2398107
    new-instance v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$DecodeTask$1;

    invoke-direct {v1, p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$DecodeTask$1;-><init>(LX/GrV;)V

    invoke-static {v1}, LX/5fo;->a(Ljava/lang/Runnable;)V

    .line 2398108
    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-boolean v1, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->G:Z

    if-eqz v1, :cond_0

    .line 2398109
    new-instance v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$DecodeTask$2;

    invoke-direct {v1, p0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer$DecodeTask$2;-><init>(LX/GrV;)V

    invoke-static {v1}, LX/5fo;->a(Ljava/lang/Runnable;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2398110
    :cond_0
    if-eqz v0, :cond_5

    .line 2398111
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move v1, v4

    .line 2398112
    :cond_1
    :goto_3
    add-int/lit16 v0, v3, 0x12c

    move v3, v0

    goto/16 :goto_1

    .line 2398113
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v2, v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v2, v0

    goto/16 :goto_0

    .line 2398114
    :catch_0
    move-object v0, v5

    :goto_4
    if-eqz v0, :cond_1

    .line 2398115
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_3

    .line 2398116
    :catch_1
    move-exception v0

    move-object v4, v5

    .line 2398117
    :goto_5
    :try_start_4
    sget-object v7, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->r:Ljava/lang/Class;

    const-string v8, "Failed to extract images"

    invoke-static {v7, v8, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2398118
    if-eqz v4, :cond_1

    .line 2398119
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_3

    .line 2398120
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v5, :cond_3

    .line 2398121
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    throw v0

    .line 2398122
    :cond_4
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-object v1, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-wide v6, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->B:J

    sub-long/2addr v2, v6

    .line 2398123
    iput-wide v2, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->A:J

    .line 2398124
    iget-object v0, p0, LX/GrV;->a:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v0, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->s:LX/BUz;

    invoke-virtual {v0}, LX/BUz;->a()V

    .line 2398125
    return-object v5

    .line 2398126
    :catchall_1
    move-exception v1

    move-object v5, v0

    move-object v0, v1

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v5, v4

    goto :goto_6

    .line 2398127
    :catch_2
    move-exception v4

    move-object v12, v4

    move-object v4, v0

    move-object v0, v12

    goto :goto_5

    :catch_3
    move-exception v1

    move-object v4, v0

    move-object v0, v1

    move v1, v6

    goto :goto_5

    :catch_4
    move-exception v1

    move-object v12, v1

    move v1, v4

    move-object v4, v0

    move-object v0, v12

    goto :goto_5

    .line 2398128
    :catch_5
    goto :goto_4

    :catch_6
    move v1, v6

    goto :goto_4

    :catch_7
    move v1, v4

    goto :goto_4

    :cond_5
    move v1, v4

    goto :goto_3

    :cond_6
    move v4, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2398129
    invoke-direct {p0}, LX/GrV;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
