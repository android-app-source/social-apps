.class public final LX/Gka;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1OZ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;)V
    .locals 0

    .prologue
    .line 2389140
    iput-object p1, p0, LX/Gka;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/View;IJ)V
    .locals 8

    .prologue
    .line 2389141
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2389142
    check-cast v0, LX/GlX;

    .line 2389143
    invoke-virtual {v0, p3}, LX/GlX;->f(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2389144
    :goto_0
    return-void

    .line 2389145
    :cond_0
    invoke-virtual {v0, p3}, LX/GlX;->e(I)LX/Gkq;

    move-result-object v2

    .line 2389146
    iget-object v1, p0, LX/Gka;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v1, v1, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    invoke-virtual {v0}, LX/GlX;->d()LX/Gkm;

    move-result-object v3

    .line 2389147
    iget-object v4, v1, LX/Glj;->c:LX/0Zb;

    const-string v5, "groups_grid_group_clicked"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2389148
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2389149
    const-string v5, "groups_grid"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    .line 2389150
    iget-object v5, v2, LX/Gkq;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2389151
    invoke-virtual {v4, v5}, LX/0oG;->d(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    iget-object v5, v1, LX/Glj;->d:LX/0kv;

    iget-object v6, v1, LX/Glj;->b:Landroid/content/Context;

    invoke-virtual {v5, v6}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "position"

    invoke-virtual {v4, v5, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v4

    const-string v5, "is_favorite"

    .line 2389152
    iget-boolean v6, v2, LX/Gkq;->g:Z

    move v6, v6

    .line 2389153
    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    const-string v5, "badge_count"

    .line 2389154
    iget v6, v2, LX/Gkq;->f:I

    move v6, v6

    .line 2389155
    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v4

    const-string v5, "ordering"

    invoke-virtual {v3}, LX/Gkm;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2389156
    :cond_1
    iget-object v1, p0, LX/Gka;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->u:LX/17W;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/0ax;->C:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 2389157
    iget-object v7, v2, LX/Gkq;->a:Ljava/lang/String;

    move-object v7, v7

    .line 2389158
    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2389159
    iget-object v1, p0, LX/Gka;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    check-cast v1, LX/Gl6;

    .line 2389160
    iget-object v3, v2, LX/Gkq;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2389161
    invoke-static {v0, p3}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->b(LX/1OM;I)LX/GkS;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/Gl6;->a(Ljava/lang/String;LX/GkS;)V

    goto :goto_0
.end method
