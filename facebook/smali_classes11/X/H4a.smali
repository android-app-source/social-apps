.class public LX/H4a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

.field public c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

.field public d:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;LX/0Zb;)V
    .locals 0
    .param p1    # Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2423158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423159
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423160
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423161
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423162
    iput-object p1, p0, LX/H4a;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2423163
    iput-object p2, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423164
    iput-object p3, p0, LX/H4a;->d:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423165
    iput-object p4, p0, LX/H4a;->a:LX/0Zb;

    .line 2423166
    return-void
.end method

.method public static a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 2423117
    const-string v0, "session_id"

    iget-object v1, p0, LX/H4a;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2423118
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, v2

    .line 2423119
    iget-object v2, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2423120
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "result_list_id"

    .line 2423121
    sget-object v2, LX/H4Y;->a:[I

    invoke-virtual {p2}, LX/H4Z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2423122
    const-string v2, ""

    :goto_0
    move-object v2, v2

    .line 2423123
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "typeahead_session_id"

    .line 2423124
    sget-object v2, LX/H4Y;->a:[I

    invoke-virtual {p2}, LX/H4Z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 2423125
    const-string v2, ""

    :goto_1
    move-object v2, v2

    .line 2423126
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "typeahead_search_type"

    .line 2423127
    sget-object v2, LX/H4Y;->a:[I

    invoke-virtual {p2}, LX/H4Z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    .line 2423128
    const-string v2, ""

    :goto_2
    move-object v2, v2

    .line 2423129
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "ref"

    iget-object v2, p0, LX/H4a;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2423130
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v2, v3

    .line 2423131
    iget-object v3, v2, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->a:LX/CQB;

    move-object v2, v3

    .line 2423132
    invoke-virtual {v2}, LX/CQB;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2423133
    return-object p1

    .line 2423134
    :pswitch_0
    iget-object v2, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423135
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v2, v3

    .line 2423136
    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    if-nez v2, :cond_0

    const-string v2, ""

    goto :goto_0

    :cond_0
    iget-object v2, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423137
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v2, v3

    .line 2423138
    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2423139
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2423140
    goto :goto_0

    .line 2423141
    :pswitch_1
    iget-object v2, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423142
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v2, v3

    .line 2423143
    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    if-nez v2, :cond_1

    const-string v2, ""

    goto :goto_0

    :cond_1
    iget-object v2, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423144
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v2, v3

    .line 2423145
    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2423146
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2423147
    goto :goto_0

    .line 2423148
    :pswitch_2
    iget-object v2, p0, LX/H4a;->d:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423149
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    move-object v2, v3

    .line 2423150
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2423151
    goto :goto_1

    .line 2423152
    :pswitch_3
    iget-object v2, p0, LX/H4a;->d:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423153
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    move-object v2, v3

    .line 2423154
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2423155
    goto :goto_1

    .line 2423156
    :pswitch_4
    const-string v2, "places"

    goto :goto_2

    .line 2423157
    :pswitch_5
    const-string v2, "location"

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;Ljava/lang/String;II)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4
    .param p2    # LX/H4Z;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2423030
    invoke-static {p0, p1, p2}, LX/H4a;->a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2423031
    const-string v1, "selection_type"

    const-string v2, "typeahead_suggestion"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "selection_ids"

    if-nez p3, :cond_0

    const-string p3, ""

    :cond_0
    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "overall_rank"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "selection_rank"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2423032
    return-object v0
.end method

.method public static a(LX/H4a;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 2423107
    const-string v0, "nearby_places_module"

    .line 2423108
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/H4a;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2423109
    iget-object p0, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v2, p0

    .line 2423110
    iget-object p0, v2, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v2, p0

    .line 2423111
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2423112
    move-object v1, v1

    .line 2423113
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2423114
    move-object v1, v1

    .line 2423115
    move-object v0, v1

    .line 2423116
    return-object v0
.end method

.method public static f(LX/H4a;LX/H4Z;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H4Z;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2423077
    sget-object v2, LX/H4Y;->a:[I

    invoke-virtual {p1}, LX/H4Z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    .line 2423078
    :goto_0
    return-object v0

    .line 2423079
    :pswitch_0
    iget-object v2, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423080
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v2, v3

    .line 2423081
    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 2423082
    goto :goto_0

    .line 2423083
    :cond_0
    iget-object v1, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423084
    iget-object v2, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v1, v2

    .line 2423085
    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2423086
    iget-object v2, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->b:LX/0Px;

    move-object v3, v2

    .line 2423087
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 2423088
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2423089
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;

    .line 2423090
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2423091
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 2423092
    goto :goto_0

    .line 2423093
    :pswitch_1
    iget-object v2, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423094
    iget-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v2, v3

    .line 2423095
    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 2423096
    goto :goto_0

    .line 2423097
    :cond_2
    iget-object v1, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423098
    iget-object v2, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v1, v2

    .line 2423099
    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2423100
    iget-object v2, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    move-object v3, v2

    .line 2423101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 2423102
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2423103
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 2423104
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2423105
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 2423106
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static g(LX/H4a;LX/H4Z;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2423061
    sget-object v0, LX/H4Y;->a:[I

    invoke-virtual {p1}, LX/H4Z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2423062
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2423063
    :pswitch_0
    iget-object v0, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423064
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v0, v1

    .line 2423065
    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423066
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v0, v1

    .line 2423067
    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2423068
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2423069
    goto :goto_0

    .line 2423070
    :pswitch_1
    iget-object v0, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423071
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v0, v1

    .line 2423072
    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    if-nez v0, :cond_1

    const-string v0, ""

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423073
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v0, v1

    .line 2423074
    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2423075
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2423076
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/H4Z;)V
    .locals 5

    .prologue
    .line 2423049
    const-string v0, ""

    .line 2423050
    sget-object v1, LX/H4Y;->a:[I

    invoke-virtual {p1}, LX/H4Z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2423051
    :goto_0
    invoke-static {p0, v0}, LX/H4a;->a(LX/H4a;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2423052
    invoke-static {p0, v1, p1}, LX/H4a;->a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2423053
    const-string v2, "search_string"

    invoke-static {p0, p1}, LX/H4a;->g(LX/H4a;LX/H4Z;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "results_from_cache"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2423054
    invoke-static {p0, p1}, LX/H4a;->f(LX/H4a;LX/H4Z;)Ljava/util/List;

    move-result-object v2

    .line 2423055
    if-eqz v2, :cond_0

    .line 2423056
    const-string v3, "places_ids"

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2423057
    :cond_0
    iget-object v2, p0, LX/H4a;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2423058
    return-void

    .line 2423059
    :pswitch_0
    const-string v0, "nearby_places_search_typeahead_did_receive_results"

    goto :goto_0

    .line 2423060
    :pswitch_1
    const-string v0, "nearby_places_city_typeahead_did_receive_results"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2423042
    const-string v0, "search_result_user_selection"

    invoke-static {p0, v0}, LX/H4a;->a(LX/H4a;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2423043
    sget-object v2, LX/H4Z;->LOCATIONS:LX/H4Z;

    move-object v0, p0

    move-object v3, p1

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, LX/H4a;->a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;Ljava/lang/String;II)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2423044
    const-string v1, "result_name"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_current_location"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "user_has_location_services"

    iget-object v3, p0, LX/H4a;->c:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2423045
    iget-object v4, v3, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    move-object v3, v4

    .line 2423046
    iget-object v3, v3, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v3}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2423047
    iget-object v1, p0, LX/H4a;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2423048
    return-void
.end method

.method public final b(LX/H4Z;)V
    .locals 4

    .prologue
    .line 2423037
    const-string v0, "nearby_places_search_impression"

    invoke-static {p0, v0}, LX/H4a;->a(LX/H4a;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2423038
    invoke-static {p0, v0, p1}, LX/H4a;->a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2423039
    const-string v1, "search_string"

    invoke-static {p0, p1}, LX/H4a;->g(LX/H4a;LX/H4Z;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "places_ids"

    invoke-static {p0, p1}, LX/H4a;->f(LX/H4a;LX/H4Z;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2423040
    iget-object v1, p0, LX/H4a;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2423041
    return-void
.end method

.method public final c(LX/H4Z;)V
    .locals 2

    .prologue
    .line 2423033
    const-string v0, "search_button_tapped"

    invoke-static {p0, v0}, LX/H4a;->a(LX/H4a;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2423034
    invoke-static {p0, v0, p1}, LX/H4a;->a(LX/H4a;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/H4Z;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2423035
    iget-object v1, p0, LX/H4a;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2423036
    return-void
.end method
