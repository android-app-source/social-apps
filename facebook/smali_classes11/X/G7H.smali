.class public LX/G7H;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/G7H;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2321200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321201
    iput-object p1, p0, LX/G7H;->a:LX/0Zb;

    .line 2321202
    return-void
.end method

.method public static a(LX/0QB;)LX/G7H;
    .locals 4

    .prologue
    .line 2321203
    sget-object v0, LX/G7H;->b:LX/G7H;

    if-nez v0, :cond_1

    .line 2321204
    const-class v1, LX/G7H;

    monitor-enter v1

    .line 2321205
    :try_start_0
    sget-object v0, LX/G7H;->b:LX/G7H;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2321206
    if-eqz v2, :cond_0

    .line 2321207
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2321208
    new-instance p0, LX/G7H;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/G7H;-><init>(LX/0Zb;)V

    .line 2321209
    move-object v0, p0

    .line 2321210
    sput-object v0, LX/G7H;->b:LX/G7H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2321211
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2321212
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2321213
    :cond_1
    sget-object v0, LX/G7H;->b:LX/G7H;

    return-object v0

    .line 2321214
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2321215
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 5
    .param p2    # LX/0yY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2321216
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "sms_zero_dialog_action"

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2321217
    const-string v0, "action"

    invoke-virtual {v2, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "thread_id"

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v3, "zero_feature"

    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0yY;->name()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2321218
    iget-object v0, p0, LX/G7H;->a:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2321219
    return-void

    :cond_1
    move-object v0, v1

    .line 2321220
    goto :goto_0
.end method
