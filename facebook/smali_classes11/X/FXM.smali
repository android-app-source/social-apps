.class public final LX/FXM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/FXN;

.field public final b:Ljava/lang/String;

.field private final c:Z

.field public final d:LX/FXq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FXN;Ljava/lang/String;ZLX/FXq;)V
    .locals 0

    .prologue
    .line 2254873
    iput-object p1, p0, LX/FXM;->a:LX/FXN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2254874
    iput-object p2, p0, LX/FXM;->b:Ljava/lang/String;

    .line 2254875
    iput-boolean p3, p0, LX/FXM;->c:Z

    .line 2254876
    iput-object p4, p0, LX/FXM;->d:LX/FXq;

    .line 2254877
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2254993
    new-instance v0, LX/AUA;

    iget-object v1, p0, LX/FXM;->a:LX/FXN;

    iget-object v1, v1, LX/FXN;->g:LX/1Sd;

    invoke-direct {v0, v1}, LX/AUA;-><init>(LX/1Se;)V

    .line 2254994
    new-instance v1, LX/BO3;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v3}, LX/BO3;-><init>(II)V

    .line 2254995
    invoke-virtual {v0, v1}, LX/AUA;->b(LX/AU9;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, LX/BO3;->b(Landroid/database/Cursor;)LX/BO1;

    move-result-object v1

    .line 2254996
    :try_start_0
    invoke-interface {v1}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v0

    const/4 v2, -0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2254997
    :goto_0
    invoke-interface {v1}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2254998
    iget-object v0, p0, LX/FXM;->a:LX/FXN;

    iget-object v0, v0, LX/FXN;->i:LX/FZ3;

    invoke-interface {v1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, LX/FZ3;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2254999
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/AU0;->c()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/AU0;->c()V

    .line 2255000
    return-void
.end method

.method private static a(LX/FXM;Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel;)V
    .locals 20

    .prologue
    .line 2254898
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel;->a()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;

    move-result-object v2

    .line 2254899
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FXM;->a:LX/FXN;

    iget-object v3, v3, LX/FXN;->d:LX/Eke;

    invoke-virtual {v3}, LX/Eke;->a()LX/Ekd;

    move-result-object v12

    .line 2254900
    new-instance v3, LX/2uo;

    invoke-direct {v3}, LX/2uo;-><init>()V

    invoke-virtual {v12, v3}, LX/Ekd;->a(LX/AU6;)LX/EkY;

    move-result-object v13

    .line 2254901
    const/4 v4, 0x0

    .line 2254902
    const/4 v3, 0x0

    .line 2254903
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/FXM;->c:Z

    if-eqz v5, :cond_0

    .line 2254904
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FXM;->a:LX/FXN;

    iget-object v5, v5, LX/FXN;->g:LX/1Sd;

    invoke-virtual {v5}, LX/1Sd;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "item"

    const-string v7, "section_name=? AND is_download_client=? AND is_deleted_client=?"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/FXM;->b:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "0"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string v10, "0"

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2254905
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel$SavedItemsModel;->a()LX/0Px;

    move-result-object v14

    invoke-virtual {v14}, LX/0Px;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v15

    const/4 v2, 0x0

    move v9, v2

    move-object v5, v3

    move v3, v4

    :goto_0
    if-ge v9, v15, :cond_1b

    :try_start_1
    invoke-virtual {v14, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2254906
    add-int/lit8 v6, v3, 0x1

    .line 2254907
    :try_start_2
    invoke-virtual {v13}, LX/EkY;->a()LX/AU2;

    move-result-object v3

    check-cast v3, LX/BNy;

    .line 2254908
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FXM;->b:Ljava/lang/String;

    invoke-interface {v3, v4}, LX/BNy;->a(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, LX/BNy;->c(Ljava/lang/String;)LX/BNy;

    .line 2254909
    invoke-virtual {v2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;

    move-result-object v16

    .line 2254910
    invoke-static/range {v16 .. v16}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2254911
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->j()J

    move-result-wide v10

    const-wide/16 v18, 0x3e8

    mul-long v10, v10, v18

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->a(Ljava/lang/Long;)LX/BNy;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->s()Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    move-result-object v4

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->VIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    if-ne v4, v8, :cond_15

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v7, v4}, LX/BNy;->a(Ljava/lang/Integer;)LX/BNy;

    .line 2254912
    invoke-virtual {v2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_1

    .line 2254913
    invoke-virtual {v2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->d(Ljava/lang/String;)LX/BNy;

    .line 2254914
    :cond_1
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->q()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_2

    .line 2254915
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->q()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->f(Ljava/lang/String;)LX/BNy;

    .line 2254916
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->p()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_3

    .line 2254917
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->p()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->g(Ljava/lang/String;)LX/BNy;

    .line 2254918
    :cond_3
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->a()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_4

    .line 2254919
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->a()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->h(Ljava/lang/String;)LX/BNy;

    .line 2254920
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2254921
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->n()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;

    move-result-object v4

    .line 2254922
    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;->k()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 2254923
    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    const v8, 0x4c808d5

    if-ne v7, v8, :cond_16

    .line 2254924
    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->k(Ljava/lang/String;)LX/BNy;

    .line 2254925
    :cond_5
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 2254926
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 2254927
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->m(Ljava/lang/String;)LX/BNy;

    .line 2254928
    :cond_6
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_19

    .line 2254929
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2254930
    const/4 v8, 0x1

    invoke-virtual {v7, v4, v8}, LX/15i;->g(II)I

    move-result v4

    if-eqz v4, :cond_18

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_7

    .line 2254931
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v8, 0x1

    invoke-virtual {v7, v4, v8}, LX/15i;->g(II)I

    move-result v4

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->n(Ljava/lang/String;)LX/BNy;

    .line 2254932
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v8, 0x1

    invoke-virtual {v7, v4, v8}, LX/15i;->g(II)I

    move-result v4

    const/4 v8, 0x1

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->o(Ljava/lang/String;)LX/BNy;

    .line 2254933
    :cond_7
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->l()Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 2254934
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->l()Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->p(Ljava/lang/String;)LX/BNy;

    .line 2254935
    :cond_8
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-lez v4, :cond_a

    .line 2254936
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->k()LX/0Px;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$SavableActorsModel;

    .line 2254937
    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$SavableActorsModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, LX/BNy;->q(Ljava/lang/String;)LX/BNy;

    .line 2254938
    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$SavableActorsModel;->a()LX/1vs;

    move-result-object v7

    iget v7, v7, LX/1vs;->b:I

    if-eqz v7, :cond_9

    .line 2254939
    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$SavableActorsModel;->a()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const/4 v10, 0x0

    invoke-virtual {v8, v7, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, LX/BNy;->s(Ljava/lang/String;)LX/BNy;

    .line 2254940
    :cond_9
    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$SavableActorsModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->r(Ljava/lang/String;)LX/BNy;

    .line 2254941
    :cond_a
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->a()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$ItemAttributionModel;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 2254942
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->o()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel;->a()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$ItemAttributionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$SourceContainerModel$ItemAttributionModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->E(Ljava/lang/String;)LX/BNy;

    .line 2254943
    :cond_b
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 2254944
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->e(Ljava/lang/String;)LX/BNy;

    .line 2254945
    :cond_c
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v4

    if-eqz v4, :cond_d

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v4

    if-eqz v4, :cond_d

    .line 2254946
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->x(Ljava/lang/String;)LX/BNy;

    .line 2254947
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->z(Ljava/lang/String;)LX/BNy;

    .line 2254948
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;->k()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_d

    .line 2254949
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->k()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel;->j()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$GlobalShareModel$InstantArticleModel;->k()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2254950
    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->y(Ljava/lang/String;)LX/BNy;

    .line 2254951
    :cond_d
    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->m()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;

    move-result-object v17

    .line 2254952
    const/4 v8, 0x0

    .line 2254953
    const/4 v7, 0x0

    .line 2254954
    if-eqz v17, :cond_1e

    .line 2254955
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->b(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->r()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->a(Ljava/lang/Boolean;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->v()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->v(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->u()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->b(Ljava/lang/Integer;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->F()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->c(Ljava/lang/Integer;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->x()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->u(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->w()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->t(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->s()Z

    move-result v10

    if-eqz v10, :cond_1a

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->E()I

    move-result v10

    int-to-long v10, v10

    :goto_4
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->b(Ljava/lang/Long;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->t()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->d(Ljava/lang/Boolean;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->C()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->f(Ljava/lang/Integer;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->y()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->a(Ljava/lang/Double;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->B()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->A(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->A()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->B(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->z()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->b(Ljava/lang/Double;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->o()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->g(Ljava/lang/Integer;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->p()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->h(Ljava/lang/Integer;)LX/BNy;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->q()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, LX/BNy;->i(Ljava/lang/Integer;)LX/BNy;

    .line 2254956
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->r()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2254957
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FXM;->a:LX/FXN;

    iget-object v4, v4, LX/FXN;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/19w;

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, LX/19w;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2254958
    const/4 v7, 0x1

    .line 2254959
    :cond_e
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_1d

    .line 2254960
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, LX/BNy;->d(Ljava/lang/Integer;)LX/BNy;

    .line 2254961
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FXM;->a:LX/FXN;

    iget-object v4, v4, LX/FXN;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1nG;

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v8, v10}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2254962
    :goto_5
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->G()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v8

    if-eqz v8, :cond_f

    .line 2254963
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->G()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8}, LX/BNy;->i(Ljava/lang/String;)LX/BNy;

    .line 2254964
    :cond_f
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v8

    if-eqz v8, :cond_10

    .line 2254965
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;->j()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8}, LX/BNy;->w(Ljava/lang/String;)LX/BNy;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;->f()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v8, v10}, LX/BNy;->e(Ljava/lang/Integer;)LX/BNy;

    .line 2254966
    :cond_10
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->l()LX/1vs;

    move-result-object v8

    iget v8, v8, LX/1vs;->b:I

    if-eqz v8, :cond_11

    .line 2254967
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->l()LX/1vs;

    move-result-object v8

    iget-object v10, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2254968
    const/4 v11, 0x0

    invoke-virtual {v10, v8, v11}, LX/15i;->h(II)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-interface {v3, v8}, LX/BNy;->b(Ljava/lang/Boolean;)LX/BNy;

    .line 2254969
    :cond_11
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    if-eqz v8, :cond_12

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    const v10, 0xa7c5482

    if-ne v8, v10, :cond_12

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->k()Z

    move-result v8

    if-eqz v8, :cond_12

    .line 2254970
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->k()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-interface {v3, v8}, LX/BNy;->c(Ljava/lang/Boolean;)LX/BNy;

    .line 2254971
    :cond_12
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->D()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel$VideoChannelModel;

    move-result-object v8

    if-eqz v8, :cond_13

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->D()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel$VideoChannelModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel$VideoChannelModel;->j()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_13

    .line 2254972
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel;->D()Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel$VideoChannelModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2DashboardItemFieldsModel$VideoChannelModel;->j()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8}, LX/BNy;->D(Ljava/lang/String;)LX/BNy;

    .line 2254973
    :cond_13
    :goto_6
    if-nez v4, :cond_14

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel;->r()Ljava/lang/String;

    move-result-object v4

    :cond_14
    invoke-interface {v3, v4}, LX/BNy;->j(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-interface {v4}, LX/BNy;->b()LX/BNy;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, LX/FXM;->b:Ljava/lang/String;

    invoke-interface {v4, v8}, LX/BNy;->C(Ljava/lang/String;)LX/BNy;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, LX/BNy;->j(Ljava/lang/Integer;)LX/BNy;

    .line 2254974
    invoke-interface {v3}, LX/AU2;->a()J

    .line 2254975
    invoke-virtual {v2}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemsEdgeModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2254976
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v3, v6

    goto/16 :goto_0

    .line 2254977
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 2254978
    :cond_16
    move-object/from16 v0, p0

    iget-object v7, v0, LX/FXM;->a:LX/FXN;

    iget-object v7, v7, LX/FXN;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v4}, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$Saved2ItemModel$PermalinkNodeModel;->k()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v10

    invoke-static {v7, v8}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2254979
    invoke-interface {v3, v4}, LX/BNy;->l(Ljava/lang/String;)LX/BNy;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 2254980
    :catchall_0
    move-exception v2

    move-object v3, v5

    move v4, v6

    :goto_7
    invoke-virtual {v12}, LX/Ekd;->a()V

    .line 2254981
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FXM;->d:LX/FXq;

    if-eqz v5, :cond_17

    .line 2254982
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FXM;->d:LX/FXq;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FXM;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v4, v3}, LX/FXq;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 2254983
    :cond_17
    sget-object v3, LX/FWw;->b:LX/AUg;

    new-instance v5, LX/FWv;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FXM;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v4}, LX/FWv;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v3, v5}, LX/AUg;->b(LX/AUI;)Z

    throw v2

    .line 2254984
    :cond_18
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_19
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 2254985
    :cond_1a
    const-wide/16 v10, 0x0

    goto/16 :goto_4

    .line 2254986
    :cond_1b
    invoke-virtual {v12}, LX/Ekd;->a()V

    .line 2254987
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FXM;->d:LX/FXq;

    if-eqz v2, :cond_1c

    .line 2254988
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FXM;->d:LX/FXq;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/FXM;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v3, v5}, LX/FXq;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 2254989
    :cond_1c
    sget-object v2, LX/FWw;->b:LX/AUg;

    new-instance v4, LX/FWv;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/FXM;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v3}, LX/FWv;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v4}, LX/AUg;->b(LX/AUI;)Z

    .line 2254990
    invoke-direct/range {p0 .. p0}, LX/FXM;->a()V

    .line 2254991
    return-void

    .line 2254992
    :catchall_1
    move-exception v2

    goto :goto_7

    :catchall_2
    move-exception v2

    move v4, v3

    move-object v3, v5

    goto :goto_7

    :cond_1d
    move-object v4, v8

    goto/16 :goto_5

    :cond_1e
    move-object v4, v8

    goto/16 :goto_6
.end method


# virtual methods
.method public final a(LX/ElA;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2254878
    invoke-virtual {p1}, LX/ElA;->a()I

    .line 2254879
    :try_start_0
    const-string v1, "Content-Type"

    invoke-interface {p1, v1}, LX/El0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2254880
    if-eqz v1, :cond_0

    const-string v2, "x-fb-flatbuffer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 2254881
    :cond_0
    if-eqz v0, :cond_1

    .line 2254882
    new-instance v0, LX/15i;

    invoke-virtual {p1}, LX/ElA;->c()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, LX/ElS;->a(Ljava/io/InputStream;)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2254883
    const-string v1, "Saved2DataFetcher"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 2254884
    :goto_0
    const-class v1, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel;

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel;

    invoke-static {p0, v0}, LX/FXM;->a(LX/FXM;Lcom/facebook/saved2/graphql/FetchSaved2ItemsGraphQLModels$FetchSaved2ItemsGraphQLModel;)V

    .line 2254885
    return-void

    .line 2254886
    :cond_1
    iget-object v0, p0, LX/FXM;->a:LX/FXN;

    iget-object v0, v0, LX/FXN;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lp;

    invoke-virtual {p1}, LX/ElA;->c()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/InputStream;)LX/15w;

    move-result-object v0

    .line 2254887
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2254888
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2254889
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2254890
    invoke-static {v0}, LX/G6d;->a$redex0(LX/15w;)LX/15i;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 2254891
    :catch_0
    move-exception v0

    .line 2254892
    :goto_1
    throw v0

    .line 2254893
    :catch_1
    move-exception v0

    .line 2254894
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 2254895
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 2254896
    throw v1

    .line 2254897
    :catch_2
    move-exception v0

    goto :goto_1
.end method
