.class public final LX/Fjh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/selfupdate/SelfUpdateInstallActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/selfupdate/SelfUpdateInstallActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2277548
    iput-object p1, p0, LX/Fjh;->b:Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    iput-object p2, p0, LX/Fjh;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x45980258

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2277549
    iget-object v0, p0, LX/Fjh;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2277550
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 2277551
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, LX/Fjh;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2277552
    :cond_0
    iget-object v2, p0, LX/Fjh;->b:Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    iget-object v2, v2, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->q:LX/Fjr;

    iget-object v3, p0, LX/Fjh;->b:Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    const/4 v4, 0x4

    const/4 v9, 0x1

    .line 2277553
    if-nez v0, :cond_4

    .line 2277554
    const/4 v6, 0x0

    .line 2277555
    :goto_0
    move-object v6, v6

    .line 2277556
    if-nez v6, :cond_1

    .line 2277557
    :goto_1
    iget-object v0, p0, LX/Fjh;->b:Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    iget-object v0, v0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->p:LX/Fjj;

    const-string v2, "selfupdate_click_install"

    const-string v3, "source"

    iget-object v4, p0, LX/Fjh;->b:Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    iget-object v4, v4, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->B:Ljava/lang/String;

    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277558
    const v0, -0x48885f05

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2277559
    :cond_1
    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 2277560
    iget-object v8, v2, LX/Fjr;->l:LX/1sf;

    .line 2277561
    invoke-virtual {v8}, LX/1sf;->b()Z

    move-result v10

    if-nez v10, :cond_5

    const/4 v10, 0x0

    .line 2277562
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    .line 2277563
    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2277564
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {p1, v0, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    .line 2277565
    iget p1, p1, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_2

    const/4 v10, 0x1

    .line 2277566
    :cond_2
    :goto_2
    move v10, v10

    .line 2277567
    if-eqz v10, :cond_5

    const/4 v10, 0x1

    :goto_3
    move v7, v10

    .line 2277568
    if-eqz v7, :cond_3

    .line 2277569
    const-string v7, "android.intent.action.INSTALL_PACKAGE"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2277570
    const-string v7, "android.intent.extra.NOT_UNKNOWN_SOURCE"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2277571
    const-string v7, "android.intent.extra.ALLOW_REPLACE"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2277572
    const-string v7, "android.intent.extra.INSTALLER_PACKAGE_NAME"

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2277573
    iget-object v7, v2, LX/Fjr;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v7, v6, v4, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_1

    .line 2277574
    :cond_3
    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2277575
    const/high16 v7, 0x10000000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2277576
    iget-object v7, v2, LX/Fjr;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v7, v6, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 2277577
    :cond_4
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 2277578
    const-string v7, "application/vnd.android.package-archive"

    invoke-virtual {v6, v0, v7}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_5
    const/4 v10, 0x0

    goto :goto_3

    :catch_0
    goto :goto_2
.end method
