.class public LX/FBs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FBs;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/398;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0Or;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/398;",
            ">;>;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2209818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2209819
    iput-object p1, p0, LX/FBs;->a:LX/0Or;

    .line 2209820
    iput-object p2, p0, LX/FBs;->b:LX/03V;

    .line 2209821
    return-void
.end method

.method public static a(LX/0QB;)LX/FBs;
    .locals 5

    .prologue
    .line 2209822
    sget-object v0, LX/FBs;->c:LX/FBs;

    if-nez v0, :cond_1

    .line 2209823
    const-class v1, LX/FBs;

    monitor-enter v1

    .line 2209824
    :try_start_0
    sget-object v0, LX/FBs;->c:LX/FBs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2209825
    if-eqz v2, :cond_0

    .line 2209826
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2209827
    new-instance v4, LX/FBs;

    invoke-static {v0}, LX/47J;->a(LX/0QB;)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, p0, v3}, LX/FBs;-><init>(LX/0Or;LX/03V;)V

    .line 2209828
    move-object v0, v4

    .line 2209829
    sput-object v0, LX/FBs;->c:LX/FBs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2209830
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2209831
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2209832
    :cond_1
    sget-object v0, LX/FBs;->c:LX/FBs;

    return-object v0

    .line 2209833
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2209834
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2209835
    if-eqz p0, :cond_0

    const-string v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2209836
    :cond_0
    :goto_0
    return-object p0

    .line 2209837
    :cond_1
    const-string v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, 0x3

    .line 2209838
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    move v0, v1

    .line 2209839
    :goto_1
    if-ge v0, v2, :cond_4

    .line 2209840
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2209841
    const/16 v4, 0x2f

    if-eq v3, v4, :cond_2

    const/16 v4, 0x3f

    if-eq v3, v4, :cond_2

    const/16 v4, 0x3d

    if-ne v3, v4, :cond_3

    .line 2209842
    :cond_2
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 2209843
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2209844
    :cond_4
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
