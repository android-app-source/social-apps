.class public final LX/F0A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesQueryModel;",
        ">;",
        "Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F0B;


# direct methods
.method public constructor <init>(LX/F0B;)V
    .locals 0

    .prologue
    .line 2188016
    iput-object p1, p0, LX/F0A;->a:LX/F0B;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2188017
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2188018
    iget-object v0, p0, LX/F0A;->a:LX/F0B;

    .line 2188019
    if-eqz p1, :cond_0

    .line 2188020
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2188021
    if-eqz v1, :cond_0

    .line 2188022
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2188023
    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesQueryModel;

    invoke-virtual {v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesQueryModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesFragmentModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2188024
    :cond_0
    const/4 v1, 0x0

    .line 2188025
    :goto_0
    move-object v0, v1

    .line 2188026
    return-object v0

    .line 2188027
    :cond_1
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2188028
    check-cast v1, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesQueryModel;

    invoke-virtual {v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesQueryModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesFragmentModel;

    move-result-object v1

    .line 2188029
    invoke-virtual {v1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    move-result-object v1

    .line 2188030
    iget-object p0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object p0, p0

    .line 2188031
    invoke-static {v0, v1, p0}, LX/F0B;->a(LX/F0B;Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;LX/0ta;)Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;

    move-result-object v1

    goto :goto_0
.end method
