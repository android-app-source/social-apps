.class public abstract LX/GDY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "LX/0zP;",
        "T::",
        "LX/0jT;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/4At;

.field public b:LX/0tX;

.field public c:LX/1Ck;

.field public final d:LX/GG3;

.field public final e:LX/2U3;

.field public f:LX/GF4;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/2U3;)V
    .locals 0

    .prologue
    .line 2329923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2329924
    iput-object p1, p0, LX/GDY;->b:LX/0tX;

    .line 2329925
    iput-object p2, p0, LX/GDY;->c:LX/1Ck;

    .line 2329926
    iput-object p3, p0, LX/GDY;->f:LX/GF4;

    .line 2329927
    iput-object p4, p0, LX/GDY;->d:LX/GG3;

    .line 2329928
    iput-object p5, p0, LX/GDY;->e:LX/2U3;

    .line 2329929
    return-void
.end method

.method public static a$redex0(LX/GDY;Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 6

    .prologue
    .line 2329930
    new-instance v0, LX/3G1;

    invoke-direct {v0}, LX/3G1;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p0, p2, v1}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 2329931
    iput-wide v2, v0, LX/3G2;->d:J

    .line 2329932
    move-object v0, v0

    .line 2329933
    const/16 v1, 0x64

    .line 2329934
    iput v1, v0, LX/3G2;->f:I

    .line 2329935
    move-object v0, v0

    .line 2329936
    invoke-virtual {v0}, LX/3G2;->a()LX/3G3;

    move-result-object v0

    check-cast v0, LX/3G4;

    .line 2329937
    iget-object v1, p0, LX/GDY;->b:LX/0tX;

    sget-object v2, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v1, v0, v2}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2329938
    iget-object v2, p0, LX/GDY;->c:LX/1Ck;

    sget-object v3, LX/GDX;->MUTATION_TASK:LX/GDX;

    new-instance v4, LX/GDW;

    invoke-direct {v4, p0, v0}, LX/GDW;-><init>(LX/GDY;LX/3G4;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2329939
    iget-object v0, v0, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0}, LX/GDY;->a(Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Ljava/lang/String;)V

    .line 2329940
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
            "Z)TM;"
        }
    .end annotation
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2329907
    iget-object v0, p0, LX/GDY;->c:LX/1Ck;

    sget-object v1, LX/GDX;->MUTATION_TASK:LX/GDX;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2329908
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2329941
    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V
    .locals 4

    .prologue
    .line 2329916
    if-nez p4, :cond_0

    .line 2329917
    new-instance v0, LX/4At;

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p2, v1}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, LX/GDY;->a:LX/4At;

    .line 2329918
    iget-object v0, p0, LX/GDY;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2329919
    :cond_0
    invoke-virtual {p0}, LX/GDY;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2329920
    invoke-static {p0, p2, p1}, LX/GDY;->a$redex0(LX/GDY;Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2329921
    :goto_0
    return-void

    .line 2329922
    :cond_1
    iget-object v0, p0, LX/GDY;->c:LX/1Ck;

    sget-object v1, LX/GDX;->MUTATION_TASK:LX/GDX;

    iget-object v2, p0, LX/GDY;->b:LX/0tX;

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;

    move-result-object v3

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/GDV;

    invoke-direct {v3, p0, p2, p1}, LX/GDV;-><init>(LX/GDY;Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public abstract a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2329915
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2329910
    iget-object v0, p0, LX/GDY;->a:LX/4At;

    if-eqz v0, :cond_0

    .line 2329911
    iget-object v0, p0, LX/GDY;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2329912
    invoke-virtual {p0}, LX/GDY;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2329913
    iget-object v0, p0, LX/GDY;->f:LX/GF4;

    new-instance v1, LX/GFO;

    invoke-direct {v1, p1}, LX/GFO;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2329914
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2329909
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 2329906
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 2329905
    const/4 v0, 0x0

    return v0
.end method

.method public abstract e()Z
.end method
