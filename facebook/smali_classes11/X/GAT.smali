.class public final LX/GAT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GAO;


# instance fields
.field public final a:Ljava/io/OutputStream;

.field public final b:LX/GsN;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;LX/GsN;Z)V
    .locals 1

    .prologue
    .line 2325184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2325185
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GAT;->c:Z

    .line 2325186
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GAT;->d:Z

    .line 2325187
    iput-object p1, p0, LX/GAT;->a:Ljava/io/OutputStream;

    .line 2325188
    iput-object p2, p0, LX/GAT;->b:LX/GsN;

    .line 2325189
    iput-boolean p3, p0, LX/GAT;->d:Z

    .line 2325190
    return-void
.end method

.method private static a()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 2325183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value is not a supported type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/GAT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2325173
    iget-boolean v0, p0, LX/GAT;->d:Z

    if-nez v0, :cond_2

    .line 2325174
    const-string v0, "Content-Disposition: form-data; name=\"%s\""

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {p0, v0, v1}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325175
    if-eqz p2, :cond_0

    .line 2325176
    const-string v0, "; filename=\"%s\""

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v3

    invoke-static {p0, v0, v1}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325177
    :cond_0
    const-string v0, ""

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325178
    if-eqz p3, :cond_1

    .line 2325179
    const-string v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Content-Type"

    aput-object v2, v1, v3

    aput-object p3, v1, v4

    invoke-static {p0, v0, v1}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325180
    :cond_1
    const-string v0, ""

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325181
    :goto_0
    return-void

    .line 2325182
    :cond_2
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    const-string v1, "%s="

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    goto :goto_0
.end method

.method public static varargs a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2325147
    iget-boolean v0, p0, LX/GAT;->d:Z

    if-nez v0, :cond_1

    .line 2325148
    iget-boolean v0, p0, LX/GAT;->c:Z

    if-eqz v0, :cond_0

    .line 2325149
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    const-string v1, "--"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 2325150
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    const-string v1, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 2325151
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    const-string v1, "\r\n"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 2325152
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GAT;->c:Z

    .line 2325153
    :cond_0
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 2325154
    :goto_0
    return-void

    .line 2325155
    :cond_1
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2325160
    if-nez p3, :cond_0

    .line 2325161
    const-string p3, "content/unknown"

    .line 2325162
    :cond_0
    invoke-static {p0, p1, p1, p3}, LX/GAT;->a(LX/GAT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2325163
    invoke-static {}, LX/GAK;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 2325164
    iget-object v2, p0, LX/GAT;->a:Ljava/io/OutputStream;

    instance-of v2, v2, LX/GAi;

    if-eqz v2, :cond_2

    .line 2325165
    invoke-static {p2}, LX/Gsc;->a(Landroid/net/Uri;)J

    move-result-wide v2

    .line 2325166
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    check-cast v0, LX/GAi;

    invoke-virtual {v0, v2, v3}, LX/GAi;->a(J)V

    move v0, v1

    .line 2325167
    :goto_0
    const-string v2, ""

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325168
    invoke-static {p0}, LX/GAT;->b(LX/GAT;)V

    .line 2325169
    iget-object v2, p0, LX/GAT;->b:LX/GsN;

    if-eqz v2, :cond_1

    .line 2325170
    iget-object v2, p0, LX/GAT;->b:LX/GsN;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v5, "<Data: %d>"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325171
    :cond_1
    return-void

    .line 2325172
    :cond_2
    iget-object v2, p0, LX/GAT;->a:Ljava/io/OutputStream;

    invoke-static {v0, v2}, LX/Gsc;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2325191
    if-nez p3, :cond_0

    .line 2325192
    const-string p3, "content/unknown"

    .line 2325193
    :cond_0
    invoke-static {p0, p1, p1, p3}, LX/GAT;->a(LX/GAT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2325194
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    instance-of v0, v0, LX/GAi;

    if-eqz v0, :cond_2

    .line 2325195
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    check-cast v0, LX/GAi;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/GAi;->a(J)V

    move v0, v1

    .line 2325196
    :goto_0
    const-string v2, ""

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325197
    invoke-static {p0}, LX/GAT;->b(LX/GAT;)V

    .line 2325198
    iget-object v2, p0, LX/GAT;->b:LX/GsN;

    if-eqz v2, :cond_1

    .line 2325199
    iget-object v2, p0, LX/GAT;->b:LX/GsN;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v5, "<Data: %d>"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325200
    :cond_1
    return-void

    .line 2325201
    :cond_2
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v0, p2}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 2325202
    iget-object v2, p0, LX/GAT;->a:Ljava/io/OutputStream;

    invoke-static {v0, v2}, LX/Gsc;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/GAT;)V
    .locals 4

    .prologue
    .line 2325156
    iget-boolean v0, p0, LX/GAT;->d:Z

    if-nez v0, :cond_0

    .line 2325157
    const-string v0, "--%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f"

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325158
    :goto_0
    return-void

    .line 2325159
    :cond_0
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    const-string v1, "&"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    goto :goto_0
.end method

.method public static varargs b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2325143
    invoke-static {p0, p1, p2}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325144
    iget-boolean v0, p0, LX/GAT;->d:Z

    if-nez v0, :cond_0

    .line 2325145
    const-string v0, "\r\n"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325146
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;LX/GAU;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2325106
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    instance-of v0, v0, LX/GAh;

    if-eqz v0, :cond_0

    .line 2325107
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    check-cast v0, LX/GAh;

    invoke-interface {v0, p3}, LX/GAh;->a(LX/GAU;)V

    .line 2325108
    :cond_0
    invoke-static {p2}, LX/GAU;->e(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2325109
    invoke-static {p2}, LX/GAU;->f(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/GAT;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325110
    :goto_0
    return-void

    .line 2325111
    :cond_1
    instance-of v0, p2, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 2325112
    check-cast p2, Landroid/graphics/Bitmap;

    .line 2325113
    const-string v0, "image/png"

    invoke-static {p0, p1, p1, v0}, LX/GAT;->a(LX/GAT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2325114
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    iget-object v2, p0, LX/GAT;->a:Ljava/io/OutputStream;

    invoke-virtual {p2, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2325115
    const-string v0, ""

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325116
    invoke-static {p0}, LX/GAT;->b(LX/GAT;)V

    .line 2325117
    iget-object v0, p0, LX/GAT;->b:LX/GsN;

    if-eqz v0, :cond_2

    .line 2325118
    iget-object v0, p0, LX/GAT;->b:LX/GsN;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "<Image>"

    invoke-virtual {v0, v1, v2}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325119
    :cond_2
    goto :goto_0

    .line 2325120
    :cond_3
    instance-of v0, p2, [B

    if-eqz v0, :cond_5

    .line 2325121
    check-cast p2, [B

    check-cast p2, [B

    const/4 p3, 0x0

    .line 2325122
    const-string v0, "content/unknown"

    invoke-static {p0, p1, p1, v0}, LX/GAT;->a(LX/GAT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2325123
    iget-object v0, p0, LX/GAT;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p2}, Ljava/io/OutputStream;->write([B)V

    .line 2325124
    const-string v0, ""

    new-array v1, p3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325125
    invoke-static {p0}, LX/GAT;->b(LX/GAT;)V

    .line 2325126
    iget-object v0, p0, LX/GAT;->b:LX/GsN;

    if-eqz v0, :cond_4

    .line 2325127
    iget-object v0, p0, LX/GAT;->b:LX/GsN;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v3, "<Data: %d>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    array-length v5, p2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, p3

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325128
    :cond_4
    goto/16 :goto_0

    .line 2325129
    :cond_5
    instance-of v0, p2, Landroid/net/Uri;

    if-eqz v0, :cond_6

    .line 2325130
    check-cast p2, Landroid/net/Uri;

    invoke-direct {p0, p1, p2, v1}, LX/GAT;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2325131
    :cond_6
    instance-of v0, p2, Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_7

    .line 2325132
    check-cast p2, Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0, p1, p2, v1}, LX/GAT;->a(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2325133
    :cond_7
    instance-of v0, p2, Lcom/facebook/GraphRequest$ParcelableResourceWithMimeType;

    if-eqz v0, :cond_a

    .line 2325134
    check-cast p2, Lcom/facebook/GraphRequest$ParcelableResourceWithMimeType;

    .line 2325135
    iget-object v0, p2, Lcom/facebook/GraphRequest$ParcelableResourceWithMimeType;->b:Landroid/os/Parcelable;

    move-object v0, v0

    .line 2325136
    iget-object v1, p2, Lcom/facebook/GraphRequest$ParcelableResourceWithMimeType;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2325137
    instance-of v2, v0, Landroid/os/ParcelFileDescriptor;

    if-eqz v2, :cond_8

    .line 2325138
    check-cast v0, Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0, p1, v0, v1}, LX/GAT;->a(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2325139
    :cond_8
    instance-of v2, v0, Landroid/net/Uri;

    if-eqz v2, :cond_9

    .line 2325140
    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v1}, LX/GAT;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2325141
    :cond_9
    invoke-static {}, LX/GAT;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2325142
    :cond_a
    invoke-static {}, LX/GAT;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2325100
    invoke-static {p0, p1, v0, v0}, LX/GAT;->a(LX/GAT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2325101
    const-string v0, "%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, LX/GAT;->b(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325102
    invoke-static {p0}, LX/GAT;->b(LX/GAT;)V

    .line 2325103
    iget-object v0, p0, LX/GAT;->b:LX/GsN;

    if-eqz v0, :cond_0

    .line 2325104
    iget-object v0, p0, LX/GAT;->b:LX/GsN;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325105
    :cond_0
    return-void
.end method
