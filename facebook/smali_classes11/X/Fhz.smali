.class public LX/Fhz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Cwr;

.field private final b:LX/0tX;

.field private final c:LX/8hr;

.field public d:LX/7B0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:LX/0Uh;

.field private final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/Cwr;LX/0tX;LX/8hr;LX/0Uh;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2274016
    iput-object p1, p0, LX/Fhz;->a:LX/Cwr;

    .line 2274017
    iput-object p2, p0, LX/Fhz;->b:LX/0tX;

    .line 2274018
    iput-object p3, p0, LX/Fhz;->c:LX/8hr;

    .line 2274019
    iput-object p4, p0, LX/Fhz;->e:LX/0Uh;

    .line 2274020
    iput-object p5, p0, LX/Fhz;->f:LX/0ad;

    .line 2274021
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ILjava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0zS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cwv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 2274022
    new-instance v0, LX/A1P;

    invoke-direct {v0}, LX/A1P;-><init>()V

    .line 2274023
    new-instance v0, LX/A1O;

    invoke-direct {v0}, LX/A1O;-><init>()V

    move-object v2, v0

    .line 2274024
    const-string v0, "source"

    .line 2274025
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2274026
    iget-object v1, p0, LX/Fhz;->e:LX/0Uh;

    sget v4, LX/2SU;->y:I

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2274027
    new-instance v1, LX/4FV;

    invoke-direct {v1}, LX/4FV;-><init>()V

    const-string v4, "trending"

    invoke-virtual {v1, v4}, LX/4FV;->a(Ljava/lang/String;)LX/4FV;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/4FV;->a(Ljava/lang/Integer;)LX/4FV;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2274028
    :goto_0
    move-object v1, v1

    .line 2274029
    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 2274030
    const-string v0, "location"

    iget-object v1, p0, LX/Fhz;->c:LX/8hr;

    invoke-virtual {v1}, LX/8hr;->a()LX/4FR;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2274031
    const-string v0, "ranking_model"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "badge_model"

    invoke-virtual {v0, v1, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "bypass_cache"

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "should_show_live_icon"

    iget-object v3, p0, LX/Fhz;->f:LX/0ad;

    sget-short v4, LX/100;->aU:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2274032
    iget-object v0, p0, LX/Fhz;->f:LX/0ad;

    sget-short v1, LX/100;->av:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x1e13380

    .line 2274033
    :goto_1
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const-string v3, "null_state_module_cache_tag"

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    .line 2274034
    iput-object v3, v2, LX/0zO;->d:Ljava/util/Set;

    .line 2274035
    move-object v2, v2

    .line 2274036
    iput-boolean v5, v2, LX/0zO;->p:Z

    .line 2274037
    move-object v3, v2

    .line 2274038
    if-eqz p5, :cond_1

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_2
    invoke-virtual {v3, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    .line 2274039
    iput-object p5, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2274040
    move-object v2, v2

    .line 2274041
    invoke-virtual {v2, v0, v1}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2274042
    iget-object v1, p0, LX/Fhz;->d:LX/7B0;

    sget-object v2, LX/7Az;->NULLSTATE:LX/7Az;

    invoke-virtual {v1, v2, v0}, LX/7B0;->a(LX/7Az;LX/0zO;)V

    .line 2274043
    iget-object v1, p0, LX/Fhz;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2274044
    new-instance v1, LX/Fhy;

    invoke-direct {v1, p0}, LX/Fhy;-><init>(LX/Fhz;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2274045
    :cond_0
    iget-object v0, p0, LX/Fhz;->f:LX/0ad;

    sget v1, LX/100;->at:I

    const/16 v3, 0xe10

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    goto :goto_1

    .line 2274046
    :cond_1
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_2

    .line 2274047
    :cond_2
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2274048
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2274049
    new-instance v7, LX/4FV;

    invoke-direct {v7}, LX/4FV;-><init>()V

    invoke-virtual {v7, v1}, LX/4FV;->a(Ljava/lang/String;)LX/4FV;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/4FV;->a(Ljava/lang/Integer;)LX/4FV;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2274050
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_0
.end method
