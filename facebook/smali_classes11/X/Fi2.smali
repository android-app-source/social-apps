.class public final LX/Fi2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;",
        ">;",
        "LX/Cwv;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fi3;


# direct methods
.method public constructor <init>(LX/Fi3;)V
    .locals 0

    .prologue
    .line 2274102
    iput-object p1, p0, LX/Fi2;->a:LX/Fi3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2274103
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2274104
    if-eqz p1, :cond_0

    .line 2274105
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2274106
    if-nez v0, :cond_1

    .line 2274107
    :cond_0
    const/4 v0, 0x0

    .line 2274108
    :goto_0
    return-object v0

    .line 2274109
    :cond_1
    iget-object v0, p0, LX/Fi2;->a:LX/Fi3;

    iget-object v1, v0, LX/Fi3;->b:LX/Cwu;

    .line 2274110
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2274111
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Cwu;->a(Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;)LX/Cwv;

    move-result-object v0

    .line 2274112
    iget-wide v4, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v4

    .line 2274113
    iput-wide v2, v0, LX/Cwv;->b:J

    .line 2274114
    goto :goto_0
.end method
