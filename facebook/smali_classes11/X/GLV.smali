.class public final LX/GLV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field public final synthetic b:LX/GLb;


# direct methods
.method public constructor <init>(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V
    .locals 0

    .prologue
    .line 2343028
    iput-object p1, p0, LX/GLV;->b:LX/GLb;

    iput-object p2, p0, LX/GLV;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x55ca40ad

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2343029
    iget-object v0, p0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setMoreOptionsViewVisibility(I)V

    .line 2343030
    iget-object v0, p0, LX/GLV;->b:LX/GLb;

    iget-object v1, p0, LX/GLV;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GLV;->b:LX/GLb;

    iget v2, v2, LX/GLb;->l:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fetch_unified_audiences_task_key"

    const/4 v4, 0x5

    new-instance v5, LX/GLU;

    invoke-direct {v5, p0}, LX/GLU;-><init>(LX/GLV;)V

    invoke-virtual/range {v0 .. v5}, LX/GLb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Vd;)V

    .line 2343031
    const v0, -0x3b34382

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
