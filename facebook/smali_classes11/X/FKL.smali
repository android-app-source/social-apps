.class public LX/FKL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225004
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2225005
    check-cast p1, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;

    .line 2225006
    const-string v0, "/me/message_accept_requests"

    .line 2225007
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2225008
    iget-wide v5, p1, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;->b:J

    move-wide v2, v5

    .line 2225009
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2225010
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "thread_ids"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225011
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    const-string v3, "acceptMessageRequests"

    .line 2225012
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2225013
    move-object v2, v2

    .line 2225014
    const-string v3, "POST"

    .line 2225015
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2225016
    move-object v2, v2

    .line 2225017
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2225018
    move-object v0, v2

    .line 2225019
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2225020
    move-object v0, v0

    .line 2225021
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2225022
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225023
    move-object v0, v0

    .line 2225024
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225025
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225026
    const/4 v0, 0x0

    return-object v0
.end method
