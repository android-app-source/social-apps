.class public LX/GaE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/commerce/core/intent/MerchantInfoViewData;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z

.field public final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Ga8;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ZZZLX/0am;ZLX/0am;LX/0am;LX/0am;LX/0am;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ",
            "LX/0am",
            "<",
            "Lcom/facebook/commerce/core/intent/MerchantInfoViewData;",
            ">;Z",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0am",
            "<",
            "LX/Ga8;",
            ">;",
            "LX/0am",
            "<",
            "Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2368053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2368054
    iput-boolean p1, p0, LX/GaE;->a:Z

    .line 2368055
    iput-boolean p2, p0, LX/GaE;->b:Z

    .line 2368056
    iput-boolean p3, p0, LX/GaE;->c:Z

    .line 2368057
    iput-object p4, p0, LX/GaE;->d:LX/0am;

    .line 2368058
    iput-boolean p5, p0, LX/GaE;->e:Z

    .line 2368059
    iput-object p6, p0, LX/GaE;->f:LX/0am;

    .line 2368060
    iput-object p7, p0, LX/GaE;->g:LX/0am;

    .line 2368061
    iput-object p8, p0, LX/GaE;->h:LX/0am;

    .line 2368062
    iput-object p9, p0, LX/GaE;->i:LX/0am;

    .line 2368063
    iput-object p10, p0, LX/GaE;->j:LX/0am;

    .line 2368064
    return-void
.end method

.method public synthetic constructor <init>(ZZZLX/0am;ZLX/0am;LX/0am;LX/0am;LX/0am;LX/0am;B)V
    .locals 0

    .prologue
    .line 2368065
    invoke-direct/range {p0 .. p10}, LX/GaE;-><init>(ZZZLX/0am;ZLX/0am;LX/0am;LX/0am;LX/0am;LX/0am;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2368066
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/GaE;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2368067
    :cond_1
    :goto_0
    return v0

    .line 2368068
    :cond_2
    if-eq p0, p1, :cond_1

    .line 2368069
    check-cast p1, LX/GaE;

    .line 2368070
    iget-boolean v2, p0, LX/GaE;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2368071
    iget-boolean v3, p1, LX/GaE;->a:Z

    move v3, v3

    .line 2368072
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GaE;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2368073
    iget-boolean v3, p1, LX/GaE;->b:Z

    move v3, v3

    .line 2368074
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GaE;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2368075
    iget-boolean v3, p1, LX/GaE;->c:Z

    move v3, v3

    .line 2368076
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GaE;->d:LX/0am;

    .line 2368077
    iget-object v3, p1, LX/GaE;->d:LX/0am;

    move-object v3, v3

    .line 2368078
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GaE;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2368079
    iget-boolean v3, p1, LX/GaE;->e:Z

    move v3, v3

    .line 2368080
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GaE;->f:LX/0am;

    .line 2368081
    iget-object v3, p1, LX/GaE;->f:LX/0am;

    move-object v3, v3

    .line 2368082
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GaE;->g:LX/0am;

    .line 2368083
    iget-object v3, p1, LX/GaE;->g:LX/0am;

    move-object v3, v3

    .line 2368084
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GaE;->h:LX/0am;

    .line 2368085
    iget-object v3, p1, LX/GaE;->h:LX/0am;

    move-object v3, v3

    .line 2368086
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GaE;->i:LX/0am;

    .line 2368087
    iget-object v3, p1, LX/GaE;->i:LX/0am;

    move-object v3, v3

    .line 2368088
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GaE;->j:LX/0am;

    .line 2368089
    iget-object v3, p1, LX/GaE;->j:LX/0am;

    move-object v3, v3

    .line 2368090
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2368091
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/GaE;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/GaE;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/GaE;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/GaE;->d:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LX/GaE;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/GaE;->f:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/GaE;->g:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, LX/GaE;->h:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, LX/GaE;->i:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, LX/GaE;->j:LX/0am;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
