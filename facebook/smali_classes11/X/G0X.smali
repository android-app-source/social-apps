.class public final LX/G0X;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2309833
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2309834
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2309835
    :goto_0
    return v1

    .line 2309836
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2309837
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2309838
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2309839
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2309840
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2309841
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2309842
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2309843
    :cond_2
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2309844
    const/4 v3, 0x0

    .line 2309845
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 2309846
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2309847
    :goto_2
    move v0, v3

    .line 2309848
    goto :goto_1

    .line 2309849
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2309850
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2309851
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2309852
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2309853
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2309854
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2309855
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2309856
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2309857
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2309858
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2309859
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 2309860
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2309861
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2309862
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2309863
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2309864
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2309865
    if-eqz v0, :cond_0

    .line 2309866
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2309867
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2309868
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2309869
    if-eqz v0, :cond_2

    .line 2309870
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2309871
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2309872
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2309873
    if-eqz v1, :cond_1

    .line 2309874
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2309875
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2309876
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2309877
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2309878
    return-void
.end method
