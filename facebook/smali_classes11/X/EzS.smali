.class public final LX/EzS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/3Fw;",
        "LX/En3",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/EzT;


# direct methods
.method public constructor <init>(LX/EzT;I)V
    .locals 0

    .prologue
    .line 2187186
    iput-object p1, p0, LX/EzS;->b:LX/EzT;

    iput p2, p0, LX/EzS;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2187187
    check-cast p1, LX/3Fw;

    .line 2187188
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2187189
    iget-object v3, p1, LX/3Fw;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 2187190
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 2187191
    iget-object v5, p0, LX/EzS;->b:LX/EzT;

    iget-object v5, v5, LX/EzT;->e:LX/Ene;

    invoke-virtual {v5, v0}, LX/Ene;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2187192
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2187193
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2187194
    :cond_1
    iget-object v0, p0, LX/EzS;->b:LX/EzT;

    iget-object v0, v0, LX/EzT;->e:LX/Ene;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ene;->a(LX/0Px;)V

    .line 2187195
    iget-object v0, p0, LX/EzS;->b:LX/EzT;

    iget-object v1, p1, LX/3Fw;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v0, v1}, LX/EzT;->a$redex0(LX/EzT;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    .line 2187196
    iget-object v0, p0, LX/EzS;->b:LX/EzT;

    iget-object v0, v0, LX/EzT;->e:LX/Ene;

    sget-object v1, LX/Enq;->RIGHT:LX/Enq;

    iget v2, p0, LX/EzS;->a:I

    invoke-virtual {v0, v1, v2}, LX/Ene;->b(LX/Enq;I)LX/En3;

    move-result-object v0

    return-object v0
.end method
