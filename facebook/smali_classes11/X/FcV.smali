.class public LX/FcV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<+",
            "LX/FcI;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fcn;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0Uh;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Fcb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fcg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FcS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fcn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FcO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FcK;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262428
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FcV;->a:Ljava/util/Map;

    .line 2262429
    iput-object p8, p0, LX/FcV;->c:LX/0ad;

    .line 2262430
    iput-object p9, p0, LX/FcV;->d:LX/0Uh;

    .line 2262431
    const-string v0, "rp_commerce_price"

    invoke-direct {p0, v0, p1}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262432
    const-string v0, "rp_commerce_price_sort"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262433
    const-string v0, "rp_commerce_source"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262434
    const-string v0, "friends"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262435
    const-string v0, "city"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262436
    const-string v0, "employer"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262437
    const-string v0, "school"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262438
    const-string v0, "rp_chrono_sort"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262439
    const-string v0, "rp_author"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262440
    const-string v0, "rp_location"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262441
    const-string v0, "rp_creation_time"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262442
    iget-object v0, p0, LX/FcV;->d:LX/0Uh;

    sget v1, LX/2SU;->ad:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262443
    const-string v0, "rp_commerce_location"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262444
    const-string v0, "rp_commerce_distance"

    invoke-direct {p0, v0, p3}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262445
    :cond_0
    const-string v0, "set_search_features"

    invoke-direct {p0, v0, p5}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262446
    const-string v0, "set_search_open_now"

    invoke-direct {p0, v0, p6}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262447
    const-string v0, "set_search_price_category"

    invoke-direct {p0, v0, p7}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262448
    const-string v0, "set_search_sort"

    invoke-direct {p0, v0, p2}, LX/FcV;->a(Ljava/lang/String;LX/0Ot;)V

    .line 2262449
    iput-object p4, p0, LX/FcV;->b:LX/0Ot;

    .line 2262450
    return-void
.end method

.method public static a(LX/0QB;)LX/FcV;
    .locals 1

    .prologue
    .line 2262463
    invoke-static {p0}, LX/FcV;->b(LX/0QB;)LX/FcV;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<+",
            "LX/FcI;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2262461
    iget-object v0, p0, LX/FcV;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2262462
    return-void
.end method

.method public static b(LX/0QB;)LX/FcV;
    .locals 10

    .prologue
    .line 2262459
    new-instance v0, LX/FcV;

    const/16 v1, 0x3336

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3337

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3334

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3339

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3333

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3338

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x3332

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v0 .. v9}, LX/FcV;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0Uh;)V

    .line 2262460
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2262455
    if-nez p1, :cond_0

    .line 2262456
    :goto_0
    return v0

    .line 2262457
    :cond_0
    sget-object v1, LX/FcU;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2262458
    :pswitch_0
    iget-object v1, p0, LX/FcV;->c:LX/0ad;

    sget-short v2, LX/100;->E:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2262454
    iget-object v0, p0, LX/FcV;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)LX/FcI;
    .locals 1

    .prologue
    .line 2262451
    invoke-virtual {p0, p1}, LX/FcV;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2262452
    iget-object v0, p0, LX/FcV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcI;

    .line 2262453
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FcV;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcI;

    goto :goto_0
.end method
