.class public final LX/F7b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/F7a;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;)V
    .locals 0

    .prologue
    .line 2202204
    iput-object p1, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2202217
    iget-object v0, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->j:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    if-eqz v0, :cond_0

    .line 2202218
    iget-object v0, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->j:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    .line 2202219
    iget-object v1, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->s:LX/EtZ;

    .line 2202220
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/EtZ;->d(I)V

    .line 2202221
    :cond_0
    iget-object v0, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    const/4 v1, 0x1

    .line 2202222
    iget-object v2, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->i:LX/0i5;

    sget-object v3, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->h:[Ljava/lang/String;

    new-instance p0, LX/F7c;

    invoke-direct {p0, v0, v1}, LX/F7c;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;Z)V

    invoke-virtual {v2, v3, p0}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2202223
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2202211
    iget-object v0, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->c:LX/9Tk;

    .line 2202212
    sget-object v1, LX/9Tj;->LEGAL_MANAGE:LX/9Tj;

    invoke-static {v0, v1}, LX/9Tk;->a(LX/9Tk;LX/9Tj;)V

    .line 2202213
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2202214
    const-string v1, "titlebar_with_modal_done"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2202215
    iget-object v1, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->b:LX/17W;

    iget-object v2, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    const-string v4, "/invite/history"

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2202216
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2202207
    iget-object v0, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->c:LX/9Tk;

    .line 2202208
    sget-object v1, LX/9Tj;->LEGAL_LEARN_MORE:LX/9Tj;

    invoke-static {v0, v1}, LX/9Tk;->a(LX/9Tk;LX/9Tj;)V

    .line 2202209
    iget-object v0, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->b:LX/17W;

    iget-object v1, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->cU:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2202210
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2202205
    iget-object v0, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->b:LX/17W;

    iget-object v1, p0, LX/F7b;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->fj:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2202206
    return-void
.end method
