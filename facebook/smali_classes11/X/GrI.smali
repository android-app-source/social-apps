.class public final LX/GrI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/Callable;

.field public final synthetic b:Ljava/util/concurrent/Callable;

.field public final synthetic c:LX/GrS;


# direct methods
.method public constructor <init>(LX/GrS;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 2397926
    iput-object p1, p0, LX/GrI;->c:LX/GrS;

    iput-object p2, p0, LX/GrI;->a:Ljava/util/concurrent/Callable;

    iput-object p3, p0, LX/GrI;->b:Ljava/util/concurrent/Callable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 2397918
    iget-object v0, p0, LX/GrI;->b:Ljava/util/concurrent/Callable;

    if-eqz v0, :cond_0

    .line 2397919
    :try_start_0
    iget-object v0, p0, LX/GrI;->b:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2397920
    :cond_0
    :goto_0
    return-void

    .line 2397921
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2397922
    iget-object v0, p0, LX/GrI;->c:LX/GrS;

    iget-object v0, v0, LX/GrS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/GrS;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".onAnimationEnd"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error attempting to call onAnimationEnd"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 2397923
    iput-object v1, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2397924
    move-object v1, v2

    .line 2397925
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2397927
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 2397910
    iget-object v0, p0, LX/GrI;->a:Ljava/util/concurrent/Callable;

    if-eqz v0, :cond_0

    .line 2397911
    :try_start_0
    iget-object v0, p0, LX/GrI;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2397912
    :cond_0
    :goto_0
    return-void

    .line 2397913
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2397914
    iget-object v0, p0, LX/GrI;->c:LX/GrS;

    iget-object v0, v0, LX/GrS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/GrS;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".onAnimationStart"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error attempting to call onAnimationStart"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 2397915
    iput-object v1, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2397916
    move-object v1, v2

    .line 2397917
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method
