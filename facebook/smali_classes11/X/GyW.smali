.class public LX/GyW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GyW;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2410195
    iput-object p1, p0, LX/GyW;->a:LX/0Zb;

    .line 2410196
    return-void
.end method

.method public static a(LX/0QB;)LX/GyW;
    .locals 4

    .prologue
    .line 2410197
    sget-object v0, LX/GyW;->b:LX/GyW;

    if-nez v0, :cond_1

    .line 2410198
    const-class v1, LX/GyW;

    monitor-enter v1

    .line 2410199
    :try_start_0
    sget-object v0, LX/GyW;->b:LX/GyW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2410200
    if-eqz v2, :cond_0

    .line 2410201
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2410202
    new-instance p0, LX/GyW;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/GyW;-><init>(LX/0Zb;)V

    .line 2410203
    move-object v0, p0

    .line 2410204
    sput-object v0, LX/GyW;->b:LX/GyW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2410205
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2410206
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2410207
    :cond_1
    sget-object v0, LX/GyW;->b:LX/GyW;

    return-object v0

    .line 2410208
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2410209
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 2410210
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "location_settings_switch_history"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 2410211
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2410212
    move-object v0, v0

    .line 2410213
    const-string v1, "is_checked"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2410214
    iget-object v1, p0, LX/GyW;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2410215
    return-void
.end method
