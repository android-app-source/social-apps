.class public final enum LX/GUk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GUk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GUk;

.field public static final enum FETCH_DATA:LX/GUk;

.field public static final enum INIT:LX/GUk;

.field public static final enum OVERALL_TTI:LX/GUk;

.field public static final enum RENDER:LX/GUk;


# instance fields
.field public final eventId:I

.field public final markerName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2357897
    new-instance v0, LX/GUk;

    const-string v1, "OVERALL_TTI"

    const v2, 0x260001

    const-string v3, "background_location_settings_perf_overall_tti"

    invoke-direct {v0, v1, v4, v2, v3}, LX/GUk;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/GUk;->OVERALL_TTI:LX/GUk;

    .line 2357898
    new-instance v0, LX/GUk;

    const-string v1, "INIT"

    const v2, 0x260003

    const-string v3, "background_location_settings_perf_init"

    invoke-direct {v0, v1, v5, v2, v3}, LX/GUk;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/GUk;->INIT:LX/GUk;

    .line 2357899
    new-instance v0, LX/GUk;

    const-string v1, "FETCH_DATA"

    const v2, 0x260002

    const-string v3, "background_location_settings_perf_fetch_data"

    invoke-direct {v0, v1, v6, v2, v3}, LX/GUk;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/GUk;->FETCH_DATA:LX/GUk;

    .line 2357900
    new-instance v0, LX/GUk;

    const-string v1, "RENDER"

    const v2, 0x260004

    const-string v3, "background_location_settings_perf_render"

    invoke-direct {v0, v1, v7, v2, v3}, LX/GUk;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/GUk;->RENDER:LX/GUk;

    .line 2357901
    const/4 v0, 0x4

    new-array v0, v0, [LX/GUk;

    sget-object v1, LX/GUk;->OVERALL_TTI:LX/GUk;

    aput-object v1, v0, v4

    sget-object v1, LX/GUk;->INIT:LX/GUk;

    aput-object v1, v0, v5

    sget-object v1, LX/GUk;->FETCH_DATA:LX/GUk;

    aput-object v1, v0, v6

    sget-object v1, LX/GUk;->RENDER:LX/GUk;

    aput-object v1, v0, v7

    sput-object v0, LX/GUk;->$VALUES:[LX/GUk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2357902
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2357903
    iput p3, p0, LX/GUk;->eventId:I

    .line 2357904
    iput-object p4, p0, LX/GUk;->markerName:Ljava/lang/String;

    .line 2357905
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GUk;
    .locals 1

    .prologue
    .line 2357906
    const-class v0, LX/GUk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GUk;

    return-object v0
.end method

.method public static values()[LX/GUk;
    .locals 1

    .prologue
    .line 2357907
    sget-object v0, LX/GUk;->$VALUES:[LX/GUk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GUk;

    return-object v0
.end method
