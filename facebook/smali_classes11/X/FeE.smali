.class public LX/FeE;
.super LX/5Jl;
.source ""


# instance fields
.field private final a:LX/EKb;

.field private final b:LX/EKn;

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsElectionRace;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsCandidateInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1P0;Landroid/content/Context;LX/EKb;LX/EKn;)V
    .locals 1
    .param p1    # LX/1P0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2265327
    invoke-direct {p0, p2, p1}, LX/5Jl;-><init>(Landroid/content/Context;LX/1P1;)V

    .line 2265328
    iput-object p3, p0, LX/FeE;->a:LX/EKb;

    .line 2265329
    iput-object p4, p0, LX/FeE;->b:LX/EKn;

    .line 2265330
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2265331
    iput-object v0, p0, LX/FeE;->c:LX/0Px;

    .line 2265332
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2265333
    iput-object v0, p0, LX/FeE;->d:LX/0Px;

    .line 2265334
    return-void
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2265282
    if-nez p2, :cond_1

    .line 2265283
    iget-object v0, p0, LX/FeE;->a:LX/EKb;

    const/4 v1, 0x0

    .line 2265284
    new-instance v2, LX/EKa;

    invoke-direct {v2, v0}, LX/EKa;-><init>(LX/EKb;)V

    .line 2265285
    sget-object p0, LX/EKb;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EKZ;

    .line 2265286
    if-nez p0, :cond_0

    .line 2265287
    new-instance p0, LX/EKZ;

    invoke-direct {p0}, LX/EKZ;-><init>()V

    .line 2265288
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/EKZ;->a$redex0(LX/EKZ;LX/1De;IILX/EKa;)V

    .line 2265289
    move-object v2, p0

    .line 2265290
    move-object v1, v2

    .line 2265291
    move-object v0, v1

    .line 2265292
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2265293
    :goto_0
    return-object v0

    .line 2265294
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 2265295
    const/4 v0, 0x0

    .line 2265296
    new-instance v1, LX/EKW;

    invoke-direct {v1}, LX/EKW;-><init>()V

    .line 2265297
    sget-object v2, LX/EKX;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EKV;

    .line 2265298
    if-nez v2, :cond_2

    .line 2265299
    new-instance v2, LX/EKV;

    invoke-direct {v2}, LX/EKV;-><init>()V

    .line 2265300
    :cond_2
    invoke-static {v2, p1, v0, v0, v1}, LX/EKV;->a$redex0(LX/EKV;LX/1De;IILX/EKW;)V

    .line 2265301
    move-object v1, v2

    .line 2265302
    move-object v0, v1

    .line 2265303
    move-object v0, v0

    .line 2265304
    iget-object v1, p0, LX/FeE;->d:LX/0Px;

    .line 2265305
    iget-object v2, v0, LX/EKV;->a:LX/EKW;

    iput-object v1, v2, LX/EKW;->a:LX/0Px;

    .line 2265306
    iget-object v2, v0, LX/EKV;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2265307
    move-object v0, v0

    .line 2265308
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 2265309
    :cond_3
    iget-object v0, p0, LX/FeE;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    .line 2265310
    iget-object v1, p0, LX/FeE;->b:LX/EKn;

    const/4 v2, 0x0

    .line 2265311
    new-instance v3, LX/EKm;

    invoke-direct {v3, v1}, LX/EKm;-><init>(LX/EKn;)V

    .line 2265312
    sget-object p2, LX/EKn;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/EKl;

    .line 2265313
    if-nez p2, :cond_4

    .line 2265314
    new-instance p2, LX/EKl;

    invoke-direct {p2}, LX/EKl;-><init>()V

    .line 2265315
    :cond_4
    invoke-static {p2, p1, v2, v2, v3}, LX/EKl;->a$redex0(LX/EKl;LX/1De;IILX/EKm;)V

    .line 2265316
    move-object v3, p2

    .line 2265317
    move-object v2, v3

    .line 2265318
    move-object v1, v2

    .line 2265319
    iget-object v2, p0, LX/FeE;->d:LX/0Px;

    .line 2265320
    iget-object v3, v1, LX/EKl;->a:LX/EKm;

    iput-object v2, v3, LX/EKm;->a:LX/0Px;

    .line 2265321
    iget-object v3, v1, LX/EKl;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2265322
    move-object v1, v1

    .line 2265323
    iget-object v2, v1, LX/EKl;->a:LX/EKm;

    iput-object v0, v2, LX/EKm;->b:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    .line 2265324
    iget-object v2, v1, LX/EKl;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2265325
    move-object v0, v1

    .line 2265326
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsElectionRace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2265279
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    new-instance v1, LX/A3B;

    invoke-direct {v1}, LX/A3B;-><init>()V

    invoke-virtual {v1}, LX/A3B;->a()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/A3B;

    invoke-direct {v1}, LX/A3B;-><init>()V

    invoke-virtual {v1}, LX/A3B;->a()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/FeE;->c:LX/0Px;

    .line 2265280
    invoke-virtual {p0}, LX/3mY;->bE_()V

    .line 2265281
    return-void
.end method

.method public final b(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsCandidateInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2265276
    iput-object p1, p0, LX/FeE;->d:LX/0Px;

    .line 2265277
    invoke-virtual {p0}, LX/3mY;->bE_()V

    .line 2265278
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2265274
    iget-object v0, p0, LX/FeE;->c:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FeE;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2265275
    const/4 v0, 0x1

    return v0
.end method
