.class public final LX/G3x;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2316824
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2316825
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316826
    :goto_0
    return v1

    .line 2316827
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316828
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2316829
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2316830
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2316831
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2316832
    const-string v4, "cover_photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2316833
    const/4 v3, 0x0

    .line 2316834
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 2316835
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316836
    :goto_2
    move v2, v3

    .line 2316837
    goto :goto_1

    .line 2316838
    :cond_2
    const-string v4, "steps"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2316839
    invoke-static {p0, p1}, LX/G3y;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2316840
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2316841
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2316842
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2316843
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2316844
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316845
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2316846
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2316847
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2316848
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2316849
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2316850
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_3

    .line 2316851
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2316852
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2316853
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2316854
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2316855
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2316856
    if-eqz v0, :cond_1

    .line 2316857
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316858
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2316859
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2316860
    if-eqz v1, :cond_0

    .line 2316861
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316862
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2316863
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2316864
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2316865
    if-eqz v0, :cond_2

    .line 2316866
    const-string v1, "steps"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316867
    invoke-static {p0, v0, p2, p3}, LX/G3y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2316868
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2316869
    return-void
.end method
