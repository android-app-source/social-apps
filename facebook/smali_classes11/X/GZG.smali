.class public final enum LX/GZG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GZG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GZG;

.field public static final enum ADDITIONAL_HEADER:LX/GZG;

.field public static final enum CREATE_STORE_VIEW:LX/GZG;

.field public static final enum EMPTY_STORE_VIEW:LX/GZG;

.field public static final enum FOOTER_VIEW:LX/GZG;

.field public static final enum LOADING_VIEW:LX/GZG;

.field public static final enum PRODUCT_GROUPING:LX/GZG;

.field public static final enum STOREFRONT_MAIN_HEADER:LX/GZG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2366115
    new-instance v0, LX/GZG;

    const-string v1, "ADDITIONAL_HEADER"

    invoke-direct {v0, v1, v3}, LX/GZG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZG;->ADDITIONAL_HEADER:LX/GZG;

    .line 2366116
    new-instance v0, LX/GZG;

    const-string v1, "LOADING_VIEW"

    invoke-direct {v0, v1, v4}, LX/GZG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZG;->LOADING_VIEW:LX/GZG;

    .line 2366117
    new-instance v0, LX/GZG;

    const-string v1, "CREATE_STORE_VIEW"

    invoke-direct {v0, v1, v5}, LX/GZG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZG;->CREATE_STORE_VIEW:LX/GZG;

    .line 2366118
    new-instance v0, LX/GZG;

    const-string v1, "EMPTY_STORE_VIEW"

    invoke-direct {v0, v1, v6}, LX/GZG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZG;->EMPTY_STORE_VIEW:LX/GZG;

    .line 2366119
    new-instance v0, LX/GZG;

    const-string v1, "STOREFRONT_MAIN_HEADER"

    invoke-direct {v0, v1, v7}, LX/GZG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZG;->STOREFRONT_MAIN_HEADER:LX/GZG;

    .line 2366120
    new-instance v0, LX/GZG;

    const-string v1, "PRODUCT_GROUPING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GZG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZG;->PRODUCT_GROUPING:LX/GZG;

    .line 2366121
    new-instance v0, LX/GZG;

    const-string v1, "FOOTER_VIEW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/GZG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZG;->FOOTER_VIEW:LX/GZG;

    .line 2366122
    const/4 v0, 0x7

    new-array v0, v0, [LX/GZG;

    sget-object v1, LX/GZG;->ADDITIONAL_HEADER:LX/GZG;

    aput-object v1, v0, v3

    sget-object v1, LX/GZG;->LOADING_VIEW:LX/GZG;

    aput-object v1, v0, v4

    sget-object v1, LX/GZG;->CREATE_STORE_VIEW:LX/GZG;

    aput-object v1, v0, v5

    sget-object v1, LX/GZG;->EMPTY_STORE_VIEW:LX/GZG;

    aput-object v1, v0, v6

    sget-object v1, LX/GZG;->STOREFRONT_MAIN_HEADER:LX/GZG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GZG;->PRODUCT_GROUPING:LX/GZG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GZG;->FOOTER_VIEW:LX/GZG;

    aput-object v2, v0, v1

    sput-object v0, LX/GZG;->$VALUES:[LX/GZG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2366114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GZG;
    .locals 1

    .prologue
    .line 2366123
    const-class v0, LX/GZG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GZG;

    return-object v0
.end method

.method public static values()[LX/GZG;
    .locals 1

    .prologue
    .line 2366113
    sget-object v0, LX/GZG;->$VALUES:[LX/GZG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GZG;

    return-object v0
.end method
