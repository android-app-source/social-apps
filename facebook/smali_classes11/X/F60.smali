.class public LX/F60;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/fbui/facepile/FacepileView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2200038
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2200039
    const-class v0, LX/F60;

    invoke-static {v0, p0}, LX/F60;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2200040
    const v0, 0x7f030839

    move v0, v0

    .line 2200041
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2200042
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/F60;->setOrientation(I)V

    .line 2200043
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b208b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/F60;->setMinimumWidth(I)V

    .line 2200044
    const v0, 0x7f0d157c

    invoke-virtual {p0, v0}, LX/F60;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/F60;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2200045
    const v0, 0x7f0d157d

    invoke-virtual {p0, v0}, LX/F60;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/F60;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2200046
    const v0, 0x7f0d157e

    invoke-virtual {p0, v0}, LX/F60;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    iput-object v0, p0, LX/F60;->h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    .line 2200047
    const v0, 0x7f0d1579

    invoke-virtual {p0, v0}, LX/F60;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/F60;->e:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2200048
    iget-object v0, p0, LX/F60;->h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    iget-object v1, p0, LX/F60;->c:Landroid/content/res/Resources;

    const p1, 0x7f0a0046

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setDividerColor(I)V

    .line 2200049
    iget-object v0, p0, LX/F60;->h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    iget-object v1, p0, LX/F60;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b0033

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setDividerThicknessPx(I)V

    .line 2200050
    iget-object v0, p0, LX/F60;->h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    iget-object v1, p0, LX/F60;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b0f55

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setDividerMarginPx(I)V

    .line 2200051
    return-void
.end method

.method private static a(LX/F60;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;)Landroid/view/View$OnClickListener;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGroupVisibility;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2200052
    new-instance v0, LX/F5z;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, LX/F5z;-><init>(LX/F60;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/F60;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v2

    check-cast v2, LX/1g8;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    iput-object v1, p1, LX/F60;->a:LX/17W;

    iput-object v2, p1, LX/F60;->b:LX/1g8;

    iput-object v3, p1, LX/F60;->c:Landroid/content/res/Resources;

    iput-object p0, p1, LX/F60;->d:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public final a(LX/F5o;)V
    .locals 14

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 2200053
    if-eqz p1, :cond_0

    .line 2200054
    iget-object v0, p1, LX/F5o;->b:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    move-object v0, v0

    .line 2200055
    if-nez v0, :cond_1

    .line 2200056
    :cond_0
    :goto_0
    return-void

    .line 2200057
    :cond_1
    iget-object v0, p1, LX/F5o;->b:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    move-object v7, v0

    .line 2200058
    iget-object v0, p0, LX/F60;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200059
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2200060
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2200061
    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2200062
    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    .line 2200063
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;

    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2200064
    if-le v1, v11, :cond_2

    .line 2200065
    const-string v0, " + "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, LX/F60;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0f0162

    add-int/lit8 v9, v1, -0x1

    new-array v10, v11, [Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v2

    invoke-virtual {v6, v8, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2200066
    :cond_2
    iget-object v0, p0, LX/F60;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200067
    iget-object v0, p0, LX/F60;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    move v1, v2

    .line 2200068
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2200069
    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;

    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2200070
    iget-object v0, p0, LX/F60;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2200071
    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;

    const/4 v6, 0x0

    .line 2200072
    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->l()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->l()LX/1vs;

    move-result-object v4

    iget-object v8, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v9, 0x0

    invoke-virtual {v8, v4, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 2200073
    :goto_2
    new-instance v8, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    new-instance v9, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->m()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v6}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lcom/facebook/user/model/UserKey;

    sget-object v10, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->j()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v10, v13}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v8, v9, v4, v6}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    move-object v0, v8

    .line 2200074
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2200075
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2200076
    :cond_4
    iget-object v0, p0, LX/F60;->g:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2200077
    :cond_5
    iget-boolean v0, p1, LX/F5o;->a:Z

    move v0, v0

    .line 2200078
    if-eqz v0, :cond_7

    .line 2200079
    iget-object v0, p0, LX/F60;->h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    invoke-virtual {p0}, LX/F60;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020c6b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2200080
    iget-object v0, p0, LX/F60;->h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    invoke-virtual {v0, v12}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200081
    iget-object v0, p0, LX/F60;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v12}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200082
    iget-object v0, p0, LX/F60;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v12}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200083
    :cond_6
    :goto_3
    iget-object v0, p0, LX/F60;->e:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceCountForOverflow(I)V

    .line 2200084
    iget-object v0, p0, LX/F60;->e:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2200085
    iget-object v1, p1, LX/F5o;->b:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    move-object v1, v1

    .line 2200086
    if-nez v1, :cond_9

    .line 2200087
    const/4 v1, 0x0

    .line 2200088
    :goto_4
    move-object v1, v1

    .line 2200089
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2200090
    :cond_7
    const-string v0, ","

    new-array v1, v11, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2200091
    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->k()Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

    move-result-object v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/F60;->a(LX/F60;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 2200092
    iget-object v1, p0, LX/F60;->h:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    invoke-virtual {v1, v0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200093
    iget-object v1, p0, LX/F60;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200094
    iget-object v1, p0, LX/F60;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_6

    .line 2200095
    iget-object v1, p0, LX/F60;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    :cond_8
    move-object v4, v6

    .line 2200096
    goto/16 :goto_2

    .line 2200097
    :cond_9
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2200098
    invoke-virtual {v1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;->l()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v4

    move v5, v4

    :goto_5
    if-ge v6, v9, :cond_e

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;

    .line 2200099
    if-eqz v1, :cond_b

    .line 2200100
    invoke-virtual {v1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->l()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2200101
    if-eqz v2, :cond_a

    move v2, v3

    :goto_6
    if-eqz v2, :cond_d

    .line 2200102
    invoke-virtual {v1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->l()LX/1vs;

    move-result-object v2

    iget-object v10, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2200103
    invoke-virtual {v10, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    move v2, v3

    :goto_7
    if-eqz v2, :cond_f

    .line 2200104
    invoke-virtual {v1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$SuggestedMembersModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2200105
    add-int/lit8 v1, v5, 0x1

    .line 2200106
    :goto_8
    const/4 v2, 0x5

    if-ge v1, v2, :cond_e

    .line 2200107
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v5, v1

    goto :goto_5

    :cond_a
    move v2, v4

    .line 2200108
    goto :goto_6

    :cond_b
    move v2, v4

    goto :goto_6

    :cond_c
    move v2, v4

    goto :goto_7

    :cond_d
    move v2, v4

    goto :goto_7

    .line 2200109
    :cond_e
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_4

    :cond_f
    move v1, v5

    goto :goto_8
.end method
