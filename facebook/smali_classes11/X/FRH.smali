.class public LX/FRH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wI",
        "<",
        "Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;",
        "LX/FRJ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240218
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;
    .locals 9

    .prologue
    .line 2240219
    check-cast p1, Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;

    .line 2240220
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2240221
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FRJ;

    .line 2240222
    sget-object v4, LX/FRG;->a:[I

    invoke-virtual {v0}, LX/FRJ;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 2240223
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2240224
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240225
    check-cast v0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;

    iget-object v0, v0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;->a:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240226
    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransactions;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v7, :cond_0

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/history/model/PaymentTransaction;

    .line 2240227
    new-instance v8, LX/FRD;

    const/4 p0, 0x0

    invoke-direct {v8, v4, p0}, LX/FRD;-><init>(Lcom/facebook/payments/history/model/PaymentTransaction;Landroid/content/Intent;)V

    invoke-virtual {v2, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240228
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 2240229
    :cond_0
    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransactions;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2240230
    new-instance v4, LX/71B;

    invoke-direct {v4}, LX/71B;-><init>()V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240231
    :cond_1
    goto :goto_1

    .line 2240232
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
