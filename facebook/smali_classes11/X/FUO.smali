.class public final LX/FUO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/qrcode/QRCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/qrcode/QRCodeFragment;)V
    .locals 0

    .prologue
    .line 2248227
    iput-object p1, p0, LX/FUO;->a:Lcom/facebook/qrcode/QRCodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x3c042694

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2248228
    iget-object v1, p0, LX/FUO;->a:Lcom/facebook/qrcode/QRCodeFragment;

    .line 2248229
    iget-object v3, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248230
    iget-object v4, v3, LX/FUf;->a:LX/0if;

    sget-object p0, LX/0ig;->H:LX/0ih;

    const-string p1, "IMPORT_FILE_START"

    invoke-virtual {v4, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248231
    iget-object v3, v1, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    .line 2248232
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/FUg;->e:Z

    .line 2248233
    iget-object v3, v1, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    .line 2248234
    const-string v4, "qrcode_import_pressed"

    invoke-static {v3, v4}, LX/FUW;->e(LX/FUW;Ljava/lang/String;)V

    .line 2248235
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2248236
    const-string v4, "image/*"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2248237
    const-string v4, "android.intent.action.GET_CONTENT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2248238
    iget-object v4, v1, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    const p0, 0x7f0832ea

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2248239
    const v1, -0x59976053

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
