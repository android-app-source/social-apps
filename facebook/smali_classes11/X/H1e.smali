.class public final LX/H1e;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public final synthetic b:Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;Lcom/facebook/widget/FbSwipeRefreshLayout;)V
    .locals 0

    .prologue
    .line 2415651
    iput-object p1, p0, LX/H1e;->b:Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;

    iput-object p2, p0, LX/H1e;->a:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2415642
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 2415643
    if-nez v0, :cond_0

    .line 2415644
    const-class v0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;

    const-string v1, "RecyclerView has no layout manager"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2415645
    :goto_0
    return-void

    .line 2415646
    :cond_0
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v3

    .line 2415647
    if-nez v3, :cond_1

    invoke-virtual {v0, v3}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2415648
    :goto_1
    iget-object v3, p0, LX/H1e;->a:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/H1e;->b:Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;

    iget-boolean v0, v0, Lcom/facebook/nativetemplates/fb/shell/NativeTemplatesFragment;->i:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {v3, v1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2415649
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2415650
    goto :goto_2
.end method
