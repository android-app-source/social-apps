.class public final enum LX/Gyp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gyp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gyp;

.field public static final enum FETCH_DATA:LX/Gyp;

.field public static final enum INIT:LX/Gyp;

.field public static final enum OVERALL_TTI:LX/Gyp;

.field public static final enum RENDER:LX/Gyp;


# instance fields
.field public markerId:I

.field public final markerName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2410611
    new-instance v0, LX/Gyp;

    const-string v1, "OVERALL_TTI"

    const v2, 0x330006

    const-string v3, "location_settings_perf_overall_tti"

    invoke-direct {v0, v1, v4, v2, v3}, LX/Gyp;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Gyp;->OVERALL_TTI:LX/Gyp;

    .line 2410612
    new-instance v0, LX/Gyp;

    const-string v1, "INIT"

    const v2, 0x330007

    const-string v3, "location_settings_perf_init"

    invoke-direct {v0, v1, v5, v2, v3}, LX/Gyp;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Gyp;->INIT:LX/Gyp;

    .line 2410613
    new-instance v0, LX/Gyp;

    const-string v1, "FETCH_DATA"

    const v2, 0x330008

    const-string v3, "location_settings_perf_fetch_data"

    invoke-direct {v0, v1, v6, v2, v3}, LX/Gyp;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Gyp;->FETCH_DATA:LX/Gyp;

    .line 2410614
    new-instance v0, LX/Gyp;

    const-string v1, "RENDER"

    const v2, 0x330009

    const-string v3, "location_settings_perf_render"

    invoke-direct {v0, v1, v7, v2, v3}, LX/Gyp;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Gyp;->RENDER:LX/Gyp;

    .line 2410615
    const/4 v0, 0x4

    new-array v0, v0, [LX/Gyp;

    sget-object v1, LX/Gyp;->OVERALL_TTI:LX/Gyp;

    aput-object v1, v0, v4

    sget-object v1, LX/Gyp;->INIT:LX/Gyp;

    aput-object v1, v0, v5

    sget-object v1, LX/Gyp;->FETCH_DATA:LX/Gyp;

    aput-object v1, v0, v6

    sget-object v1, LX/Gyp;->RENDER:LX/Gyp;

    aput-object v1, v0, v7

    sput-object v0, LX/Gyp;->$VALUES:[LX/Gyp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2410605
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2410606
    iput-object p4, p0, LX/Gyp;->markerName:Ljava/lang/String;

    .line 2410607
    iput p3, p0, LX/Gyp;->markerId:I

    .line 2410608
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gyp;
    .locals 1

    .prologue
    .line 2410610
    const-class v0, LX/Gyp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gyp;

    return-object v0
.end method

.method public static values()[LX/Gyp;
    .locals 1

    .prologue
    .line 2410609
    sget-object v0, LX/Gyp;->$VALUES:[LX/Gyp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gyp;

    return-object v0
.end method
