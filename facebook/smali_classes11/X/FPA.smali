.class public LX/FPA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/9kE;

.field public b:LX/0tX;


# direct methods
.method public constructor <init>(LX/9kE;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2236764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236765
    iput-object p1, p0, LX/FPA;->a:LX/9kE;

    .line 2236766
    iput-object p2, p0, LX/FPA;->b:LX/0tX;

    .line 2236767
    return-void
.end method

.method public static a(LX/FP4;)LX/CQD;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2236768
    if-nez p0, :cond_0

    move-object v0, v2

    .line 2236769
    :goto_0
    return-object v0

    .line 2236770
    :cond_0
    const/16 v0, 0x32

    .line 2236771
    iget-object v1, p0, LX/FP4;->i:LX/FPn;

    move-object v1, v1

    .line 2236772
    sget-object v3, LX/FPn;->USER_CENTERED:LX/FPn;

    if-eq v1, v3, :cond_1

    .line 2236773
    iget-object v1, p0, LX/FP4;->i:LX/FPn;

    move-object v1, v1

    .line 2236774
    sget-object v3, LX/FPn;->CITY:LX/FPn;

    if-ne v1, v3, :cond_2

    .line 2236775
    :cond_1
    const/16 v0, 0xa

    .line 2236776
    :cond_2
    iget-object v1, p0, LX/FP4;->j:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-object v3, v1

    .line 2236777
    iget-object v1, v3, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    move-object v1, v1

    .line 2236778
    if-eqz v1, :cond_a

    iget v1, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_a

    const-string v1, "true"

    .line 2236779
    :goto_1
    new-instance v4, LX/CQD;

    invoke-direct {v4}, LX/CQD;-><init>()V

    const-string v5, "search_query"

    .line 2236780
    iget-object v6, p0, LX/FP4;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    if-eqz v6, :cond_3

    iget-object v6, p0, LX/FP4;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236781
    iget-object v7, v6, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    move-object v6, v7

    .line 2236782
    if-nez v6, :cond_c

    .line 2236783
    :cond_3
    const-string v6, ""

    .line 2236784
    :goto_2
    move-object v6, v6

    .line 2236785
    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "restrict_region"

    .line 2236786
    iget-boolean v6, p0, LX/FP4;->g:Z

    move v6, v6

    .line 2236787
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v4

    const-string v5, "orderby"

    .line 2236788
    iget-object v6, v3, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    move-object v6, v6

    .line 2236789
    sget-object v7, LX/FP9;->a:[I

    invoke-virtual {v6}, LX/FPo;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 2236790
    const/4 v7, 0x0

    :goto_3
    move-object v6, v7

    .line 2236791
    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "open_now"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "cursor_token"

    .line 2236792
    iget-object v5, p0, LX/FP4;->h:Ljava/lang/String;

    move-object v5, v5

    .line 2236793
    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "limit"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "friends_who_visited_count"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "friendRecommendationsCount"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_place_photos"

    .line 2236794
    iget-boolean v4, p0, LX/FP4;->k:Z

    move v4, v4

    .line 2236795
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    .line 2236796
    iget-object v1, v3, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    move-object v1, v1

    .line 2236797
    if-eqz v1, :cond_4

    .line 2236798
    iget-object v4, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;->a:LX/0Px;

    move-object v1, v4

    .line 2236799
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2236800
    const-string v4, "price_ranges"

    invoke-virtual {v0, v4, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v0

    .line 2236801
    :cond_4
    iget-object v1, v3, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    move-object v1, v1

    .line 2236802
    if-eqz v1, :cond_5

    .line 2236803
    iget-object v3, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    move-object v1, v3

    .line 2236804
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2236805
    const-string v3, "rich_attributes"

    invoke-virtual {v0, v3, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v0

    .line 2236806
    :cond_5
    iget-object v1, p0, LX/FP4;->d:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-object v1, v1

    .line 2236807
    if-eqz v1, :cond_6

    .line 2236808
    const-string v3, "north"

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "west"

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;->d()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "south"

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;->c()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "east"

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 2236809
    :cond_6
    iget-object v1, p0, LX/FP4;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2236810
    if-eqz v1, :cond_7

    .line 2236811
    const-string v1, "location_id"

    .line 2236812
    iget-object v3, p0, LX/FP4;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2236813
    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    .line 2236814
    :cond_7
    iget-object v1, p0, LX/FP4;->b:Landroid/location/Location;

    move-object v1, v1

    .line 2236815
    if-eqz v1, :cond_8

    .line 2236816
    const-string v3, "latitude"

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "longitude"

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 2236817
    :cond_8
    iget-object v1, p0, LX/FP4;->a:Landroid/location/Location;

    move-object v1, v1

    .line 2236818
    if-eqz v1, :cond_9

    .line 2236819
    const-string v3, "user_latitude"

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "user_longitude"

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 2236820
    :cond_9
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    .line 2236821
    if-eqz v0, :cond_b

    check-cast v0, LX/CQD;

    goto/16 :goto_0

    .line 2236822
    :cond_a
    const-string v1, "false"

    goto/16 :goto_1

    :cond_b
    move-object v0, v2

    .line 2236823
    goto/16 :goto_0

    :cond_c
    iget-object v6, p0, LX/FP4;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236824
    iget-object v7, v6, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    move-object v6, v7

    .line 2236825
    goto/16 :goto_2

    .line 2236826
    :pswitch_0
    const-string v7, "popularity"

    goto/16 :goto_3

    .line 2236827
    :pswitch_1
    const-string v7, "overall_rating"

    goto/16 :goto_3

    .line 2236828
    :pswitch_2
    const-string v7, "relevance"

    goto/16 :goto_3

    .line 2236829
    :pswitch_3
    const-string v7, "distance"

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
