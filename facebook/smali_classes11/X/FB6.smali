.class public LX/FB6;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FB6;


# direct methods
.method public constructor <init>(Landroid/content/ComponentName;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/katana/login/LogoutActivityComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2208709
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2208710
    const-string v0, "otp?id={%s}&pass={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "pass"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/FB5;

    invoke-direct {v1, p1}, LX/FB5;-><init>(Landroid/content/ComponentName;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2208711
    return-void
.end method

.method public static a(LX/0QB;)LX/FB6;
    .locals 4

    .prologue
    .line 2208712
    sget-object v0, LX/FB6;->a:LX/FB6;

    if-nez v0, :cond_1

    .line 2208713
    const-class v1, LX/FB6;

    monitor-enter v1

    .line 2208714
    :try_start_0
    sget-object v0, LX/FB6;->a:LX/FB6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2208715
    if-eqz v2, :cond_0

    .line 2208716
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2208717
    new-instance p0, LX/FB6;

    invoke-static {v0}, LX/2lQ;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-direct {p0, v3}, LX/FB6;-><init>(Landroid/content/ComponentName;)V

    .line 2208718
    move-object v0, p0

    .line 2208719
    sput-object v0, LX/FB6;->a:LX/FB6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2208720
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2208721
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2208722
    :cond_1
    sget-object v0, LX/FB6;->a:LX/FB6;

    return-object v0

    .line 2208723
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2208724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
