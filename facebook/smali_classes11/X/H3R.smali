.class public LX/H3R;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2420983
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/H3R;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2420984
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 2420986
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2420987
    const v0, 0x7f030bce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2420988
    invoke-virtual {p0}, LX/H3R;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2420989
    sget-object v1, LX/H3S;->j:LX/H3S;

    if-nez v1, :cond_0

    .line 2420990
    new-instance v1, LX/H3S;

    invoke-direct {v1, v0}, LX/H3S;-><init>(Landroid/content/Context;)V

    sput-object v1, LX/H3S;->j:LX/H3S;

    .line 2420991
    :cond_0
    sget-object v1, LX/H3S;->j:LX/H3S;

    move-object v0, v1

    .line 2420992
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d67

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/H3S;->a:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v3}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d69

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/H3S;->c:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v3}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d68

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/H3S;->b:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v3}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d6a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/H3S;->d:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v3}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d6c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/H3S;->f:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v3}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d6d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/H3S;->g:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v3}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d6e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/H3S;->h:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v3}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const v2, 0x7f0d1d6b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, v0, LX/H3S;->e:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v0}, LX/H3R;->a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/H3R;->a:LX/0P1;

    .line 2420993
    iget-object v0, p0, LX/H3R;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2420994
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, LX/H3R;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/common/SearchSuggestion;

    iget-object v0, v0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    iget-object v0, v0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2420995
    :cond_1
    return-void
.end method

.method private static a(Lcom/facebook/nearby/common/NearbyTopic;)Lcom/facebook/nearby/common/SearchSuggestion;
    .locals 4

    .prologue
    .line 2420985
    new-instance v0, Lcom/facebook/nearby/common/SearchSuggestion;

    iget-object v1, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/facebook/nearby/common/SearchSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/common/NearbyTopic;)V

    return-object v0
.end method
