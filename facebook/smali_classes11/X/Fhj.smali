.class public LX/Fhj;
.super LX/Fhd;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fhj;


# instance fields
.field private final e:LX/Fhg;


# direct methods
.method public constructor <init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273642
    invoke-direct {p0, p1, p2, p3}, LX/Fhd;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2273643
    new-instance v0, LX/Fhg;

    const v1, 0x70021

    const-string v2, "GraphSearchRemoteKeywordSuggestionsTypeahead"

    invoke-direct {v0, v1, v2}, LX/Fhg;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, LX/Fhj;->e:LX/Fhg;

    .line 2273644
    return-void
.end method

.method public static a(LX/0QB;)LX/Fhj;
    .locals 6

    .prologue
    .line 2273646
    sget-object v0, LX/Fhj;->f:LX/Fhj;

    if-nez v0, :cond_1

    .line 2273647
    const-class v1, LX/Fhj;

    monitor-enter v1

    .line 2273648
    :try_start_0
    sget-object v0, LX/Fhj;->f:LX/Fhj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273649
    if-eqz v2, :cond_0

    .line 2273650
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273651
    new-instance p0, LX/Fhj;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/Fhj;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2273652
    move-object v0, p0

    .line 2273653
    sput-object v0, LX/Fhj;->f:LX/Fhj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273654
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273655
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273656
    :cond_1
    sget-object v0, LX/Fhj;->f:LX/Fhj;

    return-object v0

    .line 2273657
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273658
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/Fhg;
    .locals 1

    .prologue
    .line 2273645
    iget-object v0, p0, LX/Fhj;->e:LX/Fhg;

    return-object v0
.end method
