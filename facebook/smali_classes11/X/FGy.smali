.class public final LX/FGy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/6eR;",
        "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/Integer;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/FHf;

.field public final synthetic f:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZLjava/lang/Integer;Ljava/lang/String;LX/FHf;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2219679
    iput-object p1, p0, LX/FGy;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-boolean p3, p0, LX/FGy;->b:Z

    iput-object p4, p0, LX/FGy;->c:Ljava/lang/Integer;

    iput-object p5, p0, LX/FGy;->d:Ljava/lang/String;

    iput-object p6, p0, LX/FGy;->e:LX/FHf;

    iput-object p7, p0, LX/FGy;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219680
    check-cast p1, LX/6eR;

    .line 2219681
    iget-object v0, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219682
    sget-object v1, LX/6eR;->VALID:LX/6eR;

    if-eq p1, v1, :cond_1

    .line 2219683
    const/4 v1, 0x0

    .line 2219684
    iget-object v2, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->PHOTO:LX/2MK;

    if-ne v2, v3, :cond_6

    .line 2219685
    iget-object v1, p0, LX/FGy;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->D:LX/2Nx;

    iget-object v2, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2}, LX/2Nx;->c(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    .line 2219686
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 2219687
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    iget-object v2, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2219688
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2219689
    move-object v0, v0

    .line 2219690
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2219691
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2219692
    const-string v2, "mediaResource"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2219693
    const-string v0, "fullQualityImageUpload"

    iget-boolean v2, p0, LX/FGy;->b:Z

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2219694
    iget-object v0, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, LX/FGy;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2219695
    iget-object v0, p0, LX/FGy;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v2, v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 2219696
    const-string v2, "videoFullDuration"

    invoke-virtual {v0}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2219697
    :cond_2
    iget-object v0, p0, LX/FGy;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2219698
    const-string v0, "originalFbid"

    iget-object v2, p0, LX/FGy;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2219699
    :cond_3
    const-string v0, "mediaUploadType"

    iget-object v2, p0, LX/FGy;->e:LX/FHf;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2219700
    iget-object v0, p0, LX/FGy;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v2, p0, LX/FGy;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    const/4 v4, 0x0

    .line 2219701
    const-string v3, "media_upload"

    .line 2219702
    sget-object v5, LX/2MK;->PHOTO:LX/2MK;

    if-ne v2, v5, :cond_5

    .line 2219703
    iget-object v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->K:LX/0Uh;

    const/16 p1, 0x17a

    invoke-virtual {v5, p1, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2219704
    iget-boolean v3, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->O:Z

    if-eqz v3, :cond_7

    const-string v3, "photo_upload_parallel"

    .line 2219705
    :goto_1
    iget-boolean v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->O:Z

    if-nez v5, :cond_4

    const/4 v4, 0x1

    :cond_4
    iput-boolean v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->O:Z

    .line 2219706
    :cond_5
    move-object v0, v3

    .line 2219707
    iget-object v2, p0, LX/FGy;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v3, p0, LX/FGy;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v2, v3, v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2219708
    :cond_6
    iget-object v2, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    if-ne v2, v3, :cond_0

    .line 2219709
    iget-object v1, p0, LX/FGy;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->E:LX/2OB;

    iget-object v2, p0, LX/FGy;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v1, v2}, LX/2OB;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;

    move-result-object v1

    goto/16 :goto_0

    .line 2219710
    :cond_7
    const-string v3, "photo_upload"

    goto :goto_1
.end method
