.class public final LX/H7Y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 2430834
    const/16 v24, 0x0

    .line 2430835
    const-wide/16 v22, 0x0

    .line 2430836
    const/16 v21, 0x0

    .line 2430837
    const/16 v20, 0x0

    .line 2430838
    const/16 v19, 0x0

    .line 2430839
    const/16 v18, 0x0

    .line 2430840
    const/16 v17, 0x0

    .line 2430841
    const/16 v16, 0x0

    .line 2430842
    const/4 v15, 0x0

    .line 2430843
    const/4 v14, 0x0

    .line 2430844
    const/4 v13, 0x0

    .line 2430845
    const/4 v12, 0x0

    .line 2430846
    const/4 v11, 0x0

    .line 2430847
    const/4 v10, 0x0

    .line 2430848
    const/4 v9, 0x0

    .line 2430849
    const/4 v8, 0x0

    .line 2430850
    const/4 v7, 0x0

    .line 2430851
    const/4 v6, 0x0

    .line 2430852
    const/4 v5, 0x0

    .line 2430853
    const/4 v4, 0x0

    .line 2430854
    const/4 v3, 0x0

    .line 2430855
    const/4 v2, 0x0

    .line 2430856
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_18

    .line 2430857
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2430858
    const/4 v2, 0x0

    .line 2430859
    :goto_0
    return v2

    .line 2430860
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_11

    .line 2430861
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2430862
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2430863
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 2430864
    const-string v6, "coupon_claim_location"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2430865
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 2430866
    :cond_1
    const-string v6, "expiration_date"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2430867
    const/4 v2, 0x1

    .line 2430868
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2430869
    :cond_2
    const-string v6, "filtered_claim_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2430870
    const/4 v2, 0x1

    .line 2430871
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v12, v2

    move/from16 v26, v6

    goto :goto_1

    .line 2430872
    :cond_3
    const-string v6, "has_viewer_claimed"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2430873
    const/4 v2, 0x1

    .line 2430874
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v25, v6

    goto :goto_1

    .line 2430875
    :cond_4
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2430876
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 2430877
    :cond_5
    const-string v6, "is_active"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2430878
    const/4 v2, 0x1

    .line 2430879
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v23, v6

    goto/16 :goto_1

    .line 2430880
    :cond_6
    const-string v6, "is_expired"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2430881
    const/4 v2, 0x1

    .line 2430882
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move/from16 v22, v6

    goto/16 :goto_1

    .line 2430883
    :cond_7
    const-string v6, "is_stopped"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2430884
    const/4 v2, 0x1

    .line 2430885
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move/from16 v21, v6

    goto/16 :goto_1

    .line 2430886
    :cond_8
    const-string v6, "message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2430887
    invoke-static/range {p0 .. p1}, LX/H7W;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 2430888
    :cond_9
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2430889
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2430890
    :cond_a
    const-string v6, "owning_page"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2430891
    invoke-static/range {p0 .. p1}, LX/H7l;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2430892
    :cond_b
    const-string v6, "photo"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2430893
    invoke-static/range {p0 .. p1}, LX/H7X;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2430894
    :cond_c
    const-string v6, "redemption_code"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2430895
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2430896
    :cond_d
    const-string v6, "redemption_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2430897
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2430898
    :cond_e
    const-string v6, "terms"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2430899
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2430900
    :cond_f
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 2430901
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2430902
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2430903
    :cond_11
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2430904
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2430905
    if-eqz v3, :cond_12

    .line 2430906
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2430907
    :cond_12
    if-eqz v12, :cond_13

    .line 2430908
    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2430909
    :cond_13
    if-eqz v11, :cond_14

    .line 2430910
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2430911
    :cond_14
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2430912
    if-eqz v10, :cond_15

    .line 2430913
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2430914
    :cond_15
    if-eqz v9, :cond_16

    .line 2430915
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2430916
    :cond_16
    if-eqz v8, :cond_17

    .line 2430917
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2430918
    :cond_17
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2430919
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2430920
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2430921
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2430922
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2430923
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2430924
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2430925
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2430926
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_18
    move/from16 v25, v20

    move/from16 v26, v21

    move/from16 v20, v15

    move/from16 v21, v16

    move v15, v10

    move/from16 v16, v11

    move v10, v4

    move v11, v5

    move-wide/from16 v4, v22

    move/from16 v22, v17

    move/from16 v23, v18

    move/from16 v18, v13

    move/from16 v17, v12

    move v13, v8

    move v12, v6

    move v8, v2

    move/from16 v28, v14

    move v14, v9

    move v9, v3

    move v3, v7

    move/from16 v7, v24

    move/from16 v24, v19

    move/from16 v19, v28

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2430927
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2430928
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2430929
    if-eqz v0, :cond_0

    .line 2430930
    const-string v0, "coupon_claim_location"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430931
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430932
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2430933
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 2430934
    const-string v2, "expiration_date"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430935
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2430936
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2430937
    if-eqz v0, :cond_2

    .line 2430938
    const-string v1, "filtered_claim_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430939
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2430940
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2430941
    if-eqz v0, :cond_3

    .line 2430942
    const-string v1, "has_viewer_claimed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430943
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2430944
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2430945
    if-eqz v0, :cond_4

    .line 2430946
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430947
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430948
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2430949
    if-eqz v0, :cond_5

    .line 2430950
    const-string v1, "is_active"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430951
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2430952
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2430953
    if-eqz v0, :cond_6

    .line 2430954
    const-string v1, "is_expired"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430955
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2430956
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2430957
    if-eqz v0, :cond_7

    .line 2430958
    const-string v1, "is_stopped"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430959
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2430960
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2430961
    if-eqz v0, :cond_8

    .line 2430962
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430963
    invoke-static {p0, v0, p2}, LX/H7W;->a(LX/15i;ILX/0nX;)V

    .line 2430964
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2430965
    if-eqz v0, :cond_9

    .line 2430966
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430967
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430968
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2430969
    if-eqz v0, :cond_a

    .line 2430970
    const-string v1, "owning_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430971
    invoke-static {p0, v0, p2, p3}, LX/H7l;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2430972
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2430973
    if-eqz v0, :cond_b

    .line 2430974
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430975
    invoke-static {p0, v0, p2, p3}, LX/H7X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2430976
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2430977
    if-eqz v0, :cond_c

    .line 2430978
    const-string v1, "redemption_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430979
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430980
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2430981
    if-eqz v0, :cond_d

    .line 2430982
    const-string v1, "redemption_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430983
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430984
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2430985
    if-eqz v0, :cond_e

    .line 2430986
    const-string v1, "terms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430987
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430988
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2430989
    if-eqz v0, :cond_f

    .line 2430990
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430991
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430992
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2430993
    return-void
.end method
