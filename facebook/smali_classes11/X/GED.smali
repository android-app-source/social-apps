.class public final LX/GED;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/GEF;


# direct methods
.method public constructor <init>(LX/GEF;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2331216
    iput-object p1, p0, LX/GED;->c:LX/GEF;

    iput-object p2, p0, LX/GED;->a:Ljava/lang/String;

    iput-object p3, p0, LX/GED;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2331217
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v10, 0x0

    .line 2331218
    if-eqz p1, :cond_0

    .line 2331219
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331220
    if-eqz v0, :cond_0

    .line 2331221
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331222
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v10

    .line 2331223
    :goto_0
    return-object v0

    .line 2331224
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331225
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v1

    .line 2331226
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v0

    invoke-static {v0}, LX/GG6;->a(Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)LX/GGB;

    move-result-object v2

    .line 2331227
    new-instance v0, LX/GGN;

    invoke-direct {v0}, LX/GGN;-><init>()V

    .line 2331228
    iput-object v1, v0, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2331229
    move-object v3, v0

    .line 2331230
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331231
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    .line 2331232
    iput-object v0, v3, LX/GGN;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 2331233
    move-object v3, v3

    .line 2331234
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331235
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    .line 2331236
    iput-object v0, v3, LX/GGN;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2331237
    move-object v0, v3

    .line 2331238
    iget-object v3, p0, LX/GED;->a:Ljava/lang/String;

    iget-object v4, p0, LX/GED;->b:Ljava/lang/String;

    .line 2331239
    new-instance v5, LX/GGK;

    invoke-direct {v5}, LX/GGK;-><init>()V

    .line 2331240
    iput-object v3, v5, LX/GGK;->c:Ljava/lang/String;

    .line 2331241
    move-object v5, v5

    .line 2331242
    iput-object v4, v5, LX/GGK;->k:Ljava/lang/String;

    .line 2331243
    move-object v5, v5

    .line 2331244
    invoke-virtual {v5}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v5

    move-object v3, v5

    .line 2331245
    iput-object v3, v0, LX/GGN;->a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2331246
    move-object v0, v0

    .line 2331247
    invoke-virtual {v0}, LX/GGN;->b()Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v9

    .line 2331248
    invoke-static {v1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;)LX/0Px;

    move-result-object v0

    .line 2331249
    iput-object v0, v9, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    .line 2331250
    iput-object v2, v9, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    .line 2331251
    if-eqz v1, :cond_2

    .line 2331252
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->z()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v0

    .line 2331253
    iput-object v0, v9, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 2331254
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->v()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2331255
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->v()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    .line 2331256
    iput-object v0, v9, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    .line 2331257
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331258
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2331259
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331260
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;

    move-result-object v0

    .line 2331261
    new-instance v1, Lcom/facebook/adinterfaces/model/EventSpecModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RSVP"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;->n()J

    move-result-wide v6

    iget-object v8, p0, LX/GED;->c:LX/GEF;

    invoke-static {v8, v0}, LX/GEF;->a$redex0(LX/GEF;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Lcom/facebook/adinterfaces/model/EventSpecModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V

    .line 2331262
    iput-object v1, v9, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    .line 2331263
    iput-object v10, v9, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2331264
    :cond_4
    move-object v0, v9

    .line 2331265
    goto/16 :goto_0
.end method
