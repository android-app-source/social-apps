.class public LX/GVC;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/GVA;


# instance fields
.field public a:LX/GVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/fbui/facepile/FacepileView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2358705
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2358706
    const/4 p1, 0x1

    .line 2358707
    const-class v0, LX/GVC;

    invoke-static {v0, p0}, LX/GVC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2358708
    const v0, 0x7f030160

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2358709
    invoke-virtual {p0, p1}, LX/GVC;->setOrientation(I)V

    .line 2358710
    invoke-virtual {p0, p1}, LX/GVC;->setGravity(I)V

    .line 2358711
    const v0, 0x7f0d065e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/GVC;->b:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2358712
    const v0, 0x7f0d065f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GVC;->c:Landroid/widget/TextView;

    .line 2358713
    const v0, 0x7f0d0660

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/GVC;->d:Landroid/widget/Button;

    .line 2358714
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GVC;

    invoke-static {p0}, LX/GVF;->a(LX/0QB;)LX/GVF;

    move-result-object p0

    check-cast p0, LX/GVF;

    iput-object p0, p1, LX/GVC;->a:LX/GVF;

    return-void
.end method


# virtual methods
.method public getDesignName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2358704
    const-string v0, "facepile"

    return-object v0
.end method
