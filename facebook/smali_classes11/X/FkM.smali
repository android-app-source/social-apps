.class public LX/FkM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/FkL;

.field public d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

.field private e:Landroid/support/v4/app/DialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2278497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278498
    return-void
.end method

.method public static a(LX/0QB;)LX/FkM;
    .locals 1

    .prologue
    .line 2278499
    invoke-static {p0}, LX/FkM;->b(LX/0QB;)LX/FkM;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/FkM;
    .locals 3

    .prologue
    .line 2278500
    new-instance v2, LX/FkM;

    invoke-direct {v2}, LX/FkM;-><init>()V

    .line 2278501
    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    .line 2278502
    iput-object v0, v2, LX/FkM;->a:LX/0tX;

    iput-object v1, v2, LX/FkM;->b:Ljava/util/concurrent/ExecutorService;

    .line 2278503
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2278490
    iget-object v0, p0, LX/FkM;->e:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FkM;->e:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2278491
    iget-object v0, p0, LX/FkM;->e:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2278492
    :cond_0
    return-void
.end method

.method public final a(LX/0gc;)V
    .locals 3

    .prologue
    .line 2278493
    iget-object v0, p0, LX/FkM;->e:Landroid/support/v4/app/DialogFragment;

    if-nez v0, :cond_0

    .line 2278494
    const v0, 0x7f083326

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iput-object v0, p0, LX/FkM;->e:Landroid/support/v4/app/DialogFragment;

    .line 2278495
    :cond_0
    iget-object v0, p0, LX/FkM;->e:Landroid/support/v4/app/DialogFragment;

    const-string v1, "progress_dialog"

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2278496
    return-void
.end method
