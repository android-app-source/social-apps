.class public final LX/FOB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;",
        ">;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2234374
    iput-object p1, p0, LX/FOB;->b:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    iput-object p2, p0, LX/FOB;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2234375
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2234376
    if-eqz p1, :cond_0

    .line 2234377
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2234378
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2234379
    :goto_0
    return-object v0

    .line 2234380
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2234381
    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2234382
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel$BlobAttachmentsModel;

    .line 2234383
    iget-object v5, p0, LX/FOB;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel$BlobAttachmentsModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2234384
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel$BlobAttachmentsModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2234385
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 2234386
    goto :goto_0
.end method
