.class public LX/Fbb;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbb;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260523
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2260524
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbb;
    .locals 3

    .prologue
    .line 2260525
    sget-object v0, LX/Fbb;->a:LX/Fbb;

    if-nez v0, :cond_1

    .line 2260526
    const-class v1, LX/Fbb;

    monitor-enter v1

    .line 2260527
    :try_start_0
    sget-object v0, LX/Fbb;->a:LX/Fbb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260528
    if-eqz v2, :cond_0

    .line 2260529
    :try_start_1
    new-instance v0, LX/Fbb;

    invoke-direct {v0}, LX/Fbb;-><init>()V

    .line 2260530
    move-object v0, v0

    .line 2260531
    sput-object v0, LX/Fbb;->a:LX/Fbb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260532
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260533
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260534
    :cond_1
    sget-object v0, LX/Fbb;->a:LX/Fbb;

    return-object v0

    .line 2260535
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260536
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .locals 5

    .prologue
    .line 2260537
    if-nez p0, :cond_0

    .line 2260538
    const/4 v0, 0x0

    .line 2260539
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/8eO;

    invoke-direct {v0}, LX/8eO;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2260540
    iput-object v1, v0, LX/8eO;->a:Ljava/lang/String;

    .line 2260541
    move-object v0, v0

    .line 2260542
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2260543
    iput-object v1, v0, LX/8eO;->d:Ljava/lang/String;

    .line 2260544
    move-object v0, v0

    .line 2260545
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 2260546
    iput-object v1, v0, LX/8eO;->e:Ljava/lang/String;

    .line 2260547
    move-object v0, v0

    .line 2260548
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;->fC_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel$MediaModel;

    move-result-object v1

    .line 2260549
    if-nez v1, :cond_1

    .line 2260550
    const/4 v2, 0x0

    .line 2260551
    :goto_1
    move-object v1, v2

    .line 2260552
    iput-object v1, v0, LX/8eO;->f:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    .line 2260553
    move-object v0, v0

    .line 2260554
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;->fB_()Ljava/lang/String;

    move-result-object v1

    .line 2260555
    iput-object v1, v0, LX/8eO;->g:Ljava/lang/String;

    .line 2260556
    move-object v0, v0

    .line 2260557
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;->c()J

    move-result-wide v2

    .line 2260558
    iput-wide v2, v0, LX/8eO;->c:J

    .line 2260559
    move-object v0, v0

    .line 2260560
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;->b()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 2260561
    iput-object v1, v0, LX/8eO;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260562
    move-object v0, v0

    .line 2260563
    invoke-virtual {v0}, LX/8eO;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    goto :goto_0

    .line 2260564
    :cond_1
    new-instance v2, LX/8eP;

    invoke-direct {v2}, LX/8eP;-><init>()V

    .line 2260565
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel$MediaModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2260566
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel$MediaModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2260567
    iput-object v3, v2, LX/8eP;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2260568
    :cond_2
    invoke-virtual {v2}, LX/8eP;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2260569
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->d()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;

    move-result-object v1

    .line 2260570
    if-nez v1, :cond_1

    .line 2260571
    :cond_0
    :goto_0
    return-object v0

    .line 2260572
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 2260573
    if-eqz v2, :cond_0

    .line 2260574
    new-instance v0, LX/8dV;

    invoke-direct {v0}, LX/8dV;-><init>()V

    new-instance v3, LX/8eW;

    invoke-direct {v3}, LX/8eW;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v4

    invoke-static {v4}, LX/Fbb;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v4

    .line 2260575
    iput-object v4, v3, LX/8eW;->a:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 2260576
    move-object v3, v3

    .line 2260577
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->b()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v1

    invoke-static {v1}, LX/Fbb;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    .line 2260578
    iput-object v1, v3, LX/8eW;->b:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 2260579
    move-object v1, v3

    .line 2260580
    invoke-virtual {v1}, LX/8eW;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v1

    .line 2260581
    iput-object v1, v0, LX/8dV;->b:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    .line 2260582
    move-object v0, v0

    .line 2260583
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, -0x1c2adf8

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2260584
    iput-object v3, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260585
    move-object v1, v1

    .line 2260586
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->jP()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    if-nez v6, :cond_2

    .line 2260587
    const/4 v6, 0x0

    .line 2260588
    :goto_1
    move-object v3, v6

    .line 2260589
    iput-object v3, v1, LX/8dX;->aR:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 2260590
    move-object v1, v1

    .line 2260591
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kh()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v3

    if-nez v3, :cond_3

    .line 2260592
    const/4 v3, 0x0

    .line 2260593
    :goto_2
    move-object v3, v3

    .line 2260594
    iput-object v3, v1, LX/8dX;->aT:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 2260595
    move-object v1, v1

    .line 2260596
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v4

    .line 2260597
    iput-wide v4, v1, LX/8dX;->t:J

    .line 2260598
    move-object v1, v1

    .line 2260599
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v2

    .line 2260600
    iput-object v2, v1, LX/8dX;->aV:Ljava/lang/String;

    .line 2260601
    move-object v1, v1

    .line 2260602
    invoke-virtual {v1}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v1

    .line 2260603
    iput-object v1, v0, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2260604
    move-object v0, v0

    .line 2260605
    goto :goto_0

    :cond_2
    new-instance v6, LX/8f1;

    invoke-direct {v6}, LX/8f1;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->jP()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v8

    .line 2260606
    iput-wide v8, v6, LX/8f1;->c:J

    .line 2260607
    move-object v6, v6

    .line 2260608
    invoke-virtual {v6}, LX/8f1;->a()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v6

    goto :goto_1

    :cond_3
    new-instance v3, LX/A21;

    invoke-direct {v3}, LX/A21;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kh()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->q()Ljava/lang/String;

    move-result-object v4

    .line 2260609
    iput-object v4, v3, LX/A21;->h:Ljava/lang/String;

    .line 2260610
    move-object v3, v3

    .line 2260611
    invoke-virtual {v3}, LX/A21;->a()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v3

    goto :goto_2
.end method
