.class public final LX/F2g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DZN;


# instance fields
.field public final synthetic a:LX/F2r;


# direct methods
.method public constructor <init>(LX/F2r;)V
    .locals 0

    .prologue
    .line 2193244
    iput-object p1, p0, LX/F2g;->a:LX/F2r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2193230
    iget-object v0, p0, LX/F2g;->a:LX/F2r;

    iget-object v0, v0, LX/F2r;->b:Landroid/content/res/Resources;

    const v1, 0x7f0831a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 8

    .prologue
    .line 2193231
    iget-object v0, p0, LX/F2g;->a:LX/F2r;

    iget-object v1, v0, LX/F2r;->a:LX/F2L;

    iget-object v0, p0, LX/F2g;->a:LX/F2r;

    iget-object v0, v0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->ADMIN_ONLY:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    :goto_0
    iget-object v3, p0, LX/F2g;->a:LX/F2r;

    iget-object v3, v3, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/F2L;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V

    .line 2193232
    return-void

    .line 2193233
    :cond_0
    iget-object v0, p0, LX/F2g;->a:LX/F2r;

    const/4 p1, 0x1

    .line 2193234
    iget-object v3, v0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->u()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x264ce754

    invoke-static {v4, v3, p1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    .line 2193235
    if-eqz v3, :cond_2

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    .line 2193236
    :goto_1
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2193237
    const-class v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-virtual {v5, v3, p1, v6, v7}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 2193238
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->ADMIN_ONLY:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    if-eq v3, v5, :cond_1

    .line 2193239
    :goto_2
    move-object v0, v3

    .line 2193240
    goto :goto_0

    .line 2193241
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_1

    .line 2193242
    :cond_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2193243
    iget-object v0, p0, LX/F2g;->a:LX/F2r;

    iget-object v0, v0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->ordinal()I

    move-result v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->ADMIN_ONLY:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
