.class public LX/FbL;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259990
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2259991
    return-void
.end method

.method public static a(LX/0QB;)LX/FbL;
    .locals 3

    .prologue
    .line 2259992
    const-class v1, LX/FbL;

    monitor-enter v1

    .line 2259993
    :try_start_0
    sget-object v0, LX/FbL;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2259994
    sput-object v2, LX/FbL;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2259995
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2259996
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2259997
    new-instance v0, LX/FbL;

    invoke-direct {v0}, LX/FbL;-><init>()V

    .line 2259998
    move-object v0, v0

    .line 2259999
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2260000
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FbL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260001
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2260002
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260003
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2260004
    if-nez v0, :cond_0

    .line 2260005
    const/4 v0, 0x0

    .line 2260006
    :goto_0
    return-object v0

    .line 2260007
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2260008
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2260009
    move-object v1, v1

    .line 2260010
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, -0x70671f61

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2260011
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260012
    move-object v1, v1

    .line 2260013
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lx()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 2260014
    if-nez v2, :cond_1

    .line 2260015
    const/4 v3, 0x0

    .line 2260016
    :goto_1
    move-object v2, v3

    .line 2260017
    iput-object v2, v1, LX/8dX;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 2260018
    move-object v1, v1

    .line 2260019
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2260020
    if-nez v2, :cond_2

    .line 2260021
    const/4 v3, 0x0

    .line 2260022
    :goto_2
    move-object v2, v3

    .line 2260023
    iput-object v2, v1, LX/8dX;->M:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 2260024
    move-object v1, v1

    .line 2260025
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2260026
    if-nez v2, :cond_3

    .line 2260027
    const/4 v3, 0x0

    .line 2260028
    :goto_3
    move-object v2, v3

    .line 2260029
    iput-object v2, v1, LX/8dX;->N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260030
    move-object v1, v1

    .line 2260031
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->l()Ljava/lang/String;

    move-result-object v0

    .line 2260032
    iput-object v0, v1, LX/8dX;->c:Ljava/lang/String;

    .line 2260033
    move-object v0, v1

    .line 2260034
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2260035
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2260036
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2260037
    move-object v0, v1

    .line 2260038
    goto :goto_0

    :cond_1
    new-instance v3, LX/8hA;

    invoke-direct {v3}, LX/8hA;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p0

    .line 2260039
    iput-object p0, v3, LX/8hA;->b:Ljava/lang/String;

    .line 2260040
    move-object v3, v3

    .line 2260041
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p0

    .line 2260042
    iput-object p0, v3, LX/8hA;->c:Ljava/lang/String;

    .line 2260043
    move-object v3, v3

    .line 2260044
    invoke-virtual {v3}, LX/8hA;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v3, LX/8db;

    invoke-direct {v3}, LX/8db;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    .line 2260045
    iput p0, v3, LX/8db;->c:I

    .line 2260046
    move-object v3, v3

    .line 2260047
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p0

    .line 2260048
    iput p0, v3, LX/8db;->a:I

    .line 2260049
    move-object v3, v3

    .line 2260050
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    .line 2260051
    iput-object p0, v3, LX/8db;->b:Ljava/lang/String;

    .line 2260052
    move-object v3, v3

    .line 2260053
    invoke-virtual {v3}, LX/8db;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v3

    goto :goto_2

    :cond_3
    invoke-static {v2}, LX/9JZ;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1Fb;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    goto :goto_3
.end method
