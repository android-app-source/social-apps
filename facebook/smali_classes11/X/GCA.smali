.class public LX/GCA;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements LX/2ht;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GB8;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2328167
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2328168
    iput-object p1, p0, LX/GCA;->a:Landroid/content/res/Resources;

    .line 2328169
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/GCA;->b:Ljava/util/List;

    .line 2328170
    const/4 v0, 0x0

    iput v0, p0, LX/GCA;->c:I

    .line 2328171
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 2328151
    if-nez p0, :cond_0

    .line 2328152
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030019

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2328153
    :goto_0
    check-cast v0, Landroid/widget/TextView;

    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2328154
    iget-object v0, p0, LX/GCA;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2328155
    const v0, -0x5283def2

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2328156
    return-void
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2328157
    invoke-static {p2, p3}, LX/GCA;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object v0

    .line 2328158
    const v1, 0x7f08351b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2328159
    iget v1, p0, LX/GCA;->c:I

    if-nez v1, :cond_0

    .line 2328160
    const/4 p3, 0x0

    .line 2328161
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 p1, -0x1

    const/4 p2, -0x2

    invoke-direct {v1, p1, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2328162
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->measure(II)V

    .line 2328163
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v1, v1

    .line 2328164
    iput v1, p0, LX/GCA;->c:I

    .line 2328165
    :cond_0
    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2328172
    iget-object v0, p0, LX/GCA;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2328166
    iget v0, p0, LX/GCA;->c:I

    return v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 2328149
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2328150
    iget-object v0, p0, LX/GCA;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2328124
    iget-object v0, p0, LX/GCA;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2328148
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2328125
    iget-object v0, p0, LX/GCA;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GB8;

    invoke-interface {v0}, LX/GB8;->l()LX/GBB;

    move-result-object v0

    invoke-virtual {v0}, LX/GBB;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2328126
    invoke-virtual {p0, p1}, LX/GCA;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GB8;

    .line 2328127
    sget-object v1, LX/GC9;->a:[I

    invoke-interface {v0}, LX/GB8;->l()LX/GBB;

    move-result-object v2

    invoke-virtual {v2}, LX/GBB;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2328128
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2328129
    :pswitch_0
    invoke-static {p2, p3}, LX/GCA;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object v0

    .line 2328130
    const v1, 0x7f08351b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2328131
    :goto_0
    return-object v0

    .line 2328132
    :pswitch_1
    if-nez p2, :cond_1

    new-instance p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    :goto_1
    move-object v1, p2

    .line 2328133
    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    .line 2328134
    sget-object v2, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2328135
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2328136
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2328137
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2328138
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2328139
    move-object v0, v1

    .line 2328140
    goto :goto_0

    .line 2328141
    :pswitch_2
    if-nez p2, :cond_0

    .line 2328142
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030020

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2328143
    :cond_0
    move-object v0, p2

    .line 2328144
    goto :goto_0

    :cond_1
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2328145
    invoke-static {}, LX/GBB;->values()[LX/GBB;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 2328146
    iget-object v0, p0, LX/GCA;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GB8;

    invoke-interface {v0}, LX/GB8;->m()Z

    move-result v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2328147
    const/4 v0, 0x0

    return v0
.end method
