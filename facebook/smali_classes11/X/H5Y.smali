.class public LX/H5Y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Landroid/view/View$OnClickListener;

.field private static c:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2424531
    sput-object v0, LX/H5Y;->a:Ljava/lang/String;

    .line 2424532
    sput-object v0, LX/H5Y;->b:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2424534
    return-void
.end method

.method public static a(LX/0QB;)LX/H5Y;
    .locals 3

    .prologue
    .line 2424535
    const-class v1, LX/H5Y;

    monitor-enter v1

    .line 2424536
    :try_start_0
    sget-object v0, LX/H5Y;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424537
    sput-object v2, LX/H5Y;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424538
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424539
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2424540
    new-instance v0, LX/H5Y;

    invoke-direct {v0}, LX/H5Y;-><init>()V

    .line 2424541
    move-object v0, v0

    .line 2424542
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424543
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424544
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424545
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
