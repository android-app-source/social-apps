.class public LX/HB7;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/11S;

.field private final b:LX/HBP;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/content/Context;

.field private e:Landroid/view/View$OnClickListener;

.field private f:LX/HBN;

.field private g:Z


# direct methods
.method public constructor <init>(LX/HBP;LX/11S;Landroid/content/Context;LX/HBN;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p5    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2436445
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2436446
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HB7;->c:Ljava/util/List;

    .line 2436447
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HB7;->g:Z

    .line 2436448
    iput-object p1, p0, LX/HB7;->b:LX/HBP;

    .line 2436449
    iput-object p2, p0, LX/HB7;->a:LX/11S;

    .line 2436450
    iput-object p3, p0, LX/HB7;->d:Landroid/content/Context;

    .line 2436451
    iput-object p4, p0, LX/HB7;->f:LX/HBN;

    .line 2436452
    iput-object p5, p0, LX/HB7;->e:Landroid/view/View$OnClickListener;

    .line 2436453
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2436454
    sget-object v0, LX/HB6;->REQUEST:LX/HB6;

    invoke-virtual {v0}, LX/HB6;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2436455
    new-instance v0, LX/HBM;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030eca

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HBM;-><init>(Landroid/view/View;)V

    .line 2436456
    :goto_0
    return-object v0

    .line 2436457
    :cond_0
    sget-object v0, LX/HB6;->SPINNER:LX/HB6;

    invoke-virtual {v0}, LX/HB6;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2436458
    new-instance v0, LX/HB5;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030ed0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HB5;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2436459
    :cond_1
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2436460
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    .line 2436461
    invoke-virtual {p0, p2}, LX/HB7;->e(I)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    move-result-object v2

    .line 2436462
    if-eqz v2, :cond_1

    instance-of v0, p1, LX/HBM;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2436463
    check-cast v0, LX/HBM;

    .line 2436464
    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->n()Ljava/lang/String;

    move-result-object v1

    .line 2436465
    iget-object v3, v0, LX/HBM;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436466
    iget-object v1, p0, LX/HB7;->a:LX/11S;

    sget-object v3, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->k()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-interface {v1, v3, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2436467
    iget-object v3, v0, LX/HBM;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436468
    iget-object v1, p0, LX/HB7;->f:LX/HBN;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/HBN;->a(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v1

    .line 2436469
    if-nez v1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v1

    :cond_0
    const/4 v5, 0x4

    .line 2436470
    sget-object v3, LX/HBL;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2436471
    iget-object v3, v0, LX/HBM;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2436472
    iget-object v3, v0, LX/HBM;->l:Lcom/facebook/widget/CustomRelativeLayout;

    const v4, 0x7f0213be

    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomRelativeLayout;->setBackgroundResource(I)V

    .line 2436473
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2436474
    iget-object v3, v0, LX/HBM;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436475
    iget-object v1, p0, LX/HB7;->b:LX/HBP;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-virtual {v1, v4, v5}, LX/HBP;->b(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2436476
    iget-object v1, p0, LX/HB7;->b:LX/HBP;

    iget-object v3, p0, LX/HB7;->d:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-virtual {v1, v3, v4, v5}, LX/HBP;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 2436477
    iget-object v3, p0, LX/HB7;->b:LX/HBP;

    iget-object v4, p0, LX/HB7;->d:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-virtual {v3, v4, v6, v7}, LX/HBP;->a(Landroid/content/Context;J)I

    move-result v2

    .line 2436478
    iget-object v3, v0, LX/HBM;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436479
    iget-object v3, v0, LX/HBM;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2436480
    iget-object v3, v0, LX/HBM;->o:Lcom/facebook/resources/ui/FbTextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2436481
    iget-object v3, v0, LX/HBM;->p:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, v0, LX/HBM;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbTextView;->getPaddingLeft()I

    move-result v4

    iget-object v5, v0, LX/HBM;->l:Lcom/facebook/widget/CustomRelativeLayout;

    invoke-virtual {v5}, Lcom/facebook/widget/CustomRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0062

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iget-object v6, v0, LX/HBM;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbTextView;->getPaddingRight()I

    move-result v6

    iget-object v7, v0, LX/HBM;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7}, Lcom/facebook/resources/ui/FbTextView;->getPaddingBottom()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/facebook/resources/ui/FbTextView;->setPadding(IIII)V

    .line 2436482
    :goto_1
    iget-object v0, p0, LX/HB7;->e:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    .line 2436483
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/HB7;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2436484
    :cond_1
    return-void

    .line 2436485
    :cond_2
    iget-object v1, v0, LX/HBM;->o:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2436486
    goto :goto_1

    .line 2436487
    :pswitch_0
    iget-object v3, v0, LX/HBM;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2436488
    iget-object v3, v0, LX/HBM;->l:Lcom/facebook/widget/CustomRelativeLayout;

    const v4, 0x7f0213bf

    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomRelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 2436489
    :pswitch_1
    iget-object v3, v0, LX/HBM;->m:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2436490
    iget-object v3, v0, LX/HBM;->l:Lcom/facebook/widget/CustomRelativeLayout;

    const v4, 0x7f0213be

    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomRelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2436491
    iput-boolean p1, p0, LX/HB7;->g:Z

    .line 2436492
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2436493
    return-void
.end method

.method public final e(I)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2436494
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/HB7;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/HB7;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2436495
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/HB7;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2436496
    sget-object v0, LX/HB6;->REQUEST:LX/HB6;

    invoke-virtual {v0}, LX/HB6;->ordinal()I

    move-result v0

    .line 2436497
    :goto_0
    return v0

    .line 2436498
    :cond_0
    iget-boolean v0, p0, LX/HB7;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HB7;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2436499
    sget-object v0, LX/HB6;->SPINNER:LX/HB6;

    invoke-virtual {v0}, LX/HB6;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2436500
    :cond_1
    sget-object v0, LX/HB6;->UNKNOWN:LX/HB6;

    invoke-virtual {v0}, LX/HB6;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2436501
    iget-object v0, p0, LX/HB7;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-boolean v0, p0, LX/HB7;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
