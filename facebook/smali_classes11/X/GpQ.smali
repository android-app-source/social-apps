.class public LX/GpQ;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/ButtonBlockView;",
        "Lcom/facebook/instantshopping/model/data/ButtonBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/GoE;


# direct methods
.method public constructor <init>(LX/Gpx;)V
    .locals 2

    .prologue
    .line 2394957
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2394958
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/GpQ;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object p1

    check-cast p1, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v0

    check-cast v0, LX/Go4;

    iput-object p1, p0, LX/GpQ;->d:LX/Go7;

    iput-object v0, p0, LX/GpQ;->e:LX/Go4;

    .line 2394959
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 6

    .prologue
    .line 2394917
    check-cast p1, LX/Gou;

    .line 2394918
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2394919
    check-cast v0, LX/Gpx;

    .line 2394920
    iget-object v1, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v1}, LX/CHb;->d()LX/8Z4;

    move-result-object v1

    move-object v1, v1

    .line 2394921
    new-instance v2, LX/Cle;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object v2

    const v3, 0x7f0e0a16

    invoke-virtual {v2, v3}, LX/Cle;->a(I)LX/Cle;

    move-result-object v2

    invoke-virtual {v2}, LX/Cle;->a()LX/Clf;

    move-result-object v2

    move-object v1, v2

    .line 2394922
    invoke-interface {p1}, LX/GoZ;->e()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v2

    .line 2394923
    iget-object v3, v0, LX/Gpx;->f:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2394924
    iget-object v4, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v4

    .line 2394925
    invoke-virtual {v3, v1}, LX/CtG;->setText(LX/Clf;)V

    .line 2394926
    iget-object v3, v0, LX/Gpx;->f:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2394927
    iget-object v4, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v4

    .line 2394928
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/CtG;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2394929
    iget-object v3, v0, LX/Gpx;->c:LX/CIh;

    iget-object v4, v0, LX/Gpx;->f:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2394930
    iget-object v5, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v5

    .line 2394931
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;->CENTER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    invoke-virtual {v3, v4, v5, v2}, LX/CIh;->a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V

    .line 2394932
    iget-object v1, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v1}, LX/CHb;->c()LX/CHa;

    move-result-object v1

    move-object v1, v1

    .line 2394933
    invoke-interface {v1}, LX/CHa;->j()Ljava/lang/String;

    move-result-object v1

    .line 2394934
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2394935
    :goto_0
    invoke-virtual {p1}, LX/Gou;->getAction()LX/CHX;

    move-result-object v1

    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v2

    .line 2394936
    new-instance v3, LX/Gpw;

    invoke-direct {v3, v0, v2, v1}, LX/Gpw;-><init>(LX/Gpx;LX/GoE;LX/CHX;)V

    .line 2394937
    iget-object v4, v0, LX/Gpx;->f:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2394938
    iget-object v1, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v1

    .line 2394939
    invoke-virtual {v4, v3}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2394940
    iget-object v4, v0, LX/Gpx;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2394941
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    iput-object v1, p0, LX/GpQ;->f:LX/GoE;

    .line 2394942
    iget-object v1, p0, LX/GpQ;->f:LX/GoE;

    .line 2394943
    iget-object v2, v0, LX/Gpx;->a:LX/Go0;

    invoke-virtual {v2, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2394944
    iget-object v1, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v1}, LX/CHb;->jw_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v1}, LX/CHb;->jw_()LX/0Px;

    move-result-object v1

    :goto_1
    move-object v1, v1

    .line 2394945
    iget-object v2, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v2}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2394946
    const/4 v2, -0x1

    .line 2394947
    :goto_2
    move v2, v2

    .line 2394948
    iget-object v3, v0, LX/Gpx;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2394949
    if-eqz v1, :cond_4

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_COMPACT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v1, v4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2394950
    const/4 v4, -0x2

    .line 2394951
    :goto_3
    move v4, v4

    .line 2394952
    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2394953
    iget-object v3, v0, LX/Gpx;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1, v2}, LX/Gpx;->a(Landroid/content/Context;LX/0Px;I)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v4

    invoke-static {v3, v4}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2394954
    return-void

    .line 2394955
    :cond_0
    :try_start_0
    iget-object v2, v0, LX/Gpx;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-static {v3, v4}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setMinimumHeight(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2394956
    :catch_0
    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-object v2, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v2}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v2}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    goto :goto_2

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/Gou;->a:LX/CHc;

    invoke-interface {v3}, LX/CHb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    goto :goto_2

    :cond_4
    const/4 v4, -0x1

    goto :goto_3
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2394909
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 2394910
    iget-object v0, p0, LX/GpQ;->d:LX/Go7;

    const-string v1, "button_element_start"

    iget-object v2, p0, LX/GpQ;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2394911
    iget-object v0, p0, LX/GpQ;->e:LX/Go4;

    iget-object v1, p0, LX/GpQ;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2394912
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2394913
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 2394914
    iget-object v0, p0, LX/GpQ;->d:LX/Go7;

    const-string v1, "button_element_end"

    iget-object v2, p0, LX/GpQ;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2394915
    iget-object v0, p0, LX/GpQ;->e:LX/Go4;

    iget-object v1, p0, LX/GpQ;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2394916
    return-void
.end method
