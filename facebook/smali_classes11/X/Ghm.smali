.class public LX/Ghm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Ghm;


# instance fields
.field public final a:LX/Ght;

.field public final b:LX/Ghp;

.field public final c:LX/GiU;


# direct methods
.method public constructor <init>(LX/Ght;LX/Ghp;LX/GiU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2384806
    iput-object p1, p0, LX/Ghm;->a:LX/Ght;

    .line 2384807
    iput-object p2, p0, LX/Ghm;->b:LX/Ghp;

    .line 2384808
    iput-object p3, p0, LX/Ghm;->c:LX/GiU;

    .line 2384809
    return-void
.end method

.method public static a(LX/0QB;)LX/Ghm;
    .locals 6

    .prologue
    .line 2384810
    sget-object v0, LX/Ghm;->d:LX/Ghm;

    if-nez v0, :cond_1

    .line 2384811
    const-class v1, LX/Ghm;

    monitor-enter v1

    .line 2384812
    :try_start_0
    sget-object v0, LX/Ghm;->d:LX/Ghm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2384813
    if-eqz v2, :cond_0

    .line 2384814
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2384815
    new-instance p0, LX/Ghm;

    invoke-static {v0}, LX/Ght;->a(LX/0QB;)LX/Ght;

    move-result-object v3

    check-cast v3, LX/Ght;

    invoke-static {v0}, LX/Ghp;->a(LX/0QB;)LX/Ghp;

    move-result-object v4

    check-cast v4, LX/Ghp;

    invoke-static {v0}, LX/GiU;->a(LX/0QB;)LX/GiU;

    move-result-object v5

    check-cast v5, LX/GiU;

    invoke-direct {p0, v3, v4, v5}, LX/Ghm;-><init>(LX/Ght;LX/Ghp;LX/GiU;)V

    .line 2384816
    move-object v0, p0

    .line 2384817
    sput-object v0, LX/Ghm;->d:LX/Ghm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384818
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2384819
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2384820
    :cond_1
    sget-object v0, LX/Ghm;->d:LX/Ghm;

    return-object v0

    .line 2384821
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2384822
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
