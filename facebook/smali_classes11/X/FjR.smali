.class public LX/FjR;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FjR;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276875
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2276876
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2276877
    const-string v1, "source"

    sget-object v2, LX/8ci;->q:LX/8ci;

    invoke-virtual {v2}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2276878
    const-string v1, "live_sport_event/{%s}/{%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra_sport_query_live_page_id"

    const-string v3, "extra_sport_query_live_page_title"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2276879
    return-void
.end method

.method public static a(LX/0QB;)LX/FjR;
    .locals 3

    .prologue
    .line 2276880
    sget-object v0, LX/FjR;->a:LX/FjR;

    if-nez v0, :cond_1

    .line 2276881
    const-class v1, LX/FjR;

    monitor-enter v1

    .line 2276882
    :try_start_0
    sget-object v0, LX/FjR;->a:LX/FjR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2276883
    if-eqz v2, :cond_0

    .line 2276884
    :try_start_1
    new-instance v0, LX/FjR;

    invoke-direct {v0}, LX/FjR;-><init>()V

    .line 2276885
    move-object v0, v0

    .line 2276886
    sput-object v0, LX/FjR;->a:LX/FjR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276887
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2276888
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2276889
    :cond_1
    sget-object v0, LX/FjR;->a:LX/FjR;

    return-object v0

    .line 2276890
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2276891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
