.class public final enum LX/FHH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHH;

.field public static final enum cancelation:LX/FHH;

.field public static final enum failure:LX/FHH;

.field public static final enum in_progress:LX/FHH;

.field public static final enum success:LX/FHH;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2220128
    new-instance v0, LX/FHH;

    const-string v1, "success"

    invoke-direct {v0, v1, v2}, LX/FHH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHH;->success:LX/FHH;

    new-instance v0, LX/FHH;

    const-string v1, "failure"

    invoke-direct {v0, v1, v3}, LX/FHH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHH;->failure:LX/FHH;

    new-instance v0, LX/FHH;

    const-string v1, "cancelation"

    invoke-direct {v0, v1, v4}, LX/FHH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHH;->cancelation:LX/FHH;

    new-instance v0, LX/FHH;

    const-string v1, "in_progress"

    invoke-direct {v0, v1, v5}, LX/FHH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHH;->in_progress:LX/FHH;

    .line 2220129
    const/4 v0, 0x4

    new-array v0, v0, [LX/FHH;

    sget-object v1, LX/FHH;->success:LX/FHH;

    aput-object v1, v0, v2

    sget-object v1, LX/FHH;->failure:LX/FHH;

    aput-object v1, v0, v3

    sget-object v1, LX/FHH;->cancelation:LX/FHH;

    aput-object v1, v0, v4

    sget-object v1, LX/FHH;->in_progress:LX/FHH;

    aput-object v1, v0, v5

    sput-object v0, LX/FHH;->$VALUES:[LX/FHH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHH;
    .locals 1

    .prologue
    .line 2220131
    const-class v0, LX/FHH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHH;

    return-object v0
.end method

.method public static values()[LX/FHH;
    .locals 1

    .prologue
    .line 2220132
    sget-object v0, LX/FHH;->$VALUES:[LX/FHH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHH;

    return-object v0
.end method
