.class public final LX/GQP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/aldrin/status/AldrinUserStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:LX/2YW;


# direct methods
.method public constructor <init>(LX/2YW;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2350120
    iput-object p1, p0, LX/GQP;->b:LX/2YW;

    iput-object p2, p0, LX/GQP;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2350130
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350121
    check-cast p1, Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 2350122
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    if-nez v0, :cond_1

    .line 2350123
    :cond_0
    :goto_0
    return-void

    .line 2350124
    :cond_1
    sget-object v0, LX/GQR;->a:[I

    iget-object v1, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2350125
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 2350126
    if-eqz v0, :cond_0

    .line 2350127
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-ne v0, v1, :cond_0

    .line 2350128
    :cond_2
    iget-object v1, p0, LX/GQP;->b:LX/2YW;

    iget-object v2, p0, LX/GQP;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v3, LX/GQU;->AGREED_IMPLICIT:LX/GQU;

    iget-object v4, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2YW;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;LX/GQU;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2350129
    :pswitch_1
    iget-object v0, p0, LX/GQP;->b:LX/2YW;

    iget-object v0, v0, LX/2YW;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GQW;

    iget-object v1, p0, LX/GQP;->b:LX/2YW;

    iget-object v1, v1, LX/2YW;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/GQW;->a(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
