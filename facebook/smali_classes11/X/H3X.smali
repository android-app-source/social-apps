.class public final LX/H3X;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/nearby/search/NearbySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/search/NearbySearchFragment;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 2421085
    iput-object p1, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iput-object p2, p0, LX/H3X;->a:Ljava/lang/String;

    iput-wide p3, p0, LX/H3X;->b:J

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 12

    .prologue
    .line 2421086
    iget-object v0, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    iget-object v1, p0, LX/H3X;->a:Ljava/lang/String;

    iget-object v2, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v2, v2, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    .line 2421087
    iget-object v3, v0, LX/H1k;->a:LX/0Zb;

    const-string v4, "search_results_loaded"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2421088
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2421089
    const-string v4, "places_search"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "search_query_string"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "session_id"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2421090
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2421091
    :cond_0
    iget-object v0, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->setVisibility(I)V

    .line 2421092
    iget-object v1, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;

    .line 2421093
    invoke-static {v1, v0}, Lcom/facebook/nearby/search/NearbySearchFragment;->a$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;)V

    .line 2421094
    iget-object v0, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    iget-object v1, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v1, v1, Lcom/facebook/nearby/search/NearbySearchFragment;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/H3X;->b:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    .line 2421095
    iget-object v6, v0, LX/H1k;->a:LX/0Zb;

    const-string v7, "search_time_to_interaction"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2421096
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2421097
    const-string v7, "places_search"

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "source"

    const-string v9, "string_query"

    invoke-virtual {v7, v8, v9}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "elapsed_time_to_interaction"

    const/high16 v9, 0x447a0000    # 1000.0f

    div-float v9, v1, v9

    float-to-double v10, v9

    invoke-virtual {v7, v8, v10, v11}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 2421098
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2421099
    :cond_1
    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 4

    .prologue
    .line 2421100
    invoke-super {p0, p1}, LX/2h0;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 2421101
    iget-object v0, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    iget-object v1, p0, LX/H3X;->a:Ljava/lang/String;

    iget-object v2, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v2, v2, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    .line 2421102
    iget-object v3, v0, LX/H1k;->a:LX/0Zb;

    const-string p0, "search_cancel"

    const/4 p1, 0x1

    invoke-interface {v3, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2421103
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2421104
    const-string p0, "places_search"

    invoke-virtual {v3, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "search_query_string"

    invoke-virtual {p0, p1, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "session_id"

    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2421105
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2421106
    :cond_0
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 7

    .prologue
    .line 2421107
    iget-object v0, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/H3X;->a:Ljava/lang/String;

    iget-object v3, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v3, v3, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    .line 2421108
    iget-object v4, v0, LX/H1k;->a:LX/0Zb;

    const-string v5, "search_results_error"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2421109
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2421110
    const-string v5, "places_search"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "search_error"

    invoke-virtual {v5, v6, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "search_query_string"

    invoke-virtual {v5, v6, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "session_id"

    invoke-virtual {v5, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2421111
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2421112
    :cond_0
    sget-object v0, Lcom/facebook/nearby/search/NearbySearchFragment;->a:Ljava/lang/Class;

    const-string v1, "Error searching nearby places"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2421113
    iget-object v0, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->setVisibility(I)V

    .line 2421114
    iget-object v0, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->n:Lcom/facebook/nearby/ui/TypeaheadStatusView;

    iget-object v1, p0, LX/H3X;->c:Lcom/facebook/nearby/search/NearbySearchFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/ui/TypeaheadStatusView;->a(Ljava/lang/String;)V

    .line 2421115
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2421116
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/H3X;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
