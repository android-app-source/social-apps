.class public LX/FrJ;
.super LX/Fr3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(LX/Fsr;Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2295364
    invoke-direct {p0, p1}, LX/Fr3;-><init>(LX/Fsr;)V

    .line 2295365
    const v0, 0x7f0a00e5

    invoke-static {p2, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/FrJ;->a:I

    .line 2295366
    return-void
.end method

.method public static a(LX/0QB;)LX/FrJ;
    .locals 5

    .prologue
    .line 2295367
    const-class v1, LX/FrJ;

    monitor-enter v1

    .line 2295368
    :try_start_0
    sget-object v0, LX/FrJ;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2295369
    sput-object v2, LX/FrJ;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2295370
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2295371
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2295372
    new-instance p0, LX/FrJ;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v3

    check-cast v3, LX/Fsr;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p0, v3, v4}, LX/FrJ;-><init>(LX/Fsr;Landroid/content/Context;)V

    .line 2295373
    move-object v0, p0

    .line 2295374
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2295375
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FrJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2295376
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2295377
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/Fr7;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 2295378
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    if-nez v0, :cond_0

    .line 2295379
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 2295380
    :goto_0
    return v2

    .line 2295381
    :cond_0
    iput-object p0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b:LX/D3l;

    .line 2295382
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 2295383
    const v0, 0x7f0b0da8

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2295384
    const v0, 0x7f0b0df5

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v0, v0

    .line 2295385
    const v3, 0x7f0b0df5

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move v3, v3

    .line 2295386
    const v4, 0x7f0b0d7d

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2295387
    add-int v5, v1, v0

    add-int/2addr v5, v3

    invoke-virtual {p1, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2295388
    const v5, 0x7f0b0d7e

    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setThumbnailPadding(I)V

    .line 2295389
    invoke-virtual {p1, v4, v0, v4, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setPadding(IIII)V

    .line 2295390
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    .line 2295391
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2295392
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2295393
    :goto_1
    move-object v4, v3

    .line 2295394
    const-string v5, "timeline"

    move-object v0, p1

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIILandroid/net/Uri;Ljava/lang/String;)V

    .line 2295395
    iget v1, p0, LX/FrJ;->a:I

    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v3, 0x2

    const v4, 0x7f0b0050

    invoke-static {v7, v4}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v4

    invoke-virtual {p1, v1, v0, v3, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ILjava/lang/CharSequence;II)V

    .line 2295396
    invoke-virtual {p1, v6, v2, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2295397
    iput-boolean v2, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->l:Z

    .line 2295398
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-static {v0}, LX/Fr3;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Z

    move-result v0

    iget-object v1, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLjava/lang/Object;)V

    .line 2295399
    const v0, 0x7f0219a0

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setBackgroundResource(I)V

    .line 2295400
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_1
    move-object v0, v6

    .line 2295401
    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
