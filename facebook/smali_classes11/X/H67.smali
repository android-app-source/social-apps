.class public final LX/H67;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 0

    .prologue
    .line 2425837
    iput-object p1, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2425838
    const v1, 0x1e9bf2

    move-object v0, p1

    check-cast v0, LX/4Ua;

    .line 2425839
    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v2

    .line 2425840
    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    if-ne v1, v0, :cond_0

    .line 2425841
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    const/4 v1, 0x1

    .line 2425842
    iput-boolean v1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    .line 2425843
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->w()LX/H72;

    move-result-object v1

    invoke-interface {v1}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Z)V

    .line 2425844
    :goto_0
    return-void

    .line 2425845
    :cond_0
    const v1, 0x14b4d2

    move-object v0, p1

    check-cast v0, LX/4Ua;

    .line 2425846
    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v2

    .line 2425847
    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    if-ne v1, v0, :cond_1

    .line 2425848
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Error loading offers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2425849
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->O(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0

    .line 2425850
    :cond_1
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Error loading offers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2425851
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2425852
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 2425853
    if-eqz p1, :cond_0

    .line 2425854
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425855
    if-eqz v0, :cond_0

    .line 2425856
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425857
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2425858
    iget-object v1, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425859
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425860
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    invoke-static {v0}, LX/H83;->a(LX/H70;)LX/H83;

    move-result-object v0

    .line 2425861
    iput-object v0, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425862
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->n()Ljava/lang/String;

    move-result-object v1

    .line 2425863
    iput-object v1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    .line 2425864
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425865
    iput-boolean v4, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2425866
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425867
    iput-boolean v6, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->aA:Z

    .line 2425868
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iget-object v1, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->m()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425869
    iget-object v3, v2, LX/H83;->b:LX/H70;

    move-object v2, v3

    .line 2425870
    iget-object v3, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget v3, v3, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    iget-object v5, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v5, v5, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->g:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/H60;->a(Ljava/lang/String;LX/H70;IZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2425871
    iget-object v1, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v1}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)LX/0TF;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2425872
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0, v6, v6}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;ZZ)V

    .line 2425873
    :goto_0
    return-void

    .line 2425874
    :cond_0
    iget-object v0, p0, LX/H67;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0
.end method
