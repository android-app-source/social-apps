.class public final LX/FP8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$NearbyPlacesHugeResultsSearchQueryV2;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FP4;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/FPA;


# direct methods
.method public constructor <init>(LX/FPA;LX/FP4;LX/0TF;)V
    .locals 0

    .prologue
    .line 2236729
    iput-object p1, p0, LX/FP8;->c:LX/FPA;

    iput-object p2, p0, LX/FP8;->a:LX/FP4;

    iput-object p3, p0, LX/FP8;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/FP8;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;)V
    .locals 1

    .prologue
    .line 2236730
    iget-object v0, p0, LX/FP8;->b:LX/0TF;

    if-eqz v0, :cond_0

    .line 2236731
    iget-object v0, p0, LX/FP8;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2236732
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultsSearchQueryV2Model;)V
    .locals 15

    .prologue
    .line 2236733
    if-nez p1, :cond_0

    .line 2236734
    :goto_0
    return-void

    .line 2236735
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultsSearchQueryV2Model;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    move-result-object v14

    .line 2236736
    if-nez v14, :cond_1

    .line 2236737
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;-><init>()V

    invoke-static {p0, v0}, LX/FP8;->a(LX/FP8;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;)V

    .line 2236738
    :cond_1
    invoke-virtual {v14}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v8

    .line 2236739
    invoke-virtual {v14}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v0

    .line 2236740
    const/4 v10, 0x0

    .line 2236741
    const/4 v12, 0x0

    .line 2236742
    if-eqz v0, :cond_2

    .line 2236743
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v10

    .line 2236744
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;->b()Z

    move-result v12

    .line 2236745
    :cond_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2236746
    invoke-virtual {v14}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v2

    .line 2236747
    if-eqz v2, :cond_4

    .line 2236748
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2236749
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;

    .line 2236750
    if-eqz v0, :cond_3

    .line 2236751
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2236752
    if-eqz v0, :cond_3

    .line 2236753
    new-instance v3, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->a(LX/CQM;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;-><init>(LX/CQM;)V

    .line 2236754
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2236755
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2236756
    :cond_4
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    iget-object v1, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v1}, LX/FP4;->a()Landroid/location/Location;

    move-result-object v1

    iget-object v2, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v2}, LX/FP4;->b()Landroid/location/Location;

    move-result-object v2

    iget-object v3, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v3}, LX/FP4;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v4}, LX/FP4;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v4

    iget-object v5, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v5}, LX/FP4;->i()LX/FPn;

    move-result-object v5

    iget-object v6, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v6}, LX/FP4;->j()Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-result-object v6

    iget-object v7, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v7}, LX/FP4;->e()Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-result-object v7

    iget-object v11, p0, LX/FP8;->a:LX/FP4;

    invoke-virtual {v11}, LX/FP4;->h()Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_5

    const/4 v11, 0x1

    :goto_2
    invoke-virtual {v14}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->e()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v14}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->c()Z

    move-result v14

    invoke-direct/range {v0 .. v14}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;-><init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Ljava/util/ArrayList;Ljava/lang/String;ZZLjava/lang/String;Z)V

    .line 2236757
    invoke-static {p0, v0}, LX/FP8;->a(LX/FP8;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;)V

    goto/16 :goto_0

    .line 2236758
    :cond_5
    const/4 v11, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2236759
    iget-object v0, p0, LX/FP8;->b:LX/0TF;

    if-eqz v0, :cond_0

    .line 2236760
    iget-object v0, p0, LX/FP8;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2236761
    :cond_0
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2236762
    check-cast p1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultsSearchQueryV2Model;

    invoke-direct {p0, p1}, LX/FP8;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultsSearchQueryV2Model;)V

    return-void
.end method
