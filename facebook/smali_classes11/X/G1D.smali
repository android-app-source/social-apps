.class public LX/G1D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KJ",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/G1C;

.field public final b:Lcom/facebook/timeline/protiles/model/ProtileModel;

.field public c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;


# direct methods
.method public constructor <init>(LX/G1C;Lcom/facebook/timeline/protiles/model/ProtileModel;)V
    .locals 0

    .prologue
    .line 2310859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310860
    iput-object p1, p0, LX/G1D;->a:LX/G1C;

    .line 2310861
    iput-object p2, p0, LX/G1D;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2310862
    return-void
.end method

.method public static a(Lcom/facebook/timeline/protiles/model/ProtileModel;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/G1D;
    .locals 2

    .prologue
    .line 2310863
    new-instance v0, LX/G1D;

    sget-object v1, LX/G1C;->CLICK_ITEM:LX/G1C;

    invoke-direct {v0, v1, p0}, LX/G1D;-><init>(LX/G1C;Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    .line 2310864
    iput-object p1, v0, LX/G1D;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2310865
    return-object v0
.end method


# virtual methods
.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2310866
    iget-object v0, p0, LX/G1D;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G1D;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
