.class public LX/GbW;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GbW;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369898
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2369899
    sget-object v0, LX/0ax;->fm:Ljava/lang/String;

    const-class v1, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2369900
    return-void
.end method

.method public static a(LX/0QB;)LX/GbW;
    .locals 3

    .prologue
    .line 2369901
    sget-object v0, LX/GbW;->a:LX/GbW;

    if-nez v0, :cond_1

    .line 2369902
    const-class v1, LX/GbW;

    monitor-enter v1

    .line 2369903
    :try_start_0
    sget-object v0, LX/GbW;->a:LX/GbW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2369904
    if-eqz v2, :cond_0

    .line 2369905
    :try_start_1
    new-instance v0, LX/GbW;

    invoke-direct {v0}, LX/GbW;-><init>()V

    .line 2369906
    move-object v0, v0

    .line 2369907
    sput-object v0, LX/GbW;->a:LX/GbW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2369908
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2369909
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2369910
    :cond_1
    sget-object v0, LX/GbW;->a:LX/GbW;

    return-object v0

    .line 2369911
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2369912
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
