.class public LX/GIN;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

.field private b:LX/GLB;

.field private c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;


# direct methods
.method public constructor <init>(LX/GLB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336532
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336533
    iput-object p1, p0, LX/GIN;->b:LX/GLB;

    .line 2336534
    return-void
.end method

.method public static a$redex0(LX/GIN;ILandroid/widget/RadioGroup;)V
    .locals 3

    .prologue
    .line 2336543
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2336544
    sget v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->b:I

    if-ne p1, v1, :cond_1

    .line 2336545
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v0

    .line 2336546
    :goto_0
    iget-object v0, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2336547
    invoke-virtual {p2, p1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2336548
    instance-of v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    if-eqz v2, :cond_0

    .line 2336549
    iget-object v2, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    .line 2336550
    iget-object p1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a:Ljava/lang/String;

    move-object v0, p1

    .line 2336551
    iput-object v0, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2336552
    :cond_0
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2336553
    new-instance v2, LX/GF8;

    invoke-direct {v2, v1}, LX/GF8;-><init>(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    invoke-virtual {v0, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2336554
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GIN;
    .locals 5

    .prologue
    .line 2336535
    new-instance v1, LX/GIN;

    .line 2336536
    new-instance v4, LX/GLB;

    .line 2336537
    new-instance v3, LX/GEK;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v2

    check-cast v2, LX/2U4;

    invoke-direct {v3, v0, v2}, LX/GEK;-><init>(LX/0tX;LX/2U4;)V

    .line 2336538
    move-object v0, v3

    .line 2336539
    check-cast v0, LX/GEK;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v3

    check-cast v3, LX/2U3;

    invoke-direct {v4, v0, v2, v3}, LX/GLB;-><init>(LX/GEK;LX/1Ck;LX/2U3;)V

    .line 2336540
    move-object v0, v4

    .line 2336541
    check-cast v0, LX/GLB;

    invoke-direct {v1, v0}, LX/GIN;-><init>(LX/GLB;)V

    .line 2336542
    return-object v1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2336510
    iget-object v0, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v0, v1, :cond_3

    .line 2336511
    :cond_0
    iget-object v1, p0, LX/GIN;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    iget-object v0, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v2, :cond_2

    sget v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->b:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c(I)V

    .line 2336512
    :cond_1
    :goto_1
    return-void

    .line 2336513
    :cond_2
    sget v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a:I

    goto :goto_0

    .line 2336514
    :cond_3
    iget-object v0, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2336515
    iget-object v1, p0, LX/GIN;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    iget-object v0, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2336516
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v0, v2

    .line 2336517
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v2, :cond_4

    sget v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->b:I

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c(I)V

    goto :goto_1

    :cond_4
    sget v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a:I

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2336525
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336526
    iget-object v0, p0, LX/GIN;->b:LX/GLB;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2336527
    const/4 v0, 0x0

    iput-object v0, p0, LX/GIN;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    .line 2336528
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2336529
    invoke-super {p0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2336530
    iget-object v0, p0, LX/GIN;->b:LX/GLB;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2336531
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2336519
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2336520
    iget-object v0, p0, LX/GIN;->b:LX/GLB;

    invoke-virtual {v0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2336521
    const-string v0, "adinterfaces_audience"

    iget-object v1, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2336522
    iget-object p0, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, p0

    .line 2336523
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2336524
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336518
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GIN;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2336507
    iput-object p1, p0, LX/GIN;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2336508
    iget-object v0, p0, LX/GIN;->b:LX/GLB;

    invoke-virtual {v0, p1}, LX/GLB;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2336509
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336501
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336502
    iput-object p1, p0, LX/GIN;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    .line 2336503
    iget-object v0, p0, LX/GIN;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    new-instance v1, LX/GIM;

    invoke-direct {v1, p0}, LX/GIM;-><init>(LX/GIN;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->setOnCheckChangedListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2336504
    invoke-direct {p0}, LX/GIN;->b()V

    .line 2336505
    iget-object v0, p0, LX/GIN;->b:LX/GLB;

    iget-object v1, p0, LX/GIN;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    invoke-virtual {v0, v1, p2}, LX/GLB;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336506
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336491
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2336492
    if-eqz p1, :cond_0

    .line 2336493
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2336494
    if-eqz v0, :cond_0

    .line 2336495
    const-string v0, "adinterfaces_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2336496
    if-eqz v0, :cond_0

    .line 2336497
    iget-object v1, p0, LX/GIN;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v2, :cond_1

    sget v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->b:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c(I)V

    .line 2336498
    :cond_0
    iget-object v0, p0, LX/GIN;->b:LX/GLB;

    invoke-virtual {v0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2336499
    return-void

    .line 2336500
    :cond_1
    sget v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a:I

    goto :goto_0
.end method
