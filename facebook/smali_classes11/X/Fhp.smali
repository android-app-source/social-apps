.class public LX/Fhp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273811
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273812
    iput-object v0, p0, LX/Fhp;->a:LX/0Ot;

    .line 2273813
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273814
    iput-object v0, p0, LX/Fhp;->b:LX/0Ot;

    .line 2273815
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/Fhp;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2273816
    return-void
.end method

.method public static a(LX/0QB;)LX/Fhp;
    .locals 5

    .prologue
    .line 2273817
    const-class v1, LX/Fhp;

    monitor-enter v1

    .line 2273818
    :try_start_0
    sget-object v0, LX/Fhp;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2273819
    sput-object v2, LX/Fhp;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2273820
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273821
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2273822
    new-instance v3, LX/Fhp;

    invoke-direct {v3}, LX/Fhp;-><init>()V

    .line 2273823
    const/16 v4, 0x140d

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x34f6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2273824
    iput-object v4, v3, LX/Fhp;->a:LX/0Ot;

    iput-object p0, v3, LX/Fhp;->b:LX/0Ot;

    .line 2273825
    move-object v0, v3

    .line 2273826
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2273827
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fhp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273828
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2273829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
