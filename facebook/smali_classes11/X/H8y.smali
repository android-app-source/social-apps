.class public LX/H8y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2l5;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HDT;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private n:LX/H8E;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433835
    const v0, 0x7f020813

    sput v0, LX/H8y;->a:I

    .line 2433836
    const v0, 0x7f02081d

    sput v0, LX/H8y;->b:I

    .line 2433837
    const v0, 0x7f081659

    sput v0, LX/H8y;->c:I

    .line 2433838
    const v0, 0x7f08165a

    sput v0, LX/H8y;->d:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)V
    .locals 0
    .param p8    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2l5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HDT;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433782
    iput-object p1, p0, LX/H8y;->e:LX/0Ot;

    .line 2433783
    iput-object p2, p0, LX/H8y;->f:LX/0Ot;

    .line 2433784
    iput-object p3, p0, LX/H8y;->g:LX/0Ot;

    .line 2433785
    iput-object p4, p0, LX/H8y;->h:LX/0Ot;

    .line 2433786
    iput-object p5, p0, LX/H8y;->i:LX/0Ot;

    .line 2433787
    iput-object p6, p0, LX/H8y;->j:LX/0Ot;

    .line 2433788
    iput-object p7, p0, LX/H8y;->l:LX/0Ot;

    .line 2433789
    iput-object p8, p0, LX/H8y;->k:LX/0Ot;

    .line 2433790
    iput-object p9, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433791
    return-void
.end method

.method public static a$redex0(LX/H8y;Z)V
    .locals 4

    .prologue
    .line 2433839
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    instance-of v0, v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;->b()Z

    move-result v0

    if-ne v0, p1, :cond_1

    .line 2433840
    :cond_0
    :goto_0
    return-void

    .line 2433841
    :cond_1
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-static {v0}, LX/9YE;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)LX/9YE;

    move-result-object v1

    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    .line 2433842
    new-instance v2, LX/9YD;

    invoke-direct {v2}, LX/9YD;-><init>()V

    .line 2433843
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;->a()Z

    move-result v3

    iput-boolean v3, v2, LX/9YD;->a:Z

    .line 2433844
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;->b()Z

    move-result v3

    iput-boolean v3, v2, LX/9YD;->b:Z

    .line 2433845
    move-object v0, v2

    .line 2433846
    iput-boolean p1, v0, LX/9YD;->b:Z

    .line 2433847
    move-object v0, v0

    .line 2433848
    invoke-virtual {v0}, LX/9YD;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    .line 2433849
    iput-object v0, v1, LX/9YE;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    .line 2433850
    move-object v0, v1

    .line 2433851
    invoke-virtual {v0}, LX/9YE;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v0

    iput-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433852
    iget-object v0, p0, LX/H8y;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HDT;

    new-instance v1, LX/HDV;

    invoke-direct {v1, p1}, LX/HDV;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 2433829
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 7

    .prologue
    .line 2433830
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    invoke-direct {p0}, LX/H8y;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, LX/H8y;->d:I

    :goto_0
    invoke-direct {p0}, LX/H8y;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    sget v3, LX/H8y;->b:I

    :goto_1
    const/4 v4, 0x1

    .line 2433831
    iget-object v5, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    .line 2433832
    new-instance v6, LX/8A4;

    invoke-direct {v6, v5}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object p0, LX/8A3;->ADMINISTER:LX/8A3;

    invoke-virtual {v6, p0}, LX/8A4;->a(LX/8A3;)Z

    move-result v6

    move v5, v6

    .line 2433833
    move v5, v5

    .line 2433834
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    sget v2, LX/H8y;->c:I

    goto :goto_0

    :cond_1
    sget v3, LX/H8y;->a:I

    goto :goto_1
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433828
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8y;->c:I

    sget v3, LX/H8y;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2433792
    invoke-direct {p0}, LX/H8y;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2433793
    iget-object v0, p0, LX/H8y;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v3, LX/9X4;->EVENT_ADMIN_REMOVE_FROM_FAVORTIES:LX/9X4;

    iget-object v4, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433794
    new-instance v3, LX/2rA;

    invoke-direct {v3}, LX/2rA;-><init>()V

    .line 2433795
    iget-object v0, p0, LX/H8y;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2433796
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2433797
    invoke-virtual {v3, v0}, LX/2rA;->a(Ljava/lang/String;)LX/2rA;

    .line 2433798
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2rA;->b(Ljava/lang/String;)LX/2rA;

    .line 2433799
    const-string v0, "wilde_profile_more"

    invoke-virtual {v3, v0}, LX/2rA;->c(Ljava/lang/String;)LX/2rA;

    .line 2433800
    new-instance v0, LX/HEm;

    invoke-direct {v0}, LX/HEm;-><init>()V

    move-object v0, v0

    .line 2433801
    const-string v4, "input"

    invoke-virtual {v0, v4, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/HEm;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2433802
    iget-object v0, p0, LX/H8y;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-interface {v0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2433803
    iput-object v0, v3, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2433804
    iget-object v0, p0, LX/H8y;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2433805
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2433806
    :goto_0
    invoke-static {p0, v2}, LX/H8y;->a$redex0(LX/H8y;Z)V

    .line 2433807
    new-instance v1, LX/H8u;

    invoke-direct {v1, p0, v0}, LX/H8u;-><init>(LX/H8y;Z)V

    .line 2433808
    iget-object v0, p0, LX/H8y;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v3, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2433809
    :goto_1
    return-void

    .line 2433810
    :cond_1
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;->b()Z

    move-result v0

    goto :goto_0

    .line 2433811
    :cond_2
    iget-object v0, p0, LX/H8y;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v3, LX/9X4;->EVENT_ADMIN_ADD_TO_FAVORITES:LX/9X4;

    iget-object v4, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433812
    new-instance v3, LX/4D9;

    invoke-direct {v3}, LX/4D9;-><init>()V

    .line 2433813
    iget-object v0, p0, LX/H8y;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2433814
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2433815
    invoke-virtual {v3, v0}, LX/4D9;->a(Ljava/lang/String;)LX/4D9;

    .line 2433816
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/4D9;->b(Ljava/lang/String;)LX/4D9;

    .line 2433817
    const-string v0, "wilde_profile_more"

    invoke-virtual {v3, v0}, LX/4D9;->c(Ljava/lang/String;)LX/4D9;

    .line 2433818
    new-instance v0, LX/HEl;

    invoke-direct {v0}, LX/HEl;-><init>()V

    move-object v0, v0

    .line 2433819
    const-string v4, "input"

    invoke-virtual {v0, v4, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/HEl;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2433820
    iget-object v0, p0, LX/H8y;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-interface {v0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2433821
    iput-object v0, v3, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2433822
    iget-object v0, p0, LX/H8y;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2433823
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    .line 2433824
    :goto_2
    invoke-static {p0, v1}, LX/H8y;->a$redex0(LX/H8y;Z)V

    .line 2433825
    new-instance v1, LX/H8v;

    invoke-direct {v1, p0, v0}, LX/H8v;-><init>(LX/H8y;Z)V

    .line 2433826
    iget-object v0, p0, LX/H8y;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v3, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_1

    .line 2433827
    :cond_4
    iget-object v0, p0, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->c()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;->b()Z

    move-result v0

    goto :goto_2
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433778
    iget-object v0, p0, LX/H8y;->n:LX/H8E;

    if-nez v0, :cond_0

    .line 2433779
    new-instance v0, LX/H8x;

    invoke-direct {v0, p0}, LX/H8x;-><init>(LX/H8y;)V

    iput-object v0, p0, LX/H8y;->n:LX/H8E;

    .line 2433780
    :cond_0
    iget-object v0, p0, LX/H8y;->n:LX/H8E;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
