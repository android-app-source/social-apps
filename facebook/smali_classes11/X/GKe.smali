.class public final LX/GKe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GKj;


# direct methods
.method public constructor <init>(LX/GKj;)V
    .locals 0

    .prologue
    .line 2341220
    iput-object p1, p0, LX/GKe;->a:LX/GKj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2341221
    iget-object v0, p0, LX/GKe;->a:LX/GKj;

    invoke-virtual {v0}, LX/GKj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2341222
    iget-object v0, p0, LX/GKe;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GKe;->a:LX/GKj;

    invoke-static {v1}, LX/GKj;->j(LX/GKj;)I

    move-result v1

    .line 2341223
    iput v1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    .line 2341224
    :cond_0
    iget-object v0, p0, LX/GKe;->a:LX/GKj;

    .line 2341225
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2341226
    sget-object v1, LX/GG8;->INVALID_BID_AMOUNT:LX/GG8;

    iget-object v2, p0, LX/GKe;->a:LX/GKj;

    invoke-virtual {v2}, LX/GKj;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2341227
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2341228
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2341229
    return-void
.end method
