.class public LX/Gvm;
.super LX/4om;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/17W;

.field public c:Lcom/facebook/content/SecureContextHelper;

.field public d:LX/17d;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;Lcom/facebook/content/SecureContextHelper;)V
    .locals 2

    .prologue
    .line 2406066
    invoke-direct {p0, p1}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2406067
    const-class v0, LX/Gvm;

    invoke-static {v0, p0}, LX/Gvm;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 2406068
    iput-object p1, p0, LX/Gvm;->a:Landroid/content/Context;

    .line 2406069
    iput-object p2, p0, LX/Gvm;->b:LX/17W;

    .line 2406070
    iput-object p3, p0, LX/Gvm;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2406071
    const-string v0, "URI Test Widget"

    invoke-virtual {p0, v0}, LX/Gvm;->setTitle(Ljava/lang/CharSequence;)V

    .line 2406072
    const-string v0, "Test an internal or external URI"

    invoke-virtual {p0, v0}, LX/Gvm;->setSummary(Ljava/lang/CharSequence;)V

    .line 2406073
    invoke-virtual {p0}, LX/Gvm;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "e.g. fbrpc://... or fb://..."

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2406074
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gvm;

    invoke-static {p0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object p0

    check-cast p0, LX/17d;

    iput-object p0, p1, LX/Gvm;->d:LX/17d;

    return-void
.end method


# virtual methods
.method public final onDialogClosed(Z)V
    .locals 3

    .prologue
    .line 2406075
    if-nez p1, :cond_0

    .line 2406076
    :goto_0
    return-void

    .line 2406077
    :cond_0
    invoke-virtual {p0}, LX/Gvm;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 2406078
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2406079
    if-nez v0, :cond_1

    .line 2406080
    const-string v0, "Error parsing text"

    .line 2406081
    iget-object v1, p0, LX/Gvm;->a:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2406082
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2406083
    invoke-static {v0}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2406084
    iget-object v1, p0, LX/Gvm;->d:LX/17d;

    iget-object v2, p0, LX/Gvm;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 2406085
    iget-object v2, p0, LX/Gvm;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, p0, LX/Gvm;->a:Landroid/content/Context;

    invoke-interface {v2, v1, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2406086
    goto :goto_0

    .line 2406087
    :cond_2
    iget-object v1, p0, LX/Gvm;->b:LX/17W;

    iget-object v2, p0, LX/Gvm;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2406088
    const-string v1, "Please enter a valid URI"

    .line 2406089
    iget-object v2, p0, LX/Gvm;->a:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-static {v2, v1, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2406090
    :cond_3
    goto :goto_0
.end method
