.class public LX/FIU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FIU;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/FIn;",
            "LX/FIS;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2Uj;

.field private final c:LX/2Uj;

.field private final d:LX/1qI;


# direct methods
.method public constructor <init>(LX/2Uj;LX/2Uh;LX/2Uj;LX/1qI;)V
    .locals 1
    .param p1    # LX/2Uj;
        .annotation runtime Lcom/facebook/messaging/media/upload/udp/UDPOutgoingPacketQueue;
        .end annotation
    .end param
    .param p3    # LX/2Uj;
        .annotation runtime Lcom/facebook/messaging/media/upload/udp/UDPIncomingPacketQueue;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222069
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FIU;->a:Ljava/util/Map;

    .line 2222070
    iput-object p1, p0, LX/FIU;->b:LX/2Uj;

    .line 2222071
    iget-object v0, p2, LX/2Uh;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2222072
    iput-object p3, p0, LX/FIU;->c:LX/2Uj;

    .line 2222073
    iput-object p4, p0, LX/FIU;->d:LX/1qI;

    .line 2222074
    return-void
.end method

.method public static a(LX/FIU;LX/FIn;)LX/FIS;
    .locals 2

    .prologue
    .line 2222075
    iget-object v1, p0, LX/FIU;->a:Ljava/util/Map;

    monitor-enter v1

    .line 2222076
    :try_start_0
    iget-object v0, p0, LX/FIU;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIS;

    .line 2222077
    monitor-exit v1

    .line 2222078
    return-object v0

    .line 2222079
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(LX/0QB;)LX/FIU;
    .locals 7

    .prologue
    .line 2222080
    sget-object v0, LX/FIU;->e:LX/FIU;

    if-nez v0, :cond_1

    .line 2222081
    const-class v1, LX/FIU;

    monitor-enter v1

    .line 2222082
    :try_start_0
    sget-object v0, LX/FIU;->e:LX/FIU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2222083
    if-eqz v2, :cond_0

    .line 2222084
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2222085
    new-instance p0, LX/FIU;

    invoke-static {v0}, LX/2Um;->a(LX/0QB;)LX/2Uj;

    move-result-object v3

    check-cast v3, LX/2Uj;

    invoke-static {v0}, LX/2Uh;->a(LX/0QB;)LX/2Uh;

    move-result-object v4

    check-cast v4, LX/2Uh;

    invoke-static {v0}, LX/2Ui;->a(LX/0QB;)LX/2Uj;

    move-result-object v5

    check-cast v5, LX/2Uj;

    invoke-static {v0}, LX/1qI;->a(LX/0QB;)LX/1qI;

    move-result-object v6

    check-cast v6, LX/1qI;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FIU;-><init>(LX/2Uj;LX/2Uh;LX/2Uj;LX/1qI;)V

    .line 2222086
    move-object v0, p0

    .line 2222087
    sput-object v0, LX/FIU;->e:LX/FIU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222088
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2222089
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2222090
    :cond_1
    sget-object v0, LX/FIU;->e:LX/FIU;

    return-object v0

    .line 2222091
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2222092
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/FIU;LX/FIS;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FIS;",
            "Ljava/util/List",
            "<",
            "LX/FIQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2222093
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 2222094
    :cond_0
    :goto_0
    return-void

    .line 2222095
    :cond_1
    iget-object v1, p1, LX/FIS;->a:LX/FIn;

    monitor-enter v1

    .line 2222096
    :try_start_0
    invoke-static {p1, p2}, LX/FIS;->a$redex0(LX/FIS;Ljava/util/List;)V

    .line 2222097
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 2222098
    if-nez v2, :cond_2

    .line 2222099
    monitor-exit v1

    goto :goto_0

    .line 2222100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2222101
    :cond_2
    :try_start_1
    invoke-static {p1}, LX/FIS;->a(LX/FIS;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2222102
    iget-object v0, p1, LX/FIS;->a:LX/FIn;

    const-string v2, "Uploaded too many bytes for session: "

    invoke-virtual {p0, v0, v2}, LX/FIU;->a(LX/FIn;Ljava/lang/String;)V

    .line 2222103
    monitor-exit v1

    goto :goto_0

    .line 2222104
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIQ;

    .line 2222105
    iget-object v4, p0, LX/FIU;->b:LX/2Uj;

    invoke-virtual {v4, v0}, LX/2Uj;->a(LX/FIO;)V

    goto :goto_1

    .line 2222106
    :cond_4
    mul-int/lit16 v0, v2, 0x5dc

    int-to-long v2, v0

    invoke-static {p1, v2, v3}, LX/FIS;->a$redex0(LX/FIS;J)V

    .line 2222107
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222108
    iget-object v0, p0, LX/FIU;->d:LX/1qI;

    invoke-virtual {v0}, LX/1qI;->a()V

    goto :goto_0
.end method

.method public static b(LX/FIU;LX/FIT;)LX/FIS;
    .locals 4

    .prologue
    .line 2222109
    new-instance v1, LX/FIn;

    iget v0, p1, LX/FIT;->a:I

    iget-wide v2, p1, LX/FIT;->b:J

    invoke-direct {v1, v0, v2, v3}, LX/FIn;-><init>(IJ)V

    .line 2222110
    iget-object v2, p0, LX/FIU;->a:Ljava/util/Map;

    monitor-enter v2

    .line 2222111
    :try_start_0
    iget-object v0, p0, LX/FIU;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIS;

    .line 2222112
    if-nez v0, :cond_0

    .line 2222113
    new-instance v0, LX/FIS;

    invoke-direct {v0, p1}, LX/FIS;-><init>(LX/FIT;)V

    .line 2222114
    iget-object v3, p0, LX/FIU;->a:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2222115
    :cond_0
    monitor-exit v2

    .line 2222116
    return-object v0

    .line 2222117
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(LX/FIU;LX/FIn;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2222118
    iget-object v1, p0, LX/FIU;->a:Ljava/util/Map;

    monitor-enter v1

    .line 2222119
    :try_start_0
    iget-object v0, p0, LX/FIU;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIS;

    .line 2222120
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222121
    if-eqz v0, :cond_0

    .line 2222122
    iget-object v1, v0, LX/FIS;->a:LX/FIn;

    monitor-enter v1

    .line 2222123
    :try_start_1
    iget-object v0, v0, LX/FIS;->e:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, 0x20b143fc

    invoke-static {v0, p2, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2222124
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2222125
    :cond_0
    return-void

    .line 2222126
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2222127
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    .line 2222128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2222129
    iget-object v1, p0, LX/FIU;->c:LX/2Uj;

    invoke-virtual {v1, v0}, LX/2Uj;->a(Ljava/util/Collection;)V

    .line 2222130
    invoke-static {v0}, LX/FIe;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 2222131
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2222132
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FIn;

    .line 2222133
    invoke-static {p0, v1}, LX/FIU;->a(LX/FIU;LX/FIn;)LX/FIS;

    move-result-object v6

    .line 2222134
    if-eqz v6, :cond_0

    .line 2222135
    const/4 v4, 0x0

    .line 2222136
    const/4 v3, 0x0

    .line 2222137
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 2222138
    const/4 v2, -0x1

    .line 2222139
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIO;

    .line 2222140
    sget-object v9, LX/FIR;->a:[I

    .line 2222141
    iget-object v10, v0, LX/FIO;->a:[B

    move-object v10, v10

    .line 2222142
    invoke-static {v10}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v10

    sget-object v11, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v10

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v10

    .line 2222143
    const/4 v11, -0x1

    if-ne v10, v11, :cond_9

    .line 2222144
    sget-object v10, LX/FId;->FINISHED:LX/FId;

    .line 2222145
    :goto_2
    move-object v10, v10

    .line 2222146
    invoke-virtual {v10}, LX/FId;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    :goto_3
    move v0, v2

    move v2, v0

    .line 2222147
    goto :goto_1

    .line 2222148
    :pswitch_0
    invoke-interface {v7}, Ljava/util/Set;->clear()V

    .line 2222149
    iget-object v11, v0, LX/FIO;->a:[B

    move-object v11, v11

    .line 2222150
    invoke-static {v11}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    sget-object v12, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    const/16 v12, 0x10

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    move-object v0, v11

    .line 2222151
    const/4 v3, 0x1

    move v4, v3

    move-object v3, v0

    .line 2222152
    goto :goto_1

    .line 2222153
    :pswitch_1
    iget-object v9, v0, LX/FIO;->a:[B

    move-object v9, v9

    .line 2222154
    invoke-static {v9}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    sget-object v10, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v9

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v9

    move v9, v9

    .line 2222155
    invoke-static {v2, v9}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2222156
    iget-object v9, v0, LX/FIO;->a:[B

    move-object v9, v9

    .line 2222157
    invoke-static {v9}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    sget-object v10, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v10

    .line 2222158
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 2222159
    iget v9, v0, LX/FIO;->b:I

    move v9, v9

    .line 2222160
    add-int/lit8 v9, v9, -0x10

    div-int/lit8 v12, v9, 0x4

    .line 2222161
    const/16 v9, 0x10

    invoke-virtual {v10, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2222162
    const/4 v9, 0x0

    :goto_4
    if-ge v9, v12, :cond_1

    .line 2222163
    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v11, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2222164
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 2222165
    :cond_1
    move-object v0, v11

    .line 2222166
    invoke-interface {v7, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 2222167
    :cond_2
    if-eqz v4, :cond_4

    .line 2222168
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2222169
    const-string v0, "Unable to get a valid fbid from UDP finished message"

    invoke-virtual {p0, v1, v0}, LX/FIU;->a(LX/FIn;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2222170
    :cond_3
    invoke-static {p0, v1, v3}, LX/FIU;->b(LX/FIU;LX/FIn;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2222171
    :cond_4
    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2222172
    add-int/lit8 v0, v2, 0x1

    .line 2222173
    :goto_5
    iget v2, v6, LX/FIS;->d:I

    if-ge v0, v2, :cond_5

    .line 2222174
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2222175
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2222176
    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2222177
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2222178
    if-ltz v0, :cond_6

    iget v4, v6, LX/FIS;->d:I

    if-ge v0, v4, :cond_6

    .line 2222179
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2222180
    :cond_7
    :try_start_0
    iget-object v0, v6, LX/FIS;->b:Ljava/io/File;

    const/16 v3, 0x5dc

    invoke-static {v0, v2, v3, v1}, LX/FIe;->a(Ljava/io/File;Ljava/util/List;ILX/FIn;)Ljava/util/List;

    move-result-object v0

    .line 2222181
    invoke-static {p0, v6, v0}, LX/FIU;->a(LX/FIU;LX/FIS;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2222182
    :catch_0
    move-exception v0

    .line 2222183
    const-class v1, LX/FIU;

    const-string v2, "unable to create chunks from file"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2222184
    :cond_8
    return-void

    .line 2222185
    :cond_9
    if-ltz v10, :cond_a

    .line 2222186
    sget-object v10, LX/FId;->HOLE:LX/FId;

    goto/16 :goto_2

    .line 2222187
    :cond_a
    sget-object v10, LX/FId;->OTHER:LX/FId;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/FIn;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2222188
    iget-object v1, p0, LX/FIU;->a:Ljava/util/Map;

    monitor-enter v1

    .line 2222189
    :try_start_0
    iget-object v0, p0, LX/FIU;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIS;

    .line 2222190
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222191
    if-eqz v0, :cond_0

    .line 2222192
    iget-object v1, v0, LX/FIS;->a:LX/FIn;

    monitor-enter v1

    .line 2222193
    :try_start_1
    iget-object v0, v0, LX/FIS;->e:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2, p2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2222194
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2222195
    :cond_0
    return-void

    .line 2222196
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2222197
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
