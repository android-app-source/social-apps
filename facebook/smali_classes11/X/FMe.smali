.class public LX/FMe;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[B


# instance fields
.field private final b:LX/2Ma;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2230156
    const-string v0, "<>"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, LX/FMe;->a:[B

    return-void
.end method

.method public constructor <init>(LX/2Ma;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2230157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2230158
    iput-object p1, p0, LX/FMe;->b:LX/2Ma;

    .line 2230159
    return-void
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)LX/Edg;
    .locals 5

    .prologue
    .line 2230160
    new-instance v0, LX/Edg;

    invoke-direct {v0}, LX/Edg;-><init>()V

    .line 2230161
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Edg;->e([B)V

    .line 2230162
    iput-object p0, v0, LX/Edg;->e:Landroid/net/Uri;

    .line 2230163
    invoke-static {v0, p2}, LX/FMe;->a(LX/Edg;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2230164
    return-object v0

    .line 2230165
    :catch_0
    move-exception v0

    .line 2230166
    const-string v1, "PduPartFactory"

    const-string v2, "Failed creating photo pdu part for image: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2230167
    new-instance v1, LX/EdT;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(LX/Edg;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2230168
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2230169
    invoke-virtual {p0, v0}, LX/Edg;->g([B)V

    .line 2230170
    invoke-virtual {p0, v0}, LX/Edg;->c([B)V

    .line 2230171
    sget-object v0, LX/FMe;->a:[B

    invoke-virtual {p0, v0}, LX/Edg;->b([B)V

    .line 2230172
    return-void
.end method

.method private static c(LX/FMe;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2230173
    if-eqz p2, :cond_0

    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2230174
    invoke-static {p1}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2230175
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2230176
    iget-object v1, p0, LX/FMe;->b:LX/2Ma;

    invoke-virtual {v1, v0}, LX/2Ma;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2230177
    :goto_1
    if-eqz v1, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {v1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2230178
    const-string v0, "^.*/"

    invoke-virtual {v1, v0, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2230179
    invoke-static {v0}, LX/EdQ;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2230180
    :goto_2
    invoke-static {v0}, LX/EdQ;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_3
    return-object v0

    .line 2230181
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 2230182
    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)LX/Edg;
    .locals 5

    .prologue
    .line 2230183
    new-instance v1, LX/Edg;

    invoke-direct {v1}, LX/Edg;-><init>()V

    .line 2230184
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "video/"

    invoke-static {p0, v0, v2}, LX/FMe;->c(LX/FMe;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2230185
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2230186
    const-string v0, "video/mp4"

    .line 2230187
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Edg;->e([B)V

    .line 2230188
    iput-object p1, v1, LX/Edg;->e:Landroid/net/Uri;

    .line 2230189
    invoke-static {v1, p2}, LX/FMe;->a(LX/Edg;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2230190
    return-object v1

    .line 2230191
    :catch_0
    move-exception v0

    .line 2230192
    const-string v1, "PduPartFactory"

    const-string v2, "Failed creating video pdu part for uri: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2230193
    new-instance v1, LX/EdT;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)LX/Edg;
    .locals 6

    .prologue
    .line 2230194
    new-instance v1, LX/Edg;

    invoke-direct {v1}, LX/Edg;-><init>()V

    .line 2230195
    :try_start_0
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "audio/"

    invoke-static {p0, v0, v2}, LX/FMe;->c(LX/FMe;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2230196
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2230197
    const-string v0, "audio/mp4"

    .line 2230198
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Edg;->e([B)V

    .line 2230199
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 2230200
    iput-object v0, v1, LX/Edg;->e:Landroid/net/Uri;

    .line 2230201
    invoke-static {v1, p2}, LX/FMe;->a(LX/Edg;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2230202
    return-object v1

    .line 2230203
    :catch_0
    move-exception v0

    .line 2230204
    const-string v1, "PduPartFactory"

    const-string v2, "Failed creating audio pdu part for resource: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2230205
    new-instance v1, LX/EdT;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v1
.end method
