.class public final LX/Gcp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/events/carousel/EventCardViewBinder;


# direct methods
.method public constructor <init>(Lcom/facebook/events/carousel/EventCardViewBinder;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2372605
    iput-object p1, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iput-object p2, p0, LX/Gcp;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, -0x1c5dff6d

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2372606
    iget-object v0, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2372607
    :cond_0
    const v0, -0x17a6d806

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2372608
    :goto_0
    return-void

    .line 2372609
    :cond_1
    iget-object v0, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 2372610
    iget-object v0, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gco;

    .line 2372611
    iget-object v2, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v2, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->y:LX/Gcz;

    if-eqz v2, :cond_2

    .line 2372612
    iget-object v2, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v2, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->y:LX/Gcz;

    iget-object v3, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    invoke-virtual {v2, v3, v0}, LX/Gcz;->a(Lcom/facebook/events/carousel/EventCardViewBinder;LX/Gco;)V

    .line 2372613
    :cond_2
    iget-object v2, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372614
    iget-object v3, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object v2, v3

    .line 2372615
    invoke-virtual {v0, v2}, LX/Gco;->a(Lcom/facebook/events/model/Event;)V

    .line 2372616
    iget-object v2, p0, LX/Gcp;->a:Landroid/view/View;

    iget-object v3, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v3, v3, Lcom/facebook/events/carousel/EventCardViewBinder;->b:Landroid/content/Context;

    iget-object v4, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372617
    iget-object p0, v4, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object v4, p0

    .line 2372618
    invoke-virtual {v0, v3, v4}, LX/Gco;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2372619
    :goto_1
    const v0, -0x62b881f9

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2372620
    :cond_3
    iget-object v0, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v2, p0, LX/Gcp;->a:Landroid/view/View;

    .line 2372621
    new-instance v4, LX/6WS;

    iget-object v3, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->b:Landroid/content/Context;

    invoke-direct {v4, v3}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2372622
    const/4 v3, 0x0

    .line 2372623
    iput-boolean v3, v4, LX/0ht;->e:Z

    .line 2372624
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v5

    .line 2372625
    iget-object v3, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Gco;

    .line 2372626
    iget-object v7, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->b:Landroid/content/Context;

    .line 2372627
    iget-object p1, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object p1, p1

    .line 2372628
    invoke-virtual {v3, v7, p1}, LX/Gco;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    .line 2372629
    new-instance p1, LX/Gcq;

    invoke-direct {p1, v0, v3}, LX/Gcq;-><init>(Lcom/facebook/events/carousel/EventCardViewBinder;LX/Gco;)V

    move-object v3, p1

    .line 2372630
    invoke-virtual {v7, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_2

    .line 2372631
    :cond_4
    invoke-virtual {v4, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 2372632
    iget-object v0, p0, LX/Gcp;->a:Landroid/view/View;

    iget-object v2, p0, LX/Gcp;->b:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v2, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->f:Landroid/content/res/Resources;

    const v3, 0x7f082130

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
