.class public final LX/FpU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;)V
    .locals 0

    .prologue
    .line 2291637
    iput-object p1, p0, LX/FpU;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x1f87c2f6

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2291638
    iget-object v0, p0, LX/FpU;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v1, p0, LX/FpU;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, LX/0ax;->hd:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2291639
    if-eqz v3, :cond_0

    .line 2291640
    const-string v0, "currency"

    iget-object v1, p0, LX/FpU;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2291641
    iget-object v0, p0, LX/FpU;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x7c8

    iget-object v1, p0, LX/FpU;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;

    invoke-virtual {v1}, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v3, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2291642
    :cond_0
    const v0, -0x5d48cf66

    invoke-static {v5, v5, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
