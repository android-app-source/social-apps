.class public LX/G4Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/74S;

.field public final f:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:LX/2rX;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/74S;Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/2rX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2317819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317820
    iput p1, p0, LX/G4Z;->a:I

    .line 2317821
    iput-object p2, p0, LX/G4Z;->b:Ljava/lang/String;

    .line 2317822
    iput-object p3, p0, LX/G4Z;->c:Ljava/lang/String;

    .line 2317823
    iput-object p4, p0, LX/G4Z;->d:Ljava/lang/String;

    .line 2317824
    iput-object p5, p0, LX/G4Z;->e:LX/74S;

    .line 2317825
    iput-object p6, p0, LX/G4Z;->f:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 2317826
    iput-object p7, p0, LX/G4Z;->g:Ljava/lang/String;

    .line 2317827
    iput-object p8, p0, LX/G4Z;->h:Ljava/lang/String;

    .line 2317828
    iput-object p9, p0, LX/G4Z;->i:Ljava/lang/String;

    .line 2317829
    iput-object p10, p0, LX/G4Z;->j:LX/2rX;

    .line 2317830
    iput-object p11, p0, LX/G4Z;->k:Ljava/lang/String;

    .line 2317831
    iput-object p12, p0, LX/G4Z;->l:Ljava/lang/String;

    .line 2317832
    iput-object p13, p0, LX/G4Z;->m:Ljava/lang/String;

    .line 2317833
    iput-object p14, p0, LX/G4Z;->n:Ljava/lang/String;

    .line 2317834
    iput-object p15, p0, LX/G4Z;->o:Ljava/lang/String;

    .line 2317835
    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/74S;Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 2317836
    invoke-direct/range {p0 .. p15}, LX/G4Z;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/74S;Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
