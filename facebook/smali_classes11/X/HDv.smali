.class public final LX/HDv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;)V
    .locals 0

    .prologue
    .line 2441325
    iput-object p1, p0, LX/HDv;->a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x40338469

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2441326
    iget-object v1, p0, LX/HDv;->a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->k:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v1}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2441327
    iget-object v1, p0, LX/HDv;->a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    .line 2441328
    new-instance v3, LX/4I0;

    invoke-direct {v3}, LX/4I0;-><init>()V

    iget-wide v5, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->e:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 2441329
    const-string v5, "page_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2441330
    move-object v3, v3

    .line 2441331
    new-instance v4, LX/HEB;

    invoke-direct {v4}, LX/HEB;-><init>()V

    move-object v4, v4

    .line 2441332
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/HEB;

    .line 2441333
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2441334
    iget-object v3, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-boolean v6, v3, LX/HE1;->a:Z

    .line 2441335
    iget-object v3, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v7, v3, LX/HE1;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2441336
    iget-object v3, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const-string v8, "subscribe_notifications_mutation"

    iget-object v4, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/HDx;

    invoke-direct {v5, v1, v6, v7}, LX/HDx;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    invoke-virtual {v3, v8, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2441337
    :goto_0
    const v1, -0x793b038e

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2441338
    :cond_0
    iget-object v1, p0, LX/HDv;->a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    .line 2441339
    new-instance v3, LX/4I3;

    invoke-direct {v3}, LX/4I3;-><init>()V

    iget-wide v5, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->e:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 2441340
    const-string v5, "page_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2441341
    move-object v3, v3

    .line 2441342
    new-instance v4, LX/HEH;

    invoke-direct {v4}, LX/HEH;-><init>()V

    move-object v4, v4

    .line 2441343
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/HEH;

    .line 2441344
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2441345
    iget-object v3, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-boolean v6, v3, LX/HE1;->a:Z

    .line 2441346
    iget-object v3, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v7, v3, LX/HE1;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2441347
    iget-object v3, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const-string v8, "unsubscribe_notifications_mutation"

    iget-object v4, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/HDy;

    invoke-direct {v5, v1, v6, v7}, LX/HDy;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    invoke-virtual {v3, v8, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2441348
    goto :goto_0
.end method
