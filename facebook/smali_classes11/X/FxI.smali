.class public LX/FxI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:Landroid/os/Bundle;

.field public final c:LX/0ad;

.field public final d:LX/BQ9;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FwN;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/content/Context;

.field public i:Landroid/content/Intent;

.field public j:Landroid/content/Intent;

.field private k:Landroid/view/MenuItem$OnMenuItemClickListener;

.field public l:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private m:Landroid/view/MenuItem$OnMenuItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;LX/0ad;LX/BQ9;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Bundle;",
            "LX/0ad;",
            "LX/BQ9;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FwN;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304647
    invoke-interface {p8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/FxI;->a:J

    .line 2304648
    iput-object p2, p0, LX/FxI;->b:Landroid/os/Bundle;

    .line 2304649
    iput-object p3, p0, LX/FxI;->c:LX/0ad;

    .line 2304650
    iput-object p4, p0, LX/FxI;->d:LX/BQ9;

    .line 2304651
    iput-object p5, p0, LX/FxI;->e:LX/0Or;

    .line 2304652
    iput-object p6, p0, LX/FxI;->f:LX/0Or;

    .line 2304653
    iput-object p1, p0, LX/FxI;->h:Landroid/content/Context;

    .line 2304654
    iput-object p7, p0, LX/FxI;->g:LX/0Ot;

    .line 2304655
    return-void
.end method

.method public static a(LX/FxI;Landroid/support/v4/app/Fragment;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 2304656
    iget-object v0, p0, LX/FxI;->k:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-nez v0, :cond_0

    .line 2304657
    new-instance v0, LX/FxD;

    invoke-direct {v0, p0, p1}, LX/FxD;-><init>(LX/FxI;Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, LX/FxI;->k:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 2304658
    :cond_0
    iget-object v0, p0, LX/FxI;->k:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object v0
.end method

.method public static c(LX/FxI;Landroid/support/v4/app/Fragment;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 2304659
    iget-object v0, p0, LX/FxI;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-nez v0, :cond_0

    .line 2304660
    new-instance v0, LX/FxG;

    invoke-direct {v0, p0, p1}, LX/FxG;-><init>(LX/FxI;Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, LX/FxI;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 2304661
    :cond_0
    iget-object v0, p0, LX/FxI;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object v0
.end method

.method public static d(LX/FxI;Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 2304662
    iget-object v0, p0, LX/FxI;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2304663
    return-void
.end method

.method public static e(LX/FxI;Landroid/support/v4/app/Fragment;)V
    .locals 11

    .prologue
    .line 2304664
    iget-object v0, p0, LX/FxI;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2304665
    iget-object v3, p0, LX/FxI;->i:Landroid/content/Intent;

    if-nez v3, :cond_0

    .line 2304666
    iget-object v3, p0, LX/FxI;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v6, p0, LX/FxI;->h:Landroid/content/Context;

    sget-object v7, LX/0ax;->bZ:Ljava/lang/String;

    iget-wide v9, p0, LX/FxI;->a:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    iput-object v3, p0, LX/FxI;->i:Landroid/content/Intent;

    .line 2304667
    iget-object v3, p0, LX/FxI;->i:Landroid/content/Intent;

    const-string v6, "pick_pic_lite"

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2304668
    iget-object v3, p0, LX/FxI;->i:Landroid/content/Intent;

    const-string v6, "disable_camera_roll"

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2304669
    iget-object v6, p0, LX/FxI;->i:Landroid/content/Intent;

    const-string v7, "disable_tagged_media_set"

    iget-object v3, p0, LX/FxI;->b:Landroid/os/Bundle;

    const-string v8, "has_tagged_mediaset"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2304670
    iget-object v3, p0, LX/FxI;->i:Landroid/content/Intent;

    const-string v4, "land_on_uploads_tab"

    iget-object v6, p0, LX/FxI;->b:Landroid/os/Bundle;

    const-string v7, "land_on_uploads_tab"

    invoke-virtual {v6, v7, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2304671
    iget-object v3, p0, LX/FxI;->i:Landroid/content/Intent;

    const-string v4, "title"

    iget-object v5, p0, LX/FxI;->h:Landroid/content/Context;

    const v6, 0x7f0815f4

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2304672
    :cond_0
    iget-object v3, p0, LX/FxI;->i:Landroid/content/Intent;

    move-object v1, v3

    .line 2304673
    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2304674
    return-void

    :cond_1
    move v3, v5

    .line 2304675
    goto :goto_0
.end method
