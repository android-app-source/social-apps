.class public LX/FhT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field private static volatile i:LX/FhT;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/SearchTypeaheadNetworkExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7BQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fhc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2273043
    const-class v0, LX/FhT;

    sput-object v0, LX/FhT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273045
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273046
    iput-object v0, p0, LX/FhT;->b:LX/0Ot;

    .line 2273047
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273048
    iput-object v0, p0, LX/FhT;->c:LX/0Ot;

    .line 2273049
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273050
    iput-object v0, p0, LX/FhT;->d:LX/0Ot;

    .line 2273051
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273052
    iput-object v0, p0, LX/FhT;->e:LX/0Ot;

    .line 2273053
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273054
    iput-object v0, p0, LX/FhT;->f:LX/0Ot;

    .line 2273055
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273056
    iput-object v0, p0, LX/FhT;->h:LX/0Ot;

    .line 2273057
    return-void
.end method

.method public static a(LX/0QB;)LX/FhT;
    .locals 10

    .prologue
    .line 2273058
    sget-object v0, LX/FhT;->i:LX/FhT;

    if-nez v0, :cond_1

    .line 2273059
    const-class v1, LX/FhT;

    monitor-enter v1

    .line 2273060
    :try_start_0
    sget-object v0, LX/FhT;->i:LX/FhT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273061
    if-eqz v2, :cond_0

    .line 2273062
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273063
    new-instance v3, LX/FhT;

    invoke-direct {v3}, LX/FhT;-><init>()V

    .line 2273064
    const/16 v4, 0xb79

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x277

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xac0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1032

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x34c2

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x32ae

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 p0, 0x34c4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2273065
    iput-object v4, v3, LX/FhT;->b:LX/0Ot;

    iput-object v5, v3, LX/FhT;->c:LX/0Ot;

    iput-object v6, v3, LX/FhT;->d:LX/0Ot;

    iput-object v7, v3, LX/FhT;->e:LX/0Ot;

    iput-object v8, v3, LX/FhT;->f:LX/0Ot;

    iput-object v9, v3, LX/FhT;->g:LX/0Or;

    iput-object p0, v3, LX/FhT;->h:LX/0Ot;

    .line 2273066
    move-object v0, v3

    .line 2273067
    sput-object v0, LX/FhT;->i:LX/FhT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273068
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273069
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273070
    :cond_1
    sget-object v0, LX/FhT;->i:LX/FhT;

    return-object v0

    .line 2273071
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273072
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/FhT;LX/7BZ;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7BZ;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/FhS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2273073
    iget-object v0, p0, LX/FhT;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v1, LX/100;->cl:C

    const-string v2, "10+20"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2273074
    const/4 v2, 0x0

    const/4 p0, 0x1

    const/4 v5, 0x0

    .line 2273075
    :try_start_0
    const-string v1, "\\+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 2273076
    array-length v7, v6

    .line 2273077
    if-lez v7, :cond_4

    .line 2273078
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    move v4, v5

    .line 2273079
    :goto_0
    if-ge v4, v7, :cond_1

    .line 2273080
    aget-object v3, v6, v4

    const-string v8, "\\-"

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 2273081
    array-length v3, v8

    if-le v3, p0, :cond_0

    const/4 v3, 0x1

    aget-object v3, v8, v3

    .line 2273082
    :goto_1
    new-instance v9, LX/FhS;

    const/4 v10, 0x0

    aget-object v8, v8, v10

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-direct {v9, v8, v3}, LX/FhS;-><init>(ILjava/lang/String;)V

    .line 2273083
    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2273084
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    .line 2273085
    goto :goto_1

    .line 2273086
    :catch_0
    move-exception v1

    .line 2273087
    sget-object v3, LX/FhT;->a:Ljava/lang/Class;

    const-string v4, "ERROR while trying to decode batch config %s for streaming TA"

    new-array v6, p0, [Ljava/lang/Object;

    aput-object v0, v6, v5

    invoke-static {v3, v1, v4, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    .line 2273088
    :cond_1
    :goto_2
    move-object v0, v1

    .line 2273089
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2273090
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2273091
    new-instance v1, LX/FhS;

    const/16 v2, 0xa

    invoke-direct {v1, v2, p1}, LX/FhS;-><init>(ILX/7BZ;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2273092
    new-instance v1, LX/FhS;

    const/16 v2, 0x14

    invoke-direct {v1, v2, p1}, LX/FhS;-><init>(ILX/7BZ;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2273093
    :cond_3
    return-object v0

    :cond_4
    move-object v1, v2

    goto :goto_2
.end method
