.class public LX/G15;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/timeline/profileprotocol/HideTimelineStoryMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2310708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310709
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2310700
    check-cast p1, Lcom/facebook/timeline/profileprotocol/HideTimelineStoryMethod$Params;

    .line 2310701
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2310702
    iget-boolean v0, p1, Lcom/facebook/timeline/profileprotocol/HideTimelineStoryMethod$Params;->b:Z

    if-eqz v0, :cond_0

    .line 2310703
    const-string v0, "hidden"

    .line 2310704
    :goto_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "timeline_visibility"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2310705
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2310706
    new-instance v0, LX/14N;

    const-string v1, "hideStory"

    const-string v2, "POST"

    iget-object v3, p1, Lcom/facebook/timeline/profileprotocol/HideTimelineStoryMethod$Params;->a:Ljava/lang/String;

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 2310707
    :cond_0
    const-string v0, "normal"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2310698
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2310699
    const/4 v0, 0x0

    return-object v0
.end method
