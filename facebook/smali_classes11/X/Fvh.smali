.class public final enum LX/Fvh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fvh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fvh;

.field public static final enum BIO:LX/Fvh;

.field public static final enum INTRO_CARD:LX/Fvh;

.field public static final enum MEGAPHONE:LX/Fvh;

.field private static mValues:[LX/Fvh;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2301793
    new-instance v0, LX/Fvh;

    const-string v1, "MEGAPHONE"

    invoke-direct {v0, v1, v2}, LX/Fvh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvh;->MEGAPHONE:LX/Fvh;

    .line 2301794
    new-instance v0, LX/Fvh;

    const-string v1, "BIO"

    invoke-direct {v0, v1, v3}, LX/Fvh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvh;->BIO:LX/Fvh;

    .line 2301795
    new-instance v0, LX/Fvh;

    const-string v1, "INTRO_CARD"

    invoke-direct {v0, v1, v4}, LX/Fvh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvh;->INTRO_CARD:LX/Fvh;

    .line 2301796
    const/4 v0, 0x3

    new-array v0, v0, [LX/Fvh;

    sget-object v1, LX/Fvh;->MEGAPHONE:LX/Fvh;

    aput-object v1, v0, v2

    sget-object v1, LX/Fvh;->BIO:LX/Fvh;

    aput-object v1, v0, v3

    sget-object v1, LX/Fvh;->INTRO_CARD:LX/Fvh;

    aput-object v1, v0, v4

    sput-object v0, LX/Fvh;->$VALUES:[LX/Fvh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2301792
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static cachedValues()[LX/Fvh;
    .locals 1

    .prologue
    .line 2301789
    sget-object v0, LX/Fvh;->mValues:[LX/Fvh;

    if-nez v0, :cond_0

    .line 2301790
    invoke-static {}, LX/Fvh;->values()[LX/Fvh;

    move-result-object v0

    sput-object v0, LX/Fvh;->mValues:[LX/Fvh;

    .line 2301791
    :cond_0
    sget-object v0, LX/Fvh;->mValues:[LX/Fvh;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fvh;
    .locals 1

    .prologue
    .line 2301787
    const-class v0, LX/Fvh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fvh;

    return-object v0
.end method

.method public static values()[LX/Fvh;
    .locals 1

    .prologue
    .line 2301788
    sget-object v0, LX/Fvh;->$VALUES:[LX/Fvh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fvh;

    return-object v0
.end method
