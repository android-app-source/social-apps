.class public final LX/Gf8;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/GfA;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Gf9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GfA",
            "<TE;>.PagesYouMay",
            "LikeComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/GfA;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/GfA;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 2376868
    iput-object p1, p0, LX/Gf8;->b:LX/GfA;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2376869
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pageWidth"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "showHeader"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Gf8;->c:[Ljava/lang/String;

    .line 2376870
    iput v3, p0, LX/Gf8;->d:I

    .line 2376871
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Gf8;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Gf8;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Gf8;LX/1De;IILX/Gf9;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/GfA",
            "<TE;>.PagesYouMay",
            "LikeComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2376872
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2376873
    iput-object p4, p0, LX/Gf8;->a:LX/Gf9;

    .line 2376874
    iget-object v0, p0, LX/Gf8;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2376875
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2376876
    invoke-super {p0}, LX/1X5;->a()V

    .line 2376877
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gf8;->a:LX/Gf9;

    .line 2376878
    iget-object v0, p0, LX/Gf8;->b:LX/GfA;

    iget-object v0, v0, LX/GfA;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2376879
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/GfA;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2376880
    iget-object v1, p0, LX/Gf8;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Gf8;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Gf8;->d:I

    if-ge v1, v2, :cond_2

    .line 2376881
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2376882
    :goto_0
    iget v2, p0, LX/Gf8;->d:I

    if-ge v0, v2, :cond_1

    .line 2376883
    iget-object v2, p0, LX/Gf8;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2376884
    iget-object v2, p0, LX/Gf8;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2376885
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2376886
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2376887
    :cond_2
    iget-object v0, p0, LX/Gf8;->a:LX/Gf9;

    .line 2376888
    invoke-virtual {p0}, LX/Gf8;->a()V

    .line 2376889
    return-object v0
.end method
