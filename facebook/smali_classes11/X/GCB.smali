.class public LX/GCB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2328173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2328174
    iput-object p1, p0, LX/GCB;->a:Landroid/content/res/Resources;

    .line 2328175
    return-void
.end method

.method public static a(LX/0QB;)LX/GCB;
    .locals 1

    .prologue
    .line 2328176
    invoke-static {p0}, LX/GCB;->b(LX/0QB;)LX/GCB;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GCB;
    .locals 2

    .prologue
    .line 2328177
    new-instance v1, LX/GCB;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/GCB;-><init>(Landroid/content/res/Resources;)V

    .line 2328178
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2328179
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 2328180
    iput-boolean v1, v0, LX/108;->n:Z

    .line 2328181
    move-object v0, v0

    .line 2328182
    iput-boolean v1, v0, LX/108;->q:Z

    .line 2328183
    move-object v0, v0

    .line 2328184
    iget-object v1, p0, LX/GCB;->a:Landroid/content/res/Resources;

    const v2, 0x7f080abf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2328185
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2328186
    move-object v0, v0

    .line 2328187
    const v1, 0x7f0300af

    .line 2328188
    iput v1, v0, LX/108;->l:I

    .line 2328189
    move-object v0, v0

    .line 2328190
    iget-object v1, p0, LX/GCB;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2328191
    iput v1, v0, LX/108;->m:I

    .line 2328192
    move-object v0, v0

    .line 2328193
    iget-object v1, p0, LX/GCB;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0123

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2328194
    iput v1, v0, LX/108;->o:I

    .line 2328195
    move-object v0, v0

    .line 2328196
    const/4 v1, -0x2

    .line 2328197
    iput v1, v0, LX/108;->h:I

    .line 2328198
    move-object v0, v0

    .line 2328199
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method
