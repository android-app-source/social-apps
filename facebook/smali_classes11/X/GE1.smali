.class public final enum LX/GE1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GE1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GE1;

.field public static final enum FETCH_BUDGET_RECOMMENDATION:LX/GE1;

.field public static final enum FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2330798
    new-instance v0, LX/GE1;

    const-string v1, "FETCH_BUDGET_RECOMMENDATION"

    invoke-direct {v0, v1, v2}, LX/GE1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    .line 2330799
    new-instance v0, LX/GE1;

    const-string v1, "FETCH_SINGLE_BUDGET_RECOMMENDATION"

    invoke-direct {v0, v1, v3}, LX/GE1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    .line 2330800
    const/4 v0, 0x2

    new-array v0, v0, [LX/GE1;

    sget-object v1, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    aput-object v1, v0, v2

    sget-object v1, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    aput-object v1, v0, v3

    sput-object v0, LX/GE1;->$VALUES:[LX/GE1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2330797
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GE1;
    .locals 1

    .prologue
    .line 2330796
    const-class v0, LX/GE1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GE1;

    return-object v0
.end method

.method public static values()[LX/GE1;
    .locals 1

    .prologue
    .line 2330795
    sget-object v0, LX/GE1;->$VALUES:[LX/GE1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GE1;

    return-object v0
.end method
