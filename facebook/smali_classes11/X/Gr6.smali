.class public final LX/Gr6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public final synthetic d:LX/Gr7;


# direct methods
.method public constructor <init>(LX/Gr7;I)V
    .locals 1

    .prologue
    .line 2397388
    iput-object p1, p0, LX/Gr6;->d:LX/Gr7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2397389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Gr6;->a:Ljava/util/List;

    .line 2397390
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Gr6;->b:Ljava/util/Map;

    .line 2397391
    iput p2, p0, LX/Gr6;->c:I

    .line 2397392
    return-void
.end method

.method private static a(III)I
    .locals 1

    .prologue
    .line 2397369
    add-int v0, p0, p1

    div-int/lit8 v0, v0, 0x2

    .line 2397370
    sub-int/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public static d(LX/Gr6;I)I
    .locals 6

    .prologue
    .line 2397377
    iget-object v0, p0, LX/Gr6;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/Gr6;->c:I

    if-gt v0, v1, :cond_1

    .line 2397378
    const/4 p1, -0x1

    .line 2397379
    :cond_0
    return p1

    .line 2397380
    :cond_1
    iget-object v0, p0, LX/Gr6;->d:LX/Gr7;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v3

    .line 2397381
    iget-object v0, p0, LX/Gr6;->d:LX/Gr7;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v4

    .line 2397382
    invoke-static {v3, v4, p1}, LX/Gr6;->a(III)I

    move-result v0

    .line 2397383
    iget-object v1, p0, LX/Gr6;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2397384
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v3, v4, v1}, LX/Gr6;->a(III)I

    move-result v1

    .line 2397385
    if-le v1, v2, :cond_2

    .line 2397386
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    move v0, v1

    :goto_1
    move v2, v0

    .line 2397387
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/1a1;
    .locals 3

    .prologue
    .line 2397372
    iget-object v0, p0, LX/Gr6;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 2397373
    iget-object v1, p0, LX/Gr6;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2397374
    if-nez v0, :cond_0

    iget-object v1, p0, LX/Gr6;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2397375
    iget-object v0, p0, LX/Gr6;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 2397376
    :cond_0
    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2397371
    iget-object v0, p0, LX/Gr6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gr6;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
