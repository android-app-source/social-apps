.class public LX/GiU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GiU;


# instance fields
.field public a:LX/17W;


# direct methods
.method public constructor <init>(LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2385961
    iput-object p1, p0, LX/GiU;->a:LX/17W;

    .line 2385962
    return-void
.end method

.method public static a(LX/0QB;)LX/GiU;
    .locals 4

    .prologue
    .line 2385943
    sget-object v0, LX/GiU;->b:LX/GiU;

    if-nez v0, :cond_1

    .line 2385944
    const-class v1, LX/GiU;

    monitor-enter v1

    .line 2385945
    :try_start_0
    sget-object v0, LX/GiU;->b:LX/GiU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2385946
    if-eqz v2, :cond_0

    .line 2385947
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2385948
    new-instance p0, LX/GiU;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-direct {p0, v3}, LX/GiU;-><init>(LX/17W;)V

    .line 2385949
    move-object v0, p0

    .line 2385950
    sput-object v0, LX/GiU;->b:LX/GiU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2385951
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2385952
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2385953
    :cond_1
    sget-object v0, LX/GiU;->b:LX/GiU;

    return-object v0

    .line 2385954
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2385955
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Ljava/lang/String;LX/87b;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2385956
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2385957
    const-string v1, "gametime_ref"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2385958
    iget-object v1, p0, LX/GiU;->a:LX/17W;

    invoke-virtual {v1, p3, p1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2385959
    return-void
.end method
