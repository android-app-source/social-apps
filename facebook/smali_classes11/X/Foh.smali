.class public final LX/Foh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 0

    .prologue
    .line 2290041
    iput-object p1, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x4a768454    # 4038933.0f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2290042
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    if-nez v0, :cond_0

    .line 2290043
    const v0, 0x5a5fcd3d

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2290044
    :goto_0
    return-void

    .line 2290045
    :cond_0
    iget-object v0, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290046
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2290047
    iget-object v2, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->g:Landroid/os/ResultReceiver;

    if-eqz v2, :cond_1

    .line 2290048
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2290049
    const-string v3, "fundraiser_charity_id"

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290050
    iget-object v0, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->g:Landroid/os/ResultReceiver;

    invoke-virtual {v0, v4, v2}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 2290051
    iget-object v0, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2290052
    const v0, 0x424da8fd

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2290053
    :cond_1
    iget-object v2, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    .line 2290054
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2290055
    const-string v5, "charity_id"

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290056
    const-string v5, "charity_name"

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290057
    const-string v5, "charity_chosen_enum"

    .line 2290058
    iget-object v7, v3, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-eqz v7, :cond_3

    .line 2290059
    sget-object v7, LX/BOS;->CHARITY_FROM_CATEGORY:LX/BOS;

    .line 2290060
    :goto_1
    move-object v7, v7

    .line 2290061
    invoke-virtual {v6, v5, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2290062
    const-string v7, "is_daf_charity"

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const p1, 0x7fba55ef

    if-ne v5, p1, :cond_2

    const/4 v5, 0x1

    :goto_2
    invoke-virtual {v6, v7, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2290063
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 2290064
    invoke-virtual {v5, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2290065
    move-object v0, v5

    .line 2290066
    invoke-virtual {v2, v4, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2290067
    iget-object v0, p0, LX/Foh;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2290068
    const v0, 0x25fbbb25

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2290069
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 2290070
    :cond_3
    iget-object v7, v3, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v7}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 2290071
    sget-object v7, LX/BOS;->CHARITY_FROM_SEARCH:LX/BOS;

    goto :goto_1

    .line 2290072
    :cond_4
    sget-object v7, LX/BOS;->CHARITY_FROM_NO_FILTER:LX/BOS;

    goto :goto_1
.end method
