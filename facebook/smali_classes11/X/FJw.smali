.class public LX/FJw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field public final a:LX/Dd0;

.field public final b:LX/3Kw;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2Oi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2224192
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FJw;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Dd0;LX/3Kw;LX/0Or;LX/2Oi;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dd0;",
            "LX/3Kw;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Oi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224134
    iput-object p1, p0, LX/FJw;->a:LX/Dd0;

    .line 2224135
    iput-object p2, p0, LX/FJw;->b:LX/3Kw;

    .line 2224136
    iput-object p3, p0, LX/FJw;->c:LX/0Or;

    .line 2224137
    iput-object p4, p0, LX/FJw;->d:LX/2Oi;

    .line 2224138
    return-void
.end method

.method public static a(LX/0QB;)LX/FJw;
    .locals 10

    .prologue
    .line 2224163
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2224164
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2224165
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2224166
    if-nez v1, :cond_0

    .line 2224167
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2224168
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2224169
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2224170
    sget-object v1, LX/FJw;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2224171
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2224172
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2224173
    :cond_1
    if-nez v1, :cond_4

    .line 2224174
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2224175
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2224176
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2224177
    new-instance v9, LX/FJw;

    invoke-static {v0}, LX/Dd0;->b(LX/0QB;)LX/Dd0;

    move-result-object v1

    check-cast v1, LX/Dd0;

    invoke-static {v0}, LX/3Kw;->b(LX/0QB;)LX/3Kw;

    move-result-object v7

    check-cast v7, LX/3Kw;

    const/16 v8, 0x1453

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v8

    check-cast v8, LX/2Oi;

    invoke-direct {v9, v1, v7, p0, v8}, LX/FJw;-><init>(LX/Dd0;LX/3Kw;LX/0Or;LX/2Oi;)V

    .line 2224178
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2224179
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2224180
    if-nez v1, :cond_2

    .line 2224181
    sget-object v0, LX/FJw;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJw;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2224182
    :goto_1
    if-eqz v0, :cond_3

    .line 2224183
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2224184
    :goto_3
    check-cast v0, LX/FJw;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2224185
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2224186
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2224187
    :catchall_1
    move-exception v0

    .line 2224188
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2224189
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2224190
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2224191
    :cond_2
    :try_start_8
    sget-object v0, LX/FJw;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJw;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;)LX/8t9;
    .locals 2

    .prologue
    .line 2224153
    invoke-virtual {p0, p1}, LX/FJw;->b(Lcom/facebook/user/model/User;)LX/8ue;

    move-result-object v0

    .line 2224154
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->ax()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2224155
    iget-object v1, p1, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v1, v1

    .line 2224156
    if-nez v1, :cond_0

    .line 2224157
    invoke-static {p1, v0}, LX/8t9;->b(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object v0

    .line 2224158
    :goto_0
    return-object v0

    .line 2224159
    :cond_0
    iget-object v1, p1, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v1, v1

    .line 2224160
    iget-object p0, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, p0

    .line 2224161
    invoke-static {v1, v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v0

    goto :goto_0

    .line 2224162
    :cond_1
    invoke-static {p1, v0}, LX/8t9;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/user/model/User;)LX/8ue;
    .locals 1

    .prologue
    .line 2224139
    iget-object v0, p0, LX/FJw;->a:LX/Dd0;

    invoke-virtual {v0}, LX/Dd0;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2224140
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    .line 2224141
    :goto_0
    return-object v0

    .line 2224142
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->A:Z

    move v0, v0

    .line 2224143
    if-eqz v0, :cond_1

    .line 2224144
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    goto :goto_0

    .line 2224145
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->s:Z

    move v0, v0

    .line 2224146
    if-eqz v0, :cond_2

    .line 2224147
    sget-object v0, LX/8ue;->MESSENGER:LX/8ue;

    goto :goto_0

    .line 2224148
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2224149
    sget-object v0, LX/8ue;->SMS:LX/8ue;

    goto :goto_0

    .line 2224150
    :cond_3
    iget-object v0, p0, LX/FJw;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2224151
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->F:Z

    move v0, v0

    .line 2224152
    if-eqz v0, :cond_5

    :cond_4
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    goto :goto_0

    :cond_5
    sget-object v0, LX/8ue;->FACEBOOK:LX/8ue;

    goto :goto_0
.end method
