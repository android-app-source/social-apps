.class public final LX/Fy0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public final synthetic c:Z

.field public final synthetic d:J

.field public final synthetic e:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic f:LX/Fy4;


# direct methods
.method public constructor <init>(LX/Fy4;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZJLcom/google/common/util/concurrent/SettableFuture;)V
    .locals 1

    .prologue
    .line 2306191
    iput-object p1, p0, LX/Fy0;->f:LX/Fy4;

    iput-boolean p2, p0, LX/Fy0;->a:Z

    iput-object p3, p0, LX/Fy0;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-boolean p4, p0, LX/Fy0;->c:Z

    iput-wide p5, p0, LX/Fy0;->d:J

    iput-object p7, p0, LX/Fy0;->e:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2306192
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->h:LX/2do;

    new-instance v1, LX/84Z;

    iget-wide v2, p0, LX/Fy0;->d:J

    iget-object v4, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v4, v4, LX/Fy4;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v5, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v5, v5, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/84Z;-><init>(JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2306193
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->e:LX/BQ1;

    iget-object v1, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v1, v1, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v0, v1}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2306194
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->e:LX/BQ1;

    iget-object v1, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v1, v1, LX/Fy4;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0, v1}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2306195
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    invoke-static {v0}, LX/Fy4;->c(LX/Fy4;)V

    .line 2306196
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 2306197
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->lk_()V

    .line 2306198
    :cond_0
    iget-boolean v0, p0, LX/Fy0;->a:Z

    if-eqz v0, :cond_1

    const v0, 0x7f081588

    move v1, v0

    .line 2306199
    :goto_0
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2306200
    iget-object v0, p0, LX/Fy0;->e:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2306201
    return-void

    .line 2306202
    :cond_1
    const v0, 0x7f081589

    move v1, v0

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2306203
    check-cast p1, Ljava/lang/Void;

    .line 2306204
    iget-boolean v0, p0, LX/Fy0;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iget-object v1, p0, LX/Fy0;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-eq v0, v1, :cond_0

    .line 2306205
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-boolean v1, p0, LX/Fy0;->c:Z

    iget-wide v2, p0, LX/Fy0;->d:J

    iget-object v4, p0, LX/Fy0;->e:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v0, v1, v2, v3, v4}, LX/Fy4;->a$redex0(LX/Fy4;ZJLcom/google/common/util/concurrent/SettableFuture;)V

    .line 2306206
    :goto_0
    return-void

    .line 2306207
    :cond_0
    iget-object v0, p0, LX/Fy0;->f:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->lk_()V

    .line 2306208
    iget-object v0, p0, LX/Fy0;->e:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x35c6e1fa    # -3032961.5f

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0
.end method
