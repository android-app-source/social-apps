.class public final LX/FF1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FFA;

.field public final synthetic b:LX/FF2;


# direct methods
.method public constructor <init>(LX/FF2;LX/FFA;)V
    .locals 0

    .prologue
    .line 2216829
    iput-object p1, p0, LX/FF1;->b:LX/FF2;

    iput-object p2, p0, LX/FF1;->a:LX/FFA;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2216830
    iget-object v0, p0, LX/FF1;->a:LX/FFA;

    invoke-virtual {v0}, LX/FFA;->a()V

    .line 2216831
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2216801
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2216802
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2216803
    if-eqz v0, :cond_2

    .line 2216804
    iget-object v1, p0, LX/FF1;->a:LX/FFA;

    .line 2216805
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2216806
    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel;

    move-result-object v0

    .line 2216807
    iget-object v2, v1, LX/FFA;->a:LX/FFB;

    .line 2216808
    const/4 v5, 0x0

    .line 2216809
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 2216810
    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel;->a()I

    .line 2216811
    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel;->j()LX/0Px;

    move-result-object v8

    .line 2216812
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p0

    move v6, v5

    :goto_0
    if-ge v6, p0, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;

    .line 2216813
    invoke-virtual {v3}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2216814
    invoke-virtual {v3}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->j()Z

    move-result p1

    .line 2216815
    new-instance v1, LX/FFG;

    invoke-direct {v1, v4, p1}, LX/FFG;-><init>(Ljava/lang/String;Z)V

    .line 2216816
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2216817
    invoke-virtual {v3}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    move-result-object v3

    .line 2216818
    invoke-virtual {v3}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    move v4, v5

    :goto_1
    if-ge v4, v1, :cond_0

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;

    .line 2216819
    invoke-static {v7, v3}, LX/FF3;->a(Ljava/util/List;Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;)V

    .line 2216820
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2216821
    :cond_0
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_0

    .line 2216822
    :cond_1
    move-object v3, v7

    .line 2216823
    iget-object v4, v2, LX/FFB;->b:LX/FF4;

    .line 2216824
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/FF4;->b:LX/0Px;

    .line 2216825
    invoke-virtual {v4}, LX/1OM;->notifyDataSetChanged()V

    .line 2216826
    iget-object v3, v2, LX/FFB;->e:LX/FEX;

    invoke-virtual {v3}, LX/FEX;->b()V

    .line 2216827
    :goto_2
    return-void

    .line 2216828
    :cond_2
    iget-object v0, p0, LX/FF1;->a:LX/FFA;

    invoke-virtual {v0}, LX/FFA;->a()V

    goto :goto_2
.end method
