.class public LX/GvX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/Context;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2405601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2405602
    iput-object p1, p0, LX/GvX;->a:LX/0Or;

    .line 2405603
    iput-object p2, p0, LX/GvX;->b:Landroid/content/Context;

    .line 2405604
    return-void
.end method

.method public static a(LX/0QB;)LX/GvX;
    .locals 5

    .prologue
    .line 2405605
    const-class v1, LX/GvX;

    monitor-enter v1

    .line 2405606
    :try_start_0
    sget-object v0, LX/GvX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2405607
    sput-object v2, LX/GvX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2405608
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2405609
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2405610
    new-instance v4, LX/GvX;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, p0, v3}, LX/GvX;-><init>(LX/0Or;Landroid/content/Context;)V

    .line 2405611
    move-object v0, v4

    .line 2405612
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2405613
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GvX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2405614
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2405615
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/GvY;)Lcom/facebook/katana/gdp/PermissionItem;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 2405616
    sget-object v0, LX/GvW;->a:[I

    invoke-virtual {p1}, LX/GvY;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2405617
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PermissionType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not handled in factory, please add a case for it."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2405618
    :pswitch_0
    iget-object v0, p0, LX/GvX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2405619
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v4

    .line 2405620
    iget-object v2, v0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    move-object v2, v2

    .line 2405621
    sget-object v3, LX/0XJ;->UNKNOWN:LX/0XJ;

    if-ne v2, v3, :cond_0

    iget-object v0, p0, LX/GvX;->b:Landroid/content/Context;

    const v2, 0x7f083607

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2405622
    :goto_0
    new-instance v0, Lcom/facebook/katana/gdp/PermissionItem;

    iget-object v3, p0, LX/GvX;->b:Landroid/content/Context;

    const v5, 0x7f083605

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, LX/GvX;->b:Landroid/content/Context;

    const v6, 0x7f08360a

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v8

    aput-object v2, v7, v1

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, LX/GvX;->b:Landroid/content/Context;

    const v5, 0x7f08360c

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/katana/gdp/PermissionItem;-><init>(ZLX/GvY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2405623
    :goto_1
    return-object v0

    .line 2405624
    :cond_0
    iget-object v2, v0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    move-object v0, v2

    .line 2405625
    invoke-virtual {v0}, LX/0XJ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 2405626
    :pswitch_1
    new-instance v2, Lcom/facebook/katana/gdp/PermissionItem;

    iget-object v0, p0, LX/GvX;->b:Landroid/content/Context;

    const v3, 0x7f083604

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, LX/GvX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, LX/GvX;->b:Landroid/content/Context;

    const v3, 0x7f08360d

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move v3, v8

    move-object v4, p1

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/facebook/katana/gdp/PermissionItem;-><init>(ZLX/GvY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
