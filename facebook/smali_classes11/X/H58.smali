.class public LX/H58;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/H55;


# instance fields
.field public b:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

.field public final e:LX/H55;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rn;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2423622
    new-instance v0, LX/H56;

    invoke-direct {v0}, LX/H56;-><init>()V

    sput-object v0, LX/H58;->a:LX/H55;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;LX/0xW;LX/0Ot;LX/H55;)V
    .locals 2
    .param p1    # Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/H55;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLInterfaces$NotificationsBucketFields;",
            "LX/0xW;",
            "LX/0Ot",
            "<",
            "LX/1rn;",
            ">;",
            "LX/H55;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2423633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423634
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/H58;->c:Ljava/util/List;

    .line 2423635
    iput-object p1, p0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    .line 2423636
    iput-object p4, p0, LX/H58;->e:LX/H55;

    .line 2423637
    iput-object p3, p0, LX/H58;->f:LX/0Ot;

    .line 2423638
    invoke-virtual {p2}, LX/0xW;->G()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/H58;->g:Ljava/lang/Long;

    .line 2423639
    return-void
.end method

.method public static a(LX/H58;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    .line 2423623
    iget-object v1, p0, LX/H58;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2423624
    :cond_0
    :goto_0
    return v0

    .line 2423625
    :cond_1
    iget-object v1, p0, LX/H58;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2423626
    sget-object v6, LX/1ro;->a:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2423627
    sget-object v6, LX/1ro;->a:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    .line 2423628
    :goto_1
    move-object v1, v6

    .line 2423629
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 2423630
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 2423631
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2423632
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object v1, p0, LX/H58;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto :goto_1
.end method
