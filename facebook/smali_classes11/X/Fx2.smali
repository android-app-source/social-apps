.class public LX/Fx2;
.super LX/Fwg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Fwg",
        "<",
        "LX/3l7;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile b:LX/Fx2;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2304168
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Fx2;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304169
    invoke-direct {p0, p1}, LX/Fwg;-><init>(LX/0iA;)V

    .line 2304170
    return-void
.end method

.method public static a(LX/0QB;)LX/Fx2;
    .locals 4

    .prologue
    .line 2304171
    sget-object v0, LX/Fx2;->b:LX/Fx2;

    if-nez v0, :cond_1

    .line 2304172
    const-class v1, LX/Fx2;

    monitor-enter v1

    .line 2304173
    :try_start_0
    sget-object v0, LX/Fx2;->b:LX/Fx2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2304174
    if-eqz v2, :cond_0

    .line 2304175
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2304176
    new-instance p0, LX/Fx2;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-direct {p0, v3}, LX/Fx2;-><init>(LX/0iA;)V

    .line 2304177
    move-object v0, p0

    .line 2304178
    sput-object v0, LX/Fx2;->b:LX/Fx2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2304179
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2304180
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2304181
    :cond_1
    sget-object v0, LX/Fx2;->b:LX/Fx2;

    return-object v0

    .line 2304182
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2304183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2304184
    const-string v0, "4238"

    return-object v0
.end method

.method public final b()Lcom/facebook/interstitial/manager/InterstitialTrigger;
    .locals 1

    .prologue
    .line 2304185
    sget-object v0, LX/Fx2;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-object v0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 2304186
    const-class v0, LX/3l7;

    return-object v0
.end method
