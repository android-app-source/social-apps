.class public LX/Fbl;
.super LX/FbF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private static volatile b:LX/Fbl;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2261036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x78647b46

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, LX/Fbl;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261034
    invoke-direct {p0}, LX/FbF;-><init>()V

    .line 2261035
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbl;
    .locals 3

    .prologue
    .line 2261037
    sget-object v0, LX/Fbl;->b:LX/Fbl;

    if-nez v0, :cond_1

    .line 2261038
    const-class v1, LX/Fbl;

    monitor-enter v1

    .line 2261039
    :try_start_0
    sget-object v0, LX/Fbl;->b:LX/Fbl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261040
    if-eqz v2, :cond_0

    .line 2261041
    :try_start_1
    new-instance v0, LX/Fbl;

    invoke-direct {v0}, LX/Fbl;-><init>()V

    .line 2261042
    move-object v0, v0

    .line 2261043
    sput-object v0, LX/Fbl;->b:LX/Fbl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261044
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261045
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261046
    :cond_1
    sget-object v0, LX/Fbl;->b:LX/Fbl;

    return-object v0

    .line 2261047
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261048
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261033
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)LX/0Px;
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$ModuleResultEdge;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2261022
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261023
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hy()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hy()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hy()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2261024
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2261025
    :goto_0
    return-object v0

    .line 2261026
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2261027
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hy()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2261028
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;

    .line 2261029
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2261030
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2261031
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2261032
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261021
    sget-object v0, LX/Fbl;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261020
    const/4 v0, 0x0

    return-object v0
.end method
