.class public LX/Go1;
.super LX/Ci8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private a:Z

.field public b:LX/Chv;

.field private final c:LX/Go0;


# direct methods
.method public constructor <init>(LX/Chv;LX/Go0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393969
    invoke-direct {p0}, LX/Ci8;-><init>()V

    .line 2393970
    iput-object p1, p0, LX/Go1;->b:LX/Chv;

    .line 2393971
    iput-object p2, p0, LX/Go1;->c:LX/Go0;

    .line 2393972
    return-void
.end method

.method public static a(LX/0QB;)LX/Go1;
    .locals 5

    .prologue
    .line 2393973
    const-class v1, LX/Go1;

    monitor-enter v1

    .line 2393974
    :try_start_0
    sget-object v0, LX/Go1;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2393975
    sput-object v2, LX/Go1;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2393976
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393977
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2393978
    new-instance p0, LX/Go1;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v3

    check-cast v3, LX/Chv;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v4

    check-cast v4, LX/Go0;

    invoke-direct {p0, v3, v4}, LX/Go1;-><init>(LX/Chv;LX/Go0;)V

    .line 2393979
    move-object v0, p0

    .line 2393980
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2393981
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Go1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393982
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2393983
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2393984
    check-cast p1, LX/Cic;

    .line 2393985
    iget-boolean v0, p0, LX/Go1;->a:Z

    if-nez v0, :cond_0

    .line 2393986
    iget v0, p1, LX/Cic;->b:I

    move v0, v0

    .line 2393987
    if-lez v0, :cond_0

    .line 2393988
    iget-object v0, p0, LX/Go1;->c:LX/Go0;

    const-string v1, "instant_shopping_did_scroll"

    invoke-virtual {v0, v1}, LX/Go0;->a(Ljava/lang/String;)V

    .line 2393989
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Go1;->a:Z

    .line 2393990
    :cond_0
    return-void
.end method
