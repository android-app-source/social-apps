.class public final LX/H5w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/H6T;

.field private final b:LX/H60;

.field private final c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/0Zb;

.field public final f:LX/H82;

.field private g:Landroid/content/Context;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/H6T;LX/H60;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/H82;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H6T;",
            "LX/H60;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Zb;",
            "LX/H82;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2425644
    iput-object p1, p0, LX/H5w;->a:LX/H6T;

    .line 2425645
    iput-object p2, p0, LX/H5w;->b:LX/H60;

    .line 2425646
    iput-object p3, p0, LX/H5w;->c:LX/0Or;

    .line 2425647
    iput-object p4, p0, LX/H5w;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2425648
    iput-object p5, p0, LX/H5w;->e:LX/0Zb;

    .line 2425649
    iput-object p6, p0, LX/H5w;->f:LX/H82;

    .line 2425650
    return-void
.end method

.method public static a(LX/0QB;)LX/H5w;
    .locals 1

    .prologue
    .line 2425642
    invoke-static {p0}, LX/H5w;->b(LX/0QB;)LX/H5w;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/H5w;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2425640
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/H5w;->a(LX/H5w;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2425641
    return-void
.end method

.method public static a(LX/H5w;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2425630
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2425631
    const-string v1, "CLAIM_STATUS"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425632
    const-string v1, "IS_OMNI_CHANNEL"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2425633
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2425634
    const-string v1, "UNIQUE_CODE"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425635
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/H5w;->g:Landroid/content/Context;

    const-class v3, Lcom/facebook/browser/lite/BrowserLiteIntentService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2425636
    const-string v2, "EXTRA_ACTION"

    const-string v3, "ACTION_UPDATE_OFFERS_BAR"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2425637
    const-string v2, "OFFERS_BUNDLE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2425638
    iget-object v0, p0, LX/H5w;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H5w;->g:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2425639
    return-void
.end method

.method public static b(LX/0QB;)LX/H5w;
    .locals 7

    .prologue
    .line 2425612
    new-instance v0, LX/H5w;

    invoke-static {p0}, LX/H6T;->a(LX/0QB;)LX/H6T;

    move-result-object v1

    check-cast v1, LX/H6T;

    invoke-static {p0}, LX/H60;->a(LX/0QB;)LX/H60;

    move-result-object v2

    check-cast v2, LX/H60;

    const/16 v3, 0x15e7

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {p0}, LX/H82;->b(LX/0QB;)LX/H82;

    move-result-object v6

    check-cast v6, LX/H82;

    invoke-direct/range {v0 .. v6}, LX/H5w;-><init>(LX/H6T;LX/H60;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/H82;)V

    .line 2425613
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2425614
    const-string v0, "offer_view_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2425615
    const-string v0, "share_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2425616
    const-string v0, "claim_type"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/H5w;->h:Ljava/lang/String;

    .line 2425617
    const-string v0, "notif_trigger"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/H5w;->i:Ljava/lang/String;

    .line 2425618
    const-string v0, "notif_medium"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/H5w;->j:Ljava/lang/String;

    .line 2425619
    const-string v0, "rule"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/H5w;->k:Ljava/lang/String;

    .line 2425620
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2425621
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2425622
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 2425623
    iput-object p1, p0, LX/H5w;->g:Landroid/content/Context;

    .line 2425624
    iget-object v1, p0, LX/H5w;->b:LX/H60;

    iget-object v0, p0, LX/H5w;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v2, v0}, LX/H60;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2425625
    new-instance v1, LX/H5v;

    invoke-direct {v1, p0}, LX/H5v;-><init>(LX/H5w;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2425626
    return-void

    :cond_0
    move v0, v2

    .line 2425627
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2425628
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2425629
    goto :goto_2
.end method
