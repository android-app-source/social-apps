.class public final LX/Gyw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GA2;


# instance fields
.field public final synthetic a:Lcom/facebook/login/DeviceAuthDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/login/DeviceAuthDialog;)V
    .locals 0

    .prologue
    .line 2410663
    iput-object p1, p0, LX/Gyw;->a:Lcom/facebook/login/DeviceAuthDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/GAY;)V
    .locals 5

    .prologue
    .line 2410664
    iget-object v0, p1, LX/GAY;->d:LX/GAF;

    move-object v0, v0

    .line 2410665
    if-eqz v0, :cond_0

    .line 2410666
    iget-object v0, p0, LX/Gyw;->a:Lcom/facebook/login/DeviceAuthDialog;

    .line 2410667
    iget-object v1, p1, LX/GAY;->d:LX/GAF;

    move-object v1, v1

    .line 2410668
    iget-object v2, v1, LX/GAF;->o:LX/GAA;

    move-object v1, v2

    .line 2410669
    invoke-static {v0, v1}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;LX/GAA;)V

    .line 2410670
    :goto_0
    return-void

    .line 2410671
    :cond_0
    iget-object v0, p1, LX/GAY;->b:Lorg/json/JSONObject;

    move-object v0, v0

    .line 2410672
    new-instance v1, Lcom/facebook/login/DeviceAuthDialog$RequestState;

    invoke-direct {v1}, Lcom/facebook/login/DeviceAuthDialog$RequestState;-><init>()V

    .line 2410673
    :try_start_0
    const-string v2, "user_code"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2410674
    iput-object v2, v1, Lcom/facebook/login/DeviceAuthDialog$RequestState;->a:Ljava/lang/String;

    .line 2410675
    const-string v2, "code"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2410676
    iput-object v2, v1, Lcom/facebook/login/DeviceAuthDialog$RequestState;->b:Ljava/lang/String;

    .line 2410677
    const-string v2, "interval"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2410678
    iput-wide v2, v1, Lcom/facebook/login/DeviceAuthDialog$RequestState;->c:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2410679
    iget-object v0, p0, LX/Gyw;->a:Lcom/facebook/login/DeviceAuthDialog;

    invoke-static {v0, v1}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;Lcom/facebook/login/DeviceAuthDialog$RequestState;)V

    goto :goto_0

    .line 2410680
    :catch_0
    move-exception v0

    .line 2410681
    iget-object v1, p0, LX/Gyw;->a:Lcom/facebook/login/DeviceAuthDialog;

    new-instance v2, LX/GAA;

    invoke-direct {v2, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;LX/GAA;)V

    goto :goto_0
.end method
