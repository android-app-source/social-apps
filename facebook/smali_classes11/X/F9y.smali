.class public LX/F9y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/F9y;


# instance fields
.field public final a:LX/F9x;


# direct methods
.method public constructor <init>(LX/F9x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206507
    iput-object p1, p0, LX/F9y;->a:LX/F9x;

    .line 2206508
    return-void
.end method

.method public static a(LX/0QB;)LX/F9y;
    .locals 4

    .prologue
    .line 2206509
    sget-object v0, LX/F9y;->b:LX/F9y;

    if-nez v0, :cond_1

    .line 2206510
    const-class v1, LX/F9y;

    monitor-enter v1

    .line 2206511
    :try_start_0
    sget-object v0, LX/F9y;->b:LX/F9y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206512
    if-eqz v2, :cond_0

    .line 2206513
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206514
    new-instance p0, LX/F9y;

    invoke-static {v0}, LX/F9x;->a(LX/0QB;)LX/F9x;

    move-result-object v3

    check-cast v3, LX/F9x;

    invoke-direct {p0, v3}, LX/F9y;-><init>(LX/F9x;)V

    .line 2206515
    move-object v0, p0

    .line 2206516
    sput-object v0, LX/F9y;->b:LX/F9y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206517
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206518
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206519
    :cond_1
    sget-object v0, LX/F9y;->b:LX/F9y;

    return-object v0

    .line 2206520
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206521
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
