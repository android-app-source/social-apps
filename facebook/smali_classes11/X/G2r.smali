.class public final LX/G2r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;)V
    .locals 0

    .prologue
    .line 2314564
    iput-object p1, p0, LX/G2r;->a:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const v0, -0x73488f34

    invoke-static {v7, v8, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2314565
    iget-object v1, p0, LX/G2r;->a:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->b:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->cW:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, LX/5Oz;->TIMELINE_PYMK:LX/5Oz;

    invoke-virtual {v6}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    sget-object v5, LX/5P0;->SUGGESTIONS:LX/5P0;

    invoke-virtual {v5}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2314566
    const v1, 0x19f35a91

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
