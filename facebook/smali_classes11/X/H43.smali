.class public abstract LX/H43;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2422253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2422254
    return-void
.end method

.method public static a(LX/H44;Landroid/content/res/Resources;)Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;
    .locals 3

    .prologue
    .line 2422255
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422256
    sget-object v0, LX/H42;->a:[I

    invoke-virtual {p0}, LX/H44;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2422257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2422258
    :pswitch_0
    const-string v1, ""

    .line 2422259
    const v0, 0x7f0820e0

    move-object v2, v1

    move v1, v0

    .line 2422260
    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422261
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2422262
    new-instance v1, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-direct {v1, v0, v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 2422263
    :pswitch_1
    const-string v1, "Restaurants"

    .line 2422264
    const v0, 0x7f0820e1

    move-object v2, v1

    move v1, v0

    .line 2422265
    goto :goto_0

    .line 2422266
    :pswitch_2
    const-string v1, "Coffee"

    .line 2422267
    const v0, 0x7f0820e2

    move-object v2, v1

    move v1, v0

    .line 2422268
    goto :goto_0

    .line 2422269
    :pswitch_3
    const-string v1, "Nightlife"

    .line 2422270
    const v0, 0x7f0820e3

    move-object v2, v1

    move v1, v0

    .line 2422271
    goto :goto_0

    .line 2422272
    :pswitch_4
    const-string v1, "Outdoors"

    .line 2422273
    const v0, 0x7f0820e4

    move-object v2, v1

    move v1, v0

    .line 2422274
    goto :goto_0

    .line 2422275
    :pswitch_5
    const-string v1, "Arts"

    .line 2422276
    const v0, 0x7f0820e5

    move-object v2, v1

    move v1, v0

    .line 2422277
    goto :goto_0

    .line 2422278
    :pswitch_6
    const-string v1, "Hotels"

    .line 2422279
    const v0, 0x7f0820e6

    move-object v2, v1

    move v1, v0

    .line 2422280
    goto :goto_0

    .line 2422281
    :pswitch_7
    const-string v1, "Shopping"

    .line 2422282
    const v0, 0x7f0820e7

    move-object v2, v1

    move v1, v0

    .line 2422283
    goto :goto_0

    .line 2422284
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
