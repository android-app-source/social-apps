.class public final LX/Gab;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2368350
    iput-object p1, p0, LX/Gab;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iput-object p2, p0, LX/Gab;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2368351
    iget-object v0, p0, LX/Gab;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kR;

    const/4 v1, 0x1

    new-instance v2, LX/Gaa;

    invoke-direct {v2, p0}, LX/Gaa;-><init>(LX/Gab;)V

    invoke-virtual {v0, v1, v2}, LX/1kR;->a(ZLX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2368352
    invoke-direct {p0}, LX/Gab;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
