.class public LX/FIg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;",
        "Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2222480
    check-cast p1, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;

    .line 2222481
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "udp_upload"

    .line 2222482
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2222483
    move-object v0, v0

    .line 2222484
    const-string v1, "POST"

    .line 2222485
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2222486
    move-object v0, v0

    .line 2222487
    const-string v1, "/me/udp_uploads"

    .line 2222488
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2222489
    move-object v0, v0

    .line 2222490
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2222491
    iget-object v3, p1, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2222492
    new-instance v4, Ljava/io/File;

    iget-object v5, v3, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2222493
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "entity_name"

    invoke-direct {v5, v6, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222494
    iget-object v4, v3, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v5, LX/2MK;->PHOTO:LX/2MK;

    if-ne v4, v5, :cond_1

    .line 2222495
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "image_type"

    .line 2222496
    iget-object v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v6}, LX/5zj;->isQuickCamSource()Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v6, LX/5dT;->QUICKCAM:LX/5dT;

    iget-object v6, v6, LX/5dT;->apiStringValue:Ljava/lang/String;

    :goto_0
    move-object v6, v6

    .line 2222497
    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222498
    :cond_0
    :goto_1
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "mime_type"

    iget-object v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222499
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "media_hash"

    iget-object v6, p1, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222500
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "payload_size"

    iget-wide v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222501
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_id"

    iget v5, p1, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222502
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "user_id"

    iget-wide v6, p1, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222503
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2222504
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2222505
    move-object v0, v0

    .line 2222506
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2222507
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2222508
    move-object v0, v0

    .line 2222509
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2222510
    :cond_1
    iget-object v4, v3, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v5, LX/2MK;->VIDEO:LX/2MK;

    if-ne v4, v5, :cond_0

    .line 2222511
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "video_type"

    .line 2222512
    invoke-static {v3}, LX/6eg;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5dX;

    move-result-object v6

    iget-object v6, v6, LX/5dX;->apiStringValue:Ljava/lang/String;

    move-object v6, v6

    .line 2222513
    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    :cond_2
    sget-object v6, LX/5dT;->NONQUICKCAM:LX/5dT;

    iget-object v6, v6, LX/5dT;->apiStringValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2222479
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->a(LX/0lF;)Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;

    move-result-object v0

    return-object v0
.end method
