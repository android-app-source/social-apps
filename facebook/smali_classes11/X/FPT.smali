.class public final LX/FPT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;)V
    .locals 0

    .prologue
    .line 2237245
    iput-object p1, p0, LX/FPT;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2237257
    add-int v0, p2, p3

    .line 2237258
    if-ne v0, p4, :cond_0

    iget-object v0, p0, LX/FPT;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->u:LX/FPW;

    sget-object v1, LX/FPW;->LOADING_PAGINATION:LX/FPW;

    if-eq v0, v1, :cond_0

    .line 2237259
    iget-object v0, p0, LX/FPT;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237260
    iget-object v0, p0, LX/FPT;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237261
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v1

    .line 2237262
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    move v0, v1

    .line 2237263
    if-eqz v0, :cond_0

    .line 2237264
    iget-object v0, p0, LX/FPT;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v1, LX/FP6;->PAGINATION_REQUEST:LX/FP6;

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FP6;)V

    .line 2237265
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 2237246
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 2237247
    iget-object v0, p0, LX/FPT;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->v:LX/FPa;

    .line 2237248
    iget-object v1, v0, LX/FPa;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    if-nez v1, :cond_1

    .line 2237249
    :cond_0
    :goto_0
    return-void

    .line 2237250
    :cond_1
    iget-object v1, v0, LX/FPa;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237251
    iget-object p0, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, p0

    .line 2237252
    iget-object p0, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->a:LX/CQB;

    move-object v1, p0

    .line 2237253
    const-string p0, "nearby_places_scroll_results"

    invoke-static {v0, p0}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2237254
    sget-object p1, LX/CQB;->SEARCH_SUGGESTION:LX/CQB;

    if-eq p1, v1, :cond_2

    .line 2237255
    const-string v1, "mechanism"

    const-string p1, "result_list"

    invoke-virtual {p0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p1, "event_target"

    const-string p2, "result_list"

    invoke-virtual {v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p1, "event_type"

    const-string p2, "list_scroll"

    invoke-virtual {v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237256
    :cond_2
    iget-object v1, v0, LX/FPa;->a:LX/0Zb;

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
