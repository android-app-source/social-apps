.class public LX/FWL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FWL;


# instance fields
.field private final a:LX/79a;


# direct methods
.method public constructor <init>(LX/79a;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252027
    iput-object p1, p0, LX/FWL;->a:LX/79a;

    .line 2252028
    return-void
.end method

.method public static a(LX/0am;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2252029
    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2252030
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2252031
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2252032
    iget-object p0, v0, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v0, p0

    .line 2252033
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FWL;
    .locals 4

    .prologue
    .line 2252034
    sget-object v0, LX/FWL;->b:LX/FWL;

    if-nez v0, :cond_1

    .line 2252035
    const-class v1, LX/FWL;

    monitor-enter v1

    .line 2252036
    :try_start_0
    sget-object v0, LX/FWL;->b:LX/FWL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2252037
    if-eqz v2, :cond_0

    .line 2252038
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2252039
    new-instance p0, LX/FWL;

    invoke-static {v0}, LX/79a;->a(LX/0QB;)LX/79a;

    move-result-object v3

    check-cast v3, LX/79a;

    invoke-direct {p0, v3}, LX/FWL;-><init>(LX/79a;)V

    .line 2252040
    move-object v0, p0

    .line 2252041
    sput-object v0, LX/FWL;->b:LX/FWL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252042
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2252043
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2252044
    :cond_1
    sget-object v0, LX/FWL;->b:LX/FWL;

    return-object v0

    .line 2252045
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2252046
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2252047
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v0

    .line 2252048
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    if-ne v0, v1, :cond_1

    .line 2252049
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2252050
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)LX/0am;
    .locals 6
    .param p1    # Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ")",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2252051
    iget-object v0, p0, LX/FWL;->a:LX/79a;

    invoke-virtual {v0}, LX/79a;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2252052
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-static {v4}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v4

    .line 2252053
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne p1, v4, :cond_0

    .line 2252054
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2252055
    :goto_1
    return-object v0

    .line 2252056
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2252057
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_1
.end method
