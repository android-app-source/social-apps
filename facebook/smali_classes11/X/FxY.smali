.class public final LX/FxY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V
    .locals 0

    .prologue
    .line 2305101
    iput-object p1, p0, LX/FxY;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x36ae616c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2305095
    iget-object v0, p0, LX/FxY;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-boolean v0, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2305096
    const v0, -0x2a6bf557

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2305097
    :goto_0
    return-void

    .line 2305098
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    if-eqz v0, :cond_1

    .line 2305099
    iget-object v2, p0, LX/FxY;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-virtual {v2, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    .line 2305100
    :cond_1
    const v0, 0x2581a046

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
