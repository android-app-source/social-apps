.class public LX/Gkr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2389661
    const-class v0, LX/Gkr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Gkr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2389660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/Gkq;)LX/Gkn;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2389646
    iget-boolean v0, p0, LX/Gkq;->j:Z

    move v0, v0

    .line 2389647
    if-nez v0, :cond_0

    .line 2389648
    const/4 v0, 0x0

    .line 2389649
    :goto_0
    return-object v0

    .line 2389650
    :cond_0
    iget-boolean v0, p0, LX/Gkq;->g:Z

    move v0, v0

    .line 2389651
    if-eqz v0, :cond_1

    .line 2389652
    sget-object v0, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    goto :goto_0

    .line 2389653
    :cond_1
    iget-boolean v0, p0, LX/Gkq;->i:Z

    move v0, v0

    .line 2389654
    if-eqz v0, :cond_2

    .line 2389655
    sget-object v0, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    goto :goto_0

    .line 2389656
    :cond_2
    iget-boolean v0, p0, LX/Gkq;->h:Z

    move v0, v0

    .line 2389657
    if-eqz v0, :cond_3

    .line 2389658
    sget-object v0, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    goto :goto_0

    .line 2389659
    :cond_3
    sget-object v0, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    goto :goto_0
.end method

.method public static a(Ljava/util/HashMap;Ljava/lang/String;ZZ)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)",
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2389620
    invoke-static {p0, p1}, LX/Gkr;->b(Ljava/util/HashMap;Ljava/lang/String;)LX/Gkq;

    move-result-object v1

    .line 2389621
    if-eqz v1, :cond_1

    .line 2389622
    invoke-static {v1}, LX/Gkr;->a(LX/Gkq;)LX/Gkn;

    move-result-object v0

    .line 2389623
    new-instance v2, LX/Gkq;

    invoke-direct {v2, v1}, LX/Gkq;-><init>(LX/Gkq;)V

    .line 2389624
    iput-boolean p2, v2, LX/Gkq;->g:Z

    .line 2389625
    iput-boolean p3, v2, LX/Gkq;->h:Z

    .line 2389626
    invoke-static {v2}, LX/Gkr;->a(LX/Gkq;)LX/Gkn;

    move-result-object v3

    .line 2389627
    if-eq v0, v3, :cond_2

    .line 2389628
    if-eqz v0, :cond_0

    .line 2389629
    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389630
    if-eqz v0, :cond_0

    instance-of v4, v0, LX/Gkp;

    if-eqz v4, :cond_0

    .line 2389631
    check-cast v0, LX/Gkp;

    invoke-virtual {v0, v1}, LX/Gkp;->a(LX/Gkq;)V

    .line 2389632
    :cond_0
    if-eqz v3, :cond_1

    .line 2389633
    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389634
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/Gkp;

    if-eqz v1, :cond_1

    .line 2389635
    check-cast v0, LX/Gkp;

    invoke-virtual {v0, v2}, LX/Gkp;->b(LX/Gkq;)V

    .line 2389636
    :cond_1
    :goto_0
    return-object p0

    .line 2389637
    :cond_2
    sget-object v0, LX/Gkr;->a:Ljava/lang/String;

    const-string v1, "Favorite/Hidden status changed, but group section did not change."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Ljava/util/HashMap;Ljava/lang/String;)LX/Gkq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/Gkq;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2389638
    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389639
    instance-of v2, v0, LX/Gkp;

    if-eqz v2, :cond_0

    .line 2389640
    check-cast v0, LX/Gkp;

    .line 2389641
    iget-object v2, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/Gkp;->g:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/Gkp;->g:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2389642
    iget-object p0, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    iget-object v2, v0, LX/Gkp;->g:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gkq;

    .line 2389643
    :goto_0
    move-object v0, v2

    .line 2389644
    if-eqz v0, :cond_0

    .line 2389645
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
