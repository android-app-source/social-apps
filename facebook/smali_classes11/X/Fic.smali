.class public LX/Fic;
.super LX/7Hg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7Hg",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:LX/Cwe;

.field private final d:LX/0ad;

.field private e:Z

.field private f:LX/7Hi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;LX/FhI;LX/0Or;LX/0Or;LX/7Ho;LX/7Hd;LX/0Uh;LX/0ad;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/FhI;",
            "LX/0Or",
            "<",
            "LX/FhP;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FhM;",
            ">;",
            "LX/7Ho;",
            "LX/7Hd;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2275543
    invoke-direct {p0, p1, p5, p6}, LX/7Hg;-><init>(LX/0Sh;LX/7Ho;LX/7Hd;)V

    .line 2275544
    iput-boolean v2, p0, LX/Fic;->e:Z

    .line 2275545
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fic;->f:LX/7Hi;

    .line 2275546
    iput-object p8, p0, LX/Fic;->d:LX/0ad;

    .line 2275547
    sget-object v0, LX/7HY;->LOCAL:LX/7HY;

    .line 2275548
    iget-object v1, p2, LX/7HT;->c:LX/7Hn;

    move-object v1, v1

    .line 2275549
    invoke-virtual {p5, v0, v1}, LX/7Ho;->a(LX/7HY;LX/7Hn;)V

    .line 2275550
    invoke-virtual {p0, p2}, LX/7Hg;->b(LX/7HS;)V

    .line 2275551
    sget v0, LX/2SU;->e:I

    invoke-virtual {p7, v0, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-short v0, LX/100;->bU:S

    invoke-interface {p8, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2275552
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhM;

    .line 2275553
    const/4 v1, 0x3

    .line 2275554
    iget-object p1, v0, LX/FhM;->b:LX/FhP;

    .line 2275555
    iput v2, p1, LX/FhH;->h:I

    .line 2275556
    iget-object p1, v0, LX/FhM;->c:LX/FhN;

    .line 2275557
    iput v2, p1, LX/FhH;->h:I

    .line 2275558
    iget-object p1, v0, LX/FhM;->d:LX/FhO;

    .line 2275559
    iput v1, p1, LX/FhH;->h:I

    .line 2275560
    invoke-virtual {v0}, LX/FhM;->a()Ljava/util/Map;

    move-result-object v1

    .line 2275561
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 2275562
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/7HY;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Hn;

    invoke-virtual {p5, p1, v2}, LX/7Ho;->a(LX/7HY;LX/7Hn;)V

    goto :goto_0

    .line 2275563
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/7Hg;->a(LX/7HS;I)V

    .line 2275564
    iput-object v0, p0, LX/Fic;->c:LX/Cwe;

    .line 2275565
    :goto_1
    return-void

    .line 2275566
    :cond_1
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhP;

    .line 2275567
    iput v2, v0, LX/FhH;->h:I

    .line 2275568
    sget-object v1, LX/7HY;->REMOTE:LX/7HY;

    .line 2275569
    iget-object v2, v0, LX/7HT;->c:LX/7Hn;

    move-object v2, v2

    .line 2275570
    invoke-virtual {p5, v1, v2}, LX/7Ho;->a(LX/7HY;LX/7Hn;)V

    .line 2275571
    invoke-virtual {p0, v0}, LX/7Hg;->a(LX/7HS;)V

    .line 2275572
    iput-object v0, p0, LX/Fic;->c:LX/Cwe;

    goto :goto_1
.end method

.method private a(LX/7Hi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2275538
    if-nez p1, :cond_0

    .line 2275539
    :goto_0
    return-void

    .line 2275540
    :cond_0
    invoke-virtual {p0}, LX/Fic;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2275541
    invoke-virtual {p0}, LX/7Hg;->d()V

    .line 2275542
    :cond_1
    sget-object v0, LX/7Hf;->LOCAL:LX/7Hf;

    invoke-super {p0, p1, v0}, LX/7Hg;->a(LX/7Hi;LX/7Hf;)V

    goto :goto_0
.end method

.method public static c(LX/0QB;)LX/Fic;
    .locals 9

    .prologue
    .line 2275519
    new-instance v0, LX/Fic;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/FhI;->b(LX/0QB;)LX/FhI;

    move-result-object v2

    check-cast v2, LX/FhI;

    const/16 v3, 0x34be

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x34bb

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/7Ho;->b(LX/0QB;)LX/7Ho;

    move-result-object v5

    check-cast v5, LX/7Ho;

    invoke-static {p0}, LX/7Hd;->b(LX/0QB;)LX/7Hd;

    move-result-object v6

    check-cast v6, LX/7Hd;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v0 .. v8}, LX/Fic;-><init>(LX/0Sh;LX/FhI;LX/0Or;LX/0Or;LX/7Ho;LX/7Hd;LX/0Uh;LX/0ad;)V

    .line 2275520
    return-object v0
.end method


# virtual methods
.method public final a(LX/7Hi;LX/7Hf;)V
    .locals 3
    .param p2    # LX/7Hf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "LX/7Hf;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2275525
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275526
    sget-object v2, LX/7Hf;->LOCAL:LX/7Hf;

    if-ne p2, v2, :cond_2

    .line 2275527
    iget-object v2, p0, LX/Fic;->f:LX/7Hi;

    if-nez v2, :cond_0

    :goto_0
    const-string v2, "the assumption is that local responses is only fetched once"

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2275528
    iget-boolean v0, p0, LX/Fic;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Fic;->d:LX/0ad;

    sget-short v2, LX/100;->bA:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2275529
    iput-object p1, p0, LX/Fic;->f:LX/7Hi;

    .line 2275530
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2275531
    goto :goto_0

    .line 2275532
    :cond_1
    invoke-direct {p0, p1}, LX/Fic;->a(LX/7Hi;)V

    goto :goto_1

    .line 2275533
    :cond_2
    sget-object v1, LX/7Hf;->REMOTE:LX/7Hf;

    if-ne p2, v1, :cond_3

    .line 2275534
    iput-boolean v0, p0, LX/Fic;->e:Z

    .line 2275535
    iget-object v0, p0, LX/Fic;->f:LX/7Hi;

    invoke-direct {p0, v0}, LX/Fic;->a(LX/7Hi;)V

    .line 2275536
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fic;->f:LX/7Hi;

    .line 2275537
    :cond_3
    invoke-super {p0, p1, p2}, LX/7Hg;->a(LX/7Hi;LX/7Hf;)V

    goto :goto_1
.end method

.method public final a(LX/7B6;)Z
    .locals 1

    .prologue
    .line 2275522
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fic;->f:LX/7Hi;

    .line 2275523
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fic;->e:Z

    .line 2275524
    invoke-super {p0, p1}, LX/7Hg;->a(LX/7B6;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 2275521
    iget-object v0, p0, LX/Fic;->d:LX/0ad;

    sget-short v1, LX/100;->bZ:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
