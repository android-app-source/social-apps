.class public LX/G4V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/G4V;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1Kf;

.field private final c:LX/17W;

.field private final d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/G4a;

.field private final g:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;LX/1Kf;LX/17W;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/G4a;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;",
            "LX/1Kf;",
            "LX/17W;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/G4a;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317779
    iput-object p1, p0, LX/G4V;->a:LX/0Or;

    .line 2317780
    iput-object p2, p0, LX/G4V;->b:LX/1Kf;

    .line 2317781
    iput-object p3, p0, LX/G4V;->c:LX/17W;

    .line 2317782
    iput-object p4, p0, LX/G4V;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2317783
    iput-object p5, p0, LX/G4V;->f:LX/G4a;

    .line 2317784
    iput-object p6, p0, LX/G4V;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2317785
    iput-object p7, p0, LX/G4V;->g:LX/0ad;

    .line 2317786
    return-void
.end method

.method public static a(LX/0QB;)LX/G4V;
    .locals 11

    .prologue
    .line 2317765
    sget-object v0, LX/G4V;->h:LX/G4V;

    if-nez v0, :cond_1

    .line 2317766
    const-class v1, LX/G4V;

    monitor-enter v1

    .line 2317767
    :try_start_0
    sget-object v0, LX/G4V;->h:LX/G4V;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2317768
    if-eqz v2, :cond_0

    .line 2317769
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2317770
    new-instance v3, LX/G4V;

    const/16 v4, 0x97

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/G4a;->a(LX/0QB;)LX/G4a;

    move-result-object v8

    check-cast v8, LX/G4a;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v3 .. v10}, LX/G4V;-><init>(LX/0Or;LX/1Kf;LX/17W;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/G4a;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V

    .line 2317771
    move-object v0, v3

    .line 2317772
    sput-object v0, LX/G4V;->h:LX/G4V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317773
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2317774
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2317775
    :cond_1
    sget-object v0, LX/G4V;->h:LX/G4V;

    return-object v0

    .line 2317776
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2317777
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2317756
    if-eqz p1, :cond_0

    .line 2317757
    const-string v0, "profile_name"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317758
    :cond_0
    if-eqz p4, :cond_1

    .line 2317759
    const-string v0, "first_name"

    invoke-virtual {p0, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317760
    :cond_1
    if-eqz p2, :cond_2

    .line 2317761
    const-string v0, "friendship_status"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317762
    :cond_2
    if-eqz p3, :cond_3

    .line 2317763
    const-string v0, "subscribe_status"

    invoke-virtual {p0, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317764
    :cond_3
    return-void
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/74S;)Z
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2317742
    sparse-switch p2, :sswitch_data_0

    .line 2317743
    :goto_0
    if-eqz v0, :cond_2

    .line 2317744
    iget-object v2, p0, LX/G4V;->c:LX/17W;

    invoke-virtual {v2, p1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v1

    .line 2317745
    :goto_1
    return v0

    .line 2317746
    :sswitch_0
    iget-object v2, p0, LX/G4V;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    if-eqz p5, :cond_0

    invoke-static {p5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/facebook/photos/data/model/PhotoSet;->c(J)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {v2, v4, v5, v0, p6}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(JLjava/lang/String;LX/74S;)Landroid/content/Intent;

    move-result-object v0

    .line 2317747
    if-eqz v0, :cond_1

    .line 2317748
    iget-object v2, p0, LX/G4V;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    :cond_1
    move v0, v1

    .line 2317749
    goto :goto_1

    .line 2317750
    :sswitch_1
    sget-object v0, LX/0ax;->B:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2317751
    :sswitch_2
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2317752
    :sswitch_3
    sget-object v0, LX/0ax;->C:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2317753
    :sswitch_4
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2317754
    :sswitch_5
    sget-object v2, LX/0ax;->bz:Ljava/lang/String;

    invoke-static {v2, p3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2317755
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_2
        0x285feb -> :sswitch_4
        0x403827a -> :sswitch_1
        0x41e065f -> :sswitch_3
        0x4984e12 -> :sswitch_0
        0x4c808d5 -> :sswitch_5
    .end sparse-switch
.end method

.method private a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p2    # Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/2rX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2317721
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2317722
    if-eqz p2, :cond_1

    .line 2317723
    sget-object v0, LX/G4U;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    move-object v0, v2

    .line 2317724
    :goto_0
    if-eqz v0, :cond_0

    .line 2317725
    iget-object v2, p0, LX/G4V;->c:LX/17W;

    invoke-virtual {v2, p1, v0, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    move v0, v1

    .line 2317726
    :goto_1
    return v0

    .line 2317727
    :pswitch_0
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2317728
    :pswitch_1
    sget-object v0, LX/0ax;->bO:Ljava/lang/String;

    sget-object v2, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/DHr;->TIMELINE_CONTEXT_ITEM:LX/DHr;

    invoke-virtual {v4}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, p3, v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2317729
    invoke-static {v3, p4, p7, p8, p9}, LX/G4V;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2317730
    :pswitch_2
    sget-object v0, LX/0ax;->bO:Ljava/lang/String;

    sget-object v2, LX/DHs;->RECENTLY_ADDED_FRIENDS:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/DHr;->TIMELINE_CONTEXT_ITEM:LX/DHr;

    invoke-virtual {v4}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, p3, v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2317731
    invoke-static {v3, p4, p7, p8, p9}, LX/G4V;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2317732
    :pswitch_3
    sget-object v0, LX/0ax;->bO:Ljava/lang/String;

    sget-object v2, LX/DHs;->MUTUAL_FRIENDS:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/DHr;->TIMELINE_CONTEXT_ITEM:LX/DHr;

    invoke-virtual {v4}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, p3, v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2317733
    invoke-static {v3, p4, p7, p8, p9}, LX/G4V;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2317734
    :pswitch_4
    iget-object v0, p0, LX/G4V;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p10}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2317735
    iget-object v0, p0, LX/G4V;->b:LX/1Kf;

    iget-object v3, p0, LX/G4V;->f:LX/G4a;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p4, p5, p6}, LX/G4a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    const/16 v4, 0x6dc

    check-cast p1, Landroid/app/Activity;

    invoke-interface {v0, v2, v3, v4, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    move v0, v1

    .line 2317736
    goto :goto_1

    .line 2317737
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move-object v0, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/G4Z;)V
    .locals 11

    .prologue
    .line 2317738
    iget-object v2, p2, LX/G4Z;->f:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    iget-object v3, p2, LX/G4Z;->g:Ljava/lang/String;

    iget-object v4, p2, LX/G4Z;->h:Ljava/lang/String;

    iget-object v5, p2, LX/G4Z;->i:Ljava/lang/String;

    iget-object v6, p2, LX/G4Z;->j:LX/2rX;

    iget-object v7, p2, LX/G4Z;->m:Ljava/lang/String;

    iget-object v8, p2, LX/G4Z;->n:Ljava/lang/String;

    iget-object v9, p2, LX/G4Z;->o:Ljava/lang/String;

    iget-object v10, p2, LX/G4Z;->k:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, LX/G4V;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v2, p2, LX/G4Z;->a:I

    iget-object v3, p2, LX/G4Z;->b:Ljava/lang/String;

    iget-object v4, p2, LX/G4Z;->c:Ljava/lang/String;

    iget-object v5, p2, LX/G4Z;->d:Ljava/lang/String;

    iget-object v6, p2, LX/G4Z;->e:LX/74S;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/G4V;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/74S;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2317739
    iget-object v0, p2, LX/G4Z;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2317740
    iget-object v0, p0, LX/G4V;->c:LX/17W;

    iget-object v1, p2, LX/G4Z;->l:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2317741
    :cond_0
    return-void
.end method
