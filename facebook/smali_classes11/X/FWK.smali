.class public LX/FWK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FWK;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252013
    return-void
.end method

.method public static a(LX/0QB;)LX/FWK;
    .locals 3

    .prologue
    .line 2252014
    sget-object v0, LX/FWK;->a:LX/FWK;

    if-nez v0, :cond_1

    .line 2252015
    const-class v1, LX/FWK;

    monitor-enter v1

    .line 2252016
    :try_start_0
    sget-object v0, LX/FWK;->a:LX/FWK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2252017
    if-eqz v2, :cond_0

    .line 2252018
    :try_start_1
    new-instance v0, LX/FWK;

    invoke-direct {v0}, LX/FWK;-><init>()V

    .line 2252019
    move-object v0, v0

    .line 2252020
    sput-object v0, LX/FWK;->a:LX/FWK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252021
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2252022
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2252023
    :cond_1
    sget-object v0, LX/FWK;->a:LX/FWK;

    return-object v0

    .line 2252024
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2252025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
