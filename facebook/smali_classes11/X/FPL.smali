.class public final LX/FPL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;)V
    .locals 0

    .prologue
    .line 2237028
    iput-object p1, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0x4a147e23    # 2432904.8f

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2237021
    iget-object v0, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 2237022
    :goto_0
    iget-object v0, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-ne v1, v2, :cond_1

    .line 2237023
    iget-object v0, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->d:LX/121;

    sget-object v1, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    iget-object v4, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080e44

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/FPK;

    invoke-direct {v5, p0, v2}, LX/FPK;-><init>(LX/FPL;I)V

    invoke-virtual {v0, v1, v4, v5}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2237024
    iget-object v0, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->d:LX/121;

    sget-object v1, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/121;->a(LX/0yY;LX/0gc;)V

    .line 2237025
    :goto_1
    const v0, 0x79060573

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 2237026
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 2237027
    :cond_1
    iget-object v0, p0, LX/FPL;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;I)V

    goto :goto_1
.end method
