.class public final LX/Gkb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Oa;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;)V
    .locals 0

    .prologue
    .line 2389162
    iput-object p1, p0, LX/Gkb;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/View;IJ)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2389163
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2389164
    check-cast v0, LX/GlX;

    .line 2389165
    invoke-virtual {v0, p3}, LX/GlX;->f(I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 2389166
    :goto_0
    return v0

    .line 2389167
    :cond_0
    invoke-virtual {v0, p3}, LX/GlX;->e(I)LX/Gkq;

    move-result-object v2

    .line 2389168
    if-eqz v2, :cond_2

    instance-of v3, v2, LX/Gkq;

    if-eqz v3, :cond_2

    move-object v1, v2

    .line 2389169
    check-cast v1, LX/Gkq;

    .line 2389170
    iget-object v3, p0, LX/Gkb;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-static {v0, p3}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->b(LX/1OM;I)LX/GkS;

    move-result-object v4

    invoke-static {v3, v1, p3, p1, v4}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;LX/Gkq;ILcom/facebook/widget/recyclerview/BetterRecyclerView;LX/GkS;)V

    .line 2389171
    iget-object v1, p0, LX/Gkb;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v1, v1, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    invoke-virtual {v0}, LX/GlX;->d()LX/Gkm;

    move-result-object v0

    .line 2389172
    iget-object v3, v1, LX/Glj;->c:LX/0Zb;

    const-string v4, "groups_grid_group_long_clicked"

    const/4 p0, 0x0

    invoke-interface {v3, v4, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2389173
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2389174
    const-string v4, "groups_grid"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    .line 2389175
    iget-object v4, v2, LX/Gkq;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2389176
    invoke-virtual {v3, v4}, LX/0oG;->d(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object v4, v1, LX/Glj;->d:LX/0kv;

    iget-object p0, v1, LX/Glj;->b:Landroid/content/Context;

    invoke-virtual {v4, p0}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "position"

    invoke-virtual {v3, v4, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v3

    const-string v4, "is_favorite"

    .line 2389177
    iget-boolean p0, v2, LX/Gkq;->g:Z

    move p0, p0

    .line 2389178
    invoke-virtual {v3, v4, p0}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    const-string v4, "badge_count"

    .line 2389179
    iget p0, v2, LX/Gkq;->f:I

    move p0, p0

    .line 2389180
    invoke-virtual {v3, v4, p0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v3

    const-string v4, "ordering"

    invoke-virtual {v0}, LX/Gkm;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2389181
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2389182
    :cond_2
    iget-object v0, p0, LX/Gkb;->a:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->D:Ljava/lang/String;

    const-string v3, "Tried to open group options without a model"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 2389183
    goto :goto_0
.end method
