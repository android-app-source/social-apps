.class public final enum LX/H17;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H17;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H17;

.field public static final enum GK:LX/H17;

.field public static final enum PARAM:LX/H17;

.field public static final enum QE:LX/H17;

.field public static final enum UNIVERSE:LX/H17;

.field public static final enum UNKNOWN:LX/H17;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2414815
    new-instance v0, LX/H17;

    const-string v1, "UNIVERSE"

    invoke-direct {v0, v1, v2}, LX/H17;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H17;->UNIVERSE:LX/H17;

    .line 2414816
    new-instance v0, LX/H17;

    const-string v1, "QE"

    invoke-direct {v0, v1, v3}, LX/H17;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H17;->QE:LX/H17;

    .line 2414817
    new-instance v0, LX/H17;

    const-string v1, "GK"

    invoke-direct {v0, v1, v4}, LX/H17;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H17;->GK:LX/H17;

    .line 2414818
    new-instance v0, LX/H17;

    const-string v1, "PARAM"

    invoke-direct {v0, v1, v5}, LX/H17;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H17;->PARAM:LX/H17;

    .line 2414819
    new-instance v0, LX/H17;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/H17;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H17;->UNKNOWN:LX/H17;

    .line 2414820
    const/4 v0, 0x5

    new-array v0, v0, [LX/H17;

    sget-object v1, LX/H17;->UNIVERSE:LX/H17;

    aput-object v1, v0, v2

    sget-object v1, LX/H17;->QE:LX/H17;

    aput-object v1, v0, v3

    sget-object v1, LX/H17;->GK:LX/H17;

    aput-object v1, v0, v4

    sget-object v1, LX/H17;->PARAM:LX/H17;

    aput-object v1, v0, v5

    sget-object v1, LX/H17;->UNKNOWN:LX/H17;

    aput-object v1, v0, v6

    sput-object v0, LX/H17;->$VALUES:[LX/H17;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2414822
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H17;
    .locals 1

    .prologue
    .line 2414823
    const-class v0, LX/H17;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H17;

    return-object v0
.end method

.method public static values()[LX/H17;
    .locals 1

    .prologue
    .line 2414821
    sget-object v0, LX/H17;->$VALUES:[LX/H17;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H17;

    return-object v0
.end method
