.class public LX/G2X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0SG;

.field public final c:Z

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/0SG;LX/0kb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314194
    iput-object p1, p0, LX/G2X;->a:LX/0ad;

    .line 2314195
    iput-object p2, p0, LX/G2X;->b:LX/0SG;

    .line 2314196
    invoke-virtual {p3}, LX/0kb;->g()Z

    move-result v0

    iput-boolean v0, p0, LX/G2X;->c:Z

    .line 2314197
    return-void
.end method

.method public static a(LX/0QB;)LX/G2X;
    .locals 6

    .prologue
    .line 2314162
    const-class v1, LX/G2X;

    monitor-enter v1

    .line 2314163
    :try_start_0
    sget-object v0, LX/G2X;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314164
    sput-object v2, LX/G2X;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314165
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314166
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314167
    new-instance p0, LX/G2X;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-direct {p0, v3, v4, v5}, LX/G2X;-><init>(LX/0ad;LX/0SG;LX/0kb;)V

    .line 2314168
    move-object v0, p0

    .line 2314169
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314170
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G2X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314171
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static f(LX/G2X;Lcom/facebook/timeline/protiles/model/ProtileModel;)Z
    .locals 2

    .prologue
    .line 2314187
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_0

    .line 2314188
    iget-boolean v0, p0, LX/G2X;->d:Z

    .line 2314189
    :goto_0
    return v0

    .line 2314190
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_1

    .line 2314191
    iget-boolean v0, p0, LX/G2X;->e:Z

    goto :goto_0

    .line 2314192
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2314173
    invoke-static {p0, p1}, LX/G2X;->f(LX/G2X;Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2314174
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    .line 2314175
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v2, v3, :cond_3

    .line 2314176
    :goto_1
    move v1, v1

    .line 2314177
    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 2314178
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v2, v3, :cond_5

    .line 2314179
    :goto_2
    move v1, v1

    .line 2314180
    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 2314181
    :cond_3
    iget-boolean v2, p0, LX/G2X;->c:Z

    if-eqz v2, :cond_4

    .line 2314182
    iget-object v2, p0, LX/G2X;->a:LX/0ad;

    sget-short v3, LX/0wf;->p:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_1

    .line 2314183
    :cond_4
    iget-object v2, p0, LX/G2X;->a:LX/0ad;

    sget-short v3, LX/0wf;->l:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_1

    .line 2314184
    :cond_5
    iget-boolean v2, p0, LX/G2X;->c:Z

    if-eqz v2, :cond_6

    .line 2314185
    iget-object v2, p0, LX/G2X;->a:LX/0ad;

    sget-short v3, LX/0wf;->o:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_2

    .line 2314186
    :cond_6
    iget-object v2, p0, LX/G2X;->a:LX/0ad;

    sget-short v3, LX/0wf;->k:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_2
.end method
