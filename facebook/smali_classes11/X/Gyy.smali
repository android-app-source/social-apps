.class public final LX/Gyy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GA2;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/login/DeviceAuthDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/login/DeviceAuthDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2410709
    iput-object p1, p0, LX/Gyy;->b:Lcom/facebook/login/DeviceAuthDialog;

    iput-object p2, p0, LX/Gyy;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/GAY;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 2410710
    iget-object v0, p0, LX/Gyy;->b:Lcom/facebook/login/DeviceAuthDialog;

    iget-object v0, v0, Lcom/facebook/login/DeviceAuthDialog;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2410711
    :goto_0
    return-void

    .line 2410712
    :cond_0
    iget-object v0, p1, LX/GAY;->d:LX/GAF;

    move-object v0, v0

    .line 2410713
    if-eqz v0, :cond_1

    .line 2410714
    iget-object v0, p0, LX/Gyy;->b:Lcom/facebook/login/DeviceAuthDialog;

    .line 2410715
    iget-object v1, p1, LX/GAY;->d:LX/GAF;

    move-object v1, v1

    .line 2410716
    iget-object v2, v1, LX/GAF;->o:LX/GAA;

    move-object v1, v2

    .line 2410717
    invoke-static {v0, v1}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;LX/GAA;)V

    goto :goto_0

    .line 2410718
    :cond_1
    :try_start_0
    iget-object v0, p1, LX/GAY;->b:Lorg/json/JSONObject;

    move-object v0, v0

    .line 2410719
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2410720
    const-string v1, "permissions"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 2410721
    const-string v2, "data"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 2410722
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2410723
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2410724
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 2410725
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 2410726
    const-string v8, "permission"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2410727
    if-eqz v8, :cond_2

    const-string p1, "installed"

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 2410728
    const-string p1, "status"

    invoke-virtual {v6, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2410729
    if-eqz v6, :cond_2

    .line 2410730
    const-string p1, "granted"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2410731
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2410732
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2410733
    :cond_3
    const-string p1, "declined"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2410734
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2410735
    :cond_4
    new-instance v1, LX/Gsb;

    invoke-direct {v1, v4, v5}, LX/Gsb;-><init>(Ljava/util/List;Ljava/util/List;)V

    move-object v5, v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2410736
    iget-object v0, p0, LX/Gyy;->b:Lcom/facebook/login/DeviceAuthDialog;

    iget-object v0, v0, Lcom/facebook/login/DeviceAuthDialog;->l:Lcom/facebook/login/DeviceAuthMethodHandler;

    iget-object v1, p0, LX/Gyy;->a:Ljava/lang/String;

    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v2

    .line 2410737
    iget-object v4, v5, LX/Gsb;->a:Ljava/util/List;

    move-object v4, v4

    .line 2410738
    iget-object v6, v5, LX/Gsb;->b:Ljava/util/List;

    move-object v5, v6

    .line 2410739
    sget-object v6, LX/GA9;->DEVICE_AUTH:LX/GA9;

    move-object v8, v7

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/login/DeviceAuthMethodHandler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;LX/GA9;Ljava/util/Date;Ljava/util/Date;)V

    .line 2410740
    iget-object v0, p0, LX/Gyy;->b:Lcom/facebook/login/DeviceAuthDialog;

    iget-object v0, v0, Lcom/facebook/login/DeviceAuthDialog;->q:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0

    .line 2410741
    :catch_0
    move-exception v0

    .line 2410742
    iget-object v1, p0, LX/Gyy;->b:Lcom/facebook/login/DeviceAuthDialog;

    new-instance v2, LX/GAA;

    invoke-direct {v2, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;LX/GAA;)V

    goto/16 :goto_0
.end method
