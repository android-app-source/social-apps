.class public final LX/GXA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GXK;

.field public final synthetic b:LX/GXD;


# direct methods
.method public constructor <init>(LX/GXD;LX/GXK;)V
    .locals 0

    .prologue
    .line 2363055
    iput-object p1, p0, LX/GXA;->b:LX/GXD;

    iput-object p2, p0, LX/GXA;->a:LX/GXK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x1a74275f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2363056
    iget-object v1, p0, LX/GXA;->b:LX/GXD;

    iget-object v1, v1, LX/GXD;->f:LX/GXH;

    iget-object v2, p0, LX/GXA;->a:LX/GXK;

    iget-object v3, p0, LX/GXA;->b:LX/GXD;

    iget-object v3, v3, LX/GXD;->z:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, LX/GXA;->b:LX/GXD;

    invoke-virtual {v4}, LX/GXD;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, LX/GXA;->b:LX/GXD;

    iget-object v5, v5, LX/GXD;->j:LX/7iP;

    .line 2363057
    iget-object v7, v2, LX/GXK;->p:LX/GXJ;

    move-object v7, v7

    .line 2363058
    if-nez v7, :cond_1

    .line 2363059
    :cond_0
    :goto_0
    const v1, 0x60470017

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2363060
    :cond_1
    iget-object v7, v2, LX/GXK;->p:LX/GXJ;

    move-object v7, v7

    .line 2363061
    sget-object v8, LX/GXJ;->OFFSITE:LX/GXJ;

    if-ne v7, v8, :cond_2

    .line 2363062
    iget-object v7, v1, LX/GXH;->h:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/7ic;

    new-instance v8, LX/7ib;

    invoke-direct {v8}, LX/7ib;-><init>()V

    invoke-virtual {v7, v8}, LX/0b4;->a(LX/0b7;)V

    .line 2363063
    invoke-static {v2}, LX/GXH;->a(LX/GXK;)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    move-result-object v8

    .line 2363064
    if-nez v8, :cond_6

    .line 2363065
    :goto_1
    goto :goto_0

    .line 2363066
    :cond_2
    iget-object v7, v2, LX/GXK;->p:LX/GXJ;

    move-object v7, v7

    .line 2363067
    sget-object v8, LX/GXJ;->CONTACT_MERCHANT:LX/GXJ;

    if-ne v7, v8, :cond_3

    .line 2363068
    iget-object v7, v1, LX/GXH;->h:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/7ic;

    new-instance v8, LX/7ia;

    invoke-direct {v8}, LX/7ia;-><init>()V

    invoke-virtual {v7, v8}, LX/0b4;->a(LX/0b7;)V

    .line 2363069
    iget-object v7, v2, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v7, v7

    .line 2363070
    iget-object v8, v2, LX/GXK;->o:LX/0Px;

    move-object v8, v8

    .line 2363071
    const/4 p1, 0x0

    const/4 v10, 0x0

    .line 2363072
    if-nez v7, :cond_8

    .line 2363073
    :goto_2
    goto :goto_0

    .line 2363074
    :cond_3
    iget-object v7, v2, LX/GXK;->p:LX/GXJ;

    move-object v7, v7

    .line 2363075
    sget-object v8, LX/GXJ;->ONSITE:LX/GXJ;

    if-ne v7, v8, :cond_0

    .line 2363076
    invoke-static {v2}, LX/GXH;->a(LX/GXK;)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    move-result-object v8

    .line 2363077
    if-eqz v8, :cond_0

    .line 2363078
    iget-object v7, v1, LX/GXH;->i:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const/16 v9, 0x313

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, LX/0Uh;->a(IZ)Z

    move-result v7

    if-nez v7, :cond_4

    .line 2363079
    invoke-virtual {v8}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7, v4}, LX/GXH;->a(LX/GXH;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 2363080
    :cond_4
    iget-object v7, v1, LX/GXH;->b:LX/GW4;

    invoke-virtual {v8}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->o()Ljava/lang/String;

    move-result-object v8

    .line 2363081
    new-instance v9, Lcom/facebook/commerce/productdetails/api/CheckoutParams;

    const/4 v10, 0x0

    invoke-direct {v9, v8, v3, v10, v5}, Lcom/facebook/commerce/productdetails/api/CheckoutParams;-><init>(Ljava/lang/String;ILjava/lang/Long;LX/7iP;)V

    .line 2363082
    const-string v10, "checkoutParams"

    const-string v11, "submit_item_for_checkout"

    .line 2363083
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2363084
    if-eqz v9, :cond_5

    .line 2363085
    invoke-virtual {v3, v10, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2363086
    :cond_5
    iget-object v8, v7, LX/GW4;->a:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0aG;

    const v5, 0x49c5cbb8    # 1620343.0f

    invoke-static {v8, v11, v3, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v8

    .line 2363087
    invoke-interface {v8}, LX/1MF;->startTryNotToUseMainThread()LX/1ML;

    move-result-object v8

    move-object v9, v8

    .line 2363088
    move-object v7, v9

    .line 2363089
    new-instance v8, LX/GXE;

    invoke-direct {v8, v1, v4}, LX/GXE;-><init>(LX/GXH;Landroid/content/Context;)V

    iget-object v9, v1, LX/GXH;->c:LX/0TD;

    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    .line 2363090
    :cond_6
    iget-object v7, v1, LX/GXH;->i:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const/16 v9, 0x312

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, LX/0Uh;->a(IZ)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v8}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v7

    .line 2363091
    :goto_3
    invoke-static {v1, v7, v4}, LX/GXH;->a(LX/GXH;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2363092
    :cond_7
    invoke-virtual {v8}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 2363093
    :cond_8
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->s()Ljava/lang/String;

    move-result-object v9

    .line 2363094
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 2363095
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v11

    if-eqz v11, :cond_d

    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v11

    if-eqz v11, :cond_d

    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_d

    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    if-eqz v11, :cond_d

    .line 2363096
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->n()LX/1vs;

    move-result-object v11

    iget v11, v11, LX/1vs;->b:I

    .line 2363097
    if-eqz v11, :cond_c

    move v11, v12

    :goto_4
    if-eqz v11, :cond_f

    .line 2363098
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->n()LX/1vs;

    move-result-object v11

    iget-object p0, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    .line 2363099
    invoke-virtual {p0, v11, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_e

    :goto_5
    if-eqz v12, :cond_10

    .line 2363100
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v11}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->n()LX/1vs;

    move-result-object v11

    iget-object v12, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    invoke-virtual {v12, v11, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v11

    .line 2363101
    :goto_6
    move-object p0, v11

    .line 2363102
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0837c0

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    aput-object v9, v13, p1

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2363103
    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_b

    .line 2363104
    invoke-virtual {v8, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v9}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v9

    invoke-static {v9}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object p1

    .line 2363105
    :goto_7
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 2363106
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2363107
    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->m()Ljava/lang/String;

    move-result-object v12

    .line 2363108
    :goto_8
    iget-object v9, v1, LX/GXH;->f:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/14x;

    invoke-virtual {v9}, LX/14x;->a()Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, v1, LX/GXH;->f:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/14x;

    invoke-virtual {v9}, LX/14x;->d()Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, v1, LX/GXH;->f:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/14x;

    const-string v10, "35.0"

    invoke-virtual {v9, v10}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 2363109
    iget-object v9, v1, LX/GXH;->e:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/B9y;

    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v11

    const-string v5, "emerging_market_commerce"

    move-object v10, v4

    move-object v13, v12

    const/4 v8, 0x1

    .line 2363110
    new-instance v7, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-direct {v7, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2363111
    if-eqz v8, :cond_11

    sget-object v1, LX/3RH;->s:Ljava/lang/String;

    :goto_9
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2363112
    const-string v1, "share_return_to_fb4a"

    invoke-virtual {v7, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2363113
    move-object v1, v7

    .line 2363114
    const-string v7, "share_fbid"

    invoke-virtual {v1, v7, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2363115
    const-string v7, "title"

    invoke-virtual {v1, v7, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2363116
    const-string v7, "share_title"

    invoke-virtual {v1, v7, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2363117
    const-string v7, "share_media_url"

    invoke-virtual {v1, v7, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2363118
    const-string v7, "share_caption"

    invoke-virtual {v1, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2363119
    new-array v7, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    .line 2363120
    const-string v8, "preselected_recipients"

    invoke-virtual {v1, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2363121
    const-string v8, "suggested_recipients"

    invoke-virtual {v1, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2363122
    const-string v7, "share_body_text_prefill"

    invoke-virtual {v1, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2363123
    invoke-static {v1, v5}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2363124
    iget-object v7, v9, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v7, v1, v10}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2363125
    goto/16 :goto_2

    .line 2363126
    :cond_9
    sget-object v9, LX/0ax;->fV:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 2363127
    iget-object v9, v1, LX/GXH;->g:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-virtual {v9, v4, v10}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_2

    :cond_a
    move-object v12, v10

    move-object v2, v10

    goto/16 :goto_8

    :cond_b
    move-object p1, v10

    goto/16 :goto_7

    :cond_c
    move v11, v13

    .line 2363128
    goto/16 :goto_4

    :cond_d
    move v11, v13

    goto/16 :goto_4

    :cond_e
    move v12, v13

    goto/16 :goto_5

    :cond_f
    move v12, v13

    goto/16 :goto_5

    .line 2363129
    :cond_10
    const/4 v11, 0x0

    goto/16 :goto_6

    .line 2363130
    :cond_11
    sget-object v1, LX/3RH;->r:Ljava/lang/String;

    goto :goto_9
.end method
