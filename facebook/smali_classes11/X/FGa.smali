.class public final enum LX/FGa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FGa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FGa;

.field public static final enum ENCRYPTING:LX/FGa;

.field public static final enum STARTED:LX/FGa;

.field public static final enum TRANSCODING:LX/FGa;

.field public static final enum UNKNOWN:LX/FGa;

.field public static final enum UPLOADING:LX/FGa;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2219176
    new-instance v0, LX/FGa;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2}, LX/FGa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGa;->STARTED:LX/FGa;

    .line 2219177
    new-instance v0, LX/FGa;

    const-string v1, "TRANSCODING"

    invoke-direct {v0, v1, v3}, LX/FGa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGa;->TRANSCODING:LX/FGa;

    .line 2219178
    new-instance v0, LX/FGa;

    const-string v1, "ENCRYPTING"

    invoke-direct {v0, v1, v4}, LX/FGa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGa;->ENCRYPTING:LX/FGa;

    .line 2219179
    new-instance v0, LX/FGa;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v5}, LX/FGa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGa;->UPLOADING:LX/FGa;

    .line 2219180
    new-instance v0, LX/FGa;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/FGa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGa;->UNKNOWN:LX/FGa;

    .line 2219181
    const/4 v0, 0x5

    new-array v0, v0, [LX/FGa;

    sget-object v1, LX/FGa;->STARTED:LX/FGa;

    aput-object v1, v0, v2

    sget-object v1, LX/FGa;->TRANSCODING:LX/FGa;

    aput-object v1, v0, v3

    sget-object v1, LX/FGa;->ENCRYPTING:LX/FGa;

    aput-object v1, v0, v4

    sget-object v1, LX/FGa;->UPLOADING:LX/FGa;

    aput-object v1, v0, v5

    sget-object v1, LX/FGa;->UNKNOWN:LX/FGa;

    aput-object v1, v0, v6

    sput-object v0, LX/FGa;->$VALUES:[LX/FGa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2219182
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FGa;
    .locals 1

    .prologue
    .line 2219183
    const-class v0, LX/FGa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FGa;

    return-object v0
.end method

.method public static values()[LX/FGa;
    .locals 1

    .prologue
    .line 2219184
    sget-object v0, LX/FGa;->$VALUES:[LX/FGa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FGa;

    return-object v0
.end method
