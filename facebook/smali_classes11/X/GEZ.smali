.class public LX/GEZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        "D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "Ljava/lang/Object;",
        "LX/GEW",
        "<TT;TD;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:LX/GHg;

.field private c:LX/GGX;

.field private d:LX/8wK;


# direct methods
.method public constructor <init>(ILX/GHg;LX/GGX;LX/8wK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<TT;TD;>;",
            "LX/GGX;",
            "LX/8wK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2331879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331880
    iput p1, p0, LX/GEZ;->a:I

    .line 2331881
    iput-object p2, p0, LX/GEZ;->b:LX/GHg;

    .line 2331882
    iput-object p3, p0, LX/GEZ;->c:LX/GGX;

    .line 2331883
    iput-object p4, p0, LX/GEZ;->d:LX/8wK;

    .line 2331884
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2331885
    iget v0, p0, LX/GEZ;->a:I

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)Z"
        }
    .end annotation

    .prologue
    .line 2331886
    iget-object v0, p0, LX/GEZ;->c:LX/GGX;

    invoke-interface {v0, p1}, LX/GGX;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    return v0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<TT;TD;>;"
        }
    .end annotation

    .prologue
    .line 2331887
    iget-object v0, p0, LX/GEZ;->b:LX/GHg;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2331888
    iget-object v0, p0, LX/GEZ;->d:LX/8wK;

    return-object v0
.end method
