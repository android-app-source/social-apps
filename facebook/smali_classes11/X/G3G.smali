.class public LX/G3G;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/G3G;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2315145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2315146
    iput-object p1, p0, LX/G3G;->a:LX/0Zb;

    .line 2315147
    return-void
.end method

.method public static a(LX/0QB;)LX/G3G;
    .locals 4

    .prologue
    .line 2315132
    sget-object v0, LX/G3G;->b:LX/G3G;

    if-nez v0, :cond_1

    .line 2315133
    const-class v1, LX/G3G;

    monitor-enter v1

    .line 2315134
    :try_start_0
    sget-object v0, LX/G3G;->b:LX/G3G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2315135
    if-eqz v2, :cond_0

    .line 2315136
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2315137
    new-instance p0, LX/G3G;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/G3G;-><init>(LX/0Zb;)V

    .line 2315138
    move-object v0, p0

    .line 2315139
    sput-object v0, LX/G3G;->b:LX/G3G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2315140
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2315141
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2315142
    :cond_1
    sget-object v0, LX/G3G;->b:LX/G3G;

    return-object v0

    .line 2315143
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2315144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2315118
    iget-object v0, p0, LX/G3G;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2315119
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315120
    const-string v3, "profile_wizard_flow_type"

    if-nez p2, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 2315121
    const-string v0, "profile_wizard_step_type"

    if-nez p3, :cond_3

    :goto_1
    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2315122
    const-string v0, "event_type"

    invoke-virtual {v2, v0, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2315123
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2315124
    :cond_0
    return-void

    .line 2315125
    :cond_1
    new-instance p0, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {p0, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2315126
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p1

    const/4 v0, 0x0

    move v4, v0

    :goto_2
    if-ge v4, p1, :cond_2

    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 2315127
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2315128
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 2315129
    :cond_2
    move-object v0, p0

    .line 2315130
    goto :goto_0

    .line 2315131
    :cond_3
    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public final f(Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2315116
    const-string v0, "profile_wizard_edit_bio_tapped"

    invoke-static {p0, p1, p2, p3, v0}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2315117
    return-void
.end method

.method public final g(Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2315114
    const-string v0, "profile_wizard_step_save"

    invoke-static {p0, p1, p2, p3, v0}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2315115
    return-void
.end method
