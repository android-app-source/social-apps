.class public final LX/H5W;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/H5X;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Landroid/view/View$OnClickListener;

.field public final synthetic e:LX/H5X;


# direct methods
.method public constructor <init>(LX/H5X;)V
    .locals 1

    .prologue
    .line 2424457
    iput-object p1, p0, LX/H5W;->e:LX/H5X;

    .line 2424458
    move-object v0, p1

    .line 2424459
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2424460
    const/4 v0, 0x0

    iput v0, p0, LX/H5W;->b:I

    .line 2424461
    sget-object v0, LX/H5Y;->a:Ljava/lang/String;

    iput-object v0, p0, LX/H5W;->c:Ljava/lang/String;

    .line 2424462
    sget-object v0, LX/H5Y;->b:Landroid/view/View$OnClickListener;

    iput-object v0, p0, LX/H5W;->d:Landroid/view/View$OnClickListener;

    .line 2424463
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2424483
    const-string v0, "NotificationsFriendingBucketHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2424464
    if-ne p0, p1, :cond_1

    .line 2424465
    :cond_0
    :goto_0
    return v0

    .line 2424466
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2424467
    goto :goto_0

    .line 2424468
    :cond_3
    check-cast p1, LX/H5W;

    .line 2424469
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2424470
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2424471
    if-eq v2, v3, :cond_0

    .line 2424472
    iget-object v2, p0, LX/H5W;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/H5W;->a:Ljava/lang/String;

    iget-object v3, p1, LX/H5W;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2424473
    goto :goto_0

    .line 2424474
    :cond_5
    iget-object v2, p1, LX/H5W;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2424475
    :cond_6
    iget v2, p0, LX/H5W;->b:I

    iget v3, p1, LX/H5W;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2424476
    goto :goto_0

    .line 2424477
    :cond_7
    iget-object v2, p0, LX/H5W;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/H5W;->c:Ljava/lang/String;

    iget-object v3, p1, LX/H5W;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2424478
    goto :goto_0

    .line 2424479
    :cond_9
    iget-object v2, p1, LX/H5W;->c:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 2424480
    :cond_a
    iget-object v2, p0, LX/H5W;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/H5W;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/H5W;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2424481
    goto :goto_0

    .line 2424482
    :cond_b
    iget-object v2, p1, LX/H5W;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
