.class public final LX/GWu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GWv;

.field private b:Landroid/widget/TextView;

.field public c:Landroid/view/View;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:F

.field public h:LX/7iY;

.field public i:LX/7iW;


# direct methods
.method public constructor <init>(LX/GWv;Landroid/widget/TextView;Landroid/view/View;ILandroid/content/Context;)V
    .locals 5

    .prologue
    const/16 v4, 0xc8

    const/4 v3, 0x0

    .line 2362819
    iput-object p1, p0, LX/GWu;->a:LX/GWv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2362820
    iput-object p2, p0, LX/GWu;->b:Landroid/widget/TextView;

    .line 2362821
    iput-object p3, p0, LX/GWu;->c:Landroid/view/View;

    .line 2362822
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/GWu;->d:Landroid/graphics/drawable/Drawable;

    .line 2362823
    invoke-virtual {p2}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    iput v0, p0, LX/GWu;->g:F

    .line 2362824
    iget-object v0, p0, LX/GWu;->d:Landroid/graphics/drawable/Drawable;

    iget v1, p0, LX/GWu;->g:F

    float-to-int v1, v1

    iget v2, p0, LX/GWu;->g:F

    float-to-int v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2362825
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0207eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/GWu;->e:Landroid/graphics/drawable/Drawable;

    .line 2362826
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0207e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/GWu;->f:Landroid/graphics/drawable/Drawable;

    .line 2362827
    iget-object v0, p0, LX/GWu;->e:Landroid/graphics/drawable/Drawable;

    iget v1, p0, LX/GWu;->g:F

    float-to-int v1, v1

    iget v2, p0, LX/GWu;->g:F

    float-to-int v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2362828
    iget-object v0, p0, LX/GWu;->f:Landroid/graphics/drawable/Drawable;

    iget v1, p0, LX/GWu;->g:F

    float-to-int v1, v1

    iget v2, p0, LX/GWu;->g:F

    float-to-int v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2362829
    new-instance v0, LX/7iY;

    iget-object v1, p0, LX/GWu;->c:Landroid/view/View;

    invoke-direct {v0, v4, v1}, LX/7iY;-><init>(ILandroid/view/View;)V

    iput-object v0, p0, LX/GWu;->h:LX/7iY;

    .line 2362830
    new-instance v0, LX/7iW;

    iget-object v1, p0, LX/GWu;->c:Landroid/view/View;

    invoke-direct {v0, v4, v1}, LX/7iW;-><init>(ILandroid/view/View;)V

    iput-object v0, p0, LX/GWu;->i:LX/7iW;

    .line 2362831
    return-void
.end method

.method public static a(LX/GWu;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2362832
    if-eqz p1, :cond_0

    .line 2362833
    iget-object v0, p0, LX/GWu;->b:Landroid/widget/TextView;

    iget-object v1, p0, LX/GWu;->d:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/GWu;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2362834
    :goto_0
    return-void

    .line 2362835
    :cond_0
    iget-object v0, p0, LX/GWu;->b:Landroid/widget/TextView;

    iget-object v1, p0, LX/GWu;->d:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/GWu;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2362836
    iget-object v0, p0, LX/GWu;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2362837
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/GWu;->a(LX/GWu;Z)V

    .line 2362838
    :goto_0
    return-void

    .line 2362839
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/GWu;->a(LX/GWu;Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6f1a7d16

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2362840
    iget-object v1, p0, LX/GWu;->h:LX/7iY;

    invoke-virtual {v1}, LX/7iY;->hasStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GWu;->h:LX/7iY;

    invoke-virtual {v1}, LX/7iY;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, LX/GWu;->i:LX/7iW;

    invoke-virtual {v1}, LX/7iW;->hasStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/GWu;->i:LX/7iW;

    invoke-virtual {v1}, LX/7iW;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2362841
    :cond_1
    const v1, 0x51aa2bd3

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2362842
    :goto_0
    return-void

    .line 2362843
    :cond_2
    iget-object v1, p0, LX/GWu;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 2362844
    iget-object v1, p0, LX/GWu;->h:LX/7iY;

    iget-object v2, p0, LX/GWu;->c:Landroid/view/View;

    .line 2362845
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    int-to-float p1, p1

    .line 2362846
    iput p1, v1, LX/7iY;->a:F

    .line 2362847
    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2362848
    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/GWu;->a(LX/GWu;Z)V

    .line 2362849
    :goto_1
    const v1, 0x18d5705a

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2362850
    :cond_3
    iget-object v1, p0, LX/GWu;->i:LX/7iW;

    iget-object v2, p0, LX/GWu;->c:Landroid/view/View;

    const/4 p1, 0x0

    .line 2362851
    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 2362852
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    .line 2362853
    iput v3, v1, LX/7iW;->a:F

    .line 2362854
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput p1, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2362855
    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2362856
    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2362857
    const/4 v1, 0x1

    invoke-static {p0, v1}, LX/GWu;->a(LX/GWu;Z)V

    .line 2362858
    goto :goto_1
.end method
