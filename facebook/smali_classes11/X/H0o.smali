.class public final LX/H0o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/H0j;


# direct methods
.method public constructor <init>(LX/H0j;)V
    .locals 0

    .prologue
    .line 2414248
    iput-object p1, p0, LX/H0o;->a:LX/H0j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2414249
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2414250
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2414251
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2414252
    iget-object v1, p0, LX/H0o;->a:LX/H0j;

    iget-object v1, v1, LX/H0j;->p:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2414253
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    .line 2414254
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2414255
    iget-object v1, p0, LX/H0o;->a:LX/H0j;

    invoke-virtual {v1, v0}, LX/H0j;->d(I)V

    .line 2414256
    return-void
.end method
