.class public final LX/GNT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adpreview/activity/AdPreviewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adpreview/activity/AdPreviewActivity;)V
    .locals 0

    .prologue
    .line 2346050
    iput-object p1, p0, LX/GNT;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2346046
    new-instance v0, Lcom/facebook/api/story/FetchSingleStoryParams;

    const-string v1, ""

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;)V

    .line 2346047
    iget-object v1, p0, LX/GNT;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v1, v1, Lcom/facebook/adpreview/activity/AdPreviewActivity;->r:LX/GNY;

    iget-object v2, p0, LX/GNT;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v2, v2, Lcom/facebook/adpreview/activity/AdPreviewActivity;->y:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/GNY;->a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    .line 2346048
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2346049
    iget-object v1, p0, LX/GNT;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v1, v1, Lcom/facebook/adpreview/activity/AdPreviewActivity;->s:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2346051
    invoke-direct {p0}, LX/GNT;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
