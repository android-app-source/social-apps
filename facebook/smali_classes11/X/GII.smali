.class public final LX/GII;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GDQ;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;)V
    .locals 0

    .prologue
    .line 2336317
    iput-object p1, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(DD)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2336318
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    new-instance v1, LX/A9E;

    invoke-direct {v1}, LX/A9E;-><init>()V

    .line 2336319
    iput-wide p1, v1, LX/A9E;->f:D

    .line 2336320
    move-object v1, v1

    .line 2336321
    iput-wide p3, v1, LX/A9E;->h:D

    .line 2336322
    move-object v1, v1

    .line 2336323
    iget-object v2, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v2, v2, LX/GIL;->e:Ljava/lang/String;

    .line 2336324
    iput-object v2, v1, LX/A9E;->a:Ljava/lang/String;

    .line 2336325
    move-object v1, v1

    .line 2336326
    invoke-virtual {v1}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v1

    .line 2336327
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2336328
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-boolean v0, v0, LX/GIL;->l:Z

    if-eqz v0, :cond_0

    .line 2336329
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->n:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    sget-object v1, LX/GCi;->GET_DIRECTIONS_WRAPPER:LX/GCi;

    iget-object v2, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v2, v2, LX/GIL;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1, v2}, LX/GCi;->getUri(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Ljava/lang/String;

    move-result-object v1

    .line 2336330
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2336331
    :cond_0
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2336332
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    .line 2336333
    iput-boolean v3, v0, LX/GIL;->m:Z

    .line 2336334
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setMapEnabled(Z)V

    .line 2336335
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    iget-object v1, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v1, v1, LX/GIL;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a()Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2336336
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2336337
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->i:LX/GCE;

    sget-object v1, LX/GG8;->ADDRESS:LX/GG8;

    invoke-virtual {v0, v1, v3}, LX/GCE;->a(LX/GG8;Z)V

    .line 2336338
    iget-object v0, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    .line 2336339
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2336340
    new-instance v1, LX/GFG;

    invoke-direct {v1}, LX/GFG;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2336341
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2336342
    iget-object v1, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v1, v1, LX/GIL;->b:LX/2U3;

    const-class v2, LX/GDS;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "GraphQL error"

    invoke-virtual {v1, v2, v3, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2336343
    iget-object v1, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    .line 2336344
    iput-boolean v0, v1, LX/GIL;->m:Z

    .line 2336345
    iget-object v1, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v1, v1, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2336346
    iget-object v1, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v1, v1, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v2, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    invoke-virtual {v2}, LX/GIL;->b()Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2336347
    iget-object v1, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-object v1, v1, LX/GIL;->i:LX/GCE;

    sget-object v2, LX/GG8;->ADDRESS:LX/GG8;

    iget-object v3, p0, LX/GII;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    iget-object v3, v3, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;->a:LX/GIL;

    iget-boolean v3, v3, LX/GIL;->l:Z

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v2, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2336348
    return-void
.end method
