.class public final LX/Guk;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403807
    iput-object p1, p0, LX/Guk;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403808
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403809
    return-void
.end method

.method public static a(LX/Guk;Lcom/facebook/webview/FacebookWebView;LX/BWN;Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/webview/FacebookWebView;",
            "LX/BWN;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2403810
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 2403811
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403812
    invoke-interface {p2, v1, p3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403813
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2403814
    :cond_0
    :goto_0
    return-object v0

    .line 2403815
    :cond_1
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 2403816
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2403817
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v4

    .line 2403818
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2403819
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2403820
    :catch_0
    move-exception v1

    .line 2403821
    iget-object v2, p0, LX/Guk;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v2}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Invalid JSON format"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 4

    .prologue
    .line 2403822
    const-string v0, "preselected_ids"

    invoke-static {p0, p1, p2, v0}, LX/Guk;->a(LX/Guk;Lcom/facebook/webview/FacebookWebView;LX/BWN;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    move-object v0, v0

    .line 2403823
    const-string v1, "exclude_ids"

    invoke-static {p0, p1, p2, v1}, LX/Guk;->a(LX/Guk;Lcom/facebook/webview/FacebookWebView;LX/BWN;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    move-object v1, v1

    .line 2403824
    iget-object v2, p0, LX/Guk;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->a(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    .line 2403825
    iget-object v1, p0, LX/Guk;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403826
    iget-object v2, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v2, v2

    .line 2403827
    const-string v3, "callback"

    invoke-interface {p2, v2, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2403828
    iput-object v2, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    .line 2403829
    iget-object v1, p0, LX/Guk;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/16 v2, 0x28

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2403830
    return-void
.end method
