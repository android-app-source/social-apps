.class public LX/FsI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/FsI;


# instance fields
.field public final a:LX/0sa;

.field public final b:LX/0rq;

.field public final c:LX/0se;

.field public final d:LX/0ad;

.field public final e:LX/0tI;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0sU;

.field private final h:LX/A5q;

.field private final i:LX/0wo;

.field public final j:LX/0sX;

.field private final k:LX/0tO;

.field public final l:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0se;LX/0ad;LX/0tI;LX/0sU;LX/0Or;LX/A5q;LX/0wo;LX/0sX;LX/0tO;LX/0tQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sa;",
            "LX/0rq;",
            "LX/0se;",
            "LX/0ad;",
            "LX/0tI;",
            "LX/0sU;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/A5q;",
            "LX/0wo;",
            "LX/0sX;",
            "LX/0tO;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296714
    iput-object p1, p0, LX/FsI;->a:LX/0sa;

    .line 2296715
    iput-object p2, p0, LX/FsI;->b:LX/0rq;

    .line 2296716
    iput-object p3, p0, LX/FsI;->c:LX/0se;

    .line 2296717
    iput-object p5, p0, LX/FsI;->e:LX/0tI;

    .line 2296718
    iput-object p7, p0, LX/FsI;->f:LX/0Or;

    .line 2296719
    iput-object p4, p0, LX/FsI;->d:LX/0ad;

    .line 2296720
    iput-object p6, p0, LX/FsI;->g:LX/0sU;

    .line 2296721
    iput-object p8, p0, LX/FsI;->h:LX/A5q;

    .line 2296722
    iput-object p9, p0, LX/FsI;->i:LX/0wo;

    .line 2296723
    iput-object p10, p0, LX/FsI;->j:LX/0sX;

    .line 2296724
    iput-object p11, p0, LX/FsI;->k:LX/0tO;

    .line 2296725
    iput-object p12, p0, LX/FsI;->l:LX/0tQ;

    .line 2296726
    return-void
.end method

.method public static a(LX/0QB;)LX/FsI;
    .locals 3

    .prologue
    .line 2296727
    sget-object v0, LX/FsI;->m:LX/FsI;

    if-nez v0, :cond_1

    .line 2296728
    const-class v1, LX/FsI;

    monitor-enter v1

    .line 2296729
    :try_start_0
    sget-object v0, LX/FsI;->m:LX/FsI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2296730
    if-eqz v2, :cond_0

    .line 2296731
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FsI;->b(LX/0QB;)LX/FsI;

    move-result-object v0

    sput-object v0, LX/FsI;->m:LX/FsI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2296732
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2296733
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2296734
    :cond_1
    sget-object v0, LX/FsI;->m:LX/FsI;

    return-object v0

    .line 2296735
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2296736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/FsI;
    .locals 13

    .prologue
    .line 2296737
    new-instance v0, LX/FsI;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v1

    check-cast v1, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v5

    check-cast v5, LX/0tI;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v6

    check-cast v6, LX/0sU;

    const/16 v7, 0x12e4

    invoke-static {p0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v8

    check-cast v8, LX/A5q;

    invoke-static {p0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v9

    check-cast v9, LX/0wo;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v10

    check-cast v10, LX/0sX;

    invoke-static {p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v11

    check-cast v11, LX/0tO;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v12

    check-cast v12, LX/0tQ;

    invoke-direct/range {v0 .. v12}, LX/FsI;-><init>(LX/0sa;LX/0rq;LX/0se;LX/0ad;LX/0tI;LX/0sU;LX/0Or;LX/A5q;LX/0wo;LX/0sX;LX/0tO;LX/0tQ;)V

    .line 2296738
    return-object v0
.end method


# virtual methods
.method public final a(LX/G12;)LX/5xH;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 2296739
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 2296740
    new-instance v1, LX/5xH;

    invoke-direct {v1}, LX/5xH;-><init>()V

    move-object v1, v1

    .line 2296741
    const-string v2, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "nodeId"

    .line 2296742
    iget-wide v7, p1, LX/G12;->a:J

    move-wide v4, v7

    .line 2296743
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "angora_attachment_cover_image_size"

    iget-object v4, p0, LX/FsI;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "reading_attachment_profile_image_width"

    iget-object v4, p0, LX/FsI;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "reading_attachment_profile_image_height"

    iget-object v4, p0, LX/FsI;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "num_faceboxes_and_tags"

    iget-object v4, p0, LX/FsI;->a:LX/0sa;

    .line 2296744
    iget-object v5, v4, LX/0sa;->b:Ljava/lang/Integer;

    move-object v4, v5

    .line 2296745
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "image_large_aspect_height"

    iget-object v4, p0, LX/FsI;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "image_large_aspect_width"

    iget-object v4, p0, LX/FsI;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "goodwill_small_accent_image"

    iget-object v4, p0, LX/FsI;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->h()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "include_replies_in_total_count"

    iget-object v4, p0, LX/FsI;->d:LX/0ad;

    sget-short v5, LX/0wg;->i:S

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "default_image_scale"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "icon_scale"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v2, "timeline_stories"

    .line 2296746
    iget-object v3, p0, LX/FsI;->d:LX/0ad;

    sget v4, LX/0wf;->aP:I

    const/4 v5, 0x4

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    move v3, v3

    .line 2296747
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "action_location"

    sget-object v3, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v3}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "omit_unseen_stories"

    .line 2296748
    iget-boolean v3, p1, LX/G12;->c:Z

    move v3, v3

    .line 2296749
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "with_actor_profile_video_playback"

    iget-object v3, p0, LX/FsI;->d:LX/0ad;

    sget-short v4, LX/0wf;->aK:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/FsI;->j:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "enable_download"

    iget-object v3, p0, LX/FsI;->l:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "skip_sample_entities_fields"

    iget-object v3, p0, LX/FsI;->d:LX/0ad;

    sget-short v4, LX/0wn;->as:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v2, "rich_text_posts_enabled"

    iget-object v3, p0, LX/FsI;->k:LX/0tO;

    invoke-virtual {v3}, LX/0tO;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2296750
    iget-object v0, p1, LX/G12;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2296751
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2296752
    const-string v0, "timeline_filter"

    .line 2296753
    iget-object v2, p1, LX/G12;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2296754
    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2296755
    :cond_0
    iget-object v0, p0, LX/FsI;->c:LX/0se;

    iget-object v2, p0, LX/FsI;->b:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->c()LX/0wF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2296756
    iget-object v0, p0, LX/FsI;->e:LX/0tI;

    invoke-virtual {v0, v1}, LX/0tI;->a(LX/0gW;)V

    .line 2296757
    invoke-static {v1}, LX/0wo;->a(LX/0gW;)V

    .line 2296758
    iget-object v0, p0, LX/FsI;->f:LX/0Or;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FsI;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 2296759
    const/4 v0, 0x1

    move v0, v0

    .line 2296760
    if-eqz v0, :cond_1

    .line 2296761
    const-string v0, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2296762
    :cond_1
    iget-object v0, p0, LX/FsI;->g:LX/0sU;

    const-string v2, "PROFILE"

    invoke-virtual {v0, v1, v2}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 2296763
    return-object v1
.end method
