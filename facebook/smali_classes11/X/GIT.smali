.class public LX/GIT;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

.field public b:LX/GLb;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

.field public d:LX/GLd;

.field public e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public f:Landroid/view/View;

.field public g:Z

.field public h:Z

.field public i:I


# direct methods
.method public constructor <init>(LX/GLd;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336705
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336706
    iput-object p1, p0, LX/GIT;->d:LX/GLd;

    .line 2336707
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GIT;->g:Z

    .line 2336708
    return-void
.end method

.method public static b(LX/0QB;)LX/GIT;
    .locals 2

    .prologue
    .line 2336703
    new-instance v1, LX/GIT;

    invoke-static {p0}, LX/GLd;->c(LX/0QB;)LX/GLd;

    move-result-object v0

    check-cast v0, LX/GLd;

    invoke-direct {v1, v0}, LX/GIT;-><init>(LX/GLd;)V

    .line 2336704
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2336694
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336695
    iget-object v0, p0, LX/GIT;->b:LX/GLb;

    const-string v1, "see_all_audiences_task_key"

    invoke-virtual {v0, v1}, LX/GLb;->a(Ljava/lang/String;)V

    .line 2336696
    iget-boolean v0, p0, LX/GIT;->g:Z

    if-nez v0, :cond_0

    .line 2336697
    iget-object v0, p0, LX/GIT;->b:LX/GLb;

    const-string v1, "selector_unified_audiences_task_key"

    invoke-virtual {v0, v1}, LX/GLb;->a(Ljava/lang/String;)V

    .line 2336698
    :cond_0
    iget-object v0, p0, LX/GIT;->b:LX/GLb;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2336699
    iput-object v2, p0, LX/GIT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    .line 2336700
    iput-object v2, p0, LX/GIT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    .line 2336701
    iput-object v2, p0, LX/GIT;->f:Landroid/view/View;

    .line 2336702
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2336691
    invoke-super {p0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2336692
    iget-object v0, p0, LX/GIT;->d:LX/GLd;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2336693
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2336709
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2336710
    iget-object v0, p0, LX/GIT;->d:LX/GLd;

    invoke-virtual {v0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2336711
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336690
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIT;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2336689
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p0, p1}, LX/GIT;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 1

    .prologue
    .line 2336686
    iput-object p1, p0, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336687
    iget-object v0, p0, LX/GIT;->d:LX/GLd;

    invoke-virtual {v0, p1}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2336688
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2336636
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336637
    iput-object p1, p0, LX/GIT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    .line 2336638
    iget-object v1, p0, LX/GIT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    .line 2336639
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    move-object v1, v2

    .line 2336640
    iput-object v1, p0, LX/GIT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    .line 2336641
    iget-object v1, p0, LX/GIT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    iget-boolean v2, p0, LX/GIT;->g:Z

    .line 2336642
    iput-boolean v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->c:Z

    .line 2336643
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2336644
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2336645
    sget v2, LX/GDK;->k:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, LX/GIT;->h:Z

    .line 2336646
    iget-object v0, p0, LX/GIT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d1e12

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIT;->f:Landroid/view/View;

    .line 2336647
    iget-boolean v0, p0, LX/GIT;->g:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x14

    :goto_0
    iput v0, p0, LX/GIT;->i:I

    .line 2336648
    iget-object v0, p0, LX/GIT;->d:LX/GLd;

    .line 2336649
    iget-object v1, v0, LX/GLd;->q:LX/GLb;

    move-object v0, v1

    .line 2336650
    iput-object v0, p0, LX/GIT;->b:LX/GLb;

    .line 2336651
    const/4 v0, 0x1

    .line 2336652
    iget-object v1, p0, LX/GIT;->b:LX/GLb;

    .line 2336653
    iput-boolean v0, v1, LX/GLb;->m:Z

    .line 2336654
    iget-object v1, p0, LX/GIT;->b:LX/GLb;

    .line 2336655
    iput-boolean v0, v1, LX/GLb;->o:Z

    .line 2336656
    iget-object v1, p0, LX/GIT;->b:LX/GLb;

    iget v2, p0, LX/GIT;->i:I

    invoke-virtual {v1, v0, v2}, LX/GLb;->a(II)V

    .line 2336657
    iget-object v1, p0, LX/GIT;->b:LX/GLb;

    iget-boolean v2, p0, LX/GIT;->h:Z

    .line 2336658
    iput-boolean v2, v1, LX/GLb;->p:Z

    .line 2336659
    iget-object v1, p0, LX/GIT;->b:LX/GLb;

    iget-boolean v2, p0, LX/GIT;->h:Z

    if-nez v2, :cond_3

    .line 2336660
    :goto_1
    iput-boolean v0, v1, LX/GLb;->n:Z

    .line 2336661
    iget-object v0, p0, LX/GIT;->d:LX/GLd;

    iget-boolean v1, p0, LX/GIT;->h:Z

    .line 2336662
    iput-boolean v1, v0, LX/GLd;->s:Z

    .line 2336663
    iget-object v0, p0, LX/GIT;->d:LX/GLd;

    iget-object v1, p0, LX/GIT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v0, v1, p2}, LX/GLd;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336664
    iget-boolean v0, p0, LX/GIT;->g:Z

    if-eqz v0, :cond_1

    .line 2336665
    iget-object v0, p0, LX/GIT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setVisibility(I)V

    .line 2336666
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2336667
    new-instance v1, LX/GIP;

    invoke-direct {v1, p0}, LX/GIP;-><init>(LX/GIT;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2336668
    iget-object v0, p0, LX/GIT;->b:LX/GLb;

    invoke-virtual {v0}, LX/GLb;->c()V

    .line 2336669
    iget-object v0, p0, LX/GIT;->b:LX/GLb;

    iget-object v1, p0, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v1

    const-string v2, "see_all_audiences_task_key"

    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/GLb;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    .line 2336670
    :cond_1
    iget-boolean v0, p0, LX/GIT;->g:Z

    .line 2336671
    iget-object v1, p0, LX/GIT;->f:Landroid/view/View;

    if-nez v1, :cond_4

    .line 2336672
    :goto_2
    new-instance v0, LX/GIQ;

    invoke-direct {v0, p0}, LX/GIQ;-><init>(LX/GIT;)V

    .line 2336673
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2336674
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0}, LX/GCE;->a(ILX/GFR;)V

    .line 2336675
    iget-boolean v0, p0, LX/GIT;->g:Z

    if-eqz v0, :cond_6

    .line 2336676
    :goto_3
    return-void

    .line 2336677
    :cond_2
    const/4 v0, 0x4

    goto :goto_0

    .line 2336678
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2336679
    :cond_4
    if-nez v0, :cond_5

    .line 2336680
    iget-object v1, p0, LX/GIT;->f:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2336681
    :cond_5
    iget-object v1, p0, LX/GIT;->f:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2336682
    iget-object v1, p0, LX/GIT;->f:Landroid/view/View;

    new-instance v2, LX/GIS;

    invoke-direct {v2, p0}, LX/GIS;-><init>(LX/GIT;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2336683
    :cond_6
    new-instance v0, LX/GIR;

    invoke-direct {v0, p0}, LX/GIR;-><init>(LX/GIT;)V

    .line 2336684
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2336685
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v0}, LX/GCE;->a(ILX/GFR;)V

    goto :goto_3
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336633
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2336634
    iget-object v0, p0, LX/GIT;->d:LX/GLd;

    invoke-virtual {v0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2336635
    return-void
.end method
