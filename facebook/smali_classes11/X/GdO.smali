.class public final LX/GdO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DC2;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;)V
    .locals 0

    .prologue
    .line 2373878
    iput-object p1, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2373879
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v1, LX/0ig;->bb:LX/0ih;

    const-string v2, "fetch_start"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2373880
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2373881
    new-instance v1, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$1;-><init>(LX/GdO;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2373882
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2373883
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2373884
    new-instance v1, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$2;-><init>(LX/GdO;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2373885
    return-void
.end method

.method public final a(LX/DC6;)V
    .locals 3

    .prologue
    .line 2373886
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v1, LX/0ig;->bb:LX/0ih;

    const-string v2, "fetch_failure"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2373887
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2373888
    new-instance v1, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$4;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$4;-><init>(LX/GdO;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2373889
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2373890
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v1, LX/0ig;->bb:LX/0ih;

    const-string v2, "fetch_cancel"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2373891
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2373892
    new-instance v1, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$5;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$5;-><init>(LX/GdO;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2373893
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 2373894
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v1, LX/0ig;->bb:LX/0ih;

    const-string v2, "fetch_finish"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2373895
    iget-object v0, p0, LX/GdO;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2373896
    new-instance v1, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment$2$3;-><init>(LX/GdO;ILandroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2373897
    return-void
.end method
