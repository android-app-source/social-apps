.class public final LX/GUY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 2357451
    iput-object p1, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2357452
    iget-object v1, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->V:LX/0Uh;

    const/16 v2, 0x21d

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2357453
    iget-object v1, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->W:LX/GUi;

    if-nez p2, :cond_0

    const/4 v0, 0x1

    .line 2357454
    :cond_0
    iget-object v2, v1, LX/GUi;->a:LX/0Zb;

    const-string v3, "friends_nearby_settings_pause_toggle"

    invoke-static {v3}, LX/GUi;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "pause"

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2357455
    if-eqz p2, :cond_2

    .line 2357456
    iget-object v0, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-boolean v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ai:Z

    if-eqz v0, :cond_1

    .line 2357457
    iget-object v0, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->E(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    .line 2357458
    :goto_0
    return-void

    .line 2357459
    :cond_1
    iget-object v0, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->C(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    goto :goto_0

    .line 2357460
    :cond_2
    iget-object v0, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->B(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    goto :goto_0

    .line 2357461
    :cond_3
    iget-object v0, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->W:LX/GUi;

    .line 2357462
    iget-object v1, v0, LX/GUi;->a:LX/0Zb;

    const-string v2, "friends_nearby_settings_toggle"

    invoke-static {v2}, LX/GUi;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "is_on"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2357463
    if-eqz p2, :cond_4

    .line 2357464
    iget-object v0, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->C(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    goto :goto_0

    .line 2357465
    :cond_4
    iget-object v0, p0, LX/GUY;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->D(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    goto :goto_0
.end method
