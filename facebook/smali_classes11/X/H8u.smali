.class public final LX/H8u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/models/bookmark_favorites/PageFavoriteBookmarksGraphQLModels$FBBookmarkRemoveFromFavoritesMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/H8y;


# direct methods
.method public constructor <init>(LX/H8y;Z)V
    .locals 0

    .prologue
    .line 2433761
    iput-object p1, p0, LX/H8u;->b:LX/H8y;

    iput-boolean p2, p0, LX/H8u;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2433754
    iget-object v0, p0, LX/H8u;->b:LX/H8y;

    iget-object v0, v0, LX/H8y;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XA;->EVENT_PAGE_REMOVE_FROM_FAVORITES_ERROR:LX/9XA;

    iget-object v2, p0, LX/H8u;->b:LX/H8y;

    iget-object v2, v2, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433755
    iget-object v0, p0, LX/H8u;->b:LX/H8y;

    iget-boolean v1, p0, LX/H8u;->a:Z

    invoke-static {v0, v1}, LX/H8y;->a$redex0(LX/H8y;Z)V

    .line 2433756
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2433757
    iget-object v0, p0, LX/H8u;->b:LX/H8y;

    iget-object v0, v0, LX/H8y;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2l5;

    invoke-interface {v0}, LX/2l5;->a()V

    .line 2433758
    iget-object v0, p0, LX/H8u;->b:LX/H8y;

    iget-object v0, v0, LX/H8y;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XB;->EVENT_PAGE_REMOVE_FROM_FAVORITES_SUCCESS:LX/9XB;

    iget-object v2, p0, LX/H8u;->b:LX/H8y;

    iget-object v2, v2, LX/H8y;->m:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433759
    iget-object v0, p0, LX/H8u;->b:LX/H8y;

    iget-object v0, v0, LX/H8y;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08165c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2433760
    return-void
.end method
