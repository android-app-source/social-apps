.class public LX/GQO;
.super Landroid/preference/Preference;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2YW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Dg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2350114
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2350115
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/GQO;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2YW;->b(LX/0QB;)LX/2YW;

    move-result-object v4

    check-cast v4, LX/2YW;

    invoke-static {v0}, LX/2Dg;->a(LX/0QB;)LX/2Dg;

    move-result-object v5

    check-cast v5, LX/2Dg;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v3, v2, LX/GQO;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v2, LX/GQO;->b:LX/2YW;

    iput-object v5, v2, LX/GQO;->c:LX/2Dg;

    iput-object p1, v2, LX/GQO;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v0, v2, LX/GQO;->e:Ljava/util/concurrent/ExecutorService;

    .line 2350116
    const-string v0, "Force Refresh Aldrin User Status"

    invoke-virtual {p0, v0}, LX/GQO;->setTitle(Ljava/lang/CharSequence;)V

    .line 2350117
    invoke-static {p0}, LX/GQO;->a$redex0(LX/GQO;)V

    .line 2350118
    new-instance v0, Lcom/facebook/aldrin/prefs/RefreshAldrinUserStatusPreference$1;

    invoke-direct {v0, p0}, Lcom/facebook/aldrin/prefs/RefreshAldrinUserStatusPreference$1;-><init>(LX/GQO;)V

    invoke-virtual {p0, v0}, LX/GQO;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2350119
    return-void
.end method

.method public static a$redex0(LX/GQO;)V
    .locals 10

    .prologue
    .line 2350093
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2350094
    iget-object v1, p0, LX/GQO;->c:LX/2Dg;

    invoke-virtual {v1}, LX/2Dg;->d()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v1

    .line 2350095
    if-nez v1, :cond_0

    .line 2350096
    const-string v1, "status: null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2350097
    :goto_0
    iget-object v1, p0, LX/GQO;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2YT;->a:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 2350098
    const-string v1, "\nLast fetch time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2350099
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_1

    .line 2350100
    const-string v6, ""

    .line 2350101
    :goto_1
    move-object v1, v6

    .line 2350102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2350103
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GQO;->setSummary(Ljava/lang/CharSequence;)V

    .line 2350104
    return-void

    .line 2350105
    :cond_0
    const-string v2, "\nEffective region: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2350106
    iget-object v2, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2350107
    const-string v2, "\nCurrent region: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2350108
    iget-object v2, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2350109
    const-string v2, "\nTOS transition type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2350110
    iget-object v1, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2350111
    :cond_1
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 2350112
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v8, "MMM d, hh:mm:ss a z"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v7, v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2350113
    invoke-virtual {v7, v6}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method
