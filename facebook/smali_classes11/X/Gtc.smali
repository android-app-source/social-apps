.class public LX/Gtc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2402045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2402046
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2402047
    sget-object v0, LX/0ax;->ck:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 2402048
    new-instance v1, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;

    invoke-direct {v1}, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;-><init>()V

    .line 2402049
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2402050
    const-string p1, "bookmarks_group"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2402051
    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2402052
    move-object v0, v1

    .line 2402053
    return-object v0
.end method
