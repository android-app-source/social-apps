.class public LX/FIw;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:LX/0wY;

.field public final b:LX/0SG;

.field public final c:LX/FIy;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FIq;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FIq;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/view/animation/Interpolator;

.field public g:J

.field public h:J

.field private final i:LX/0wa;


# direct methods
.method private constructor <init>(LX/0wY;LX/0SG;LX/FIy;)V
    .locals 8
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2222744
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2222745
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FIw;->d:Ljava/util/List;

    .line 2222746
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FIw;->e:Ljava/util/List;

    .line 2222747
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    const/high16 v1, 0x40200000    # 2.5f

    invoke-direct {v0, v1}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    iput-object v0, p0, LX/FIw;->f:Landroid/view/animation/Interpolator;

    .line 2222748
    new-instance v0, LX/FIv;

    invoke-direct {v0, p0}, LX/FIv;-><init>(LX/FIw;)V

    iput-object v0, p0, LX/FIw;->i:LX/0wa;

    .line 2222749
    iput-object p1, p0, LX/FIw;->a:LX/0wY;

    .line 2222750
    iput-object p2, p0, LX/FIw;->b:LX/0SG;

    .line 2222751
    iput-object p3, p0, LX/FIw;->c:LX/FIy;

    .line 2222752
    iget-object v2, p0, LX/FIw;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2222753
    iget-object v2, p0, LX/FIw;->d:Ljava/util/List;

    iget-object v3, p0, LX/FIw;->c:LX/FIy;

    invoke-virtual {v3}, LX/FIy;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2222754
    iget-object v2, p0, LX/FIw;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2222755
    iget-object v2, p0, LX/FIw;->e:Ljava/util/List;

    iget-object v3, p0, LX/FIw;->c:LX/FIy;

    invoke-virtual {v3}, LX/FIy;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2222756
    iget-object v2, p0, LX/FIw;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/FIw;->g:J

    .line 2222757
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/FIw;->h:J

    .line 2222758
    iget-object v2, p0, LX/FIw;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FIq;

    .line 2222759
    iget-wide v4, v2, LX/FIq;->i:J

    iget-wide v6, p0, LX/FIw;->h:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, LX/FIw;->h:J

    goto :goto_0

    .line 2222760
    :cond_0
    iget-object v2, p0, LX/FIw;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FIq;

    .line 2222761
    iget-wide v4, v2, LX/FIq;->i:J

    const-wide/16 v6, 0xc8

    add-long/2addr v4, v6

    iget-wide v6, p0, LX/FIw;->h:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, LX/FIw;->h:J

    goto :goto_1

    .line 2222762
    :cond_1
    invoke-virtual {p0}, LX/FIw;->invalidateSelf()V

    .line 2222763
    return-void
.end method

.method private a(Landroid/graphics/Canvas;LX/FIq;IIJ)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x1

    .line 2222764
    long-to-float v0, p5

    iget-wide v6, p2, LX/FIq;->h:J

    long-to-float v3, v6

    iget-wide v6, p2, LX/FIq;->i:J

    long-to-float v5, v6

    .line 2222765
    invoke-static {v0, v3, v5}, LX/0yq;->c(FFF)F

    move-result v6

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {v6, v7, v8}, LX/0yq;->b(FFF)F

    move-result v6

    move v0, v6

    .line 2222766
    iget-object v3, p0, LX/FIw;->f:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v5

    .line 2222767
    invoke-virtual {p0}, LX/FIw;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    if-le p3, v0, :cond_1

    move v0, v1

    .line 2222768
    :goto_0
    int-to-float v6, p3

    iget v7, p2, LX/FIq;->a:F

    if-eqz v0, :cond_2

    move v3, v4

    :goto_1
    int-to-float v3, v3

    mul-float/2addr v3, v7

    add-float/2addr v3, v6

    .line 2222769
    int-to-float v6, p4

    iget v7, p2, LX/FIq;->b:F

    if-eqz v0, :cond_3

    :goto_2
    int-to-float v1, v4

    mul-float/2addr v1, v7

    add-float/2addr v1, v6

    .line 2222770
    iget v4, p2, LX/FIq;->d:F

    if-eqz v0, :cond_0

    const/16 v2, 0xb4

    :cond_0
    int-to-float v0, v2

    add-float/2addr v0, v4

    .line 2222771
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v5

    iget v4, p2, LX/FIq;->e:F

    mul-float/2addr v2, v4

    add-float/2addr v0, v2

    .line 2222772
    iget v2, p2, LX/FIq;->c:F

    mul-float/2addr v2, v5

    iget-object v4, p2, LX/FIq;->f:LX/FIs;

    iget v4, v4, LX/FIs;->b:F

    mul-float/2addr v2, v4

    .line 2222773
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2222774
    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2222775
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2222776
    invoke-virtual {p1, v2, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 2222777
    iget-object v0, p2, LX/FIq;->f:LX/FIs;

    .line 2222778
    iget-object v1, v0, LX/FIs;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    .line 2222779
    iget-object v2, v0, LX/FIs;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v2

    .line 2222780
    iget-object v3, v0, LX/FIs;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    neg-int v1, v1

    int-to-float v1, v1

    iget v4, v0, LX/FIs;->c:F

    mul-float/2addr v1, v4

    neg-int v2, v2

    int-to-float v2, v2

    iget v0, v0, LX/FIs;->d:F

    mul-float/2addr v0, v2

    iget-object v2, p2, LX/FIq;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2222781
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2222782
    return-void

    :cond_1
    move v0, v2

    .line 2222783
    goto :goto_0

    :cond_2
    move v3, v1

    .line 2222784
    goto :goto_1

    :cond_3
    move v4, v1

    .line 2222785
    goto :goto_2
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 18

    .prologue
    .line 2222786
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FIw;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/FIw;->g:J

    sub-long v8, v2, v4

    .line 2222787
    invoke-virtual/range {p0 .. p0}, LX/FIw;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 2222788
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FIw;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FIq;

    .line 2222789
    iget v6, v2, Landroid/graphics/Rect;->left:I

    iget v7, v2, Landroid/graphics/Rect;->top:I

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v9}, LX/FIw;->a(Landroid/graphics/Canvas;LX/FIq;IIJ)V

    goto :goto_0

    .line 2222790
    :cond_0
    const-wide/16 v4, 0xc8

    sub-long v16, v8, v4

    .line 2222791
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FIw;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/FIq;

    .line 2222792
    iget v14, v2, Landroid/graphics/Rect;->right:I

    iget v15, v2, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-direct/range {v11 .. v17}, LX/FIw;->a(Landroid/graphics/Canvas;LX/FIq;IIJ)V

    goto :goto_1

    .line 2222793
    :cond_1
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/FIw;->h:J

    cmp-long v2, v8, v2

    if-gez v2, :cond_2

    .line 2222794
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FIw;->a:LX/0wY;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FIw;->i:LX/0wa;

    invoke-interface {v2, v3}, LX/0wY;->b(LX/0wa;)V

    .line 2222795
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FIw;->a:LX/0wY;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FIw;->i:LX/0wa;

    invoke-interface {v2, v3}, LX/0wY;->a(LX/0wa;)V

    .line 2222796
    :cond_2
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2222797
    const/4 v0, 0x0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 2222798
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 2222799
    return-void
.end method
