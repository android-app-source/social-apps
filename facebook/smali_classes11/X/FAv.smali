.class public LX/FAv;
.super LX/3pF;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FAu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/FAu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2208527
    invoke-direct {p0, p1}, LX/3pF;-><init>(LX/0gc;)V

    .line 2208528
    iput-object p2, p0, LX/FAv;->a:Landroid/content/Context;

    .line 2208529
    iput-object p3, p0, LX/FAv;->b:Ljava/util/List;

    .line 2208530
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2208531
    iget-object v0, p0, LX/FAv;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, LX/FAv;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FAu;

    iget v0, v0, LX/FAu;->b:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2208532
    iget-object v0, p0, LX/FAv;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FAu;

    .line 2208533
    iget-object v1, p0, LX/FAv;->a:Landroid/content/Context;

    iget-object v2, v0, LX/FAu;->c:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, LX/FAu;->d:Landroid/os/Bundle;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2208534
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2208535
    iget-object v0, p0, LX/FAv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
