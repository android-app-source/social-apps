.class public LX/FwG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FwE;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/FwT;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FwW;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/Executor;

.field private final f:LX/0ad;

.field public final g:LX/BQP;

.field private final h:LX/5SB;

.field private final i:LX/BQ1;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9hF;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/BQ9;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/BQ9;LX/5SB;LX/0QR;LX/BQ1;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/0Ot;Ljava/util/concurrent/Executor;LX/0ad;LX/BQP;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BQ9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0QR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/BQ9;",
            "LX/5SB;",
            "LX/0QR",
            "<",
            "LX/FwT;",
            ">;",
            "LX/BQ1;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/9hF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FwW;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "LX/0ad;",
            "LX/BQP;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2302959
    iput-object p1, p0, LX/FwG;->a:Landroid/app/Activity;

    .line 2302960
    iput-object p2, p0, LX/FwG;->l:LX/BQ9;

    .line 2302961
    iput-object p3, p0, LX/FwG;->h:LX/5SB;

    .line 2302962
    iput-object p4, p0, LX/FwG;->b:LX/0QR;

    .line 2302963
    iput-object p5, p0, LX/FwG;->i:LX/BQ1;

    .line 2302964
    iput-object p6, p0, LX/FwG;->j:LX/0Or;

    .line 2302965
    iput-object p7, p0, LX/FwG;->k:LX/0Or;

    .line 2302966
    iput-object p8, p0, LX/FwG;->c:LX/0Ot;

    .line 2302967
    iput-object p9, p0, LX/FwG;->d:LX/0Or;

    .line 2302968
    iput-object p10, p0, LX/FwG;->m:LX/0Ot;

    .line 2302969
    iput-object p11, p0, LX/FwG;->e:Ljava/util/concurrent/Executor;

    .line 2302970
    iput-object p12, p0, LX/FwG;->f:LX/0ad;

    .line 2302971
    iput-object p13, p0, LX/FwG;->g:LX/BQP;

    .line 2302972
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 2302926
    iget-object v0, p0, LX/FwG;->l:LX/BQ9;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FwG;->h:LX/5SB;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FwG;->i:LX/BQ1;

    if-eqz v0, :cond_0

    .line 2302927
    iget-object v0, p0, LX/FwG;->l:LX/BQ9;

    iget-object v1, p0, LX/FwG;->h:LX/5SB;

    .line 2302928
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2302929
    const-string v5, "profile_picture_popover_menu_item_click"

    const/4 v9, 0x0

    move-object v4, v0

    move-wide v6, v2

    move-object v8, p1

    invoke-static/range {v4 .. v9}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2302930
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2302931
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 2302978
    iget-object v0, p0, LX/FwG;->b:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    iget-object v1, p0, LX/FwG;->i:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->K()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/FwG;->i:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v2

    .line 2302979
    iget-object v3, v0, LX/FwT;->m:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/B3r;

    iget-object v4, v0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    const/4 v5, 0x0

    const/16 v8, 0xc36

    move-object v6, v1

    move-object v7, v2

    invoke-interface/range {v3 .. v8}, LX/B3r;->a(Landroid/support/v4/app/Fragment;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2302980
    return-void
.end method

.method public final a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;LX/1bf;LX/1Fb;)V
    .locals 4

    .prologue
    .line 2302973
    const-string v0, "view_picture"

    invoke-direct {p0, v0}, LX/FwG;->a(Ljava/lang/String;)V

    .line 2302974
    invoke-static {p2, p4}, LX/FwM;->a(Ljava/lang/String;LX/1Fb;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2302975
    iget-object v1, p0, LX/FwG;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {p2}, LX/9hF;->f(Ljava/lang/String;)LX/9hE;

    move-result-object v1

    sget-object v2, LX/74S;->TIMELINE_PROFILE_PHOTO:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/9hD;->a(LX/0Px;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2302976
    iget-object v0, p0, LX/FwG;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    iget-object v2, p0, LX/FwG;->a:Landroid/app/Activity;

    invoke-static {p2, p1, p3}, LX/FwM;->a(Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;)LX/9hN;

    move-result-object v3

    invoke-interface {v0, v2, v1, v3}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2302977
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2302952
    const-string v0, "selector_picture"

    invoke-direct {p0, v0}, LX/FwG;->a(Ljava/lang/String;)V

    .line 2302953
    iget-object v0, p0, LX/FwG;->b:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    iget-object v1, p0, LX/FwG;->f:LX/0ad;

    sget-short v2, LX/0wf;->aE:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1, v3}, LX/FwT;->b(ZZ)V

    .line 2302954
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2302955
    const-string v0, "upload_picture"

    invoke-direct {p0, v0}, LX/FwG;->a(Ljava/lang/String;)V

    .line 2302956
    iget-object v0, p0, LX/FwG;->b:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/FwT;->a(J)V

    .line 2302957
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 2302948
    iget-object v0, p0, LX/FwG;->b:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    .line 2302949
    new-instance v2, Landroid/content/Intent;

    iget-object v1, v0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2302950
    iget-object v1, v0, LX/FwT;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x82

    iget-object p0, v0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v2, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2302951
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 2302944
    iget-object v0, p0, LX/FwG;->b:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    .line 2302945
    new-instance v2, Landroid/content/Intent;

    iget-object v1, v0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Lcom/facebook/timeline/tempprofilepic/EditEndTimeActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2302946
    iget-object v1, v0, LX/FwT;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x114d

    iget-object p0, v0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v2, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2302947
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 2302932
    iget-object v0, p0, LX/FwG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwW;

    .line 2302933
    new-instance v1, LX/FwV;

    invoke-direct {v1}, LX/FwV;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2302934
    const-string v3, "client_mutation_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2302935
    move-object v1, v1

    .line 2302936
    iget-object v2, v0, LX/FwW;->a:Ljava/lang/String;

    .line 2302937
    const-string v3, "actor_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2302938
    move-object v1, v1

    .line 2302939
    new-instance v2, LX/5yr;

    invoke-direct {v2}, LX/5yr;-><init>()V

    move-object v2, v2

    .line 2302940
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2302941
    iget-object v1, v0, LX/FwW;->b:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2302942
    new-instance v1, LX/FwF;

    invoke-direct {v1, p0}, LX/FwF;-><init>(LX/FwG;)V

    iget-object v2, p0, LX/FwG;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2302943
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2302924
    const-string v0, "view_profile_video"

    invoke-direct {p0, v0}, LX/FwG;->a(Ljava/lang/String;)V

    .line 2302925
    return-void
.end method
