.class public final LX/FCu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

.field private final b:LX/FCa;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/FCa;)V
    .locals 0

    .prologue
    .line 2210749
    iput-object p1, p0, LX/FCu;->a:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210750
    iput-object p2, p0, LX/FCu;->b:LX/FCa;

    .line 2210751
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2210752
    :try_start_0
    iget-object v0, p0, LX/FCu;->a:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    iget-object v0, v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->b:LX/2Vw;

    iget-object v1, p0, LX/FCu;->b:LX/FCa;

    invoke-virtual {v0, v1, p1}, LX/2Vx;->a(LX/2WG;Ljava/io/InputStream;)LX/1gI;

    .line 2210753
    iget-object v0, p0, LX/FCu;->a:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    iget-object v0, v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->b:LX/2Vw;

    iget-object v1, p0, LX/FCu;->b:LX/FCa;

    invoke-virtual {v0, v1}, LX/2Vx;->d(LX/2WG;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2210754
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method
