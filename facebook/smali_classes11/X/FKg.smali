.class public LX/FKg;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/messaging/service/model/FetchThreadParams;",
        "Lcom/facebook/messaging/service/model/FetchThreadResult;",
        ">;"
    }
.end annotation


# instance fields
.field public b:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

.field public c:LX/3Mw;

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0sO;Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/3Mw;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sO;",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            "LX/3Mw;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2225415
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2225416
    iput-object p2, p0, LX/FKg;->b:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2225417
    iput-object p3, p0, LX/FKg;->c:LX/3Mw;

    .line 2225418
    iput-object p4, p0, LX/FKg;->d:LX/0Or;

    .line 2225419
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2225420
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 2225421
    iget-object v1, p0, LX/FKg;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2225422
    iget-object v2, p0, LX/FKg;->c:LX/3Mw;

    iget-object v3, p0, LX/FKg;->c:LX/3Mw;

    invoke-virtual {v3, v0, v1}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    iget-object v1, p0, LX/FKg;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;Lcom/facebook/user/model/User;LX/0Px;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2225423
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2225424
    check-cast p1, Lcom/facebook/messaging/service/model/FetchThreadParams;

    const/4 v3, 0x1

    .line 2225425
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v0, v0

    .line 2225426
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    .line 2225427
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2225428
    iget-object v0, p0, LX/FKg;->b:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2225429
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v1, v1

    .line 2225430
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2225431
    iget v2, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v2, v2

    .line 2225432
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(LX/0Px;ILjava/lang/Boolean;)LX/5ZA;

    move-result-object v0

    .line 2225433
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/FKg;->b:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2225434
    iget v2, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v2, v2

    .line 2225435
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;ILjava/lang/Boolean;)LX/5ZA;

    move-result-object v0

    goto :goto_0
.end method
