.class public LX/FxB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field public b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/fig/mediagrid/FigMediaGrid;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2304545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304546
    iput-object p1, p0, LX/FxB;->a:LX/0ad;

    .line 2304547
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2304548
    iget-object v0, p0, LX/FxB;->a:LX/0ad;

    sget-short v1, LX/0wf;->ax:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2304549
    const v0, 0x7f0314de

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2304550
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d2f3c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iput-object v0, p0, LX/FxB;->c:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2304551
    iget-object v0, p0, LX/FxB;->c:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-virtual {v0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2304552
    invoke-virtual {v0, v2, p2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2304553
    iget-object v1, p0, LX/FxB;->c:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2304554
    :goto_0
    return-void

    .line 2304555
    :cond_0
    const v0, 0x7f0314df

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2304556
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d2f3d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    iput-object v0, p0, LX/FxB;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    .line 2304557
    iget-object v0, p0, LX/FxB;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    invoke-virtual {v0, v2, p2, v2, v2}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->a(IIII)V

    goto :goto_0
.end method
