.class public LX/GXY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/7ja;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/7ja;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2364333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static i(LX/GXY;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2364379
    iget-object v0, p0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->l()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()LX/2uF;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v5, -0x40c13bea

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2364365
    invoke-static {p0}, LX/GXY;->m(LX/GXY;)Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;

    move-result-object v0

    .line 2364366
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;->l()Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$ProductCatalogFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2364367
    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;->l()Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$ProductCatalogFieldsModel;

    move-result-object v0

    .line 2364368
    :goto_0
    move-object v3, v0

    .line 2364369
    if-eqz v3, :cond_2

    .line 2364370
    invoke-virtual {v3}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$ProductCatalogFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2364371
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 2364372
    invoke-virtual {v3}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$ProductCatalogFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2364373
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    if-eqz v2, :cond_5

    .line 2364374
    invoke-virtual {v3}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$ProductCatalogFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v2, v0, v1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2364375
    :goto_3
    return-object v0

    :cond_1
    move v0, v2

    .line 2364376
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    .line 2364377
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2364378
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(LX/GXY;)Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2364362
    iget-object v0, p0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2364363
    iget-object v0, p0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;

    move-result-object v0

    .line 2364364
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2364348
    iget-object v2, p0, LX/GXY;->b:LX/0Px;

    if-eqz v2, :cond_1

    .line 2364349
    iget-object v2, p0, LX/GXY;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 2364350
    iget-object v0, p0, LX/GXY;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    .line 2364351
    :goto_0
    return-object v0

    .line 2364352
    :cond_0
    iget-object v2, p0, LX/GXY;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    sub-int/2addr p1, v2

    .line 2364353
    :cond_1
    invoke-direct {p0}, LX/GXY;->k()LX/2uF;

    move-result-object v2

    .line 2364354
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/39O;->c()I

    move-result v3

    if-le v3, p1, :cond_2

    .line 2364355
    invoke-virtual {v2, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2364356
    const-class v5, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    invoke-virtual {v4, v3, v1, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    if-eqz v3, :cond_2

    move v0, v1

    :cond_2
    if-eqz v0, :cond_4

    .line 2364357
    invoke-virtual {v2, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    invoke-virtual {v3, v0, v1, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/7ja;

    .line 2364358
    iget-object v3, p0, LX/GXY;->c:LX/0P1;

    invoke-interface {v0}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2364359
    iget-object v1, p0, LX/GXY;->c:LX/0P1;

    invoke-interface {v0}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    goto :goto_0

    .line 2364360
    :cond_3
    invoke-virtual {v2, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    goto :goto_0

    .line 2364361
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/Currency;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2364380
    invoke-static {p0}, LX/GXY;->m(LX/GXY;)Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;

    move-result-object v0

    .line 2364381
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2364382
    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2364383
    :goto_0
    move-object v0, v0

    .line 2364384
    if-eqz v0, :cond_0

    .line 2364385
    invoke-static {v0}, LX/7j4;->b(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    .line 2364386
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/7ja;)Z
    .locals 2

    .prologue
    .line 2364347
    invoke-static {p1}, LX/7ka;->a(LX/7ja;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/GXY;->c:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GXY;->c:LX/0P1;

    invoke-interface {p1}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2364342
    iget-object v0, p0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    if-nez v0, :cond_0

    .line 2364343
    const/4 v0, 0x0

    .line 2364344
    :goto_0
    return v0

    .line 2364345
    :cond_0
    iget-object v0, p0, LX/GXY;->a:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2364346
    invoke-static {v1, v0}, LX/7jZ;->a(LX/15i;I)I

    move-result v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2364339
    iget-object v0, p0, LX/GXY;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2364340
    iget-object v0, p0, LX/GXY;->e:Ljava/lang/String;

    .line 2364341
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/GXY;->i(LX/GXY;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2364334
    invoke-direct {p0}, LX/GXY;->k()LX/2uF;

    move-result-object v2

    .line 2364335
    iget-object v1, p0, LX/GXY;->b:LX/0Px;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/GXY;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2364336
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/39O;->c()I

    move-result v0

    .line 2364337
    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v1, v0

    .line 2364338
    goto :goto_0
.end method
