.class public final LX/Fco;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Fcp;


# direct methods
.method public constructor <init>(LX/Fcp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2262759
    iput-object p1, p0, LX/Fco;->d:LX/Fcp;

    iput-object p2, p0, LX/Fco;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Fco;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Fco;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    .prologue
    .line 2262760
    iget-object v0, p0, LX/Fco;->d:LX/Fcp;

    iget-object v0, v0, LX/Fcp;->f:LX/Fc4;

    iget-object v1, p0, LX/Fco;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/Fc4;->b(Ljava/lang/String;)V

    .line 2262761
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2262762
    iget-object v0, p0, LX/Fco;->d:LX/Fcp;

    iget-object v0, v0, LX/Fcp;->f:LX/Fc4;

    iget-object v1, p0, LX/Fco;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/Fc4;->a(Ljava/lang/String;)V

    .line 2262763
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2262764
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2262765
    if-eqz p1, :cond_0

    .line 2262766
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2262767
    if-eqz v0, :cond_0

    .line 2262768
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2262769
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel;->j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel$FilterValuesModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2262770
    :cond_0
    iget-object v0, p0, LX/Fco;->d:LX/Fcp;

    iget-object v0, v0, LX/Fcp;->e:LX/2Sc;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    const-string v2, "Needle Filter Value returned null"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2262771
    iget-object v0, p0, LX/Fco;->d:LX/Fcp;

    iget-object v0, v0, LX/Fcp;->f:LX/Fc4;

    iget-object v1, p0, LX/Fco;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/Fc4;->a(Ljava/lang/String;)V

    .line 2262772
    :goto_0
    return-void

    .line 2262773
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2262774
    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel;->j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel$FilterValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v2

    .line 2262775
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2262776
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel$FilterValuesModel$EdgesModel;

    .line 2262777
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2262778
    :try_start_0
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/Cwq;->a(Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v0

    .line 2262779
    iget-object v5, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2262780
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2262781
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2262782
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2262783
    :catch_0
    move-exception v0

    .line 2262784
    iget-object v5, p0, LX/Fco;->d:LX/Fcp;

    iget-object v5, v5, LX/Fcp;->e:LX/2Sc;

    invoke-virtual {v5, v0}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_2

    .line 2262785
    :cond_3
    iget-object v0, p0, LX/Fco;->d:LX/Fcp;

    iget-object v0, v0, LX/Fcp;->f:LX/Fc4;

    iget-object v1, p0, LX/Fco;->b:Ljava/lang/String;

    iget-object v2, p0, LX/Fco;->a:Ljava/lang/String;

    iget-object v4, p0, LX/Fco;->c:Ljava/lang/String;

    new-instance v5, LX/7Hc;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v5, v3}, LX/7Hc;-><init>(LX/0Px;)V

    invoke-interface {v0, v1, v2, v4, v5}, LX/Fc4;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7Hc;)V

    goto :goto_0
.end method
