.class public LX/HE4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2441481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2441482
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2441483
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2441484
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2441485
    const-string v1, "notification_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2441486
    const-string v4, "secondary_subscribe_status"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v4

    .line 2441487
    const-string v5, "subscribe_status"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    .line 2441488
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2441489
    const-string p0, "com.facebook.katana.profile.id"

    invoke-virtual {v5, p0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2441490
    const-string p0, "notification_status"

    invoke-virtual {v5, p0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2441491
    const-string p0, "secondary_subscribe_status"

    invoke-virtual {v5, p0, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2441492
    const-string p0, "subscribe_status"

    invoke-virtual {v5, p0, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2441493
    new-instance p0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-direct {p0}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;-><init>()V

    .line 2441494
    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2441495
    move-object v0, p0

    .line 2441496
    return-object v0
.end method
