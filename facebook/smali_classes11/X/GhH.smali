.class public final LX/GhH;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/gametime/ui/GametimeActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/GametimeActivity;)V
    .locals 0

    .prologue
    .line 2383812
    iput-object p1, p0, LX/GhH;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2383813
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2383814
    if-eqz v0, :cond_0

    .line 2383815
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2383816
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel$SportsMatchDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2383817
    iget-object v0, p0, LX/GhH;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    iget-object v1, v0, Lcom/facebook/gametime/ui/GametimeActivity;->z:Lcom/facebook/gametime/ui/GametimeHeaderView;

    .line 2383818
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2383819
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeMatchHeaderQueryModel$SportsMatchDataModel;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/gametime/ui/GametimeHeaderView;->setMatchData(LX/Ggh;)V

    .line 2383820
    :cond_0
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2383821
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2383822
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/GhH;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
