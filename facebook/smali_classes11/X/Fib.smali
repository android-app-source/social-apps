.class public final enum LX/Fib;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fib;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fib;

.field public static final enum HIDE:LX/Fib;

.field public static final enum SHOW:LX/Fib;

.field public static final enum UNCHANGED:LX/Fib;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2275514
    new-instance v0, LX/Fib;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v2}, LX/Fib;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fib;->SHOW:LX/Fib;

    new-instance v0, LX/Fib;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v3}, LX/Fib;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fib;->HIDE:LX/Fib;

    new-instance v0, LX/Fib;

    const-string v1, "UNCHANGED"

    invoke-direct {v0, v1, v4}, LX/Fib;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fib;->UNCHANGED:LX/Fib;

    .line 2275515
    const/4 v0, 0x3

    new-array v0, v0, [LX/Fib;

    sget-object v1, LX/Fib;->SHOW:LX/Fib;

    aput-object v1, v0, v2

    sget-object v1, LX/Fib;->HIDE:LX/Fib;

    aput-object v1, v0, v3

    sget-object v1, LX/Fib;->UNCHANGED:LX/Fib;

    aput-object v1, v0, v4

    sput-object v0, LX/Fib;->$VALUES:[LX/Fib;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2275516
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fib;
    .locals 1

    .prologue
    .line 2275517
    const-class v0, LX/Fib;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fib;

    return-object v0
.end method

.method public static values()[LX/Fib;
    .locals 1

    .prologue
    .line 2275518
    sget-object v0, LX/Fib;->$VALUES:[LX/Fib;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fib;

    return-object v0
.end method
