.class public LX/GKv;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Runnable;

.field private b:Ljava/lang/String;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field private d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public e:LX/3Lz;

.field private f:Landroid/view/inputmethod/InputMethodManager;

.field private g:LX/GDS;

.field public h:Landroid/content/res/Resources;

.field public i:Lcom/facebook/widget/text/BetterEditTextView;

.field public j:LX/GCE;

.field public k:Z

.field public l:I

.field public m:Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

.field private n:Ljava/lang/String;

.field public o:Z

.field private p:Landroid/text/Spanned;

.field private q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

.field private r:LX/GDN;


# direct methods
.method public constructor <init>(LX/3Lz;Landroid/view/inputmethod/InputMethodManager;LX/GDS;Landroid/content/res/Resources;LX/GDN;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2341542
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2341543
    new-instance v0, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberViewController$1;

    invoke-direct {v0, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberViewController$1;-><init>(LX/GKv;)V

    iput-object v0, p0, LX/GKv;->a:Ljava/lang/Runnable;

    .line 2341544
    const/4 v0, -0x1

    iput v0, p0, LX/GKv;->l:I

    .line 2341545
    iput-object p1, p0, LX/GKv;->e:LX/3Lz;

    .line 2341546
    iput-object p2, p0, LX/GKv;->f:Landroid/view/inputmethod/InputMethodManager;

    .line 2341547
    iput-object p3, p0, LX/GKv;->g:LX/GDS;

    .line 2341548
    iput-object p4, p0, LX/GKv;->h:Landroid/content/res/Resources;

    .line 2341549
    iput-object p5, p0, LX/GKv;->r:LX/GDN;

    .line 2341550
    return-void
.end method

.method public static a(LX/0QB;)LX/GKv;
    .locals 1

    .prologue
    .line 2341703
    invoke-static {p0}, LX/GKv;->b(LX/0QB;)LX/GKv;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 2

    .prologue
    .line 2341694
    iput-object p1, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341695
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341696
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    move-object v0, v1

    .line 2341697
    iput-object v0, p0, LX/GKv;->b:Ljava/lang/String;

    .line 2341698
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341699
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2341700
    iput-object v0, p0, LX/GKv;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2341701
    iget-object v0, p0, LX/GKv;->h:Landroid/content/res/Resources;

    const v1, 0x7f080b14

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, LX/GKv;->p:Landroid/text/Spanned;

    .line 2341702
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341673
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341674
    iput-object p2, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2341675
    iput-object p1, p0, LX/GKv;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

    .line 2341676
    iget-object v0, p1, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    move-object v0, v0

    .line 2341677
    iput-object v0, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2341678
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2341679
    iput-object v0, p0, LX/GKv;->j:LX/GCE;

    .line 2341680
    invoke-direct {p0}, LX/GKv;->e()V

    .line 2341681
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341682
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2341683
    if-eqz v0, :cond_0

    .line 2341684
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341685
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2341686
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v0, v1

    .line 2341687
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, LX/GKv;->b(LX/GKv;Z)V

    .line 2341688
    :cond_0
    iget-object v0, p0, LX/GKv;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2341689
    iget-object v0, p0, LX/GKv;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/GKv;->a(Ljava/lang/String;)V

    .line 2341690
    :cond_1
    invoke-direct {p0}, LX/GKv;->g()V

    .line 2341691
    invoke-virtual {p0}, LX/GKv;->b()V

    .line 2341692
    return-void

    .line 2341693
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2341656
    :try_start_0
    iget-object v3, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341657
    iput-object p1, v3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    .line 2341658
    iget-boolean v3, p0, LX/GKv;->k:Z

    if-eqz v3, :cond_0

    .line 2341659
    iget-object v3, p0, LX/GKv;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    sget-object v4, LX/GCi;->CALL_NOW_WRAPPER:LX/GCi;

    iget-object v5, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v4, v5}, LX/GCi;->getUri(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Ljava/lang/String;

    move-result-object v4

    .line 2341660
    iput-object v4, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2341661
    :cond_0
    iget-object v3, p0, LX/GKv;->e:LX/3Lz;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, LX/3Lz;->parseAndKeepRawInput(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v3

    .line 2341662
    iget-object v4, p0, LX/GKv;->e:LX/3Lz;

    invoke-virtual {v4, v3}, LX/3Lz;->isValidNumber(LX/4hT;)Z

    move-result v4

    iput-boolean v4, p0, LX/GKv;->o:Z

    .line 2341663
    iget-wide v6, v3, LX/4hT;->nationalNumber_:J

    move-wide v4, v6

    .line 2341664
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LX/GKv;->n:Ljava/lang/String;

    .line 2341665
    iget v4, v3, LX/4hT;->countryCode_:I

    move v3, v4

    .line 2341666
    iput v3, p0, LX/GKv;->l:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2341667
    iget-object v3, p0, LX/GKv;->j:LX/GCE;

    sget-object v4, LX/GG8;->PHONE_NUMBER:LX/GG8;

    iget-boolean v5, p0, LX/GKv;->k:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, LX/GKv;->o:Z

    if-eqz v5, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    invoke-virtual {v3, v4, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2341668
    :goto_0
    iget-object v1, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-boolean v0, p0, LX/GKv;->o:Z

    if-eqz v0, :cond_7

    move-object v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2341669
    return-void

    .line 2341670
    :catch_0
    const/4 v3, 0x0

    :try_start_1
    iput-boolean v3, p0, LX/GKv;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2341671
    iget-object v3, p0, LX/GKv;->j:LX/GCE;

    sget-object v4, LX/GG8;->PHONE_NUMBER:LX/GG8;

    iget-boolean v5, p0, LX/GKv;->k:Z

    if-eqz v5, :cond_3

    iget-boolean v5, p0, LX/GKv;->o:Z

    if-eqz v5, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    invoke-virtual {v3, v4, v0}, LX/GCE;->a(LX/GG8;Z)V

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, LX/GKv;->j:LX/GCE;

    sget-object v4, LX/GG8;->PHONE_NUMBER:LX/GG8;

    iget-boolean v5, p0, LX/GKv;->k:Z

    if-eqz v5, :cond_5

    iget-boolean v5, p0, LX/GKv;->o:Z

    if-eqz v5, :cond_6

    :cond_5
    move v0, v1

    :cond_6
    invoke-virtual {v3, v4, v0}, LX/GCE;->a(LX/GG8;Z)V

    throw v2

    .line 2341672
    :cond_7
    iget-object v0, p0, LX/GKv;->p:Landroid/text/Spanned;

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2341642
    iget-object v2, p0, LX/GKv;->j:LX/GCE;

    sget-object v3, LX/GG8;->PHONE_NUMBER:LX/GG8;

    iget-boolean v0, p0, LX/GKv;->k:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2341643
    iget-object v0, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2341644
    iget-object v0, p0, LX/GKv;->r:LX/GDN;

    new-instance v1, LX/GKt;

    invoke-direct {v1, p0}, LX/GKt;-><init>(LX/GKv;)V

    .line 2341645
    new-instance v2, LX/4Cb;

    invoke-direct {v2}, LX/4Cb;-><init>()V

    .line 2341646
    const-string v3, "audience_id"

    invoke-virtual {v2, v3, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341647
    move-object v2, v2

    .line 2341648
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2341649
    const-string v4, "audience_code"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341650
    move-object v2, v2

    .line 2341651
    new-instance v3, LX/A8r;

    invoke-direct {v3}, LX/A8r;-><init>()V

    move-object v3, v3

    .line 2341652
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/A8r;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2341653
    iget-object v3, v0, LX/GDN;->a:LX/1Ck;

    sget-object v4, LX/GDM;->TASK_TARGETING_COUNTRY_CODE:LX/GDM;

    iget-object p0, v0, LX/GDN;->c:LX/0tX;

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance p0, LX/GDL;

    invoke-direct {p0, v0, v1}, LX/GDL;-><init>(LX/GDN;LX/GKt;)V

    invoke-virtual {v3, v4, v2, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2341654
    return-void

    .line 2341655
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/GKv;Landroid/location/Location;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 2341632
    iget-object v2, p0, LX/GKv;->j:LX/GCE;

    sget-object v3, LX/GG8;->PHONE_NUMBER:LX/GG8;

    iget-boolean v0, p0, LX/GKv;->k:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2341633
    iget-object v0, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2341634
    iget-object v1, p0, LX/GKv;->g:LX/GDS;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    new-instance v6, LX/GKs;

    invoke-direct {v6, p0}, LX/GKs;-><init>(LX/GKv;)V

    .line 2341635
    new-instance v0, LX/2vb;

    invoke-direct {v0}, LX/2vb;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2341636
    new-instance v7, LX/4JC;

    invoke-direct {v7}, LX/4JC;-><init>()V

    invoke-virtual {v7, v0}, LX/4JC;->a(Ljava/util/List;)LX/4JC;

    move-result-object v0

    .line 2341637
    new-instance v7, LX/AAZ;

    invoke-direct {v7}, LX/AAZ;-><init>()V

    move-object v7, v7

    .line 2341638
    const-string v8, "coordinates"

    invoke-virtual {v7, v8, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/AAZ;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2341639
    iget-object v7, v1, LX/GDS;->a:LX/1Ck;

    sget-object v8, LX/GDR;->TASK_REVERSE_GEOCODE:LX/GDR;

    iget-object p0, v1, LX/GDS;->c:LX/0tX;

    invoke-virtual {p0, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance p0, LX/GDP;

    invoke-direct {p0, v1, v6}, LX/GDP;-><init>(LX/GDS;LX/GKs;)V

    invoke-virtual {v7, v8, v0, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2341640
    return-void

    .line 2341641
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GKv;
    .locals 9

    .prologue
    .line 2341627
    new-instance v0, LX/GKv;

    invoke-static {p0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v1

    check-cast v1, LX/3Lz;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p0}, LX/GDS;->b(LX/0QB;)LX/GDS;

    move-result-object v3

    check-cast v3, LX/GDS;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    .line 2341628
    new-instance v8, LX/GDN;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v7

    check-cast v7, LX/2U3;

    invoke-direct {v8, v5, v6, v7}, LX/GDN;-><init>(LX/0tX;LX/1Ck;LX/2U3;)V

    .line 2341629
    move-object v5, v8

    .line 2341630
    check-cast v5, LX/GDN;

    invoke-direct/range {v0 .. v5}, LX/GKv;-><init>(LX/3Lz;Landroid/view/inputmethod/InputMethodManager;LX/GDS;Landroid/content/res/Resources;LX/GDN;)V

    .line 2341631
    return-object v0
.end method

.method public static b(LX/GKv;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2341616
    iput-boolean p1, p0, LX/GKv;->k:Z

    .line 2341617
    iget-object v2, p0, LX/GKv;->j:LX/GCE;

    sget-object v3, LX/GG8;->PHONE_NUMBER:LX/GG8;

    iget-boolean v0, p0, LX/GKv;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/GKv;->o:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2341618
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2341619
    if-nez v0, :cond_2

    .line 2341620
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 2341621
    goto :goto_0

    .line 2341622
    :cond_2
    if-nez p1, :cond_3

    .line 2341623
    iget-object v0, p0, LX/GKv;->f:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2341624
    :cond_3
    iget-boolean v0, p0, LX/GKv;->k:Z

    if-eqz v0, :cond_4

    .line 2341625
    iget-object v0, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0}, LX/GMo;->b(Landroid/view/View;)V

    .line 2341626
    :cond_4
    iget-object v0, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz p1, :cond_5

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setVisibility(I)V

    goto :goto_1

    :cond_5
    const/16 v1, 0x8

    goto :goto_2
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2341588
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2341589
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v0, v1

    .line 2341590
    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    if-ne v0, v1, :cond_1

    .line 2341591
    :try_start_0
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2341592
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v0, v1

    .line 2341593
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2341594
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v0

    invoke-static {p0, v0}, LX/GKv;->a$redex0(LX/GKv;Landroid/location/Location;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2341595
    :cond_0
    :goto_0
    return-void

    .line 2341596
    :cond_1
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2341597
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2341598
    if-nez v0, :cond_5

    .line 2341599
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2341600
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v2, v1

    .line 2341601
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2341602
    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2341603
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2341604
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2341605
    iput-boolean v4, p0, LX/GKv;->o:Z

    .line 2341606
    :cond_2
    iget-object v1, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-boolean v0, p0, LX/GKv;->o:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2341607
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2341608
    :cond_4
    iget-object v0, p0, LX/GKv;->h:Landroid/content/res/Resources;

    const v2, 0x7f080b15

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_2

    .line 2341609
    :cond_5
    :try_start_1
    iget-object v0, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2341610
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2341611
    iget-object v1, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2341612
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2341613
    invoke-direct {p0, v0, v1}, LX/GKv;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2341614
    :catch_0
    goto :goto_0

    .line 2341615
    :catch_1
    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2341538
    iget-object v0, p0, LX/GKv;->j:LX/GCE;

    new-instance v1, LX/GKp;

    invoke-direct {v1, p0}, LX/GKp;-><init>(LX/GKv;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2341539
    iget-object v0, p0, LX/GKv;->j:LX/GCE;

    new-instance v1, LX/GKq;

    invoke-direct {v1, p0}, LX/GKq;-><init>(LX/GKv;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2341540
    iget-object v0, p0, LX/GKv;->j:LX/GCE;

    new-instance v1, LX/GKr;

    invoke-direct {v1, p0}, LX/GKr;-><init>(LX/GKv;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2341541
    return-void
.end method

.method private f()Z
    .locals 4

    .prologue
    .line 2341584
    iget-object v0, p0, LX/GKv;->j:LX/GCE;

    iget-object v1, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341585
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v2

    .line 2341586
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v1, v2

    .line 2341587
    iget-object v2, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v3, p0, LX/GKv;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0, v1, v2, v3}, LX/GJO;->a(LX/GCE;Lcom/facebook/graphql/enums/GraphQLCallToActionType;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v0

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2341580
    iget-object v0, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/GKv;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341581
    iget-object v0, p0, LX/GKv;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

    iget v1, p0, LX/GKv;->l:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->setCountryCode(Ljava/lang/CharSequence;)V

    .line 2341582
    iget-object v0, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, LX/GKu;

    invoke-direct {v1, p0}, LX/GKu;-><init>(LX/GKv;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2341583
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2341571
    invoke-super {p0}, LX/GHg;->a()V

    .line 2341572
    iget-object v0, p0, LX/GKv;->g:LX/GDS;

    invoke-virtual {v0}, LX/GDS;->a()V

    .line 2341573
    iget-object v0, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/GKv;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2341574
    iput-object v2, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2341575
    iput-object v2, p0, LX/GKv;->f:Landroid/view/inputmethod/InputMethodManager;

    .line 2341576
    iput-object v2, p0, LX/GKv;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

    .line 2341577
    iput-object v2, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2341578
    iput-object v2, p0, LX/GKv;->j:LX/GCE;

    .line 2341579
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2341568
    const-string v0, "phone_number_key"

    iget-object v1, p0, LX/GKv;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341569
    const-string v0, "country_code_key"

    iget v1, p0, LX/GKv;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2341570
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341567
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

    invoke-direct {p0, p1, p2}, LX/GKv;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2341566
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0, p1}, LX/GKv;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2341561
    invoke-direct {p0}, LX/GKv;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2341562
    invoke-direct {p0}, LX/GKv;->d()V

    .line 2341563
    iget-object v1, p0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-boolean v0, p0, LX/GKv;->o:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2341564
    :cond_0
    return-void

    .line 2341565
    :cond_1
    iget-object v0, p0, LX/GKv;->p:Landroid/text/Spanned;

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341555
    if-eqz p1, :cond_0

    .line 2341556
    const-string v0, "phone_number_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GKv;->n:Ljava/lang/String;

    .line 2341557
    const-string v0, "country_code_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/GKv;->l:I

    .line 2341558
    iget-object v0, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/GKv;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341559
    iget-object v0, p0, LX/GKv;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

    iget v1, p0, LX/GKv;->l:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->setCountryCode(Ljava/lang/CharSequence;)V

    .line 2341560
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2341551
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2341552
    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/GKv;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2341553
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/GKv;->a(Ljava/lang/String;)V

    .line 2341554
    return-void
.end method
