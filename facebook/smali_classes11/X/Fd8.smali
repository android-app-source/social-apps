.class public abstract LX/Fd8;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/FcX;

.field public c:LX/FcW;

.field private d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263179
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263180
    return-void
.end method


# virtual methods
.method public final a(LX/FcW;Z)V
    .locals 3

    .prologue
    .line 2263181
    invoke-virtual {p0}, LX/Fd8;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312a0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Fd8;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2263182
    iget-object v0, p0, LX/Fd8;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    .line 2263183
    iget-object v0, p0, LX/Fd8;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, LX/Fd8;->addView(Landroid/view/View;)V

    .line 2263184
    iput-object p1, p0, LX/Fd8;->c:LX/FcW;

    .line 2263185
    iget-object v0, p0, LX/Fd8;->d:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/FdD;

    invoke-direct {v1, p0}, LX/FdD;-><init>(LX/Fd8;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263186
    iget-object v1, p0, LX/Fd8;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, LX/Fd8;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p2, :cond_0

    const v0, 0x7f08208f

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263187
    return-void

    .line 2263188
    :cond_0
    const v0, 0x7f08208e

    goto :goto_0
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public setItems(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2263189
    iput-object p1, p0, LX/Fd8;->a:LX/0Px;

    .line 2263190
    iget-object v1, p0, LX/Fd8;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/Fd8;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2263191
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2263192
    new-instance v4, LX/FdC;

    invoke-direct {v4, p0}, LX/FdC;-><init>(LX/Fd8;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263193
    invoke-virtual {p0, v0}, LX/Fd8;->a(Landroid/view/View;)V

    .line 2263194
    add-int/lit8 v2, v2, 0x1

    .line 2263195
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2263196
    :cond_0
    return-void
.end method

.method public setOnItemSelectedListener(LX/FcX;)V
    .locals 0

    .prologue
    .line 2263197
    iput-object p1, p0, LX/Fd8;->b:LX/FcX;

    .line 2263198
    return-void
.end method
