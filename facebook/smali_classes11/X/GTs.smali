.class public final enum LX/GTs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GTs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GTs;

.field public static final enum HELP_CENTER:LX/GTs;

.field public static final enum LEARN_MORE:LX/GTs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2356172
    new-instance v0, LX/GTs;

    const-string v1, "LEARN_MORE"

    invoke-direct {v0, v1, v2}, LX/GTs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GTs;->LEARN_MORE:LX/GTs;

    .line 2356173
    new-instance v0, LX/GTs;

    const-string v1, "HELP_CENTER"

    invoke-direct {v0, v1, v3}, LX/GTs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GTs;->HELP_CENTER:LX/GTs;

    .line 2356174
    const/4 v0, 0x2

    new-array v0, v0, [LX/GTs;

    sget-object v1, LX/GTs;->LEARN_MORE:LX/GTs;

    aput-object v1, v0, v2

    sget-object v1, LX/GTs;->HELP_CENTER:LX/GTs;

    aput-object v1, v0, v3

    sput-object v0, LX/GTs;->$VALUES:[LX/GTs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2356175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GTs;
    .locals 1

    .prologue
    .line 2356176
    const-class v0, LX/GTs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GTs;

    return-object v0
.end method

.method public static values()[LX/GTs;
    .locals 1

    .prologue
    .line 2356177
    sget-object v0, LX/GTs;->$VALUES:[LX/GTs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GTs;

    return-object v0
.end method
