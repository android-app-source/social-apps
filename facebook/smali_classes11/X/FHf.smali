.class public final enum LX/FHf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHf;

.field public static final enum PRE_UPLOAD:LX/FHf;

.field public static final enum UPLOAD:LX/FHf;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2220863
    new-instance v0, LX/FHf;

    const-string v1, "UPLOAD"

    invoke-direct {v0, v1, v2}, LX/FHf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHf;->UPLOAD:LX/FHf;

    new-instance v0, LX/FHf;

    const-string v1, "PRE_UPLOAD"

    invoke-direct {v0, v1, v3}, LX/FHf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHf;->PRE_UPLOAD:LX/FHf;

    const/4 v0, 0x2

    new-array v0, v0, [LX/FHf;

    sget-object v1, LX/FHf;->UPLOAD:LX/FHf;

    aput-object v1, v0, v2

    sget-object v1, LX/FHf;->PRE_UPLOAD:LX/FHf;

    aput-object v1, v0, v3

    sput-object v0, LX/FHf;->$VALUES:[LX/FHf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220864
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHf;
    .locals 1

    .prologue
    .line 2220862
    const-class v0, LX/FHf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHf;

    return-object v0
.end method

.method public static values()[LX/FHf;
    .locals 1

    .prologue
    .line 2220861
    sget-object v0, LX/FHf;->$VALUES:[LX/FHf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHf;

    return-object v0
.end method
