.class public LX/GNE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/GND;

.field private final b:D

.field public final c:Landroid/content/res/Resources;

.field public final d:Z

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/util/Locale;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345561
    iput-object p1, p0, LX/GNE;->c:Landroid/content/res/Resources;

    .line 2345562
    invoke-static {p2}, LX/482;->from(Ljava/util/Locale;)LX/482;

    move-result-object v0

    sget-object v1, LX/482;->IMPERIAL:LX/482;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/GNE;->d:Z

    .line 2345563
    iget-boolean v0, p0, LX/GNE;->d:Z

    if-eqz v0, :cond_1

    const-wide v0, 0x409925604189374cL    # 1609.344

    :goto_1
    iput-wide v0, p0, LX/GNE;->b:D

    .line 2345564
    new-instance v2, LX/GND;

    iget-wide v4, p0, LX/GNE;->b:D

    iget-boolean v0, p0, LX/GNE;->d:Z

    if-eqz v0, :cond_2

    const-wide v0, 0x40f3a53333333333L    # 80467.2

    :goto_2
    invoke-direct {v2, v4, v5, v0, v1}, LX/GND;-><init>(DD)V

    iput-object v2, p0, LX/GNE;->a:LX/GND;

    .line 2345565
    return-void

    .line 2345566
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2345567
    :cond_1
    const-wide v0, 0x408f400000000000L    # 1000.0

    goto :goto_1

    .line 2345568
    :cond_2
    const-wide v0, 0x40f3880000000000L    # 80000.0

    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/GNE;
    .locals 5

    .prologue
    .line 2345569
    const-class v1, LX/GNE;

    monitor-enter v1

    .line 2345570
    :try_start_0
    sget-object v0, LX/GNE;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2345571
    sput-object v2, LX/GNE;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2345572
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2345573
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2345574
    new-instance p0, LX/GNE;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v4

    check-cast v4, Ljava/util/Locale;

    invoke-direct {p0, v3, v4}, LX/GNE;-><init>(Landroid/content/res/Resources;Ljava/util/Locale;)V

    .line 2345575
    move-object v0, p0

    .line 2345576
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2345577
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GNE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2345578
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2345579
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/31h;Landroid/view/View;)D
    .locals 4

    .prologue
    .line 2345580
    invoke-static {p1, p2}, LX/GND;->a(LX/31h;Landroid/view/View;)D

    move-result-wide v0

    iget-wide v2, p0, LX/GNE;->b:D

    div-double/2addr v0, v2

    .line 2345581
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/680;Lcom/facebook/android/maps/model/LatLng;DI)F
    .locals 3

    .prologue
    .line 2345582
    iget-wide v0, p0, LX/GNE;->b:D

    mul-double/2addr v0, p3

    invoke-static {p1, p2, v0, v1, p5}, LX/GND;->a(LX/680;Lcom/facebook/android/maps/model/LatLng;DI)F

    move-result v0

    return v0
.end method
