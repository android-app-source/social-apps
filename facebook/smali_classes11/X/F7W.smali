.class public final LX/F7W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ii;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 0

    .prologue
    .line 2201822
    iput-object p1, p0, LX/F7W;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 11

    .prologue
    .line 2201823
    iget-object v0, p0, LX/F7W;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    const/4 v5, 0x0

    .line 2201824
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v1, p1}, LX/F73;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 2201825
    instance-of v2, v1, LX/F7L;

    if-nez v2, :cond_0

    .line 2201826
    :goto_0
    return-void

    :cond_0
    move-object v4, v1

    .line 2201827
    check-cast v4, LX/F7L;

    .line 2201828
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {v4}, LX/F7L;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2201829
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2201830
    const-string v2, "timeline_friend_request_ref"

    sget-object v3, LX/5P2;->CONTACT_IMPORTER:LX/5P2;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2201831
    invoke-virtual {v4}, LX/F7L;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, LX/F7L;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/F7L;->b()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-static/range {v1 .. v6}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 2201832
    iget-object v2, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->g:LX/17W;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v7, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_0
.end method
