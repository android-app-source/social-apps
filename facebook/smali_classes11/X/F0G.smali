.class public final LX/F0G;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F0J;


# direct methods
.method public constructor <init>(LX/F0J;)V
    .locals 0

    .prologue
    .line 2188372
    iput-object p1, p0, LX/F0G;->a:LX/F0J;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2188373
    iget-object v0, p0, LX/F0G;->a:LX/F0J;

    .line 2188374
    iget-object v1, v0, LX/F0J;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/F0H;

    .line 2188375
    invoke-interface {v1}, LX/F0H;->b()V

    goto :goto_0

    .line 2188376
    :cond_0
    iget-object v1, v0, LX/F0J;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2188377
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2188378
    iget-object v0, p0, LX/F0G;->a:LX/F0J;

    .line 2188379
    iget-object p0, v0, LX/F0J;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/F0H;

    .line 2188380
    invoke-interface {p0}, LX/F0H;->a()V

    goto :goto_0

    .line 2188381
    :cond_0
    iget-object p0, v0, LX/F0J;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 2188382
    return-void
.end method
