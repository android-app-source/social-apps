.class public final synthetic LX/FPV;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2237301
    invoke-static {}, LX/FPW;->values()[LX/FPW;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/FPV;->c:[I

    :try_start_0
    sget-object v0, LX/FPV;->c:[I

    sget-object v1, LX/FPW;->LOADING_INITIAL_RESULTS:LX/FPW;

    invoke-virtual {v1}, LX/FPW;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_0
    :try_start_1
    sget-object v0, LX/FPV;->c:[I

    sget-object v1, LX/FPW;->LOADING_PAGINATION:LX/FPW;

    invoke-virtual {v1}, LX/FPW;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_1
    :try_start_2
    sget-object v0, LX/FPV;->c:[I

    sget-object v1, LX/FPW;->DISPLAYING_RESULTS:LX/FPW;

    invoke-virtual {v1}, LX/FPW;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_2
    :try_start_3
    sget-object v0, LX/FPV;->c:[I

    sget-object v1, LX/FPW;->COUNT:LX/FPW;

    invoke-virtual {v1}, LX/FPW;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    .line 2237302
    :goto_3
    invoke-static {}, LX/FP6;->values()[LX/FP6;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/FPV;->b:[I

    :try_start_4
    sget-object v0, LX/FPV;->b:[I

    sget-object v1, LX/FP6;->PAGINATION_REQUEST:LX/FP6;

    invoke-virtual {v1}, LX/FP6;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_4
    :try_start_5
    sget-object v0, LX/FPV;->b:[I

    sget-object v1, LX/FP6;->RESULT_LIST_REQUEST:LX/FP6;

    invoke-virtual {v1}, LX/FP6;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_5
    :try_start_6
    sget-object v0, LX/FPV;->b:[I

    sget-object v1, LX/FP6;->MAP_REGION_REQUEST:LX/FP6;

    invoke-virtual {v1}, LX/FP6;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_6
    :try_start_7
    sget-object v0, LX/FPV;->b:[I

    sget-object v1, LX/FP6;->FILTER_REQUEST:LX/FP6;

    invoke-virtual {v1}, LX/FP6;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    .line 2237303
    :goto_7
    invoke-static {}, LX/FQN;->values()[LX/FQN;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/FPV;->a:[I

    :try_start_8
    sget-object v0, LX/FPV;->a:[I

    sget-object v1, LX/FQN;->DISTANCE:LX/FQN;

    invoke-virtual {v1}, LX/FQN;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_8
    :try_start_9
    sget-object v0, LX/FPV;->a:[I

    sget-object v1, LX/FQN;->LIKES:LX/FQN;

    invoke-virtual {v1}, LX/FQN;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_9
    :try_start_a
    sget-object v0, LX/FPV;->a:[I

    sget-object v1, LX/FQN;->REVIEW:LX/FQN;

    invoke-virtual {v1}, LX/FQN;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_a
    :try_start_b
    sget-object v0, LX/FPV;->a:[I

    sget-object v1, LX/FQN;->OPEN_NOW:LX/FQN;

    invoke-virtual {v1}, LX/FQN;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_b
    :try_start_c
    sget-object v0, LX/FPV;->a:[I

    sget-object v1, LX/FQN;->PRICE:LX/FQN;

    invoke-virtual {v1}, LX/FQN;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_c
    return-void

    :catch_0
    goto :goto_c

    :catch_1
    goto :goto_b

    :catch_2
    goto :goto_a

    :catch_3
    goto :goto_9

    :catch_4
    goto :goto_8

    :catch_5
    goto :goto_7

    :catch_6
    goto :goto_6

    :catch_7
    goto :goto_5

    :catch_8
    goto :goto_4

    :catch_9
    goto :goto_3

    :catch_a
    goto/16 :goto_2

    :catch_b
    goto/16 :goto_1

    :catch_c
    goto/16 :goto_0
.end method
