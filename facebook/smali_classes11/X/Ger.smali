.class public LX/Ger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Pj;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/1Ad;

.field private final b:LX/5KP;

.field private final c:LX/0dk;

.field private final d:LX/0Zb;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5KP;LX/0dk;LX/1Ad;LX/0Zb;LX/0Ot;)V
    .locals 0
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5KP;",
            "LX/0dk;",
            "LX/1Ad;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2376117
    iput-object p1, p0, LX/Ger;->b:LX/5KP;

    .line 2376118
    iput-object p2, p0, LX/Ger;->c:LX/0dk;

    .line 2376119
    iput-object p3, p0, LX/Ger;->a:LX/1Ad;

    .line 2376120
    iput-object p4, p0, LX/Ger;->d:LX/0Zb;

    .line 2376121
    iput-object p5, p0, LX/Ger;->e:LX/0Ot;

    .line 2376122
    return-void
.end method

.method public static a(LX/0QB;)LX/Ger;
    .locals 9

    .prologue
    .line 2376123
    const-class v1, LX/Ger;

    monitor-enter v1

    .line 2376124
    :try_start_0
    sget-object v0, LX/Ger;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376125
    sput-object v2, LX/Ger;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376126
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376127
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376128
    new-instance v3, LX/Ger;

    const-class v4, LX/5KP;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/5KP;

    invoke-static {v0}, LX/0dk;->a(LX/0QB;)LX/0dk;

    move-result-object v5

    check-cast v5, LX/0dk;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    const/16 v8, 0x1430

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/Ger;-><init>(LX/5KP;LX/0dk;LX/1Ad;LX/0Zb;LX/0Ot;)V

    .line 2376129
    move-object v0, v3

    .line 2376130
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376131
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376132
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376133
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1f9;LX/2dx;LX/1Pk;)LX/1Dg;
    .locals 14
    .param p2    # Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1f9;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/2dx;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1Pk;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;I",
            "LX/1f9;",
            "LX/2dx;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2376134
    const-string v2, "CSPYML.onCreateLayout"

    const v3, 0x4bb732c0    # 2.401216E7f

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2376135
    :try_start_0
    invoke-virtual {p1}, LX/1De;->c()LX/1cp;

    move-result-object v2

    .line 2376136
    if-eqz v2, :cond_0

    .line 2376137
    iget-object v3, p0, LX/Ger;->c:LX/0dk;

    invoke-virtual {v3}, LX/0dk;->b()Ljava/lang/String;

    move-result-object v3

    .line 2376138
    const/4 v4, 0x0

    const-string v5, "cs_startup_type"

    invoke-virtual {v2, v4, p1, v5, v3}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2376139
    :cond_0
    iget-object v2, p0, LX/Ger;->c:LX/0dk;

    invoke-virtual {v2}, LX/0dk;->a()Lcom/facebook/java2js/JSContext;

    move-result-object v3

    .line 2376140
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 2376141
    move-object/from16 v0, p7

    check-cast v0, LX/1Pj;

    move-object v2, v0

    invoke-interface {v2}, LX/1Pj;->getNavigator()LX/5KM;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2376142
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Environment of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has no navigator"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2376143
    :catch_0
    move-exception v2

    .line 2376144
    :try_start_1
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "component_script_error"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2376145
    const-string v4, "message"

    invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2376146
    iget-object v2, p0, LX/Ger;->d:LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376147
    const v2, 0x1a13d486

    invoke-static {v2}, LX/02m;->a(I)V

    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 2376148
    :cond_1
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v5

    .line 2376149
    new-instance v2, LX/5KK;

    invoke-direct {v2, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v6, "isHScroll"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v6, "imageDownloader"

    iget-object v7, p0, LX/Ger;->a:LX/1Ad;

    invoke-virtual {v2, v6, v7}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v6, "prefetchInfo"

    move-object/from16 v0, p5

    invoke-virtual {v2, v6, v0}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v6, "toolbox"

    move-object/from16 v0, p7

    invoke-virtual {v2, v6, v0}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v6

    const-string v7, "trackingCodes"

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, LX/16h;

    move-object/from16 v0, p2

    invoke-static {v0, v2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v6, "onPageLikeAction"

    new-instance v7, LX/Geq;

    move-object/from16 v0, p6

    invoke-direct {v7, p0, v0}, LX/Geq;-><init>(LX/Ger;LX/2dx;)V

    invoke-static {v3, v7}, Lcom/facebook/java2js/JSValue;->makeFunction(Lcom/facebook/java2js/JSContext;Lcom/facebook/java2js/Invokable;)Lcom/facebook/java2js/JSValue;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v6, "insets"

    new-instance v7, LX/5KK;

    invoke-direct {v7, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v8, "left"

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0917

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v7

    const-string v8, "right"

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0917

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v7

    const-string v8, "top"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v7

    const-string v8, "bottom"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v7

    invoke-virtual {v7}, LX/5KK;->a()LX/0P1;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v6

    const-string v7, "unit"

    new-instance v2, LX/5KK;

    invoke-direct {v2, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v8, "__native_object__"

    move-object/from16 v0, p3

    invoke-virtual {v2, v8, v0}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v8

    const-string v9, "mobileTitle"

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v2}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v8, v9, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    invoke-virtual {v2}, LX/5KK;->a()LX/0P1;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v6

    const-string v7, "item"

    new-instance v2, LX/5KK;

    invoke-direct {v2, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v8, "__native_object__"

    move-object/from16 v0, p2

    invoke-virtual {v2, v8, v0}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v8, "creative_text"

    invoke-static/range {p2 .. p2}, LX/Gdi;->a(LX/25E;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v8

    const-string v9, "is_sponsored"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v8, v9, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v8

    const-string v9, "social_context"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, ""

    :goto_2
    invoke-virtual {v8, v9, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v8

    const-string v9, "sponsored_data"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-nez v2, :cond_4

    invoke-static {v3}, Lcom/facebook/java2js/JSValue;->makeNull(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    :goto_3
    invoke-virtual {v8, v9, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v8, "creativeImageHigh"

    new-instance v9, LX/5KK;

    invoke-direct {v9, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v10, "uri"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    const-string v10, "width"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    invoke-virtual {v9}, LX/5KK;->a()LX/0P1;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v8, "creativeImageMedium"

    new-instance v9, LX/5KK;

    invoke-direct {v9, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v10, "uri"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    const-string v10, "width"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    invoke-virtual {v9}, LX/5KK;->a()LX/0P1;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v8, "creativeImageLow"

    new-instance v9, LX/5KK;

    invoke-direct {v9, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v10, "uri"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    const-string v10, "width"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    invoke-virtual {v9}, LX/5KK;->a()LX/0P1;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v8

    const-string v9, "creative_image_mobile_feed_focus"

    new-instance v10, LX/5KK;

    invoke-direct {v10, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v11, "x"

    if-nez v5, :cond_6

    const/high16 v2, 0x3f000000    # 0.5f

    :goto_4
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string v11, "y"

    if-nez v5, :cond_7

    const/high16 v2, 0x3f000000    # 0.5f

    :goto_5
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    invoke-virtual {v2}, LX/5KK;->a()LX/0P1;

    move-result-object v2

    invoke-virtual {v8, v9, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v5

    const-string v8, "profile"

    new-instance v2, LX/5KK;

    invoke-direct {v2, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v9, "__native_object__"

    invoke-virtual {v2, v9, v4}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v9, "id"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v9, "name"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    const-string v10, "page_likers"

    new-instance v11, LX/5KK;

    invoke-direct {v11, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v12, "count"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->V()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v2

    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v12, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    invoke-virtual {v2}, LX/5KK;->a()LX/0P1;

    move-result-object v2

    invoke-virtual {v9, v10, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    const-string v9, "category_names"

    new-instance v10, LX/5KK;

    invoke-direct {v10, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static/range {p2 .. p2}, LX/Gdi;->b(LX/25E;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    invoke-virtual {v10}, LX/5KK;->a()LX/0P1;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v9

    const-string v10, "profilePicUri"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    :goto_7
    invoke-virtual {v9, v10, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    invoke-virtual {v2}, LX/5KK;->a()LX/0P1;

    move-result-object v2

    invoke-virtual {v5, v8, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    invoke-virtual {v2}, LX/5KK;->a()LX/0P1;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    invoke-virtual {v2}, LX/5KK;->a()LX/0P1;

    move-result-object v2

    .line 2376150
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    move/from16 v0, p4

    invoke-interface {v4, v0}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/5KZ;->c(LX/1De;)LX/5KX;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/5KX;->a(Lcom/facebook/java2js/JSContext;)LX/5KX;

    move-result-object v5

    iget-object v6, p0, LX/Ger;->b:LX/5KP;

    invoke-virtual {v6, p1, v3}, LX/5KP;->a(LX/1De;Lcom/facebook/java2js/JSContext;)LX/5KO;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/5KX;->a(Ljava/lang/Object;)LX/5KX;

    move-result-object v3

    const-string v5, "CSPYMLWithLargeImageFeedUnitPage"

    invoke-virtual {v3, v5}, LX/5KX;->b(Ljava/lang/String;)LX/5KX;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/5KX;->b(Ljava/lang/Object;)LX/5KX;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 2376151
    const v3, -0x2793fa87

    invoke-static {v3}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 2376152
    :cond_2
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v2

    goto/16 :goto_1

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto/16 :goto_2

    :cond_4
    new-instance v10, LX/5KK;

    invoke-direct {v10, v3}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v11, "is_demo_ad"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-nez v2, :cond_5

    const/4 v2, 0x0

    :goto_8
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v2

    invoke-virtual {v2}, LX/5KK;->a()LX/0P1;

    move-result-object v2

    goto/16 :goto_3

    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->c()Z

    move-result v2

    goto :goto_8

    :cond_6
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v12

    double-to-float v2, v12

    goto/16 :goto_4

    :cond_7
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v12

    double-to-float v2, v12

    goto/16 :goto_5

    :cond_8
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->V()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;->a()I

    move-result v2

    goto/16 :goto_6

    :cond_9
    const-string v2, ""
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_7

    .line 2376153
    :catchall_0
    move-exception v2

    const v3, 0x12fb62c1

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
.end method
