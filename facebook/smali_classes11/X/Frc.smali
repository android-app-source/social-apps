.class public final LX/Frc;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Frd;


# direct methods
.method public constructor <init>(LX/Frd;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2295854
    iput-object p1, p0, LX/Frc;->c:LX/Frd;

    iput-object p2, p0, LX/Frc;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Frc;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2295855
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2295856
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v0, v1, :cond_0

    .line 2295857
    iget-object v0, p0, LX/Frc;->c:LX/Frd;

    iget-object v0, v0, LX/Frd;->q:LX/03V;

    const-string v1, "timeline_delete_story_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2295858
    :cond_0
    iget-object v0, p0, LX/Frc;->c:LX/Frd;

    iget-object v0, v0, LX/Frd;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081057

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2295859
    iget-object v0, p0, LX/Frc;->c:LX/Frd;

    iget-object v0, v0, LX/Frd;->f:LX/G4x;

    iget-object v1, p0, LX/Frc;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Frc;->b:Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;)V

    .line 2295860
    iget-object v0, p0, LX/Frc;->c:LX/Frd;

    invoke-virtual {v0}, LX/Frd;->f()V

    .line 2295861
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2295862
    iget-object v0, p0, LX/Frc;->c:LX/Frd;

    invoke-virtual {v0}, LX/Frd;->f()V

    .line 2295863
    iget-object v0, p0, LX/Frc;->c:LX/Frd;

    iget-object v0, v0, LX/Frd;->b:LX/FqO;

    if-eqz v0, :cond_0

    .line 2295864
    iget-object v0, p0, LX/Frc;->c:LX/Frd;

    iget-object v0, v0, LX/Frd;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->G()V

    .line 2295865
    :cond_0
    return-void
.end method
