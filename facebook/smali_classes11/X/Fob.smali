.class public LX/Fob;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final c:LX/Foa;

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Foa;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/FoT;

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2289877
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2289878
    new-instance v0, LX/Foa;

    sget-object v1, LX/FoZ;->LOADING:LX/FoZ;

    invoke-direct {v0, v1}, LX/Foa;-><init>(LX/FoZ;)V

    iput-object v0, p0, LX/Fob;->c:LX/Foa;

    .line 2289879
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Fob;->d:Ljava/util/ArrayList;

    .line 2289880
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fob;->f:Z

    .line 2289881
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fob;->g:Z

    .line 2289882
    iput-object p1, p0, LX/Fob;->a:Landroid/content/Context;

    .line 2289883
    return-void
.end method

.method public static a(LX/Fob;LX/FoZ;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2289868
    sget-object v1, LX/FoY;->a:[I

    invoke-virtual {p1}, LX/FoZ;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2289869
    :pswitch_0
    invoke-virtual {p0}, LX/Fob;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    :goto_0
    :pswitch_1
    return v0

    .line 2289870
    :pswitch_2
    invoke-virtual {p0, v0}, LX/Fob;->a(I)LX/Foa;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, LX/Fob;->a(I)LX/Foa;

    move-result-object v1

    .line 2289871
    iget-object v2, v1, LX/Foa;->a:LX/FoZ;

    move-object v1, v2

    .line 2289872
    sget-object v2, LX/FoZ;->SUBTITLE:LX/FoZ;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 2289873
    :pswitch_3
    sget-object v0, LX/FoZ;->DAF_DISCLOSURE:LX/FoZ;

    invoke-static {p0, v0}, LX/Fob;->a(LX/Fob;LX/FoZ;)I

    move-result v0

    .line 2289874
    invoke-virtual {p0, v0}, LX/Fob;->a(I)LX/Foa;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, LX/Fob;->a(I)LX/Foa;

    move-result-object v1

    .line 2289875
    iget-object v2, v1, LX/Foa;->a:LX/FoZ;

    move-object v1, v2

    .line 2289876
    sget-object v2, LX/FoZ;->DAF_DISCLOSURE:LX/FoZ;

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/widget/LinearLayout;LX/Foa;)Landroid/widget/LinearLayout;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2289844
    const v0, 0x7f0d137f

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2289845
    iget-object v1, p2, LX/Foa;->e:Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;

    move-object v6, v1

    .line 2289846
    invoke-virtual {v6}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;

    move-result-object v2

    .line 2289847
    invoke-static {v2}, LX/Fob;->a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;)Ljava/lang/String;

    move-result-object v1

    .line 2289848
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2289849
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;->a()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v3, v1, v4}, LX/15i;->g(II)I

    move-result v1

    .line 2289850
    const-class v5, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v3, v1, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2289851
    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2289852
    :goto_0
    const v0, 0x7f0d13b2

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2289853
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2289854
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->m()Ljava/lang/String;

    move-result-object v3

    .line 2289855
    :goto_1
    const-string v5, ""

    .line 2289856
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2289857
    invoke-virtual {v6}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 2289858
    :goto_2
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->p()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/Fob;->a(LX/Fob;Lcom/facebook/fig/listitem/FigListItem;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2289859
    new-instance v0, LX/FoX;

    invoke-direct {v0, p0, v1}, LX/FoX;-><init>(LX/Fob;Lcom/facebook/fig/listitem/FigListItem;)V

    invoke-virtual {v1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2289860
    return-object p1

    .line 2289861
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 2289862
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;->j()Z

    move-result v3

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;->k()Z

    move-result v5

    iget-object v7, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-static {v0, v3, v5, v7}, LX/FkG;->a(Ljava/lang/CharSequence;ZZLandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    .line 2289863
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2289864
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->k()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2289865
    invoke-virtual {v6, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_5

    .line 2289866
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->k()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v5, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_3
    move v0, v4

    .line 2289867
    goto :goto_3

    :cond_4
    move v0, v4

    goto :goto_3

    :cond_5
    move-object v4, v5

    goto :goto_2
.end method

.method private static a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2289745
    if-eqz p0, :cond_4

    .line 2289746
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;

    move-result-object v0

    .line 2289747
    if-eqz v0, :cond_4

    .line 2289748
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2289749
    if-eqz v4, :cond_1

    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2289750
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2289751
    const-class v5, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v3, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    if-eqz v1, :cond_4

    .line 2289752
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v3, v0, v2, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2289753
    :goto_2
    return-object v0

    .line 2289754
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move v0, v2

    .line 2289755
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1

    .line 2289756
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(LX/Fob;Lcom/facebook/fig/listitem/FigListItem;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 2289831
    invoke-virtual {p1, p3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2289832
    invoke-virtual {p1, p4}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2289833
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2289834
    iget-boolean v0, p0, LX/Fob;->g:Z

    if-eqz v0, :cond_0

    .line 2289835
    invoke-virtual {p1, p5}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2289836
    :cond_0
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2289837
    invoke-static {p6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2289838
    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2289839
    :goto_0
    iget-object v0, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083330

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2289840
    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2289841
    invoke-virtual {p1, p2}, Lcom/facebook/fig/listitem/FigListItem;->setTag(Ljava/lang/Object;)V

    .line 2289842
    return-void

    .line 2289843
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/Foa;
    .locals 1

    .prologue
    .line 2289828
    iget-object v0, p0, LX/Fob;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2289829
    iget-object v0, p0, LX/Fob;->c:LX/Foa;

    .line 2289830
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Fob;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Foa;

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2289827
    iget-object v0, p0, LX/Fob;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-boolean v0, p0, LX/Fob;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2289826
    invoke-virtual {p0, p1}, LX/Fob;->a(I)LX/Foa;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2289825
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2289757
    invoke-virtual {p0, p1}, LX/Fob;->a(I)LX/Foa;

    move-result-object v1

    .line 2289758
    sget-object v0, LX/FoY;->a:[I

    .line 2289759
    iget-object v2, v1, LX/Foa;->a:LX/FoZ;

    move-object v2, v2

    .line 2289760
    invoke-virtual {v2}, LX/FoZ;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2289761
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized row type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2289762
    iget-object v3, v1, LX/Foa;->a:LX/FoZ;

    move-object v1, v3

    .line 2289763
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2289764
    :pswitch_0
    iget-object v0, p0, LX/Fob;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030753

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2289765
    check-cast v0, Landroid/widget/TextView;

    .line 2289766
    iget-object v2, v1, LX/Foa;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2289767
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2289768
    move-object v0, v0

    .line 2289769
    :goto_0
    return-object v0

    .line 2289770
    :pswitch_1
    new-instance v0, Lcom/facebook/fig/footer/FigFooter;

    iget-object v1, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/footer/FigFooter;-><init>(Landroid/content/Context;)V

    .line 2289771
    invoke-virtual {v0, v4}, Lcom/facebook/fig/footer/FigFooter;->setFooterType(I)V

    .line 2289772
    invoke-virtual {v0, v4}, Lcom/facebook/fig/footer/FigFooter;->setTopDivider(Z)V

    goto :goto_0

    .line 2289773
    :pswitch_2
    iget-object v0, p0, LX/Fob;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030742

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2289774
    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 p2, 0x8

    const/4 p1, 0x0

    .line 2289775
    iget-object v2, v1, LX/Foa;->d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    move-object v2, v2

    .line 2289776
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2289777
    :cond_0
    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2289778
    :goto_1
    move-object v0, v0

    .line 2289779
    goto :goto_0

    .line 2289780
    :pswitch_3
    if-eqz p2, :cond_1

    instance-of v0, p2, Lcom/facebook/fig/listitem/FigListItem;

    if-nez v0, :cond_7

    .line 2289781
    :cond_1
    iget-object v0, p0, LX/Fob;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030761

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2289782
    :goto_2
    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v6, 0x0

    .line 2289783
    iget-object v5, v1, LX/Foa;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    move-object v7, v5

    .line 2289784
    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v5

    if-nez v5, :cond_a

    .line 2289785
    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v8

    .line 2289786
    :goto_3
    const-string v9, ""

    .line 2289787
    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_c

    .line 2289788
    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v5

    iget-object v10, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2289789
    invoke-virtual {v10, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_2

    .line 2289790
    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v5

    iget-object v9, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v9, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    .line 2289791
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->p()Ljava/lang/String;

    move-result-object v11

    move-object v5, p0

    move-object v6, v0

    invoke-static/range {v5 .. v11}, LX/Fob;->a(LX/Fob;Lcom/facebook/fig/listitem/FigListItem;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2289792
    new-instance v5, LX/FoW;

    invoke-direct {v5, p0, v0}, LX/FoW;-><init>(LX/Fob;Lcom/facebook/fig/listitem/FigListItem;)V

    invoke-virtual {v0, v5}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2289793
    move-object v0, v0

    .line 2289794
    goto/16 :goto_0

    .line 2289795
    :pswitch_4
    iget-object v0, p0, LX/Fob;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030751

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2289796
    check-cast v0, Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v1}, LX/Fob;->a(Landroid/widget/LinearLayout;LX/Foa;)Landroid/widget/LinearLayout;

    move-result-object v0

    goto/16 :goto_0

    .line 2289797
    :pswitch_5
    if-eqz p2, :cond_3

    instance-of v0, p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-nez v0, :cond_6

    .line 2289798
    :cond_3
    new-instance v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    .line 2289799
    :goto_5
    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2289800
    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2289801
    move-object v0, v0

    .line 2289802
    goto/16 :goto_0

    .line 2289803
    :pswitch_6
    if-eqz p2, :cond_4

    instance-of v0, p2, Landroid/widget/Space;

    if-nez v0, :cond_5

    .line 2289804
    :cond_4
    new-instance v0, Landroid/widget/Space;

    iget-object v1, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    .line 2289805
    :goto_6
    check-cast v0, Landroid/widget/Space;

    .line 2289806
    invoke-virtual {v0}, Landroid/widget/Space;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/AbsListView$LayoutParams;

    .line 2289807
    if-nez v1, :cond_d

    .line 2289808
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 2289809
    :goto_7
    invoke-virtual {v0, v1}, Landroid/widget/Space;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2289810
    move-object v0, v0

    .line 2289811
    goto/16 :goto_0

    .line 2289812
    :pswitch_7
    new-instance v0, Lcom/facebook/fig/header/FigHeader;

    iget-object v1, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/header/FigHeader;-><init>(Landroid/content/Context;)V

    .line 2289813
    const v1, 0x7f083333

    invoke-virtual {v0, v1}, Lcom/facebook/fig/header/FigHeader;->setTitleText(I)V

    goto/16 :goto_0

    :cond_5
    move-object v0, p2

    goto :goto_6

    :cond_6
    move-object v0, p2

    goto :goto_5

    :cond_7
    move-object v0, p2

    goto/16 :goto_2

    .line 2289814
    :cond_8
    invoke-virtual {p0, p1}, LX/Fob;->a(I)LX/Foa;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-virtual {p0, p1}, LX/Fob;->a(I)LX/Foa;

    move-result-object v3

    .line 2289815
    iget-object v4, v3, LX/Foa;->a:LX/FoZ;

    move-object v3, v4

    .line 2289816
    sget-object v4, LX/FoZ;->SUBTITLE:LX/FoZ;

    if-ne v3, v4, :cond_9

    .line 2289817
    invoke-virtual {v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b21f3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2289818
    invoke-virtual {v0, v3, p1, v3, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setPadding(IIII)V

    .line 2289819
    :cond_9
    :try_start_0
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V

    .line 2289820
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 2289821
    :catch_0
    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2289822
    :cond_a
    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->a()Z

    move-result v8

    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->j()Z

    move-result v9

    iget-object v10, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-static {v5, v8, v9, v10}, LX/FkG;->a(Ljava/lang/CharSequence;ZZLandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v8

    goto/16 :goto_3

    :cond_b
    move v5, v6

    .line 2289823
    goto/16 :goto_4

    :cond_c
    move v5, v6

    goto/16 :goto_4

    .line 2289824
    :cond_d
    iget-object v2, p0, LX/Fob;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/AbsListView$LayoutParams;->height:I

    goto/16 :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
