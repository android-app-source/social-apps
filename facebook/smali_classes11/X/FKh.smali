.class public LX/FKh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/attachments/OtherAttachmentData;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225437
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2225438
    check-cast p1, Lcom/facebook/messaging/attachments/OtherAttachmentData;

    .line 2225439
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2225440
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225441
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "q"

    .line 2225442
    new-instance v2, LX/3fG;

    invoke-direct {v2}, LX/3fG;-><init>()V

    .line 2225443
    const-string v3, "url"

    const-string v4, "SELECT src FROM message_previewable_attachment_src WHERE message_id=\'%1$s\' and  attachment_id=\'%2$s\'"

    iget-object v6, p1, Lcom/facebook/messaging/attachments/OtherAttachmentData;->e:Ljava/lang/String;

    invoke-static {v6}, LX/DoA;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object p0, p1, Lcom/facebook/messaging/attachments/OtherAttachmentData;->f:Ljava/lang/String;

    invoke-static {v4, v6, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/3fG;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225444
    invoke-virtual {v2}, LX/3fG;->a()LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2225445
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225446
    new-instance v0, LX/14N;

    const-string v1, "fetchThreadList"

    const-string v2, "GET"

    const-string v3, "fql"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2225447
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225448
    new-instance v1, LX/3fM;

    invoke-direct {v1, v0}, LX/3fM;-><init>(LX/0lF;)V

    .line 2225449
    const-string v0, "url"

    invoke-virtual {v1, v0}, LX/3fM;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "src"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2225450
    if-nez v0, :cond_0

    .line 2225451
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no src field returned in fql response"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2225452
    :cond_0
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2225453
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2225454
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no value returned for src field in fql response"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2225455
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
