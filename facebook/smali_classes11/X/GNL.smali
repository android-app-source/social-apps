.class public LX/GNL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4Cg;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/01T;

.field private g:LX/0ad;

.field public h:LX/4Ch;


# direct methods
.method public constructor <init>(LX/01T;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345830
    iput-object p2, p0, LX/GNL;->g:LX/0ad;

    .line 2345831
    iput-object p1, p0, LX/GNL;->f:LX/01T;

    .line 2345832
    return-void
.end method

.method public static a(LX/0QB;)LX/GNL;
    .locals 1

    .prologue
    .line 2345828
    invoke-static {p0}, LX/GNL;->b(LX/0QB;)LX/GNL;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0W9;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2345833
    invoke-virtual {p0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/482;->from(Ljava/util/Locale;)LX/482;

    move-result-object v0

    sget-object v1, LX/482;->IMPERIAL:LX/482;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 2345834
    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "mile"

    :goto_1
    return-object v0

    .line 2345835
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2345836
    :cond_1
    const-string v0, "kilometer"

    goto :goto_1
.end method

.method private a(LX/0Px;LX/0Px;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;LX/GGG;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;",
            "LX/0Px",
            "<",
            "LX/GGF;",
            ">;",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            "LX/GGG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2345714
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_0
    sget-object v2, LX/GGG;->REGION:LX/GGG;

    if-ne p4, v2, :cond_8

    .line 2345715
    :cond_1
    :goto_0
    if-nez p2, :cond_b

    .line 2345716
    :cond_2
    new-instance v0, LX/4Ck;

    invoke-direct {v0}, LX/4Ck;-><init>()V

    .line 2345717
    iget-object v1, p0, LX/GNL;->e:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/GNL;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2345718
    iget-object v1, p0, LX/GNL;->e:Ljava/util/List;

    .line 2345719
    const-string v2, "custom_locations"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345720
    :cond_3
    iget-object v1, p0, LX/GNL;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 2345721
    iget-object v1, p0, LX/GNL;->c:Ljava/util/ArrayList;

    .line 2345722
    const-string v2, "city_keys"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345723
    :cond_4
    iget-object v1, p0, LX/GNL;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    .line 2345724
    iget-object v1, p0, LX/GNL;->a:Ljava/util/ArrayList;

    .line 2345725
    const-string v2, "countries"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345726
    :cond_5
    iget-object v1, p0, LX/GNL;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    .line 2345727
    iget-object v1, p0, LX/GNL;->b:Ljava/util/ArrayList;

    .line 2345728
    const-string v2, "region_keys"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345729
    :cond_6
    iget-object v1, p0, LX/GNL;->d:Ljava/util/ArrayList;

    if-eqz v1, :cond_7

    .line 2345730
    iget-object v1, p0, LX/GNL;->d:Ljava/util/ArrayList;

    .line 2345731
    const-string v2, "location_types"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345732
    :cond_7
    iget-object v1, p0, LX/GNL;->h:LX/4Ch;

    .line 2345733
    const-string v2, "geo_locations"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2345734
    return-void

    .line 2345735
    :cond_8
    if-nez p3, :cond_9

    sget-object v2, LX/GGG;->ADDRESS:LX/GGG;

    if-eq p4, v2, :cond_1

    .line 2345736
    :cond_9
    sget-object v2, LX/GGG;->ADDRESS:LX/GGG;

    if-ne p4, v2, :cond_a

    .line 2345737
    iget-object v2, p0, LX/GNL;->e:Ljava/util/List;

    .line 2345738
    new-instance v7, LX/4Cg;

    invoke-direct {v7}, LX/4Cg;-><init>()V

    .line 2345739
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->d()Ljava/lang/String;

    move-result-object v8

    .line 2345740
    const-string v9, "distance_unit"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2345741
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    .line 2345742
    const-string v9, "radius"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2345743
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    .line 2345744
    const-string v9, "latitude"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2345745
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    .line 2345746
    const-string v9, "longitude"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2345747
    move-object v3, v7

    .line 2345748
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2345749
    :cond_a
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2345750
    sget-object v5, LX/GNK;->a:[I

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2345751
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2345752
    :pswitch_0
    iget-object v5, p0, LX/GNL;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2345753
    :pswitch_1
    iget-object v5, p0, LX/GNL;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2345754
    :pswitch_2
    iget-object v5, p0, LX/GNL;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2345755
    :cond_b
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_2

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GGF;

    .line 2345756
    iget-object v3, p0, LX/GNL;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, LX/GGF;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2345757
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2345826
    iget-object v1, p0, LX/GNL;->f:LX/01T;

    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-eq v1, v2, :cond_0

    .line 2345827
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/GNL;->g:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/GDK;->d:S

    invoke-interface {v1, v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GNL;
    .locals 3

    .prologue
    .line 2345824
    new-instance v2, LX/GNL;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/GNL;-><init>(LX/01T;LX/0ad;)V

    .line 2345825
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)LX/4Ch;
    .locals 7

    .prologue
    .line 2345758
    new-instance v0, LX/4Ch;

    invoke-direct {v0}, LX/4Ch;-><init>()V

    iput-object v0, p0, LX/GNL;->h:LX/4Ch;

    .line 2345759
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GNL;->a:Ljava/util/ArrayList;

    .line 2345760
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GNL;->b:Ljava/util/ArrayList;

    .line 2345761
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GNL;->c:Ljava/util/ArrayList;

    .line 2345762
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GNL;->e:Ljava/util/List;

    .line 2345763
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GNL;->d:Ljava/util/ArrayList;

    .line 2345764
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v0, v0

    .line 2345765
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2345766
    iget-object v0, p0, LX/GNL;->h:LX/4Ch;

    .line 2345767
    iget v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v1, v1

    .line 2345768
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2345769
    const-string v2, "age_max"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2345770
    iget-object v0, p0, LX/GNL;->h:LX/4Ch;

    .line 2345771
    iget v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    move v1, v1

    .line 2345772
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2345773
    const-string v2, "age_min"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2345774
    :cond_0
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v0, v0

    .line 2345775
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2345776
    iget-object v0, p0, LX/GNL;->h:LX/4Ch;

    .line 2345777
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-object v1, v1

    .line 2345778
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2345779
    sget-object v2, LX/GNK;->b:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2345780
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    :goto_0
    move-object v1, v2

    .line 2345781
    const-string v2, "genders"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345782
    :cond_1
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v0, v0

    .line 2345783
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2345784
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v0, v0

    .line 2345785
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    move-object v1, v1

    .line 2345786
    iget-object v2, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v2, v2

    .line 2345787
    iget-object v3, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v3, v3

    .line 2345788
    invoke-direct {p0, v0, v1, v2, v3}, LX/GNL;->a(LX/0Px;LX/0Px;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;LX/GGG;)V

    .line 2345789
    :cond_2
    invoke-direct {p0}, LX/GNL;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2345790
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v0, v0

    .line 2345791
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2345792
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    move-object v0, v0

    .line 2345793
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2345794
    :cond_3
    :goto_1
    invoke-direct {p0}, LX/GNL;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2345795
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v0, v0

    .line 2345796
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->DETAILED_TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2345797
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    move-object v0, v0

    .line 2345798
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2345799
    :cond_4
    :goto_2
    iget-object v0, p0, LX/GNL;->h:LX/4Ch;

    return-object v0

    .line 2345800
    :pswitch_0
    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "FEMALE"

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    .line 2345801
    :pswitch_1
    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "MALE"

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    .line 2345802
    :cond_5
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2345803
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_6

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    .line 2345804
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2345805
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2345806
    :cond_6
    iget-object v1, p0, LX/GNL;->h:LX/4Ch;

    .line 2345807
    const-string v2, "interest_ids"

    invoke-virtual {v1, v2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345808
    goto :goto_1

    .line 2345809
    :cond_7
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2345810
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_8

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    .line 2345811
    new-instance v5, LX/4Ci;

    invoke-direct {v5}, LX/4Ci;-><init>()V

    .line 2345812
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->l()Ljava/lang/String;

    move-result-object v6

    .line 2345813
    const-string p1, "id"

    invoke-virtual {v5, p1, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2345814
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v1

    .line 2345815
    const-string v6, "type"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2345816
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2345817
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 2345818
    :cond_8
    new-instance v1, LX/4Cj;

    invoke-direct {v1}, LX/4Cj;-><init>()V

    .line 2345819
    const-string v2, "detailed_targeting_items"

    invoke-virtual {v1, v2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345820
    move-object v1, v1

    .line 2345821
    iget-object v2, p0, LX/GNL;->h:LX/4Ch;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2345822
    const-string v3, "flexible_spec"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2345823
    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
