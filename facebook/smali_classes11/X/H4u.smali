.class public final LX/H4u;
.super LX/CSB;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V
    .locals 0

    .prologue
    .line 2423504
    iput-object p1, p0, LX/H4u;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-direct {p0}, LX/CSB;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2423505
    check-cast p1, LX/CSA;

    .line 2423506
    iget-object v0, p0, LX/H4u;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    iget-object v1, p1, LX/CSA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/H5D;->b(Ljava/lang/String;)I

    move-result v0

    .line 2423507
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2423508
    iget-object v1, p0, LX/H4u;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v1, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    iget-object v2, p1, LX/CSA;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/H5D;->a(ILjava/lang/String;)V

    .line 2423509
    iget-object v1, p0, LX/H4u;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v1, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v1}, LX/2kw;->e()V

    .line 2423510
    iget-object v1, p0, LX/H4u;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    .line 2423511
    new-instance v2, Landroid/text/SpannableString;

    iget-object v3, p1, LX/CSA;->d:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2423512
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget-object v4, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->U:Landroid/content/Context;

    const v5, 0x7f0a00d5

    invoke-static {v4, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v4, 0x0

    iget-object v5, p1, LX/CSA;->d:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 p0, 0x11

    invoke-interface {v2, v3, v4, v5, p0}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2423513
    iget-object v3, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v4, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->k:LX/1rx;

    invoke-virtual {v4}, LX/1rx;->h()I

    move-result v4

    invoke-static {v3, v2, v4}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v2

    .line 2423514
    iget-object v3, p1, LX/CSA;->c:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2423515
    iget-object v3, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->U:Landroid/content/Context;

    const v4, 0x7f0a00d2

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, LX/3oW;->a(I)LX/3oW;

    move-result-object v3

    const v4, 0x7f08116d

    new-instance v5, LX/H4v;

    invoke-direct {v5, v1, v0, p1}, LX/H4v;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;ILX/CSA;)V

    invoke-virtual {v3, v4, v5}, LX/3oW;->a(ILandroid/view/View$OnClickListener;)LX/3oW;

    .line 2423516
    :cond_0
    iget-object v3, v2, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v3, v3

    .line 2423517
    const v4, 0x7f0a00f7

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2423518
    invoke-virtual {v2}, LX/3oW;->b()V

    .line 2423519
    :cond_1
    return-void
.end method
