.class public final LX/GMV;
.super Landroid/view/View$AccessibilityDelegate;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/EditableRadioButton;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/EditableRadioButton;)V
    .locals 0

    .prologue
    .line 2344782
    iput-object p1, p0, LX/GMV;->a:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 2344783
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2344784
    const-class v0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 2344785
    iget-object v0, p0, LX/GMV;->a:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-boolean v0, v0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->e:Z

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 2344786
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 2344787
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 2344788
    const-class v0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 2344789
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 2344790
    iget-object v0, p0, LX/GMV;->a:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-boolean v0, v0, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->e:Z

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 2344791
    return-void
.end method
