.class public abstract LX/FcJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FcI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Header:",
        "Landroid/view/View;",
        "Child:",
        "LX/Fd8;",
        "Item:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "LX/FcI",
        "<THeader;TChild;>;"
    }
.end annotation


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lC;

.field public final d:LX/0wM;

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/03V;

.field public h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2262273
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "set_search_features"

    const v2, 0x7f0209e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "set_search_price_category"

    const v2, 0x7f020935

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/FcJ;->c:LX/0P1;

    .line 2262274
    const-string v0, "set_search_features"

    const-string v1, "set_search_price_category"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/FcJ;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0wM;LX/0lC;LX/03V;)V
    .locals 1

    .prologue
    .line 2262267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262268
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FcJ;->i:Z

    .line 2262269
    iput-object p1, p0, LX/FcJ;->d:LX/0wM;

    .line 2262270
    iput-object p2, p0, LX/FcJ;->b:LX/0lC;

    .line 2262271
    iput-object p3, p0, LX/FcJ;->g:LX/03V;

    .line 2262272
    return-void
.end method

.method public static a$redex0(LX/FcJ;Ljava/util/Map;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2262263
    :try_start_0
    iget-object v0, p0, LX/FcJ;->b:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2262264
    :goto_0
    return-object v0

    .line 2262265
    :catch_0
    iget-object v0, p0, LX/FcJ;->g:LX/03V;

    const-string v1, "map_to_value_string_error"

    const-string v2, "There was an error converting the values map to a string"

    invoke-static {v1, v2}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2262266
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2262248
    iput-object p2, p0, LX/FcJ;->h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 2262249
    const/4 v2, 0x0

    .line 2262250
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v3

    .line 2262251
    new-instance v4, LX/0P2;

    invoke-direct {v4}, LX/0P2;-><init>()V

    .line 2262252
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/FcJ;->f:Ljava/util/Map;

    .line 2262253
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262254
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    .line 2262255
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 2262256
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 2262257
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2262258
    iget-object v7, p0, LX/FcJ;->f:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v7, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2262259
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2262260
    :cond_1
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/FcJ;->e:Ljava/util/Map;

    .line 2262261
    iget-object v0, p0, LX/FcJ;->e:Ljava/util/Map;

    invoke-static {p0, v0}, LX/FcJ;->a$redex0(LX/FcJ;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 2262262
    new-instance v1, LX/CyH;

    const-string v2, ""

    invoke-direct {v1, p1, v2, v0}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public final a(LX/CyH;)LX/FcT;
    .locals 5

    .prologue
    .line 2262239
    iget-object v0, p1, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262240
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "name"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2262241
    sget-object v1, LX/FcJ;->c:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2262242
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unimplemented filter "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2262243
    :cond_0
    new-instance v1, LX/FcT;

    .line 2262244
    iget-object v2, p1, LX/CyH;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2262245
    iget-object v3, p0, LX/FcJ;->d:LX/0wM;

    sget-object v4, LX/FcJ;->c:LX/0P1;

    invoke-virtual {v4, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v4, -0xa76f01

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2262246
    iget-object v3, p1, LX/CyH;->c:LX/4FP;

    move-object v3, v3

    .line 2262247
    invoke-direct {v1, v2, v0, v3}, LX/FcT;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;LX/4FP;)V

    return-object v1
.end method

.method public a(LX/5uu;LX/Fd8;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uu;",
            "TChild;",
            "LX/CyH;",
            "Lcom/facebook/search/results/filters/definition/SearchResultPageRadioGroupFilterDefinition$OnChooseMoreSelectedListener;",
            "Lcom/facebook/search/results/filters/ui/SearchResultFilterExpandableListAdapter$FilterPersistentStateCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2262212
    sget-object v2, LX/FcJ;->a:LX/0Rf;

    invoke-interface/range {p1 .. p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 2262213
    invoke-virtual {p0}, LX/FcJ;->d()I

    move-result v8

    .line 2262214
    invoke-interface/range {p1 .. p1}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v9

    .line 2262215
    if-nez v9, :cond_0

    .line 2262216
    :goto_0
    return-void

    .line 2262217
    :cond_0
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v2, v8, :cond_2

    const/4 v2, 0x1

    move v4, v2

    .line 2262218
    :goto_1
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2262219
    const/4 v3, 0x0

    .line 2262220
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v2, 0x0

    move v5, v2

    move v6, v3

    :goto_2
    if-ge v5, v12, :cond_4

    invoke-virtual {v11, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262221
    invoke-virtual {p0}, LX/FcJ;->c()LX/FcE;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, LX/Fd8;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-interface {v3, v13}, LX/FcE;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v13

    .line 2262222
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2262223
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v3

    .line 2262224
    if-nez v3, :cond_3

    const-string v3, ""

    .line 2262225
    :goto_3
    iget-object v14, p0, LX/FcJ;->f:Ljava/util/Map;

    invoke-interface {v14, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2262226
    move-object/from16 v0, p3

    invoke-virtual {p0, v2, v13, v0, v3}, LX/FcJ;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;Landroid/view/View;LX/CyH;Z)V

    .line 2262227
    invoke-virtual {v10, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262228
    add-int/lit8 v3, v6, 0x1

    .line 2262229
    if-eqz v7, :cond_1

    if-ne v3, v8, :cond_1

    iget-boolean v2, p0, LX/FcJ;->i:Z

    if-eqz v2, :cond_4

    .line 2262230
    :cond_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v6, v3

    goto :goto_2

    .line 2262231
    :cond_2
    const/4 v2, 0x0

    move v4, v2

    goto :goto_1

    .line 2262232
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2262233
    :cond_4
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/Fd8;->setItems(LX/0Px;)V

    .line 2262234
    if-eqz v7, :cond_5

    if-eqz v4, :cond_5

    .line 2262235
    new-instance v2, LX/FcW;

    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-direct {v2, p0, v0, v1}, LX/FcW;-><init>(LX/FcJ;LX/FdQ;LX/CyH;)V

    iget-boolean v3, p0, LX/FcJ;->i:Z

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, LX/Fd8;->a(LX/FcW;Z)V

    .line 2262236
    :cond_5
    new-instance v2, LX/FcX;

    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-direct {v2, p0, v9, v0, v1}, LX/FcX;-><init>(LX/FcJ;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;LX/FdQ;LX/CyH;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/Fd8;->setOnItemSelectedListener(LX/FcX;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 6

    .prologue
    .line 2262238
    move-object v2, p2

    check-cast v2, LX/Fd8;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/FcJ;->a(LX/5uu;LX/Fd8;LX/CyH;LX/FdU;LX/FdQ;)V

    return-void
.end method

.method public abstract a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;Landroid/view/View;LX/CyH;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageFilterValuesFragment$FilterValues$Edges;",
            "TItem;",
            "LX/CyH;",
            "Z)V"
        }
    .end annotation
.end method

.method public abstract c()LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<TItem;>;"
        }
    .end annotation
.end method

.method public abstract d()I
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2262237
    iget-object v0, p0, LX/FcJ;->e:Ljava/util/Map;

    iget-object v1, p0, LX/FcJ;->f:Ljava/util/Map;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
