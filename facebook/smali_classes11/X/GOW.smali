.class public final LX/GOW;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "LX/0am",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V
    .locals 0

    .prologue
    .line 2347646
    iput-object p1, p0, LX/GOW;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(LX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2347647
    iget-object v1, p0, LX/GOW;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-virtual {p1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-static {v1, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->b(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2347648
    return-void
.end method


# virtual methods
.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347649
    check-cast p1, LX/0am;

    invoke-direct {p0, p1}, LX/GOW;->a(LX/0am;)V

    return-void
.end method
