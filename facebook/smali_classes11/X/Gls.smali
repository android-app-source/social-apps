.class public LX/Gls;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/fig/button/FigButton;

.field public c:Landroid/view/View$OnClickListener;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2391313
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2391314
    const-class v0, LX/Gls;

    invoke-static {v0, p0}, LX/Gls;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2391315
    const v0, 0x7f03083e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2391316
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, LX/Gls;->setGravity(I)V

    .line 2391317
    const v0, 0x7f0d1586

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/Gls;->b:Lcom/facebook/fig/button/FigButton;

    .line 2391318
    new-instance v0, LX/Glr;

    invoke-direct {v0, p0}, LX/Glr;-><init>(LX/Gls;)V

    move-object v0, v0

    .line 2391319
    iput-object v0, p0, LX/Gls;->c:Landroid/view/View$OnClickListener;

    .line 2391320
    iget-object v0, p0, LX/Gls;->b:Lcom/facebook/fig/button/FigButton;

    iget-object p1, p0, LX/Gls;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2391321
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Gls;->setOrientation(I)V

    .line 2391322
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gls;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p0

    check-cast p0, LX/17W;

    iput-object p0, p1, LX/Gls;->a:LX/17W;

    return-void
.end method
