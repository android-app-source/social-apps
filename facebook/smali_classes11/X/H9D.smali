.class public LX/H9D;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/H9j;

.field private final B:LX/H9H;

.field private final C:LX/H9a;

.field private final D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Landroid/content/Context;

.field private final F:Landroid/view/View;

.field private final G:LX/01T;

.field private final b:LX/H9M;

.field private final c:LX/H95;

.field private final d:LX/H9S;

.field private final e:LX/H8f;

.field private final f:LX/H9y;

.field private final g:LX/H9m;

.field private final h:LX/H8r;

.field private final i:LX/H9c;

.field private final j:LX/H8t;

.field private final k:LX/H8h;

.field private final l:LX/H8n;

.field private final m:LX/H9Y;

.field private final n:LX/H8z;

.field private final o:LX/H8l;

.field private final p:LX/H8p;

.field private final q:LX/H9W;

.field private final r:LX/H9Q;

.field private final s:LX/H9s;

.field private final t:LX/H9u;

.field private final u:LX/H9h;

.field private final v:LX/H8b;

.field private final w:LX/H9B;

.field private final x:LX/H9w;

.field private final y:LX/H8d;

.field private final z:LX/H97;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 2434121
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SUGGEST_EDITS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPageActionType;->EDIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PROMOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SETTINGS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/16 v12, 0x26

    new-array v12, v12, [Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v13, 0x0

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PLACE_CLAIM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FAVOURITES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_EVENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/4 v13, 0x4

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->POST:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/4 v13, 0x5

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/4 v13, 0x6

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/4 v13, 0x7

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REPORT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x8

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ADS_MANAGER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x9

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_MESSAGE_SHORTLINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0xa

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->GET_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0xb

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REQUEST_RIDE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0xc

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0xd

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ADD_BUTTON:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0xe

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0xf

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x10

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x11

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x12

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x13

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x14

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_EMAIL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x15

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x16

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x17

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x18

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x19

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LISTEN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x1a

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x1b

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x1c

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x1d

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x1e

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x1f

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x20

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x21

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x22

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x23

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x24

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    const/16 v13, 0x25

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H9D;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/H9M;LX/H95;LX/H9S;LX/H8f;LX/H9y;LX/H9m;LX/H8h;LX/H8r;LX/H9W;LX/H9c;LX/H8t;LX/H8p;LX/H8n;LX/H9Y;LX/H8z;LX/H8l;LX/H9Q;LX/H9s;LX/H9u;LX/H9h;LX/H8b;LX/H9B;LX/H9w;LX/H8d;LX/H97;LX/H9j;LX/H9H;LX/H9a;LX/0Ot;LX/01T;Landroid/view/View;)V
    .locals 3
    .param p31    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H9M;",
            "LX/H95;",
            "LX/H9S;",
            "LX/H8f;",
            "LX/H9y;",
            "LX/H9m;",
            "LX/H8h;",
            "LX/H8r;",
            "LX/H9W;",
            "LX/H9c;",
            "LX/H8t;",
            "LX/H8p;",
            "LX/H8n;",
            "LX/H9Y;",
            "LX/H8z;",
            "LX/H8l;",
            "LX/H9Q;",
            "LX/H9s;",
            "LX/H9u;",
            "LX/H9h;",
            "LX/H8b;",
            "LX/H9B;",
            "LX/H9w;",
            "LX/H8d;",
            "LX/H97;",
            "LX/H9j;",
            "LX/H9H;",
            "LX/H9a;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/01T;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434123
    iput-object p1, p0, LX/H9D;->b:LX/H9M;

    .line 2434124
    iput-object p2, p0, LX/H9D;->c:LX/H95;

    .line 2434125
    move-object/from16 v0, p18

    iput-object v0, p0, LX/H9D;->s:LX/H9s;

    .line 2434126
    move-object/from16 v0, p29

    iput-object v0, p0, LX/H9D;->D:LX/0Ot;

    .line 2434127
    iput-object p3, p0, LX/H9D;->d:LX/H9S;

    .line 2434128
    iput-object p4, p0, LX/H9D;->e:LX/H8f;

    .line 2434129
    iput-object p5, p0, LX/H9D;->f:LX/H9y;

    .line 2434130
    iput-object p6, p0, LX/H9D;->g:LX/H9m;

    .line 2434131
    iput-object p7, p0, LX/H9D;->k:LX/H8h;

    .line 2434132
    iput-object p8, p0, LX/H9D;->h:LX/H8r;

    .line 2434133
    iput-object p9, p0, LX/H9D;->q:LX/H9W;

    .line 2434134
    iput-object p10, p0, LX/H9D;->i:LX/H9c;

    .line 2434135
    iput-object p11, p0, LX/H9D;->j:LX/H8t;

    .line 2434136
    iput-object p12, p0, LX/H9D;->p:LX/H8p;

    .line 2434137
    move-object/from16 v0, p13

    iput-object v0, p0, LX/H9D;->l:LX/H8n;

    .line 2434138
    move-object/from16 v0, p14

    iput-object v0, p0, LX/H9D;->m:LX/H9Y;

    .line 2434139
    move-object/from16 v0, p15

    iput-object v0, p0, LX/H9D;->n:LX/H8z;

    .line 2434140
    move-object/from16 v0, p16

    iput-object v0, p0, LX/H9D;->o:LX/H8l;

    .line 2434141
    move-object/from16 v0, p17

    iput-object v0, p0, LX/H9D;->r:LX/H9Q;

    .line 2434142
    move-object/from16 v0, p19

    iput-object v0, p0, LX/H9D;->t:LX/H9u;

    .line 2434143
    move-object/from16 v0, p20

    iput-object v0, p0, LX/H9D;->u:LX/H9h;

    .line 2434144
    move-object/from16 v0, p21

    iput-object v0, p0, LX/H9D;->v:LX/H8b;

    .line 2434145
    move-object/from16 v0, p22

    iput-object v0, p0, LX/H9D;->w:LX/H9B;

    .line 2434146
    move-object/from16 v0, p23

    iput-object v0, p0, LX/H9D;->x:LX/H9w;

    .line 2434147
    move-object/from16 v0, p24

    iput-object v0, p0, LX/H9D;->y:LX/H8d;

    .line 2434148
    move-object/from16 v0, p25

    iput-object v0, p0, LX/H9D;->z:LX/H97;

    .line 2434149
    move-object/from16 v0, p26

    iput-object v0, p0, LX/H9D;->A:LX/H9j;

    .line 2434150
    move-object/from16 v0, p27

    iput-object v0, p0, LX/H9D;->B:LX/H9H;

    .line 2434151
    move-object/from16 v0, p28

    iput-object v0, p0, LX/H9D;->C:LX/H9a;

    .line 2434152
    invoke-virtual/range {p31 .. p31}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434153
    move-object/from16 v0, p31

    iput-object v0, p0, LX/H9D;->F:Landroid/view/View;

    .line 2434154
    move-object/from16 v0, p30

    iput-object v0, p0, LX/H9D;->G:LX/01T;

    .line 2434155
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2434026
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2434027
    :cond_0
    iget-object v0, p0, LX/H9D;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "pages_identity_action_bar_fail"

    const-string v3, "Null action sent from server for action bar"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2434028
    :goto_0
    return-object v0

    .line 2434029
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, -0x7ceb8589

    if-ne v0, v2, :cond_2

    .line 2434030
    iget-object v0, p0, LX/H9D;->B:LX/H9H;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v4

    iget-object v5, p0, LX/H9D;->E:Landroid/content/Context;

    invoke-virtual/range {v0 .. v5}, LX/H9H;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)LX/H9G;

    move-result-object v0

    goto :goto_0

    .line 2434031
    :cond_2
    sget-object v0, LX/H9C;->a:[I

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2434032
    iget-object v0, p0, LX/H9D;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "pages_identity_action_bar_fail"

    const-string v3, "Unsupported action sent from server for action bar"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2434033
    goto :goto_0

    .line 2434034
    :pswitch_0
    iget-object v0, p0, LX/H9D;->b:LX/H9M;

    .line 2434035
    new-instance v6, LX/H9L;

    invoke-static {v0}, LX/H8W;->a(LX/0QB;)LX/H8W;

    move-result-object v7

    check-cast v7, LX/H8W;

    const/16 v8, 0x12b1

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x476

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x123

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    move-object v11, p1

    invoke-direct/range {v6 .. v11}, LX/H9L;-><init>(LX/H8W;LX/0Ot;LX/0Ot;LX/0Or;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)V

    .line 2434036
    move-object v0, v6

    .line 2434037
    goto :goto_0

    .line 2434038
    :pswitch_1
    iget-object v0, p0, LX/H9D;->c:LX/H95;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434039
    new-instance v6, LX/H94;

    invoke-static {v0}, LX/H8W;->a(LX/0QB;)LX/H8W;

    move-result-object v7

    check-cast v7, LX/H8W;

    const/16 v8, 0x2b49

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1430

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x455

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xc49

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    move-object v12, v1

    move-object v13, v2

    invoke-direct/range {v6 .. v13}, LX/H94;-><init>(LX/H8W;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434040
    move-object v0, v6

    .line 2434041
    goto/16 :goto_0

    .line 2434042
    :pswitch_2
    iget-object v0, p0, LX/H9D;->d:LX/H9S;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434043
    new-instance v6, LX/H9R;

    invoke-static {v0}, LX/H8W;->a(LX/0QB;)LX/H8W;

    move-result-object v7

    check-cast v7, LX/H8W;

    const/16 v8, 0xbc6

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x455

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v11, v1

    move-object v12, v2

    invoke-direct/range {v6 .. v12}, LX/H9R;-><init>(LX/H8W;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434044
    move-object v0, v6

    .line 2434045
    goto/16 :goto_0

    .line 2434046
    :pswitch_3
    iget-object v0, p0, LX/H9D;->k:LX/H8h;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434047
    new-instance v3, LX/H8g;

    const/16 v4, 0x2b48

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v4, v1, v2}, LX/H8g;-><init>(LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434048
    move-object v0, v3

    .line 2434049
    goto/16 :goto_0

    .line 2434050
    :pswitch_4
    iget-object v1, p0, LX/H9D;->e:LX/H8f;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v0, p0, LX/H9D;->E:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2434051
    new-instance v3, LX/H8e;

    const/16 v4, 0x2b48

    invoke-static {v1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3be

    invoke-static {v1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2, v0}, LX/H8e;-><init>(LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/app/Activity;)V

    .line 2434052
    move-object v0, v3

    .line 2434053
    goto/16 :goto_0

    .line 2434054
    :pswitch_5
    iget-object v1, p0, LX/H9D;->f:LX/H9y;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v0, p0, LX/H9D;->E:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2434055
    new-instance v6, LX/H9x;

    const/16 v7, 0x2b48

    invoke-static {v1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1a59

    invoke-static {v1, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x259

    invoke-static {v1, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x455

    invoke-static {v1, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v11, v2

    move-object v12, v0

    invoke-direct/range {v6 .. v12}, LX/H9x;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/app/Activity;)V

    .line 2434056
    move-object v0, v6

    .line 2434057
    goto/16 :goto_0

    .line 2434058
    :pswitch_6
    iget-object v0, p0, LX/H9D;->g:LX/H9m;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434059
    new-instance v6, LX/H9l;

    const/16 v7, 0x2b48

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x31cf

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3be

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    move-object v10, v1

    move-object v11, v2

    invoke-direct/range {v6 .. v11}, LX/H9l;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434060
    move-object v0, v6

    .line 2434061
    goto/16 :goto_0

    .line 2434062
    :pswitch_7
    iget-object v1, p0, LX/H9D;->h:LX/H8r;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v0, p0, LX/H9D;->E:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2434063
    new-instance v6, LX/H8q;

    const/16 v7, 0x2b48

    invoke-static {v1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2b68

    invoke-static {v1, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xac0

    invoke-static {v1, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc49

    invoke-static {v1, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x455

    invoke-static {v1, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    move-object v12, v2

    move-object v13, v0

    invoke-direct/range {v6 .. v13}, LX/H8q;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434064
    move-object v0, v6

    .line 2434065
    goto/16 :goto_0

    .line 2434066
    :pswitch_8
    iget-object v1, p0, LX/H9D;->q:LX/H9W;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v0, p0, LX/H9D;->E:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v2, v0}, LX/H9W;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)LX/H9V;

    move-result-object v0

    goto/16 :goto_0

    .line 2434067
    :pswitch_9
    iget-object v0, p0, LX/H9D;->i:LX/H9c;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434068
    new-instance v6, LX/H9b;

    const/16 v7, 0x455

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2b68

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xc49

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    move-object v10, v1

    move-object v11, v2

    invoke-direct/range {v6 .. v12}, LX/H9b;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;LX/0W3;)V

    .line 2434069
    move-object v0, v6

    .line 2434070
    goto/16 :goto_0

    .line 2434071
    :pswitch_a
    iget-object v0, p0, LX/H9D;->j:LX/H8t;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434072
    new-instance v6, LX/H8s;

    const/16 v7, 0x455

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xc49

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2b68

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    move-object v10, v1

    move-object v11, v2

    invoke-direct/range {v6 .. v11}, LX/H8s;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434073
    move-object v0, v6

    .line 2434074
    goto/16 :goto_0

    .line 2434075
    :pswitch_b
    iget-object v0, p0, LX/H9D;->p:LX/H8p;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    .line 2434076
    new-instance v6, LX/H8o;

    const/16 v7, 0x2b48

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1926

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2b2

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x34

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x12c4

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    move-object v12, v1

    invoke-direct/range {v6 .. v12}, LX/H8o;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)V

    .line 2434077
    move-object v0, v6

    .line 2434078
    goto/16 :goto_0

    .line 2434079
    :pswitch_c
    iget-object v0, p0, LX/H9D;->l:LX/H8n;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434080
    new-instance v6, LX/H8m;

    const/16 v7, 0x2b48

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2c1f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x455

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    move-object v12, v1

    move-object v13, v2

    invoke-direct/range {v6 .. v13}, LX/H8m;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/Boolean;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434081
    move-object v0, v6

    .line 2434082
    goto/16 :goto_0

    .line 2434083
    :pswitch_d
    iget-object v1, p0, LX/H9D;->m:LX/H9Y;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v0, p0, LX/H9D;->E:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2434084
    new-instance v6, LX/H9X;

    const/16 v7, 0x2b48

    invoke-static {v1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x259

    invoke-static {v1, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x455

    invoke-static {v1, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v1}, LX/HSx;->b(LX/0QB;)LX/HSx;

    move-result-object v10

    check-cast v10, LX/HSx;

    move-object v11, v2

    move-object v12, v0

    invoke-direct/range {v6 .. v12}, LX/H9X;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/HSx;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434085
    move-object v0, v6

    .line 2434086
    goto/16 :goto_0

    .line 2434087
    :pswitch_e
    iget-object v0, p0, LX/H9D;->n:LX/H8z;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/H8z;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)LX/H8y;

    move-result-object v0

    goto/16 :goto_0

    .line 2434088
    :pswitch_f
    iget-object v0, p0, LX/H9D;->o:LX/H8l;

    iget-object v1, p0, LX/H9D;->G:LX/01T;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v3, p0, LX/H9D;->E:Landroid/content/Context;

    invoke-virtual {v0, v1, v2, v3}, LX/H8l;->a(LX/01T;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)LX/H8k;

    move-result-object v0

    goto/16 :goto_0

    .line 2434089
    :pswitch_10
    iget-object v1, p0, LX/H9D;->r:LX/H9Q;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v0, p0, LX/H9D;->E:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v2, v0}, LX/H9Q;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)LX/H9P;

    move-result-object v0

    goto/16 :goto_0

    .line 2434090
    :pswitch_11
    iget-object v0, p0, LX/H9D;->s:LX/H9s;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/H9s;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)LX/H9r;

    move-result-object v0

    goto/16 :goto_0

    .line 2434091
    :pswitch_12
    iget-object v0, p0, LX/H9D;->t:LX/H9u;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434092
    new-instance v3, LX/H9t;

    const/16 v4, 0x2b48

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3be

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct {v3, v4, v5, v1, v2}, LX/H9t;-><init>(LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434093
    move-object v0, v3

    .line 2434094
    goto/16 :goto_0

    .line 2434095
    :pswitch_13
    iget-object v1, p0, LX/H9D;->u:LX/H9h;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v2

    iget-object v0, p0, LX/H9D;->E:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iget-object v3, p0, LX/H9D;->F:Landroid/view/View;

    invoke-virtual {v1, v2, v0, v3}, LX/H9h;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;Landroid/view/View;)LX/H9g;

    move-result-object v0

    goto/16 :goto_0

    .line 2434096
    :pswitch_14
    iget-object v0, p0, LX/H9D;->v:LX/H8b;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434097
    new-instance v6, LX/H8a;

    const/16 v7, 0x2b48

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x78

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xbc6

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x455

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    move-object v12, v1

    move-object v13, v2

    invoke-direct/range {v6 .. v13}, LX/H8a;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434098
    move-object v0, v6

    .line 2434099
    goto/16 :goto_0

    .line 2434100
    :pswitch_15
    iget-object v0, p0, LX/H9D;->x:LX/H9w;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434101
    new-instance v3, LX/H9v;

    const/16 v4, 0x455

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v4, v1, v2}, LX/H9v;-><init>(LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434102
    move-object v0, v3

    .line 2434103
    goto/16 :goto_0

    .line 2434104
    :pswitch_16
    iget-object v0, p0, LX/H9D;->w:LX/H9B;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/H9B;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)LX/H9A;

    move-result-object v0

    goto/16 :goto_0

    .line 2434105
    :pswitch_17
    iget-object v0, p0, LX/H9D;->z:LX/H97;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434106
    new-instance v6, LX/H96;

    const/16 v7, 0x2b48

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v11

    check-cast v11, LX/E1i;

    move-object v7, v1

    move-object v10, v2

    invoke-direct/range {v6 .. v11}, LX/H96;-><init>(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;LX/0Ot;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/E1i;)V

    .line 2434107
    move-object v0, v6

    .line 2434108
    goto/16 :goto_0

    .line 2434109
    :pswitch_18
    iget-object v0, p0, LX/H9D;->y:LX/H8d;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434110
    new-instance v6, LX/H8c;

    const/16 v7, 0x2b48

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    move-object v7, v1

    move-object v8, v2

    invoke-direct/range {v6 .. v11}, LX/H8c;-><init>(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V

    .line 2434111
    move-object v0, v6

    .line 2434112
    goto/16 :goto_0

    .line 2434113
    :pswitch_19
    iget-object v0, p0, LX/H9D;->A:LX/H9j;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434114
    new-instance v6, LX/H9i;

    const/16 v7, 0x2b48

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/CK5;->a(LX/0QB;)LX/CK5;

    move-result-object v11

    check-cast v11, LX/CK5;

    move-object v7, v1

    move-object v10, v2

    invoke-direct/range {v6 .. v11}, LX/H9i;-><init>(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;LX/0Ot;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/CK5;)V

    .line 2434115
    move-object v0, v6

    .line 2434116
    goto/16 :goto_0

    .line 2434117
    :pswitch_1a
    iget-object v0, p0, LX/H9D;->C:LX/H9a;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    iget-object v2, p0, LX/H9D;->E:Landroid/content/Context;

    .line 2434118
    new-instance v3, LX/H9Z;

    const/16 v4, 0x2b48

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v4, v1, v2}, LX/H9Z;-><init>(LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V

    .line 2434119
    move-object v0, v3

    .line 2434120
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method
