.class public final LX/GuY;
.super LX/GuX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

.field public b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2403398
    iput-object p1, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403399
    invoke-direct {p0, p2}, LX/GuX;-><init>(Landroid/os/Handler;)V

    .line 2403400
    const/4 v0, -0x1

    iput v0, p0, LX/GuY;->c:I

    .line 2403401
    iput-object v1, p0, LX/GuY;->d:Ljava/lang/String;

    .line 2403402
    iput-object v1, p0, LX/GuY;->e:Ljava/lang/String;

    .line 2403403
    iput-object v1, p0, LX/GuY;->h:Ljava/lang/String;

    .line 2403404
    return-void
.end method

.method public static c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2403440
    const-string v0, "compose"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2403441
    const v0, 0x7f020f25

    .line 2403442
    :goto_0
    return v0

    .line 2403443
    :cond_0
    const-string v0, "add"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2403444
    const v0, 0x7f020f20

    goto :goto_0

    .line 2403445
    :cond_1
    const-string v0, "action"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2403446
    const v0, 0x7f020725

    goto :goto_0

    .line 2403447
    :cond_2
    const-string v0, "feed_filter_live"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2403448
    const v0, 0x7f020b26

    goto :goto_0

    .line 2403449
    :cond_3
    const-string v0, "feed_filter_h_chr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2403450
    const v0, 0x7f020b22

    goto :goto_0

    .line 2403451
    :cond_4
    const-string v0, "feed_filter_status"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2403452
    const v0, 0x7f020b25

    goto :goto_0

    .line 2403453
    :cond_5
    const-string v0, "feed_filter_photos"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2403454
    const v0, 0x7f020b24

    goto :goto_0

    .line 2403455
    :cond_6
    const-string v0, "feed_filter_links"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2403456
    const v0, 0x7f020b21

    goto :goto_0

    .line 2403457
    :cond_7
    const-string v0, "feed_filter_pages"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2403458
    const v0, 0x7f020b23

    goto :goto_0

    .line 2403459
    :cond_8
    const-string v0, "feed_filter_events"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2403460
    const v0, 0x7f020b1f

    goto :goto_0

    .line 2403461
    :cond_9
    const-string v0, "feed_filter_videos"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2403462
    const v0, 0x7f020b27

    goto :goto_0

    .line 2403463
    :cond_a
    const-string v0, "feed_filter_lists"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2403464
    const v0, 0x7f020b20

    goto/16 :goto_0

    .line 2403465
    :cond_b
    const-string v0, "like"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2403466
    const v0, 0x7f020f24

    goto/16 :goto_0

    .line 2403467
    :cond_c
    const-string v0, "unlike"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2403468
    const v0, 0x7f020f24

    goto/16 :goto_0

    .line 2403469
    :cond_d
    const-string v0, "feed_find_friends"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2403470
    const v0, 0x7f020b29

    goto/16 :goto_0

    .line 2403471
    :cond_e
    const/4 v0, -0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2403472
    iget-object v0, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const-class v1, LX/1ZF;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2403473
    iget-object v1, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403474
    iget-object v4, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v4

    .line 2403475
    iget-object v4, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403476
    iget-boolean v5, v4, Landroid/support/v4/app/Fragment;->mHidden:Z

    move v4, v5

    .line 2403477
    if-nez v4, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2403478
    :cond_0
    :goto_0
    return-void

    .line 2403479
    :cond_1
    iget-object v1, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->V:LX/0zF;

    .line 2403480
    iget-object v4, v1, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v4, v4

    .line 2403481
    if-nez v4, :cond_2

    if-eqz v0, :cond_0

    .line 2403482
    :cond_2
    iget-object v1, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->H:LX/GuQ;

    sget-object v5, LX/GuQ;->NONE:LX/GuQ;

    if-eq v1, v5, :cond_0

    .line 2403483
    iget-object v1, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->H:LX/GuQ;

    sget-object v5, LX/GuQ;->OPTIONS_MENU:LX/GuQ;

    if-ne v1, v5, :cond_6

    .line 2403484
    iget-object v0, p0, LX/GuY;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2403485
    iget-object v0, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    new-instance v1, LX/GuR;

    iget-object v2, p0, LX/GuY;->e:Ljava/lang/String;

    .line 2403486
    const-string v3, "feed_filter_live"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "feed_filter_h_chr"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2403487
    :cond_3
    const v3, 0x7f020b1e

    .line 2403488
    :goto_1
    move v2, v3

    .line 2403489
    iget-object v3, p0, LX/GuY;->e:Ljava/lang/String;

    .line 2403490
    const/4 v4, 0x0

    .line 2403491
    const-string v5, "feed_filter_live"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "feed_filter_h_chr"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2403492
    :cond_4
    iget-object v4, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f083150

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2403493
    :cond_5
    move-object v3, v4

    .line 2403494
    iget-object v4, p0, LX/GuY;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, LX/GuR;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 2403495
    iput-object v1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->I:LX/GuR;

    .line 2403496
    goto :goto_0

    .line 2403497
    :cond_6
    iget v1, p0, LX/GuY;->c:I

    if-lez v1, :cond_f

    .line 2403498
    iget v1, p0, LX/GuY;->c:I

    const v5, 0x7f020f24

    if-ne v1, v5, :cond_f

    .line 2403499
    iget-object v1, p0, LX/GuY;->e:Ljava/lang/String;

    const-string v5, "like"

    invoke-static {v1, v5}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    move v1, v2

    .line 2403500
    :goto_2
    iget-object v5, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403501
    iget-object v6, v5, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v6

    .line 2403502
    const-string v6, "titlebar_with_modal_done"

    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2403503
    invoke-virtual {v4}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a()V

    .line 2403504
    invoke-virtual {v4, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2403505
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    iget-object v2, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08001e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2403506
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2403507
    move-object v1, v1

    .line 2403508
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2403509
    new-instance v2, LX/GuT;

    invoke-direct {v2, p0, v0, v4}, LX/GuT;-><init>(LX/GuY;LX/1ZF;Lcom/facebook/ui/titlebar/Fb4aTitleBar;)V

    .line 2403510
    if-eqz v0, :cond_8

    .line 2403511
    const-string v3, ""

    invoke-interface {v0, v3}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2403512
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2403513
    new-instance v1, LX/GuU;

    invoke-direct {v1, p0, v2}, LX/GuU;-><init>(LX/GuY;LX/107;)V

    .line 2403514
    iget-object v2, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {v2}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2403515
    iget-object v2, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v2, v2, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->aa:LX/Guy;

    .line 2403516
    iput-object v1, v2, LX/Guy;->f:LX/63W;

    .line 2403517
    new-instance v3, LX/Gux;

    invoke-direct {v3, v2}, LX/Gux;-><init>(LX/Guy;)V

    move-object v1, v3

    .line 2403518
    :cond_7
    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto/16 :goto_0

    .line 2403519
    :cond_8
    const-string v0, ""

    invoke-virtual {v4, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2403520
    invoke-virtual {v4, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2403521
    invoke-virtual {v4, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    goto/16 :goto_0

    .line 2403522
    :cond_9
    iget-object v5, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {v5}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->b(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2403523
    iget-object v1, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403524
    invoke-static {v1, v4, v0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a$redex0(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Lcom/facebook/ui/titlebar/Fb4aTitleBar;LX/1ZF;)V

    .line 2403525
    goto/16 :goto_0

    .line 2403526
    :cond_a
    iget-object v5, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {v5}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->D(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2403527
    invoke-virtual {v4, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    goto/16 :goto_0

    .line 2403528
    :cond_b
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v5

    iget v6, p0, LX/GuY;->c:I

    .line 2403529
    iput v6, v5, LX/108;->i:I

    .line 2403530
    move-object v5, v5

    .line 2403531
    iget-object v6, p0, LX/GuY;->d:Ljava/lang/String;

    .line 2403532
    iput-object v6, v5, LX/108;->g:Ljava/lang/String;

    .line 2403533
    move-object v5, v5

    .line 2403534
    iget-object v6, p0, LX/GuY;->h:Ljava/lang/String;

    .line 2403535
    iput-object v6, v5, LX/108;->j:Ljava/lang/String;

    .line 2403536
    move-object v5, v5

    .line 2403537
    iput-boolean v1, v5, LX/108;->k:Z

    .line 2403538
    move-object v1, v5

    .line 2403539
    iget-boolean v5, p0, LX/GuY;->i:Z

    if-nez v5, :cond_c

    .line 2403540
    :goto_3
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2403541
    move-object v1, v1

    .line 2403542
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2403543
    iget-object v1, p0, LX/GuY;->b:Ljava/lang/String;

    if-nez v1, :cond_d

    const/4 v1, 0x0

    .line 2403544
    :goto_4
    if-eqz v0, :cond_e

    .line 2403545
    invoke-interface {v0, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2403546
    if-eqz v1, :cond_0

    .line 2403547
    new-instance v2, LX/GuW;

    invoke-direct {v2, p0, v1}, LX/GuW;-><init>(LX/GuY;LX/107;)V

    .line 2403548
    invoke-interface {v0, v2}, LX/1ZF;->a(LX/63W;)V

    goto/16 :goto_0

    :cond_c
    move v2, v3

    .line 2403549
    goto :goto_3

    .line 2403550
    :cond_d
    new-instance v1, LX/GuV;

    invoke-direct {v1, p0}, LX/GuV;-><init>(LX/GuY;)V

    goto :goto_4

    .line 2403551
    :cond_e
    invoke-virtual {v4, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2403552
    if-eqz v1, :cond_0

    .line 2403553
    invoke-virtual {v4, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    goto/16 :goto_0

    :cond_f
    move v1, v3

    goto/16 :goto_2

    :cond_10
    invoke-static {v2}, LX/GuY;->c(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2403405
    const-string v0, "right"

    iget-object v1, p0, LX/GuX;->g:LX/BWN;

    .line 2403406
    iget-object v2, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v2, v2

    .line 2403407
    const-string v3, "position"

    invoke-interface {v1, v2, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2403408
    :goto_0
    return-void

    .line 2403409
    :cond_0
    iget-object v0, p0, LX/GuX;->g:LX/BWN;

    .line 2403410
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403411
    const-string v2, "script"

    invoke-interface {v0, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GuY;->b:Ljava/lang/String;

    .line 2403412
    iget-object v0, p0, LX/GuX;->g:LX/BWN;

    .line 2403413
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403414
    const-string v2, "title"

    invoke-interface {v0, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403415
    iget-object v1, p0, LX/GuX;->g:LX/BWN;

    .line 2403416
    iget-object v2, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v2, v2

    .line 2403417
    const-string v3, "type"

    invoke-interface {v1, v2, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403418
    invoke-static {v1}, LX/GuY;->c(Ljava/lang/String;)I

    move-result v2

    .line 2403419
    iget-object v3, p0, LX/GuY;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2403420
    if-eqz v3, :cond_4

    .line 2403421
    const-string v4, "add"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2403422
    const v4, 0x7f08315b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2403423
    :goto_1
    move-object v3, v3

    .line 2403424
    iput-object v3, p0, LX/GuY;->h:Ljava/lang/String;

    .line 2403425
    iget-object v3, p0, LX/GuX;->g:LX/BWN;

    .line 2403426
    iget-object v4, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v4, v4

    .line 2403427
    const-string v5, "isDisabled"

    invoke-interface {v3, v4, v5}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, LX/GuY;->i:Z

    .line 2403428
    if-ltz v2, :cond_1

    .line 2403429
    iput v2, p0, LX/GuY;->c:I

    .line 2403430
    iput-object v6, p0, LX/GuY;->d:Ljava/lang/String;

    .line 2403431
    iput-object v1, p0, LX/GuY;->e:Ljava/lang/String;

    goto :goto_0

    .line 2403432
    :cond_1
    const/4 v1, -0x1

    iput v1, p0, LX/GuY;->c:I

    .line 2403433
    iput-object v0, p0, LX/GuY;->d:Ljava/lang/String;

    .line 2403434
    iput-object v6, p0, LX/GuY;->e:Ljava/lang/String;

    goto :goto_0

    .line 2403435
    :cond_2
    const-string v4, "like"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2403436
    const v4, 0x7f08314f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2403437
    :cond_3
    const-string v4, "unlike"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2403438
    const v4, 0x7f08315a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2403439
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method
