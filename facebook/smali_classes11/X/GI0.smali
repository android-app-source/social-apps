.class public LX/GI0;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/17Y;

.field private final b:Landroid/content/Context;

.field private c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

.field public d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public e:LX/15i;

.field public f:I


# direct methods
.method public constructor <init>(LX/17Y;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2335655
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2335656
    iput-object p2, p0, LX/GI0;->b:Landroid/content/Context;

    .line 2335657
    iput-object p1, p0, LX/GI0;->a:LX/17Y;

    .line 2335658
    return-void
.end method

.method private static a(Ljava/lang/String;)LX/8wK;
    .locals 2

    .prologue
    .line 2335643
    const/4 v0, 0x0

    .line 2335644
    :try_start_0
    invoke-static {p0}, LX/8wK;->valueOf(Ljava/lang/String;)LX/8wK;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2335645
    :goto_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2335646
    iput-object p1, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    .line 2335647
    iget-object v0, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setVisibility(I)V

    .line 2335648
    iget-object v0, p0, LX/GI0;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335649
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2335650
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x4c046c8a    # 3.4714152E7f

    invoke-static {v1, v0, v3, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, p0, LX/GI0;->e:LX/15i;

    iput v0, p0, LX/GI0;->f:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2335651
    invoke-direct {p0}, LX/GI0;->b()V

    .line 2335652
    invoke-direct {p0}, LX/GI0;->c()V

    .line 2335653
    return-void

    .line 2335654
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static synthetic b(LX/GI0;Ljava/lang/String;)LX/8wK;
    .locals 1

    .prologue
    .line 2335659
    invoke-static {p1}, LX/GI0;->a(Ljava/lang/String;)LX/8wK;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GI0;
    .locals 3

    .prologue
    .line 2335633
    new-instance v2, LX/GI0;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/GI0;-><init>(LX/17Y;Landroid/content/Context;)V

    .line 2335634
    return-object v2
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 2335635
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v2, p0, LX/GI0;->f:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    const/4 v3, 0x7

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setTitleText(Ljava/lang/String;)V

    .line 2335636
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v2, p0, LX/GI0;->f:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v1, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setContentText(Ljava/lang/String;)V

    .line 2335637
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v2, p0, LX/GI0;->f:I

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual {v0, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2335638
    :goto_0
    return-void

    .line 2335639
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2335640
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2335641
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 2335642
    :cond_0
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_6
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v2, p0, LX/GI0;->f:I

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    iget-object v1, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    invoke-virtual {v0, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setFbDraweeImageURI(Landroid/net/Uri;)V

    goto :goto_0

    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method

.method public static b(LX/GI0;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2335628
    iget-object v1, p0, LX/GI0;->a:LX/17Y;

    iget-object v0, p0, LX/GI0;->b:Landroid/content/Context;

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-interface {v1, v0, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2335629
    if-eqz v0, :cond_0

    .line 2335630
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2335631
    new-instance v2, LX/GFS;

    invoke-direct {v2, v0}, LX/GFS;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2335632
    :cond_0
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x5

    const/4 v2, 0x0

    .line 2335604
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v4, p0, LX/GI0;->f:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0, v4, v5}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    .line 2335605
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v4, p0, LX/GI0;->f:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2335606
    invoke-virtual {v0, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 2335607
    :goto_0
    iget-object v3, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setAYMTButtonVisibility(Z)V

    .line 2335608
    if-nez v0, :cond_3

    .line 2335609
    :cond_0
    :goto_1
    return-void

    .line 2335610
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2335611
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_1
    move v0, v2

    .line 2335612
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2335613
    :cond_3
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_4
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v4, p0, LX/GI0;->f:I

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    iget-object v3, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    invoke-virtual {v0, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setAYMTButtonText(Ljava/lang/String;)V

    .line 2335614
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_5
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v3, p0, LX/GI0;->f:I

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    invoke-virtual {v0, v3, v5}, LX/15i;->g(II)I

    move-result v2

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2335615
    iget-object v0, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    new-instance v1, LX/GHy;

    invoke-direct {v1, p0}, LX/GHy;-><init>(LX/GI0;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setAYMTButtonListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2335616
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 2335617
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    .line 2335618
    :cond_4
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_8
    iget-object v0, p0, LX/GI0;->e:LX/15i;

    iget v3, p0, LX/GI0;->f:I

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    invoke-virtual {v0, v3, v5}, LX/15i;->g(II)I

    move-result v2

    const-class v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2335619
    iget-object v0, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    new-instance v1, LX/GHz;

    invoke-direct {v1, p0}, LX/GHz;-><init>(LX/GI0;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;->setAYMTButtonListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2335620
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2335625
    invoke-super {p0}, LX/GHg;->a()V

    .line 2335626
    const/4 v0, 0x0

    iput-object v0, p0, LX/GI0;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    .line 2335627
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335624
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;

    invoke-direct {p0, p1}, LX/GI0;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2335621
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335622
    iput-object p1, p0, LX/GI0;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335623
    return-void
.end method
