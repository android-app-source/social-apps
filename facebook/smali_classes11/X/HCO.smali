.class public LX/HCO;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/pages/common/editpage/EditTabOrderItemTouchHelperAdapter;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageOpenActionEditActionData;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0wM;

.field public final c:LX/00H;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/content/Context;

.field public final i:J

.field public final j:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

.field private final k:Landroid/view/View$OnClickListener;

.field public l:Z

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>(LX/0wM;LX/00H;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;JLcom/facebook/pages/common/editpage/EditTabOrderFragment;)V
    .locals 2
    .param p7    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Lcom/facebook/pages/common/editpage/EditTabOrderFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0wM;",
            "LX/00H;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/content/Context;",
            "J",
            "Lcom/facebook/pages/common/editpage/OnStartDragListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2438576
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2438577
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HCO;->a:Ljava/util/List;

    .line 2438578
    iput-boolean v1, p0, LX/HCO;->l:Z

    .line 2438579
    iput-boolean v1, p0, LX/HCO;->m:Z

    .line 2438580
    iput-boolean v1, p0, LX/HCO;->n:Z

    .line 2438581
    iput-object p1, p0, LX/HCO;->b:LX/0wM;

    .line 2438582
    iput-object p2, p0, LX/HCO;->c:LX/00H;

    .line 2438583
    iput-object p3, p0, LX/HCO;->d:LX/0Ot;

    .line 2438584
    iput-object p4, p0, LX/HCO;->e:LX/0Ot;

    .line 2438585
    iput-object p5, p0, LX/HCO;->f:LX/0Ot;

    .line 2438586
    iput-object p6, p0, LX/HCO;->g:LX/0Ot;

    .line 2438587
    iput-object p7, p0, LX/HCO;->h:Landroid/content/Context;

    .line 2438588
    iput-wide p8, p0, LX/HCO;->i:J

    .line 2438589
    iput-object p10, p0, LX/HCO;->j:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    .line 2438590
    new-instance v0, LX/HCI;

    invoke-direct {v0, p0}, LX/HCI;-><init>(LX/HCO;)V

    iput-object v0, p0, LX/HCO;->k:Landroid/view/View$OnClickListener;

    .line 2438591
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2438592
    sget-object v0, LX/HCN;->UPDATE_APP_HEADER:LX/HCN;

    invoke-virtual {v0}, LX/HCN;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2438593
    new-instance v0, LX/HCM;

    iget-object v1, p0, LX/HCO;->h:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f031549

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HCM;-><init>(Landroid/view/View;)V

    .line 2438594
    :goto_0
    return-object v0

    .line 2438595
    :cond_0
    sget-object v0, LX/HCN;->TAB_ROW:LX/HCN;

    invoke-virtual {v0}, LX/HCN;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2438596
    new-instance v0, LX/HCL;

    iget-object v1, p0, LX/HCO;->h:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030463

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/HCL;-><init>(LX/HCO;Landroid/view/View;)V

    goto :goto_0

    .line 2438597
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create ViewHolder itemViewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2438598
    instance-of v0, p1, LX/HCM;

    if-eqz v0, :cond_0

    .line 2438599
    check-cast p1, LX/HCM;

    iget-object v0, p0, LX/HCO;->k:Landroid/view/View$OnClickListener;

    .line 2438600
    iget-object v1, p1, LX/HCM;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2438601
    :goto_0
    return-void

    .line 2438602
    :cond_0
    instance-of v0, p1, LX/HCL;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2438603
    check-cast v0, LX/HCL;

    .line 2438604
    iget-object v1, p0, LX/HCO;->a:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2438605
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v1

    .line 2438606
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    .line 2438607
    iget-object v3, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2438608
    iget-object v1, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v3, p0, LX/HCO;->b:LX/0wM;

    invoke-static {v2}, LX/HCF;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v4

    iget-object v5, p0, LX/HCO;->h:Landroid/content/Context;

    const v6, 0x7f0a00a6

    invoke-static {v5, v6}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2438609
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v2, v1, :cond_1

    .line 2438610
    iget-object v1, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, LX/HCO;->h:Landroid/content/Context;

    const v4, 0x7f0a009a

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v1, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2438611
    iget-object v1, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v2, p0, LX/HCO;->b:LX/0wM;

    const v3, 0x7f020914

    iget-object v4, p0, LX/HCO;->h:Landroid/content/Context;

    const v5, 0x7f0a00a6

    invoke-static {v4, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2438612
    iget-object v1, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2438613
    invoke-virtual {v1, v7}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2438614
    :goto_1
    iget-object v0, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2438615
    invoke-virtual {v0, v7}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2438616
    goto :goto_0

    .line 2438617
    :cond_1
    iget-object v1, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, LX/HCO;->h:Landroid/content/Context;

    const v4, 0x7f0a0097

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v1, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2438618
    iget-object v1, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v2, p0, LX/HCO;->b:LX/0wM;

    const v3, 0x7f020908

    iget-object v4, p0, LX/HCO;->h:Landroid/content/Context;

    const v5, 0x7f0a00a6

    invoke-static {v4, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2438619
    iget-object v1, v0, LX/HCL;->l:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v2, LX/HCJ;

    invoke-direct {v2, p0, p1}, LX/HCJ;-><init>(LX/HCO;LX/1a1;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    .line 2438620
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot bind ViewHolder for position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2438621
    iget-boolean v0, p0, LX/HCO;->l:Z

    if-eqz v0, :cond_0

    .line 2438622
    sget-object v0, LX/HCN;->UPDATE_APP_HEADER:LX/HCN;

    invoke-virtual {v0}, LX/HCN;->ordinal()I

    move-result v0

    .line 2438623
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HCN;->TAB_ROW:LX/HCN;

    invoke-virtual {v0}, LX/HCN;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2438624
    iget-boolean v0, p0, LX/HCO;->l:Z

    if-eqz v0, :cond_0

    .line 2438625
    const/4 v0, 0x1

    .line 2438626
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/HCO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
