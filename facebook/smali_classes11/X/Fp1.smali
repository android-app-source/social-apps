.class public final LX/Fp1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V
    .locals 0

    .prologue
    .line 2290516
    iput-object p1, p0, LX/Fp1;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 2290517
    if-nez p2, :cond_0

    .line 2290518
    iget-object v0, p0, LX/Fp1;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2290519
    iget-object p0, v0, LX/BOa;->a:LX/0Zb;

    const-string p1, "fundraiser_creation_changed_title"

    const/4 p2, 0x0

    invoke-static {v0, p1, p2}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290520
    :cond_0
    return-void
.end method
