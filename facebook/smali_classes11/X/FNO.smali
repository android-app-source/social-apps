.class public LX/FNO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FNO;


# instance fields
.field public a:LX/3MI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2232566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232567
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232568
    iput-object v0, p0, LX/FNO;->b:LX/0Ot;

    .line 2232569
    return-void
.end method

.method public static a(LX/0QB;)LX/FNO;
    .locals 6

    .prologue
    .line 2232570
    sget-object v0, LX/FNO;->d:LX/FNO;

    if-nez v0, :cond_1

    .line 2232571
    const-class v1, LX/FNO;

    monitor-enter v1

    .line 2232572
    :try_start_0
    sget-object v0, LX/FNO;->d:LX/FNO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232573
    if-eqz v2, :cond_0

    .line 2232574
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232575
    new-instance v5, LX/FNO;

    invoke-direct {v5}, LX/FNO;-><init>()V

    .line 2232576
    invoke-static {v0}, LX/3MI;->a(LX/0QB;)LX/3MI;

    move-result-object v3

    check-cast v3, LX/3MI;

    const/16 v4, 0x29b7

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    .line 2232577
    iput-object v3, v5, LX/FNO;->a:LX/3MI;

    iput-object p0, v5, LX/FNO;->b:LX/0Ot;

    iput-object v4, v5, LX/FNO;->c:LX/0SG;

    .line 2232578
    move-object v0, v5

    .line 2232579
    sput-object v0, LX/FNO;->d:LX/FNO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232580
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232581
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232582
    :cond_1
    sget-object v0, LX/FNO;->d:LX/FNO;

    return-object v0

    .line 2232583
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/FNO;LX/6jM;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6jM;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2232557
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2232558
    :cond_0
    :goto_0
    return-void

    .line 2232559
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v3, v0

    move v1, v0

    move v2, v0

    :goto_1
    if-ge v3, v4, :cond_3

    invoke-virtual {p2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2232560
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, p0, LX/FNO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNh;

    invoke-virtual {v0}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2232561
    add-int/lit8 v0, v2, 0x1

    move v6, v1

    move v1, v0

    move v0, v6

    .line 2232562
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 2232563
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    goto :goto_2

    .line 2232564
    :cond_3
    iput v2, p1, LX/6jM;->d:I

    .line 2232565
    iput v1, p1, LX/6jM;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2232548
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FNO;->a:LX/3MI;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/3MI;->b(Ljava/lang/String;)LX/6jM;

    move-result-object v3

    .line 2232549
    if-nez v3, :cond_0

    .line 2232550
    invoke-static {}, LX/6jM;->newBuilder()LX/6jN;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/6jN;->a(Ljava/lang/String;)LX/6jN;

    move-result-object v2

    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v4, v5, v6, v7}, LX/6jN;->a(DJ)LX/6jN;

    move-result-object v2

    invoke-virtual {v2}, LX/6jN;->g()LX/6jM;

    move-result-object v3

    .line 2232551
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FNO;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    .line 2232552
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v3, v1}, LX/FNO;->a(LX/FNO;LX/6jM;LX/0Px;)V

    .line 2232553
    invoke-static {v3, v6, v7}, LX/FNR;->a(LX/6jM;J)D

    move-result-wide v10

    .line 2232554
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FNO;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FNh;

    invoke-virtual {v2}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v4

    const/4 v8, 0x0

    move-object/from16 v5, p2

    invoke-static/range {v3 .. v8}, LX/6jQ;->a(LX/6jM;Lcom/facebook/messaging/model/messages/ParticipantInfo;LX/0Px;JZ)D

    move-result-wide v12

    .line 2232555
    move-object/from16 v0, p0

    iget-object v9, v0, LX/FNO;->a:LX/3MI;

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    move-object/from16 v10, p1

    move-wide v14, v6

    invoke-virtual/range {v9 .. v15}, LX/3MI;->a(Ljava/lang/String;Ljava/lang/Double;DJ)V

    .line 2232556
    return-void
.end method

.method public final b(Ljava/lang/String;LX/0Px;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2232540
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FNO;->a:LX/3MI;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/3MI;->b(Ljava/lang/String;)LX/6jM;

    move-result-object v3

    .line 2232541
    if-nez v3, :cond_0

    .line 2232542
    invoke-static {}, LX/6jM;->newBuilder()LX/6jN;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/6jN;->a(Ljava/lang/String;)LX/6jN;

    move-result-object v2

    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v4, v5, v6, v7}, LX/6jN;->a(DJ)LX/6jN;

    move-result-object v2

    invoke-virtual {v2}, LX/6jN;->g()LX/6jM;

    move-result-object v3

    .line 2232543
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FNO;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    .line 2232544
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v3, v1}, LX/FNO;->a(LX/FNO;LX/6jM;LX/0Px;)V

    .line 2232545
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FNO;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FNh;

    invoke-virtual {v2}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v4

    const/4 v8, 0x0

    move-object/from16 v5, p2

    invoke-static/range {v3 .. v8}, LX/6jQ;->a(LX/6jM;Lcom/facebook/messaging/model/messages/ParticipantInfo;LX/0Px;JZ)D

    move-result-wide v12

    .line 2232546
    move-object/from16 v0, p0

    iget-object v9, v0, LX/FNO;->a:LX/3MI;

    const/4 v11, 0x0

    move-object/from16 v10, p1

    move-wide v14, v6

    invoke-virtual/range {v9 .. v15}, LX/3MI;->a(Ljava/lang/String;Ljava/lang/Double;DJ)V

    .line 2232547
    return-void
.end method
