.class public final LX/G9u;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2324108
    const/16 v0, 0x60

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/G9u;->a:[I

    return-void

    :array_0
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x24
        -0x1
        -0x1
        -0x1
        0x25
        0x26
        -0x1
        -0x1
        -0x1
        -0x1
        0x27
        0x28
        -0x1
        0x29
        0x2a
        0x2b
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0x2c
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
        0x1a
        0x1b
        0x1c
        0x1d
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0x23
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2324106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2324107
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 2324103
    sget-object v0, LX/G9u;->a:[I

    array-length v0, v0

    if-ge p0, v0, :cond_0

    .line 2324104
    sget-object v0, LX/G9u;->a:[I

    aget v0, v0, p0

    .line 2324105
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(LX/G9s;)I
    .locals 12

    .prologue
    .line 2324052
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/G9v;->a(LX/G9s;Z)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/G9v;->a(LX/G9s;Z)I

    move-result v1

    add-int/2addr v0, v1

    move v0, v0

    .line 2324053
    const/4 v3, 0x0

    .line 2324054
    iget-object v1, p0, LX/G9s;->a:[[B

    move-object v5, v1

    .line 2324055
    iget v1, p0, LX/G9s;->b:I

    move v6, v1

    .line 2324056
    iget v1, p0, LX/G9s;->c:I

    move v7, v1

    .line 2324057
    move v4, v3

    move v1, v3

    .line 2324058
    :goto_0
    add-int/lit8 v2, v7, -0x1

    if-ge v4, v2, :cond_2

    move v2, v3

    .line 2324059
    :goto_1
    add-int/lit8 v8, v6, -0x1

    if-ge v2, v8, :cond_1

    .line 2324060
    aget-object v8, v5, v4

    aget-byte v8, v8, v2

    .line 2324061
    aget-object v9, v5, v4

    add-int/lit8 v10, v2, 0x1

    aget-byte v9, v9, v10

    if-ne v8, v9, :cond_0

    add-int/lit8 v9, v4, 0x1

    aget-object v9, v5, v9

    aget-byte v9, v9, v2

    if-ne v8, v9, :cond_0

    add-int/lit8 v9, v4, 0x1

    aget-object v9, v5, v9

    add-int/lit8 v10, v2, 0x1

    aget-byte v9, v9, v10

    if-ne v8, v9, :cond_0

    .line 2324062
    add-int/lit8 v1, v1, 0x1

    .line 2324063
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2324064
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2324065
    :cond_2
    mul-int/lit8 v1, v1, 0x3

    move v1, v1

    .line 2324066
    add-int/2addr v0, v1

    .line 2324067
    const/4 v3, 0x0

    const/4 v11, 0x1

    .line 2324068
    iget-object v1, p0, LX/G9s;->a:[[B

    move-object v5, v1

    .line 2324069
    iget v1, p0, LX/G9s;->b:I

    move v6, v1

    .line 2324070
    iget v1, p0, LX/G9s;->c:I

    move v7, v1

    .line 2324071
    move v4, v3

    move v1, v3

    .line 2324072
    :goto_2
    if-ge v4, v7, :cond_8

    move v2, v3

    .line 2324073
    :goto_3
    if-ge v2, v6, :cond_7

    .line 2324074
    aget-object v8, v5, v4

    .line 2324075
    add-int/lit8 v9, v2, 0x6

    if-ge v9, v6, :cond_4

    aget-byte v9, v8, v2

    if-ne v9, v11, :cond_4

    add-int/lit8 v9, v2, 0x1

    aget-byte v9, v8, v9

    if-nez v9, :cond_4

    add-int/lit8 v9, v2, 0x2

    aget-byte v9, v8, v9

    if-ne v9, v11, :cond_4

    add-int/lit8 v9, v2, 0x3

    aget-byte v9, v8, v9

    if-ne v9, v11, :cond_4

    add-int/lit8 v9, v2, 0x4

    aget-byte v9, v8, v9

    if-ne v9, v11, :cond_4

    add-int/lit8 v9, v2, 0x5

    aget-byte v9, v8, v9

    if-nez v9, :cond_4

    add-int/lit8 v9, v2, 0x6

    aget-byte v9, v8, v9

    if-ne v9, v11, :cond_4

    add-int/lit8 v9, v2, -0x4

    .line 2324076
    invoke-static {v8, v9, v2}, LX/G9v;->a([BII)Z

    move-result v9

    if-nez v9, :cond_3

    add-int/lit8 v9, v2, 0x7

    add-int/lit8 v10, v2, 0xb

    invoke-static {v8, v9, v10}, LX/G9v;->a([BII)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2324077
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 2324078
    :cond_4
    add-int/lit8 v8, v4, 0x6

    if-ge v8, v7, :cond_6

    aget-object v8, v5, v4

    aget-byte v8, v8, v2

    if-ne v8, v11, :cond_6

    add-int/lit8 v8, v4, 0x1

    aget-object v8, v5, v8

    aget-byte v8, v8, v2

    if-nez v8, :cond_6

    add-int/lit8 v8, v4, 0x2

    aget-object v8, v5, v8

    aget-byte v8, v8, v2

    if-ne v8, v11, :cond_6

    add-int/lit8 v8, v4, 0x3

    aget-object v8, v5, v8

    aget-byte v8, v8, v2

    if-ne v8, v11, :cond_6

    add-int/lit8 v8, v4, 0x4

    aget-object v8, v5, v8

    aget-byte v8, v8, v2

    if-ne v8, v11, :cond_6

    add-int/lit8 v8, v4, 0x5

    aget-object v8, v5, v8

    aget-byte v8, v8, v2

    if-nez v8, :cond_6

    add-int/lit8 v8, v4, 0x6

    aget-object v8, v5, v8

    aget-byte v8, v8, v2

    if-ne v8, v11, :cond_6

    add-int/lit8 v8, v4, -0x4

    .line 2324079
    invoke-static {v5, v2, v8, v4}, LX/G9v;->a([[BIII)Z

    move-result v8

    if-nez v8, :cond_5

    add-int/lit8 v8, v4, 0x7

    add-int/lit8 v9, v4, 0xb

    invoke-static {v5, v2, v8, v9}, LX/G9v;->a([[BIII)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2324080
    :cond_5
    add-int/lit8 v1, v1, 0x1

    .line 2324081
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 2324082
    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 2324083
    :cond_8
    mul-int/lit8 v1, v1, 0x28

    move v1, v1

    .line 2324084
    add-int/2addr v0, v1

    .line 2324085
    const/4 v3, 0x0

    .line 2324086
    iget-object v1, p0, LX/G9s;->a:[[B

    move-object v5, v1

    .line 2324087
    iget v1, p0, LX/G9s;->b:I

    move v6, v1

    .line 2324088
    iget v1, p0, LX/G9s;->c:I

    move v7, v1

    .line 2324089
    move v4, v3

    move v1, v3

    .line 2324090
    :goto_4
    if-ge v4, v7, :cond_b

    .line 2324091
    aget-object v8, v5, v4

    move v2, v3

    .line 2324092
    :goto_5
    if-ge v2, v6, :cond_a

    .line 2324093
    aget-byte v9, v8, v2

    const/4 v10, 0x1

    if-ne v9, v10, :cond_9

    .line 2324094
    add-int/lit8 v1, v1, 0x1

    .line 2324095
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 2324096
    :cond_a
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    .line 2324097
    :cond_b
    iget v2, p0, LX/G9s;->c:I

    move v2, v2

    .line 2324098
    iget v3, p0, LX/G9s;->b:I

    move v3, v3

    .line 2324099
    mul-int/2addr v2, v3

    .line 2324100
    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    div-int/2addr v1, v2

    .line 2324101
    mul-int/lit8 v1, v1, 0xa

    move v1, v1

    .line 2324102
    add-int/2addr v0, v1

    return v0
.end method

.method public static a(LX/G95;III)LX/G95;
    .locals 11

    .prologue
    .line 2324014
    invoke-virtual {p0}, LX/G95;->b()I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 2324015
    new-instance v0, LX/G94;

    const-string v1, "Number of bits and data bytes does not match"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324016
    :cond_0
    const/4 v2, 0x0

    .line 2324017
    const/4 v1, 0x0

    .line 2324018
    const/4 v0, 0x0

    .line 2324019
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2324020
    const/4 v3, 0x0

    move v6, v0

    move v7, v1

    move v8, v2

    :goto_0
    if-ge v3, p3, :cond_1

    .line 2324021
    const/4 v0, 0x1

    new-array v4, v0, [I

    .line 2324022
    const/4 v0, 0x1

    new-array v5, v0, [I

    move v0, p1

    move v1, p2

    move v2, p3

    .line 2324023
    invoke-static/range {v0 .. v5}, LX/G9u;->a(IIII[I[I)V

    .line 2324024
    const/4 v0, 0x0

    aget v0, v4, v0

    .line 2324025
    new-array v1, v0, [B

    .line 2324026
    mul-int/lit8 v2, v8, 0x8

    const/4 v10, 0x0

    invoke-virtual {p0, v2, v1, v10, v0}, LX/G95;->a(I[BII)V

    .line 2324027
    const/4 v2, 0x0

    aget v2, v5, v2

    invoke-static {v1, v2}, LX/G9u;->a([BI)[B

    move-result-object v2

    .line 2324028
    new-instance v5, LX/G9r;

    invoke-direct {v5, v1, v2}, LX/G9r;-><init>([B[B)V

    invoke-interface {v9, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 2324029
    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2324030
    array-length v0, v2

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2324031
    const/4 v2, 0x0

    aget v2, v4, v2

    add-int/2addr v2, v8

    .line 2324032
    add-int/lit8 v3, v3, 0x1

    move v6, v0

    move v7, v1

    move v8, v2

    goto :goto_0

    .line 2324033
    :cond_1
    if-eq p2, v8, :cond_2

    .line 2324034
    new-instance v0, LX/G94;

    const-string v1, "Data bytes does not match offset"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324035
    :cond_2
    new-instance v2, LX/G95;

    invoke-direct {v2}, LX/G95;-><init>()V

    .line 2324036
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v7, :cond_5

    .line 2324037
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9r;

    .line 2324038
    iget-object v4, v0, LX/G9r;->a:[B

    move-object v0, v4

    .line 2324039
    array-length v4, v0

    if-ge v1, v4, :cond_3

    .line 2324040
    aget-byte v0, v0, v1

    const/16 v4, 0x8

    invoke-virtual {v2, v0, v4}, LX/G95;->a(II)V

    goto :goto_2

    .line 2324041
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2324042
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v6, :cond_8

    .line 2324043
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9r;

    .line 2324044
    iget-object v4, v0, LX/G9r;->b:[B

    move-object v0, v4

    .line 2324045
    array-length v4, v0

    if-ge v1, v4, :cond_6

    .line 2324046
    aget-byte v0, v0, v1

    const/16 v4, 0x8

    invoke-virtual {v2, v0, v4}, LX/G95;->a(II)V

    goto :goto_4

    .line 2324047
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2324048
    :cond_8
    invoke-virtual {v2}, LX/G95;->b()I

    move-result v0

    if-eq p1, v0, :cond_9

    .line 2324049
    new-instance v0, LX/G94;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Interleaving error: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " and "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2324050
    invoke-virtual {v2}, LX/G95;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " differ."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324051
    :cond_9
    return-object v2
.end method

.method public static a(ILX/G9c;)LX/G9i;
    .locals 4

    .prologue
    .line 2324003
    const/4 v0, 0x1

    :goto_0
    const/16 v1, 0x28

    if-gt v0, v1, :cond_1

    .line 2324004
    invoke-static {v0}, LX/G9i;->b(I)LX/G9i;

    move-result-object v1

    .line 2324005
    iget v2, v1, LX/G9i;->f:I

    move v2, v2

    .line 2324006
    invoke-virtual {v1, p1}, LX/G9i;->a(LX/G9c;)LX/G9h;

    move-result-object v3

    .line 2324007
    invoke-virtual {v3}, LX/G9h;->c()I

    move-result v3

    .line 2324008
    sub-int/2addr v2, v3

    .line 2324009
    add-int/lit8 v3, p0, 0x7

    div-int/lit8 v3, v3, 0x8

    .line 2324010
    if-lt v2, v3, :cond_0

    .line 2324011
    return-object v1

    .line 2324012
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2324013
    :cond_1
    new-instance v0, LX/G94;

    const-string v1, "Data too big"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(IIII[I[I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2324109
    if-lt p3, p2, :cond_0

    .line 2324110
    new-instance v0, LX/G94;

    const-string v1, "Block ID too large"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324111
    :cond_0
    rem-int v0, p0, p2

    .line 2324112
    sub-int v1, p2, v0

    .line 2324113
    div-int v2, p0, p2

    .line 2324114
    add-int/lit8 v3, v2, 0x1

    .line 2324115
    div-int v4, p1, p2

    .line 2324116
    add-int/lit8 v5, v4, 0x1

    .line 2324117
    sub-int/2addr v2, v4

    .line 2324118
    sub-int/2addr v3, v5

    .line 2324119
    if-eq v2, v3, :cond_1

    .line 2324120
    new-instance v0, LX/G94;

    const-string v1, "EC bytes mismatch"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324121
    :cond_1
    add-int v6, v1, v0

    if-eq p2, v6, :cond_2

    .line 2324122
    new-instance v0, LX/G94;

    const-string v1, "RS blocks mismatch"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324123
    :cond_2
    add-int v6, v4, v2

    mul-int/2addr v6, v1

    add-int v7, v5, v3

    mul-int/2addr v0, v7

    add-int/2addr v0, v6

    if-eq p0, v0, :cond_3

    .line 2324124
    new-instance v0, LX/G94;

    const-string v1, "Total bytes mismatch"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324125
    :cond_3
    if-ge p3, v1, :cond_4

    .line 2324126
    aput v4, p4, v8

    .line 2324127
    aput v2, p5, v8

    .line 2324128
    :goto_0
    return-void

    .line 2324129
    :cond_4
    aput v5, p4, v8

    .line 2324130
    aput v3, p5, v8

    goto :goto_0
.end method

.method public static a(ILX/G95;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x0

    .line 2323977
    mul-int/lit8 v2, p0, 0x8

    .line 2323978
    iget v1, p1, LX/G95;->b:I

    move v1, v1

    .line 2323979
    if-le v1, v2, :cond_0

    .line 2323980
    new-instance v0, LX/G94;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "data bits cannot fit in the QR Code"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2323981
    iget v3, p1, LX/G95;->b:I

    move v3, v3

    .line 2323982
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " > "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, v0

    .line 2323983
    :goto_0
    const/4 v3, 0x4

    if-ge v1, v3, :cond_1

    .line 2323984
    iget v3, p1, LX/G95;->b:I

    move v3, v3

    .line 2323985
    if-ge v3, v2, :cond_1

    .line 2323986
    invoke-virtual {p1, v0}, LX/G95;->a(Z)V

    .line 2323987
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2323988
    :cond_1
    iget v1, p1, LX/G95;->b:I

    move v1, v1

    .line 2323989
    and-int/lit8 v1, v1, 0x7

    .line 2323990
    if-lez v1, :cond_2

    .line 2323991
    :goto_1
    if-ge v1, v4, :cond_2

    .line 2323992
    invoke-virtual {p1, v0}, LX/G95;->a(Z)V

    .line 2323993
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2323994
    :cond_2
    invoke-virtual {p1}, LX/G95;->b()I

    move-result v1

    sub-int v3, p0, v1

    move v1, v0

    .line 2323995
    :goto_2
    if-ge v1, v3, :cond_4

    .line 2323996
    and-int/lit8 v0, v1, 0x1

    if-nez v0, :cond_3

    const/16 v0, 0xec

    :goto_3
    invoke-virtual {p1, v0, v4}, LX/G95;->a(II)V

    .line 2323997
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2323998
    :cond_3
    const/16 v0, 0x11

    goto :goto_3

    .line 2323999
    :cond_4
    iget v0, p1, LX/G95;->b:I

    move v0, v0

    .line 2324000
    if-eq v0, v2, :cond_5

    .line 2324001
    new-instance v0, LX/G94;

    const-string v1, "Bits size does not equal capacity"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324002
    :cond_5
    return-void
.end method

.method public static a(ILX/G9i;LX/G9e;LX/G95;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2323972
    invoke-virtual {p2, p1}, LX/G9e;->getCharacterCountBits(LX/G9i;)I

    move-result v0

    .line 2323973
    shl-int v1, v4, v0

    if-lt p0, v1, :cond_0

    .line 2323974
    new-instance v1, LX/G94;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is bigger than "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    shl-int v0, v4, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2323975
    :cond_0
    invoke-virtual {p3, p0, v0}, LX/G95;->a(II)V

    .line 2323976
    return-void
.end method

.method private static a(Ljava/lang/String;LX/G95;)V
    .locals 7

    .prologue
    const v6, 0x8140

    const/4 v1, -0x1

    .line 2323954
    :try_start_0
    const-string v0, "Shift_JIS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2323955
    array-length v4, v3

    .line 2323956
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    .line 2323957
    aget-byte v0, v3, v2

    and-int/lit16 v0, v0, 0xff

    .line 2323958
    add-int/lit8 v5, v2, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    .line 2323959
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v5

    .line 2323960
    if-lt v0, v6, :cond_0

    const v5, 0x9ffc

    if-gt v0, v5, :cond_0

    .line 2323961
    sub-int/2addr v0, v6

    .line 2323962
    :goto_1
    if-ne v0, v1, :cond_1

    .line 2323963
    new-instance v0, LX/G94;

    const-string v1, "Invalid byte sequence"

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2323964
    :catch_0
    move-exception v0

    .line 2323965
    new-instance v1, LX/G94;

    invoke-direct {v1, v0}, LX/G94;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2323966
    :cond_0
    const v5, 0xe040

    if-lt v0, v5, :cond_3

    const v5, 0xebbf

    if-gt v0, v5, :cond_3

    .line 2323967
    const v5, 0xc140

    sub-int/2addr v0, v5

    goto :goto_1

    .line 2323968
    :cond_1
    shr-int/lit8 v5, v0, 0x8

    mul-int/lit16 v5, v5, 0xc0

    and-int/lit16 v0, v0, 0xff

    add-int/2addr v0, v5

    .line 2323969
    const/16 v5, 0xd

    invoke-virtual {p1, v0, v5}, LX/G95;->a(II)V

    .line 2323970
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    .line 2323971
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;LX/G95;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2323947
    :try_start_0
    invoke-virtual {p0, p2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2323948
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-byte v3, v1, v0

    .line 2323949
    const/16 v4, 0x8

    invoke-virtual {p1, v3, v4}, LX/G95;->a(II)V

    .line 2323950
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2323951
    :catch_0
    move-exception v0

    .line 2323952
    new-instance v1, LX/G94;

    invoke-direct {v1, v0}, LX/G94;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2323953
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;LX/G9e;LX/G95;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2323896
    sget-object v0, LX/G9t;->a:[I

    invoke-virtual {p1}, LX/G9e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2323897
    new-instance v0, LX/G94;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/G94;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2323898
    :pswitch_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 2323899
    const/4 v0, 0x0

    .line 2323900
    :goto_0
    if-ge v0, v1, :cond_2

    .line 2323901
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    add-int/lit8 v2, v2, -0x30

    .line 2323902
    add-int/lit8 p1, v0, 0x2

    if-ge p1, v1, :cond_0

    .line 2323903
    add-int/lit8 p1, v0, 0x1

    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result p1

    add-int/lit8 p1, p1, -0x30

    .line 2323904
    add-int/lit8 p3, v0, 0x2

    invoke-interface {p0, p3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result p3

    add-int/lit8 p3, p3, -0x30

    .line 2323905
    mul-int/lit8 v2, v2, 0x64

    mul-int/lit8 p1, p1, 0xa

    add-int/2addr v2, p1

    add-int/2addr v2, p3

    const/16 p1, 0xa

    invoke-virtual {p2, v2, p1}, LX/G95;->a(II)V

    .line 2323906
    add-int/lit8 v0, v0, 0x3

    .line 2323907
    goto :goto_0

    :cond_0
    add-int/lit8 p1, v0, 0x1

    if-ge p1, v1, :cond_1

    .line 2323908
    add-int/lit8 p1, v0, 0x1

    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result p1

    add-int/lit8 p1, p1, -0x30

    .line 2323909
    mul-int/lit8 v2, v2, 0xa

    add-int/2addr v2, p1

    const/4 p1, 0x7

    invoke-virtual {p2, v2, p1}, LX/G95;->a(II)V

    .line 2323910
    add-int/lit8 v0, v0, 0x2

    .line 2323911
    goto :goto_0

    .line 2323912
    :cond_1
    const/4 p1, 0x4

    invoke-virtual {p2, v2, p1}, LX/G95;->a(II)V

    .line 2323913
    add-int/lit8 v0, v0, 0x1

    .line 2323914
    goto :goto_0

    .line 2323915
    :cond_2
    :goto_1
    return-void

    .line 2323916
    :pswitch_1
    invoke-static {p0, p2}, LX/G9u;->b(Ljava/lang/CharSequence;LX/G95;)V

    goto :goto_1

    .line 2323917
    :pswitch_2
    invoke-static {p0, p2, p3}, LX/G9u;->a(Ljava/lang/String;LX/G95;Ljava/lang/String;)V

    goto :goto_1

    .line 2323918
    :pswitch_3
    invoke-static {p0, p2}, LX/G9u;->a(Ljava/lang/String;LX/G95;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a([BI)[B
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2323936
    array-length v2, p0

    .line 2323937
    add-int v1, v2, p1

    new-array v3, v1, [I

    move v1, v0

    .line 2323938
    :goto_0
    if-ge v1, v2, :cond_0

    .line 2323939
    aget-byte v4, p0, v1

    and-int/lit16 v4, v4, 0xff

    aput v4, v3, v1

    .line 2323940
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2323941
    :cond_0
    new-instance v1, LX/G9L;

    sget-object v4, LX/G9I;->e:LX/G9I;

    invoke-direct {v1, v4}, LX/G9L;-><init>(LX/G9I;)V

    invoke-virtual {v1, v3, p1}, LX/G9L;->a([II)V

    .line 2323942
    new-array v1, p1, [B

    .line 2323943
    :goto_1
    if-ge v0, p1, :cond_1

    .line 2323944
    add-int v4, v2, v0

    aget v4, v3, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v0

    .line 2323945
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2323946
    :cond_1
    return-object v1
.end method

.method private static b(Ljava/lang/CharSequence;LX/G95;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 2323919
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 2323920
    const/4 v0, 0x0

    .line 2323921
    :goto_0
    if-ge v0, v1, :cond_3

    .line 2323922
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, LX/G9u;->a(I)I

    move-result v2

    .line 2323923
    if-ne v2, v4, :cond_0

    .line 2323924
    new-instance v0, LX/G94;

    invoke-direct {v0}, LX/G94;-><init>()V

    throw v0

    .line 2323925
    :cond_0
    add-int/lit8 v3, v0, 0x1

    if-ge v3, v1, :cond_2

    .line 2323926
    add-int/lit8 v3, v0, 0x1

    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, LX/G9u;->a(I)I

    move-result v3

    .line 2323927
    if-ne v3, v4, :cond_1

    .line 2323928
    new-instance v0, LX/G94;

    invoke-direct {v0}, LX/G94;-><init>()V

    throw v0

    .line 2323929
    :cond_1
    mul-int/lit8 v2, v2, 0x2d

    add-int/2addr v2, v3

    const/16 v3, 0xb

    invoke-virtual {p1, v2, v3}, LX/G95;->a(II)V

    .line 2323930
    add-int/lit8 v0, v0, 0x2

    .line 2323931
    goto :goto_0

    .line 2323932
    :cond_2
    const/4 v3, 0x6

    invoke-virtual {p1, v2, v3}, LX/G95;->a(II)V

    .line 2323933
    add-int/lit8 v0, v0, 0x1

    .line 2323934
    goto :goto_0

    .line 2323935
    :cond_3
    return-void
.end method
