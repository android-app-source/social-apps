.class public final LX/G9L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G9I;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/G9J;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/G9I;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2322366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322367
    iput-object p1, p0, LX/G9L;->a:LX/G9I;

    .line 2322368
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/G9L;->b:Ljava/util/List;

    .line 2322369
    iget-object v0, p0, LX/G9L;->b:Ljava/util/List;

    new-instance v1, LX/G9J;

    new-array v2, v4, [I

    const/4 v3, 0x0

    aput v4, v2, v3

    invoke-direct {v1, p1, v2}, LX/G9J;-><init>(LX/G9I;[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2322370
    return-void
.end method


# virtual methods
.method public final a([II)V
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2322338
    if-nez p2, :cond_0

    .line 2322339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No error correction bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322340
    :cond_0
    array-length v0, p1

    sub-int v2, v0, p2

    .line 2322341
    if-gtz v2, :cond_1

    .line 2322342
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No data bytes provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322343
    :cond_1
    const/4 v11, 0x1

    .line 2322344
    iget-object v0, p0, LX/G9L;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_2

    .line 2322345
    iget-object v0, p0, LX/G9L;->b:Ljava/util/List;

    iget-object v3, p0, LX/G9L;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9J;

    .line 2322346
    iget-object v3, p0, LX/G9L;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v12, v3

    move-object v3, v0

    move v0, v12

    :goto_0
    if-gt v0, p2, :cond_2

    .line 2322347
    new-instance v4, LX/G9J;

    iget-object v5, p0, LX/G9L;->a:LX/G9I;

    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v11, v7, v8

    iget-object v8, p0, LX/G9L;->a:LX/G9I;

    add-int/lit8 v9, v0, -0x1

    iget-object v10, p0, LX/G9L;->a:LX/G9I;

    .line 2322348
    iget v12, v10, LX/G9I;->o:I

    move v10, v12

    .line 2322349
    add-int/2addr v9, v10

    invoke-virtual {v8, v9}, LX/G9I;->a(I)I

    move-result v8

    aput v8, v7, v11

    invoke-direct {v4, v5, v7}, LX/G9J;-><init>(LX/G9I;[I)V

    .line 2322350
    invoke-virtual {v3, v4}, LX/G9J;->b(LX/G9J;)LX/G9J;

    move-result-object v3

    .line 2322351
    iget-object v4, p0, LX/G9L;->b:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2322352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2322353
    :cond_2
    iget-object v0, p0, LX/G9L;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9J;

    move-object v0, v0

    .line 2322354
    new-array v3, v2, [I

    .line 2322355
    invoke-static {p1, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2322356
    new-instance v4, LX/G9J;

    iget-object v5, p0, LX/G9L;->a:LX/G9I;

    invoke-direct {v4, v5, v3}, LX/G9J;-><init>(LX/G9I;[I)V

    .line 2322357
    invoke-virtual {v4, p2, v6}, LX/G9J;->a(II)LX/G9J;

    move-result-object v3

    .line 2322358
    invoke-virtual {v3, v0}, LX/G9J;->c(LX/G9J;)[LX/G9J;

    move-result-object v0

    aget-object v0, v0, v6

    .line 2322359
    iget-object v3, v0, LX/G9J;->b:[I

    move-object v3, v3

    .line 2322360
    array-length v0, v3

    sub-int v4, p2, v0

    move v0, v1

    .line 2322361
    :goto_1
    if-ge v0, v4, :cond_3

    .line 2322362
    add-int v5, v2, v0

    aput v1, p1, v5

    .line 2322363
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322364
    :cond_3
    add-int v0, v2, v4

    array-length v2, v3

    invoke-static {v3, v1, p1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2322365
    return-void
.end method
