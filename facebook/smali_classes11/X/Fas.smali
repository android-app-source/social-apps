.class public LX/Fas;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ST;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/Fas;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/Fan;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fap;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Far;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FaY;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/content/Context;

.field private k:LX/Fam;

.field public l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259290
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259291
    iput-object v0, p0, LX/Fas;->f:LX/0Ot;

    .line 2259292
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259293
    iput-object v0, p0, LX/Fas;->g:LX/0Ot;

    .line 2259294
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259295
    iput-object v0, p0, LX/Fas;->h:LX/0Ot;

    .line 2259296
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259297
    iput-object v0, p0, LX/Fas;->i:LX/0Ot;

    .line 2259298
    iput-object p1, p0, LX/Fas;->j:Landroid/content/Context;

    .line 2259299
    return-void
.end method

.method public static a(LX/0QB;)LX/Fas;
    .locals 12

    .prologue
    .line 2259274
    sget-object v0, LX/Fas;->m:LX/Fas;

    if-nez v0, :cond_1

    .line 2259275
    const-class v1, LX/Fas;

    monitor-enter v1

    .line 2259276
    :try_start_0
    sget-object v0, LX/Fas;->m:LX/Fas;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2259277
    if-eqz v2, :cond_0

    .line 2259278
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2259279
    new-instance v3, LX/Fas;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v3, v4}, LX/Fas;-><init>(Landroid/content/Context;)V

    .line 2259280
    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xf9a

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xac0

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x455

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const-class v8, LX/Fan;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Fan;

    const/16 v9, 0x32f8

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x32f9

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x32f6

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p0, 0xbd2

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2259281
    iput-object v4, v3, LX/Fas;->a:LX/0Or;

    iput-object v5, v3, LX/Fas;->b:LX/0Or;

    iput-object v6, v3, LX/Fas;->c:LX/0Or;

    iput-object v7, v3, LX/Fas;->d:LX/0Or;

    iput-object v8, v3, LX/Fas;->e:LX/Fan;

    iput-object v9, v3, LX/Fas;->f:LX/0Ot;

    iput-object v10, v3, LX/Fas;->g:LX/0Ot;

    iput-object v11, v3, LX/Fas;->h:LX/0Ot;

    iput-object p0, v3, LX/Fas;->i:LX/0Ot;

    .line 2259282
    move-object v0, v3

    .line 2259283
    sput-object v0, LX/Fas;->m:LX/Fas;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2259284
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2259285
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2259286
    :cond_1
    sget-object v0, LX/Fas;->m:LX/Fas;

    return-object v0

    .line 2259287
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2259288
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Fas;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 9

    .prologue
    .line 2259259
    invoke-virtual {p0}, LX/Fas;->c()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2259260
    invoke-static {p0}, LX/Fas;->e(LX/Fas;)LX/Fam;

    move-result-object v0

    invoke-virtual {v0}, LX/Fam;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2259261
    iget-object v0, p0, LX/Fas;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    const-class v1, LX/Fb8;

    invoke-virtual {v0, p1, v1}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13G;

    .line 2259262
    if-nez v0, :cond_1

    .line 2259263
    :cond_0
    :goto_0
    return-void

    .line 2259264
    :cond_1
    iget-object v1, p0, LX/Fas;->j:Landroid/content/Context;

    invoke-interface {v0, v1}, LX/13G;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 2259265
    if-eqz v1, :cond_0

    .line 2259266
    iget-object v3, p0, LX/Fas;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-virtual {v3}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v3

    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2259267
    iget-object v3, p0, LX/Fas;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2259268
    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/7CP;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    .line 2259269
    const-wide/16 v5, 0x0

    invoke-interface {v3, v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    .line 2259270
    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2259271
    iget-object v0, p0, LX/Fas;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 2259272
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2259273
    iget-object v2, p0, LX/Fas;->j:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static c(LX/Fas;Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)Z
    .locals 9

    .prologue
    .line 2259216
    iget-object v0, p0, LX/Fas;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Far;

    .line 2259217
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2259218
    const/4 v1, 0x0

    .line 2259219
    :goto_0
    move v0, v1

    .line 2259220
    return v0

    .line 2259221
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2259222
    if-nez v1, :cond_3

    .line 2259223
    const-string v2, "Carousel configuration is null."

    invoke-static {v0, v2}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    .line 2259224
    :cond_1
    :goto_1
    move v1, v3

    .line 2259225
    if-nez v1, :cond_2

    .line 2259226
    iget-object v2, v0, LX/Far;->a:LX/03V;

    const-string v3, "SearchAwareness"

    iget-object v4, v0, LX/Far;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2259227
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, v0, LX/Far;->b:Ljava/lang/StringBuilder;

    .line 2259228
    goto :goto_0

    .line 2259229
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2259230
    const-string v2, "Carousel is missing required primary action label.\n"

    invoke-static {v0, v2}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v2, v3

    .line 2259231
    :goto_2
    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->r()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2259232
    const-string v2, "Carousel is missing required secondary action label.\n"

    invoke-static {v0, v2}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v2, v3

    .line 2259233
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2259234
    :cond_5
    const-string v2, "Carousel is missing required null state image URI(s).\n"

    invoke-static {v0, v2}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v2, v3

    .line 2259235
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v5

    invoke-virtual {v5}, LX/39O;->a()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2259236
    const-string v2, "Carousel must have at least one card.\n"

    invoke-static {v0, v2}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v2, v3

    .line 2259237
    :cond_7
    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v2

    const/4 v6, 0x1

    .line 2259238
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    move v5, v6

    :goto_3
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v8

    iget-object p0, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2259239
    if-eqz v5, :cond_8

    .line 2259240
    sget-object p1, LX/Faq;->a:[I

    const/16 v5, 0x8

    const-class v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {p0, v8, v5, v1, v2}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->ordinal()I

    move-result v5

    aget v5, p1, v5

    packed-switch v5, :pswitch_data_0

    .line 2259241
    const-string v5, "Unsupported template.\n"

    invoke-static {v0, v5}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    .line 2259242
    const/4 v5, 0x0

    :goto_4
    move v5, v5

    .line 2259243
    if-eqz v5, :cond_8

    move v5, v6

    goto :goto_3

    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    .line 2259244
    :cond_9
    move v2, v5

    .line 2259245
    if-eqz v2, :cond_1

    move v3, v4

    goto/16 :goto_1

    :cond_a
    move v2, v4

    goto/16 :goto_2

    .line 2259246
    :pswitch_0
    const/4 v1, 0x1

    const/4 p1, 0x0

    .line 2259247
    const/4 v5, 0x2

    invoke-virtual {p0, v8, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 2259248
    const-string v5, "BASIC card is missing required header.\n"

    invoke-static {v0, v5}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v5, p1

    .line 2259249
    :goto_5
    const/4 v2, 0x7

    invoke-virtual {p0, v8, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2259250
    const-string v5, "BASIC card is missing required search term.\n"

    invoke-static {v0, v5}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v5, p1

    .line 2259251
    :cond_b
    invoke-virtual {p0, v8, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2259252
    const-string v5, "BASIC card is missing required body.\n"

    invoke-static {v0, v5}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v5, p1

    .line 2259253
    :cond_c
    const/4 v2, 0x4

    invoke-virtual {p0, v8, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x3

    invoke-virtual {p0, v8, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x5

    invoke-virtual {p0, v8, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x6

    invoke-virtual {p0, v8, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2259254
    :cond_d
    const-string v5, "BASIC card is missing required image URI(s).\n"

    invoke-static {v0, v5}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    move v5, p1

    .line 2259255
    :cond_e
    invoke-virtual {p0, v8, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2259256
    const-string v5, "BASIC card is missing required background color value.\n"

    invoke-static {v0, v5}, LX/Far;->a(LX/Far;Ljava/lang/String;)V

    .line 2259257
    :goto_6
    move v5, p1

    .line 2259258
    goto/16 :goto_4

    :cond_f
    move p1, v5

    goto :goto_6

    :cond_10
    move v5, v1

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static e(LX/Fas;)LX/Fam;
    .locals 2

    .prologue
    .line 2259212
    iget-object v0, p0, LX/Fas;->k:LX/Fam;

    if-nez v0, :cond_0

    .line 2259213
    iget-object v0, p0, LX/Fas;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2259214
    iget-object v1, p0, LX/Fas;->e:LX/Fan;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, LX/Fan;->a(I)LX/Fam;

    move-result-object v0

    iput-object v0, p0, LX/Fas;->k:LX/Fam;

    .line 2259215
    :cond_0
    iget-object v0, p0, LX/Fas;->k:LX/Fam;

    return-object v0
.end method


# virtual methods
.method public final a(LX/7C4;)V
    .locals 2

    .prologue
    .line 2259209
    iget-object v0, p0, LX/Fas;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 2259210
    const-string v1, "SearchAwareness"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2259211
    return-void
.end method

.method public final a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)V
    .locals 2

    .prologue
    .line 2259201
    invoke-static {p0, p1}, LX/Fas;->c(LX/Fas;Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2259202
    :goto_0
    return-void

    .line 2259203
    :cond_0
    iput-object p1, p0, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    .line 2259204
    invoke-static {p0}, LX/Fas;->e(LX/Fas;)LX/Fam;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Fam;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)V

    .line 2259205
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_TUTORIAL_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2259206
    invoke-static {p0, v0}, LX/Fas;->a(LX/Fas;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2259207
    iget-object v0, p0, LX/Fas;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    .line 2259208
    sget v2, LX/2SU;->q:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fas;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaY;

    sget-object v2, LX/FaF;->TUTORIAL_NUX:LX/FaF;

    invoke-virtual {v0, v2}, LX/FaY;->a(LX/FaF;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
