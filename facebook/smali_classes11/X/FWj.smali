.class public LX/FWj;
.super LX/1Cd;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/FVn;

.field public final c:LX/0So;

.field public final d:LX/03V;

.field private final e:LX/FWL;

.field public final f:LX/16H;

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/FVt;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2252485
    const-class v0, LX/FWj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FWj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/FVn;LX/16H;LX/FWL;LX/0So;LX/03V;)V
    .locals 1
    .param p1    # LX/FVn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252486
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 2252487
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FWj;->h:Z

    .line 2252488
    iput-object p1, p0, LX/FWj;->b:LX/FVn;

    .line 2252489
    iput-object p2, p0, LX/FWj;->f:LX/16H;

    .line 2252490
    iput-object p3, p0, LX/FWj;->e:LX/FWL;

    .line 2252491
    iput-object p4, p0, LX/FWj;->c:LX/0So;

    .line 2252492
    iput-object p5, p0, LX/FWj;->d:LX/03V;

    .line 2252493
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/FWj;->g:Ljava/util/Map;

    .line 2252494
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2252495
    instance-of v0, p1, LX/FVt;

    if-nez v0, :cond_0

    .line 2252496
    :goto_0
    return-void

    .line 2252497
    :cond_0
    check-cast p1, LX/FVt;

    .line 2252498
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2252499
    if-eqz v1, :cond_1

    .line 2252500
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2252501
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2252502
    :cond_1
    :goto_1
    goto :goto_0

    .line 2252503
    :cond_2
    iget-object v1, p0, LX/FWj;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2252504
    iget-boolean v1, p0, LX/FWj;->h:Z

    if-eqz v1, :cond_4

    .line 2252505
    :goto_2
    goto :goto_1

    .line 2252506
    :cond_3
    iget-object v1, p0, LX/FWj;->g:Ljava/util/Map;

    iget-object v2, p0, LX/FWj;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2252507
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/FWj;->h:Z

    .line 2252508
    iget-object v1, p0, LX/FWj;->d:LX/03V;

    sget-object v2, LX/FWj;->a:Ljava/lang/String;

    const-string v3, "Saved Item %s enters the viewport twice without exiting."

    .line 2252509
    iget-object v4, p1, LX/FVt;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2252510
    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2252511
    instance-of v0, p1, LX/FVt;

    if-nez v0, :cond_0

    .line 2252512
    :goto_0
    return-void

    .line 2252513
    :cond_0
    check-cast p1, LX/FVt;

    .line 2252514
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2252515
    if-eqz v1, :cond_1

    .line 2252516
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2252517
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2252518
    :cond_1
    iget-boolean v1, p0, LX/FWj;->h:Z

    if-eqz v1, :cond_4

    .line 2252519
    :goto_1
    goto :goto_0

    .line 2252520
    :cond_2
    iget-object v1, p0, LX/FWj;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2252521
    iget-boolean v1, p0, LX/FWj;->h:Z

    if-eqz v1, :cond_5

    .line 2252522
    :goto_2
    goto :goto_1

    .line 2252523
    :cond_3
    iget-object v1, p0, LX/FWj;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 2252524
    iget-object v1, p0, LX/FWj;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v5

    .line 2252525
    iget-object v2, p0, LX/FWj;->f:LX/16H;

    .line 2252526
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2252527
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, LX/FWj;->b:LX/FVn;

    .line 2252528
    iget-object v8, v1, LX/FVn;->s:LX/0am;

    move-object v1, v8

    .line 2252529
    invoke-static {v1}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v1

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v1, v8}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    sub-long v3, v5, v3

    .line 2252530
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "saved_dashboard_saved_item_vpv"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "saved_dashboard"

    .line 2252531
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2252532
    move-object v5, v5

    .line 2252533
    const-string v6, "object_id"

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "dashboard_section_type"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "timespan_ms"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2252534
    iget-object v6, v2, LX/16H;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2252535
    iget-object v1, p0, LX/FWj;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2252536
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/FWj;->h:Z

    .line 2252537
    iget-object v1, p0, LX/FWj;->d:LX/03V;

    sget-object v2, LX/FWj;->a:Ljava/lang/String;

    const-string v3, "Saved Item %s does not have ID to be logged."

    .line 2252538
    iget-object v4, p1, LX/FVt;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2252539
    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2252540
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/FWj;->h:Z

    .line 2252541
    iget-object v1, p0, LX/FWj;->d:LX/03V;

    sget-object v2, LX/FWj;->a:Ljava/lang/String;

    const-string v3, "Saved Item %s exits the viewport without entering."

    .line 2252542
    iget-object v4, p1, LX/FVt;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2252543
    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
