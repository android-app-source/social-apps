.class public LX/Ffk;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/SearchResultsFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Ffk;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268824
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f08231e

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268825
    return-void
.end method

.method public static a(LX/0QB;)LX/Ffk;
    .locals 4

    .prologue
    .line 2268826
    sget-object v0, LX/Ffk;->a:LX/Ffk;

    if-nez v0, :cond_1

    .line 2268827
    const-class v1, LX/Ffk;

    monitor-enter v1

    .line 2268828
    :try_start_0
    sget-object v0, LX/Ffk;->a:LX/Ffk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268829
    if-eqz v2, :cond_0

    .line 2268830
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268831
    new-instance p0, LX/Ffk;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/Ffk;-><init>(Landroid/content/res/Resources;)V

    .line 2268832
    move-object v0, p0

    .line 2268833
    sput-object v0, LX/Ffk;->a:LX/Ffk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268834
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268835
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268836
    :cond_1
    sget-object v0, LX/Ffk;->a:LX/Ffk;

    return-object v0

    .line 2268837
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268838
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/Fdv;
    .locals 1

    .prologue
    .line 2268839
    const-string v0, "graph_search_results_page_videos"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsFragment;

    move-result-object v0

    return-object v0
.end method
