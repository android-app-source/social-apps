.class public final LX/FUl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public final synthetic a:LX/FUm;


# direct methods
.method public constructor <init>(LX/FUm;)V
    .locals 0

    .prologue
    .line 2249605
    iput-object p1, p0, LX/FUl;->a:LX/FUm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 13

    .prologue
    .line 2249584
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x186a1

    if-ne v0, v1, :cond_0

    .line 2249585
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    .line 2249586
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 2249587
    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 2249588
    iget-object v3, p0, LX/FUl;->a:LX/FUm;

    const/4 v8, 0x0

    .line 2249589
    new-instance v4, LX/G8y;

    move-object v5, v0

    move v6, v1

    move v7, v2

    move v9, v8

    move v10, v1

    move v11, v2

    move v12, v8

    invoke-direct/range {v4 .. v12}, LX/G8y;-><init>([BIIIIIIZ)V

    .line 2249590
    new-instance v5, LX/G8q;

    new-instance v6, LX/G9E;

    invoke-direct {v6, v4}, LX/G9E;-><init>(LX/G8w;)V

    invoke-direct {v5, v6}, LX/G8q;-><init>(LX/G8p;)V

    .line 2249591
    :try_start_0
    iget-object v4, v3, LX/FUm;->g:LX/G9N;

    .line 2249592
    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/G9N;->a(LX/G8q;Ljava/util/Map;)LX/G90;

    move-result-object v6

    move-object v4, v6

    .line 2249593
    if-nez v4, :cond_1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2249594
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2249595
    :cond_1
    :try_start_1
    iget-object v5, v3, LX/FUm;->d:LX/FUf;

    .line 2249596
    iget-object v6, v4, LX/G90;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2249597
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v7

    const-string v8, "text"

    invoke-virtual {v7, v8, v6}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v7

    .line 2249598
    iget-object v8, v5, LX/FUf;->a:LX/0if;

    sget-object v9, LX/0ig;->H:LX/0ih;

    const-string v10, "QR_CODE_SCAN_SUCCESS"

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2249599
    iget-object v5, v3, LX/FUm;->e:LX/FUW;

    .line 2249600
    iget-object v6, v4, LX/G90;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2249601
    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, LX/FUW;->b(Ljava/lang/String;Z)V

    .line 2249602
    iget-object v5, v3, LX/FUm;->i:Landroid/os/Handler;

    const v6, 0x186a1

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 2249603
    iget-object v5, v3, LX/FUm;->b:LX/0Sh;

    new-instance v6, Lcom/facebook/qrcode/thread/CameraQRDecoderThread$2;

    invoke-direct {v6, v3, v4}, Lcom/facebook/qrcode/thread/CameraQRDecoderThread$2;-><init>(LX/FUm;LX/G90;)V

    invoke-virtual {v5, v6}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2249604
    :catch_0
    goto :goto_0
.end method
