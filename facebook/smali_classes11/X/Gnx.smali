.class public abstract LX/Gnx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/11i;

.field public b:LX/0Pq;

.field public c:LX/11o;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2393869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/Gnx;)LX/11o;
    .locals 2

    .prologue
    .line 2393870
    iget-object v0, p0, LX/Gnx;->b:LX/0Pq;

    if-eqz v0, :cond_0

    .line 2393871
    iget-object v0, p0, LX/Gnx;->a:LX/11i;

    iget-object v1, p0, LX/Gnx;->b:LX/0Pq;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2393872
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2393873
    invoke-static {p0}, LX/Gnx;->b(LX/Gnx;)LX/11o;

    move-result-object v0

    iput-object v0, p0, LX/Gnx;->c:LX/11o;

    .line 2393874
    iget-object v0, p0, LX/Gnx;->c:LX/11o;

    if-nez v0, :cond_0

    .line 2393875
    iget-object v0, p0, LX/Gnx;->b:LX/0Pq;

    if-eqz v0, :cond_0

    .line 2393876
    iget-object v0, p0, LX/Gnx;->a:LX/11i;

    iget-object v1, p0, LX/Gnx;->b:LX/0Pq;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v0

    iput-object v0, p0, LX/Gnx;->c:LX/11o;

    .line 2393877
    :cond_0
    iget-object v0, p0, LX/Gnx;->c:LX/11o;

    if-eqz v0, :cond_1

    .line 2393878
    iget-object v0, p0, LX/Gnx;->c:LX/11o;

    const/4 v1, 0x0

    const v2, 0x6b3b954b

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2393879
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2393880
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Gnx;->b(Ljava/lang/String;LX/0P1;)V

    .line 2393881
    return-void
.end method

.method public final b(Ljava/lang/String;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2393882
    invoke-static {p0}, LX/Gnx;->b(LX/Gnx;)LX/11o;

    move-result-object v0

    iput-object v0, p0, LX/Gnx;->c:LX/11o;

    .line 2393883
    iget-object v0, p0, LX/Gnx;->c:LX/11o;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gnx;->c:LX/11o;

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393884
    iget-object v0, p0, LX/Gnx;->c:LX/11o;

    const/4 v1, 0x0

    const v2, -0x6bb73031

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2393885
    :cond_0
    return-void
.end method
