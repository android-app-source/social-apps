.class public LX/FEb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Uh;

.field public final c:LX/0s6;

.field private final d:Landroid/content/Context;

.field private final e:LX/FEk;

.field private final f:LX/FFJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2216543
    new-instance v0, Ljava/lang/String;

    const v1, 0x1f3ae

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sput-object v0, LX/FEb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0s6;Landroid/content/Context;LX/FEk;LX/FFJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216537
    iput-object p1, p0, LX/FEb;->b:LX/0Uh;

    .line 2216538
    iput-object p2, p0, LX/FEb;->c:LX/0s6;

    .line 2216539
    iput-object p3, p0, LX/FEb;->d:Landroid/content/Context;

    .line 2216540
    iput-object p4, p0, LX/FEb;->e:LX/FEk;

    .line 2216541
    iput-object p5, p0, LX/FEb;->f:LX/FFJ;

    .line 2216542
    return-void
.end method

.method public static b(LX/0QB;)LX/FEb;
    .locals 6

    .prologue
    .line 2216544
    new-instance v0, LX/FEb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v2

    check-cast v2, LX/0s6;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/FEk;->a(LX/0QB;)LX/FEk;

    move-result-object v4

    check-cast v4, LX/FEk;

    invoke-static {p0}, LX/FFJ;->b(LX/0QB;)LX/FFJ;

    move-result-object v5

    check-cast v5, LX/FFJ;

    invoke-direct/range {v0 .. v5}, LX/FEb;-><init>(LX/0Uh;LX/0s6;Landroid/content/Context;LX/FEk;LX/FFJ;)V

    .line 2216545
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 2216535
    iget-object v0, p0, LX/FEb;->b:LX/0Uh;

    const/16 v1, 0x15a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
