.class public LX/GnN;
.super LX/7TY;
.source ""


# instance fields
.field public c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2393694
    invoke-direct {p0, p1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2393695
    iput-object p1, p0, LX/GnN;->c:Landroid/content/Context;

    .line 2393696
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2393699
    iget-object v0, p0, LX/GnN;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2393700
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 2393701
    const v1, 0x7f030968

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2393702
    new-instance v0, LX/GnM;

    invoke-direct {v0, v1}, LX/GnM;-><init>(Landroid/view/View;)V

    .line 2393703
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/7TY;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2393704
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2393705
    if-ne v0, v1, :cond_1

    .line 2393706
    :cond_0
    :goto_0
    return-void

    .line 2393707
    :cond_1
    invoke-super {p0, p1, p2}, LX/7TY;->a(LX/1a1;I)V

    .line 2393708
    if-eq v0, v1, :cond_0

    .line 2393709
    check-cast p1, LX/7TW;

    invoke-virtual {p0, p2}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2393710
    invoke-interface {v0}, Landroid/view/MenuItem;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2393711
    iget-object v1, p1, LX/7TV;->m:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v1

    .line 2393712
    iget-object v2, p0, LX/GnN;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p2, 0x7f0a07ba

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2393713
    :cond_2
    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2393697
    if-ne p1, v0, :cond_0

    .line 2393698
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/7TY;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method
