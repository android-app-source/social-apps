.class public final LX/Fg4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fee;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2269305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2269303
    const-class v0, LX/Fg2;

    const-string v1, "No-op Data Handler Head Load."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2269304
    return-void
.end method

.method public final a(LX/7C4;)V
    .locals 2

    .prologue
    .line 2269301
    const-class v0, LX/Fg2;

    const-string v1, "No-op Data Handler Failure."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2269302
    return-void
.end method

.method public final b(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2269299
    const-class v0, LX/Fg2;

    const-string v1, "No-op Data Handler Tail Load."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2269300
    return-void
.end method
