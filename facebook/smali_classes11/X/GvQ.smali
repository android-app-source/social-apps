.class public final LX/GvQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;)V
    .locals 0

    .prologue
    .line 2405323
    iput-object p1, p0, LX/GvQ;->a:Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7ae475a5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2405324
    iget-object v1, p0, LX/GvQ;->a:Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;

    iget-object v1, v1, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->t:LX/0if;

    sget-object v2, LX/0ig;->aQ:LX/0ih;

    const-string v3, "cancel_dialog_click_outside"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2405325
    iget-object v1, p0, LX/GvQ;->a:Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;

    .line 2405326
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 2405327
    invoke-virtual {v1}, Landroid/app/Activity;->finishAfterTransition()V

    .line 2405328
    :goto_0
    iget-object v1, p0, LX/GvQ;->a:Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;

    iget-object v1, v1, Lcom/facebook/katana/gdp/LightWeightProxyAuthActivity;->t:LX/0if;

    sget-object v2, LX/0ig;->aQ:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2405329
    const v1, 0x1b74dd75

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2405330
    :cond_0
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
