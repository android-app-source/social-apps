.class public final LX/FNp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJq;


# instance fields
.field public final synthetic a:LX/FNq;


# direct methods
.method public constructor <init>(LX/FNq;)V
    .locals 0

    .prologue
    .line 2233349
    iput-object p1, p0, LX/FNp;->a:LX/FNq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2233350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cancel button was clicked but should not be shown on UI."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2233351
    iget-object v0, p0, LX/FNp;->a:LX/FNq;

    iget-object v0, v0, LX/FNq;->a:LX/FNr;

    iget-object v0, v0, LX/FNr;->f:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 2233352
    iget-object v0, p0, LX/FNp;->a:LX/FNq;

    iget-object v0, v0, LX/FNq;->a:LX/FNr;

    iget-object v0, v0, LX/FNr;->f:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2233353
    :cond_0
    return-void
.end method
