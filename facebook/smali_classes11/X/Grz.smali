.class public LX/Grz;
.super Landroid/content/BroadcastReceiver;
.source ""


# static fields
.field public static a:LX/Grz;


# instance fields
.field public b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2398979
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2398980
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/Grz;->b:Landroid/content/Context;

    .line 2398981
    return-void
.end method


# virtual methods
.method public final finalize()V
    .locals 1

    .prologue
    .line 2398992
    :try_start_0
    iget-object v0, p0, LX/Grz;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v0

    .line 2398993
    invoke-virtual {v0, p0}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2398994
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 2398995
    return-void

    .line 2398996
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x207faf81

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2398982
    const/4 v2, 0x0

    .line 2398983
    new-instance v0, LX/GR4;

    invoke-direct {v0, p1, v2, v2}, LX/GR4;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/AccessToken;)V

    move-object v2, v0

    .line 2398984
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "bf_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "event_name"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2398985
    const-string v0, "event_args"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 2398986
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2398987
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2398988
    const-string v7, "[^0-9a-zA-Z _-]"

    const-string v8, "-"

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "^[ -]*"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "[ -]*$"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2398989
    invoke-virtual {v4, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v7, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2398990
    :cond_0
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v5, v4}, LX/GR4;->a(LX/GR4;Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;Z)V

    .line 2398991
    const v0, 0x7ffd7bbc

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    return-void
.end method
