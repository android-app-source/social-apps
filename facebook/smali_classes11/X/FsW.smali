.class public LX/FsW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/FsW;


# instance fields
.field private final a:LX/0sa;

.field private final b:LX/0rq;

.field private final c:LX/0se;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0tI;

.field private final f:LX/0ad;

.field private final g:LX/0sU;

.field private final h:LX/A5q;

.field private final i:LX/0wo;

.field private final j:LX/0sX;

.field private final k:LX/0tO;

.field private final l:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0se;LX/0Or;LX/0ad;LX/0tI;LX/0sU;LX/A5q;LX/0wo;LX/0sX;LX/0tO;LX/0tQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sa;",
            "LX/0rq;",
            "LX/0se;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0ad;",
            "LX/0tI;",
            "LX/0sU;",
            "LX/A5q;",
            "LX/0wo;",
            "LX/0sX;",
            "LX/0tO;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297186
    iput-object p1, p0, LX/FsW;->a:LX/0sa;

    .line 2297187
    iput-object p2, p0, LX/FsW;->b:LX/0rq;

    .line 2297188
    iput-object p3, p0, LX/FsW;->c:LX/0se;

    .line 2297189
    iput-object p4, p0, LX/FsW;->d:LX/0Or;

    .line 2297190
    iput-object p6, p0, LX/FsW;->e:LX/0tI;

    .line 2297191
    iput-object p5, p0, LX/FsW;->f:LX/0ad;

    .line 2297192
    iput-object p7, p0, LX/FsW;->g:LX/0sU;

    .line 2297193
    iput-object p8, p0, LX/FsW;->h:LX/A5q;

    .line 2297194
    iput-object p9, p0, LX/FsW;->i:LX/0wo;

    .line 2297195
    iput-object p10, p0, LX/FsW;->j:LX/0sX;

    .line 2297196
    iput-object p11, p0, LX/FsW;->k:LX/0tO;

    .line 2297197
    iput-object p12, p0, LX/FsW;->l:LX/0tQ;

    .line 2297198
    return-void
.end method

.method public static a(LX/0QB;)LX/FsW;
    .locals 3

    .prologue
    .line 2297175
    sget-object v0, LX/FsW;->m:LX/FsW;

    if-nez v0, :cond_1

    .line 2297176
    const-class v1, LX/FsW;

    monitor-enter v1

    .line 2297177
    :try_start_0
    sget-object v0, LX/FsW;->m:LX/FsW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2297178
    if-eqz v2, :cond_0

    .line 2297179
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FsW;->b(LX/0QB;)LX/FsW;

    move-result-object v0

    sput-object v0, LX/FsW;->m:LX/FsW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297180
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2297181
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2297182
    :cond_1
    sget-object v0, LX/FsW;->m:LX/FsW;

    return-object v0

    .line 2297183
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2297184
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/FsW;
    .locals 13

    .prologue
    .line 2297173
    new-instance v0, LX/FsW;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v1

    check-cast v1, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    const/16 v4, 0x12e4

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v6

    check-cast v6, LX/0tI;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v7

    check-cast v7, LX/0sU;

    invoke-static {p0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v8

    check-cast v8, LX/A5q;

    invoke-static {p0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v9

    check-cast v9, LX/0wo;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v10

    check-cast v10, LX/0sX;

    invoke-static {p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v11

    check-cast v11, LX/0tO;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v12

    check-cast v12, LX/0tQ;

    invoke-direct/range {v0 .. v12}, LX/FsW;-><init>(LX/0sa;LX/0rq;LX/0se;LX/0Or;LX/0ad;LX/0tI;LX/0sU;LX/A5q;LX/0wo;LX/0sX;LX/0tO;LX/0tQ;)V

    .line 2297174
    return-object v0
.end method


# virtual methods
.method public final a(LX/Fso;I)LX/5xI;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2297148
    new-instance v0, LX/5xI;

    invoke-direct {v0}, LX/5xI;-><init>()V

    move-object v0, v0

    .line 2297149
    iget-object v1, p1, LX/Fso;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2297150
    const-string v1, "nodeId"

    iget-object v2, p1, LX/Fso;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2297151
    :cond_0
    iget-object v1, p1, LX/Fso;->c:LX/4a1;

    if-eqz v1, :cond_1

    .line 2297152
    const-string v1, "nodeId"

    iget-object v2, p1, LX/Fso;->c:LX/4a1;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 2297153
    :cond_1
    const-string v1, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "timeline_stories"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "angora_attachment_cover_image_size"

    iget-object v3, p0, LX/FsW;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reading_attachment_profile_image_width"

    iget-object v3, p0, LX/FsW;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reading_attachment_profile_image_height"

    iget-object v3, p0, LX/FsW;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "goodwill_small_accent_image"

    iget-object v3, p0, LX/FsW;->b:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->h()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "image_large_aspect_height"

    iget-object v3, p0, LX/FsW;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "image_large_aspect_width"

    iget-object v3, p0, LX/FsW;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "num_faceboxes_and_tags"

    iget-object v3, p0, LX/FsW;->a:LX/0sa;

    .line 2297154
    iget-object v4, v3, LX/0sa;->b:Ljava/lang/Integer;

    move-object v3, v4

    .line 2297155
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "include_replies_in_total_count"

    iget-object v3, p0, LX/FsW;->f:LX/0ad;

    sget-short v4, LX/0wg;->i:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "action_location"

    sget-object v3, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v3}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "with_actor_profile_video_playback"

    iget-object v3, p0, LX/FsW;->f:LX/0ad;

    sget-short v4, LX/0wf;->aK:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "enable_download"

    iget-object v3, p0, LX/FsW;->l:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/FsW;->j:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "skip_sample_entities_fields"

    iget-object v3, p0, LX/FsW;->f:LX/0ad;

    sget-short v4, LX/0wn;->as:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "rich_text_posts_enabled"

    iget-object v3, p0, LX/FsW;->k:LX/0tO;

    invoke-virtual {v3}, LX/0tO;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2297156
    iget-object v1, p0, LX/FsW;->d:LX/0Or;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/FsW;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    .line 2297157
    const/4 v1, 0x1

    move v1, v1

    .line 2297158
    if-eqz v1, :cond_2

    .line 2297159
    const-string v1, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2297160
    :cond_2
    iget-object v1, p1, LX/Fso;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2297161
    const-string v1, "timeline_section_after"

    iget-object v2, p1, LX/Fso;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2297162
    :cond_3
    iget-object v1, p1, LX/Fso;->e:LX/4a1;

    if-eqz v1, :cond_4

    .line 2297163
    const-string v1, "timeline_section_after"

    iget-object v2, p1, LX/Fso;->e:LX/4a1;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 2297164
    :cond_4
    iget-object v1, p1, LX/Fso;->i:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2297165
    const-string v1, "timeline_filter"

    iget-object v2, p1, LX/Fso;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2297166
    :cond_5
    iget-object v1, p0, LX/FsW;->c:LX/0se;

    iget-object v2, p0, LX/FsW;->b:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->c()LX/0wF;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2297167
    iget-object v1, p0, LX/FsW;->g:LX/0sU;

    const-string v2, "PAGE"

    invoke-virtual {v1, v0, v2}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 2297168
    const/4 v1, 0x1

    .line 2297169
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 2297170
    iget-object v1, p0, LX/FsW;->e:LX/0tI;

    invoke-virtual {v1, v0}, LX/0tI;->a(LX/0gW;)V

    .line 2297171
    invoke-static {v0}, LX/0wo;->a(LX/0gW;)V

    .line 2297172
    return-object v0
.end method
