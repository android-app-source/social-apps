.class public final LX/FCX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public downsizedHeight:I

.field public downsizedWidth:I

.field public graphAttempts:I

.field public mediaDurationMs:J

.field public final mediaSource:Ljava/lang/String;

.field public mediaType:Ljava/lang/String;

.field public final messageType:Ljava/lang/String;

.field public mimeType:Ljava/lang/String;

.field public mqttAttempts:I

.field public final numberOfSubAttachments:I

.field public outcome:LX/FCW;

.field public preparationAttempts:I

.field public sizeInBytesOfSubAttachments:I

.field public sizeInBytesOriginally:I

.field public final startTimestamp:J


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2210386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210387
    iput v0, p0, LX/FCX;->mqttAttempts:I

    .line 2210388
    iput v0, p0, LX/FCX;->graphAttempts:I

    .line 2210389
    const/4 v0, 0x1

    iput v0, p0, LX/FCX;->preparationAttempts:I

    .line 2210390
    sget-object v0, LX/FCW;->UNKNOWN:LX/FCW;

    iput-object v0, p0, LX/FCX;->outcome:LX/FCW;

    .line 2210391
    iput-wide p1, p0, LX/FCX;->startTimestamp:J

    .line 2210392
    iput-object p3, p0, LX/FCX;->messageType:Ljava/lang/String;

    .line 2210393
    iput-object p4, p0, LX/FCX;->mediaSource:Ljava/lang/String;

    .line 2210394
    iput p5, p0, LX/FCX;->numberOfSubAttachments:I

    .line 2210395
    return-void
.end method
