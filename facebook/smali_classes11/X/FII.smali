.class public LX/FII;
.super LX/FIH;
.source ""


# instance fields
.field public f:LX/FIJ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2221943
    invoke-direct {p0}, LX/FIH;-><init>()V

    return-void
.end method

.method private static a(Ljava/nio/ByteBuffer;)J
    .locals 4

    .prologue
    .line 2221938
    :try_start_0
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 2221939
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/zip/CRC32;->update([BII)V

    .line 2221940
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    const-wide/32 v2, 0x5354554e

    xor-long/2addr v0, v2

    return-wide v0

    .line 2221941
    :catch_0
    move-exception v0

    .line 2221942
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/nio/ByteBuffer;[B)[B
    .locals 4

    .prologue
    .line 2221931
    :try_start_0
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "HmacSHA1"

    invoke-direct {v0, p1, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 2221932
    const-string v1, "HmacSHA1"

    invoke-static {v1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    .line 2221933
    invoke-virtual {v1, v0}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 2221934
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Ljavax/crypto/Mac;->update([BII)V

    .line 2221935
    invoke-virtual {v1}, Ljavax/crypto/Mac;->doFinal()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2221936
    :catch_0
    move-exception v0

    .line 2221937
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()LX/FIP;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 2221897
    iget-object v0, p0, LX/FII;->f:LX/FIJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FIH;->a:[B

    if-nez v0, :cond_1

    .line 2221898
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error in trying to write stun ping data to byte buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2221899
    :cond_1
    iget-object v0, p0, LX/FIH;->a:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 2221900
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 2221901
    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 2221902
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221903
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221904
    const v1, 0x2112a442

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 2221905
    iget-object v1, p0, LX/FII;->f:LX/FIJ;

    iget v1, v1, LX/FIJ;->a:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 2221906
    iget-object v1, p0, LX/FII;->f:LX/FIJ;

    iget-wide v2, v1, LX/FIJ;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 2221907
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 2221908
    iput v1, p0, LX/FIH;->d:I

    .line 2221909
    iget-object v2, p0, LX/FII;->f:LX/FIJ;

    iget-object v2, v2, LX/FIJ;->c:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 2221910
    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221911
    array-length v3, v2

    int-to-short v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221912
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2221913
    array-length v2, v2

    rem-int/lit8 v2, v2, 0x4

    .line 2221914
    if-lez v2, :cond_2

    .line 2221915
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    rsub-int/lit8 v2, v2, 0x4

    add-int/2addr v2, v3

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2221916
    :cond_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    sub-int/2addr v2, v1

    add-int/lit8 v2, v2, 0x18

    .line 2221917
    int-to-short v2, v2

    invoke-virtual {v0, v4, v2}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    .line 2221918
    iget-object v2, p0, LX/FII;->f:LX/FIJ;

    iget-object v2, v2, LX/FIJ;->d:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-static {v0, v2}, LX/FII;->a(Ljava/nio/ByteBuffer;[B)[B

    move-result-object v2

    .line 2221919
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221920
    const/16 v3, 0x14

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221921
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2221922
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    sub-int v1, v2, v1

    add-int/lit8 v1, v1, 0x8

    .line 2221923
    int-to-short v1, v1

    invoke-virtual {v0, v4, v1}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    .line 2221924
    invoke-static {v0}, LX/FII;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    .line 2221925
    const/16 v1, -0x7fd8

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221926
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 2221927
    long-to-int v1, v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 2221928
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v1, p0, LX/FIH;->d:I

    sub-int/2addr v0, v1

    .line 2221929
    iput v0, p0, LX/FIH;->c:I

    .line 2221930
    new-instance v0, LX/FIP;

    iget-object v1, p0, LX/FIH;->a:[B

    iget v2, p0, LX/FIH;->c:I

    iget v3, p0, LX/FIH;->d:I

    invoke-direct {v0, v1, v2, v3}, LX/FIP;-><init>([BII)V

    return-object v0
.end method
