.class public final LX/F5J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/5pD;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/react/bridge/Callback;

.field public final synthetic b:LX/F5M;


# direct methods
.method public constructor <init>(LX/F5M;Lcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 2198451
    iput-object p1, p0, LX/F5J;->b:LX/F5M;

    iput-object p2, p0, LX/F5J;->a:Lcom/facebook/react/bridge/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2198452
    const/4 v3, 0x0

    .line 2198453
    iget-object v0, p0, LX/F5J;->b:LX/F5M;

    iget-object v0, v0, LX/F5M;->e:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    iget-object v1, p0, LX/F5J;->b:LX/F5M;

    iget-object v1, v1, LX/F5M;->a:LX/3Oq;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2198454
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 2198455
    move-object v0, v0

    .line 2198456
    iput-boolean v3, v0, LX/2RR;->o:Z

    .line 2198457
    move-object v0, v0

    .line 2198458
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 2198459
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 2198460
    move-object v0, v0

    .line 2198461
    iget-object v1, p0, LX/F5J;->b:LX/F5M;

    iget-object v1, v1, LX/F5M;->b:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v2

    .line 2198462
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 2198463
    if-nez v2, :cond_0

    .line 2198464
    iget-object v0, p0, LX/F5J;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2198465
    const/4 v0, 0x0

    .line 2198466
    :goto_0
    return-object v0

    .line 2198467
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v2}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2198468
    invoke-interface {v2}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2198469
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2198470
    const-string v4, "id"

    .line 2198471
    iget-object v5, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2198472
    invoke-interface {v3, v4, v5}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198473
    const-string v4, "name"

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198474
    const-string v4, "image_url"

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198475
    const-string v4, "first_name"

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198476
    invoke-interface {v1, v3}, LX/5pD;->a(LX/5pH;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2198477
    :catchall_0
    invoke-interface {v2}, LX/3On;->close()V

    move-object v0, v1

    .line 2198478
    goto :goto_0

    .line 2198479
    :cond_1
    invoke-interface {v2}, LX/3On;->close()V

    move-object v0, v1

    .line 2198480
    goto :goto_0
.end method
