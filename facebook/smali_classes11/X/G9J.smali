.class public final LX/G9J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/G9I;

.field public final b:[I


# direct methods
.method public constructor <init>(LX/G9I;[I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2322231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322232
    array-length v0, p2

    if-nez v0, :cond_0

    .line 2322233
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2322234
    :cond_0
    iput-object p1, p0, LX/G9J;->a:LX/G9I;

    .line 2322235
    array-length v2, p2

    .line 2322236
    if-le v2, v1, :cond_3

    aget v0, p2, v4

    if-nez v0, :cond_3

    move v0, v1

    .line 2322237
    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, p2, v0

    if-nez v3, :cond_1

    .line 2322238
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2322239
    :cond_1
    if-ne v0, v2, :cond_2

    .line 2322240
    new-array v0, v1, [I

    aput v4, v0, v4

    iput-object v0, p0, LX/G9J;->b:[I

    .line 2322241
    :goto_1
    return-void

    .line 2322242
    :cond_2
    sub-int v1, v2, v0

    new-array v1, v1, [I

    iput-object v1, p0, LX/G9J;->b:[I

    .line 2322243
    iget-object v1, p0, LX/G9J;->b:[I

    iget-object v2, p0, LX/G9J;->b:[I

    array-length v2, v2

    invoke-static {p2, v0, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 2322244
    :cond_3
    iput-object p2, p0, LX/G9J;->b:[I

    goto :goto_1
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 2322113
    iget-object v0, p0, LX/G9J;->b:[I

    iget-object v1, p0, LX/G9J;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, p1

    aget v0, v0, v1

    return v0
.end method

.method public final a(II)LX/G9J;
    .locals 5

    .prologue
    .line 2322219
    if-gez p1, :cond_0

    .line 2322220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2322221
    :cond_0
    if-nez p2, :cond_1

    .line 2322222
    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    .line 2322223
    iget-object v1, v0, LX/G9I;->k:LX/G9J;

    move-object v0, v1

    .line 2322224
    :goto_0
    return-object v0

    .line 2322225
    :cond_1
    iget-object v0, p0, LX/G9J;->b:[I

    array-length v1, v0

    .line 2322226
    add-int v0, v1, p1

    new-array v2, v0, [I

    .line 2322227
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 2322228
    iget-object v3, p0, LX/G9J;->a:LX/G9I;

    iget-object v4, p0, LX/G9J;->b:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4, p2}, LX/G9I;->c(II)I

    move-result v3

    aput v3, v2, v0

    .line 2322229
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322230
    :cond_2
    new-instance v0, LX/G9J;

    iget-object v1, p0, LX/G9J;->a:LX/G9I;

    invoke-direct {v0, v1, v2}, LX/G9J;-><init>(LX/G9I;[I)V

    goto :goto_0
.end method

.method public final a(LX/G9J;)LX/G9J;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2322201
    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    iget-object v1, p1, LX/G9J;->a:LX/G9I;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2322202
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GenericGFPolys do not have same GenericGF field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322203
    :cond_0
    invoke-virtual {p0}, LX/G9J;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2322204
    :goto_0
    return-object p1

    .line 2322205
    :cond_1
    invoke-virtual {p1}, LX/G9J;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object p1, p0

    .line 2322206
    goto :goto_0

    .line 2322207
    :cond_2
    iget-object v0, p0, LX/G9J;->b:[I

    .line 2322208
    iget-object v1, p1, LX/G9J;->b:[I

    .line 2322209
    array-length v2, v0

    array-length v3, v1

    if-le v2, v3, :cond_4

    .line 2322210
    :goto_1
    array-length v2, v0

    new-array v4, v2, [I

    .line 2322211
    array-length v2, v0

    array-length v3, v1

    sub-int v3, v2, v3

    .line 2322212
    invoke-static {v0, v5, v4, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v3

    .line 2322213
    :goto_2
    array-length v5, v0

    if-ge v2, v5, :cond_3

    .line 2322214
    sub-int v5, v2, v3

    aget v5, v1, v5

    aget v6, v0, v2

    .line 2322215
    xor-int v7, v5, v6

    move v5, v7

    .line 2322216
    aput v5, v4, v2

    .line 2322217
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2322218
    :cond_3
    new-instance p1, LX/G9J;

    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    invoke-direct {p1, v0, v4}, LX/G9J;-><init>(LX/G9I;[I)V

    goto :goto_0

    :cond_4
    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2322200
    iget-object v0, p0, LX/G9J;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final b(I)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2322187
    if-nez p1, :cond_1

    .line 2322188
    invoke-virtual {p0, v1}, LX/G9J;->a(I)I

    move-result v0

    .line 2322189
    :cond_0
    return v0

    .line 2322190
    :cond_1
    iget-object v2, p0, LX/G9J;->b:[I

    array-length v3, v2

    .line 2322191
    if-ne p1, v0, :cond_2

    .line 2322192
    iget-object v3, p0, LX/G9J;->b:[I

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v1, v4, :cond_0

    aget v2, v3, v1

    .line 2322193
    xor-int v5, v0, v2

    move v2, v5

    .line 2322194
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 2322195
    :cond_2
    iget-object v2, p0, LX/G9J;->b:[I

    aget v1, v2, v1

    move v5, v0

    move v0, v1

    move v1, v5

    .line 2322196
    :goto_1
    if-ge v1, v3, :cond_0

    .line 2322197
    iget-object v2, p0, LX/G9J;->a:LX/G9I;

    invoke-virtual {v2, p1, v0}, LX/G9I;->c(II)I

    move-result v0

    iget-object v2, p0, LX/G9J;->b:[I

    aget v2, v2, v1

    .line 2322198
    xor-int v4, v0, v2

    move v2, v4

    .line 2322199
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1
.end method

.method public final b(LX/G9J;)LX/G9J;
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2322166
    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    iget-object v2, p1, LX/G9J;->a:LX/G9I;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2322167
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GenericGFPolys do not have same GenericGF field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322168
    :cond_0
    invoke-virtual {p0}, LX/G9J;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, LX/G9J;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2322169
    :cond_1
    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    .line 2322170
    iget-object v1, v0, LX/G9I;->k:LX/G9J;

    move-object v0, v1

    .line 2322171
    :goto_0
    return-object v0

    .line 2322172
    :cond_2
    iget-object v3, p0, LX/G9J;->b:[I

    .line 2322173
    array-length v4, v3

    .line 2322174
    iget-object v5, p1, LX/G9J;->b:[I

    .line 2322175
    array-length v6, v5

    .line 2322176
    add-int v0, v4, v6

    add-int/lit8 v0, v0, -0x1

    new-array v7, v0, [I

    move v2, v1

    .line 2322177
    :goto_1
    if-ge v2, v4, :cond_4

    .line 2322178
    aget v8, v3, v2

    move v0, v1

    .line 2322179
    :goto_2
    if-ge v0, v6, :cond_3

    .line 2322180
    add-int v9, v2, v0

    add-int v10, v2, v0

    aget v10, v7, v10

    iget-object v11, p0, LX/G9J;->a:LX/G9I;

    aget v12, v5, v0

    .line 2322181
    invoke-virtual {v11, v8, v12}, LX/G9I;->c(II)I

    move-result v11

    .line 2322182
    xor-int v12, v10, v11

    move v10, v12

    .line 2322183
    aput v10, v7, v9

    .line 2322184
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2322185
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2322186
    :cond_4
    new-instance v0, LX/G9J;

    iget-object v1, p0, LX/G9J;->a:LX/G9I;

    invoke-direct {v0, v1, v7}, LX/G9J;-><init>(LX/G9I;[I)V

    goto :goto_0
.end method

.method public final c(I)LX/G9J;
    .locals 5

    .prologue
    .line 2322155
    if-nez p1, :cond_1

    .line 2322156
    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    .line 2322157
    iget-object v1, v0, LX/G9I;->k:LX/G9J;

    move-object p0, v1

    .line 2322158
    :cond_0
    :goto_0
    return-object p0

    .line 2322159
    :cond_1
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 2322160
    iget-object v0, p0, LX/G9J;->b:[I

    array-length v1, v0

    .line 2322161
    new-array v2, v1, [I

    .line 2322162
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 2322163
    iget-object v3, p0, LX/G9J;->a:LX/G9I;

    iget-object v4, p0, LX/G9J;->b:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4, p1}, LX/G9I;->c(II)I

    move-result v3

    aput v3, v2, v0

    .line 2322164
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322165
    :cond_2
    new-instance v0, LX/G9J;

    iget-object v1, p0, LX/G9J;->a:LX/G9I;

    invoke-direct {v0, v1, v2}, LX/G9J;-><init>(LX/G9I;[I)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2322154
    iget-object v1, p0, LX/G9J;->b:[I

    aget v1, v1, v0

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c(LX/G9J;)[LX/G9J;
    .locals 7

    .prologue
    .line 2322138
    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    iget-object v1, p1, LX/G9J;->a:LX/G9I;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2322139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GenericGFPolys do not have same GenericGF field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322140
    :cond_0
    invoke-virtual {p1}, LX/G9J;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2322141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Divide by 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322142
    :cond_1
    iget-object v0, p0, LX/G9J;->a:LX/G9I;

    .line 2322143
    iget-object v1, v0, LX/G9I;->k:LX/G9J;

    move-object v0, v1

    .line 2322144
    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v1

    invoke-virtual {p1, v1}, LX/G9J;->a(I)I

    move-result v1

    .line 2322145
    iget-object v2, p0, LX/G9J;->a:LX/G9I;

    invoke-virtual {v2, v1}, LX/G9I;->c(I)I

    move-result v2

    move-object v1, v0

    move-object v0, p0

    .line 2322146
    :goto_0
    invoke-virtual {v0}, LX/G9J;->b()I

    move-result v3

    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v4

    if-lt v3, v4, :cond_2

    invoke-virtual {v0}, LX/G9J;->c()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2322147
    invoke-virtual {v0}, LX/G9J;->b()I

    move-result v3

    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v4

    sub-int/2addr v3, v4

    .line 2322148
    iget-object v4, p0, LX/G9J;->a:LX/G9I;

    invoke-virtual {v0}, LX/G9J;->b()I

    move-result v5

    invoke-virtual {v0, v5}, LX/G9J;->a(I)I

    move-result v5

    invoke-virtual {v4, v5, v2}, LX/G9I;->c(II)I

    move-result v4

    .line 2322149
    invoke-virtual {p1, v3, v4}, LX/G9J;->a(II)LX/G9J;

    move-result-object v5

    .line 2322150
    iget-object v6, p0, LX/G9J;->a:LX/G9I;

    invoke-virtual {v6, v3, v4}, LX/G9I;->a(II)LX/G9J;

    move-result-object v3

    .line 2322151
    invoke-virtual {v1, v3}, LX/G9J;->a(LX/G9J;)LX/G9J;

    move-result-object v1

    .line 2322152
    invoke-virtual {v0, v5}, LX/G9J;->a(LX/G9J;)LX/G9J;

    move-result-object v0

    goto :goto_0

    .line 2322153
    :cond_2
    const/4 v2, 0x2

    new-array v2, v2, [LX/G9J;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    return-object v2
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2322114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, LX/G9J;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x8

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2322115
    invoke-virtual {p0}, LX/G9J;->b()I

    move-result v0

    move v1, v0

    :goto_0
    if-ltz v1, :cond_8

    .line 2322116
    invoke-virtual {p0, v1}, LX/G9J;->a(I)I

    move-result v0

    .line 2322117
    if-eqz v0, :cond_3

    .line 2322118
    if-gez v0, :cond_4

    .line 2322119
    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2322120
    neg-int v0, v0

    .line 2322121
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    if-eq v0, v4, :cond_2

    .line 2322122
    :cond_1
    iget-object v3, p0, LX/G9J;->a:LX/G9I;

    invoke-virtual {v3, v0}, LX/G9I;->b(I)I

    move-result v0

    .line 2322123
    if-nez v0, :cond_5

    .line 2322124
    const/16 v0, 0x31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322125
    :cond_2
    :goto_2
    if-eqz v1, :cond_3

    .line 2322126
    if-ne v1, v4, :cond_7

    .line 2322127
    const/16 v0, 0x78

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322128
    :cond_3
    :goto_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2322129
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 2322130
    const-string v3, " + "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2322131
    :cond_5
    if-ne v0, v4, :cond_6

    .line 2322132
    const/16 v0, 0x61

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 2322133
    :cond_6
    const-string v3, "a^"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2322134
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 2322135
    :cond_7
    const-string v0, "x^"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2322136
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2322137
    :cond_8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
