.class public final LX/FRA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/history/model/PaymentTransactions;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6zj;

.field public final synthetic b:Lcom/facebook/payments/history/model/PaymentTransactions;

.field public final synthetic c:Ljava/lang/Long;

.field public final synthetic d:LX/FRB;


# direct methods
.method public constructor <init>(LX/FRB;LX/6zj;Lcom/facebook/payments/history/model/PaymentTransactions;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2240082
    iput-object p1, p0, LX/FRA;->d:LX/FRB;

    iput-object p2, p0, LX/FRA;->a:LX/6zj;

    iput-object p3, p0, LX/FRA;->b:Lcom/facebook/payments/history/model/PaymentTransactions;

    iput-object p4, p0, LX/FRA;->c:Ljava/lang/Long;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2240080
    iget-object v0, p0, LX/FRA;->d:LX/FRB;

    iget-object v0, v0, LX/FRB;->c:LX/70k;

    new-instance v1, LX/FR9;

    invoke-direct {v1, p0}, LX/FR9;-><init>(LX/FRA;)V

    invoke-virtual {v0, v1}, LX/70k;->a(LX/1DI;)V

    .line 2240081
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2240062
    check-cast p1, Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240063
    iget-object v0, p0, LX/FRA;->d:LX/FRB;

    iget-object v0, v0, LX/FRB;->c:LX/70k;

    invoke-virtual {v0}, LX/70k;->b()V

    .line 2240064
    iget-object v0, p0, LX/FRA;->a:LX/6zj;

    .line 2240065
    new-instance v1, LX/FR8;

    invoke-direct {v1}, LX/FR8;-><init>()V

    move-object v1, v1

    .line 2240066
    iget-object v3, p0, LX/FRA;->b:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240067
    if-nez v3, :cond_0

    .line 2240068
    :goto_0
    move-object v4, p1

    .line 2240069
    move-object v2, v4

    .line 2240070
    iput-object v2, v1, LX/FR8;->a:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240071
    move-object v1, v1

    .line 2240072
    new-instance v2, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;

    invoke-direct {v2, v1}, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;-><init>(LX/FR8;)V

    move-object v1, v2

    .line 2240073
    invoke-interface {v0, v1}, LX/6zj;->a(Lcom/facebook/payments/picker/model/CoreClientData;)V

    .line 2240074
    return-void

    .line 2240075
    :cond_0
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2240076
    invoke-interface {v3}, Lcom/facebook/payments/history/model/PaymentTransactions;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2240077
    invoke-interface {p1}, Lcom/facebook/payments/history/model/PaymentTransactions;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2240078
    new-instance p0, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    invoke-interface {p1}, Lcom/facebook/payments/history/model/PaymentTransactions;->c()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;-><init>(Z)V

    .line 2240079
    new-instance p1, Lcom/facebook/payments/history/model/SimplePaymentTransactions;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-direct {p1, v4, p0}, Lcom/facebook/payments/history/model/SimplePaymentTransactions;-><init>(LX/0Px;Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;)V

    goto :goto_0
.end method
