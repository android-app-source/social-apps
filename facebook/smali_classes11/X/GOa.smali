.class public final LX/GOa;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Lcom/facebook/adspayments/protocol/CvvPrepayData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V
    .locals 0

    .prologue
    .line 2347664
    iput-object p1, p0, LX/GOa;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/adspayments/protocol/CvvPrepayData;)V
    .locals 2

    .prologue
    .line 2347665
    iget-object v0, p0, LX/GOa;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347666
    iget-object v0, p0, LX/GOa;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-virtual {p1}, Lcom/facebook/adspayments/protocol/CvvPrepayData;->c()LX/50M;

    move-result-object v1

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2347667
    iput-object v1, v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->U:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2347668
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2347669
    iget-object v0, p0, LX/GOa;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347670
    iget-object v0, p0, LX/GOa;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    const/4 v1, 0x0

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2347671
    iput-object v1, v0, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->U:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2347672
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347673
    check-cast p1, Lcom/facebook/adspayments/protocol/CvvPrepayData;

    invoke-direct {p0, p1}, LX/GOa;->a(Lcom/facebook/adspayments/protocol/CvvPrepayData;)V

    return-void
.end method
