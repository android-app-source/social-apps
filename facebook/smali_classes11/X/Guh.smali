.class public final LX/Guh;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403705
    iput-object p1, p0, LX/Guh;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403706
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403707
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 5

    .prologue
    .line 2403708
    iget-object v0, p0, LX/Guh;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403709
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403710
    const-string v2, "callback"

    invoke-interface {p2, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403711
    iput-object v1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    .line 2403712
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403713
    const-string v1, "target"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 2403714
    sget-object v2, LX/21D;->FACEWEB:LX/21D;

    const-string v3, "facewebCheckinButton"

    invoke-static {v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    new-instance v3, LX/89I;

    sget-object v4, LX/2rw;->OTHER:LX/2rw;

    invoke-direct {v3, v0, v1, v4}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v3}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2403715
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v1

    sget-object v2, LX/9jG;->CHECKIN:LX/9jG;

    .line 2403716
    iput-object v2, v1, LX/9jF;->q:LX/9jG;

    .line 2403717
    move-object v1, v1

    .line 2403718
    const/4 v2, 0x1

    .line 2403719
    iput-boolean v2, v1, LX/9jF;->k:Z

    .line 2403720
    move-object v1, v1

    .line 2403721
    iput-object v0, v1, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2403722
    move-object v0, v1

    .line 2403723
    iget-object v1, p0, LX/Guh;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Y:LX/1ay;

    iget-object v2, p0, LX/Guh;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v0

    invoke-static {v2, v0}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0xa

    iget-object v3, p0, LX/Guh;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2403724
    return-void
.end method
