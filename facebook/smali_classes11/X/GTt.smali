.class public LX/GTt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:Lcom/facebook/common/time/RealtimeSinceBootClock;

.field private c:J

.field private d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/common/time/RealtimeSinceBootClock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2356213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2356214
    iput-object p1, p0, LX/GTt;->a:LX/0Zb;

    .line 2356215
    iput-object p2, p0, LX/GTt;->b:Lcom/facebook/common/time/RealtimeSinceBootClock;

    .line 2356216
    return-void
.end method

.method public static a(LX/0QB;)LX/GTt;
    .locals 5

    .prologue
    .line 2356178
    const-class v1, LX/GTt;

    monitor-enter v1

    .line 2356179
    :try_start_0
    sget-object v0, LX/GTt;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2356180
    sput-object v2, LX/GTt;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2356181
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2356182
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2356183
    new-instance p0, LX/GTt;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-direct {p0, v3, v4}, LX/GTt;-><init>(LX/0Zb;Lcom/facebook/common/time/RealtimeSinceBootClock;)V

    .line 2356184
    move-object v0, p0

    .line 2356185
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2356186
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GTt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2356187
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2356188
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 2356209
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 2356210
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2356211
    move-object v0, v0

    .line 2356212
    const-string v1, "session_id"

    iget-wide v2, p0, LX/GTt;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    iget-object v2, p0, LX/GTt;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "surface"

    iget-object v2, p0, LX/GTt;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2356205
    const-string v0, "informational"

    iget-object v1, p0, LX/GTt;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "nearby_friends_informational_impression"

    .line 2356206
    :goto_0
    iget-object v1, p0, LX/GTt;->a:LX/0Zb;

    invoke-static {p0, v0}, LX/GTt;->a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2356207
    return-void

    .line 2356208
    :cond_0
    const-string v0, "nearby_friends_now_nux_impression"

    goto :goto_0
.end method

.method public final a(LX/GTs;)V
    .locals 4

    .prologue
    .line 2356217
    const-string v0, "informational"

    iget-object v1, p0, LX/GTt;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "nearby_friends_informational_more_info_click"

    .line 2356218
    :goto_0
    iget-object v1, p0, LX/GTt;->a:LX/0Zb;

    invoke-static {p0, v0}, LX/GTt;->a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "type"

    invoke-virtual {p1}, LX/GTs;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2356219
    return-void

    .line 2356220
    :cond_0
    const-string v0, "nearby_friends_now_nux_more_info_click"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2356201
    iget-object v0, p0, LX/GTt;->b:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/GTt;->c:J

    .line 2356202
    iput-object p1, p0, LX/GTt;->d:Ljava/lang/String;

    .line 2356203
    iput-object p2, p0, LX/GTt;->e:Ljava/lang/String;

    .line 2356204
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2356197
    const-string v0, "informational"

    iget-object v1, p0, LX/GTt;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2356198
    iget-object v0, p0, LX/GTt;->a:LX/0Zb;

    const-string v1, "nearby_friends_now_nux_turn_on"

    invoke-static {p0, v1}, LX/GTt;->a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2356199
    return-void

    .line 2356200
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2356193
    const-string v0, "informational"

    iget-object v1, p0, LX/GTt;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2356194
    iget-object v0, p0, LX/GTt;->a:LX/0Zb;

    const-string v1, "nearby_friends_now_nux_not_now"

    invoke-static {p0, v1}, LX/GTt;->a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2356195
    return-void

    .line 2356196
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2356189
    const-string v0, "informational"

    iget-object v1, p0, LX/GTt;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "nearby_friends_informational_back_out"

    .line 2356190
    :goto_0
    iget-object v1, p0, LX/GTt;->a:LX/0Zb;

    invoke-static {p0, v0}, LX/GTt;->a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2356191
    return-void

    .line 2356192
    :cond_0
    const-string v0, "nearby_friends_now_nux_back_out"

    goto :goto_0
.end method
