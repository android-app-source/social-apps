.class public final LX/GDV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic c:LX/GDY;


# direct methods
.method public constructor <init>(LX/GDY;Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 0

    .prologue
    .line 2329884
    iput-object p1, p0, LX/GDV;->c:LX/GDY;

    iput-object p2, p0, LX/GDV;->a:Landroid/content/Context;

    iput-object p3, p0, LX/GDV;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2329885
    const-class v0, LX/5oc;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GDV;->c:LX/GDY;

    invoke-virtual {v0}, LX/GDY;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2329886
    iget-object v0, p0, LX/GDV;->c:LX/GDY;

    iget-object v1, p0, LX/GDV;->a:Landroid/content/Context;

    iget-object v2, p0, LX/GDV;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0, v1, v2}, LX/GDY;->a$redex0(LX/GDY;Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2329887
    :goto_0
    return-void

    .line 2329888
    :cond_0
    iget-object v0, p0, LX/GDV;->c:LX/GDY;

    invoke-virtual {v0, p1}, LX/GDY;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2329889
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2329890
    iget-object v0, p0, LX/GDV;->c:LX/GDY;

    invoke-virtual {v0, p1}, LX/GDY;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2329891
    return-void
.end method
