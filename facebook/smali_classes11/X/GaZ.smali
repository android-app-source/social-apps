.class public final LX/GaZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

.field public final synthetic b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V
    .locals 0

    .prologue
    .line 2368335
    iput-object p1, p0, LX/GaZ;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iput-object p2, p0, LX/GaZ;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 2

    .prologue
    .line 2368343
    iget-object v0, p0, LX/GaZ;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2368344
    iget-object v0, p0, LX/GaZ;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v1, p0, LX/GaZ;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2368345
    invoke-static {v0, v1}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a$redex0(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 2368346
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2368337
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2368338
    iget-object v0, p0, LX/GaZ;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->a()V

    .line 2368339
    :goto_0
    iget-object v0, p0, LX/GaZ;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->G:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2368340
    iget-object v0, p0, LX/GaZ;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 2368341
    return-void

    .line 2368342
    :cond_0
    iget-object v0, p0, LX/GaZ;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    goto :goto_0
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2368336
    check-cast p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-direct {p0, p1}, LX/GaZ;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    return-void
.end method
