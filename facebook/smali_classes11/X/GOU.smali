.class public final LX/GOU;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Lcom/facebook/adspayments/model/Payment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/ImageView;

.field public final synthetic b:Landroid/widget/TextView;

.field public final synthetic c:Landroid/widget/TextView;

.field public final synthetic e:Lcom/facebook/adspayments/activity/PaymentStatusActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/PaymentStatusActivity;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 2347590
    iput-object p1, p0, LX/GOU;->e:Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    iput-object p2, p0, LX/GOU;->a:Landroid/widget/ImageView;

    iput-object p3, p0, LX/GOU;->b:Landroid/widget/TextView;

    iput-object p4, p0, LX/GOU;->c:Landroid/widget/TextView;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/adspayments/model/Payment;)V
    .locals 5

    .prologue
    .line 2347592
    iget-object v0, p1, Lcom/facebook/adspayments/model/Payment;->i:LX/GPV;

    .line 2347593
    sget-object v1, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->p:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2347594
    iget-object v1, p0, LX/GOU;->e:Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected payment status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Ljava/lang/Throwable;)V

    .line 2347595
    :cond_0
    sget-object v1, LX/GPV;->COMPLETED:LX/GPV;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, LX/GOU;->a(Z)V

    .line 2347596
    return-void

    .line 2347597
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 2347598
    iget-object v0, p0, LX/GOU;->e:Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347599
    iget-object v1, p0, LX/GOU;->a:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const v0, 0x7f02142a

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2347600
    iget-object v1, p0, LX/GOU;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const v0, 0x7f082807

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2347601
    iget-object v1, p0, LX/GOU;->c:Landroid/widget/TextView;

    if-eqz p1, :cond_4

    iget-object v0, p0, LX/GOU;->e:Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    iget-boolean v0, v0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->s:Z

    if-eqz v0, :cond_3

    const v0, 0x7f08280b

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2347602
    return-void

    .line 2347603
    :cond_0
    const v0, 0x7f020c2a

    goto :goto_0

    .line 2347604
    :cond_1
    iget-object v0, p0, LX/GOU;->e:Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    iget-boolean v0, v0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->s:Z

    if-eqz v0, :cond_2

    const v0, 0x7f082809

    goto :goto_1

    :cond_2
    const v0, 0x7f082808

    goto :goto_1

    .line 2347605
    :cond_3
    const v0, 0x7f08280a

    goto :goto_2

    :cond_4
    iget-object v0, p0, LX/GOU;->e:Lcom/facebook/adspayments/activity/PaymentStatusActivity;

    iget-boolean v0, v0, Lcom/facebook/adspayments/activity/PaymentStatusActivity;->s:Z

    if-eqz v0, :cond_5

    const v0, 0x7f08280d

    goto :goto_2

    :cond_5
    const v0, 0x7f08280c

    goto :goto_2
.end method


# virtual methods
.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347591
    check-cast p1, Lcom/facebook/adspayments/model/Payment;

    invoke-direct {p0, p1}, LX/GOU;->a(Lcom/facebook/adspayments/model/Payment;)V

    return-void
.end method
