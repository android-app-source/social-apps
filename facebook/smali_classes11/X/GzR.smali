.class public LX/GzR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2411873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411874
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2411875
    if-eqz p1, :cond_0

    const-string v0, "ReactRouteName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ReactURI"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2411876
    :cond_0
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v1, "LoyaltyHomeRoute"

    .line 2411877
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 2411878
    move-object v0, v0

    .line 2411879
    sget-object v1, LX/0ax;->hK:Ljava/lang/String;

    .line 2411880
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 2411881
    move-object v0, v0

    .line 2411882
    const/4 v1, 0x1

    .line 2411883
    iput v1, v0, LX/98r;->h:I

    .line 2411884
    move-object v0, v0

    .line 2411885
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 2411886
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    move-object v0, v0

    .line 2411887
    :goto_0
    return-object v0

    .line 2411888
    :cond_1
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v1, "LoyaltyHomeRoute"

    .line 2411889
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 2411890
    move-object v0, v0

    .line 2411891
    sget-object v1, LX/0ax;->hK:Ljava/lang/String;

    .line 2411892
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 2411893
    move-object v0, v0

    .line 2411894
    iput v2, v0, LX/98r;->h:I

    .line 2411895
    move-object v0, v0

    .line 2411896
    const-string v1, "non_immersive"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2411897
    iput-boolean v1, v0, LX/98r;->m:Z

    .line 2411898
    move-object v0, v0

    .line 2411899
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 2411900
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    goto :goto_0
.end method
