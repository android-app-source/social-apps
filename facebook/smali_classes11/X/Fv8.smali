.class public LX/Fv8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FwQ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FwQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300878
    iput-object p1, p0, LX/Fv8;->a:Landroid/content/Context;

    .line 2300879
    iput-object p2, p0, LX/Fv8;->b:LX/0Or;

    .line 2300880
    iput-object p3, p0, LX/Fv8;->c:LX/0Or;

    .line 2300881
    iput-object p4, p0, LX/Fv8;->d:LX/0Or;

    .line 2300882
    return-void
.end method

.method public static a(LX/0QB;)LX/Fv8;
    .locals 7

    .prologue
    .line 2300883
    const-class v1, LX/Fv8;

    monitor-enter v1

    .line 2300884
    :try_start_0
    sget-object v0, LX/Fv8;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2300885
    sput-object v2, LX/Fv8;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2300886
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300887
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2300888
    new-instance v4, LX/Fv8;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v5, 0x123

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3671

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x2a6d

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/Fv8;-><init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2300889
    move-object v0, v4

    .line 2300890
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2300891
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fv8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2300892
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2300893
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/36O;LX/5SB;LX/BQ1;)V
    .locals 8
    .param p1    # LX/36O;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2300894
    if-eqz p1, :cond_2

    .line 2300895
    const/4 v4, 0x0

    .line 2300896
    instance-of v0, p1, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2300897
    check-cast v0, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v4

    .line 2300898
    :cond_0
    iget-wide v6, p2, LX/5SB;->b:J

    move-wide v0, v6

    .line 2300899
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/36O;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/36O;->cp_()LX/1Fb;

    move-result-object v3

    invoke-interface {p1}, LX/36O;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/BQ1;->a(Ljava/lang/String;Ljava/lang/String;LX/1Fb;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2300900
    :cond_1
    :goto_0
    invoke-virtual {p3}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/Fv8;->a(LX/BQ1;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)V

    .line 2300901
    return-void

    .line 2300902
    :cond_2
    invoke-virtual {p2}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2300903
    iget-object v0, p0, LX/Fv8;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 2300904
    if-eqz v0, :cond_1

    .line 2300905
    iget-wide v6, p2, LX/5SB;->b:J

    move-wide v2, v6

    .line 2300906
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/9JZ;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1Fb;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-static {v4}, LX/BQ3;->a(Lcom/facebook/graphql/model/GraphQLFocusedPhoto;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/BQ1;->a(Ljava/lang/String;Ljava/lang/String;LX/1Fb;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0
.end method

.method public final a(LX/BQ1;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)V
    .locals 4

    .prologue
    .line 2300907
    invoke-virtual {p1, p2}, LX/BQ1;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300908
    iget-object v0, p0, LX/Fv8;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwQ;

    .line 2300909
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/FwQ;->a(LX/FwQ;I)V

    .line 2300910
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->ch_()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/Fv8;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BA0;

    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->ch_()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Fv8;->a:Landroid/content/Context;

    invoke-static {v3}, LX/BQ3;->a(Landroid/content/Context;)F

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2300911
    iget-object v2, p1, LX/BQ1;->g:LX/BPw;

    .line 2300912
    iput-object v1, v2, LX/BPw;->b:Ljava/lang/String;

    .line 2300913
    iput-object v0, v2, LX/BPw;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2300914
    :cond_0
    return-void
.end method
