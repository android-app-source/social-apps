.class public final LX/Frs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G4x;

.field public final synthetic b:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

.field public final synthetic c:LX/Frv;


# direct methods
.method public constructor <init>(LX/Frv;LX/G4x;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 0

    .prologue
    .line 2296160
    iput-object p1, p0, LX/Frs;->c:LX/Frv;

    iput-object p2, p0, LX/Frs;->a:LX/G4x;

    iput-object p3, p0, LX/Frs;->b:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2296161
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2296162
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    .line 2296163
    iget-object v0, p0, LX/Frs;->b:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    sget-object v1, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a(Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;Lcom/facebook/timeline/protocol/ResultSource;)V

    .line 2296164
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2296165
    iget-object v0, p0, LX/Frs;->a:LX/G4x;

    sget-object v1, LX/G5A;->FAILED:LX/G5A;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/G4x;->a(LX/G5A;LX/Fsp;)V

    .line 2296166
    return-void
.end method
