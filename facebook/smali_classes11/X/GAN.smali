.class public final LX/GAN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GA2;


# instance fields
.field public final synthetic a:LX/GA2;

.field public final synthetic b:LX/GAU;


# direct methods
.method public constructor <init>(LX/GAU;LX/GA2;)V
    .locals 0

    .prologue
    .line 2325047
    iput-object p1, p0, LX/GAN;->b:LX/GAU;

    iput-object p2, p0, LX/GAN;->a:LX/GA2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/GAY;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2325048
    iget-object v0, p1, LX/GAY;->b:Lorg/json/JSONObject;

    move-object v0, v0

    .line 2325049
    if-eqz v0, :cond_3

    const-string v1, "__debug__"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2325050
    :goto_0
    if-eqz v0, :cond_4

    const-string v1, "messages"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    move-object v6, v0

    .line 2325051
    :goto_1
    if-eqz v6, :cond_8

    .line 2325052
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 2325053
    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 2325054
    if-eqz v3, :cond_5

    const-string v1, "message"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2325055
    :goto_3
    if-eqz v3, :cond_6

    const-string v1, "type"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 2325056
    :goto_4
    if-eqz v3, :cond_7

    const-string v1, "link"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 2325057
    :goto_5
    if-eqz v2, :cond_2

    if-eqz v5, :cond_2

    .line 2325058
    sget-object v1, LX/GAb;->GRAPH_API_DEBUG_INFO:LX/GAb;

    .line 2325059
    const-string v7, "warning"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2325060
    sget-object v1, LX/GAb;->GRAPH_API_DEBUG_WARNING:LX/GAb;

    .line 2325061
    :cond_0
    invoke-static {v3}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2325062
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " Link: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2325063
    :cond_1
    sget-object v3, LX/GAU;->a:Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;)V

    .line 2325064
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move-object v0, v4

    .line 2325065
    goto :goto_0

    :cond_4
    move-object v6, v4

    .line 2325066
    goto :goto_1

    :cond_5
    move-object v2, v4

    .line 2325067
    goto :goto_3

    :cond_6
    move-object v5, v4

    .line 2325068
    goto :goto_4

    :cond_7
    move-object v3, v4

    .line 2325069
    goto :goto_5

    .line 2325070
    :cond_8
    iget-object v0, p0, LX/GAN;->a:LX/GA2;

    if-eqz v0, :cond_9

    .line 2325071
    iget-object v0, p0, LX/GAN;->a:LX/GA2;

    invoke-interface {v0, p1}, LX/GA2;->a(LX/GAY;)V

    .line 2325072
    :cond_9
    return-void
.end method
