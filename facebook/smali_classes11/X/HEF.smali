.class public final LX/HEF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2441804
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2441805
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2441806
    :goto_0
    return v1

    .line 2441807
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2441808
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2441809
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2441810
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 2441811
    const-string v6, "notification_status"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2441812
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2441813
    :cond_1
    const-string v6, "subscribe_status"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2441814
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 2441815
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2441816
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2441817
    if-eqz v0, :cond_4

    .line 2441818
    invoke-virtual {p1, v1, v4}, LX/186;->a(IZ)V

    .line 2441819
    :cond_4
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2441820
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2441793
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2441794
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2441795
    if-eqz v0, :cond_0

    .line 2441796
    const-string v1, "notification_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2441797
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2441798
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2441799
    if-eqz v0, :cond_1

    .line 2441800
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2441801
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2441802
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2441803
    return-void
.end method
