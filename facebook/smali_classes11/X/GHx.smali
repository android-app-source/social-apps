.class public LX/GHx;
.super LX/GHg;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:LX/8AA;


# instance fields
.field private final b:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

.field public final c:Landroid/view/View$OnClickListener;

.field public final d:Landroid/view/View$OnClickListener;

.field public final e:Landroid/view/View$OnClickListener;

.field public final f:Landroid/text/TextWatcher;

.field private final g:LX/GHv;

.field public final h:Landroid/text/TextWatcher;

.field public final i:Landroid/view/View$OnFocusChangeListener;

.field public final j:Landroid/view/View$OnFocusChangeListener;

.field public final k:LX/GKy;

.field public final l:Landroid/view/View$OnClickListener;

.field public m:Ljava/lang/String;

.field public n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

.field public p:Z

.field public q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

.field public t:LX/GHw;

.field public u:LX/GHw;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2335563
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->PAGE:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->LAUNCH_AD_IMAGE_CROPPER:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    sput-object v0, LX/GHx;->a:LX/8AA;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/adinterfaces/ui/UploadImageHelper;LX/GKy;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2335547
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2335548
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GHx;->p:Z

    .line 2335549
    iput-object p1, p0, LX/GHx;->b:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    .line 2335550
    iput-object p2, p0, LX/GHx;->k:LX/GKy;

    .line 2335551
    new-instance v0, LX/GHn;

    invoke-direct {v0, p0}, LX/GHn;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->f:Landroid/text/TextWatcher;

    .line 2335552
    sget-object v0, LX/GHw;->UNCHANGED:LX/GHw;

    iput-object v0, p0, LX/GHx;->t:LX/GHw;

    .line 2335553
    new-instance v0, LX/GHo;

    invoke-direct {v0, p0}, LX/GHo;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->i:Landroid/view/View$OnFocusChangeListener;

    .line 2335554
    new-instance v0, LX/GHp;

    invoke-direct {v0, p0}, LX/GHp;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->h:Landroid/text/TextWatcher;

    .line 2335555
    sget-object v0, LX/GHw;->UNCHANGED:LX/GHw;

    iput-object v0, p0, LX/GHx;->u:LX/GHw;

    .line 2335556
    new-instance v0, LX/GHq;

    invoke-direct {v0, p0}, LX/GHq;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->j:Landroid/view/View$OnFocusChangeListener;

    .line 2335557
    new-instance v0, LX/GHr;

    invoke-direct {v0, p0}, LX/GHr;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->c:Landroid/view/View$OnClickListener;

    .line 2335558
    new-instance v0, LX/GHs;

    invoke-direct {v0, p0}, LX/GHs;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->l:Landroid/view/View$OnClickListener;

    .line 2335559
    new-instance v0, LX/GHt;

    invoke-direct {v0, p0}, LX/GHt;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->d:Landroid/view/View$OnClickListener;

    .line 2335560
    new-instance v0, LX/GHu;

    invoke-direct {v0, p0}, LX/GHu;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->e:Landroid/view/View$OnClickListener;

    .line 2335561
    new-instance v0, LX/GHv;

    invoke-direct {v0, p0}, LX/GHv;-><init>(LX/GHx;)V

    iput-object v0, p0, LX/GHx;->g:LX/GHv;

    .line 2335562
    return-void
.end method

.method public static a(LX/0QB;)LX/GHx;
    .locals 5

    .prologue
    .line 2335541
    new-instance v2, LX/GHx;

    .line 2335542
    new-instance v4, Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v3

    check-cast v3, LX/2U3;

    invoke-direct {v4, v0, v1, v3}, Lcom/facebook/adinterfaces/ui/UploadImageHelper;-><init>(LX/0aG;LX/1Ck;LX/2U3;)V

    .line 2335543
    move-object v0, v4

    .line 2335544
    check-cast v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v1

    check-cast v1, LX/GKy;

    invoke-direct {v2, v0, v1}, LX/GHx;-><init>(Lcom/facebook/adinterfaces/ui/UploadImageHelper;LX/GKy;)V

    .line 2335545
    move-object v0, v2

    .line 2335546
    return-object v0
.end method

.method public static a$redex0(LX/GHx;Landroid/view/View;)V
    .locals 13

    .prologue
    .line 2335505
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335506
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2335507
    iget-object v1, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v8, 0x0

    .line 2335508
    const-string v6, "enter_flow"

    const-string v7, "image_picker"

    const-string v10, "creative_edit"

    const/4 v12, 0x1

    move-object v4, v0

    move-object v5, v1

    move-object v9, v8

    move-object v11, v8

    invoke-static/range {v4 .. v12}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2335509
    iget-object v0, p0, LX/GHx;->k:LX/GKy;

    invoke-virtual {v0}, LX/GKy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/GHx;->k:LX/GKy;

    iget-object v2, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335510
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v4

    .line 2335511
    sget-object v5, LX/GCF;->a:[I

    invoke-virtual {v4}, LX/8wL;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2335512
    new-instance v5, LX/GCG;

    const/16 v6, 0x4b0

    const/16 v7, 0x274

    invoke-direct {v5, v6, v7}, LX/GCG;-><init>(II)V

    move-object v5, v5

    .line 2335513
    :goto_0
    move-object v4, v5

    .line 2335514
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v5

    .line 2335515
    sget-object v6, LX/GCF;->a:[I

    invoke-virtual {v5}, LX/8wL;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 2335516
    new-instance v6, LX/GCG;

    const/16 v7, 0xfe

    const/16 v2, 0x85

    invoke-direct {v6, v7, v2}, LX/GCG;-><init>(II)V

    move-object v6, v6

    .line 2335517
    :goto_1
    move-object v5, v6

    .line 2335518
    sget-object v6, LX/0ax;->r:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2335519
    iget-object v6, v1, LX/GKy;->c:LX/17Y;

    invoke-interface {v6, v0, v7}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2335520
    if-nez v6, :cond_1

    .line 2335521
    iget-object v6, v1, LX/GKy;->b:LX/2U3;

    const-class v8, LX/GKy;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Got null intent for uri: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v8, v7}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2335522
    const/4 v6, 0x0

    .line 2335523
    :goto_2
    move-object v3, v6

    .line 2335524
    move-object v0, v3

    .line 2335525
    :goto_3
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2335526
    new-instance v2, LX/GFS;

    const/16 v3, 0x4e26

    invoke-direct {v2, v0, v3}, LX/GFS;-><init>(Landroid/content/Intent;I)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2335527
    return-void

    .line 2335528
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/GHx;->a:LX/8AA;

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_3

    .line 2335529
    :pswitch_0
    new-instance v5, LX/GCG;

    const/16 v6, 0x4b0

    const/16 v7, 0x1bc

    invoke-direct {v5, v6, v7}, LX/GCG;-><init>(II)V

    goto :goto_0

    .line 2335530
    :pswitch_1
    new-instance v6, LX/GCG;

    const/16 v7, 0x190

    const/16 v2, 0x96

    invoke-direct {v6, v7, v2}, LX/GCG;-><init>(II)V

    goto :goto_1

    .line 2335531
    :cond_1
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2335532
    const-string v8, "page"

    invoke-virtual {v7, v8, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335533
    const-string v8, "cropWidth"

    iget v9, v4, LX/GCG;->b:I

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2335534
    const-string v8, "cropHeight"

    iget v9, v4, LX/GCG;->a:I

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2335535
    const-string v8, "minCropWidth"

    iget v9, v5, LX/GCG;->b:I

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2335536
    const-string v8, "minCropHeight"

    iget v9, v5, LX/GCG;->a:I

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2335537
    const-string v8, "imageSourceCategory"

    iget-object v9, v1, LX/GKy;->d:LX/0ad;

    sget-char v10, LX/GDK;->g:C

    const-string v2, "CAMERA_ROLL"

    invoke-interface {v9, v10, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335538
    const-string v8, "callbackModule"

    const-string v9, "AdInterfacesModule"

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335539
    const-string v8, "callbackMethod"

    const-string v9, "onImageSelected"

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335540
    const-string v8, "init_props"

    invoke-virtual {v6, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2335442
    iget-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335443
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    move-object v4, v1

    .line 2335444
    iget-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335445
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2335446
    iget-object v1, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335447
    iget-object v5, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v1, v5

    .line 2335448
    if-eqz v1, :cond_0

    .line 2335449
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v5, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0018

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setMaxHeadlineLength(I)V

    .line 2335450
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    const v5, 0x7f080afc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineLabelText(Ljava/lang/Integer;)V

    .line 2335451
    :cond_0
    invoke-static {p0}, LX/GHx;->e(LX/GHx;)Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v5, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2335452
    iget v6, v5, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    move v5, v6

    .line 2335453
    if-le v1, v5, :cond_7

    move v1, v2

    .line 2335454
    :goto_0
    if-eqz v1, :cond_1

    .line 2335455
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2335456
    iget v5, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    move v1, v5

    .line 2335457
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2335458
    :cond_1
    iget-object v1, p0, LX/GHx;->m:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 2335459
    iget-object v1, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335460
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    move-object v1, v3

    .line 2335461
    iput-object v1, p0, LX/GHx;->m:Ljava/lang/String;

    .line 2335462
    :cond_2
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHx;->m:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setAdImageThumbnail(Ljava/lang/String;)V

    .line 2335463
    invoke-static {p0}, LX/GHx;->e(LX/GHx;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2335464
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHx;->f:Landroid/text/TextWatcher;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b(Landroid/text/TextWatcher;)V

    .line 2335465
    :cond_3
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHx;->h:Landroid/text/TextWatcher;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c(Landroid/text/TextWatcher;)V

    .line 2335466
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHx;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setOnImagePickerButtonClick(Landroid/view/View$OnClickListener;)V

    .line 2335467
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHx;->i:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2335468
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHx;->j:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setTextOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2335469
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHx;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setAddImageListener(Landroid/view/View$OnClickListener;)V

    .line 2335470
    invoke-static {p0}, LX/GHx;->e(LX/GHx;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2335471
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b()V

    .line 2335472
    :goto_1
    invoke-direct {p0}, LX/GHx;->i()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2335473
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GHx;->r:Ljava/util/List;

    .line 2335474
    iget-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335475
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    move-object v0, v1

    .line 2335476
    if-eqz v0, :cond_9

    .line 2335477
    iget-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335478
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    move-object v0, v1

    .line 2335479
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2335480
    iget-object v3, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->a(Ljava/lang/String;)Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    move-result-object v0

    .line 2335481
    iget-object v3, p0, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2335482
    iget-object v3, p0, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_4

    .line 2335483
    iget-object v3, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->g()V

    .line 2335484
    :cond_4
    iget-object v3, p0, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x5

    if-ne v3, v5, :cond_5

    .line 2335485
    iget-object v3, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->m()V

    .line 2335486
    :cond_5
    iget-object v3, p0, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    if-eqz v3, :cond_6

    .line 2335487
    iget-object v3, p0, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->b()V

    .line 2335488
    :cond_6
    iget-object v3, p0, LX/GHx;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2335489
    iget-object v3, p0, LX/GHx;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->setDeleteImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2335490
    iput-object v0, p0, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    .line 2335491
    invoke-static {p0}, LX/GHx;->h(LX/GHx;)V

    goto :goto_2

    :cond_7
    move v1, v3

    .line 2335492
    goto/16 :goto_0

    .line 2335493
    :cond_8
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->a()V

    .line 2335494
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineText(Ljava/lang/CharSequence;)V

    .line 2335495
    invoke-static {p0, v0}, LX/GHx;->b$redex0(LX/GHx;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2335496
    :cond_9
    iget-object v0, p0, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2335497
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->g()V

    .line 2335498
    :cond_a
    iput-boolean v2, p0, LX/GHx;->p:Z

    .line 2335499
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b()V

    .line 2335500
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->d()V

    .line 2335501
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e()V

    .line 2335502
    :cond_b
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setDescriptionText(Ljava/lang/CharSequence;)V

    .line 2335503
    invoke-static {p0, v4}, LX/GHx;->c(LX/GHx;Ljava/lang/CharSequence;)V

    .line 2335504
    return-void
.end method

.method public static b$redex0(LX/GHx;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2335436
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2335437
    iget v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    move v1, v2

    .line 2335438
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineRemainingCharacters(I)V

    .line 2335439
    iget-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2335440
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    .line 2335441
    return-void
.end method

.method public static c(LX/GHx;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2335428
    iget-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2335429
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    .line 2335430
    invoke-static {p0}, LX/GHx;->f(LX/GHx;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2335431
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2335432
    :goto_0
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2335433
    sget-object v2, LX/GG8;->PAGE_LIKE_BODY_TEXT:LX/GG8;

    invoke-virtual {v1, v2, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2335434
    :cond_0
    return-void

    .line 2335435
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/GHx;)Z
    .locals 3

    .prologue
    .line 2335422
    invoke-static {p0}, LX/GHx;->f(LX/GHx;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 2335423
    iget-object v1, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->LOCAL_AWARENESS_EDIT_CREATIVE:LX/8wL;

    if-ne v1, v2, :cond_1

    .line 2335424
    :cond_0
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2335425
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2335426
    sget-short v2, LX/GDK;->w:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 2335427
    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/GHx;)Z
    .locals 2

    .prologue
    .line 2335564
    iget-object v0, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->PAGE_LIKE_EDIT_CREATIVE:LX/8wL;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/GHx;)V
    .locals 3

    .prologue
    .line 2335410
    iget-object v0, p0, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->a()V

    .line 2335411
    iget-object v0, p0, LX/GHx;->r:Ljava/util/List;

    iget-object v1, p0, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2335412
    iget-object v1, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335413
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    move-object v1, v2

    .line 2335414
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2335415
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v1, v2}, Ljava/lang/Character;->codePointCount(Ljava/lang/CharSequence;II)I

    move-result v1

    .line 2335416
    iget-object v2, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setMultiHeadlineText(Ljava/lang/String;)V

    .line 2335417
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->i()V

    .line 2335418
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v2, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2335419
    iget p0, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->b:I

    move v2, p0

    .line 2335420
    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setMultiHeadlineRemainingCharacters(I)V

    .line 2335421
    return-void
.end method

.method private i()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2335400
    iget-object v2, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->PROMOTE_WEBSITE_EDIT_CREATIVE:LX/8wL;

    if-ne v2, v3, :cond_1

    .line 2335401
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2335402
    iget-object v3, v2, LX/GCE;->e:LX/0ad;

    move-object v2, v3

    .line 2335403
    sget-short v3, LX/GDK;->C:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2335404
    :cond_0
    :goto_0
    return v0

    .line 2335405
    :cond_1
    iget-object v2, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->PROMOTE_CTA_EDIT_CREATIVE:LX/8wL;

    if-ne v2, v3, :cond_2

    .line 2335406
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2335407
    iget-object v3, v2, LX/GCE;->e:LX/0ad;

    move-object v2, v3

    .line 2335408
    sget-short v3, LX/GDK;->B:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    .line 2335409
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2335390
    invoke-super {p0}, LX/GHg;->a()V

    .line 2335391
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v2, p0, LX/GHx;->f:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->d(Landroid/text/TextWatcher;)V

    .line 2335392
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v2, p0, LX/GHx;->h:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->e(Landroid/text/TextWatcher;)V

    .line 2335393
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->k()V

    .line 2335394
    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->j()V

    .line 2335395
    iput-object v0, p0, LX/GHx;->r:Ljava/util/List;

    .line 2335396
    iput-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2335397
    iget-object v0, p0, LX/GHx;->b:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    .line 2335398
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->d:LX/1Ck;

    sget-object v2, LX/GMf;->UPLOAD_IMAGE_TASKS:LX/GMf;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2335399
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2335387
    const-string v0, "current_creative_ad_model"

    iget-object v1, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2335388
    const-string v0, "current_photo_url"

    iget-object v1, p0, LX/GHx;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335389
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335372
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2335373
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2335374
    iput-object p1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2335375
    invoke-direct {p0}, LX/GHx;->b()V

    .line 2335376
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335377
    const/16 v1, 0x4e26

    new-instance v2, LX/GHi;

    invoke-direct {v2, p0}, LX/GHi;-><init>(LX/GHx;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2335378
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335379
    new-instance v1, LX/GHj;

    invoke-direct {v1, p0, p2}, LX/GHj;-><init>(LX/GHx;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2335380
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335381
    new-instance v1, LX/GHk;

    invoke-direct {v1, p0}, LX/GHk;-><init>(LX/GHx;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2335382
    invoke-direct {p0}, LX/GHx;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2335383
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    new-instance v1, LX/GHm;

    invoke-direct {v1, p0}, LX/GHm;-><init>(LX/GHx;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->a(Landroid/text/TextWatcher;)V

    .line 2335384
    :goto_0
    return-void

    .line 2335385
    :cond_0
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->c()V

    .line 2335386
    iget-object v0, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->f()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2335346
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335347
    iput-object p1, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335348
    iget-object v0, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335349
    iget-object p1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, p1

    .line 2335350
    iput-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335351
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 2335359
    iget-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335360
    iput-object p1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->m:Ljava/lang/String;

    .line 2335361
    iget-object v0, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335362
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335363
    iget-object v0, p0, LX/GHx;->b:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    const/4 v2, 0x1

    iget-object v1, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, LX/GHx;->g:LX/GHv;

    move-object v1, p1

    .line 2335364
    if-eqz v2, :cond_1

    .line 2335365
    iget-object v6, v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->b:LX/4At;

    if-nez v6, :cond_0

    .line 2335366
    new-instance v6, LX/4At;

    const v7, 0x7f080b09

    invoke-direct {v6, v4, v7}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v6, v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->b:LX/4At;

    .line 2335367
    :cond_0
    iget-object v6, v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->b:LX/4At;

    invoke-virtual {v6}, LX/4At;->beginShowingProgress()V

    .line 2335368
    :cond_1
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2335369
    const-string v6, "adInterfacesUploadAdImageParams"

    new-instance v7, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;

    invoke-direct {v7, v3, v1}, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2335370
    iget-object v12, v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->d:LX/1Ck;

    sget-object v13, LX/GMf;->UPLOAD_IMAGE_TASKS:LX/GMf;

    iget-object v6, v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->c:LX/0aG;

    const-string v7, "ad_interfaces_upload_ad_image"

    sget-object v9, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v10, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v11, -0x24666a11

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    new-instance v7, LX/GMd;

    invoke-direct {v7, v0, v2, v5}, LX/GMd;-><init>(Lcom/facebook/adinterfaces/ui/UploadImageHelper;ZLX/GHv;)V

    invoke-virtual {v12, v13, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2335371
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335352
    if-eqz p1, :cond_0

    .line 2335353
    const-string v0, "current_photo_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GHx;->m:Ljava/lang/String;

    .line 2335354
    const-string v0, "current_creative_ad_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;

    iput-object v0, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335355
    iget-object v0, p0, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335356
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335357
    invoke-direct {p0}, LX/GHx;->b()V

    .line 2335358
    :cond_0
    return-void
.end method
