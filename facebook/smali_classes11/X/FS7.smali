.class public LX/FS7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FS7;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/6Z0;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/6Z0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241253
    iput-object p1, p0, LX/FS7;->a:Landroid/content/ContentResolver;

    .line 2241254
    iput-object p2, p0, LX/FS7;->b:LX/6Z0;

    .line 2241255
    return-void
.end method

.method public static a(LX/0QB;)LX/FS7;
    .locals 5

    .prologue
    .line 2241256
    sget-object v0, LX/FS7;->c:LX/FS7;

    if-nez v0, :cond_1

    .line 2241257
    const-class v1, LX/FS7;

    monitor-enter v1

    .line 2241258
    :try_start_0
    sget-object v0, LX/FS7;->c:LX/FS7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2241259
    if-eqz v2, :cond_0

    .line 2241260
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2241261
    new-instance p0, LX/FS7;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/6Z0;->a(LX/0QB;)LX/6Z0;

    move-result-object v4

    check-cast v4, LX/6Z0;

    invoke-direct {p0, v3, v4}, LX/FS7;-><init>(Landroid/content/ContentResolver;LX/6Z0;)V

    .line 2241262
    move-object v0, p0

    .line 2241263
    sput-object v0, LX/FS7;->c:LX/FS7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2241264
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2241265
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2241266
    :cond_1
    sget-object v0, LX/FS7;->c:LX/FS7;

    return-object v0

    .line 2241267
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2241268
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2241269
    iget-object v0, p0, LX/FS7;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/FS7;->b:LX/6Z0;

    iget-object v1, v1, LX/6Z0;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2241270
    return-void
.end method
