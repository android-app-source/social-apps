.class public final LX/GV5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:LX/0yG;

.field public final synthetic b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;LX/0yG;)V
    .locals 0

    .prologue
    .line 2358573
    iput-object p1, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iput-object p2, p0, LX/GV5;->a:LX/0yG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2358574
    iget-object v0, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->s:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    .line 2358575
    iget-object v1, p0, LX/GV5;->a:LX/0yG;

    if-eq v0, v1, :cond_0

    .line 2358576
    iget-object v1, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->y:LX/2bj;

    const-string v2, "nearby_friends_resurrection_location_permission_request_allowed"

    invoke-virtual {v1, v2, v0}, LX/2bj;->a(Ljava/lang/String;LX/0yG;)V

    .line 2358577
    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_0

    .line 2358578
    iget-object v1, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->z:LX/GTt;

    invoke-virtual {v1}, LX/GTt;->b()V

    .line 2358579
    :cond_0
    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v0, v1, :cond_1

    .line 2358580
    iget-object v0, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->u:LX/6Zb;

    new-instance v1, LX/2si;

    invoke-direct {v1}, LX/2si;-><init>()V

    const-string v2, "surface_nearby_friends_resurrection"

    const-string v3, "mechanism_turn_on_button"

    invoke-virtual {v0, v1, v2, v3}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2358581
    :goto_0
    return-void

    .line 2358582
    :cond_1
    iget-object v0, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->finish()V

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2358583
    iget-object v0, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->y:LX/2bj;

    const-string v1, "nearby_friends_resurrection_location_permission_request_denied"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    .line 2358584
    iget-object v0, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->z:LX/GTt;

    invoke-virtual {v0}, LX/GTt;->c()V

    .line 2358585
    iget-object v0, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->finish()V

    .line 2358586
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2358587
    iget-object v0, p0, LX/GV5;->b:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->y:LX/2bj;

    const-string v1, "nearby_friends_resurrection_location_permission_request_canceled"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    .line 2358588
    return-void
.end method
