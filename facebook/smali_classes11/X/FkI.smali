.class public final LX/FkI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FkM;


# direct methods
.method public constructor <init>(LX/FkM;)V
    .locals 0

    .prologue
    .line 2278469
    iput-object p1, p0, LX/FkI;->a:LX/FkM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2278467
    iget-object v0, p0, LX/FkI;->a:LX/FkM;

    iget-object v0, v0, LX/FkM;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a()V

    .line 2278468
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2278442
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2278443
    if-eqz p1, :cond_0

    .line 2278444
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278445
    if-eqz v0, :cond_0

    .line 2278446
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278447
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2278448
    :cond_0
    iget-object v0, p0, LX/FkI;->a:LX/FkM;

    iget-object v0, v0, LX/FkM;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a()V

    .line 2278449
    :goto_0
    return-void

    .line 2278450
    :cond_1
    iget-object v0, p0, LX/FkI;->a:LX/FkM;

    iget-object v0, v0, LX/FkM;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    .line 2278451
    new-instance v2, LX/Fm9;

    invoke-direct {v2}, LX/Fm9;-><init>()V

    .line 2278452
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2278453
    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2278454
    iput-object v1, v2, LX/Fm9;->d:Ljava/lang/String;

    .line 2278455
    move-object v1, v2

    .line 2278456
    iget-object v2, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->B:Ljava/lang/String;

    .line 2278457
    iput-object v2, v1, LX/Fm9;->b:Ljava/lang/String;

    .line 2278458
    move-object v2, v1

    .line 2278459
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2278460
    check-cast v1, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreationModels$CharityQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const p0, 0x7fba55ef

    if-ne v1, p0, :cond_2

    const/4 v1, 0x1

    .line 2278461
    :goto_1
    iput-boolean v1, v2, LX/Fm9;->c:Z

    .line 2278462
    move-object v1, v2

    .line 2278463
    invoke-virtual {v1}, LX/Fm9;->a()Lcom/facebook/socialgood/model/Fundraiser;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->y:Lcom/facebook/socialgood/model/Fundraiser;

    .line 2278464
    invoke-static {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->t(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V

    .line 2278465
    goto :goto_0

    .line 2278466
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
