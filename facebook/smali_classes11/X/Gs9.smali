.class public final enum LX/Gs9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gs9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gs9;

.field public static final enum AppGroupCreate:LX/Gs9;

.field public static final enum AppGroupJoin:LX/Gs9;

.field public static final enum AppInvite:LX/Gs9;

.field public static final enum GameRequest:LX/Gs9;

.field public static final enum Like:LX/Gs9;

.field public static final enum Login:LX/Gs9;

.field public static final enum Message:LX/Gs9;

.field public static final enum Share:LX/Gs9;


# instance fields
.field private final offset:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2399059
    new-instance v0, LX/Gs9;

    const-string v1, "Login"

    invoke-direct {v0, v1, v4, v4}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->Login:LX/Gs9;

    .line 2399060
    new-instance v0, LX/Gs9;

    const-string v1, "Share"

    invoke-direct {v0, v1, v5, v5}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->Share:LX/Gs9;

    .line 2399061
    new-instance v0, LX/Gs9;

    const-string v1, "Message"

    invoke-direct {v0, v1, v6, v6}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->Message:LX/Gs9;

    .line 2399062
    new-instance v0, LX/Gs9;

    const-string v1, "Like"

    invoke-direct {v0, v1, v7, v7}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->Like:LX/Gs9;

    .line 2399063
    new-instance v0, LX/Gs9;

    const-string v1, "GameRequest"

    invoke-direct {v0, v1, v8, v8}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->GameRequest:LX/Gs9;

    .line 2399064
    new-instance v0, LX/Gs9;

    const-string v1, "AppGroupCreate"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->AppGroupCreate:LX/Gs9;

    .line 2399065
    new-instance v0, LX/Gs9;

    const-string v1, "AppGroupJoin"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->AppGroupJoin:LX/Gs9;

    .line 2399066
    new-instance v0, LX/Gs9;

    const-string v1, "AppInvite"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/Gs9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Gs9;->AppInvite:LX/Gs9;

    .line 2399067
    const/16 v0, 0x8

    new-array v0, v0, [LX/Gs9;

    sget-object v1, LX/Gs9;->Login:LX/Gs9;

    aput-object v1, v0, v4

    sget-object v1, LX/Gs9;->Share:LX/Gs9;

    aput-object v1, v0, v5

    sget-object v1, LX/Gs9;->Message:LX/Gs9;

    aput-object v1, v0, v6

    sget-object v1, LX/Gs9;->Like:LX/Gs9;

    aput-object v1, v0, v7

    sget-object v1, LX/Gs9;->GameRequest:LX/Gs9;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Gs9;->AppGroupCreate:LX/Gs9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Gs9;->AppGroupJoin:LX/Gs9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Gs9;->AppInvite:LX/Gs9;

    aput-object v2, v0, v1

    sput-object v0, LX/Gs9;->$VALUES:[LX/Gs9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2399056
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2399057
    iput p3, p0, LX/Gs9;->offset:I

    .line 2399058
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gs9;
    .locals 1

    .prologue
    .line 2399051
    const-class v0, LX/Gs9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gs9;

    return-object v0
.end method

.method public static values()[LX/Gs9;
    .locals 1

    .prologue
    .line 2399055
    sget-object v0, LX/Gs9;->$VALUES:[LX/Gs9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gs9;

    return-object v0
.end method


# virtual methods
.method public final toRequestCode()I
    .locals 2

    .prologue
    .line 2399052
    invoke-static {}, LX/Gsd;->a()V

    .line 2399053
    sget v0, LX/GAK;->n:I

    move v0, v0

    .line 2399054
    iget v1, p0, LX/Gs9;->offset:I

    add-int/2addr v0, v1

    return v0
.end method
