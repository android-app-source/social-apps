.class public final LX/GI4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GCE;

.field public final synthetic b:LX/GIA;


# direct methods
.method public constructor <init>(LX/GIA;LX/GCE;)V
    .locals 0

    .prologue
    .line 2335688
    iput-object p1, p0, LX/GI4;->b:LX/GIA;

    iput-object p2, p0, LX/GI4;->a:LX/GCE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 11

    .prologue
    .line 2335689
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 2335690
    iget-object v0, p0, LX/GI4;->b:LX/GIA;

    .line 2335691
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2335692
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2335693
    iget-object v1, p0, LX/GI4;->b:LX/GIA;

    iget-object v1, v1, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const/4 v9, 0x0

    .line 2335694
    const-string v7, "cancel_flow"

    const-string v8, "payments"

    move-object v5, v0

    move-object v6, v1

    move-object v10, v9

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2335695
    :goto_0
    return-void

    .line 2335696
    :cond_0
    iget-object v0, p0, LX/GI4;->b:LX/GIA;

    .line 2335697
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2335698
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2335699
    iget-object v1, p0, LX/GI4;->b:LX/GIA;

    iget-object v1, v1, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const/4 v9, 0x0

    .line 2335700
    const-string v7, "exit_flow"

    const-string v8, "payments"

    move-object v5, v0

    move-object v6, v1

    move-object v10, v9

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2335701
    const-string v0, "payments_flow_context_key"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2335702
    const-string v1, "credential_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2335703
    const-string v2, "cached_csc_token"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2335704
    iget-object v3, p0, LX/GI4;->a:LX/GCE;

    .line 2335705
    iput-object v1, v3, LX/GCE;->a:Ljava/lang/String;

    .line 2335706
    iget-object v1, p0, LX/GI4;->a:LX/GCE;

    .line 2335707
    iput-object v2, v1, LX/GCE;->b:Ljava/lang/String;

    .line 2335708
    iget-object v1, p0, LX/GI4;->a:LX/GCE;

    .line 2335709
    iput-object v0, v1, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2335710
    iget-object v0, p0, LX/GI4;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v1, p0, LX/GI4;->b:LX/GIA;

    iget-object v1, v1, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    .line 2335711
    iget-object v0, p0, LX/GI4;->b:LX/GIA;

    iget-object v2, v0, LX/GIA;->h:LX/GDg;

    iget-object v0, p0, LX/GI4;->b:LX/GIA;

    .line 2335712
    iget-object v3, v0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2335713
    iget-object v0, p0, LX/GI4;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v4, p0, LX/GI4;->b:LX/GIA;

    iget-object v4, v4, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4}, LX/GDg;->b(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    .line 2335714
    iget-object v0, p0, LX/GI4;->a:LX/GCE;

    new-instance v2, LX/GFT;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/GFT;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0
.end method
