.class public final LX/H13;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:LX/H14;


# direct methods
.method public constructor <init>(LX/H14;LX/4BY;)V
    .locals 0

    .prologue
    .line 2414712
    iput-object p1, p0, LX/H13;->b:LX/H14;

    iput-object p2, p0, LX/H13;->a:LX/4BY;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 2414713
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 2414714
    iget-object v0, p0, LX/H13;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2414715
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2414716
    :goto_0
    return-void

    .line 2414717
    :pswitch_0
    iget-object v0, p0, LX/H13;->b:LX/H14;

    iget-object v0, v0, LX/H14;->a:Lcom/facebook/mobileconfig/ui/MainFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    invoke-virtual {v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a()V

    .line 2414718
    iget-object v0, p0, LX/H13;->b:LX/H14;

    iget-object v0, v0, LX/H14;->a:Lcom/facebook/mobileconfig/ui/MainFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    const-string v1, "Update complete! Restart the app for changes to take effect."

    invoke-virtual {v0, v1}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    goto :goto_0

    .line 2414719
    :pswitch_1
    iget-object v0, p0, LX/H13;->b:LX/H14;

    iget-object v0, v0, LX/H14;->a:Lcom/facebook/mobileconfig/ui/MainFragment;

    iget-object v0, v0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    const-string v1, "Update failed."

    invoke-virtual {v0, v1}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
