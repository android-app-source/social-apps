.class public final LX/Gzl;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V
    .locals 0

    .prologue
    .line 2412298
    iput-object p1, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 5

    .prologue
    .line 2412299
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-boolean v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->l:Z

    if-eqz v0, :cond_0

    .line 2412300
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->g:LX/Gze;

    iget-object v1, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Gze;->b(Ljava/lang/String;)V

    .line 2412301
    :goto_0
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2412302
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-static {v0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->c$redex0(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2412303
    :goto_1
    return-void

    .line 2412304
    :cond_0
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->g:LX/Gze;

    iget-object v1, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    .line 2412305
    iget-object p1, v0, LX/Gze;->a:LX/0Zb;

    const-string p2, "get_quote_cta_admin_tap_next"

    invoke-static {p2, v1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p1, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2412306
    goto :goto_0

    .line 2412307
    :cond_1
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-boolean v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->l:Z

    if-eqz v0, :cond_2

    .line 2412308
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-static {v0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->m(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V

    goto :goto_1

    .line 2412309
    :cond_2
    iget-object v0, p0, LX/Gzl;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    .line 2412310
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    .line 2412311
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0400d6

    const v3, 0x7f0400e1

    const v4, 0x7f0400d5

    const p0, 0x7f0400e2

    invoke-virtual {v1, v2, v3, v4, p0}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v1

    .line 2412312
    iget v2, v0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v2, v2

    .line 2412313
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    .line 2412314
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2412315
    const-string p0, "arg_get_quote_cta_label"

    invoke-virtual {v4, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2412316
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p0, p0

    .line 2412317
    const-string p1, "arg_get_quote_description"

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->n:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412318
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 2412319
    const-string v0, "arg_page_id"

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412320
    const-string v0, "arg_get_quote_cta_label"

    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412321
    const-string v0, "arg_get_quote_description"

    invoke-virtual {p2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412322
    const-string v0, "arg_form_data"

    invoke-virtual {p2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2412323
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;-><init>()V

    .line 2412324
    invoke-virtual {v0, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2412325
    move-object v3, v0

    .line 2412326
    invoke-virtual {v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2412327
    goto/16 :goto_1
.end method
