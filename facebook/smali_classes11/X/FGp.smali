.class public final LX/FGp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2219558
    iput-object p1, p0, LX/FGp;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FGp;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p3, p0, LX/FGp;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    .line 2219559
    sget-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v1, "Phase-two upload failed. MediaUri=%s, Exception=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/FGp;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v4, v4, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2219560
    iget-object v0, p0, LX/FGp;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->B:LX/2N3;

    iget-object v1, p0, LX/FGp;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1, p1}, LX/2N3;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/Throwable;)V

    .line 2219561
    iget-object v0, p0, LX/FGp;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FGp;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, p0, LX/FGp;->b:Ljava/lang/String;

    .line 2219562
    iget-object v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    invoke-virtual {v5, v2}, LX/2OC;->c(Ljava/lang/String;)I

    move-result v5

    .line 2219563
    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 2219564
    iget-object v6, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    invoke-virtual {v6, v1, v2}, LX/2OC;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    .line 2219565
    :cond_0
    int-to-long v5, v5

    const-wide/16 v7, 0x32

    cmp-long v5, v5, v7

    if-gez v5, :cond_1

    .line 2219566
    iget-object v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->u:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$25;

    invoke-direct {v6, v0, v1, v2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$25;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    const-wide/16 v7, 0x1388

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v6, v7, v8, v9}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2219567
    :goto_0
    return-void

    .line 2219568
    :cond_1
    iget-object v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    invoke-virtual {v5, v2}, LX/2OC;->a(Ljava/lang/String;)V

    .line 2219569
    sget-object v5, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v6, "Failed all phase-two retry attempts with Fbid: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219570
    check-cast p1, Ljava/lang/String;

    .line 2219571
    if-eqz p1, :cond_0

    .line 2219572
    iget-object v0, p0, LX/FGp;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->B:LX/2N3;

    iget-object v1, p0, LX/FGp;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2N3;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/Throwable;)V

    .line 2219573
    iget-object v0, p0, LX/FGp;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    invoke-virtual {v0, p1}, LX/2OC;->a(Ljava/lang/String;)V

    .line 2219574
    :cond_0
    return-void
.end method
