.class public LX/FiG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fi7;


# instance fields
.field public final a:LX/FgS;

.field private final b:LX/0W9;

.field private final c:I

.field private final d:Z

.field private final e:I


# direct methods
.method public constructor <init>(LX/FgS;LX/0W9;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2274851
    iput-object p1, p0, LX/FiG;->a:LX/FgS;

    .line 2274852
    iput-object p2, p0, LX/FiG;->b:LX/0W9;

    .line 2274853
    sget v0, LX/100;->bL:I

    const/4 v1, 0x6

    invoke-interface {p3, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/FiG;->c:I

    .line 2274854
    sget-short v0, LX/100;->cg:S

    const/4 v1, 0x0

    invoke-interface {p3, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/FiG;->d:Z

    .line 2274855
    sget v0, LX/100;->ch:I

    const/4 v1, 0x4

    invoke-interface {p3, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/FiG;->e:I

    .line 2274856
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 2274857
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2274858
    iget-object v0, p2, LX/7Hc;->b:LX/0Px;

    move-object v7, v0

    .line 2274859
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v2

    move-object v1, v4

    :goto_0
    if-ge v5, v8, :cond_4

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274860
    instance-of v3, v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v3, :cond_2

    .line 2274861
    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-object v3, v0

    .line 2274862
    :goto_1
    if-eqz v3, :cond_0

    .line 2274863
    invoke-virtual {v3}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-static {v0, v3}, LX/FiB;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    .line 2274864
    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v0, v1

    .line 2274865
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v1, v0

    goto :goto_0

    .line 2274866
    :cond_2
    instance-of v3, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v3, :cond_7

    .line 2274867
    check-cast v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2274868
    if-nez v1, :cond_3

    iget-boolean v3, p0, LX/FiG;->d:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/FiG;->a:LX/FgS;

    invoke-virtual {v3}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget v9, p0, LX/FiG;->e:I

    if-lt v3, v9, :cond_3

    .line 2274869
    iget-boolean v3, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v3, v3

    .line 2274870
    if-nez v3, :cond_1

    .line 2274871
    :cond_3
    iget-object v3, p0, LX/FiG;->b:LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v0, v3}, LX/FiB;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;Ljava/util/Locale;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    .line 2274872
    :cond_4
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2274873
    if-eqz v1, :cond_5

    .line 2274874
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274875
    :cond_5
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274876
    iget v2, p0, LX/FiG;->c:I

    if-eq v1, v2, :cond_6

    .line 2274877
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274878
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2274879
    goto :goto_2

    .line 2274880
    :cond_6
    iget-object v0, p0, LX/FiG;->a:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/CwI;->ESCAPE:LX/CwI;

    invoke-static {v0, v1}, LX/CwH;->a(Ljava/lang/String;LX/CwI;)LX/CwH;

    move-result-object v0

    sget-object v1, LX/CwF;->escape:LX/CwF;

    .line 2274881
    iput-object v1, v0, LX/CwH;->g:LX/CwF;

    .line 2274882
    move-object v0, v0

    .line 2274883
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    move-object v0, v0

    .line 2274884
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274885
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_7
    move-object v3, v4

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2274886
    iget-object v0, p0, LX/FiG;->a:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    invoke-static {v0, v1}, LX/CwH;->a(Ljava/lang/String;LX/CwI;)LX/CwH;

    move-result-object v0

    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 2274887
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/Fid;)Z
    .locals 1

    .prologue
    .line 2274888
    const/4 v0, 0x0

    return v0
.end method

.method public final b()LX/CwU;
    .locals 1

    .prologue
    .line 2274889
    sget-object v0, LX/CwU;->TYPED:LX/CwU;

    return-object v0
.end method
