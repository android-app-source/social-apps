.class public final LX/FoK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V
    .locals 0

    .prologue
    .line 2289309
    iput-object p1, p0, LX/FoK;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x49488632

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2289310
    iget-object v1, p0, LX/FoK;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->c:LX/0Zb;

    iget-object v2, p0, LX/FoK;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    .line 2289311
    const-string p1, "add_profile_frame"

    invoke-static {v2, p1}, LX/BOe;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    move-object v2, p1

    .line 2289312
    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289313
    iget-object v1, p0, LX/FoK;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    .line 2289314
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->p:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2289315
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "post_donation_page_fragment_error"

    const-string p0, "Starting add profile frame flow with no overlay id."

    invoke-virtual {v2, v4, p0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289316
    :goto_0
    const v1, 0x663efbec

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2289317
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2289318
    new-instance v4, LX/5QX;

    invoke-direct {v4}, LX/5QX;-><init>()V

    iget-object p0, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->p:Ljava/lang/String;

    .line 2289319
    iput-object p0, v4, LX/5QX;->b:Ljava/lang/String;

    .line 2289320
    move-object v4, v4

    .line 2289321
    invoke-virtual {v4}, LX/5QX;->a()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v4

    .line 2289322
    new-instance p0, LX/B4K;

    const-string p1, "donation"

    invoke-direct {p0, v4, v2, p1}, LX/B4K;-><init>(LX/5QV;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, LX/B4K;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    move-result-object v4

    .line 2289323
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/B35;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    const/16 p1, 0x2769

    invoke-virtual {v2, p0, p1, v4}, LX/B35;->a(Landroid/app/Activity;ILcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V

    goto :goto_0
.end method
