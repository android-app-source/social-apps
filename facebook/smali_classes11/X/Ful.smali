.class public LX/Ful;
.super Landroid/widget/LinearLayout;
.source ""


# static fields
.field public static final a:LX/Fwd;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/View;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2300477
    new-instance v0, LX/Fwd;

    invoke-direct {v0, v2, v2, v1, v1}, LX/Fwd;-><init>(ZZZZ)V

    sput-object v0, LX/Ful;->a:LX/Fwd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2300487
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2300488
    iput-object p2, p0, LX/Ful;->b:LX/0Ot;

    .line 2300489
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Ful;->setOrientation(I)V

    .line 2300490
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, LX/Ful;->setGravity(I)V

    .line 2300491
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, LX/Ful;->setBackgroundResource(I)V

    .line 2300492
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2300493
    iput-object p1, p0, LX/Ful;->c:Landroid/view/View;

    .line 2300494
    invoke-virtual {p0, p1}, LX/Ful;->addView(Landroid/view/View;)V

    .line 2300495
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2300483
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2300484
    iget-boolean v0, p0, LX/Ful;->d:Z

    if-nez v0, :cond_0

    .line 2300485
    :goto_0
    return-void

    .line 2300486
    :cond_0
    iget-object v0, p0, LX/Ful;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyY;

    sget-object v1, LX/Ful;->a:LX/Fwd;

    invoke-virtual {v0, p0, p1, v1}, LX/FyY;->a(Landroid/view/View;Landroid/graphics/Canvas;LX/Fwd;)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2300478
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 2300479
    iget-object v0, p0, LX/Ful;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2300480
    iget-object v0, p0, LX/Ful;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2300481
    iget-object v1, p0, LX/Ful;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    invoke-virtual {p0, p1, v0}, LX/Ful;->setMeasuredDimension(II)V

    .line 2300482
    :cond_0
    return-void
.end method
