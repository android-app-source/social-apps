.class public final LX/Fqn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Z

.field public g:Landroid/view/View;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public final synthetic i:LX/Fqo;


# direct methods
.method public constructor <init>(LX/Fqo;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIII)V
    .locals 1

    .prologue
    .line 2294496
    iput-object p1, p0, LX/Fqn;->i:LX/Fqo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294497
    iput-object p2, p0, LX/Fqn;->a:Landroid/view/View;

    .line 2294498
    iput-object p3, p0, LX/Fqn;->g:Landroid/view/View;

    .line 2294499
    iput-object p4, p0, LX/Fqn;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2294500
    iput-boolean p5, p0, LX/Fqn;->f:Z

    .line 2294501
    iput p8, p0, LX/Fqn;->b:I

    .line 2294502
    iput p9, p0, LX/Fqn;->c:I

    .line 2294503
    iput p6, p0, LX/Fqn;->e:I

    .line 2294504
    iput p7, p0, LX/Fqn;->d:I

    .line 2294505
    new-instance v0, LX/Fqm;

    invoke-direct {v0, p0, p1}, LX/Fqm;-><init>(LX/Fqn;LX/Fqo;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2294506
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2294507
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v0, v0, LX/Fqo;->f:LX/Fqn;

    if-ne v0, p0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2294508
    if-eqz v0, :cond_1

    .line 2294509
    :cond_0
    :goto_1
    return-void

    .line 2294510
    :cond_1
    iget-object v0, p0, LX/Fqn;->g:Landroid/view/View;

    iget v1, p0, LX/Fqn;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2294511
    iget-object v0, p0, LX/Fqn;->h:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0217e4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 2294512
    iget-object v0, p0, LX/Fqn;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v1, v1, LX/Fqo;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a055d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2294513
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v0, v0, LX/Fqo;->f:LX/Fqn;

    if-eqz v0, :cond_2

    .line 2294514
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v0, v0, LX/Fqo;->f:LX/Fqn;

    .line 2294515
    iget-object v1, v0, LX/Fqn;->g:Landroid/view/View;

    iget v2, v0, LX/Fqn;->c:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2294516
    iget-object v1, v0, LX/Fqn;->h:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 2294517
    iget-object v1, v0, LX/Fqn;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, v0, LX/Fqn;->i:LX/Fqo;

    iget-object v2, v2, LX/Fqo;->e:Landroid/content/res/Resources;

    const v3, 0x7f0a00e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2294518
    :cond_2
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    .line 2294519
    iput-object p0, v0, LX/Fqo;->f:LX/Fqn;

    .line 2294520
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v0, v0, LX/Fqo;->c:Lcom/facebook/resources/ui/FbTextView;

    iget v1, p0, LX/Fqn;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2294521
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v0, v0, LX/Fqo;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->sendAccessibilityEvent(I)V

    .line 2294522
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v0, v0, LX/Fqo;->j:LX/EpI;

    if-eqz v0, :cond_0

    .line 2294523
    iget-object v0, p0, LX/Fqn;->i:LX/Fqo;

    iget-object v0, v0, LX/Fqo;->j:LX/EpI;

    iget-object v1, p0, LX/Fqn;->i:LX/Fqo;

    .line 2294524
    iget-object v2, v1, LX/Fqo;->f:LX/Fqn;

    iget-object v3, v1, LX/Fqo;->i:LX/Fqn;

    if-eq v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 2294525
    iget-object v2, p0, LX/Fqn;->i:LX/Fqo;

    .line 2294526
    iget-object v3, v2, LX/Fqo;->f:LX/Fqn;

    iget-object p0, v2, LX/Fqo;->g:LX/Fqn;

    if-ne v3, p0, :cond_5

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 2294527
    invoke-interface {v0, v1, v2}, LX/EpI;->a(ZZ)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    goto :goto_3
.end method
