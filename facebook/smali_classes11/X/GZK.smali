.class public LX/GZK;
.super LX/GZ5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GZ5",
        "<",
        "LX/7iz;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/7j6;

.field private final b:LX/GaG;

.field private final c:Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

.field public final d:LX/0Zb;

.field private final e:Landroid/content/Context;

.field private final f:Z

.field public g:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

.field public h:J

.field public i:I

.field public j:LX/7iP;


# direct methods
.method public constructor <init>(LX/7j6;LX/GaG;Lcom/facebook/commerce/core/ui/ProductItemViewBinder;LX/0Zb;Landroid/content/Context;ZLX/7iP;)V
    .locals 1
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/7iP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2366400
    invoke-direct {p0}, LX/GZ5;-><init>()V

    .line 2366401
    const/4 v0, 0x0

    iput v0, p0, LX/GZK;->i:I

    .line 2366402
    iput-object p1, p0, LX/GZK;->a:LX/7j6;

    .line 2366403
    iput-object p2, p0, LX/GZK;->b:LX/GaG;

    .line 2366404
    iput-object p3, p0, LX/GZK;->c:Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

    .line 2366405
    iput-object p4, p0, LX/GZK;->d:LX/0Zb;

    .line 2366406
    iput-object p5, p0, LX/GZK;->e:Landroid/content/Context;

    .line 2366407
    iput-boolean p6, p0, LX/GZK;->f:Z

    .line 2366408
    iput-object p7, p0, LX/GZK;->j:LX/7iP;

    .line 2366409
    return-void
.end method

.method private e(I)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2366367
    iget-object v0, p0, LX/GZK;->g:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GZK;->g:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 2366368
    :cond_0
    const/4 v0, 0x0

    .line 2366369
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/GZK;->g:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;->a()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2366399
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2366397
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03103d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2366398
    new-instance v1, LX/7iz;

    invoke-direct {v1, v0}, LX/7iz;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 2366373
    check-cast p1, LX/7iz;

    .line 2366374
    invoke-direct {p0, p2}, LX/GZK;->e(I)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    move-result-object v0

    iget-boolean v1, p0, LX/GZK;->f:Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2366375
    new-instance v5, LX/7j0;

    invoke-direct {v5}, LX/7j0;-><init>()V

    .line 2366376
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2366377
    iput-object v2, v5, LX/7j0;->e:Ljava/lang/String;

    .line 2366378
    move-object v2, v5

    .line 2366379
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->e()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v6

    invoke-static {v6}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v6

    .line 2366380
    iput-object v6, v2, LX/7j0;->f:Ljava/lang/String;

    .line 2366381
    if-eqz v1, :cond_0

    .line 2366382
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->b()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->PRODUCT_REJECTED:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne v2, v6, :cond_2

    move v2, v3

    .line 2366383
    :goto_0
    iput-boolean v2, v5, LX/7j0;->b:Z

    .line 2366384
    move-object v2, v5

    .line 2366385
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->b()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->PRODUCT_IN_REVIEW:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne v6, v7, :cond_3

    .line 2366386
    :goto_1
    iput-boolean v3, v2, LX/7j0;->c:Z

    .line 2366387
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->d()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->d()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2366388
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->d()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 2366389
    iput-object v2, v5, LX/7j0;->g:LX/0am;

    .line 2366390
    :cond_1
    invoke-virtual {v5}, LX/7j0;->a()LX/7j1;

    move-result-object v2

    move-object v0, v2

    .line 2366391
    invoke-static {p1, v0}, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;->a(LX/7iz;LX/7j1;)V

    .line 2366392
    invoke-direct {p0, p2}, LX/GZK;->e(I)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    move-result-object v0

    .line 2366393
    new-instance v1, LX/GZJ;

    invoke-direct {v1, p0, v0}, LX/GZJ;-><init>(LX/GZK;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;)V

    move-object v0, v1

    .line 2366394
    iget-object v1, p1, LX/7iz;->l:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2366395
    return-void

    :cond_2
    move v2, v4

    .line 2366396
    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2366372
    const/4 v0, 0x1

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2366371
    const/4 v0, 0x0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2366370
    iget v0, p0, LX/GZK;->i:I

    return v0
.end method
