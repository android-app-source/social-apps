.class public LX/FF3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216837
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/FFD;",
            ">;",
            "Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2216838
    invoke-virtual {p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 2216839
    invoke-virtual {p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2216840
    invoke-virtual {p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2216841
    invoke-virtual {p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/FF0;->a(LX/0Px;)LX/0Px;

    move-result-object v4

    .line 2216842
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 2216843
    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFH;

    .line 2216844
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    if-le v8, v6, :cond_2

    .line 2216845
    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FFH;

    move-object v5, v4

    move-object v4, v0

    .line 2216846
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2216847
    invoke-virtual {p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGameItemModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    .line 2216848
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->NEW:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    if-ne v0, v8, :cond_0

    move v0, v6

    :goto_1
    move v6, v0

    .line 2216849
    :goto_2
    new-instance v0, LX/FFE;

    invoke-direct/range {v0 .. v6}, LX/FFE;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/FFH;LX/FFH;Z)V

    .line 2216850
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2216851
    return-void

    :cond_0
    move v0, v7

    .line 2216852
    goto :goto_1

    :cond_1
    move v6, v7

    goto :goto_2

    :cond_2
    move-object v4, v0

    goto :goto_0

    :cond_3
    move-object v4, v5

    goto :goto_0
.end method
