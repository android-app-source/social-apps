.class public final LX/FHW;
.super LX/FHS;
.source ""


# instance fields
.field public final synthetic b:LX/FHX;


# direct methods
.method public constructor <init>(LX/FHX;)V
    .locals 1

    .prologue
    .line 2220423
    iput-object p1, p0, LX/FHW;->b:LX/FHX;

    invoke-direct {p0}, LX/FHS;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/File;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    .locals 12

    .prologue
    .line 2220434
    iget-object v0, p0, LX/FHW;->b:LX/FHX;

    iget-object v0, v0, LX/FHX;->b:LX/G5l;

    invoke-virtual {v0, p2}, LX/G5l;->a(Ljava/io/File;)LX/G5k;

    move-result-object v0

    .line 2220435
    iget-object v1, p0, LX/FHW;->b:LX/FHX;

    iget-object v1, v1, LX/FHX;->e:LX/1Er;

    const-string v2, "media_upload"

    iget-object v3, p0, LX/FHW;->b:LX/FHX;

    invoke-static {v3, v0}, LX/FHX;->a(LX/FHX;LX/G5k;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, LX/46h;->PREFER_SDCARD:LX/46h;

    invoke-virtual {v1, v2, v0, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 2220436
    new-instance v11, LX/FHV;

    invoke-direct {v11, p0, p3}, LX/FHV;-><init>(LX/FHW;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2220437
    new-instance v6, LX/G5p;

    iget v9, p3, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    iget v10, p3, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    move-object v7, p2

    move-object v8, v0

    invoke-direct/range {v6 .. v11}, LX/G5p;-><init>(Ljava/io/File;Ljava/io/File;IILX/60y;)V

    .line 2220438
    iget-object v7, p0, LX/FHW;->b:LX/FHX;

    iget-object v7, v7, LX/FHX;->d:LX/G5o;

    invoke-virtual {v7, v6}, LX/G5o;->a(LX/G5p;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v1, v6

    .line 2220439
    iput-object v1, p0, LX/FHW;->a:Ljava/util/concurrent/Future;

    .line 2220440
    iget-object v1, p0, LX/FHW;->b:LX/FHX;

    iget-object v1, v1, LX/FHX;->o:LX/FI1;

    iget-object v2, p0, LX/FHS;->a:Ljava/util/concurrent/Future;

    invoke-virtual {v1, p1, v2}, LX/FI1;->a(Ljava/lang/String;Ljava/util/concurrent/Future;)V

    .line 2220441
    :goto_0
    :try_start_0
    iget-object v1, p0, LX/FHS;->a:Ljava/util/concurrent/Future;

    const-wide/16 v2, 0x32

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v5, 0x16c28685

    invoke-static {v1, v2, v3, v4, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2220442
    return-object v0

    .line 2220443
    :catch_0
    iget-object v1, p0, LX/FHW;->b:LX/FHX;

    iget-object v1, v1, LX/FHX;->o:LX/FI1;

    invoke-virtual {v1, p1}, LX/FI1;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 2220444
    :catch_1
    move-exception v0

    .line 2220445
    new-instance v1, LX/7T9;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, LX/7T9;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2220446
    :catch_2
    move-exception v0

    .line 2220447
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 2220448
    new-instance v1, LX/7T9;

    invoke-direct {v1, v0}, LX/7T9;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    .locals 5

    .prologue
    .line 2220424
    iget-object v0, p2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 2220425
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220426
    iget-object v0, p0, LX/FHW;->b:LX/FHX;

    iget-object v1, v0, LX/FHX;->f:LX/2MM;

    iget-object v2, p2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    iget-object v0, p0, LX/FHW;->b:LX/FHX;

    iget-object v0, v0, LX/FHX;->r:LX/0Uh;

    const/16 v3, 0x268

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;

    move-result-object v1

    .line 2220427
    :try_start_0
    iget-object v0, v1, LX/46f;->a:Ljava/io/File;

    invoke-direct {p0, p1, v0, p2}, LX/FHW;->a(Ljava/lang/String;Ljava/io/File;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2220428
    invoke-virtual {v1}, LX/46f;->a()V

    return-object v0

    .line 2220429
    :cond_0
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    goto :goto_0

    .line 2220430
    :catch_0
    move-exception v0

    .line 2220431
    :try_start_1
    const-class v2, Ljava/lang/Exception;

    invoke-static {v0, v2}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 2220432
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2220433
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/46f;->a()V

    throw v0
.end method
