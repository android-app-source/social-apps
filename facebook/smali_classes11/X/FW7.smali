.class public final enum LX/FW7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FW7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FW7;

.field public static final enum SAVED_ITEMS_LIST:LX/FW7;


# instance fields
.field public final fragmentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final fragmentFactory:LX/FWH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/saved/fragment/SavedDashboardFragmentFactory",
            "<+",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2251556
    new-instance v0, LX/FW7;

    const-string v1, "SAVED_ITEMS_LIST"

    const-class v2, Lcom/facebook/saved/fragment/SavedItemsListFragment;

    new-instance v3, LX/FWH;

    invoke-direct {v3}, LX/FWH;-><init>()V

    invoke-direct {v0, v1, v4, v2, v3}, LX/FW7;-><init>(Ljava/lang/String;ILjava/lang/Class;LX/FWH;)V

    sput-object v0, LX/FW7;->SAVED_ITEMS_LIST:LX/FW7;

    .line 2251557
    const/4 v0, 0x1

    new-array v0, v0, [LX/FW7;

    sget-object v1, LX/FW7;->SAVED_ITEMS_LIST:LX/FW7;

    aput-object v1, v0, v4

    sput-object v0, LX/FW7;->$VALUES:[LX/FW7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;LX/FWH;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;",
            "Lcom/facebook/saved/fragment/SavedDashboardFragmentFactory",
            "<+",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2251558
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2251559
    iput-object p3, p0, LX/FW7;->fragmentClass:Ljava/lang/Class;

    .line 2251560
    iput-object p4, p0, LX/FW7;->fragmentFactory:LX/FWH;

    .line 2251561
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FW7;
    .locals 1

    .prologue
    .line 2251562
    const-class v0, LX/FW7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FW7;

    return-object v0
.end method

.method public static values()[LX/FW7;
    .locals 1

    .prologue
    .line 2251563
    sget-object v0, LX/FW7;->$VALUES:[LX/FW7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FW7;

    return-object v0
.end method


# virtual methods
.method public final getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2251564
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "saved_dashboard_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/FW7;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
