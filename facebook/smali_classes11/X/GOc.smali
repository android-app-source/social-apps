.class public final LX/GOc;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/PaymentOption;

.field public final synthetic b:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final synthetic c:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 0

    .prologue
    .line 2347682
    iput-object p1, p0, LX/GOc;->c:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    iput-object p2, p0, LX/GOc;->a:Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    iput-object p3, p0, LX/GOc;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2347683
    iget-object v0, p0, LX/GOc;->c:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    iget-object v1, p0, LX/GOc;->a:Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    iget-object v2, p0, LX/GOc;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0, v1, p1, v2}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 2347684
    return-void
.end method


# virtual methods
.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347685
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, LX/GOc;->a(Ljava/lang/String;)V

    return-void
.end method
