.class public LX/GyJ;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PlaceNode:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GyK;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GyJ",
            "<TPlaceNode;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/GyK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2409845
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2409846
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/GyJ;->b:LX/0Zi;

    .line 2409847
    iput-object p1, p0, LX/GyJ;->a:LX/0Ot;

    .line 2409848
    return-void
.end method

.method public static a(LX/0QB;)LX/GyJ;
    .locals 4

    .prologue
    .line 2409834
    const-class v1, LX/GyJ;

    monitor-enter v1

    .line 2409835
    :try_start_0
    sget-object v0, LX/GyJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2409836
    sput-object v2, LX/GyJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2409837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2409838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2409839
    new-instance v3, LX/GyJ;

    const/16 p0, 0x25ed

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GyJ;-><init>(LX/0Ot;)V

    .line 2409840
    move-object v0, v3

    .line 2409841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2409842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GyJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2409843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2409844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2409849
    invoke-static {}, LX/1dS;->b()V

    .line 2409850
    iget v0, p1, LX/BcQ;->b:I

    .line 2409851
    packed-switch v0, :pswitch_data_0

    .line 2409852
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2409853
    :pswitch_0
    check-cast p2, LX/BdG;

    .line 2409854
    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    check-cast v0, LX/BcP;

    .line 2409855
    iget-object p1, p0, LX/GyJ;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/GyK;

    .line 2409856
    iget-object p0, p1, LX/GyK;->a:LX/GyQ;

    const/4 v1, 0x0

    .line 2409857
    new-instance v3, LX/GyP;

    invoke-direct {v3, p0}, LX/GyP;-><init>(LX/GyQ;)V

    .line 2409858
    sget-object p1, LX/GyQ;->a:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/GyO;

    .line 2409859
    if-nez p1, :cond_0

    .line 2409860
    new-instance p1, LX/GyO;

    invoke-direct {p1}, LX/GyO;-><init>()V

    .line 2409861
    :cond_0
    invoke-static {p1, v0, v1, v1, v3}, LX/GyO;->a$redex0(LX/GyO;LX/1De;IILX/GyP;)V

    .line 2409862
    move-object v3, p1

    .line 2409863
    move-object v1, v3

    .line 2409864
    move-object p0, v1

    .line 2409865
    check-cast v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2409866
    iget-object v1, p0, LX/GyO;->a:LX/GyP;

    iput-object v2, v1, LX/GyP;->a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2409867
    iget-object v1, p0, LX/GyO;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2409868
    move-object p0, p0

    .line 2409869
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object p1, p0

    .line 2409870
    move-object v0, p1

    .line 2409871
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x71c6abcb
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 2

    .prologue
    .line 2409828
    check-cast p3, LX/GyI;

    .line 2409829
    iget-object v0, p0, LX/GyJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/GyI;->b:LX/2kW;

    .line 2409830
    invoke-static {p1}, LX/Bd3;->b(LX/BcP;)LX/Bd0;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Bd0;->a(LX/2kW;)LX/Bd0;

    move-result-object v1

    const/16 p0, 0xa

    invoke-virtual {v1, p0}, LX/Bd0;->b(I)LX/Bd0;

    move-result-object v1

    .line 2409831
    const p0, -0x71c6abcb

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p3, v0

    invoke-static {p1, p0, p3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p0

    move-object p0, p0

    .line 2409832
    invoke-virtual {v1, p0}, LX/Bd0;->b(LX/BcQ;)LX/Bd0;

    move-result-object v1

    invoke-static {p1}, LX/BcS;->a(LX/BcP;)LX/BcQ;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/Bd0;->c(LX/BcQ;)LX/Bd0;

    move-result-object v1

    invoke-virtual {v1}, LX/Bd0;->b()LX/BcO;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2409833
    return-void
.end method
