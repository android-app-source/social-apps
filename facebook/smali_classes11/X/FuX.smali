.class public LX/FuX;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/resources/ui/FbTextView;

.field public final b:Landroid/widget/ProgressBar;

.field public final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2300115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FuX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2300116
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2300117
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2300118
    const v0, 0x7f0314ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2300119
    const v0, 0x7f0d2f5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/FuX;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2300120
    const v0, 0x7f0d2f5e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/FuX;->b:Landroid/widget/ProgressBar;

    .line 2300121
    invoke-virtual {p0}, LX/FuX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0dc0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/FuX;->c:I

    .line 2300122
    return-void
.end method
