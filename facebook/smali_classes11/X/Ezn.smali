.class public LX/Ezn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Ezn;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187646
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ezn;->a:Ljava/util/List;

    .line 2187647
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ezn;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2187648
    return-void
.end method

.method public static a(LX/0QB;)LX/Ezn;
    .locals 3

    .prologue
    .line 2187649
    sget-object v0, LX/Ezn;->c:LX/Ezn;

    if-nez v0, :cond_1

    .line 2187650
    const-class v1, LX/Ezn;

    monitor-enter v1

    .line 2187651
    :try_start_0
    sget-object v0, LX/Ezn;->c:LX/Ezn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2187652
    if-eqz v2, :cond_0

    .line 2187653
    :try_start_1
    new-instance v0, LX/Ezn;

    invoke-direct {v0}, LX/Ezn;-><init>()V

    .line 2187654
    move-object v0, v0

    .line 2187655
    sput-object v0, LX/Ezn;->c:LX/Ezn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2187656
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2187657
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2187658
    :cond_1
    sget-object v0, LX/Ezn;->c:LX/Ezn;

    return-object v0

    .line 2187659
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2187660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
