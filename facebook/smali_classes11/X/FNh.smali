.class public LX/FNh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/FNh;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MG;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/3MQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2Uq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/3Lx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/model/messages/ParticipantInfo;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2233240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2233241
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2233242
    iput-object v0, p0, LX/FNh;->a:LX/0Ot;

    .line 2233243
    return-void
.end method

.method public static a(LX/0QB;)LX/FNh;
    .locals 10

    .prologue
    .line 2233244
    sget-object v0, LX/FNh;->i:LX/FNh;

    if-nez v0, :cond_1

    .line 2233245
    const-class v1, LX/FNh;

    monitor-enter v1

    .line 2233246
    :try_start_0
    sget-object v0, LX/FNh;->i:LX/FNh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2233247
    if-eqz v2, :cond_0

    .line 2233248
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2233249
    new-instance v3, LX/FNh;

    invoke-direct {v3}, LX/FNh;-><init>()V

    .line 2233250
    const/16 v4, 0xdad

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x12cb

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x12c7

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/3MQ;->b(LX/0QB;)LX/3MQ;

    move-result-object v7

    check-cast v7, LX/3MQ;

    invoke-static {v0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v8

    check-cast v8, LX/2Uq;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, LX/3Lx;->b(LX/0QB;)LX/3Lx;

    move-result-object p0

    check-cast p0, LX/3Lx;

    .line 2233251
    iput-object v4, v3, LX/FNh;->a:LX/0Ot;

    iput-object v5, v3, LX/FNh;->b:LX/0Or;

    iput-object v6, v3, LX/FNh;->c:LX/0Or;

    iput-object v7, v3, LX/FNh;->d:LX/3MQ;

    iput-object v8, v3, LX/FNh;->e:LX/2Uq;

    iput-object v9, v3, LX/FNh;->f:Landroid/content/res/Resources;

    iput-object p0, v3, LX/FNh;->g:LX/3Lx;

    .line 2233252
    move-object v0, v3

    .line 2233253
    sput-object v0, LX/FNh;->i:LX/FNh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2233254
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2233255
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2233256
    :cond_1
    sget-object v0, LX/FNh;->i:LX/FNh;

    return-object v0

    .line 2233257
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2233258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2233230
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v1, Lcom/facebook/user/model/UserKey;

    .line 2233231
    iget-object v2, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v2, v2

    .line 2233232
    iget-object v3, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2233233
    invoke-direct {v1, v2, v3}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v4

    .line 2233234
    iget-object v6, v4, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v4, v6

    .line 2233235
    :goto_0
    iget-object v6, p0, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v6, v6

    .line 2233236
    if-eqz v6, :cond_0

    .line 2233237
    iget-object v5, p0, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v5, v5

    .line 2233238
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2233239
    :cond_0
    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    :cond_1
    move-object v4, v5

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2233259
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2233260
    :cond_0
    :goto_0
    return v0

    .line 2233261
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2233262
    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2233225
    iget-object v0, p0, LX/FNh;->h:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-nez v0, :cond_0

    .line 2233226
    iget-object v0, p0, LX/FNh;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2233227
    iget-object v0, p0, LX/FNh;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/FNh;->a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    iput-object v0, p0, LX/FNh;->h:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2233228
    :cond_0
    :goto_0
    iget-object v0, p0, LX/FNh;->h:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    return-object v0

    .line 2233229
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/FNh;->h:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 4

    .prologue
    .line 2233218
    iget-object v0, p0, LX/FNh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MG;

    invoke-virtual {v0, p1}, LX/3MG;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2233219
    iget-object v1, p0, LX/FNh;->e:LX/2Uq;

    invoke-virtual {v1}, LX/2Uq;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2233220
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2233221
    return-object v0

    .line 2233222
    :cond_1
    iget-object v1, p0, LX/FNh;->d:LX/3MQ;

    invoke-virtual {v1, p1}, LX/3MQ;->a(Ljava/lang/String;)LX/6lI;

    move-result-object v1

    .line 2233223
    if-eqz v1, :cond_0

    iget v2, v1, LX/6lI;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 2233224
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    invoke-virtual {v2, v0}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v2

    invoke-static {v1, v2}, LX/6lI;->a(LX/6lI;LX/0XI;)LX/0XI;

    move-result-object v1

    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2233201
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2233202
    :goto_0
    return v0

    .line 2233203
    :cond_0
    iget-object v0, p0, LX/FNh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MG;

    invoke-virtual {v0, p1}, LX/3MG;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2233204
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->P()Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v2

    .line 2233205
    goto :goto_0

    .line 2233206
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v3

    .line 2233207
    if-eqz v3, :cond_2

    .line 2233208
    iget-object v4, v3, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2233209
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2233210
    iget-object v0, v3, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2233211
    invoke-static {v0}, LX/6jR;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 2233212
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v0

    .line 2233213
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2233214
    invoke-static {p1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p1}, LX/FNh;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2233215
    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2233216
    const/4 v0, 0x0

    iput-object v0, p0, LX/FNh;->h:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2233217
    return-void
.end method
