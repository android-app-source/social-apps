.class public LX/FZS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/2do;

.field public final b:LX/FZR;

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2do;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2do;",
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2257210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2257211
    new-instance v0, LX/FZR;

    invoke-direct {v0, p0}, LX/FZR;-><init>(LX/FZS;)V

    iput-object v0, p0, LX/FZS;->b:LX/FZR;

    .line 2257212
    iput-object p1, p0, LX/FZS;->a:LX/2do;

    .line 2257213
    iput-object p2, p0, LX/FZS;->c:LX/0Ot;

    .line 2257214
    return-void
.end method

.method public static a(LX/0QB;)LX/FZS;
    .locals 5

    .prologue
    .line 2257215
    const-class v1, LX/FZS;

    monitor-enter v1

    .line 2257216
    :try_start_0
    sget-object v0, LX/FZS;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2257217
    sput-object v2, LX/FZS;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2257218
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2257219
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2257220
    new-instance v4, LX/FZS;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v3

    check-cast v3, LX/2do;

    const/16 p0, 0x113c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/FZS;-><init>(LX/2do;LX/0Ot;)V

    .line 2257221
    move-object v0, v4

    .line 2257222
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2257223
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FZS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2257224
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2257225
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
