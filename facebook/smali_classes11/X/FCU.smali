.class public final enum LX/FCU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FCU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FCU;

.field public static final enum FAILURE_PERMANENT:LX/FCU;

.field public static final enum FAILURE_RETRYABLE:LX/FCU;

.field public static final enum SUCCESS_GRAPH:LX/FCU;

.field public static final enum SUCCESS_MQTT:LX/FCU;

.field public static final enum UNKNOWN:LX/FCU;


# instance fields
.field public final rawValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2210361
    new-instance v0, LX/FCU;

    const-string v1, "UNKNOWN"

    const-string v2, "u"

    invoke-direct {v0, v1, v3, v2}, LX/FCU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCU;->UNKNOWN:LX/FCU;

    .line 2210362
    new-instance v0, LX/FCU;

    const-string v1, "SUCCESS_MQTT"

    const-string v2, "m"

    invoke-direct {v0, v1, v4, v2}, LX/FCU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCU;->SUCCESS_MQTT:LX/FCU;

    .line 2210363
    new-instance v0, LX/FCU;

    const-string v1, "SUCCESS_GRAPH"

    const-string v2, "g"

    invoke-direct {v0, v1, v5, v2}, LX/FCU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCU;->SUCCESS_GRAPH:LX/FCU;

    .line 2210364
    new-instance v0, LX/FCU;

    const-string v1, "FAILURE_RETRYABLE"

    const-string v2, "f"

    invoke-direct {v0, v1, v6, v2}, LX/FCU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCU;->FAILURE_RETRYABLE:LX/FCU;

    .line 2210365
    new-instance v0, LX/FCU;

    const-string v1, "FAILURE_PERMANENT"

    const-string v2, "p"

    invoke-direct {v0, v1, v7, v2}, LX/FCU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCU;->FAILURE_PERMANENT:LX/FCU;

    .line 2210366
    const/4 v0, 0x5

    new-array v0, v0, [LX/FCU;

    sget-object v1, LX/FCU;->UNKNOWN:LX/FCU;

    aput-object v1, v0, v3

    sget-object v1, LX/FCU;->SUCCESS_MQTT:LX/FCU;

    aput-object v1, v0, v4

    sget-object v1, LX/FCU;->SUCCESS_GRAPH:LX/FCU;

    aput-object v1, v0, v5

    sget-object v1, LX/FCU;->FAILURE_RETRYABLE:LX/FCU;

    aput-object v1, v0, v6

    sget-object v1, LX/FCU;->FAILURE_PERMANENT:LX/FCU;

    aput-object v1, v0, v7

    sput-object v0, LX/FCU;->$VALUES:[LX/FCU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2210356
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2210357
    iput-object p3, p0, LX/FCU;->rawValue:Ljava/lang/String;

    .line 2210358
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FCU;
    .locals 1

    .prologue
    .line 2210360
    const-class v0, LX/FCU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FCU;

    return-object v0
.end method

.method public static values()[LX/FCU;
    .locals 1

    .prologue
    .line 2210359
    sget-object v0, LX/FCU;->$VALUES:[LX/FCU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCU;

    return-object v0
.end method
