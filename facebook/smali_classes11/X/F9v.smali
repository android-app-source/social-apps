.class public final LX/F9v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/FA5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F9x;


# direct methods
.method public constructor <init>(LX/F9x;)V
    .locals 0

    .prologue
    .line 2206319
    iput-object p1, p0, LX/F9v;->a:LX/F9x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2206320
    iget-object v0, p0, LX/F9v;->a:LX/F9x;

    const/4 v1, 0x0

    .line 2206321
    iput-boolean v1, v0, LX/F9x;->s:Z

    .line 2206322
    iget-object v0, p0, LX/F9v;->a:LX/F9x;

    sget-object v1, LX/4cd;->CONNECTION_CHECK_ERROR:LX/4cd;

    invoke-static {v0, v1}, LX/F9x;->a$redex0(LX/F9x;LX/4cd;)V

    .line 2206323
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2206324
    check-cast p1, LX/FA5;

    .line 2206325
    iget-object v0, p0, LX/F9v;->a:LX/F9x;

    const/4 v1, 0x0

    .line 2206326
    iput-boolean v1, v0, LX/F9x;->s:Z

    .line 2206327
    if-nez p1, :cond_0

    .line 2206328
    iget-object v0, p0, LX/F9v;->a:LX/F9x;

    sget-object v1, LX/4cd;->CONNECTION_CHECK_ERROR:LX/4cd;

    invoke-static {v0, v1}, LX/F9x;->a$redex0(LX/F9x;LX/4cd;)V

    .line 2206329
    :goto_0
    return-void

    .line 2206330
    :cond_0
    sget-object v0, LX/F9w;->b:[I

    invoke-virtual {p1}, LX/FA5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2206331
    :pswitch_0
    iget-object v0, p0, LX/F9v;->a:LX/F9x;

    sget-object v1, LX/4cd;->CONNECTION_CHECK_FAILED:LX/4cd;

    invoke-static {v0, v1}, LX/F9x;->a$redex0(LX/F9x;LX/4cd;)V

    goto :goto_0

    .line 2206332
    :pswitch_1
    iget-object v0, p0, LX/F9v;->a:LX/F9x;

    sget-object v1, LX/4cd;->CHECKED_CONNECTION_TOR:LX/4cd;

    invoke-static {v0, v1}, LX/F9x;->a$redex0(LX/F9x;LX/4cd;)V

    goto :goto_0

    .line 2206333
    :pswitch_2
    iget-object v0, p0, LX/F9v;->a:LX/F9x;

    sget-object v1, LX/4cd;->CHECKED_CONNECTION_ONION:LX/4cd;

    invoke-static {v0, v1}, LX/F9x;->a$redex0(LX/F9x;LX/4cd;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
