.class public final LX/Gly;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V
    .locals 0

    .prologue
    .line 2391417
    iput-object p1, p0, LX/Gly;->a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2391418
    iget-object v0, p0, LX/Gly;->a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    .line 2391419
    iget-object v1, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    sget-object p0, LX/0ig;->X:LX/0ih;

    const-string p1, "contact_invite_list_fetch_failed"

    invoke-virtual {v1, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2391420
    iget-object v1, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f08003a

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/Glz;

    invoke-direct {p1, v0}, LX/Glz;-><init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V

    invoke-virtual {v1, p0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2391421
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2391422
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2391423
    iget-object v0, p0, LX/Gly;->a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    iget-object v1, p0, LX/Gly;->a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    iget-object v1, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->f:LX/Gm8;

    const v6, 0x23f217e7

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2391424
    if-eqz p1, :cond_0

    .line 2391425
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2391426
    if-nez v2, :cond_2

    :cond_0
    move v2, v4

    :goto_0
    if-eqz v2, :cond_4

    move v2, v4

    :goto_1
    if-eqz v2, :cond_6

    .line 2391427
    const/4 v2, 0x0

    .line 2391428
    :cond_1
    move-object v1, v2

    .line 2391429
    iget-object v2, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2391430
    if-nez v1, :cond_8

    .line 2391431
    iget-object v2, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    sget-object v3, LX/0ig;->X:LX/0ih;

    const-string v4, "contact_invite_list_fetch_success"

    const-string v5, "empty"

    invoke-virtual {v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2391432
    iget-object v2, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->l:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2391433
    :goto_2
    return-void

    .line 2391434
    :cond_2
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2391435
    check-cast v2, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    invoke-virtual {v2}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2391436
    if-nez v2, :cond_3

    move v2, v4

    goto :goto_0

    :cond_3
    move v2, v5

    goto :goto_0

    .line 2391437
    :cond_4
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2391438
    check-cast v2, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    invoke-virtual {v2}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v3, v2, v5, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2391439
    if-eqz v2, :cond_5

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_3
    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    goto :goto_1

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_3

    .line 2391440
    :cond_6
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2391441
    check-cast v2, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    invoke-virtual {v2}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v3, v2, v5, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2391442
    if-eqz v2, :cond_7

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    move-object v3, v2

    .line 2391443
    :goto_4
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2391444
    check-cast v2, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    invoke-virtual {v2}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    iget-object v7, v1, LX/Gm8;->b:LX/Gm9;

    invoke-virtual {v6, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2391445
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2391446
    const-string v8, "contact_list_header_text"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/Gm9;->a:Ljava/lang/String;

    .line 2391447
    const-string v8, "contact_list_item_add_message_action_text"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/Gm9;->b:Ljava/lang/String;

    .line 2391448
    const-string v8, "confirmation_header_text"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/Gm9;->c:Ljava/lang/String;

    .line 2391449
    const-string v8, "confirmation_title_text"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/Gm9;->d:Ljava/lang/String;

    .line 2391450
    const-string v8, "confirmation_body_text"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/Gm9;->e:Ljava/lang/String;

    .line 2391451
    const-string v8, "is_top_up_incentive"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, v7, LX/Gm9;->f:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2391452
    :goto_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2391453
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2391454
    new-instance v8, LX/GmD;

    invoke-virtual {v7, v6, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7, v6, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, p0, v6}, LX/GmD;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2391455
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    move-object v3, v2

    goto :goto_4

    .line 2391456
    :catch_0
    move-exception v6

    .line 2391457
    iget-object v8, v7, LX/Gm9;->h:LX/03V;

    sget-object p0, LX/Gm9;->g:Ljava/lang/String;

    invoke-virtual {v8, p0, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 2391458
    :cond_8
    iget-object v2, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    sget-object v3, LX/0ig;->X:LX/0ih;

    const-string v4, "contact_invite_list_fetch_success"

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2391459
    iget-object v2, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a:LX/Glv;

    .line 2391460
    iput-object v1, v2, LX/Glv;->c:Ljava/util/List;

    .line 2391461
    iget-object v3, v2, LX/Glv;->c:Ljava/util/List;

    .line 2391462
    iget-object v4, v2, LX/Glv;->b:LX/0W9;

    invoke-virtual {v4}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v4

    .line 2391463
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 2391464
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2391465
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2391466
    goto/16 :goto_2
.end method
