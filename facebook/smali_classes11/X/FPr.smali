.class public final LX/FPr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/FPr;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/E1i;

.field private final c:LX/17W;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/23R;

.field private final f:LX/9hF;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/content/SecureContextHelper;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/17W;LX/0Ot;LX/23R;LX/9hF;LX/E1i;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/17W;",
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;",
            "LX/23R;",
            "LX/9hF;",
            "LX/E1i;",
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "LX/1nD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2238119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2238120
    iput-object p1, p0, LX/FPr;->a:LX/0SG;

    .line 2238121
    iput-object p2, p0, LX/FPr;->c:LX/17W;

    .line 2238122
    iput-object p3, p0, LX/FPr;->d:LX/0Ot;

    .line 2238123
    iput-object p4, p0, LX/FPr;->e:LX/23R;

    .line 2238124
    iput-object p5, p0, LX/FPr;->f:LX/9hF;

    .line 2238125
    iput-object p6, p0, LX/FPr;->b:LX/E1i;

    .line 2238126
    iput-object p7, p0, LX/FPr;->g:LX/0Ot;

    .line 2238127
    iput-object p8, p0, LX/FPr;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2238128
    iput-object p9, p0, LX/FPr;->i:LX/0Or;

    .line 2238129
    return-void
.end method

.method public static a(LX/0QB;)LX/FPr;
    .locals 13

    .prologue
    .line 2238130
    sget-object v0, LX/FPr;->j:LX/FPr;

    if-nez v0, :cond_1

    .line 2238131
    const-class v1, LX/FPr;

    monitor-enter v1

    .line 2238132
    :try_start_0
    sget-object v0, LX/FPr;->j:LX/FPr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2238133
    if-eqz v2, :cond_0

    .line 2238134
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2238135
    new-instance v3, LX/FPr;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    const/16 v6, 0x25ea

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v7

    check-cast v7, LX/23R;

    invoke-static {v0}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v8

    check-cast v8, LX/9hF;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v9

    check-cast v9, LX/E1i;

    const/16 v10, 0x1067

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    const/16 v12, 0x1140

    invoke-static {v0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/FPr;-><init>(LX/0SG;LX/17W;LX/0Ot;LX/23R;LX/9hF;LX/E1i;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0Or;)V

    .line 2238136
    move-object v0, v3

    .line 2238137
    sput-object v0, LX/FPr;->j:LX/FPr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2238138
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2238139
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2238140
    :cond_1
    sget-object v0, LX/FPr;->j:LX/FPr;

    return-object v0

    .line 2238141
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2238142
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2238143
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238144
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238145
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2238146
    iget-object v0, p0, LX/FPr;->c:LX/17W;

    invoke-virtual {v0, p1, p2, p3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2238147
    const-string v0, "Failed navigating to url = %s "

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {p4, v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2238148
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2238149
    goto :goto_0
.end method

.method private a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Lcom/facebook/search/results/model/SearchResultsMutableContext;Landroid/support/v4/app/Fragment;)V
    .locals 10

    .prologue
    .line 2238150
    iget-object v0, p0, LX/FPr;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vC;

    .line 2238151
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2238152
    const-string v2, "ANDROID_SEARCH_LOCAL_PLACES_TAB"

    .line 2238153
    const v3, 0x1e0002

    invoke-virtual {v0, v3, v1, v2}, LX/1vC;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2238154
    const v3, 0x1e000a

    invoke-virtual {v0, v3, v1, v2}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2238155
    iget-object v0, p0, LX/FPr;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIs;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/8ci;->A:LX/8ci;

    invoke-virtual {v5}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CIs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2238156
    iget-object v0, p0, LX/FPr;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1nD;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v5

    sget-object v7, LX/8ci;->A:LX/8ci;

    .line 2238157
    iget-object v0, p2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v9, v0

    .line 2238158
    move-object v6, v1

    move-object v8, v2

    invoke-virtual/range {v3 .. v9}, LX/1nD;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v0

    .line 2238159
    iget-object v1, p0, LX/FPr;->h:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2b67

    invoke-interface {v1, v0, v2, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2238160
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2238099
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2238100
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238101
    invoke-static {p1}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    .line 2238102
    iput-boolean v1, v0, LX/9hD;->m:Z

    .line 2238103
    move-object v0, v0

    .line 2238104
    sget-object v1, LX/74S;->NEARBYPLACES:LX/74S;

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    .line 2238105
    iget-object v1, p0, LX/FPr;->e:LX/23R;

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, p3, v0, v2}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2238106
    return-void

    .line 2238107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;Landroid/support/v4/app/Fragment;Ljava/lang/Class;)V
    .locals 2
    .param p3    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            "Landroid/location/Location;",
            "Lcom/facebook/nearby/v2/logging/NearbyPlacesSessionProvider;",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2238108
    if-eqz p4, :cond_0

    .line 2238109
    iget-object v0, p4, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v0, v0

    .line 2238110
    if-eqz v0, :cond_0

    .line 2238111
    iget-object v0, p4, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v0, v0

    .line 2238112
    iget-object v1, v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v1

    .line 2238113
    if-nez v0, :cond_1

    .line 2238114
    :cond_0
    invoke-virtual {p0, p2, p3, p1, p6}, LX/FPr;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2238115
    :goto_0
    return-void

    .line 2238116
    :cond_1
    iget-object v0, p4, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v0, v0

    .line 2238117
    iget-object v1, v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v0, v1

    .line 2238118
    invoke-direct {p0, p2, v0, p5}, LX/FPr;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Lcom/facebook/search/results/model/SearchResultsMutableContext;Landroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/content/Context;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2238057
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238058
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2238059
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2238060
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2238061
    const-string v2, "com.facebook.katana.profile.id"

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2238062
    const-string v2, "profile_name"

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238063
    sget-object v2, LX/0ax;->eG:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1, v0, p3}, LX/FPr;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 2238064
    return-void

    .line 2238065
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Landroid/content/Context;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            "Landroid/location/Location;",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2238090
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238091
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2238092
    sget-object v1, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2238093
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/FPr;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/03l;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 2238094
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2238095
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v3, v4}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2238096
    const-string v0, "extra_user_location"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2238097
    invoke-direct {p0, p3, v1, v2, p4}, LX/FPr;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 2238098
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2238066
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2238067
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/FPr;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/03l;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 2238068
    sget-object v0, LX/0ax;->aM:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2238069
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2238070
    invoke-direct {p0, p2, v0, v1, p3}, LX/FPr;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 2238071
    return-void

    .line 2238072
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2238073
    iget-object v0, p0, LX/FPr;->b:LX/E1i;

    invoke-virtual {v0, p3, p4, p1, p2}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2238074
    iget-object v1, p0, LX/FPr;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, v0, LX/Cfl;->d:Landroid/content/Intent;

    invoke-interface {v1, v0, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2238075
    return-void
.end method

.method public final b(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/content/Context;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2238076
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238077
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->f()Landroid/location/Location;

    move-result-object v0

    .line 2238078
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238079
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2238080
    const-string v2, "place_name"

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238081
    const-string v2, "latitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2238082
    const-string v2, "longitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2238083
    const-string v0, "zoom"

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 2238084
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->c()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    .line 2238085
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2238086
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082122

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2238087
    const-string v2, "address"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238088
    :cond_0
    sget-object v0, LX/0ax;->gH:Ljava/lang/String;

    invoke-direct {p0, p2, v0, v1, p3}, LX/FPr;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 2238089
    return-void
.end method
