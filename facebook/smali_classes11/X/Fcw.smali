.class public LX/Fcw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/Fcu;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile g:LX/Fcw;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:LX/0W9;

.field private final e:LX/0lC;

.field public final f:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2262891
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    const-string v1, "default"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    const-string v1, "sort_distance"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    const-string v1, "sort_popularity"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    const-string v1, "sort_rating"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Fcw;->a:LX/0Rf;

    .line 2262892
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/Fcu;->HAS_DELIVERY:LX/Fcu;

    const v2, 0x7f08209c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->HAS_PARKING:LX/Fcu;

    const v2, 0x7f0820a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->HAS_TAKEOUT:LX/Fcu;

    const v2, 0x7f0820a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->HAS_WIFI:LX/Fcu;

    const v2, 0x7f0820a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->OUTDOOR_SEATING:LX/Fcu;

    const v2, 0x7f08209f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->POPULAR_WITH_GROUPS:LX/Fcu;

    const v2, 0x7f08209e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->TAKES_CREDIT_CARDS:LX/Fcu;

    const v2, 0x7f08209b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->TAKES_RESERVATIONS:LX/Fcu;

    const v2, 0x7f0820a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/Fcu;->VISITED_BY_FRIENDS:LX/Fcu;

    const v2, 0x7f08209d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Fcw;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0W9;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262894
    iput-object p1, p0, LX/Fcw;->c:Landroid/content/Context;

    .line 2262895
    iput-object p2, p0, LX/Fcw;->d:LX/0W9;

    .line 2262896
    iput-object p3, p0, LX/Fcw;->e:LX/0lC;

    .line 2262897
    iput-object p4, p0, LX/Fcw;->f:LX/03V;

    .line 2262898
    return-void
.end method

.method public static a(LX/0QB;)LX/Fcw;
    .locals 7

    .prologue
    .line 2262899
    sget-object v0, LX/Fcw;->g:LX/Fcw;

    if-nez v0, :cond_1

    .line 2262900
    const-class v1, LX/Fcw;

    monitor-enter v1

    .line 2262901
    :try_start_0
    sget-object v0, LX/Fcw;->g:LX/Fcw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2262902
    if-eqz v2, :cond_0

    .line 2262903
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2262904
    new-instance p0, LX/Fcw;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Fcw;-><init>(Landroid/content/Context;LX/0W9;LX/0lC;LX/03V;)V

    .line 2262905
    move-object v0, p0

    .line 2262906
    sput-object v0, LX/Fcw;->g:LX/Fcw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262907
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2262908
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2262909
    :cond_1
    sget-object v0, LX/Fcw;->g:LX/Fcw;

    return-object v0

    .line 2262910
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2262911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;
    .locals 2

    .prologue
    .line 2262912
    new-instance v0, LX/5ux;

    invoke-direct {v0}, LX/5ux;-><init>()V

    .line 2262913
    iput-boolean p0, v0, LX/5ux;->a:Z

    .line 2262914
    move-object v0, v0

    .line 2262915
    iput-object p1, v0, LX/5ux;->b:Ljava/lang/String;

    .line 2262916
    move-object v0, v0

    .line 2262917
    iput-object p2, v0, LX/5ux;->c:Ljava/lang/String;

    .line 2262918
    move-object v0, v0

    .line 2262919
    invoke-virtual {v0}, LX/5ux;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    .line 2262920
    new-instance v1, LX/5uz;

    invoke-direct {v1}, LX/5uz;-><init>()V

    .line 2262921
    iput-object v0, v1, LX/5uz;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    .line 2262922
    move-object v0, v1

    .line 2262923
    invoke-virtual {v0}, LX/5uz;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a([Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;
    .locals 2

    .prologue
    .line 2262924
    new-instance v0, LX/5uy;

    invoke-direct {v0}, LX/5uy;-><init>()V

    invoke-static {p0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2262925
    iput-object v1, v0, LX/5uy;->a:LX/0Px;

    .line 2262926
    move-object v0, v0

    .line 2262927
    invoke-virtual {v0}, LX/5uy;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Fcw;Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262928
    :try_start_0
    new-instance v0, LX/Fct;

    invoke-direct {v0, p0}, LX/Fct;-><init>(LX/Fcw;)V

    .line 2262929
    iget-object v1, p0, LX/Fcw;->e:LX/0lC;

    invoke-virtual {v1, p1, v0}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2262930
    :goto_0
    return-object v0

    .line 2262931
    :catch_0
    iget-object v0, p0, LX/Fcw;->f:LX/03V;

    const-string v1, "filter_string_value_to_map"

    const-string v2, "An error occurred converting the string filter value to a map."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262932
    const/4 v0, 0x0

    goto :goto_0
.end method
