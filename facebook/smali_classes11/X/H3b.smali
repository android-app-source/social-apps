.class public LX/H3b;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/6aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2421458
    const-class v0, LX/H3b;

    sput-object v0, LX/H3b;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/6aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2421455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2421456
    iput-object p1, p0, LX/H3b;->b:LX/6aG;

    .line 2421457
    return-void
.end method

.method private static a(LX/H3b;Landroid/view/View;I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2421443
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 2421444
    check-cast p1, Landroid/view/ViewGroup;

    .line 2421445
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2421446
    if-eqz v0, :cond_1

    move-object p1, v0

    .line 2421447
    :cond_0
    :goto_0
    return-object p1

    .line 2421448
    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 2421449
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p0, v1, p2}, LX/H3b;->a(LX/H3b;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 2421450
    if-eqz v1, :cond_2

    move-object p1, v1

    .line 2421451
    goto :goto_0

    .line 2421452
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object p1, v2

    .line 2421453
    goto :goto_0

    .line 2421454
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, p2, :cond_0

    move-object p1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BVS;Landroid/view/View;Landroid/location/Location;Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 2421432
    :try_start_0
    const-string v0, "place_distance"

    invoke-virtual {p1, v0}, LX/BVS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 2421433
    if-eqz v0, :cond_0

    .line 2421434
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, p2, v0}, LX/H3b;->a(LX/H3b;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 2421435
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 2421436
    iget-object v1, p0, LX/H3b;->b:LX/6aG;

    invoke-virtual {p3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v1, p4, v2}, LX/6aG;->a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    .line 2421437
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2421438
    :cond_0
    return-void

    .line 2421439
    :cond_1
    new-instance v0, LX/H3a;

    invoke-direct {v0}, LX/H3a;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2421440
    :catch_0
    move-exception v0

    .line 2421441
    sget-object v1, LX/H3b;->a:Ljava/lang/Class;

    const-string v2, "caught %s, message = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2421442
    new-instance v0, LX/H3a;

    invoke-direct {v0}, LX/H3a;-><init>()V

    throw v0
.end method
