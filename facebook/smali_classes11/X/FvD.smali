.class public LX/FvD;
.super LX/Fv4;
.source ""


# instance fields
.field private final e:LX/G4m;

.field private final f:LX/FrH;

.field public final g:LX/Fv9;

.field private final h:Z

.field private i:LX/Fw6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BP0;LX/BQ1;LX/FrH;LX/G4m;LX/Fv9;Ljava/lang/Boolean;LX/0ad;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/FrH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Fv9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2301135
    invoke-direct {p0, p1, p8, p2, p3}, LX/Fv4;-><init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V

    .line 2301136
    iput-object p5, p0, LX/FvD;->e:LX/G4m;

    .line 2301137
    iput-object p4, p0, LX/FvD;->f:LX/FrH;

    .line 2301138
    iput-object p6, p0, LX/FvD;->g:LX/Fv9;

    .line 2301139
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/FvD;->h:Z

    .line 2301140
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2301134
    invoke-static {}, LX/FvC;->cachedValues()[LX/FvC;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2301126
    invoke-static {}, LX/FvC;->cachedValues()[LX/FvC;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2301127
    sget-object v1, LX/FvB;->a:[I

    invoke-virtual {v0}, LX/FvC;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2301128
    invoke-static {p1}, LX/Fv4;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2301129
    :pswitch_0
    new-instance v0, LX/Fw6;

    iget-object v1, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Fw6;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/FvD;->i:LX/Fw6;

    .line 2301130
    iget-boolean v0, p0, LX/FvD;->h:Z

    if-eqz v0, :cond_0

    .line 2301131
    iget-object v0, p0, LX/FvD;->i:LX/Fw6;

    .line 2301132
    invoke-virtual {v0}, LX/Fw6;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2301133
    :cond_0
    iget-object v0, p0, LX/FvD;->i:LX/Fw6;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2301141
    invoke-static {}, LX/FvC;->cachedValues()[LX/FvC;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a([Z)V
    .locals 2

    .prologue
    .line 2301124
    sget-object v0, LX/FvC;->FULL_HEADER:LX/FvC;

    invoke-virtual {v0}, LX/FvC;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    aput-boolean v1, p1, v0

    .line 2301125
    return-void
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 4

    .prologue
    .line 2301119
    invoke-static {}, LX/FvC;->cachedValues()[LX/FvC;

    move-result-object v0

    aget-object v0, v0, p2

    .line 2301120
    sget-object v1, LX/FvC;->FULL_HEADER:LX/FvC;

    if-ne v0, v1, :cond_0

    instance-of v0, p1, LX/Fw6;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2301121
    check-cast v0, LX/Fw6;

    iget-object v1, p0, LX/Fv4;->c:LX/BP0;

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v3, p0, LX/FvD;->f:LX/FrH;

    invoke-virtual {v0, v1, v2, v3}, LX/Fw6;->a(LX/BP0;LX/BQ1;LX/FrH;)Z

    move-result v0

    .line 2301122
    new-instance v1, Lcom/facebook/timeline/header/TimelineHeaderFullHeaderAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/timeline/header/TimelineHeaderFullHeaderAdapter$1;-><init>(LX/FvD;Landroid/view/View;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2301123
    :goto_0
    return v0

    :cond_0
    invoke-static {p2}, LX/Fv4;->c(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2301118
    invoke-virtual {p0, p1}, LX/Fv4;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FvC;

    invoke-virtual {v0}, LX/FvC;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2301117
    invoke-static {}, LX/FvC;->cachedValues()[LX/FvC;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
