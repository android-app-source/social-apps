.class public LX/FmJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dp;


# instance fields
.field private final a:LX/6tI;


# direct methods
.method public constructor <init>(LX/6tI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2282307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282308
    iput-object p1, p0, LX/FmJ;->a:LX/6tI;

    .line 2282309
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;
    .locals 2

    .prologue
    .line 2282310
    sget-object v0, LX/FmI;->a:[I

    invoke-virtual {p2}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2282311
    iget-object v0, p0, LX/FmJ;->a:LX/6tI;

    invoke-virtual {v0, p1, p2}, LX/6tI;->a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2282312
    :pswitch_0
    new-instance v1, LX/FmH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const p0, 0x7f030757

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-direct {v1, v0}, LX/FmH;-><init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V

    move-object v0, v1

    .line 2282313
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
