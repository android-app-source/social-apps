.class public LX/H4p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:Lcom/facebook/notifications/jewel/JewelCountHelper;

.field public b:LX/12w;

.field public c:LX/2GB;

.field public d:LX/0Uq;

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0iE;

.field public g:LX/0xC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2423452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423453
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x4e59e20f    # 9.1386771E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2423454
    sget-object v1, LX/12w;->a:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2423455
    const/16 v1, 0x27

    const v2, 0x52bd92c0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2423456
    :goto_0
    return-void

    .line 2423457
    :cond_0
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v2, p0

    check-cast v2, LX/H4p;

    invoke-static {v1}, Lcom/facebook/notifications/jewel/JewelCountHelper;->a(LX/0QB;)Lcom/facebook/notifications/jewel/JewelCountHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/jewel/JewelCountHelper;

    invoke-static {v1}, LX/12w;->a(LX/0QB;)LX/12w;

    move-result-object v4

    check-cast v4, LX/12w;

    invoke-static {v1}, LX/2GB;->a(LX/0QB;)LX/2GB;

    move-result-object v5

    check-cast v5, LX/2GB;

    invoke-static {v1}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v6

    check-cast v6, LX/0Uq;

    const/16 p2, 0x15e7

    invoke-static {v1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {v1}, LX/2t5;->b(LX/0QB;)LX/2t5;

    move-result-object p3

    check-cast p3, LX/0iE;

    invoke-static {v1}, LX/0xC;->a(LX/0QB;)LX/0xC;

    move-result-object v1

    check-cast v1, LX/0xC;

    iput-object v3, v2, LX/H4p;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    iput-object v4, v2, LX/H4p;->b:LX/12w;

    iput-object v5, v2, LX/H4p;->c:LX/2GB;

    iput-object v6, v2, LX/H4p;->d:LX/0Uq;

    iput-object p2, v2, LX/H4p;->e:LX/0Or;

    iput-object p3, v2, LX/H4p;->f:LX/0iE;

    iput-object v1, v2, LX/H4p;->g:LX/0xC;

    .line 2423458
    iget-object v1, p0, LX/H4p;->d:LX/0Uq;

    invoke-virtual {v1}, LX/0Uq;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2423459
    iget-object v1, p0, LX/H4p;->c:LX/2GB;

    new-instance v2, Lcom/facebook/notifications/jewel/JewelCountInitiateFetchReceiver$1;

    invoke-direct {v2, p0}, Lcom/facebook/notifications/jewel/JewelCountInitiateFetchReceiver$1;-><init>(LX/H4p;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, LX/2GB;->a(Ljava/lang/Runnable;J)V

    .line 2423460
    :cond_1
    const v1, 0x6a7aaceb

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
