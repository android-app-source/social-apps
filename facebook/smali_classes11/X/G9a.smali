.class public final LX/G9a;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[C


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2322921
    const/16 v0, 0x2d

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, LX/G9a;->a:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
        0x47s
        0x48s
        0x49s
        0x4as
        0x4bs
        0x4cs
        0x4ds
        0x4es
        0x4fs
        0x50s
        0x51s
        0x52s
        0x53s
        0x54s
        0x55s
        0x56s
        0x57s
        0x58s
        0x59s
        0x5as
        0x20s
        0x24s
        0x25s
        0x2as
        0x2bs
        0x2ds
        0x2es
        0x2fs
        0x3as
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2322919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322920
    return-void
.end method

.method private static a(I)C
    .locals 1

    .prologue
    .line 2322916
    sget-object v0, LX/G9a;->a:[C

    array-length v0, v0

    if-lt p0, v0, :cond_0

    .line 2322917
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322918
    :cond_0
    sget-object v0, LX/G9a;->a:[C

    aget-char v0, v0, p0

    return v0
.end method

.method private static a(LX/G97;)I
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2322905
    invoke-virtual {p0, v3}, LX/G97;->a(I)I

    move-result v0

    .line 2322906
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_0

    .line 2322907
    and-int/lit8 v0, v0, 0x7f

    .line 2322908
    :goto_0
    return v0

    .line 2322909
    :cond_0
    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-ne v1, v2, :cond_1

    .line 2322910
    invoke-virtual {p0, v3}, LX/G97;->a(I)I

    move-result v1

    .line 2322911
    and-int/lit8 v0, v0, 0x3f

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    goto :goto_0

    .line 2322912
    :cond_1
    and-int/lit16 v1, v0, 0xe0

    const/16 v2, 0xc0

    if-ne v1, v2, :cond_2

    .line 2322913
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, LX/G97;->a(I)I

    move-result v1

    .line 2322914
    and-int/lit8 v0, v0, 0x1f

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v1

    goto :goto_0

    .line 2322915
    :cond_2
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0
.end method

.method public static a([BLX/G9i;LX/G9c;Ljava/util/Map;)LX/G99;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "LX/G9i;",
            "LX/G9c;",
            "Ljava/util/Map",
            "<",
            "LX/G8t;",
            "*>;)",
            "LX/G99;"
        }
    .end annotation

    .prologue
    .line 2322922
    new-instance v0, LX/G97;

    invoke-direct {v0, p0}, LX/G97;-><init>([B)V

    .line 2322923
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2322924
    new-instance v4, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2322925
    const/4 v5, -0x1

    .line 2322926
    const/4 v6, -0x1

    .line 2322927
    const/4 v3, 0x0

    .line 2322928
    const/4 v2, 0x0

    move v8, v6

    move v9, v5

    move v6, v2

    .line 2322929
    :goto_0
    :try_start_0
    invoke-virtual {v0}, LX/G97;->a()I

    move-result v2

    const/4 v5, 0x4

    if-ge v2, v5, :cond_1

    .line 2322930
    sget-object v2, LX/G9e;->TERMINATOR:LX/G9e;

    move-object v7, v2

    .line 2322931
    :goto_1
    sget-object v2, LX/G9e;->TERMINATOR:LX/G9e;

    if-eq v7, v2, :cond_f

    .line 2322932
    sget-object v2, LX/G9e;->FNC1_FIRST_POSITION:LX/G9e;

    if-eq v7, v2, :cond_0

    sget-object v2, LX/G9e;->FNC1_SECOND_POSITION:LX/G9e;

    if-ne v7, v2, :cond_2

    .line 2322933
    :cond_0
    const/4 v6, 0x1

    move v2, v6

    move v5, v9

    move v6, v8

    .line 2322934
    :goto_2
    sget-object v8, LX/G9e;->TERMINATOR:LX/G9e;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v7, v8, :cond_10

    .line 2322935
    new-instance v0, LX/G99;

    .line 2322936
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2322937
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v3, 0x0

    :goto_3
    if-nez p2, :cond_e

    const/4 v4, 0x0

    :goto_4
    move-object v1, p0

    .line 2322938
    invoke-direct/range {v0 .. v6}, LX/G99;-><init>([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;II)V

    return-object v0

    .line 2322939
    :cond_1
    const/4 v2, 0x4

    :try_start_1
    invoke-virtual {v0, v2}, LX/G97;->a(I)I

    move-result v2

    invoke-static {v2}, LX/G9e;->forBits(I)LX/G9e;

    move-result-object v2

    move-object v7, v2

    goto :goto_1

    .line 2322940
    :cond_2
    sget-object v2, LX/G9e;->STRUCTURED_APPEND:LX/G9e;

    if-ne v7, v2, :cond_4

    .line 2322941
    invoke-virtual {v0}, LX/G97;->a()I

    move-result v2

    const/16 v5, 0x10

    if-ge v2, v5, :cond_3

    .line 2322942
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2322943
    :catch_0
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322944
    :cond_3
    const/16 v2, 0x8

    :try_start_2
    invoke-virtual {v0, v2}, LX/G97;->a(I)I

    move-result v9

    .line 2322945
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, LX/G97;->a(I)I

    move-result v8

    move v2, v6

    move v5, v9

    move v6, v8

    goto :goto_2

    .line 2322946
    :cond_4
    sget-object v2, LX/G9e;->ECI:LX/G9e;

    if-ne v7, v2, :cond_6

    .line 2322947
    invoke-static {v0}, LX/G9a;->a(LX/G97;)I

    move-result v2

    .line 2322948
    invoke-static {v2}, LX/G98;->getCharacterSetECIByValue(I)LX/G98;

    move-result-object v3

    .line 2322949
    if-nez v3, :cond_5

    .line 2322950
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    :cond_5
    move v2, v6

    move v5, v9

    move v6, v8

    .line 2322951
    goto :goto_2

    .line 2322952
    :cond_6
    sget-object v2, LX/G9e;->HANZI:LX/G9e;

    if-ne v7, v2, :cond_8

    .line 2322953
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/G97;->a(I)I

    move-result v2

    .line 2322954
    invoke-virtual {v7, p1}, LX/G9e;->getCharacterCountBits(LX/G9i;)I

    move-result v5

    invoke-virtual {v0, v5}, LX/G97;->a(I)I

    move-result v5

    .line 2322955
    const/4 v10, 0x1

    if-ne v2, v10, :cond_7

    .line 2322956
    invoke-static {v0, v1, v5}, LX/G9a;->a(LX/G97;Ljava/lang/StringBuilder;I)V

    :cond_7
    move v2, v6

    move v5, v9

    move v6, v8

    .line 2322957
    goto :goto_2

    .line 2322958
    :cond_8
    invoke-virtual {v7, p1}, LX/G9e;->getCharacterCountBits(LX/G9i;)I

    move-result v2

    invoke-virtual {v0, v2}, LX/G97;->a(I)I

    move-result v2

    .line 2322959
    sget-object v5, LX/G9e;->NUMERIC:LX/G9e;

    if-ne v7, v5, :cond_9

    .line 2322960
    invoke-static {v0, v1, v2}, LX/G9a;->c(LX/G97;Ljava/lang/StringBuilder;I)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_2

    .line 2322961
    :cond_9
    sget-object v5, LX/G9e;->ALPHANUMERIC:LX/G9e;

    if-ne v7, v5, :cond_a

    .line 2322962
    invoke-static {v0, v1, v2, v6}, LX/G9a;->a(LX/G97;Ljava/lang/StringBuilder;IZ)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_2

    .line 2322963
    :cond_a
    sget-object v5, LX/G9e;->BYTE:LX/G9e;

    if-ne v7, v5, :cond_b

    move-object v5, p3

    .line 2322964
    invoke-static/range {v0 .. v5}, LX/G9a;->a(LX/G97;Ljava/lang/StringBuilder;ILX/G98;Ljava/util/Collection;Ljava/util/Map;)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_2

    .line 2322965
    :cond_b
    sget-object v5, LX/G9e;->KANJI:LX/G9e;

    if-ne v7, v5, :cond_c

    .line 2322966
    invoke-static {v0, v1, v2}, LX/G9a;->b(LX/G97;Ljava/lang/StringBuilder;I)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_2

    .line 2322967
    :cond_c
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_d
    move-object v3, v4

    .line 2322968
    goto/16 :goto_3

    .line 2322969
    :cond_e
    invoke-virtual {p2}, LX/G9c;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    :cond_f
    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_2

    :cond_10
    move v8, v6

    move v9, v5

    move v6, v2

    goto/16 :goto_0
.end method

.method private static a(LX/G97;Ljava/lang/StringBuilder;I)V
    .locals 4

    .prologue
    .line 2322887
    mul-int/lit8 v0, p2, 0xd

    invoke-virtual {p0}, LX/G97;->a()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2322888
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322889
    :cond_0
    mul-int/lit8 v0, p2, 0x2

    new-array v2, v0, [B

    .line 2322890
    const/4 v0, 0x0

    move v1, v0

    .line 2322891
    :goto_0
    if-lez p2, :cond_2

    .line 2322892
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, LX/G97;->a(I)I

    move-result v0

    .line 2322893
    div-int/lit8 v3, v0, 0x60

    shl-int/lit8 v3, v3, 0x8

    rem-int/lit8 v0, v0, 0x60

    or-int/2addr v0, v3

    .line 2322894
    const/16 v3, 0x3bf

    if-ge v0, v3, :cond_1

    .line 2322895
    const v3, 0xa1a1

    add-int/2addr v0, v3

    .line 2322896
    :goto_1
    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 2322897
    add-int/lit8 v3, v1, 0x1

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    .line 2322898
    add-int/lit8 v0, v1, 0x2

    .line 2322899
    add-int/lit8 p2, p2, -0x1

    move v1, v0

    .line 2322900
    goto :goto_0

    .line 2322901
    :cond_1
    const v3, 0xa6a1

    add-int/2addr v0, v3

    goto :goto_1

    .line 2322902
    :cond_2
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "GB2312"

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2322903
    return-void

    .line 2322904
    :catch_0
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0
.end method

.method private static a(LX/G97;Ljava/lang/StringBuilder;ILX/G98;Ljava/util/Collection;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G97;",
            "Ljava/lang/StringBuilder;",
            "I",
            "LX/G98;",
            "Ljava/util/Collection",
            "<[B>;",
            "Ljava/util/Map",
            "<",
            "LX/G8t;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2322874
    mul-int/lit8 v0, p2, 0x8

    invoke-virtual {p0}, LX/G97;->a()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2322875
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322876
    :cond_0
    new-array v1, p2, [B

    .line 2322877
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    .line 2322878
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, LX/G97;->a(I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 2322879
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2322880
    :cond_1
    if-nez p3, :cond_2

    .line 2322881
    invoke-static {v1, p5}, LX/G9G;->a([BLjava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 2322882
    :goto_1
    :try_start_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2322883
    invoke-interface {p4, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 2322884
    return-void

    .line 2322885
    :cond_2
    invoke-virtual {p3}, LX/G98;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2322886
    :catch_0
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0
.end method

.method private static a(LX/G97;Ljava/lang/StringBuilder;IZ)V
    .locals 7

    .prologue
    const/16 v6, 0x25

    const/16 v5, 0xb

    const/4 v4, 0x6

    const/4 v3, 0x1

    .line 2322853
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 2322854
    :goto_0
    if-le p2, v3, :cond_1

    .line 2322855
    invoke-virtual {p0}, LX/G97;->a()I

    move-result v1

    if-ge v1, v5, :cond_0

    .line 2322856
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322857
    :cond_0
    invoke-virtual {p0, v5}, LX/G97;->a(I)I

    move-result v1

    .line 2322858
    div-int/lit8 v2, v1, 0x2d

    invoke-static {v2}, LX/G9a;->a(I)C

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322859
    rem-int/lit8 v1, v1, 0x2d

    invoke-static {v1}, LX/G9a;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322860
    add-int/lit8 p2, p2, -0x2

    .line 2322861
    goto :goto_0

    .line 2322862
    :cond_1
    if-ne p2, v3, :cond_3

    .line 2322863
    invoke-virtual {p0}, LX/G97;->a()I

    move-result v1

    if-ge v1, v4, :cond_2

    .line 2322864
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322865
    :cond_2
    invoke-virtual {p0, v4}, LX/G97;->a(I)I

    move-result v1

    invoke-static {v1}, LX/G9a;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322866
    :cond_3
    if-eqz p3, :cond_6

    .line 2322867
    :goto_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 2322868
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_4

    .line 2322869
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_5

    .line 2322870
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 2322871
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322872
    :cond_5
    const/16 v1, 0x1d

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_2

    .line 2322873
    :cond_6
    return-void
.end method

.method private static b(LX/G97;Ljava/lang/StringBuilder;I)V
    .locals 4

    .prologue
    .line 2322835
    mul-int/lit8 v0, p2, 0xd

    invoke-virtual {p0}, LX/G97;->a()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2322836
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322837
    :cond_0
    mul-int/lit8 v0, p2, 0x2

    new-array v2, v0, [B

    .line 2322838
    const/4 v0, 0x0

    move v1, v0

    .line 2322839
    :goto_0
    if-lez p2, :cond_2

    .line 2322840
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, LX/G97;->a(I)I

    move-result v0

    .line 2322841
    div-int/lit16 v3, v0, 0xc0

    shl-int/lit8 v3, v3, 0x8

    rem-int/lit16 v0, v0, 0xc0

    or-int/2addr v0, v3

    .line 2322842
    const/16 v3, 0x1f00

    if-ge v0, v3, :cond_1

    .line 2322843
    const v3, 0x8140

    add-int/2addr v0, v3

    .line 2322844
    :goto_1
    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 2322845
    add-int/lit8 v3, v1, 0x1

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    .line 2322846
    add-int/lit8 v0, v1, 0x2

    .line 2322847
    add-int/lit8 p2, p2, -0x1

    move v1, v0

    .line 2322848
    goto :goto_0

    .line 2322849
    :cond_1
    const v3, 0xc140

    add-int/2addr v0, v3

    goto :goto_1

    .line 2322850
    :cond_2
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "SJIS"

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2322851
    return-void

    .line 2322852
    :catch_0
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0
.end method

.method private static c(LX/G97;Ljava/lang/StringBuilder;I)V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x4

    const/16 v2, 0xa

    .line 2322808
    :goto_0
    const/4 v0, 0x3

    if-lt p2, v0, :cond_2

    .line 2322809
    invoke-virtual {p0}, LX/G97;->a()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 2322810
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322811
    :cond_0
    invoke-virtual {p0, v2}, LX/G97;->a(I)I

    move-result v0

    .line 2322812
    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_1

    .line 2322813
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322814
    :cond_1
    div-int/lit8 v1, v0, 0x64

    invoke-static {v1}, LX/G9a;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322815
    div-int/lit8 v1, v0, 0xa

    rem-int/lit8 v1, v1, 0xa

    invoke-static {v1}, LX/G9a;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322816
    rem-int/lit8 v0, v0, 0xa

    invoke-static {v0}, LX/G9a;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322817
    add-int/lit8 p2, p2, -0x3

    .line 2322818
    goto :goto_0

    .line 2322819
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_6

    .line 2322820
    invoke-virtual {p0}, LX/G97;->a()I

    move-result v0

    if-ge v0, v4, :cond_3

    .line 2322821
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322822
    :cond_3
    invoke-virtual {p0, v4}, LX/G97;->a(I)I

    move-result v0

    .line 2322823
    const/16 v1, 0x64

    if-lt v0, v1, :cond_4

    .line 2322824
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322825
    :cond_4
    div-int/lit8 v1, v0, 0xa

    invoke-static {v1}, LX/G9a;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322826
    rem-int/lit8 v0, v0, 0xa

    invoke-static {v0}, LX/G9a;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2322827
    :cond_5
    :goto_1
    return-void

    .line 2322828
    :cond_6
    const/4 v0, 0x1

    if-ne p2, v0, :cond_5

    .line 2322829
    invoke-virtual {p0}, LX/G97;->a()I

    move-result v0

    if-ge v0, v3, :cond_7

    .line 2322830
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322831
    :cond_7
    invoke-virtual {p0, v3}, LX/G97;->a(I)I

    move-result v0

    .line 2322832
    if-lt v0, v2, :cond_8

    .line 2322833
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322834
    :cond_8
    invoke-static {v0}, LX/G9a;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
