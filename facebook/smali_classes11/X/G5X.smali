.class public LX/G5X;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/G5b;

.field private final b:LX/G5b;

.field private final c:LX/0Sh;

.field private final d:LX/0aG;

.field private final e:Ljava/lang/Boolean;

.field private final f:LX/G5P;

.field private final g:LX/7BO;

.field private final h:LX/03V;


# direct methods
.method public constructor <init>(LX/G5b;LX/G5b;LX/0Sh;LX/0aG;Ljava/lang/Boolean;LX/G5P;LX/7BO;LX/03V;)V
    .locals 0
    .param p1    # LX/G5b;
        .annotation runtime Lcom/facebook/uberbar/annotations/FriendsResolver;
        .end annotation
    .end param
    .param p2    # LX/G5b;
        .annotation runtime Lcom/facebook/uberbar/annotations/PagesResolver;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/uberbar/annotations/IsPageOnlyUberbarSearchEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318947
    iput-object p1, p0, LX/G5X;->a:LX/G5b;

    .line 2318948
    iput-object p2, p0, LX/G5X;->b:LX/G5b;

    .line 2318949
    iput-object p3, p0, LX/G5X;->c:LX/0Sh;

    .line 2318950
    iput-object p4, p0, LX/G5X;->d:LX/0aG;

    .line 2318951
    iput-object p5, p0, LX/G5X;->e:Ljava/lang/Boolean;

    .line 2318952
    iput-object p6, p0, LX/G5X;->f:LX/G5P;

    .line 2318953
    iput-object p7, p0, LX/G5X;->g:LX/7BO;

    .line 2318954
    iput-object p8, p0, LX/G5X;->h:LX/03V;

    .line 2318955
    return-void
.end method


# virtual methods
.method public final a(ILcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;)LX/G5W;
    .locals 12

    .prologue
    .line 2318956
    new-instance v0, LX/G5W;

    iget-object v1, p0, LX/G5X;->a:LX/G5b;

    iget-object v2, p0, LX/G5X;->b:LX/G5b;

    iget-object v3, p0, LX/G5X;->c:LX/0Sh;

    iget-object v4, p0, LX/G5X;->d:LX/0aG;

    iget-object v5, p0, LX/G5X;->e:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-object v9, p0, LX/G5X;->f:LX/G5P;

    iget-object v10, p0, LX/G5X;->g:LX/7BO;

    iget-object v11, p0, LX/G5X;->h:LX/03V;

    move v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v11}, LX/G5W;-><init>(LX/G5b;LX/G5b;LX/0Sh;LX/0aG;ILcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;ZLX/G5P;LX/7BO;LX/03V;)V

    return-object v0
.end method
