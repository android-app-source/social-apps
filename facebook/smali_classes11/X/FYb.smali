.class public LX/FYb;
.super LX/1a1;
.source ""


# static fields
.field private static final l:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field private final A:Landroid/view/View;

.field private final B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final C:Lcom/facebook/widget/text/BetterTextView;

.field public final D:LX/FYP;

.field public final E:LX/FWt;

.field public final F:Landroid/os/Handler;

.field private final G:LX/16U;

.field private final H:LX/FYa;

.field private final I:LX/0ad;

.field private final J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field private final K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final L:LX/15X;

.field private final M:LX/FYy;

.field private N:Landroid/content/res/Resources;

.field public O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:J

.field public S:LX/2fs;

.field private T:LX/19w;

.field private U:Ljava/lang/String;

.field private final m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

.field private final n:LX/FYX;

.field private final o:LX/FYW;

.field private final p:LX/FYY;

.field private final q:LX/FYZ;

.field private final r:Landroid/graphics/drawable/Drawable;

.field private final s:Landroid/graphics/drawable/Drawable;

.field private final t:LX/FYm;

.field private final u:Lcom/facebook/widget/text/BetterTextView;

.field private final v:Lcom/facebook/widget/text/BetterTextView;

.field public final w:Lcom/facebook/widget/text/BetterTextView;

.field private final x:Lcom/facebook/fbui/glyph/GlyphView;

.field private final y:Lcom/facebook/widget/text/BetterTextView;

.field private final z:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2256303
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_DASHBOARD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/FYb;->l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/View;Ljava/lang/String;LX/FYP;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/16H;LX/FWt;LX/FYn;LX/19w;LX/16U;LX/0ad;LX/15X;LX/0Ot;LX/FYy;)V
    .locals 9
    .param p1    # Landroid/support/v4/app/FragmentActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/FYP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "LX/FYP;",
            "LX/0Ot",
            "<",
            "LX/FYh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FY2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/16H;",
            "LX/FWt;",
            "LX/FYn;",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            "LX/16U;",
            "LX/0ad;",
            "LX/15X;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/FYy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256304
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2256305
    const v1, 0x7f0d2b0f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    iput-object v1, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    .line 2256306
    iget-object v1, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    invoke-virtual {v1}, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    .line 2256307
    iget-object v1, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    iget-object v2, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v3, 0x7f0b229b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2256308
    const v1, 0x7f0d2b10

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/FYb;->u:Lcom/facebook/widget/text/BetterTextView;

    .line 2256309
    const v1, 0x7f0d2b11

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/FYb;->v:Lcom/facebook/widget/text/BetterTextView;

    .line 2256310
    const v1, 0x7f0d2b14

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/FYb;->w:Lcom/facebook/widget/text/BetterTextView;

    .line 2256311
    const v1, 0x7f0d2b12

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v1, p0, LX/FYb;->x:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2256312
    const v1, 0x7f0d2b13

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/FYb;->y:Lcom/facebook/widget/text/BetterTextView;

    .line 2256313
    const v1, 0x7f0d2b1a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->z:Landroid/view/View;

    .line 2256314
    const v1, 0x7f0d2b19

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    move-object/from16 v0, p12

    invoke-virtual {v0, v1}, LX/FYn;->a(Landroid/view/ViewStub;)LX/FYm;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->t:LX/FYm;

    .line 2256315
    new-instance v1, LX/FYX;

    iget-object v5, p0, LX/FYb;->t:LX/FYm;

    move-object v2, p5

    move-object/from16 v3, p7

    move-object/from16 v4, p9

    move-object v6, p1

    move-object v7, p3

    move-object/from16 v8, p13

    invoke-direct/range {v1 .. v8}, LX/FYX;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/FYm;Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;LX/19w;)V

    iput-object v1, p0, LX/FYb;->n:LX/FYX;

    .line 2256316
    new-instance v1, LX/FYY;

    invoke-direct {v1, p0, p1, p6}, LX/FYY;-><init>(LX/FYb;Landroid/support/v4/app/FragmentActivity;LX/0Ot;)V

    iput-object v1, p0, LX/FYb;->p:LX/FYY;

    .line 2256317
    new-instance v1, LX/FYW;

    invoke-direct {v1, p0, p1, p6}, LX/FYW;-><init>(LX/FYb;Landroid/support/v4/app/FragmentActivity;LX/0Ot;)V

    iput-object v1, p0, LX/FYb;->o:LX/FYW;

    .line 2256318
    new-instance v1, LX/FYZ;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v3, p10

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, LX/FYZ;-><init>(Landroid/content/Context;LX/16H;LX/0Ot;LX/0Ot;Ljava/lang/String;)V

    iput-object v1, p0, LX/FYb;->q:LX/FYZ;

    .line 2256319
    iput-object p4, p0, LX/FYb;->D:LX/FYP;

    .line 2256320
    iget-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v2, 0x7f020afe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->r:Landroid/graphics/drawable/Drawable;

    .line 2256321
    iget-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v2, 0x7f020caa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->s:Landroid/graphics/drawable/Drawable;

    .line 2256322
    iget-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v2, 0x7f0b115a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2256323
    iget-object v2, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    invoke-virtual {v2, v1, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->c(II)V

    .line 2256324
    move-object/from16 v0, p11

    iput-object v0, p0, LX/FYb;->E:LX/FWt;

    .line 2256325
    const/4 v1, 0x0

    iput-object v1, p0, LX/FYb;->O:Ljava/lang/String;

    .line 2256326
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/FYb;->F:Landroid/os/Handler;

    .line 2256327
    move-object/from16 v0, p13

    iput-object v0, p0, LX/FYb;->T:LX/19w;

    .line 2256328
    move-object/from16 v0, p14

    iput-object v0, p0, LX/FYb;->G:LX/16U;

    .line 2256329
    new-instance v1, LX/FYa;

    invoke-direct {v1, p0}, LX/FYa;-><init>(LX/FYb;)V

    iput-object v1, p0, LX/FYb;->H:LX/FYa;

    .line 2256330
    move-object/from16 v0, p15

    iput-object v0, p0, LX/FYb;->I:LX/0ad;

    .line 2256331
    move-object/from16 v0, p16

    iput-object v0, p0, LX/FYb;->L:LX/15X;

    .line 2256332
    move-object/from16 v0, p9

    iput-object v0, p0, LX/FYb;->J:LX/0Ot;

    .line 2256333
    move-object/from16 v0, p17

    iput-object v0, p0, LX/FYb;->K:LX/0Ot;

    .line 2256334
    iget-object v1, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    new-instance v2, LX/FYU;

    invoke-direct {v2, p0}, LX/FYU;-><init>(LX/FYb;)V

    invoke-virtual {v1, v2}, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;->setOnAfterDrawListener(LX/FYT;)V

    .line 2256335
    const v1, 0x7f0d2b16

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->A:Landroid/view/View;

    .line 2256336
    const v1, 0x7f0d2b17

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v1, p0, LX/FYb;->B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2256337
    new-instance v1, LX/1Uo;

    iget-object v2, p0, LX/FYb;->N:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2256338
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Uo;->a(LX/4Ab;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 2256339
    iget-object v2, p0, LX/FYb;->B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2256340
    const v1, 0x7f0d2b18

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/FYb;->C:Lcom/facebook/widget/text/BetterTextView;

    .line 2256341
    iput-object p3, p0, LX/FYb;->U:Ljava/lang/String;

    .line 2256342
    move-object/from16 v0, p18

    iput-object v0, p0, LX/FYb;->M:LX/FYy;

    .line 2256343
    return-void
.end method

.method private static a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2256382
    const-wide/16 v0, 0xa

    mul-long/2addr v0, p0

    long-to-double v0, v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    div-double/2addr v0, v2

    .line 2256383
    const-string v2, "%.1f MB"

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/BO1;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0x8

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2256344
    iget-object v0, p0, LX/FYb;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1}, LX/BO1;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2256345
    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2256346
    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, LX/BO1;->V()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, LX/FYb;->R:J

    .line 2256347
    iget-object v0, p0, LX/FYb;->I:LX/0ad;

    sget-short v1, LX/0wh;->X:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FYb;->M:LX/FYy;

    invoke-virtual {v0}, LX/FYy;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, LX/FYb;->b(LX/BO1;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2256348
    iget-object v0, p0, LX/FYb;->M:LX/FYy;

    iget-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    invoke-virtual {v0, p1, v1}, LX/FYy;->a(LX/BO1;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 2256349
    invoke-interface {p1}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->P:Ljava/lang/String;

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    .line 2256350
    :cond_0
    :goto_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2256351
    iget-object v1, p0, LX/FYb;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2256352
    :goto_2
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2256353
    iget-object v1, p0, LX/FYb;->y:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2256354
    :goto_3
    iget-object v1, p0, LX/FYb;->P:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2256355
    iget-object v1, p0, LX/FYb;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v8}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    .line 2256356
    :goto_4
    iget-object v2, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    iget-object v3, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v4, 0x7f083418

    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/Object;

    invoke-interface {p1}, LX/BO1;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, LX/BO1;->k()Ljava/lang/String;

    move-result-object v1

    :goto_5
    aput-object v1, v5, v6

    if-eqz v0, :cond_9

    :goto_6
    aput-object v0, v5, v7

    iget-object v0, p0, LX/FYb;->P:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/FYb;->P:Ljava/lang/String;

    :goto_7
    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2256357
    return-void

    .line 2256358
    :cond_1
    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-wide v0, v0, LX/2fs;->a:J

    goto :goto_0

    .line 2256359
    :cond_2
    invoke-interface {p1}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v0

    :goto_8
    iput-object v0, p0, LX/FYb;->Q:Ljava/lang/String;

    .line 2256360
    invoke-static {p0}, LX/FYb;->y(LX/FYb;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FYb;->P:Ljava/lang/String;

    move-object v0, v2

    goto :goto_1

    .line 2256361
    :cond_3
    invoke-interface {p1}, LX/BO1;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 2256362
    :cond_4
    invoke-interface {p1}, LX/BO1;->l()Ljava/lang/String;

    move-result-object v0

    .line 2256363
    invoke-interface {p1}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/FYb;->P:Ljava/lang/String;

    .line 2256364
    iget-object v1, p0, LX/FYb;->I:LX/0ad;

    sget-short v3, LX/0wh;->X:S

    invoke-interface {v1, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FYb;->M:LX/FYy;

    invoke-virtual {v1}, LX/FYy;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LX/BO1;->O()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2256365
    iget-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    .line 2256366
    invoke-interface {p1}, LX/BO1;->Q()Ljava/lang/String;

    move-result-object v2

    .line 2256367
    if-eqz v2, :cond_b

    .line 2256368
    const v2, 0x7f08341f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2256369
    :goto_9
    move-object v2, v2

    .line 2256370
    goto/16 :goto_1

    .line 2256371
    :cond_5
    iget-object v1, p0, LX/FYb;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2256372
    iget-object v1, p0, LX/FYb;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v6}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2256373
    :cond_6
    iget-object v1, p0, LX/FYb;->y:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2256374
    iget-object v1, p0, LX/FYb;->y:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v6}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2256375
    :cond_7
    iget-object v1, p0, LX/FYb;->w:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, LX/FYb;->P:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2256376
    iget-object v1, p0, LX/FYb;->w:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v6}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2256377
    iget-object v1, p0, LX/FYb;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v7}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    goto/16 :goto_4

    .line 2256378
    :cond_8
    const-string v1, ""

    goto/16 :goto_5

    :cond_9
    const-string v0, ""

    goto/16 :goto_6

    :cond_a
    const-string v0, ""

    goto/16 :goto_7

    .line 2256379
    :cond_b
    invoke-interface {p1}, LX/BO1;->o()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_c

    .line 2256380
    const v2, 0x7f083421

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_9

    .line 2256381
    :cond_c
    const v2, 0x7f083420

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_9
.end method

.method private b(LX/BO1;)Z
    .locals 2

    .prologue
    .line 2256265
    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 2256243
    iget-object v0, p0, LX/FYb;->z:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const v0, 0x106000d

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2256244
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    .line 2256245
    iget-object v1, p0, LX/FYb;->z:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2256246
    :goto_1
    iget-object v1, p0, LX/FYb;->z:Landroid/view/View;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2256247
    return-void

    .line 2256248
    :cond_0
    const v0, 0x7f0a010a

    goto :goto_0

    .line 2256249
    :cond_1
    iget-object v1, p0, LX/FYb;->z:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 2256250
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private c(LX/BO1;)V
    .locals 9

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2256266
    iget-object v0, p0, LX/FYb;->I:LX/0ad;

    sget-short v1, LX/0wh;->X:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2256267
    iget-object v0, p0, LX/FYb;->x:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2256268
    :goto_0
    return-void

    .line 2256269
    :cond_0
    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FYb;->M:LX/FYy;

    invoke-virtual {v0}, LX/FYy;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2256270
    iget-object v0, p0, LX/FYb;->M:LX/FYy;

    .line 2256271
    invoke-interface {p1}, LX/BO1;->o()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 2256272
    const v1, 0x7f021ae8

    .line 2256273
    :goto_1
    move v0, v1

    .line 2256274
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2256275
    :goto_2
    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, LX/FYb;->b(LX/BO1;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, 0x7f021ae8

    if-ne v1, v2, :cond_5

    iget-object v1, p0, LX/FYb;->M:LX/FYy;

    .line 2256276
    iget-object v5, v1, LX/FYy;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0W3;

    sget-wide v7, LX/0X5;->ek:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    move v1, v5

    .line 2256277
    if-nez v1, :cond_5

    .line 2256278
    :cond_1
    iget-object v0, p0, LX/FYb;->x:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    .line 2256279
    :cond_2
    invoke-interface {p1}, LX/BO1;->O()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/FYb;->M:LX/FYy;

    invoke-virtual {v0}, LX/FYy;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2256280
    invoke-interface {p1}, LX/BO1;->Q()Ljava/lang/String;

    move-result-object v0

    .line 2256281
    if-eqz v0, :cond_b

    .line 2256282
    const v0, 0x7f021aea

    .line 2256283
    :goto_3
    move v0, v0

    .line 2256284
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 2256285
    :cond_3
    invoke-interface {p1}, LX/BO1;->o()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    iget-object v0, p0, LX/FYb;->U:Ljava/lang/String;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2256286
    const v0, 0x7f021ae7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 2256287
    :cond_4
    const v0, 0x7f021ae8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 2256288
    :cond_5
    iget-object v1, p0, LX/FYb;->x:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2256289
    iget-object v0, p0, LX/FYb;->x:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2256290
    :cond_6
    invoke-static {p1}, LX/FYy;->d(LX/BO1;)I

    move-result v1

    .line 2256291
    invoke-interface {p1}, LX/BO1;->I()I

    move-result v2

    invoke-static {v0}, LX/FYy;->d(LX/FYy;)I

    move-result v5

    if-ge v2, v5, :cond_7

    .line 2256292
    const v1, 0x7f021ae7

    goto/16 :goto_1

    .line 2256293
    :cond_7
    const/16 v2, 0x32

    if-ge v1, v2, :cond_8

    .line 2256294
    const v1, 0x7f021ae9

    goto/16 :goto_1

    .line 2256295
    :cond_8
    const/16 v2, 0x4b

    if-ge v1, v2, :cond_9

    .line 2256296
    const v1, 0x7f021aea

    goto/16 :goto_1

    .line 2256297
    :cond_9
    invoke-static {v0}, LX/FYy;->e(LX/FYy;)I

    move-result v2

    if-ge v1, v2, :cond_a

    .line 2256298
    const v1, 0x7f021aeb

    goto/16 :goto_1

    .line 2256299
    :cond_a
    const v1, 0x7f021ae8

    goto/16 :goto_1

    .line 2256300
    :cond_b
    invoke-interface {p1}, LX/BO1;->o()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_c

    .line 2256301
    const v0, 0x7f021ae7

    goto :goto_3

    .line 2256302
    :cond_c
    const v0, 0x7f021ae8

    goto :goto_3
.end method

.method public static y(LX/FYb;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2256251
    iget-wide v0, p0, LX/FYb;->R:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    if-nez v0, :cond_1

    .line 2256252
    :cond_0
    iget-object v0, p0, LX/FYb;->Q:Ljava/lang/String;

    .line 2256253
    :goto_0
    return-object v0

    .line 2256254
    :cond_1
    sget-object v0, LX/FYV;->a:[I

    iget-object v1, p0, LX/FYb;->S:LX/2fs;

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2256255
    :cond_2
    iget-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    if-ne v0, v2, :cond_5

    const v0, 0x7f08341b

    :goto_1
    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, LX/FYb;->S:LX/2fs;

    invoke-static {v3}, LX/15V;->a(LX/2fs;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    iget-wide v4, p0, LX/FYb;->R:J

    invoke-static {v4, v5}, LX/FYb;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2256256
    :pswitch_0
    iget-object v0, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v1, 0x7f08341c

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, LX/FYb;->Q:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-wide v4, p0, LX/FYb;->R:J

    invoke-static {v4, v5}, LX/FYb;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2256257
    :pswitch_1
    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-object v0, v0, LX/2fs;->d:LX/2ft;

    sget-object v1, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    if-ne v0, v1, :cond_4

    .line 2256258
    iget-object v0, p0, LX/FYb;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0df;->V:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2256259
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2256260
    iget-object v0, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v1, 0x7f0819fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2256261
    :cond_3
    iget-object v1, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v2, 0x7f0819fe

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2256262
    :cond_4
    iget-object v0, p0, LX/FYb;->S:LX/2fs;

    iget-object v0, v0, LX/2fs;->d:LX/2ft;

    sget-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    if-ne v0, v1, :cond_2

    .line 2256263
    invoke-direct {p0}, LX/FYb;->z()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2256264
    :cond_5
    const v0, 0x7f08341a

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private z()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2256235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2256236
    iget-object v1, p0, LX/FYb;->L:LX/15X;

    iget-object v2, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    invoke-virtual {v2}, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/15X;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2256237
    iget-object v1, p0, LX/FYb;->S:LX/2fs;

    iget-wide v2, v1, LX/2fs;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 2256238
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2256239
    iget-object v1, p0, LX/FYb;->S:LX/2fs;

    invoke-static {v1}, LX/15V;->c(LX/2fs;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2256240
    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2256241
    iget-wide v2, p0, LX/FYb;->R:J

    invoke-static {v2, v3}, LX/FYb;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2256242
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/BO1;I)V
    .locals 1

    .prologue
    .line 2256233
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/FYb;->a(LX/BO1;II)V

    .line 2256234
    return-void
.end method

.method public final a(LX/BO1;II)V
    .locals 11

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2256179
    if-nez p2, :cond_0

    iget-object v0, p0, LX/FYb;->T:LX/19w;

    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/19w;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2256180
    iget-object v0, p0, LX/FYb;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    iget-object v2, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    invoke-virtual {v2}, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/FYb;->l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v4, LX/0i1;

    iget-object v5, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    .line 2256181
    iget-object p2, v5, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v5, p2

    .line 2256182
    invoke-virtual {v0, v2, v3, v4, v5}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2256183
    :cond_0
    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/FYb;->O:Ljava/lang/String;

    .line 2256184
    iget-object v0, p0, LX/FYb;->T:LX/19w;

    iget-object v2, p0, LX/FYb;->O:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v0

    iput-object v0, p0, LX/FYb;->S:LX/2fs;

    .line 2256185
    invoke-direct {p0, p1}, LX/FYb;->a(LX/BO1;)V

    .line 2256186
    invoke-direct {p0, p3}, LX/FYb;->c(I)V

    .line 2256187
    invoke-direct {p0, p1}, LX/FYb;->c(LX/BO1;)V

    .line 2256188
    iget-object v0, p0, LX/FYb;->t:LX/FYm;

    .line 2256189
    iget-object v2, v0, LX/FYm;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    if-eqz v2, :cond_1

    .line 2256190
    iget-object v2, v0, LX/FYm;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setVisibility(I)V

    .line 2256191
    :cond_1
    iget-object v2, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    invoke-interface {p1}, LX/BO1;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p1}, LX/BO1;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2256192
    invoke-interface {p1}, LX/AU0;->b()I

    move-result v2

    .line 2256193
    iget-object v0, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    iget-object v3, p0, LX/FYb;->n:LX/FYX;

    invoke-virtual {v3, p1, v2}, LX/EkQ;->a(LX/AU0;I)LX/EkQ;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256194
    iget-object v0, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    iget-object v3, p0, LX/FYb;->p:LX/FYY;

    .line 2256195
    iput-object p1, v3, LX/EkR;->b:LX/AU0;

    .line 2256196
    iput v2, v3, LX/EkR;->a:I

    .line 2256197
    invoke-interface {p1}, LX/AU0;->d()J

    move-result-wide v9

    iput-wide v9, v3, LX/EkR;->c:J

    .line 2256198
    move-object v3, v3

    .line 2256199
    invoke-virtual {v0, v3}, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2256200
    iget-object v0, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    .line 2256201
    iget-object v3, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v3

    .line 2256202
    iget-object v3, p0, LX/FYb;->o:LX/FYW;

    invoke-virtual {v3, p1, v2}, LX/EkQ;->a(LX/AU0;I)LX/EkQ;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256203
    iget-object v0, p0, LX/FYb;->D:LX/FYP;

    iget-object v0, v0, LX/FYP;->a:LX/3Af;

    if-eqz v0, :cond_2

    .line 2256204
    iget-object v0, p0, LX/FYb;->D:LX/FYP;

    iget-object v0, v0, LX/FYP;->a:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->dismiss()V

    .line 2256205
    iget-object v0, p0, LX/FYb;->D:LX/FYP;

    iput-object v1, v0, LX/FYP;->a:LX/3Af;

    .line 2256206
    :cond_2
    iget-object v0, p0, LX/FYb;->m:Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, LX/BO1;->U()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/FYb;->s:Landroid/graphics/drawable/Drawable;

    :cond_3
    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2256207
    invoke-interface {p1}, LX/BO1;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    move-result-object v1

    .line 2256208
    iget-object v0, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v3, 0x7f0a010d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2256209
    const-string v3, "#%06X"

    new-array v4, v7, [Ljava/lang/Object;

    const v5, 0xffffff

    and-int/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2256210
    invoke-static {p1}, LX/FZ3;->b(LX/BO1;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/FYb;->I:LX/0ad;

    sget-short v4, LX/0wh;->U:S

    invoke-interface {v0, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, LX/BO1;->E()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, LX/BO1;->D()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-interface {p1}, LX/BO1;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 2256211
    :cond_4
    invoke-interface {p1}, LX/BO1;->D()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, LX/BO1;->D()Ljava/lang/String;

    move-result-object v0

    .line 2256212
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<font color=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</font>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2256213
    iget-object v4, p0, LX/FYb;->C:Lcom/facebook/widget/text/BetterTextView;

    iget-object v5, p0, LX/FYb;->N:Landroid/content/res/Resources;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->POST:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    if-ne v1, v0, :cond_9

    const v0, 0x7f083422

    :goto_4
    new-array v1, v7, [Ljava/lang/Object;

    aput-object v3, v1, v6

    invoke-virtual {v5, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2256214
    iget-object v0, p0, LX/FYb;->B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-interface {p1}, LX/BO1;->E()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2256215
    iget-object v0, p0, LX/FYb;->A:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2256216
    iget-object v0, p0, LX/FYb;->A:Landroid/view/View;

    iget-object v1, p0, LX/FYb;->q:LX/FYZ;

    invoke-virtual {v1, p1, v2}, LX/EkQ;->a(LX/AU0;I)LX/EkQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256217
    :goto_5
    return-void

    :cond_5
    move-object v0, v1

    .line 2256218
    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    .line 2256219
    goto/16 :goto_1

    .line 2256220
    :cond_7
    iget-object v1, p0, LX/FYb;->r:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_2

    .line 2256221
    :cond_8
    invoke-interface {p1}, LX/BO1;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2256222
    :cond_9
    const v0, 0x7f083423

    goto :goto_4

    .line 2256223
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->QUICK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    if-ne v1, v0, :cond_d

    .line 2256224
    invoke-interface {p1}, LX/BO1;->Z()Ljava/lang/String;

    move-result-object v0

    .line 2256225
    if-eqz v0, :cond_b

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2256226
    :cond_b
    iget-object v0, p0, LX/FYb;->A:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 2256227
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "<font color=\'"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2256228
    iget-object v1, p0, LX/FYb;->C:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, LX/FYb;->N:Landroid/content/res/Resources;

    const v4, 0x7f083424

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2256229
    iget-object v0, p0, LX/FYb;->B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2256230
    iget-object v0, p0, LX/FYb;->A:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2256231
    iget-object v0, p0, LX/FYb;->A:Landroid/view/View;

    iget-object v1, p0, LX/FYb;->n:LX/FYX;

    invoke-virtual {v1, p1, v2}, LX/EkQ;->a(LX/AU0;I)LX/EkQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 2256232
    :cond_d
    iget-object v0, p0, LX/FYb;->A:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5
.end method

.method public final w()V
    .locals 3

    .prologue
    .line 2256177
    iget-object v0, p0, LX/FYb;->G:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/FYb;->H:LX/FYa;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 2256178
    return-void
.end method

.method public final x()V
    .locals 3

    .prologue
    .line 2256171
    iget-object v0, p0, LX/FYb;->G:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/FYb;->H:LX/FYa;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 2256172
    iget-object v0, p0, LX/FYb;->t:LX/FYm;

    const/4 v2, 0x0

    .line 2256173
    iget-object v1, v0, LX/FYm;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    if-eqz v1, :cond_0

    .line 2256174
    iget-object v1, v0, LX/FYm;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256175
    iget-object v1, v0, LX/FYm;->f:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256176
    :cond_0
    return-void
.end method
