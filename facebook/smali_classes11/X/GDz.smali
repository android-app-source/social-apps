.class public final LX/GDz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GCY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;LX/GCY;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2330708
    iput-object p1, p0, LX/GDz;->d:Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;

    iput-object p2, p0, LX/GDz;->a:LX/GCY;

    iput-object p3, p0, LX/GDz;->b:Ljava/lang/String;

    iput-object p4, p0, LX/GDz;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2330709
    iget-object v0, p0, LX/GDz;->d:Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;

    iget-object v0, v0, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->d:LX/GF4;

    new-instance v1, LX/GFO;

    invoke-direct {v1, p1}, LX/GFO;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2330710
    iget-object v0, p0, LX/GDz;->a:LX/GCY;

    iget-object v1, p0, LX/GDz;->d:Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/GCY;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2330711
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2330712
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2330713
    if-eqz p1, :cond_0

    .line 2330714
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330715
    if-nez v0, :cond_1

    .line 2330716
    :cond_0
    iget-object v0, p0, LX/GDz;->a:LX/GCY;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2330717
    :goto_0
    return-void

    .line 2330718
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330719
    check-cast v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;

    .line 2330720
    iget-object v1, p0, LX/GDz;->d:Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;

    iget-object v2, p0, LX/GDz;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    .line 2330721
    iget-object v1, p0, LX/GDz;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c(Ljava/lang/String;)V

    .line 2330722
    iget-object v1, p0, LX/GDz;->d:Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;

    iget-object v1, v1, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->f:LX/GG6;

    invoke-virtual {v1, v0}, LX/GG6;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2330723
    iget-object v1, p0, LX/GDz;->a:LX/GCY;

    invoke-interface {v1, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    goto :goto_0
.end method
