.class public final LX/H60;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/18V;

.field public final c:LX/H5z;

.field private final d:LX/0y2;


# direct methods
.method public constructor <init>(LX/0tX;LX/18V;LX/H5z;LX/0y2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2425687
    iput-object p1, p0, LX/H60;->a:LX/0tX;

    .line 2425688
    iput-object p2, p0, LX/H60;->b:LX/18V;

    .line 2425689
    iput-object p3, p0, LX/H60;->c:LX/H5z;

    .line 2425690
    iput-object p4, p0, LX/H60;->d:LX/0y2;

    .line 2425691
    return-void
.end method

.method public static a(LX/0QB;)LX/H60;
    .locals 7

    .prologue
    .line 2425725
    const-class v1, LX/H60;

    monitor-enter v1

    .line 2425726
    :try_start_0
    sget-object v0, LX/H60;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2425727
    sput-object v2, LX/H60;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2425728
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425729
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2425730
    new-instance p0, LX/H60;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    .line 2425731
    new-instance v5, LX/H5z;

    invoke-direct {v5}, LX/H5z;-><init>()V

    .line 2425732
    move-object v5, v5

    .line 2425733
    move-object v5, v5

    .line 2425734
    check-cast v5, LX/H5z;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v6

    check-cast v6, LX/0y2;

    invoke-direct {p0, v3, v4, v5, v6}, LX/H60;-><init>(LX/0tX;LX/18V;LX/H5z;LX/0y2;)V

    .line 2425735
    move-object v0, p0

    .line 2425736
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2425737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H60;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2425738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2425739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(IZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2425740
    sget-object v0, LX/0zS;->a:LX/0zS;

    .line 2425741
    if-eqz p2, :cond_0

    .line 2425742
    sget-object v0, LX/0zS;->d:LX/0zS;

    .line 2425743
    :cond_0
    new-instance v1, LX/H6s;

    invoke-direct {v1}, LX/H6s;-><init>()V

    move-object v1, v1

    .line 2425744
    const-string v2, "profile_pic_width"

    const/16 v3, 0x64

    div-int/lit8 v4, p1, 0x4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "creative_img_size"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2425745
    iget-object v2, p0, LX/H60;->d:LX/0y2;

    invoke-virtual {v2}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v2

    .line 2425746
    if-eqz v2, :cond_1

    .line 2425747
    const-string v3, "center_lat"

    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2425748
    const-string v3, "center_long"

    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2425749
    :cond_1
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2425750
    iget-object v1, p0, LX/H60;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/H70;IZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "markOfferClaimUsed"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/H70;",
            "IZ",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2425706
    const/4 v0, 0x0

    .line 2425707
    if-eqz p2, :cond_0

    .line 2425708
    invoke-static {p2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->a(LX/H70;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    invoke-static {v0}, LX/H78;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;)LX/H78;

    move-result-object v0

    .line 2425709
    iput-boolean p4, v0, LX/H78;->h:Z

    .line 2425710
    move-object v0, v0

    .line 2425711
    invoke-virtual {v0}, LX/H78;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    .line 2425712
    :cond_0
    new-instance v1, LX/4HT;

    invoke-direct {v1}, LX/4HT;-><init>()V

    .line 2425713
    const-string v2, "offer_claim_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425714
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2425715
    const-string v3, "used"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2425716
    const-string v2, "actor_id"

    invoke-virtual {v1, v2, p5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425717
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2425718
    const-string v3, "client_mutation_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425719
    new-instance v2, LX/H6k;

    invoke-direct {v2}, LX/H6k;-><init>()V

    move-object v2, v2

    .line 2425720
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    const-string v3, "profile_pic_width"

    const/16 v4, 0x64

    div-int/lit8 v5, p3, 0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v3, "creative_img_size"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2425721
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2425722
    if-eqz v0, :cond_1

    .line 2425723
    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    .line 2425724
    :cond_1
    iget-object v0, p0, LX/H60;->a:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2425692
    new-instance v0, LX/4HU;

    invoke-direct {v0}, LX/4HU;-><init>()V

    .line 2425693
    const-string v1, "offer_view_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425694
    const-string v1, "share_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425695
    const-string v1, "actor_id"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425696
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2425697
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425698
    new-instance v1, LX/H6l;

    invoke-direct {v1}, LX/H6l;-><init>()V

    move-object v1, v1

    .line 2425699
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v2, "profile_pic_width"

    const/16 v3, 0x64

    div-int/lit8 v4, p3, 0x4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "creative_img_size"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2425700
    iget-object v0, p0, LX/H60;->d:LX/0y2;

    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 2425701
    if-eqz v0, :cond_0

    .line 2425702
    const-string v2, "center_lat"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2425703
    const-string v2, "center_long"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2425704
    :cond_0
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2425705
    iget-object v1, p0, LX/H60;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
