.class public LX/FYm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/ViewStub;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;

.field public d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field public e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public f:Lcom/facebook/fbui/widget/text/GlyphWithTextView;


# direct methods
.method public constructor <init>(Landroid/view/ViewStub;LX/0Ot;LX/0ad;)V
    .locals 0
    .param p1    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewStub;",
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2256501
    iput-object p1, p0, LX/FYm;->a:Landroid/view/ViewStub;

    .line 2256502
    iput-object p2, p0, LX/FYm;->b:LX/0Ot;

    .line 2256503
    iput-object p3, p0, LX/FYm;->c:LX/0ad;

    .line 2256504
    return-void
.end method

.method public static b(LX/BO1;)Z
    .locals 2

    .prologue
    .line 2256484
    invoke-interface {p0}, LX/BO1;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/BO1;)Z
    .locals 1

    .prologue
    .line 2256499
    invoke-static {p0}, LX/FZ3;->a(LX/BO1;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/FragmentActivity;LX/BO1;)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    .line 2256485
    const/4 v0, 0x0

    .line 2256486
    iget-object v1, p0, LX/FYm;->c:LX/0ad;

    sget-short v2, LX/0wh;->H:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p2}, LX/FYm;->b(LX/BO1;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, LX/FYm;->c(LX/BO1;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 2256487
    if-eqz v0, :cond_2

    .line 2256488
    invoke-interface {p2}, LX/AUV;->d()J

    move-result-wide v4

    .line 2256489
    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v6

    .line 2256490
    invoke-interface {p2}, LX/BO1;->W()Ljava/lang/String;

    move-result-object v7

    .line 2256491
    invoke-interface {p2}, LX/BO1;->X()I

    move-result v0

    if-ne v0, v8, :cond_3

    .line 2256492
    :goto_0
    invoke-static {p2}, LX/FZ3;->a(LX/BO1;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v11

    .line 2256493
    invoke-static {p2}, LX/FYm;->b(LX/BO1;)Z

    move-result v3

    .line 2256494
    invoke-static {p2}, LX/FYm;->c(LX/BO1;)Z

    move-result v9

    .line 2256495
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 2256496
    new-instance v1, Lcom/facebook/saved2/ui/itemadapters/Saved2PostConsumeViewController$1;

    move-object v2, p0

    move-object v10, p1

    invoke-direct/range {v1 .. v11}, Lcom/facebook/saved2/ui/itemadapters/Saved2PostConsumeViewController$1;-><init>(LX/FYm;ZJLjava/lang/String;Ljava/lang/String;ZZLandroid/support/v4/app/FragmentActivity;Lcom/facebook/graphql/model/GraphQLEntity;)V

    const-wide/16 v2, 0x7d0

    const v4, -0x1e4a5217

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2256497
    :cond_2
    return-void

    .line 2256498
    :cond_3
    const/4 v8, 0x0

    goto :goto_0
.end method
