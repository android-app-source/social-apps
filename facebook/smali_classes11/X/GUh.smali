.class public final enum LX/GUh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GUh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GUh;

.field public static final enum LOCATION_SHARING_PAUSE:LX/GUh;

.field public static final enum LOCATION_SHARING_PRIVACY_READING:LX/GUh;

.field public static final enum LOCATION_SHARING_PRIVACY_WRITING:LX/GUh;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2357507
    new-instance v0, LX/GUh;

    const-string v1, "LOCATION_SHARING_PRIVACY_WRITING"

    invoke-direct {v0, v1, v2}, LX/GUh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GUh;->LOCATION_SHARING_PRIVACY_WRITING:LX/GUh;

    .line 2357508
    new-instance v0, LX/GUh;

    const-string v1, "LOCATION_SHARING_PRIVACY_READING"

    invoke-direct {v0, v1, v3}, LX/GUh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GUh;->LOCATION_SHARING_PRIVACY_READING:LX/GUh;

    .line 2357509
    new-instance v0, LX/GUh;

    const-string v1, "LOCATION_SHARING_PAUSE"

    invoke-direct {v0, v1, v4}, LX/GUh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GUh;->LOCATION_SHARING_PAUSE:LX/GUh;

    .line 2357510
    const/4 v0, 0x3

    new-array v0, v0, [LX/GUh;

    sget-object v1, LX/GUh;->LOCATION_SHARING_PRIVACY_WRITING:LX/GUh;

    aput-object v1, v0, v2

    sget-object v1, LX/GUh;->LOCATION_SHARING_PRIVACY_READING:LX/GUh;

    aput-object v1, v0, v3

    sget-object v1, LX/GUh;->LOCATION_SHARING_PAUSE:LX/GUh;

    aput-object v1, v0, v4

    sput-object v0, LX/GUh;->$VALUES:[LX/GUh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2357513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GUh;
    .locals 1

    .prologue
    .line 2357512
    const-class v0, LX/GUh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GUh;

    return-object v0
.end method

.method public static values()[LX/GUh;
    .locals 1

    .prologue
    .line 2357511
    sget-object v0, LX/GUh;->$VALUES:[LX/GUh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GUh;

    return-object v0
.end method
