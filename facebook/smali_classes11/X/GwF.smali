.class public LX/GwF;
.super LX/4hd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4hd",
        "<",
        "LX/GwD;",
        "LX/GwW;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GwE;


# direct methods
.method public constructor <init>(LX/GwE;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406801
    const-class v0, LX/GwW;

    const-string v1, "com.facebook.platform.action.request.SHARE_DIALOG"

    invoke-direct {p0, v0, v1}, LX/4hd;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2406802
    iput-object p1, p0, LX/GwF;->a:LX/GwE;

    .line 2406803
    return-void
.end method


# virtual methods
.method public final b(Landroid/app/Activity;LX/4hh;)LX/4hY;
    .locals 7

    .prologue
    .line 2406804
    check-cast p2, LX/GwW;

    .line 2406805
    iget-object v0, p0, LX/GwF;->a:LX/GwE;

    .line 2406806
    new-instance v1, LX/GwD;

    invoke-static {v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static {v0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v6

    check-cast v6, LX/1nC;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, LX/GwD;-><init>(Landroid/app/Activity;LX/GwW;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/1Kf;LX/1nC;)V

    .line 2406807
    move-object v0, v1

    .line 2406808
    return-object v0
.end method

.method public final b()LX/4hh;
    .locals 1

    .prologue
    .line 2406800
    new-instance v0, LX/GwW;

    invoke-direct {v0}, LX/GwW;-><init>()V

    return-object v0
.end method
