.class public final LX/GpA;
.super LX/Gos;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Gos",
        "<",
        "Lcom/facebook/instantshopping/model/data/InstantShoppingMapBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:D

.field public final h:D

.field public final i:D

.field public final j:D

.field public final k:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CHs;II)V
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2394762
    move-object v0, p1

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    invoke-direct {p0, v0, p2, p3}, LX/Gos;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    .line 2394763
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/GpA;->a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394764
    invoke-interface {p1}, LX/CHs;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->b:Ljava/lang/String;

    .line 2394765
    invoke-interface {p1}, LX/CHs;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->c:Ljava/lang/String;

    .line 2394766
    invoke-interface {p1}, LX/CHs;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->d:Ljava/lang/String;

    .line 2394767
    invoke-interface {p1}, LX/CHr;->t()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->e:Ljava/lang/String;

    .line 2394768
    invoke-interface {p1}, LX/CHs;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    invoke-virtual {v1, v0, v5, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->f:Ljava/lang/String;

    .line 2394769
    invoke-interface {p1}, LX/CHs;->u()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v4}, LX/15i;->l(II)D

    move-result-wide v0

    iput-wide v0, p0, LX/GpA;->g:D

    .line 2394770
    invoke-interface {p1}, LX/CHs;->u()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v5}, LX/15i;->l(II)D

    move-result-wide v0

    iput-wide v0, p0, LX/GpA;->h:D

    .line 2394771
    invoke-interface {p1}, LX/CHs;->u()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v6}, LX/15i;->l(II)D

    move-result-wide v0

    iput-wide v0, p0, LX/GpA;->i:D

    .line 2394772
    invoke-interface {p1}, LX/CHs;->u()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->l(II)D

    move-result-wide v0

    iput-wide v0, p0, LX/GpA;->j:D

    .line 2394773
    invoke-interface {p1}, LX/CHs;->w()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v5, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    iput-object v0, p0, LX/GpA;->k:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    .line 2394774
    invoke-interface {p1}, LX/CHs;->w()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->l:Ljava/lang/String;

    .line 2394775
    invoke-interface {p1}, LX/CHs;->w()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->m:Ljava/lang/String;

    .line 2394776
    invoke-interface {p1}, LX/CHr;->s()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->c()Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->n:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 2394777
    invoke-interface {p1}, LX/CHr;->s()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GpA;->o:LX/0Px;

    .line 2394778
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;
    .locals 1

    .prologue
    .line 2394779
    iput-object p1, p0, LX/GpA;->a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394780
    move-object v0, p0

    .line 2394781
    return-object v0
.end method

.method public final synthetic b()LX/Clr;
    .locals 1

    .prologue
    .line 2394782
    invoke-virtual {p0}, LX/GpA;->d()LX/GpB;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 2394783
    iget-object v0, p0, LX/GpA;->a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method public final d()LX/GpB;
    .locals 2

    .prologue
    .line 2394784
    new-instance v0, LX/GpB;

    invoke-direct {v0, p0}, LX/GpB;-><init>(LX/GpA;)V

    return-object v0
.end method
