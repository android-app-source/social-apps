.class public final enum LX/Fs1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fs1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fs1;

.field public static final enum CANCELLED:LX/Fs1;

.field public static final enum PAUSED:LX/Fs1;

.field public static final enum REFRESH_ON_RESUME:LX/Fs1;

.field public static final enum VISIBLE:LX/Fs1;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2296509
    new-instance v0, LX/Fs1;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2}, LX/Fs1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fs1;->VISIBLE:LX/Fs1;

    .line 2296510
    new-instance v0, LX/Fs1;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, LX/Fs1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fs1;->PAUSED:LX/Fs1;

    .line 2296511
    new-instance v0, LX/Fs1;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4}, LX/Fs1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fs1;->CANCELLED:LX/Fs1;

    .line 2296512
    new-instance v0, LX/Fs1;

    const-string v1, "REFRESH_ON_RESUME"

    invoke-direct {v0, v1, v5}, LX/Fs1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fs1;->REFRESH_ON_RESUME:LX/Fs1;

    .line 2296513
    const/4 v0, 0x4

    new-array v0, v0, [LX/Fs1;

    sget-object v1, LX/Fs1;->VISIBLE:LX/Fs1;

    aput-object v1, v0, v2

    sget-object v1, LX/Fs1;->PAUSED:LX/Fs1;

    aput-object v1, v0, v3

    sget-object v1, LX/Fs1;->CANCELLED:LX/Fs1;

    aput-object v1, v0, v4

    sget-object v1, LX/Fs1;->REFRESH_ON_RESUME:LX/Fs1;

    aput-object v1, v0, v5

    sput-object v0, LX/Fs1;->$VALUES:[LX/Fs1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2296514
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fs1;
    .locals 1

    .prologue
    .line 2296515
    const-class v0, LX/Fs1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fs1;

    return-object v0
.end method

.method public static values()[LX/Fs1;
    .locals 1

    .prologue
    .line 2296516
    sget-object v0, LX/Fs1;->$VALUES:[LX/Fs1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fs1;

    return-object v0
.end method
