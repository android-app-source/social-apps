.class public final LX/GkI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GkH;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;)V
    .locals 0

    .prologue
    .line 2388960
    iput-object p1, p0, LX/GkI;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2388961
    sget-object v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->f:Ljava/lang/String;

    const-string v1, "Unable to fetch groups from groupsstore"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2388962
    return-void
.end method

.method public final a(Ljava/util/HashMap;ZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 2388963
    if-nez p2, :cond_0

    .line 2388964
    iget-object v0, p0, LX/GkI;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    iget-object v0, v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->h:LX/GkA;

    .line 2388965
    iput-object p1, v0, LX/GkA;->b:Ljava/util/HashMap;

    .line 2388966
    iget-object p0, v0, LX/GkA;->a:LX/Gk6;

    .line 2388967
    iput-object p1, p0, LX/Gk6;->b:Ljava/util/HashMap;

    .line 2388968
    const p0, 0x3045ae48

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2388969
    :cond_0
    return-void
.end method
