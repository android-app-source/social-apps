.class public LX/GUB;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOption;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2357064
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2357065
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2357066
    iput-object v0, p0, LX/GUB;->a:LX/0Px;

    .line 2357067
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GUB;->b:Z

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2357068
    new-instance v0, LX/GUA;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GUA;-><init>(Landroid/content/Context;)V

    .line 2357069
    iget-boolean v1, p0, LX/GUB;->b:Z

    .line 2357070
    iget-object p1, v0, LX/GUA;->k:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v1, :cond_0

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {p1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2357071
    return-object v0

    .line 2357072
    :cond_0
    const/16 p0, 0x8

    goto :goto_0
.end method

.method public final a(I)Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;
    .locals 1

    .prologue
    .line 2357063
    iget-object v0, p0, LX/GUB;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2357073
    check-cast p3, LX/GUA;

    .line 2357074
    check-cast p2, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357075
    invoke-virtual {p2}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->b()LX/1Fd;

    move-result-object p0

    invoke-interface {p0}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object p0

    .line 2357076
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object p0

    .line 2357077
    sget-object p4, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-static {p0, p4}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result p0

    .line 2357078
    iget-object p1, p3, LX/GUA;->k:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2357079
    iget-object p0, p3, LX/GUA;->l:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2357080
    invoke-virtual {p2}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p3, p0}, LX/GUA;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2357081
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2357060
    iput-object p1, p0, LX/GUB;->a:LX/0Px;

    .line 2357061
    const v0, 0x6639d4cc

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2357062
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2357059
    iget-object v0, p0, LX/GUB;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2357057
    invoke-virtual {p0, p1}, LX/GUB;->a(I)Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2357058
    int-to-long v0, p1

    return-wide v0
.end method
