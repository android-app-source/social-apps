.class public final LX/F1M;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 2191616
    iput-object p1, p0, LX/F1M;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;

    iput-object p2, p0, LX/F1M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2191617
    iget-object v0, p0, LX/F1M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191618
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2191619
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2191620
    iget-object v4, p0, LX/F1M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v4, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 2191621
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2191622
    if-eqz v0, :cond_0

    .line 2191623
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2191624
    iget-object v0, p0, LX/F1M;->b:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoStoriesHScrollPartDefinition;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;

    invoke-virtual {p1, v0, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2191625
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2191626
    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 2191627
    return-void
.end method
