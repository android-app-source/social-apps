.class public LX/HDG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HDF;


# instance fields
.field private final a:LX/HC0;


# direct methods
.method public constructor <init>(LX/HC0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2441063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2441064
    iput-object p1, p0, LX/HDG;->a:LX/HC0;

    .line 2441065
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;JLjava/lang/String;LX/HDH;LX/HBj;)V
    .locals 6

    .prologue
    .line 2441066
    sget-object v0, LX/HDH;->EDIT:LX/HDH;

    if-ne p5, v0, :cond_0

    .line 2441067
    invoke-interface {p6}, LX/HBj;->a()V

    .line 2441068
    iget-object v0, p0, LX/HDG;->a:LX/HC0;

    .line 2441069
    sget-object v1, LX/8Dq;->c:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2441070
    iget-object v2, v0, LX/HC0;->c:LX/17Y;

    iget-object v3, v0, LX/HC0;->a:Landroid/content/Context;

    invoke-interface {v2, v3, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2441071
    iget-object v2, v0, LX/HC0;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/HC0;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2441072
    :goto_0
    return-void

    .line 2441073
    :cond_0
    sget-object v0, LX/HDH;->ADD:LX/HDH;

    if-ne p5, v0, :cond_1

    .line 2441074
    iget-object v0, p0, LX/HDG;->a:LX/HC0;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, LX/HC0;->a(Landroid/app/Activity;JLcom/facebook/graphql/enums/GraphQLPageActionType;LX/HBj;)V

    goto :goto_0

    .line 2441075
    :cond_1
    invoke-interface {p6}, LX/HBj;->b()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldShowEditSection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2441076
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2441077
    const/4 v0, 0x1

    return v0
.end method

.method public final d()I
    .locals 1
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 2441078
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v0, v0

    .line 2441079
    invoke-static {v0}, LX/HCF;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 2441080
    const v0, 0x7f0836cc

    return v0
.end method
