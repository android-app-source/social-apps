.class public LX/GfT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/GfR;

.field private final b:LX/GfW;

.field private final c:LX/3mL;

.field public final d:LX/1LV;


# direct methods
.method public constructor <init>(LX/GfR;LX/GfW;LX/3mL;LX/1LV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2377436
    iput-object p1, p0, LX/GfT;->a:LX/GfR;

    .line 2377437
    iput-object p2, p0, LX/GfT;->b:LX/GfW;

    .line 2377438
    iput-object p3, p0, LX/GfT;->c:LX/3mL;

    .line 2377439
    iput-object p4, p0, LX/GfT;->d:LX/1LV;

    .line 2377440
    return-void
.end method

.method public static a(LX/0QB;)LX/GfT;
    .locals 7

    .prologue
    .line 2377441
    const-class v1, LX/GfT;

    monitor-enter v1

    .line 2377442
    :try_start_0
    sget-object v0, LX/GfT;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377443
    sput-object v2, LX/GfT;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377444
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377445
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377446
    new-instance p0, LX/GfT;

    const-class v3, LX/GfR;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/GfR;

    invoke-static {v0}, LX/GfW;->a(LX/0QB;)LX/GfW;

    move-result-object v4

    check-cast v4, LX/GfW;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v5

    check-cast v5, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v6

    check-cast v6, LX/1LV;

    invoke-direct {p0, v3, v4, v5, v6}, LX/GfT;-><init>(LX/GfR;LX/GfW;LX/3mL;LX/1LV;)V

    .line 2377447
    move-object v0, p0

    .line 2377448
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377449
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GfT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377450
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377451
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;IZ)LX/1Dg;
    .locals 8
    .param p2    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;",
            ">;IZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2377452
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2377453
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;->u()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2377454
    new-instance v0, LX/GfS;

    invoke-direct {v0, p0, p3}, LX/GfS;-><init>(LX/GfT;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2377455
    const/high16 v1, 0x40c00000    # 6.0f

    invoke-static {p1, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int v6, p4, v1

    .line 2377456
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v1

    .line 2377457
    iput-object v0, v1, LX/3mP;->g:LX/25K;

    .line 2377458
    move-object v0, v1

    .line 2377459
    const/4 v1, 0x0

    .line 2377460
    iput-boolean v1, v0, LX/3mP;->a:Z

    .line 2377461
    move-object v1, v0

    .line 2377462
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2377463
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v0

    .line 2377464
    iput-object v0, v1, LX/3mP;->d:LX/25L;

    .line 2377465
    move-object v1, v1

    .line 2377466
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2377467
    check-cast v0, LX/0jW;

    .line 2377468
    iput-object v0, v1, LX/3mP;->e:LX/0jW;

    .line 2377469
    move-object v0, v1

    .line 2377470
    const/16 v1, 0x8

    .line 2377471
    iput v1, v0, LX/3mP;->b:I

    .line 2377472
    move-object v0, v0

    .line 2377473
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v5

    .line 2377474
    iget-object v0, p0, LX/GfT;->a:LX/GfR;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move v7, p5

    invoke-virtual/range {v0 .. v7}, LX/GfR;->a(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/Object;LX/25M;IZ)LX/GfQ;

    move-result-object v0

    .line 2377475
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/GfT;->b:LX/GfW;

    const/4 v3, 0x0

    .line 2377476
    new-instance v4, LX/GfV;

    invoke-direct {v4, v2}, LX/GfV;-><init>(LX/GfW;)V

    .line 2377477
    iget-object v5, v2, LX/GfW;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GfU;

    .line 2377478
    if-nez v5, :cond_0

    .line 2377479
    new-instance v5, LX/GfU;

    invoke-direct {v5, v2}, LX/GfU;-><init>(LX/GfW;)V

    .line 2377480
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/GfU;->a$redex0(LX/GfU;LX/1De;IILX/GfV;)V

    .line 2377481
    move-object v4, v5

    .line 2377482
    move-object v3, v4

    .line 2377483
    move-object v2, v3

    .line 2377484
    iget-object v3, v2, LX/GfU;->a:LX/GfV;

    iput-object p3, v3, LX/GfV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377485
    iget-object v3, v2, LX/GfU;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2377486
    move-object v2, v2

    .line 2377487
    iget-object v3, v2, LX/GfU;->a:LX/GfV;

    iput-object p2, v3, LX/GfV;->b:LX/1Pn;

    .line 2377488
    iget-object v3, v2, LX/GfU;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2377489
    move-object v2, v2

    .line 2377490
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/GfT;->c:LX/3mL;

    invoke-virtual {v2, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
