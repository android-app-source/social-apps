.class public LX/FOI;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements LX/FOF;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FOH;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/FOH;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIIZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2234999
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2235000
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2235001
    iput-object v0, p0, LX/FOI;->b:LX/0Px;

    .line 2235002
    iput-object p1, p0, LX/FOI;->c:Landroid/content/Context;

    .line 2235003
    iput p2, p0, LX/FOI;->d:I

    .line 2235004
    iput p3, p0, LX/FOI;->e:I

    .line 2235005
    iput p4, p0, LX/FOI;->f:I

    .line 2235006
    iput-boolean p5, p0, LX/FOI;->g:Z

    .line 2235007
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2234995
    iget-object v0, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOH;

    .line 2234996
    invoke-virtual {v0}, LX/FOH;->a()V

    .line 2234997
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2234998
    :cond_0
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2234960
    const/4 v3, 0x0

    .line 2234961
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 2234962
    :cond_0
    :goto_0
    move v0, v3

    .line 2234963
    if-nez v0, :cond_2

    .line 2234964
    iget v0, p0, LX/FOI;->f:I

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2234965
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2234966
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 2234967
    iget-object v0, p0, LX/FOI;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOH;

    .line 2234968
    iget-object v5, p0, LX/FOI;->c:Landroid/content/Context;

    iget v6, p0, LX/FOI;->d:I

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    iget-boolean v7, p0, LX/FOI;->g:Z

    .line 2234969
    iput-object v5, v0, LX/FOH;->b:Landroid/content/Context;

    .line 2234970
    iput v6, v0, LX/FOH;->d:I

    .line 2234971
    iput-object v1, v0, LX/FOH;->c:Lcom/facebook/user/model/UserKey;

    .line 2234972
    iput-boolean v7, v0, LX/FOH;->e:Z

    .line 2234973
    iget-object v8, v0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v8, v5, v9, v6, v10}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Landroid/content/Context;ZILX/8ug;)V

    .line 2234974
    iget-object v8, v0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-static {v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(LX/8t9;)V

    .line 2234975
    iget-object v8, v0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    new-instance v9, LX/FOG;

    invoke-direct {v9, v0}, LX/FOG;-><init>(LX/FOH;)V

    .line 2234976
    iput-object v9, v8, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    .line 2234977
    iget-object v8, v0, LX/FOH;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 2234978
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    iput-object v9, v0, LX/FOH;->f:Landroid/graphics/Paint;

    .line 2234979
    iget-object v9, v0, LX/FOH;->f:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2234980
    iget-object v9, v0, LX/FOH;->f:Landroid/graphics/Paint;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2234981
    iget-object v9, v0, LX/FOH;->f:Landroid/graphics/Paint;

    const v10, 0x7f0a0610

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 2234982
    iget-object v9, v0, LX/FOH;->f:Landroid/graphics/Paint;

    const v10, 0x7f0b125d

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v9, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2234983
    invoke-virtual {v0, p0}, LX/FOH;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2234984
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2234985
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2234986
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/FOI;->b:LX/0Px;

    .line 2234987
    :cond_2
    invoke-virtual {p0}, LX/FOI;->invalidateSelf()V

    .line 2234988
    return-void

    :cond_3
    move v2, v3

    .line 2234989
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2234990
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iget-object v1, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FOH;

    .line 2234991
    iget-object v4, v1, LX/FOH;->c:Lcom/facebook/user/model/UserKey;

    move-object v1, v4

    .line 2234992
    invoke-virtual {v0, v1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2234993
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2234994
    :cond_4
    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2234956
    iget-object v0, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOH;

    .line 2234957
    invoke-virtual {v0}, LX/FOH;->b()V

    .line 2234958
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2234959
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2234947
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2234948
    iget v0, p0, LX/FOI;->d:I

    iget v1, p0, LX/FOI;->e:I

    add-int v2, v0, v1

    .line 2234949
    iget-object v0, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2234950
    iget-object v0, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 2234951
    iget-object v0, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOH;

    invoke-virtual {v0, p1}, LX/FOH;->draw(Landroid/graphics/Canvas;)V

    .line 2234952
    neg-int v0, v2

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2234953
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2234954
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2234955
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2234945
    iget-object v1, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOH;

    invoke-virtual {v0}, LX/FOH;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final getIntrinsicWidth()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2235008
    iget-object v1, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2235009
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-object v2, p0, LX/FOI;->b:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOH;

    invoke-virtual {v0}, LX/FOH;->getIntrinsicWidth()I

    move-result v0

    iget v2, p0, LX/FOI;->e:I

    add-int/2addr v0, v2

    mul-int/2addr v0, v1

    iget v1, p0, LX/FOI;->e:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2234946
    const/4 v0, 0x0

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2234943
    invoke-virtual {p0}, LX/FOI;->invalidateSelf()V

    .line 2234944
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 2234941
    invoke-virtual {p0, p2, p3, p4}, LX/FOI;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 2234942
    return-void
.end method

.method public final setAlpha(I)V
    .locals 2

    .prologue
    .line 2234940
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Setting an alpha is not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234939
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Setting a color filter is not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2234937
    invoke-virtual {p0, p2}, LX/FOI;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 2234938
    return-void
.end method
