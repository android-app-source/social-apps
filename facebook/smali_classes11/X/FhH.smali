.class public abstract LX/FhH;
.super LX/7HT;
.source ""

# interfaces
.implements LX/Cwe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HT",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;",
        "LX/Cwe;"
    }
.end annotation


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhQ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/7BZ;

.field public final c:LX/Fhc;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhX;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2Sc;

.field private g:LX/Cvp;

.field public h:I


# direct methods
.method public constructor <init>(LX/7BZ;LX/1Ck;LX/2Sc;LX/Fhc;LX/0Ot;LX/0Ot;LX/0Ot;LX/7Hq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7BZ;",
            "LX/1Ck;",
            "LX/2Sc;",
            "LX/Fhc;",
            "LX/0Ot",
            "<",
            "LX/FhQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FhX;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/7Hq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2272522
    invoke-direct {p0, p2, p8}, LX/7HT;-><init>(LX/1Ck;LX/7Hq;)V

    .line 2272523
    iput-object p5, p0, LX/FhH;->a:LX/0Ot;

    .line 2272524
    iput-object p6, p0, LX/FhH;->d:LX/0Ot;

    .line 2272525
    iput-object p7, p0, LX/FhH;->e:LX/0Ot;

    .line 2272526
    iput-object p1, p0, LX/FhH;->b:LX/7BZ;

    .line 2272527
    iput-object p3, p0, LX/FhH;->f:LX/2Sc;

    .line 2272528
    iput-object p4, p0, LX/FhH;->c:LX/Fhc;

    .line 2272529
    return-void
.end method

.method public static d(LX/7B6;)I
    .locals 2

    .prologue
    .line 2272551
    iget-object v0, p0, LX/7B6;->d:LX/0P1;

    const-string v1, "DURATION_MS"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2272552
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2272546
    invoke-static {p1}, LX/FhH;->d(LX/7B6;)I

    move-result v0

    .line 2272547
    iget-object v1, p0, LX/FhH;->c:LX/Fhc;

    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    move-result-object v2

    invoke-virtual {v1, v2, p1, v0}, LX/Fhc;->a(LX/7HY;LX/7B6;I)V

    .line 2272548
    iget-object v0, p0, LX/FhH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhQ;

    iget-object v1, p0, LX/FhH;->b:LX/7BZ;

    move-object v2, p1

    check-cast v2, Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272549
    iget v3, p0, LX/FhH;->h:I

    move v3, v3

    .line 2272550
    invoke-virtual {p0}, LX/FhH;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, LX/7B6;->d:LX/0P1;

    const-string v6, "FRIENDLY_NAME"

    invoke-virtual {v5, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/FhQ;->a(LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;ILjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0P1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2272530
    invoke-super {p0, p1}, LX/7HT;->a(LX/0P1;)V

    .line 2272531
    iget-object v0, p0, LX/FhH;->c:LX/Fhc;

    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    move-result-object v1

    .line 2272532
    invoke-static {v0, v1}, LX/Fhc;->a(LX/Fhc;LX/7HY;)LX/Fhd;

    move-result-object v2

    .line 2272533
    const/4 v1, 0x0

    .line 2272534
    iget-object v3, v2, LX/Fhd;->a:LX/11i;

    invoke-virtual {v2}, LX/Fhd;->a()LX/Fhg;

    move-result-object v4

    invoke-interface {v3, v4}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2272535
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    const-string v4, "KW_ONLY_TA_ENABLED"

    iget-object p0, v2, LX/Fhd;->e:LX/0ad;

    sget-short v0, LX/100;->bN:S

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    .line 2272536
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0P1;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2272537
    invoke-virtual {v3, p1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2272538
    :cond_0
    iget-object v4, v2, LX/Fhd;->a:LX/11i;

    invoke-virtual {v2}, LX/Fhd;->a()LX/Fhg;

    move-result-object p0

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    invoke-interface {v4, p0, v3}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 2272539
    :cond_1
    iget-object v3, v2, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 2272540
    iget-object v3, v2, LX/Fhd;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2272541
    iget-object v3, v2, LX/Fhd;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2272542
    iget-object v3, v2, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 2272543
    iget-object v3, v2, LX/Fhd;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2272544
    iput v1, v2, LX/Fhd;->d:I

    .line 2272545
    return-void
.end method

.method public final a(LX/7B6;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2272519
    iget-object v0, p0, LX/FhH;->c:LX/Fhc;

    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/Fhc;->a(LX/7HY;LX/7B6;)V

    .line 2272520
    iget-object v0, p0, LX/FhH;->f:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_SIMPLE_REMOTE_TYPEAHEAD_SUGGESTION_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2272521
    return-void
.end method

.method public final a(LX/7B6;Ljava/util/concurrent/CancellationException;)V
    .locals 2
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2272553
    iget-object v0, p0, LX/FhH;->c:LX/Fhc;

    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/Fhc;->a(LX/7HY;LX/7B6;)V

    .line 2272554
    return-void
.end method

.method public final a(LX/7Hi;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2272508
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 2272509
    iget v1, v0, LX/7Hc;->d:I

    move v0, v1

    .line 2272510
    iget-object v1, p0, LX/FhH;->c:LX/Fhc;

    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    move-result-object v2

    .line 2272511
    iget-object v3, p1, LX/7Hi;->b:LX/7Hc;

    move-object v3, v3

    .line 2272512
    iget-object v4, v3, LX/7Hc;->b:LX/0Px;

    move-object v3, v4

    .line 2272513
    iget-object v4, p1, LX/7Hi;->a:LX/7B6;

    move-object v4, v4

    .line 2272514
    const/4 v5, -0x1

    if-ne v0, v5, :cond_0

    const/4 v0, 0x0

    .line 2272515
    :goto_0
    invoke-static {v1, v2}, LX/Fhc;->a(LX/Fhc;LX/7HY;)LX/Fhd;

    move-result-object v5

    invoke-virtual {v5, v4, v3, v0}, LX/Fhd;->a(LX/7B6;Ljava/util/List;LX/0P1;)V

    .line 2272516
    invoke-super {p0, p1}, LX/7HT;->a(LX/7Hi;)V

    .line 2272517
    return-void

    .line 2272518
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    const-string v6, "forking_pool_id"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/Cvp;)V
    .locals 0

    .prologue
    .line 2272498
    iput-object p1, p0, LX/FhH;->g:LX/Cvp;

    .line 2272499
    return-void
.end method

.method public final c(LX/7B6;)LX/4d9;
    .locals 1

    .prologue
    .line 2272507
    new-instance v0, LX/FhG;

    invoke-direct {v0, p0, p1}, LX/FhG;-><init>(LX/FhH;LX/7B6;)V

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2272503
    const-string v0, ""

    .line 2272504
    iget-object v1, p0, LX/FhH;->g:LX/Cvp;

    if-eqz v1, :cond_0

    .line 2272505
    iget-object v0, p0, LX/FhH;->g:LX/Cvp;

    invoke-virtual {v0}, LX/Cvp;->a()Ljava/lang/String;

    move-result-object v0

    .line 2272506
    :cond_0
    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2272500
    iget-object v0, p0, LX/FhH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhQ;

    .line 2272501
    iget-object p0, v0, LX/FhQ;->a:LX/0TV;

    invoke-interface {p0}, LX/0TV;->d()V

    .line 2272502
    return-void
.end method
