.class public LX/GpU;
.super LX/GpR;
.source ""


# instance fields
.field public d:LX/CIZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CIb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:LX/15i;

.field private final g:I


# direct methods
.method public constructor <init>(LX/Gpv;LX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2395011
    invoke-direct {p0, p1}, LX/GpR;-><init>(LX/Gpv;)V

    .line 2395012
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p2, p0, LX/GpU;->f:LX/15i;

    iput p3, p0, LX/GpU;->g:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2395013
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/GpU;

    invoke-static {v0}, LX/CIZ;->a(LX/0QB;)LX/CIZ;

    move-result-object p1

    check-cast p1, LX/CIZ;

    invoke-static {v0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v0

    check-cast v0, LX/CIb;

    iput-object p1, p0, LX/GpU;->d:LX/CIZ;

    iput-object v0, p0, LX/GpU;->e:LX/CIb;

    .line 2395014
    return-void

    .line 2395015
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2395016
    invoke-virtual {p0}, LX/GpR;->b()V

    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2395017
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395018
    check-cast v0, LX/Gpv;

    invoke-interface {v0}, LX/Gpv;->a()V

    .line 2395019
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395020
    check-cast v0, LX/Gq2;

    iget-object v1, p0, LX/GpU;->d:LX/CIZ;

    iget-object v2, p0, LX/GpU;->e:LX/CIb;

    .line 2395021
    iget-object v3, v2, LX/CIb;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2395022
    invoke-virtual {v1, v2}, LX/CIZ;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/Gq2;->a(I)V

    .line 2395023
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, LX/GpU;->f:LX/15i;

    iget v3, p0, LX/GpU;->g:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2395024
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395025
    check-cast v0, LX/Gq2;

    const/4 v1, 0x5

    invoke-virtual {v2, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2395026
    iput-object v1, v0, LX/Gq2;->h:Ljava/lang/String;

    .line 2395027
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, LX/GpU;->f:LX/15i;

    iget v3, p0, LX/GpU;->g:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2395028
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395029
    check-cast v0, LX/Gpv;

    const/4 v1, 0x1

    const-class v4, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {v2, v3, v1, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, LX/CHX;

    invoke-interface {v0, v1}, LX/Gpv;->a(LX/CHX;)V

    .line 2395030
    return-void

    .line 2395031
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2395032
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
