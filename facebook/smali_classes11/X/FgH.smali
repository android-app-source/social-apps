.class public LX/FgH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FgH;


# instance fields
.field private final a:LX/CvY;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/CvY;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270053
    iput-object p1, p0, LX/FgH;->a:LX/CvY;

    .line 2270054
    iput-object p2, p0, LX/FgH;->b:LX/0Uh;

    .line 2270055
    return-void
.end method

.method private static a(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270056
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 2270057
    instance-of v1, p0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v1, :cond_0

    .line 2270058
    check-cast p0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 2270059
    iget-object v1, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    move-object v1, v1

    .line 2270060
    invoke-virtual {v1}, LX/Cz3;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 2270061
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 2270062
    :sswitch_0
    const-string v2, "media_combined_module_number_of_annotated_results"

    invoke-virtual {v1}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/Cvf;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 2270063
    :sswitch_1
    const-string v2, "thumbnail_decorations"

    invoke-virtual {v1}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/Cvh;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5852ec13 -> :sswitch_0
        -0x5389cb28 -> :sswitch_1
        -0x30cf2d19 -> :sswitch_1
        0x4ed245b -> :sswitch_1
        0xd5fee8a -> :sswitch_1
        0x735086f1 -> :sswitch_1
    .end sparse-switch
.end method

.method private static a(LX/CzE;)LX/8cg;
    .locals 2

    .prologue
    .line 2270064
    invoke-virtual {p0}, LX/CzE;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 2270065
    sget-object v0, LX/8cg;->EMPTY:LX/8cg;

    .line 2270066
    :goto_0
    return-object v0

    .line 2270067
    :cond_0
    invoke-virtual {p0}, LX/CzE;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 2270068
    sget-object v0, LX/8cg;->ONLY_RESULTS:LX/8cg;

    goto :goto_0

    .line 2270069
    :cond_1
    invoke-virtual {p0}, LX/CzE;->a()I

    move-result v0

    invoke-virtual {p0}, LX/CzE;->b()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 2270070
    sget-object v0, LX/8cg;->TOP_MODULE_AND_RESULTS:LX/8cg;

    goto :goto_0

    .line 2270071
    :cond_2
    sget-object v0, LX/8cg;->ONLY_TOP_MODULE:LX/8cg;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FgH;
    .locals 5

    .prologue
    .line 2270072
    sget-object v0, LX/FgH;->c:LX/FgH;

    if-nez v0, :cond_1

    .line 2270073
    const-class v1, LX/FgH;

    monitor-enter v1

    .line 2270074
    :try_start_0
    sget-object v0, LX/FgH;->c:LX/FgH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2270075
    if-eqz v2, :cond_0

    .line 2270076
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2270077
    new-instance p0, LX/FgH;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v3

    check-cast v3, LX/CvY;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/FgH;-><init>(LX/CvY;LX/0Uh;)V

    .line 2270078
    move-object v0, p0

    .line 2270079
    sput-object v0, LX/FgH;->c:LX/FgH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2270080
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2270081
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2270082
    :cond_1
    sget-object v0, LX/FgH;->c:LX/FgH;

    return-object v0

    .line 2270083
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2270084
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/CzE;Lcom/facebook/search/model/SearchResultsBaseFeedUnit;ILcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2270085
    instance-of v0, p2, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    if-nez v0, :cond_1

    .line 2270086
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p2

    .line 2270087
    check-cast v0, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    invoke-interface {v0}, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 2270088
    invoke-static {v6}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 2270089
    instance-of v0, p2, LX/Cz6;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FgH;->b:LX/0Uh;

    sget v1, LX/2SU;->ae:I

    invoke-virtual {v0, v1, v8}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 2270090
    check-cast v0, LX/Cz6;

    invoke-interface {v0}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    move-object v0, p2

    .line 2270091
    check-cast v0, LX/Cz6;

    invoke-interface {v0}, LX/Cz6;->m()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    .line 2270092
    :goto_1
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    move-object v0, p4

    move v1, p3

    move-object v3, p2

    invoke-static/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/CvV;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2270093
    invoke-static {v7}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2270094
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FgH;->b:LX/0Uh;

    sget v2, LX/2SU;->Y:I

    invoke-virtual {v1, v2, v8}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2270095
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2270096
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 2270097
    :cond_2
    invoke-virtual {p1, v6}, LX/CzE;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    .line 2270098
    invoke-virtual {p1, v6}, LX/CzE;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method private a(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;ILcom/facebook/search/model/SearchResultsBaseFeedUnit;LX/0Px;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 19
    .param p3    # Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            "I",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Lcom/facebook/search/results/model/contract/SearchResultsContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2270099
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    if-eqz v3, :cond_1

    .line 2270100
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    .line 2270101
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FgH;->a:LX/CvY;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->e:Ljava/lang/String;

    move-object/from16 v4, p5

    move/from16 v5, p2

    invoke-virtual/range {v3 .. v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;Ljava/lang/String;Ljava/lang/String;)V

    .line 2270102
    :cond_0
    :goto_0
    return-void

    .line 2270103
    :cond_1
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    if-eqz v3, :cond_4

    .line 2270104
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2270105
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v3, v4, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    const-class v4, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    if-ne v3, v4, :cond_3

    .line 2270106
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v17

    .line 2270107
    const/4 v3, 0x0

    move v15, v3

    :goto_1
    move/from16 v0, v17

    if-ge v15, v0, :cond_0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v14, v3

    check-cast v14, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    .line 2270108
    move-object/from16 v0, p0

    iget-object v0, v0, LX/FgH;->a:LX/CvY;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    invoke-virtual {v14}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v14}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v14}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->n()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v12

    invoke-virtual {v14}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->t()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v13, 0x1

    :goto_2
    move-object/from16 v3, p5

    move/from16 v4, p2

    move-object/from16 v5, p4

    invoke-static/range {v3 .. v13}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;IZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object/from16 v0, v18

    move-object/from16 v1, p5

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v14, v3}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2270109
    add-int/lit8 v3, v15, 0x1

    move v15, v3

    goto :goto_1

    .line 2270110
    :cond_2
    const/4 v13, 0x0

    goto :goto_2

    .line 2270111
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, LX/FgH;->a:LX/CvY;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->v()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v12

    move-object/from16 v4, p5

    move/from16 v5, p2

    move-object/from16 v6, p4

    invoke-static/range {v4 .. v12}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object/from16 v0, p5

    move/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v13, v0, v1, v2, v3}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0

    .line 2270112
    :cond_4
    invoke-static/range {p0 .. p1}, LX/FgH;->b(LX/FgH;Lcom/facebook/search/model/SearchResultsBaseFeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2270113
    const/4 v9, 0x0

    .line 2270114
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v3, :cond_5

    move-object/from16 v3, p1

    .line 2270115
    check-cast v3, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsBridge;->o()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    move-object/from16 v3, p1

    check-cast v3, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsBridge;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    invoke-static {v4, v3}, LX/8eM;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 2270116
    :cond_5
    if-eqz v9, :cond_6

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgH;->b:LX/0Uh;

    invoke-static {v3, v4}, LX/FgI;->a(ILX/0Uh;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v3, p1

    .line 2270117
    check-cast v3, LX/Cz6;

    invoke-interface {v3}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    move-object/from16 v3, p1

    .line 2270118
    check-cast v3, LX/Cz6;

    invoke-interface {v3}, LX/Cz6;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2270119
    move-object/from16 v0, p0

    iget-object v11, v0, LX/FgH;->a:LX/CvY;

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-static/range {p1 .. p1}, LX/FgH;->a(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;)LX/0P1;

    move-result-object v10

    move-object/from16 v3, p5

    move/from16 v4, p2

    invoke-static/range {v3 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object/from16 v0, p5

    move/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v11, v0, v1, v2, v3}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0

    .line 2270120
    :cond_6
    const/4 v3, 0x0

    goto :goto_3

    .line 2270121
    :cond_7
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    if-eqz v3, :cond_0

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    instance-of v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    if-eqz v3, :cond_0

    move-object/from16 v3, p3

    .line 2270122
    check-cast v3, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    move-object/from16 v3, p3

    .line 2270123
    check-cast v3, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->t()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    .line 2270124
    check-cast p3, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 2270125
    move-object/from16 v0, p0

    iget-object v9, v0, LX/FgH;->a:LX/CvY;

    move-object/from16 v4, p5

    move/from16 v5, p2

    invoke-static/range {v4 .. v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object/from16 v0, p5

    move/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v9, v0, v1, v2, v3}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0
.end method

.method private static b(LX/FgH;Lcom/facebook/search/model/SearchResultsBaseFeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2270126
    instance-of v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    if-eqz v1, :cond_1

    .line 2270127
    iget-object v1, p0, LX/FgH;->b:LX/0Uh;

    sget v2, LX/2SU;->af:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2270128
    :cond_0
    :goto_0
    return v0

    .line 2270129
    :cond_1
    instance-of v1, p1, LX/Cz6;

    if-eqz v1, :cond_0

    .line 2270130
    instance-of v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/FgH;->b:LX/0Uh;

    sget v2, LX/2SU;->af:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/CzE;LX/0Px;IILjava/lang/String;)V
    .locals 11
    .param p4    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/contract/SearchResultsContext;",
            "LX/0Px",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;>;",
            "LX/CzE;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2270131
    iget-object v10, p0, LX/FgH;->a:LX/CvY;

    iget-object v2, p0, LX/FgH;->a:LX/CvY;

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    invoke-static {p3}, LX/FgH;->a(LX/CzE;)LX/8cg;

    move-result-object v8

    const/4 v9, 0x0

    move-object v3, p1

    move/from16 v4, p5

    move/from16 v6, p6

    move-object v7, p4

    invoke-virtual/range {v2 .. v9}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IIILX/0Px;LX/8cg;LX/8cf;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object/from16 v0, p7

    move/from16 v1, p6

    invoke-virtual {v10, p1, v0, v1, v2}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;ILcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2270132
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v9

    .line 2270133
    const/4 v2, 0x0

    move v8, v2

    :goto_0
    if-ge v8, v9, :cond_1

    .line 2270134
    invoke-virtual {p2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 2270135
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p3, v3}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v4

    .line 2270136
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    invoke-direct {p0, p3, v3, v4, p1}, LX/FgH;->a(LX/CzE;Lcom/facebook/search/model/SearchResultsBaseFeedUnit;ILcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2270137
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    add-int/lit8 v2, v8, 0x1

    if-ge v2, v9, :cond_0

    add-int/lit8 v2, v8, 0x1

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    move-object v5, v2

    :goto_1
    move-object v2, p0

    move-object v6, p4

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, LX/FgH;->a(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;ILcom/facebook/search/model/SearchResultsBaseFeedUnit;LX/0Px;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2270138
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_0

    .line 2270139
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 2270140
    :cond_1
    return-void
.end method
