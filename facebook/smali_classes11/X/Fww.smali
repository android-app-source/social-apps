.class public final LX/Fww;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2303987
    const-class v1, Lcom/facebook/timeline/header/intro/edit/protocol/FetchFavPhotosGraphQLModels$FavPhotosQueryModel;

    const v0, 0x50b539a9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FavPhotosQuery"

    const-string v6, "11e329d7d37f0a3c64e04f57d37a56bb"

    const-string v7, "user"

    const-string v8, "10155069964411729"

    const-string v9, "10155259086991729"

    .line 2303988
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2303989
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2303990
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2303991
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2303992
    sparse-switch v0, :sswitch_data_0

    .line 2303993
    :goto_0
    return-object p1

    .line 2303994
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2303995
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2303996
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2303997
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2303998
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2303999
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2304000
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2304001
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2304002
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2304003
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2304004
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2304005
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_7
        -0x680de62a -> :sswitch_2
        -0x6326fdb3 -> :sswitch_5
        -0x4496acc9 -> :sswitch_1
        -0x41b8e48f -> :sswitch_0
        -0x1b87b280 -> :sswitch_6
        -0x12efdeb3 -> :sswitch_4
        0xa1fa812 -> :sswitch_9
        0x14658929 -> :sswitch_a
        0x155c0cca -> :sswitch_b
        0x214100e0 -> :sswitch_3
        0x73a026b5 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2304006
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2304007
    :goto_1
    return v0

    .line 2304008
    :sswitch_0
    const-string v2, "10"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "11"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 2304009
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2304010
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2304011
    :pswitch_2
    const-string v0, "image/jpeg"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2304012
    :pswitch_3
    const-string v0, "contain-fit"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x37 -> :sswitch_3
        0x38 -> :sswitch_2
        0x61f -> :sswitch_0
        0x620 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
