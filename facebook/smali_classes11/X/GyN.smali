.class public LX/GyN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/CharSequence;

.field private static g:LX/0Xm;


# instance fields
.field public final b:Landroid/graphics/drawable/Drawable;

.field public final c:Landroid/graphics/drawable/Drawable;

.field private final d:LX/0y2;

.field private final e:LX/6aG;

.field public final f:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2410030
    const-string v0, "[badge]"

    sput-object v0, LX/GyN;->a:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0wM;LX/0y2;LX/6aG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2410032
    const v0, 0x7f021a22

    invoke-static {p1, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/GyN;->b:Landroid/graphics/drawable/Drawable;

    .line 2410033
    const v0, 0x7f021a23

    invoke-static {p1, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/GyN;->c:Landroid/graphics/drawable/Drawable;

    .line 2410034
    iput-object p3, p0, LX/GyN;->d:LX/0y2;

    .line 2410035
    iput-object p4, p0, LX/GyN;->e:LX/6aG;

    .line 2410036
    iput-object p2, p0, LX/GyN;->f:LX/0wM;

    .line 2410037
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b22f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2410038
    iget-object v1, p0, LX/GyN;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, LX/GyN;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2410039
    iget-object v1, p0, LX/GyN;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, LX/GyN;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2410040
    return-void
.end method

.method public static a(LX/0QB;)LX/GyN;
    .locals 7

    .prologue
    .line 2409970
    const-class v1, LX/GyN;

    monitor-enter v1

    .line 2409971
    :try_start_0
    sget-object v0, LX/GyN;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2409972
    sput-object v2, LX/GyN;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2409973
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2409974
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2409975
    new-instance p0, LX/GyN;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v5

    check-cast v5, LX/0y2;

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v6

    check-cast v6, LX/6aG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/GyN;-><init>(Landroid/content/Context;LX/0wM;LX/0y2;LX/6aG;)V

    .line 2409976
    move-object v0, p0

    .line 2409977
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2409978
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GyN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2409979
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2409980
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 2410001
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2410002
    :goto_0
    return-object p0

    .line 2410003
    :cond_0
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    .line 2410004
    const-string v0, " \u2022 "

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2410005
    :cond_1
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static a(LX/GyN;Landroid/content/Context;Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;)Ljava/lang/CharSequence;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2410006
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v11, 0x21

    const/4 v10, 0x0

    .line 2410007
    if-nez p2, :cond_0

    .line 2410008
    :goto_0
    move-object v0, v1

    .line 2410009
    return-object v0

    .line 2410010
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2410011
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a008a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2410012
    invoke-virtual {p2}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->j()D

    move-result-wide v5

    .line 2410013
    invoke-virtual {p2}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->a()I

    move-result v4

    .line 2410014
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2410015
    if-gtz v4, :cond_1

    const-wide/16 v7, 0x0

    cmpl-double v7, v5, v7

    if-lez v7, :cond_4

    .line 2410016
    :cond_1
    iget-object v7, p0, LX/GyN;->f:LX/0wM;

    const v8, 0x7f0209e3

    const v9, -0xbd984e

    invoke-virtual {v7, v8, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 2410017
    if-eqz v7, :cond_2

    .line 2410018
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v7, v10, v10, v1, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2410019
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v8, 0x1

    invoke-direct {v1, v7, v8}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2410020
    :cond_2
    const-string v7, "%.1f"

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v7, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2410021
    if-eqz v1, :cond_3

    .line 2410022
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 2410023
    const-string v6, "*"

    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2410024
    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v2, v1, v5, v6, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2410025
    :cond_3
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 2410026
    invoke-virtual {v2, v3, v10, v1, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2410027
    if-eqz v0, :cond_4

    .line 2410028
    const-string v1, " ("

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v3

    int-to-long v5, v4

    invoke-virtual {v3, v5, v6}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    move-object v1, v2

    .line 2410029
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 2409981
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2409982
    invoke-virtual {p1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    move-result-object v1

    .line 2409983
    invoke-virtual {p1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->p()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v3

    .line 2409984
    :try_start_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2409985
    if-eqz v1, :cond_1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-eq v3, v4, :cond_1

    .line 2409986
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/GyN;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2409987
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    .line 2409988
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v5, v4, v1

    .line 2409989
    if-eqz v3, :cond_1

    .line 2409990
    const/4 v1, 0x0

    .line 2409991
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v3, v6, :cond_2

    .line 2409992
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a00d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2409993
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 2409994
    const/16 v2, 0x21

    invoke-virtual {v0, v1, v5, v4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2409995
    :cond_1
    :goto_1
    return-object v0

    .line 2409996
    :cond_2
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v3, v6, :cond_3

    .line 2409997
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a014a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2409998
    :catch_0
    const-string v0, ""

    goto :goto_1

    .line 2409999
    :cond_3
    :try_start_1
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v3, v6, :cond_0

    .line 2410000
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a00d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static a(Landroid/graphics/drawable/Drawable;I)V
    .locals 3

    .prologue
    .line 2409968
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2409969
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;)Ljava/lang/CharSequence;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2409954
    invoke-virtual {p1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    move-result-object v7

    .line 2409955
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2409956
    :cond_0
    :goto_0
    return-object v6

    .line 2409957
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2409958
    iget-object v1, p0, LX/GyN;->d:LX/0y2;

    invoke-virtual {v1}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v2

    .line 2409959
    invoke-virtual {p1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    move-result-object v1

    .line 2409960
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 2409961
    new-instance v3, Landroid/location/Location;

    const-string v4, "local_surface"

    invoke-direct {v3, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2409962
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;->a()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 2409963
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;->b()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 2409964
    iget-object v1, p0, LX/GyN;->e:LX/6aG;

    invoke-static {v3}, Lcom/facebook/location/ImmutableLocation;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    const-wide v4, 0x40f3a53340000000L    # 80467.203125

    invoke-virtual/range {v1 .. v6}, LX/6aG;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2409965
    invoke-static {v0, v1}, LX/GyN;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2409966
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GyN;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v6, v0

    .line 2409967
    goto :goto_0
.end method
