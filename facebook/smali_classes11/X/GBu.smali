.class public final LX/GBu;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 0

    .prologue
    .line 2327639
    iput-object p1, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2327624
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2327625
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 2327626
    iget v3, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v0, v3

    .line 2327627
    const v3, 0x19219a

    if-ne v0, v3, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2327628
    if-eqz v0, :cond_1

    .line 2327629
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2327630
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    invoke-virtual {v0, v2}, LX/2Ne;->a(Z)V

    .line 2327631
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->q:LX/2Nh;

    iget-object v1, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/2Nh;->c(Ljava/lang/String;)V

    .line 2327632
    :goto_1
    return-void

    .line 2327633
    :cond_0
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    invoke-virtual {v0, v1}, LX/2Ne;->a(Z)V

    .line 2327634
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2327635
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_2

    .line 2327636
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    .line 2327637
    iput-boolean v2, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->H:Z

    .line 2327638
    :cond_2
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->n(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2327577
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2327578
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2327579
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v1, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v1}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v1

    .line 2327580
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/GB4;->SEARCH_PERFORMED:LX/GB4;

    invoke-virtual {v4}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "account_recovery"

    .line 2327581
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327582
    move-object v3, v3

    .line 2327583
    const-string v4, "search_type"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327584
    :goto_0
    iget-object v1, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    const/4 v6, 0x0

    .line 2327585
    if-nez v0, :cond_2

    .line 2327586
    invoke-static {v1}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->n(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327587
    :goto_2
    return-void

    .line 2327588
    :cond_0
    iget-object v0, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v1, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    iget-object v2, p0, LX/GBu;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v2, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v2}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v2

    .line 2327589
    iget-object v3, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->FRIEND_SEARCH_PERFORMED:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 2327590
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327591
    move-object v4, v4

    .line 2327592
    const-string v5, "account_name"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "search_type"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327593
    goto :goto_0

    .line 2327594
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;

    goto :goto_1

    .line 2327595
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->b()LX/0Px;

    move-result-object v2

    .line 2327596
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2327597
    :cond_3
    invoke-static {v1}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->n(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    goto :goto_2

    .line 2327598
    :cond_4
    iget-object v3, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->C:LX/03R;

    invoke-virtual {v3, v6}, LX/03R;->asBoolean(Z)Z

    move-result v3

    .line 2327599
    if-eqz v3, :cond_5

    .line 2327600
    iget-object v4, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    .line 2327601
    iget-object v5, v4, LX/2Ne;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/GB4;->AUTO_IDENTIFY_PERFORMED:LX/GB4;

    invoke-virtual {p1}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "account_recovery"

    .line 2327602
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327603
    move-object p0, p0

    .line 2327604
    const/4 p1, 0x1

    invoke-interface {v5, p0, p1}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327605
    invoke-static {v1}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327606
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->a()I

    move-result v4

    .line 2327607
    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    .line 2327608
    iget-object v4, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->q:LX/2Nh;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-interface {v4, v2, v3}, LX/2Nh;->a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;Z)V

    goto :goto_2

    .line 2327609
    :cond_6
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2327610
    new-instance v5, LX/GBA;

    invoke-direct {v5}, LX/GBA;-><init>()V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2327611
    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2327612
    const/16 v2, 0xa

    if-le v4, v2, :cond_7

    .line 2327613
    new-instance v2, LX/GBC;

    invoke-direct {v2}, LX/GBC;-><init>()V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2327614
    :cond_7
    iget-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    .line 2327615
    iget-object v5, v2, LX/GCA;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 2327616
    iget-object v5, v2, LX/GCA;->b:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2327617
    const v5, 0x197aa95a

    invoke-static {v2, v5}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2327618
    iget-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v3, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    iget-object v5, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    iget-object v6, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v6}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v6

    .line 2327619
    iget-object p0, v2, LX/2Ne;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/GB4;->LIST_SHOWN:LX/GB4;

    invoke-virtual {v0}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "account_recovery"

    .line 2327620
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327621
    move-object p1, p1

    .line 2327622
    const-string v0, "size"

    invoke-virtual {p1, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string v0, "account_name"

    invoke-virtual {p1, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string v0, "friend_name"

    invoke-virtual {p1, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string v0, "search_type"

    invoke-virtual {p1, v0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p0, p1, v0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327623
    const-string v2, ""

    iput-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    goto/16 :goto_2
.end method
