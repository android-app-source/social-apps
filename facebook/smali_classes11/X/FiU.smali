.class public LX/FiU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/FgS;

.field private final b:LX/FgR;

.field private c:LX/01T;

.field private d:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

.field private e:Lcom/facebook/ui/search/SearchEditText;

.field private f:LX/1ZF;


# direct methods
.method public constructor <init>(LX/FgS;LX/FgR;LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275443
    iput-object p1, p0, LX/FiU;->a:LX/FgS;

    .line 2275444
    iput-object p2, p0, LX/FiU;->b:LX/FgR;

    .line 2275445
    iput-object p3, p0, LX/FiU;->c:LX/01T;

    .line 2275446
    return-void
.end method

.method public static a(LX/0QB;)LX/FiU;
    .locals 6

    .prologue
    .line 2275431
    const-class v1, LX/FiU;

    monitor-enter v1

    .line 2275432
    :try_start_0
    sget-object v0, LX/FiU;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275433
    sput-object v2, LX/FiU;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275434
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275435
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275436
    new-instance p0, LX/FiU;

    invoke-static {v0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v3

    check-cast v3, LX/FgS;

    invoke-static {v0}, LX/FgR;->a(LX/0QB;)LX/FgR;

    move-result-object v4

    check-cast v4, LX/FgR;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v5

    check-cast v5, LX/01T;

    invoke-direct {p0, v3, v4, v5}, LX/FiU;-><init>(LX/FgS;LX/FgR;LX/01T;)V

    .line 2275437
    move-object v0, p0

    .line 2275438
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275439
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FiU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275440
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275441
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2275392
    iget-object v0, p0, LX/FiU;->e:Lcom/facebook/ui/search/SearchEditText;

    if-nez v0, :cond_0

    .line 2275393
    :goto_0
    return-void

    .line 2275394
    :cond_0
    iget-object v0, p0, LX/FiU;->e:Lcom/facebook/ui/search/SearchEditText;

    .line 2275395
    invoke-static {v0}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2275396
    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2275420
    iput-object p1, p0, LX/FiU;->d:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2275421
    iget-object v0, p1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v0

    .line 2275422
    iput-object v0, p0, LX/FiU;->e:Lcom/facebook/ui/search/SearchEditText;

    .line 2275423
    iget-object v0, p0, LX/FiU;->b:LX/FgR;

    .line 2275424
    iget-object v1, p1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v1

    .line 2275425
    iput-object v1, v0, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    .line 2275426
    iget-object v0, p0, LX/FiU;->a:LX/FgS;

    .line 2275427
    iput-object p1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2275428
    iget-object v0, p0, LX/FiU;->e:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, p2}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2275429
    iget-object v0, p0, LX/FiU;->e:Lcom/facebook/ui/search/SearchEditText;

    const v1, 0x7f0822d6

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHint(I)V

    .line 2275430
    return-void
.end method

.method public final a(ZLX/1ZF;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2275409
    iget-object v0, p0, LX/FiU;->d:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275410
    iput-object p2, p0, LX/FiU;->f:LX/1ZF;

    .line 2275411
    iget-object v0, p0, LX/FiU;->c:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    invoke-virtual {v0, v1}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275412
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    invoke-interface {v0, p3}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2275413
    :goto_0
    return-void

    .line 2275414
    :cond_0
    if-nez p1, :cond_1

    .line 2275415
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    sget-object v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2275416
    :cond_1
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2275417
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    const-string v1, ""

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2275418
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    iget-object v1, p0, LX/FiU;->d:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-interface {v0, v1}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2275419
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2275397
    iget-object v0, p0, LX/FiU;->b:LX/FgR;

    .line 2275398
    const/4 v1, 0x0

    iput-object v1, v0, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    .line 2275399
    iget-object v0, p0, LX/FiU;->a:LX/FgS;

    .line 2275400
    const/4 v1, 0x0

    iput-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2275401
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FiU;->e:Lcom/facebook/ui/search/SearchEditText;

    if-nez v0, :cond_1

    .line 2275402
    :cond_0
    :goto_0
    return-void

    .line 2275403
    :cond_1
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    invoke-interface {v0, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2275404
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    invoke-interface {v0, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2275405
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    invoke-interface {v0, v2}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2275406
    iget-object v0, p0, LX/FiU;->f:LX/1ZF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2275407
    iput-object v2, p0, LX/FiU;->e:Lcom/facebook/ui/search/SearchEditText;

    .line 2275408
    iput-object v2, p0, LX/FiU;->f:LX/1ZF;

    goto :goto_0
.end method
