.class public final enum LX/GXQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GXQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GXQ;

.field public static final enum MEDIA_ITEM:LX/GXQ;

.field public static final enum PRODUCT_IMAGE:LX/GXQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2364113
    new-instance v0, LX/GXQ;

    const-string v1, "PRODUCT_IMAGE"

    invoke-direct {v0, v1, v2}, LX/GXQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXQ;->PRODUCT_IMAGE:LX/GXQ;

    .line 2364114
    new-instance v0, LX/GXQ;

    const-string v1, "MEDIA_ITEM"

    invoke-direct {v0, v1, v3}, LX/GXQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXQ;->MEDIA_ITEM:LX/GXQ;

    .line 2364115
    const/4 v0, 0x2

    new-array v0, v0, [LX/GXQ;

    sget-object v1, LX/GXQ;->PRODUCT_IMAGE:LX/GXQ;

    aput-object v1, v0, v2

    sget-object v1, LX/GXQ;->MEDIA_ITEM:LX/GXQ;

    aput-object v1, v0, v3

    sput-object v0, LX/GXQ;->$VALUES:[LX/GXQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2364116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GXQ;
    .locals 1

    .prologue
    .line 2364117
    const-class v0, LX/GXQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GXQ;

    return-object v0
.end method

.method public static values()[LX/GXQ;
    .locals 1

    .prologue
    .line 2364118
    sget-object v0, LX/GXQ;->$VALUES:[LX/GXQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GXQ;

    return-object v0
.end method
