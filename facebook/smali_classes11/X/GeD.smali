.class public final LX/GeD;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2375364
    const-class v1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    const-string v5, "PaginatedPagesYouMayLikeQuery"

    const-string v6, "0efffdb5ba83e0cbe4e5f6d213af667e"

    const-string v7, "node"

    const-string v8, "10155255205331729"

    const/4 v9, 0x0

    .line 2375365
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2375366
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2375367
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2375368
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2375369
    sparse-switch v0, :sswitch_data_0

    .line 2375370
    :goto_0
    return-object p1

    .line 2375371
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2375372
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2375373
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2375374
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2375375
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2375376
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2375377
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2375378
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6c1bed17 -> :sswitch_6
        -0x513764de -> :sswitch_3
        -0x41a91745 -> :sswitch_4
        -0x329b0d61 -> :sswitch_1
        0x15888c51 -> :sswitch_7
        0x21beac6a -> :sswitch_2
        0x5eacdfdf -> :sswitch_5
        0x7e07ec78 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2375379
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2375380
    :goto_1
    return v0

    .line 2375381
    :sswitch_0
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 2375382
    :pswitch_0
    const-string v0, "image/jpeg"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2375383
    :pswitch_1
    const/4 v0, 0x5

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2375384
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_1
        0x33 -> :sswitch_2
        0x37 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
