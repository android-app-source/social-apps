.class public final enum LX/FPN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FPN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FPN;

.field public static final enum HIDE_FAB:LX/FPN;

.field public static final enum SHOW_FAB:LX/FPN;

.field public static final enum SHOW_MAP:LX/FPN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2237038
    new-instance v0, LX/FPN;

    const-string v1, "SHOW_FAB"

    invoke-direct {v0, v1, v2}, LX/FPN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPN;->SHOW_FAB:LX/FPN;

    .line 2237039
    new-instance v0, LX/FPN;

    const-string v1, "HIDE_FAB"

    invoke-direct {v0, v1, v3}, LX/FPN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPN;->HIDE_FAB:LX/FPN;

    .line 2237040
    new-instance v0, LX/FPN;

    const-string v1, "SHOW_MAP"

    invoke-direct {v0, v1, v4}, LX/FPN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPN;->SHOW_MAP:LX/FPN;

    .line 2237041
    const/4 v0, 0x3

    new-array v0, v0, [LX/FPN;

    sget-object v1, LX/FPN;->SHOW_FAB:LX/FPN;

    aput-object v1, v0, v2

    sget-object v1, LX/FPN;->HIDE_FAB:LX/FPN;

    aput-object v1, v0, v3

    sget-object v1, LX/FPN;->SHOW_MAP:LX/FPN;

    aput-object v1, v0, v4

    sput-object v0, LX/FPN;->$VALUES:[LX/FPN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2237042
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FPN;
    .locals 1

    .prologue
    .line 2237043
    const-class v0, LX/FPN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FPN;

    return-object v0
.end method

.method public static values()[LX/FPN;
    .locals 1

    .prologue
    .line 2237044
    sget-object v0, LX/FPN;->$VALUES:[LX/FPN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FPN;

    return-object v0
.end method
