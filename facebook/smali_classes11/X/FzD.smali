.class public LX/FzD;
.super LX/Fz5;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2307748
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FzD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2307749
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2307750
    const v0, 0x7f0102b5

    invoke-direct {p0, p1, p2, v0}, LX/FzD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2307751
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2307752
    invoke-direct {p0, p1, p2, p3}, LX/Fz5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2307753
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2307754
    return-void
.end method


# virtual methods
.method public setTitle(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;)V
    .locals 1

    .prologue
    .line 2307755
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2307756
    return-void
.end method
