.class public LX/FUe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public final b:LX/FUf;

.field private final c:LX/0ad;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public e:LX/17W;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FUf;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2249503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2249504
    iput-object p1, p0, LX/FUe;->a:Landroid/content/Context;

    .line 2249505
    iput-object p2, p0, LX/FUe;->b:LX/FUf;

    .line 2249506
    iput-object p3, p0, LX/FUe;->c:LX/0ad;

    .line 2249507
    iput-object p4, p0, LX/FUe;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2249508
    iput-object p5, p0, LX/FUe;->e:LX/17W;

    .line 2249509
    return-void
.end method

.method public static a(LX/0QB;)LX/FUe;
    .locals 7

    .prologue
    .line 2249500
    new-instance v1, LX/FUe;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/FUf;->b(LX/0QB;)LX/FUf;

    move-result-object v3

    check-cast v3, LX/FUf;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-direct/range {v1 .. v6}, LX/FUe;-><init>(Landroid/content/Context;LX/FUf;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/17W;)V

    .line 2249501
    move-object v0, v1

    .line 2249502
    return-object v0
.end method

.method public static a(LX/FUe;Landroid/net/Uri;Z)Ljava/util/TimerTask;
    .locals 8

    .prologue
    .line 2249463
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2249464
    const/4 v0, 0x0

    .line 2249465
    if-nez p1, :cond_5

    .line 2249466
    :cond_0
    :goto_0
    move v0, v0

    .line 2249467
    if-nez v0, :cond_1

    .line 2249468
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2249469
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2249470
    :goto_1
    invoke-static {p1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2249471
    iget-object v1, p0, LX/FUe;->b:LX/FUf;

    .line 2249472
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "uri"

    invoke-virtual {v3, v4, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "regex"

    invoke-virtual {v3, v4, p2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v3

    .line 2249473
    iget-object v4, v1, LX/FUf;->a:LX/0if;

    sget-object v5, LX/0ig;->H:LX/0ih;

    const-string v6, "NON_FB_URL_DECODED_OPEN"

    const/4 p1, 0x0

    invoke-virtual {v4, v5, v6, p1, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2249474
    new-instance v1, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;

    invoke-direct {v1, p0, v0}, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;-><init>(LX/FUe;Ljava/lang/String;)V

    move-object v0, v1

    .line 2249475
    :goto_2
    return-object v0

    .line 2249476
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2249477
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/qr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 2249478
    const-string v1, "id"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    .line 2249479
    :goto_3
    if-eqz v3, :cond_4

    if-eqz v1, :cond_4

    .line 2249480
    :try_start_0
    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2249481
    iget-object v0, p0, LX/FUe;->b:LX/FUf;

    .line 2249482
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v4, "fbid"

    invoke-virtual {v1, v4, v2, v3}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v1

    const-string v4, "regex"

    invoke-virtual {v1, v4, p2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v1

    .line 2249483
    iget-object v4, v0, LX/FUf;->a:LX/0if;

    sget-object v5, LX/0ig;->H:LX/0ih;

    const-string v6, "PROFILE_FBID_SCANNED"

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2249484
    new-instance v0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;

    invoke-direct {v0, p0, v2, v3}, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;-><init>(LX/FUe;J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2249485
    :catch_0
    iget-object v0, p0, LX/FUe;->b:LX/FUf;

    const-string v1, "id"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2249486
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "uri"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    const-string v3, "fbid"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 2249487
    iget-object v3, v0, LX/FUf;->a:LX/0if;

    sget-object v4, LX/0ig;->H:LX/0ih;

    const-string v5, "FBID_SCAN_PARSE_EXCEPTION"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2249488
    const/4 v0, 0x0

    goto :goto_2

    .line 2249489
    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    .line 2249490
    :cond_4
    iget-object v1, p0, LX/FUe;->b:LX/FUf;

    .line 2249491
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "uri"

    invoke-virtual {v3, v4, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "regex"

    invoke-virtual {v3, v4, p2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v3

    .line 2249492
    iget-object v4, v1, LX/FUf;->a:LX/0if;

    sget-object v5, LX/0ig;->H:LX/0ih;

    const-string v6, "FB_URL_SCANNED"

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2249493
    new-instance v1, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;

    invoke-direct {v1, p0, v0}, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;-><init>(LX/FUe;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_2

    .line 2249494
    :cond_5
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 2249495
    const-string v3, "http"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "https"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2249496
    if-nez p0, :cond_1

    .line 2249497
    :cond_0
    :goto_0
    return v0

    .line 2249498
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 2249499
    const-string v2, "http"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "https"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
