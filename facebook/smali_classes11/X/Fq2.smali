.class public LX/Fq2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2292252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2292253
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    .line 2292254
    const-string v0, "cover_photo_fbid"

    invoke-virtual {p1, v0, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2292255
    const-string v2, "cover_photo_uri"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2292256
    const-string v3, "profile_id"

    invoke-virtual {p1, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    .line 2292257
    const-string v5, "cover_photo_refresh_header"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 2292258
    cmp-long v7, v3, v8

    if-eqz v7, :cond_0

    const/4 v6, 0x1

    :cond_0
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 2292259
    new-instance v6, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;

    invoke-direct {v6}, Lcom/facebook/timeline/coverphoto/UserCoverPhotoRepositionFragment;-><init>()V

    .line 2292260
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2292261
    const-string v8, "cover_photo_id"

    invoke-virtual {v7, v8, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2292262
    const-string v8, "cover_photo_uri"

    invoke-virtual {v7, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2292263
    const-string v8, "profile_id"

    invoke-virtual {v7, v8, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2292264
    const-string v8, "force_refresh"

    invoke-virtual {v7, v8, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2292265
    invoke-virtual {v6, v7}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2292266
    move-object v0, v6

    .line 2292267
    return-object v0
.end method
