.class public LX/F31;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2193582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2193583
    iput-object p1, p0, LX/F31;->a:LX/0tX;

    .line 2193584
    return-void
.end method

.method public static b(LX/0QB;)LX/F31;
    .locals 2

    .prologue
    .line 2193585
    new-instance v1, LX/F31;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/F31;-><init>(LX/0tX;)V

    .line 2193586
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2193587
    invoke-static {}, LX/F3W;->a()LX/F3V;

    move-result-object v0

    .line 2193588
    if-eqz p1, :cond_0

    .line 2193589
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2193590
    const-string v1, "description_retrieval_enabled"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2193591
    const-string v1, "tag_retrieval_enabled"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2193592
    :cond_0
    iget-object v1, p0, LX/F31;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v2, 0x1

    .line 2193593
    iput-boolean v2, v0, LX/0zO;->p:Z

    .line 2193594
    move-object v0, v0

    .line 2193595
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2193596
    return-object v0
.end method
