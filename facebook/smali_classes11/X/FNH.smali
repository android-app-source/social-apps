.class public LX/FNH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FNH;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0SG;

.field private final c:LX/2Uq;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/2Uq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232373
    iput-object p1, p0, LX/FNH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2232374
    iput-object p2, p0, LX/FNH;->b:LX/0SG;

    .line 2232375
    iput-object p3, p0, LX/FNH;->c:LX/2Uq;

    .line 2232376
    return-void
.end method

.method public static a(LX/0QB;)LX/FNH;
    .locals 6

    .prologue
    .line 2232377
    sget-object v0, LX/FNH;->d:LX/FNH;

    if-nez v0, :cond_1

    .line 2232378
    const-class v1, LX/FNH;

    monitor-enter v1

    .line 2232379
    :try_start_0
    sget-object v0, LX/FNH;->d:LX/FNH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232380
    if-eqz v2, :cond_0

    .line 2232381
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232382
    new-instance p0, LX/FNH;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v5

    check-cast v5, LX/2Uq;

    invoke-direct {p0, v3, v4, v5}, LX/FNH;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/2Uq;)V

    .line 2232383
    move-object v0, p0

    .line 2232384
    sput-object v0, LX/FNH;->d:LX/FNH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232385
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232386
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232387
    :cond_1
    sget-object v0, LX/FNH;->d:LX/FNH;

    return-object v0

    .line 2232388
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
