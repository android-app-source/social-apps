.class public final enum LX/Fkk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fkk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fkk;

.field public static final enum FRIEND:LX/Fkk;

.field public static final enum NONPROFIT:LX/Fkk;

.field public static final enum VIEWER:LX/Fkk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2278903
    new-instance v0, LX/Fkk;

    const-string v1, "VIEWER"

    invoke-direct {v0, v1, v2}, LX/Fkk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fkk;->VIEWER:LX/Fkk;

    .line 2278904
    new-instance v0, LX/Fkk;

    const-string v1, "NONPROFIT"

    invoke-direct {v0, v1, v3}, LX/Fkk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fkk;->NONPROFIT:LX/Fkk;

    .line 2278905
    new-instance v0, LX/Fkk;

    const-string v1, "FRIEND"

    invoke-direct {v0, v1, v4}, LX/Fkk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fkk;->FRIEND:LX/Fkk;

    .line 2278906
    const/4 v0, 0x3

    new-array v0, v0, [LX/Fkk;

    sget-object v1, LX/Fkk;->VIEWER:LX/Fkk;

    aput-object v1, v0, v2

    sget-object v1, LX/Fkk;->NONPROFIT:LX/Fkk;

    aput-object v1, v0, v3

    sget-object v1, LX/Fkk;->FRIEND:LX/Fkk;

    aput-object v1, v0, v4

    sput-object v0, LX/Fkk;->$VALUES:[LX/Fkk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2278907
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fkk;
    .locals 1

    .prologue
    .line 2278908
    const-class v0, LX/Fkk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fkk;

    return-object v0
.end method

.method public static values()[LX/Fkk;
    .locals 1

    .prologue
    .line 2278909
    sget-object v0, LX/Fkk;->$VALUES:[LX/Fkk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fkk;

    return-object v0
.end method
