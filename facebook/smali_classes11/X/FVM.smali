.class public final enum LX/FVM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FVM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FVM;

.field public static final enum LOADING_MORE:LX/FVM;

.field public static final enum LOAD_MORE_FAILED:LX/FVM;

.field public static final enum NOT_LOADING:LX/FVM;

.field public static final enum REFRESHING:LX/FVM;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2250707
    new-instance v0, LX/FVM;

    const-string v1, "REFRESHING"

    invoke-direct {v0, v1, v2}, LX/FVM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FVM;->REFRESHING:LX/FVM;

    .line 2250708
    new-instance v0, LX/FVM;

    const-string v1, "LOADING_MORE"

    invoke-direct {v0, v1, v3}, LX/FVM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FVM;->LOADING_MORE:LX/FVM;

    .line 2250709
    new-instance v0, LX/FVM;

    const-string v1, "LOAD_MORE_FAILED"

    invoke-direct {v0, v1, v4}, LX/FVM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FVM;->LOAD_MORE_FAILED:LX/FVM;

    .line 2250710
    new-instance v0, LX/FVM;

    const-string v1, "NOT_LOADING"

    invoke-direct {v0, v1, v5}, LX/FVM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FVM;->NOT_LOADING:LX/FVM;

    .line 2250711
    const/4 v0, 0x4

    new-array v0, v0, [LX/FVM;

    sget-object v1, LX/FVM;->REFRESHING:LX/FVM;

    aput-object v1, v0, v2

    sget-object v1, LX/FVM;->LOADING_MORE:LX/FVM;

    aput-object v1, v0, v3

    sget-object v1, LX/FVM;->LOAD_MORE_FAILED:LX/FVM;

    aput-object v1, v0, v4

    sget-object v1, LX/FVM;->NOT_LOADING:LX/FVM;

    aput-object v1, v0, v5

    sput-object v0, LX/FVM;->$VALUES:[LX/FVM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2250712
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FVM;
    .locals 1

    .prologue
    .line 2250713
    const-class v0, LX/FVM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FVM;

    return-object v0
.end method

.method public static values()[LX/FVM;
    .locals 1

    .prologue
    .line 2250714
    sget-object v0, LX/FVM;->$VALUES:[LX/FVM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FVM;

    return-object v0
.end method
