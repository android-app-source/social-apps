.class public LX/H9l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNE;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field public g:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2434583
    const v0, 0x7f0209e1

    sput v0, LX/H9l;->a:I

    .line 2434584
    const v0, 0x7f0814f1

    sput v0, LX/H9l;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p4    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BNE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434586
    iput-object p1, p0, LX/H9l;->c:LX/0Ot;

    .line 2434587
    iput-object p2, p0, LX/H9l;->d:LX/0Ot;

    .line 2434588
    iput-object p3, p0, LX/H9l;->e:LX/0Ot;

    .line 2434589
    iput-object p4, p0, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434590
    iput-object p5, p0, LX/H9l;->g:Landroid/content/Context;

    .line 2434591
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    .line 2434592
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9l;->b:I

    sget v3, LX/H9l;->a:I

    const/4 v4, 0x1

    .line 2434593
    iget-object v5, p0, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/8A4;->a(LX/0Px;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->k()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->B()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    move v5, v5

    .line 2434594
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434595
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9l;->b:I

    sget v3, LX/H9l;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 2434596
    iget-object v0, p0, LX/H9l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XI;->EVENT_TAPPED_REVIEW_IN_ACTION_BAR:LX/9XI;

    iget-object v2, p0, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434597
    iget-object v0, p0, LX/H9l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    iget-object v1, p0, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/H8W;->a(Z)LX/CSL;

    move-result-object v1

    .line 2434598
    iget-object v0, p0, LX/H9l;->g:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, LX/H9l;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0814fe

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v0, v2, v3, v4, v5}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v2

    .line 2434599
    iget-object v0, p0, LX/H9l;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNE;

    iget-object v3, p0, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/H9k;

    invoke-direct {v4, p0, v2, v1}, LX/H9k;-><init>(LX/H9l;LX/4BY;LX/CSL;)V

    invoke-virtual {v0, v3, v4}, LX/BNE;->a(Ljava/lang/String;LX/BND;)V

    .line 2434600
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434601
    const/4 v0, 0x0

    return-object v0
.end method
