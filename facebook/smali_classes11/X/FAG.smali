.class public LX/FAG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/net/SocketImplFactory;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/FAG;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/FAK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206867
    const-class v0, LX/FAG;

    sput-object v0, LX/FAG;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/FAK;)V
    .locals 0

    .prologue
    .line 2206868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206869
    iput-object p1, p0, LX/FAG;->b:LX/FAK;

    .line 2206870
    return-void
.end method


# virtual methods
.method public final createSocketImpl()Ljava/net/SocketImpl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2206871
    :try_start_0
    iget-object v0, p0, LX/FAG;->b:LX/FAK;

    invoke-virtual {v0}, LX/FAK;->a()LX/FAI;

    move-result-object v0

    sget-object v2, LX/FAI;->DISABLED:LX/FAI;

    if-ne v0, v2, :cond_0

    .line 2206872
    invoke-static {}, LX/FAB;->a()Ljava/net/SocketImpl;

    move-result-object v0

    .line 2206873
    :goto_0
    return-object v0

    .line 2206874
    :cond_0
    new-instance v0, LX/FAF;

    invoke-static {}, LX/FAB;->a()Ljava/net/SocketImpl;

    move-result-object v2

    iget-object v3, p0, LX/FAG;->b:LX/FAK;

    invoke-direct {v0, v2, v3}, LX/FAF;-><init>(Ljava/net/SocketImpl;LX/FAK;)V
    :try_end_0
    .catch LX/FAA; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/FAE; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2206875
    :catch_0
    move-exception v0

    .line 2206876
    sget-object v2, LX/FAG;->a:Ljava/lang/Class;

    const-string v3, "Failed to create StrictSocketImpl"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 2206877
    goto :goto_0

    .line 2206878
    :catch_1
    move-exception v0

    .line 2206879
    sget-object v2, LX/FAG;->a:Ljava/lang/Class;

    const-string v3, "Failed to create StrictSocketImpl"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 2206880
    goto :goto_0
.end method
