.class public LX/H0y;
.super LX/H0w;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/H0w",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V
    .locals 1

    .prologue
    .line 2414643
    invoke-direct/range {p0 .. p6}, LX/H0w;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    .line 2414644
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414663
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2414664
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2414659
    invoke-super {p0, p1, p2}, LX/H0w;->a(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 2414660
    const v0, 0x7f0d1c12

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    .line 2414661
    const/16 v1, 0x3002

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setInputType(I)V

    .line 2414662
    return-void
.end method

.method public final b(LX/4gq;)Z
    .locals 2

    .prologue
    .line 2414658
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->i(J)Z

    move-result v0

    return v0
.end method

.method public final c(LX/4gq;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2414653
    iget-wide v0, p0, LX/H0w;->h:J

    .line 2414654
    invoke-virtual {p1, v0, v1}, LX/4gq;->i(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2414655
    invoke-virtual {p1, v0, v1}, LX/4gq;->j(J)D

    move-result-wide v2

    .line 2414656
    :goto_0
    move-wide v0, v2

    .line 2414657
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p1, v0, v1}, LX/4gq;->k(J)D

    move-result-wide v2

    goto :goto_0
.end method

.method public final d(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414652
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->j(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public final e(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414651
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->k(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public final f(LX/4gq;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2414646
    iget-wide v0, p0, LX/H0w;->h:J

    .line 2414647
    iget-object v2, p1, LX/4gq;->c:LX/0W4;

    if-eqz v2, :cond_0

    .line 2414648
    iget-object v2, p1, LX/4gq;->c:LX/0W4;

    invoke-interface {v2, v0, v1}, LX/0W4;->h(J)D

    move-result-wide v2

    .line 2414649
    :goto_0
    move-wide v0, v2

    .line 2414650
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0, v1}, LX/0ok;->c(J)D

    move-result-wide v2

    goto :goto_0
.end method

.method public final g(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414645
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-static {v0, v1}, LX/0ok;->c(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method
