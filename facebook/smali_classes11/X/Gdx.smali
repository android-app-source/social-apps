.class public final LX/Gdx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

.field public final synthetic b:LX/Ge0;


# direct methods
.method public constructor <init>(LX/Ge0;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)V
    .locals 0

    .prologue
    .line 2374836
    iput-object p1, p0, LX/Gdx;->b:LX/Ge0;

    iput-object p2, p0, LX/Gdx;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2374826
    iget-object v0, p0, LX/Gdx;->b:LX/Ge0;

    iget-object v1, p0, LX/Gdx;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->u()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-object v2, p0, LX/Gdx;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->z()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Gdx;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->I_()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 2374827
    iget-object v4, v0, LX/Ge0;->m:LX/0ad;

    sget v5, LX/9Is;->c:I

    const/16 v6, 0x8

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    iget-object v5, v0, LX/Ge0;->m:LX/0ad;

    sget v6, LX/9Is;->a:I

    const/16 v7, 0x19

    invoke-interface {v5, v6, v7}, LX/0ad;->a(II)I

    move-result v5

    sub-int/2addr v5, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2374828
    new-instance v5, LX/Ge6;

    invoke-direct {v5}, LX/Ge6;-><init>()V

    move-object v5, v5

    .line 2374829
    const-string v6, "offsetCount"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "paginationId"

    invoke-virtual {v6, v7, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "count"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "afterCursor"

    invoke-virtual {v4, v6, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2374830
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2374831
    iget-object v5, v0, LX/Ge0;->m:LX/0ad;

    sget-short v6, LX/9Is;->d:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2374832
    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    iget-object v5, v0, LX/Ge0;->m:LX/0ad;

    sget v6, LX/9Is;->b:I

    const/16 v7, 0x12c

    invoke-interface {v5, v6, v7}, LX/0ad;->a(II)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 2374833
    :cond_0
    iget-object v5, v0, LX/Ge0;->d:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2374834
    new-instance v5, LX/Gdz;

    invoke-direct {v5, v0, v1}, LX/Gdz;-><init>(LX/Ge0;I)V

    iget-object v6, v0, LX/Ge0;->e:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2374835
    return-object v0
.end method
