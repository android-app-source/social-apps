.class public final LX/FIY;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:LX/FIb;

.field public final synthetic b:LX/FIc;


# direct methods
.method public constructor <init>(LX/FIc;LX/FIb;)V
    .locals 0

    .prologue
    .line 2222264
    iput-object p1, p0, LX/FIY;->b:LX/FIc;

    iput-object p2, p0, LX/FIY;->a:LX/FIb;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 3

    .prologue
    .line 2222265
    invoke-super {p0, p1}, LX/1Mt;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 2222266
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Metadata upload to server was cancelled. Cancellation message: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/concurrent/CancellationException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2222267
    iget-object v1, p0, LX/FIY;->b:LX/FIc;

    iget-object v2, p0, LX/FIY;->a:LX/FIb;

    invoke-static {v1, v2, v0}, LX/FIc;->a$redex0(LX/FIc;LX/FIb;Ljava/lang/String;)V

    .line 2222268
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2222269
    invoke-super {p0, p1}, LX/1Mt;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2222270
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Exception when trying to upload metadata to server. The actual problem was: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2222271
    iget-object v1, p0, LX/FIY;->b:LX/FIc;

    iget-object v2, p0, LX/FIY;->a:LX/FIb;

    invoke-static {v1, v2, v0}, LX/FIc;->a$redex0(LX/FIc;LX/FIb;Ljava/lang/String;)V

    .line 2222272
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 1

    .prologue
    .line 2222273
    invoke-super {p0, p1}, LX/1Mt;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 2222274
    iget-object v0, p0, LX/FIY;->b:LX/FIc;

    iget-object v0, v0, LX/FIc;->d:LX/FIK;

    invoke-virtual {v0}, LX/FIK;->b()Z

    .line 2222275
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2222276
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/FIY;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
