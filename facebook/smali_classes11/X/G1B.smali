.class public LX/G1B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2310773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;Ljava/lang/String;)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;
    .locals 13

    .prologue
    .line 2310774
    new-instance v0, LX/G1Y;

    invoke-direct {v0}, LX/G1Y;-><init>()V

    .line 2310775
    iput-object p0, v0, LX/G1Y;->c:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 2310776
    move-object v0, v0

    .line 2310777
    new-instance v1, LX/G1Z;

    invoke-direct {v1}, LX/G1Z;-><init>()V

    .line 2310778
    iput-object p1, v1, LX/G1Z;->a:Ljava/lang/String;

    .line 2310779
    move-object v1, v1

    .line 2310780
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2310781
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2310782
    iget-object v3, v1, LX/G1Z;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2310783
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 2310784
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 2310785
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2310786
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2310787
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2310788
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2310789
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2310790
    new-instance v3, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    invoke-direct {v3, v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;-><init>(LX/15i;)V

    .line 2310791
    move-object v1, v3

    .line 2310792
    iput-object v1, v0, LX/G1Y;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    .line 2310793
    move-object v0, v0

    .line 2310794
    const/4 v6, 0x1

    const/4 v12, 0x0

    const/4 v4, 0x0

    .line 2310795
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2310796
    iget-object v3, v0, LX/G1Y;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2310797
    iget-object v5, v0, LX/G1Y;->b:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2310798
    iget-object v7, v0, LX/G1Y;->c:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    invoke-virtual {v2, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2310799
    iget-object v8, v0, LX/G1Y;->d:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2310800
    iget-object v9, v0, LX/G1Y;->e:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2310801
    iget-object v10, v0, LX/G1Y;->f:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2310802
    const/4 v11, 0x6

    invoke-virtual {v2, v11}, LX/186;->c(I)V

    .line 2310803
    invoke-virtual {v2, v12, v3}, LX/186;->b(II)V

    .line 2310804
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 2310805
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 2310806
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 2310807
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 2310808
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 2310809
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2310810
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2310811
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2310812
    invoke-virtual {v3, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2310813
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2310814
    new-instance v3, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;-><init>(LX/15i;)V

    .line 2310815
    move-object v0, v3

    .line 2310816
    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;
    .locals 9

    .prologue
    .line 2310817
    new-instance v0, LX/G1b;

    invoke-direct {v0}, LX/G1b;-><init>()V

    new-instance v1, LX/G1a;

    invoke-direct {v1}, LX/G1a;-><init>()V

    .line 2310818
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2310819
    if-nez p1, :cond_0

    .line 2310820
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const v4, 0x7f08156e

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/G1B;->a(Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;Ljava/lang/String;)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2310821
    :cond_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const v4, 0x7f08156f

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/G1B;->a(Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;Ljava/lang/String;)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2310822
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2310823
    iput-object v2, v1, LX/G1a;->a:LX/0Px;

    .line 2310824
    move-object v1, v1

    .line 2310825
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2310826
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2310827
    iget-object v4, v1, LX/G1a;->a:LX/0Px;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 2310828
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 2310829
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 2310830
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 2310831
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 2310832
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2310833
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2310834
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2310835
    new-instance v4, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    invoke-direct {v4, v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;-><init>(LX/15i;)V

    .line 2310836
    move-object v1, v4

    .line 2310837
    iput-object v1, v0, LX/G1b;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    .line 2310838
    move-object v0, v0

    .line 2310839
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2310840
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2310841
    iget-object v4, v0, LX/G1b;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2310842
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 2310843
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 2310844
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 2310845
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 2310846
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2310847
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2310848
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2310849
    new-instance v4, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;

    invoke-direct {v4, v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;-><init>(LX/15i;)V

    .line 2310850
    move-object v0, v4

    .line 2310851
    return-object v0
.end method
