.class public LX/G2d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/G2b;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1Kf;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/app/Activity;

.field private final f:LX/Frd;

.field private final g:LX/5SB;

.field private final h:LX/BQ1;

.field private final i:LX/BQ9;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/BQ9;LX/5SB;LX/BQ1;LX/Frd;LX/0Ot;LX/1Kf;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BQ9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Frd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/BQ9;",
            "LX/5SB;",
            "LX/BQ1;",
            "Lcom/facebook/timeline/units/controller/interfaces/TimelineRefreshUnitsController;",
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;",
            "LX/1Kf;",
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G4a;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314391
    iput-object p1, p0, LX/G2d;->e:Landroid/app/Activity;

    .line 2314392
    iput-object p2, p0, LX/G2d;->i:LX/BQ9;

    .line 2314393
    iput-object p3, p0, LX/G2d;->g:LX/5SB;

    .line 2314394
    iput-object p4, p0, LX/G2d;->h:LX/BQ1;

    .line 2314395
    iput-object p5, p0, LX/G2d;->f:LX/Frd;

    .line 2314396
    iput-object p6, p0, LX/G2d;->a:LX/0Ot;

    .line 2314397
    iput-object p7, p0, LX/G2d;->b:LX/1Kf;

    .line 2314398
    iput-object p8, p0, LX/G2d;->c:LX/0Or;

    .line 2314399
    iput-object p9, p0, LX/G2d;->d:LX/0Or;

    .line 2314400
    return-void
.end method

.method private a()Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2314387
    iget-object v0, p0, LX/G2d;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4a;

    iget-object v1, p0, LX/G2d;->e:Landroid/app/Activity;

    iget-object v2, p0, LX/G2d;->g:LX/5SB;

    .line 2314388
    iget-wide v6, v2, LX/5SB;->b:J

    move-wide v2, v6

    .line 2314389
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->Y()LX/2rX;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/G4a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2314382
    iget-object v0, p0, LX/G2d;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2314383
    iget-object v1, p0, LX/G2d;->b:LX/1Kf;

    const/4 v2, 0x0

    iget-object v0, p0, LX/G2d;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4a;

    iget-object v3, p0, LX/G2d;->g:LX/5SB;

    .line 2314384
    iget-wide v7, v3, LX/5SB;->b:J

    move-wide v4, v7

    .line 2314385
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->Y()LX/2rX;

    move-result-object v6

    invoke-virtual {v0, v3, v4, v5, v6}, LX/G4a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/16 v3, 0x6dc

    iget-object v4, p0, LX/G2d;->e:Landroid/app/Activity;

    invoke-interface {v1, v2, v0, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2314386
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2314363
    iget-object v0, p0, LX/G2d;->i:LX/BQ9;

    iget-object v1, p0, LX/G2d;->g:LX/5SB;

    .line 2314364
    iget-wide v7, v1, LX/5SB;->b:J

    move-wide v2, v7

    .line 2314365
    const-string v1, "update_status"

    iget-object v4, p0, LX/G2d;->g:LX/5SB;

    invoke-virtual {v4}, LX/5SB;->i()Z

    move-result v4

    iget-object v5, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    iget-object v6, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, LX/BQ9;->a(JLjava/lang/String;LX/9lQ;)V

    .line 2314366
    invoke-direct {p0, p1}, LX/G2d;->d(Ljava/lang/String;)V

    .line 2314367
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2314376
    iget-object v0, p0, LX/G2d;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2314377
    iget-object v0, p0, LX/G2d;->i:LX/BQ9;

    iget-object v1, p0, LX/G2d;->g:LX/5SB;

    .line 2314378
    iget-wide v7, v1, LX/5SB;->b:J

    move-wide v2, v7

    .line 2314379
    const-string v1, "publish_photo"

    iget-object v4, p0, LX/G2d;->g:LX/5SB;

    invoke-virtual {v4}, LX/5SB;->i()Z

    move-result v4

    iget-object v5, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    iget-object v6, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, LX/BQ9;->a(JLjava/lang/String;LX/9lQ;)V

    .line 2314380
    iget-object v0, p0, LX/G2d;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ay;

    invoke-direct {p0}, LX/G2d;->a()Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x6dc

    iget-object v3, p0, LX/G2d;->e:Landroid/app/Activity;

    invoke-virtual {v0, v1, v2, v3}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2314381
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2314368
    iget-object v0, p0, LX/G2d;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2314369
    iget-object v0, p0, LX/G2d;->i:LX/BQ9;

    iget-object v1, p0, LX/G2d;->g:LX/5SB;

    .line 2314370
    iget-wide v7, v1, LX/5SB;->b:J

    move-wide v2, v7

    .line 2314371
    const-string v1, "publish_moment"

    iget-object v4, p0, LX/G2d;->g:LX/5SB;

    invoke-virtual {v4}, LX/5SB;->i()Z

    move-result v4

    iget-object v5, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    iget-object v6, p0, LX/G2d;->h:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, LX/BQ9;->a(JLjava/lang/String;LX/9lQ;)V

    .line 2314372
    iget-object v0, p0, LX/G2d;->f:LX/Frd;

    if-eqz v0, :cond_0

    .line 2314373
    iget-object v0, p0, LX/G2d;->f:LX/Frd;

    invoke-virtual {v0}, LX/Frd;->a()V

    .line 2314374
    :cond_0
    iget-object v0, p0, LX/G2d;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ay;

    iget-object v1, p0, LX/G2d;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G4a;

    iget-object v2, p0, LX/G2d;->e:Landroid/app/Activity;

    invoke-virtual {v1, v2}, LX/G4a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x6e0

    iget-object v3, p0, LX/G2d;->e:Landroid/app/Activity;

    invoke-virtual {v0, v1, v2, v3}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2314375
    return-void
.end method
