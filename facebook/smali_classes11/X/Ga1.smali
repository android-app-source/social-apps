.class public LX/Ga1;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/7j6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/support/v7/widget/RecyclerView;

.field public f:Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;

.field public g:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

.field public h:LX/GaH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7iP;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2367799
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2367800
    const-class v0, LX/Ga1;

    invoke-static {v0, p0}, LX/Ga1;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2367801
    const v0, 0x7f0302ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2367802
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Ga1;->setOrientation(I)V

    .line 2367803
    const v0, 0x7f0d0a0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Ga1;->c:Landroid/widget/TextView;

    .line 2367804
    const v0, 0x7f0d0a0b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Ga1;->d:Landroid/widget/TextView;

    .line 2367805
    const v0, 0x7f0d0a0c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/Ga1;->e:Landroid/support/v7/widget/RecyclerView;

    .line 2367806
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, LX/Ga1;->getContext()Landroid/content/Context;

    invoke-direct {v0, v1, v1}, LX/1P1;-><init>(IZ)V

    .line 2367807
    iget-object v1, p0, LX/Ga1;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2367808
    new-instance v0, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;

    iget-object v1, p0, LX/Ga1;->a:LX/7j6;

    iget-object v2, p0, LX/Ga1;->b:LX/0Zb;

    invoke-direct {v0, v1, v2, p2}, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;-><init>(LX/7j6;LX/0Zb;LX/7iP;)V

    iput-object v0, p0, LX/Ga1;->f:Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;

    .line 2367809
    iget-object v0, p0, LX/Ga1;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/Ga1;->f:Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2367810
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ga1;

    invoke-static {p0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v1

    check-cast v1, LX/7j6;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    iput-object v1, p1, LX/Ga1;->a:LX/7j6;

    iput-object p0, p1, LX/Ga1;->b:LX/0Zb;

    return-void
.end method
