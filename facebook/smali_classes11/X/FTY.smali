.class public final LX/FTY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2245958
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2245959
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245960
    :goto_0
    return v1

    .line 2245961
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245962
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2245963
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2245964
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2245965
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2245966
    const-string v5, "bio"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2245967
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2245968
    :cond_2
    const-string v5, "context_items"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2245969
    invoke-static {p0, p1}, LX/FTX;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2245970
    :cond_3
    const-string v5, "favorite_photos"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2245971
    invoke-static {p0, p1}, LX/5vT;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2245972
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2245973
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2245974
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2245975
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2245976
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2245977
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2245978
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245979
    if-eqz v0, :cond_0

    .line 2245980
    const-string v1, "bio"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245981
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2245982
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245983
    if-eqz v0, :cond_1

    .line 2245984
    const-string v1, "context_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245985
    invoke-static {p0, v0, p2, p3}, LX/FTX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245986
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245987
    if-eqz v0, :cond_2

    .line 2245988
    const-string v1, "favorite_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245989
    invoke-static {p0, v0, p2, p3}, LX/5vT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245990
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2245991
    return-void
.end method
