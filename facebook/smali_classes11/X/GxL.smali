.class public final LX/GxL;
.super LX/BWH;
.source ""


# instance fields
.field public final synthetic a:LX/GxN;


# direct methods
.method public constructor <init>(LX/GxN;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2408050
    iput-object p1, p0, LX/GxL;->a:LX/GxN;

    .line 2408051
    iget-object v2, p1, LX/GxN;->i:LX/BWW;

    new-instance v3, LX/GxK;

    invoke-direct {v3, p1}, LX/GxK;-><init>(LX/GxN;)V

    invoke-static {p1}, LX/GxN;->a(LX/GxN;)Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-result-object v4

    invoke-static {p1}, LX/GxN;->b(LX/GxN;)LX/44G;

    move-result-object v5

    iget-object v6, p1, LX/GxN;->k:LX/03R;

    iget-object v7, p1, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v7}, LX/BWH;-><init>(Landroid/content/Context;LX/BWW;LX/BWV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/44G;LX/03R;LX/48V;)V

    .line 2408052
    return-void
.end method


# virtual methods
.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2408053
    invoke-super {p0, p1, p2, p3, p4}, LX/BWH;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 2408054
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2408055
    if-eqz v0, :cond_0

    .line 2408056
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2408057
    const-string v1, "url "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed (code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2408058
    :cond_0
    return-void
.end method
