.class public final LX/Gke;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Gkq;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

.field public final synthetic c:LX/GkS;

.field public final synthetic d:I

.field public final synthetic e:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;LX/Gkq;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;LX/GkS;I)V
    .locals 0

    .prologue
    .line 2389203
    iput-object p1, p0, LX/Gke;->e:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iput-object p2, p0, LX/Gke;->a:LX/Gkq;

    iput-object p3, p0, LX/Gke;->b:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iput-object p4, p0, LX/Gke;->c:LX/GkS;

    iput p5, p0, LX/Gke;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2389204
    iget-object v0, p0, LX/Gke;->e:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    check-cast v0, LX/Gl6;

    iget-object v1, p0, LX/Gke;->a:LX/Gkq;

    .line 2389205
    iget-object v2, v1, LX/Gkq;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2389206
    iget-object v2, p0, LX/Gke;->b:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iget-object v3, p0, LX/Gke;->c:LX/GkS;

    invoke-virtual {v0, v1, v2, v3}, LX/Gl6;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;LX/GkS;)V

    .line 2389207
    iget-object v0, p0, LX/Gke;->e:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    const-string v1, "turn_off_notifs"

    iget v2, p0, LX/Gke;->d:I

    iget-object v3, p0, LX/Gke;->a:LX/Gkq;

    iget-object v4, p0, LX/Gke;->e:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v4, v4, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {v4}, LX/GlX;->d()LX/Gkm;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Glj;->a(Ljava/lang/String;ILX/Gkq;LX/Gkm;)V

    .line 2389208
    const/4 v0, 0x1

    return v0
.end method
