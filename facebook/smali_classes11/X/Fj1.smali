.class public LX/Fj1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/8iB;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/0ad;

.field public final c:LX/8iE;

.field public final d:LX/FgS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2276113
    new-instance v0, LX/Fj0;

    invoke-direct {v0}, LX/Fj0;-><init>()V

    sput-object v0, LX/Fj1;->a:LX/8iB;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/8iE;LX/FgS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2276126
    iput-object p1, p0, LX/Fj1;->b:LX/0ad;

    .line 2276127
    iput-object p2, p0, LX/Fj1;->c:LX/8iE;

    .line 2276128
    iput-object p3, p0, LX/Fj1;->d:LX/FgS;

    .line 2276129
    return-void
.end method

.method public static a(LX/0QB;)LX/Fj1;
    .locals 6

    .prologue
    .line 2276114
    const-class v1, LX/Fj1;

    monitor-enter v1

    .line 2276115
    :try_start_0
    sget-object v0, LX/Fj1;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276116
    sput-object v2, LX/Fj1;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276117
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276118
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276119
    new-instance p0, LX/Fj1;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/8iE;->a(LX/0QB;)LX/8iE;

    move-result-object v4

    check-cast v4, LX/8iE;

    invoke-static {v0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v5

    check-cast v5, LX/FgS;

    invoke-direct {p0, v3, v4, v5}, LX/Fj1;-><init>(LX/0ad;LX/8iE;LX/FgS;)V

    .line 2276120
    move-object v0, p0

    .line 2276121
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276122
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fj1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276123
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
