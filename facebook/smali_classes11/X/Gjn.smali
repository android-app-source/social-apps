.class public final LX/Gjn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Gk2;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/model/GreetingCard;

.field public final synthetic b:Lcom/facebook/greetingcards/render/RenderCardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/RenderCardFragment;Lcom/facebook/greetingcards/model/GreetingCard;)V
    .locals 0

    .prologue
    .line 2388537
    iput-object p1, p0, LX/Gjn;->b:Lcom/facebook/greetingcards/render/RenderCardFragment;

    iput-object p2, p0, LX/Gjn;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2388538
    iget-object v0, p0, LX/Gjn;->b:Lcom/facebook/greetingcards/render/RenderCardFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/RenderCardFragment;->g:LX/03V;

    const-string v1, "greeting_card_load_failure"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2388539
    iget-object v0, p0, LX/Gjn;->b:Lcom/facebook/greetingcards/render/RenderCardFragment;

    sget-object v1, LX/Gjp;->ERROR:LX/Gjp;

    invoke-static {v0, v1}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gjp;)V

    .line 2388540
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2388541
    check-cast p1, LX/Gk2;

    .line 2388542
    :try_start_0
    iget-object v0, p0, LX/Gjn;->b:Lcom/facebook/greetingcards/render/RenderCardFragment;

    iget-object v1, p0, LX/Gjn;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {v0, p1, v1}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a$redex0(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/Gk2;Lcom/facebook/greetingcards/model/GreetingCard;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2388543
    :goto_0
    return-void

    .line 2388544
    :catch_0
    move-exception v0

    .line 2388545
    sget-object v1, Lcom/facebook/greetingcards/render/RenderCardFragment;->c:Ljava/lang/Class;

    const-string v2, "Error displaying layout"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
