.class public LX/G6J;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/G6I;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/view/View$OnClickListener;

.field private final c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:I

.field public e:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/view/View$OnClickListener;Lcom/facebook/common/callercontext/CallerContext;Landroid/net/Uri;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2319970
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2319971
    const/4 v0, 0x0

    iput v0, p0, LX/G6J;->d:I

    .line 2319972
    iput-object p2, p0, LX/G6J;->b:Landroid/view/View$OnClickListener;

    .line 2319973
    iput-object p3, p0, LX/G6J;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2319974
    iput-object p1, p0, LX/G6J;->a:Ljava/util/List;

    .line 2319975
    iput-object p4, p0, LX/G6J;->e:Landroid/net/Uri;

    .line 2319976
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2319977
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031617

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2319978
    new-instance v1, LX/G6I;

    invoke-direct {v1, p0, v0}, LX/G6I;-><init>(LX/G6J;Landroid/view/View;)V

    .line 2319979
    new-instance v2, LX/G6H;

    invoke-direct {v2, p0, v1}, LX/G6H;-><init>(LX/G6J;LX/G6I;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2319980
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2319981
    check-cast p1, LX/G6I;

    .line 2319982
    iget-object v1, p1, LX/G6I;->l:Landroid/view/View;

    iget v0, p0, LX/G6J;->d:I

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 2319983
    iget-object v0, p1, LX/G6I;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/G6J;->e:Landroid/net/Uri;

    iget-object v2, p0, LX/G6J;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2319984
    iget-object v1, p1, LX/G6I;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, LX/G6J;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel;->a()Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel;->k()Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/protocol/GetWatermarkOverlaysQueryModels$GetWatermarkOverlaysQueryModel$WatermarkAvailableOverlaysModel$EdgesModel$NodeModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, LX/G6J;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2319985
    return-void

    .line 2319986
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2319987
    iget-object v0, p0, LX/G6J;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
