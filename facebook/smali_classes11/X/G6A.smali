.class public final LX/G6A;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9fc;

.field public final synthetic b:Lcom/facebook/wem/watermark/WatermarkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/watermark/WatermarkActivity;LX/9fc;)V
    .locals 0

    .prologue
    .line 2319779
    iput-object p1, p0, LX/G6A;->b:Lcom/facebook/wem/watermark/WatermarkActivity;

    iput-object p2, p0, LX/G6A;->a:LX/9fc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 10

    .prologue
    .line 2319780
    iget-object v0, p0, LX/G6A;->b:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->r:LX/G6G;

    iget-object v1, p0, LX/G6A;->a:LX/9fc;

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 2319781
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v5

    .line 2319782
    iget v2, v5, LX/434;->b:I

    .line 2319783
    iget v3, v5, LX/434;->a:I

    .line 2319784
    if-le v2, v3, :cond_0

    .line 2319785
    sub-int v4, v2, v3

    int-to-float v4, v4

    div-float/2addr v4, v6

    .line 2319786
    new-instance v6, Landroid/graphics/RectF;

    int-to-float v7, v2

    div-float v7, v4, v7

    int-to-float v3, v3

    add-float/2addr v3, v4

    int-to-float v2, v2

    div-float v2, v3, v2

    invoke-direct {v6, v7, v8, v2, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2319787
    :goto_0
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v2

    invoke-static {v6}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 2319788
    iget-object v2, v0, LX/G6G;->f:LX/9fn;

    move-object v4, p1

    move-object v7, v1

    invoke-virtual/range {v2 .. v7}, LX/9fn;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/434;Landroid/graphics/RectF;LX/9fc;)V

    .line 2319789
    return-void

    .line 2319790
    :cond_0
    if-ge v2, v3, :cond_1

    .line 2319791
    sub-int v4, v3, v2

    int-to-float v4, v4

    div-float/2addr v4, v6

    .line 2319792
    new-instance v6, Landroid/graphics/RectF;

    int-to-float v7, v3

    div-float v7, v4, v7

    int-to-float v2, v2

    add-float/2addr v2, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-direct {v6, v8, v7, v9, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 2319793
    :cond_1
    sget-object v6, LX/G6G;->a:Landroid/graphics/RectF;

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2319794
    iget-object v0, p0, LX/G6A;->b:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/G6Q;->c(Ljava/lang/String;)V

    .line 2319795
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2319796
    check-cast p1, Landroid/net/Uri;

    invoke-direct {p0, p1}, LX/G6A;->a(Landroid/net/Uri;)V

    return-void
.end method
