.class public LX/FK3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FK3;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FK5;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/FJx;

.field public final c:LX/0SI;

.field public final d:LX/01T;


# direct methods
.method public constructor <init>(LX/0Or;LX/FJx;LX/0SI;LX/01T;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/FK5;",
            ">;",
            "LX/FJx;",
            "LX/0SI;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224467
    iput-object p1, p0, LX/FK3;->a:LX/0Or;

    .line 2224468
    iput-object p2, p0, LX/FK3;->b:LX/FJx;

    .line 2224469
    iput-object p3, p0, LX/FK3;->c:LX/0SI;

    .line 2224470
    iput-object p4, p0, LX/FK3;->d:LX/01T;

    .line 2224471
    return-void
.end method

.method public static a(LX/0QB;)LX/FK3;
    .locals 7

    .prologue
    .line 2224418
    sget-object v0, LX/FK3;->e:LX/FK3;

    if-nez v0, :cond_1

    .line 2224419
    const-class v1, LX/FK3;

    monitor-enter v1

    .line 2224420
    :try_start_0
    sget-object v0, LX/FK3;->e:LX/FK3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2224421
    if-eqz v2, :cond_0

    .line 2224422
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2224423
    new-instance v6, LX/FK3;

    const/16 v3, 0x28ea

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/FJx;->a(LX/0QB;)LX/FJx;

    move-result-object v3

    check-cast v3, LX/FJx;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v4

    check-cast v4, LX/0SI;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v5

    check-cast v5, LX/01T;

    invoke-direct {v6, p0, v3, v4, v5}, LX/FK3;-><init>(LX/0Or;LX/FJx;LX/0SI;LX/01T;)V

    .line 2224424
    move-object v0, v6

    .line 2224425
    sput-object v0, LX/FK3;->e:LX/FK3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2224426
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2224427
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2224428
    :cond_1
    sget-object v0, LX/FK3;->e:LX/FK3;

    return-object v0

    .line 2224429
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2224430
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/FK3;LX/0Tn;)V
    .locals 11

    .prologue
    .line 2224431
    const/4 v0, 0x0

    .line 2224432
    if-eqz p1, :cond_0

    sget-object v1, LX/0db;->ab:LX/0Tn;

    invoke-virtual {p1, v1}, LX/0To;->a(LX/0To;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "muted_until2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2224433
    :cond_0
    :goto_0
    move-object v2, v0

    .line 2224434
    if-eqz v2, :cond_4

    .line 2224435
    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2224436
    iget-object v5, p0, LX/FK3;->d:LX/01T;

    sget-object v6, LX/01T;->PAA:LX/01T;

    if-eq v5, v6, :cond_6

    .line 2224437
    sget-object v5, LX/1mW;->a:LX/1mW;

    .line 2224438
    :goto_1
    move-object v3, v5

    .line 2224439
    const/4 v1, 0x0

    .line 2224440
    :try_start_0
    iget-object v0, p0, LX/FK3;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK5;

    .line 2224441
    iget-object v4, v0, LX/FK5;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/messaging/prefs/notifications/ThreadNotificationPrefsSyncUtil$1;

    invoke-direct {v5, v0, v2}, Lcom/facebook/messaging/prefs/notifications/ThreadNotificationPrefsSyncUtil$1;-><init>(LX/FK5;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    const v6, -0x1a2c4bb

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2224442
    if-eqz v3, :cond_1

    invoke-interface {v3}, LX/1mW;->close()V

    .line 2224443
    :cond_1
    :goto_2
    return-void

    .line 2224444
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2224445
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_3
    if-eqz v3, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-interface {v3}, LX/1mW;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_4
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_3
    invoke-interface {v3}, LX/1mW;->close()V

    goto :goto_4

    .line 2224446
    :cond_4
    sget-object v0, LX/0db;->J:LX/0Tn;

    invoke-virtual {p1, v0}, LX/0Tn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2224447
    iget-object v0, p0, LX/FK3;->b:LX/FJx;

    .line 2224448
    iget-object v1, v0, LX/FJx;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/messaging/prefs/notifications/GlobalNotificationPrefsSyncUtil$1;

    sget-object v3, LX/FJx;->a:Ljava/lang/Class;

    const-string v4, "synchronizeAfterClientChange"

    invoke-direct {v2, v0, v3, v4}, Lcom/facebook/messaging/prefs/notifications/GlobalNotificationPrefsSyncUtil$1;-><init>(LX/FJx;Ljava/lang/Class;Ljava/lang/String;)V

    const v3, 0x75b19b4d

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2224449
    goto :goto_2

    .line 2224450
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 2224451
    :cond_5
    sget-object v1, LX/0db;->ab:LX/0Tn;

    invoke-virtual {p1, v1}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v1

    .line 2224452
    const-string v2, "/"

    invoke-static {v2}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2224453
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2224454
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    iget-object v5, p0, LX/FK3;->c:LX/0SI;

    .line 2224455
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v7

    const/4 v8, 0x1

    .line 2224456
    iput-boolean v8, v7, LX/0SK;->d:Z

    .line 2224457
    move-object v7, v7

    .line 2224458
    iget-wide v9, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    .line 2224459
    iput-object v8, v7, LX/0SK;->a:Ljava/lang/String;

    .line 2224460
    move-object v7, v7

    .line 2224461
    const-string v8, ""

    .line 2224462
    iput-object v8, v7, LX/0SK;->b:Ljava/lang/String;

    .line 2224463
    move-object v7, v7

    .line 2224464
    invoke-virtual {v7}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v7

    move-object v6, v7

    .line 2224465
    invoke-interface {v5, v6}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    move-result-object v5

    goto/16 :goto_1
.end method
