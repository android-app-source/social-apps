.class public final LX/FIj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Ul;


# direct methods
.method public constructor <init>(LX/2Ul;)V
    .locals 0

    .prologue
    .line 2222574
    iput-object p1, p0, LX/FIj;->a:LX/2Ul;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2222575
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2222576
    iget-object v0, p0, LX/FIj;->a:LX/2Ul;

    iget-object v0, v0, LX/2Ul;->a:LX/2Uj;

    invoke-virtual {v0, v1}, LX/2Uj;->a(Ljava/util/Collection;)V

    .line 2222577
    iget-object v0, p0, LX/FIj;->a:LX/2Ul;

    iget-object v0, v0, LX/2Ul;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/SocketAddress;

    .line 2222578
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FIO;

    .line 2222579
    new-instance v2, Ljava/net/DatagramPacket;

    .line 2222580
    iget-object v4, v1, LX/FIO;->a:[B

    move-object v4, v4

    .line 2222581
    iget v5, v1, LX/FIO;->b:I

    move v5, v5

    .line 2222582
    invoke-direct {v2, v4, v5}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 2222583
    invoke-virtual {v2, v0}, Ljava/net/DatagramPacket;->setSocketAddress(Ljava/net/SocketAddress;)V

    .line 2222584
    :try_start_0
    iget-object v4, p0, LX/FIj;->a:LX/2Ul;

    iget-object v4, v4, LX/2Ul;->c:Ljava/net/DatagramSocket;

    invoke-virtual {v4, v2}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222585
    iget-object v2, v1, LX/FIO;->a:[B

    move-object v1, v2

    .line 2222586
    invoke-static {v1}, LX/FIN;->a([B)V

    goto :goto_0

    .line 2222587
    :catch_0
    move-exception v2

    .line 2222588
    :try_start_1
    const-class v4, LX/2Ul;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error sending to socket "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222589
    iget-object v2, v1, LX/FIO;->a:[B

    move-object v1, v2

    .line 2222590
    invoke-static {v1}, LX/FIN;->a([B)V

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 2222591
    iget-object v2, v1, LX/FIO;->a:[B

    move-object v1, v2

    .line 2222592
    invoke-static {v1}, LX/FIN;->a([B)V

    throw v0

    .line 2222593
    :cond_0
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    return-object v0
.end method
