.class public LX/F62;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field public final b:Landroid/app/Activity;

.field public c:LX/2Ib;

.field public d:LX/0i5;

.field public e:Lcom/facebook/content/SecureContextHelper;

.field public f:LX/0kL;

.field public g:Lcom/facebook/react/bridge/Callback;

.field public h:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2200118
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, LX/F62;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;LX/2Ib;Lcom/facebook/content/SecureContextHelper;LX/0i4;LX/0kL;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200120
    iput-object p1, p0, LX/F62;->b:Landroid/app/Activity;

    .line 2200121
    iput-object p2, p0, LX/F62;->c:LX/2Ib;

    .line 2200122
    iput-object p3, p0, LX/F62;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2200123
    invoke-virtual {p4, p1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, LX/F62;->d:LX/0i5;

    .line 2200124
    iput-object p5, p0, LX/F62;->f:LX/0kL;

    .line 2200125
    return-void
.end method
