.class public final LX/F0o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic d:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;LX/1Po;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 2190582
    iput-object p1, p0, LX/F0o;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iput-object p2, p0, LX/F0o;->a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object p3, p0, LX/F0o;->b:LX/1Po;

    iput-object p4, p0, LX/F0o;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x2

    const v0, -0x6e775fd3

    invoke-static {v6, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2190583
    iget-object v0, p0, LX/F0o;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->i:LX/1Cn;

    iget-object v2, p0, LX/F0o;->a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v2

    const-string v3, "permalink"

    invoke-virtual {v0, v2, v3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2190584
    iget-object v0, p0, LX/F0o;->b:LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v0

    .line 2190585
    const-string v2, "throwbackCampaign"

    invoke-static {v0, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v2, p0, LX/F0o;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iget-object v2, v2, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->g:LX/1Nq;

    const-string v3, "throwback_permalink"

    iget-object v4, p0, LX/F0o;->a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, LX/89G;->a()LX/89G;

    move-result-object v2

    iget-object v3, p0, LX/F0o;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2190586
    iput-object v3, v2, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2190587
    move-object v2, v2

    .line 2190588
    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v2, p0, LX/F0o;->a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    invoke-static {v2}, LX/CFE;->a(Lcom/facebook/graphql/model/GraphQLUser;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2190589
    iget-object v0, p0, LX/F0o;->d:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iget-object v3, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->e:LX/1Kf;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v5, Landroid/app/Activity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-interface {v3, v4, v2, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2190590
    const v0, 0x390e4f2f

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
