.class public LX/EzQ;
.super LX/Ep8;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/EzQ;


# instance fields
.field private final d:LX/EoY;

.field private final e:LX/EzU;


# direct methods
.method public constructor <init>(LX/EoB;LX/EnR;LX/EoY;LX/EzU;LX/Eod;LX/Enj;LX/EpA;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187157
    move-object v0, p0

    move-object v1, p1

    move-object v2, p7

    move-object v3, p2

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/Ep8;-><init>(LX/EoB;LX/EpA;LX/EnR;LX/Eod;LX/Enj;)V

    .line 2187158
    iput-object p3, p0, LX/EzQ;->d:LX/EoY;

    .line 2187159
    iput-object p4, p0, LX/EzQ;->e:LX/EzU;

    .line 2187160
    return-void
.end method

.method public static a(LX/0QB;)LX/EzQ;
    .locals 11

    .prologue
    .line 2187161
    sget-object v0, LX/EzQ;->f:LX/EzQ;

    if-nez v0, :cond_1

    .line 2187162
    const-class v1, LX/EzQ;

    monitor-enter v1

    .line 2187163
    :try_start_0
    sget-object v0, LX/EzQ;->f:LX/EzQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2187164
    if-eqz v2, :cond_0

    .line 2187165
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2187166
    new-instance v3, LX/EzQ;

    invoke-static {v0}, LX/EoB;->b(LX/0QB;)LX/EoB;

    move-result-object v4

    check-cast v4, LX/EoB;

    invoke-static {v0}, LX/EnR;->a(LX/0QB;)LX/EnR;

    move-result-object v5

    check-cast v5, LX/EnR;

    invoke-static {v0}, LX/EoY;->a(LX/0QB;)LX/EoY;

    move-result-object v6

    check-cast v6, LX/EoY;

    const-class v7, LX/EzU;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/EzU;

    const-class v8, LX/Eod;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Eod;

    const-class v9, LX/Enj;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Enj;

    invoke-static {v0}, LX/EpA;->a(LX/0QB;)LX/EpA;

    move-result-object v10

    check-cast v10, LX/EpA;

    invoke-direct/range {v3 .. v10}, LX/EzQ;-><init>(LX/EoB;LX/EnR;LX/EoY;LX/EzU;LX/Eod;LX/Enj;LX/EpA;)V

    .line 2187167
    move-object v0, v3

    .line 2187168
    sput-object v0, LX/EzQ;->f:LX/EzQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2187169
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2187170
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2187171
    :cond_1
    sget-object v0, LX/EzQ;->f:LX/EzQ;

    return-object v0

    .line 2187172
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2187173
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)LX/Emj;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2187174
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/EnQ;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)LX/End;
    .locals 10
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2187148
    new-instance v0, LX/Ene;

    iget-object v1, p1, LX/EnQ;->a:LX/0Px;

    iget v2, p1, LX/EnQ;->d:I

    iget v3, p1, LX/EnQ;->e:I

    invoke-direct {v0, v1, v2, v3}, LX/Ene;-><init>(LX/0Px;II)V

    .line 2187149
    iget-object v1, p0, LX/EzQ;->e:LX/EzU;

    .line 2187150
    new-instance v4, LX/EzT;

    invoke-static {v1}, LX/2dk;->b(LX/0QB;)LX/2dk;

    move-result-object v5

    check-cast v5, LX/2dk;

    invoke-static {v1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    move-object v7, v0

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v4 .. v9}, LX/EzT;-><init>(LX/2dk;Ljava/util/concurrent/ExecutorService;LX/Ene;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)V

    .line 2187151
    move-object v0, v4

    .line 2187152
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2187156
    const-string v0, "friending"

    return-object v0
.end method

.method public final b()LX/Enm;
    .locals 1

    .prologue
    .line 2187155
    iget-object v0, p0, LX/EzQ;->d:LX/EoY;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)LX/Jxq;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2187154
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2187153
    const/4 v0, 0x0

    return v0
.end method
