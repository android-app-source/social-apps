.class public LX/Fd9;
.super LX/Fd8;
.source ""


# instance fields
.field public a:I

.field private b:Landroid/widget/LinearLayout;

.field private c:Z

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2263199
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/Fd9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263200
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2263229
    invoke-direct {p0, p1, p2, p3}, LX/Fd8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263230
    const/4 v0, 0x4

    iput v0, p0, LX/Fd9;->a:I

    .line 2263231
    iput-boolean v4, p0, LX/Fd9;->c:Z

    .line 2263232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Fd9;->d:Ljava/util/List;

    .line 2263233
    iput v4, p0, LX/Fd9;->e:I

    .line 2263234
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Fd9;->setOrientation(I)V

    .line 2263235
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/Fd9;->setShowDividers(I)V

    .line 2263236
    invoke-virtual {p0}, LX/Fd9;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0217c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Fd9;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2263237
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    .line 2263238
    iget-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2263239
    iget-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/Fd9;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b14e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p0}, LX/Fd9;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b14e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v0, v1, v4, v2, v4}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 2263240
    iget-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 2263241
    iget-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/Fd9;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0217c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2263242
    iget-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2263243
    iget-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, LX/Fd9;->addView(Landroid/view/View;)V

    .line 2263244
    return-void
.end method

.method private declared-synchronized a()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2263212
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, LX/Fd9;->c:Z

    .line 2263213
    iget-object v1, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v1, v0

    .line 2263214
    :goto_0
    iget v2, p0, LX/Fd9;->a:I

    if-ge v1, v2, :cond_0

    .line 2263215
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/Fd9;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2263216
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, 0x0

    const/4 v5, -0x2

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2263217
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2263218
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 2263219
    invoke-virtual {p0}, LX/Fd9;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0217c9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2263220
    iget-object v3, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2263221
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2263222
    :cond_0
    iget-object v1, p0, LX/Fd9;->d:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2263223
    iget-object v1, p0, LX/Fd9;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2263224
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2263225
    invoke-virtual {p0, v0}, LX/Fd9;->a(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2263226
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2263227
    :cond_1
    monitor-exit p0

    return-void

    .line 2263228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2263204
    iget-boolean v0, p0, LX/Fd9;->c:Z

    if-nez v0, :cond_0

    .line 2263205
    invoke-direct {p0}, LX/Fd9;->a()V

    .line 2263206
    :cond_0
    iget-object v0, p0, LX/Fd9;->b:Landroid/widget/LinearLayout;

    iget v1, p0, LX/Fd9;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2263207
    if-nez v0, :cond_1

    .line 2263208
    :goto_0
    return-void

    .line 2263209
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2263210
    iget v0, p0, LX/Fd9;->e:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/Fd9;->a:I

    rem-int/2addr v0, v1

    iput v0, p0, LX/Fd9;->e:I

    .line 2263211
    iget-object v0, p0, LX/Fd9;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setButtonsPerRow(I)V
    .locals 0

    .prologue
    .line 2263201
    iput p1, p0, LX/Fd9;->a:I

    .line 2263202
    invoke-direct {p0}, LX/Fd9;->a()V

    .line 2263203
    return-void
.end method
