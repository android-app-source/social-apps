.class public final LX/FAj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2208030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2208031
    new-instance v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;

    invoke-direct {v0, p1}, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2208032
    new-array v0, p1, [Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;

    return-object v0
.end method
