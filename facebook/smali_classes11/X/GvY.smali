.class public final enum LX/GvY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GvY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GvY;

.field public static final enum EMAIL:LX/GvY;

.field public static final enum PUBLIC_PROFILE:LX/GvY;


# instance fields
.field private final mText:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2405627
    new-instance v0, LX/GvY;

    const-string v1, "EMAIL"

    const-string v2, "email"

    invoke-direct {v0, v1, v3, v2}, LX/GvY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GvY;->EMAIL:LX/GvY;

    .line 2405628
    new-instance v0, LX/GvY;

    const-string v1, "PUBLIC_PROFILE"

    const-string v2, "public_profile"

    invoke-direct {v0, v1, v4, v2}, LX/GvY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GvY;->PUBLIC_PROFILE:LX/GvY;

    .line 2405629
    const/4 v0, 0x2

    new-array v0, v0, [LX/GvY;

    sget-object v1, LX/GvY;->EMAIL:LX/GvY;

    aput-object v1, v0, v3

    sget-object v1, LX/GvY;->PUBLIC_PROFILE:LX/GvY;

    aput-object v1, v0, v4

    sput-object v0, LX/GvY;->$VALUES:[LX/GvY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2405638
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2405639
    iput-object p3, p0, LX/GvY;->mText:Ljava/lang/String;

    .line 2405640
    return-void
.end method

.method public static fromName(Ljava/lang/String;)LX/GvY;
    .locals 5

    .prologue
    .line 2405633
    invoke-static {}, LX/GvY;->values()[LX/GvY;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2405634
    invoke-virtual {v3}, LX/GvY;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2405635
    return-object v3

    .line 2405636
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2405637
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No PermissionType for permission "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/GvY;
    .locals 1

    .prologue
    .line 2405641
    const-class v0, LX/GvY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GvY;

    return-object v0
.end method

.method public static values()[LX/GvY;
    .locals 1

    .prologue
    .line 2405632
    sget-object v0, LX/GvY;->$VALUES:[LX/GvY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GvY;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2405631
    iget-object v0, p0, LX/GvY;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2405630
    iget-object v0, p0, LX/GvY;->mText:Ljava/lang/String;

    return-object v0
.end method
