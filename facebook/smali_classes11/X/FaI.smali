.class public final LX/FaI;
.super LX/13D;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2258609
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_NULL_STATE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/FaI;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/13J;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2258614
    invoke-direct {p0, p1}, LX/13D;-><init>(LX/13J;)V

    .line 2258615
    return-void
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2258613
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2258616
    const-string v0, "3191"

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 2258612
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2258611
    const-string v0, "Android Search Null-State Megaphone"

    return-object v0
.end method

.method public final l()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2258610
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->SEARCH_NULL_STATE_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
