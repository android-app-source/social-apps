.class public final LX/Fog;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 0

    .prologue
    .line 2290025
    iput-object p1, p0, LX/Fog;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5b444820

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2290026
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    if-nez v0, :cond_1

    .line 2290027
    :cond_0
    const v0, -0x16037cb1

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2290028
    :goto_0
    return-void

    .line 2290029
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    .line 2290030
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->j()Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2290031
    const v0, -0x7e9e8970

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2290032
    :cond_2
    iget-object v2, p0, LX/Fog;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->c:LX/BOa;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->j()Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v3

    .line 2290033
    new-instance v4, LX/BOW;

    invoke-direct {v4, v2, v3}, LX/BOW;-><init>(LX/BOa;Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;)V

    .line 2290034
    iget-object v5, v2, LX/BOa;->a:LX/0Zb;

    const-string p1, "fundraiser_creation_searched_categories"

    invoke-static {v2, p1, v4}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290035
    iget-object v2, p0, LX/Fog;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->j()Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    move-result-object v3

    .line 2290036
    iput-object v3, v2, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 2290037
    iget-object v2, p0, LX/Fog;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2290038
    iput-object v0, v2, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->r:Ljava/lang/String;

    .line 2290039
    iget-object v0, p0, LX/Fog;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b$redex0(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290040
    const v0, 0x481db655

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
