.class public final LX/F6y;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/F73;


# direct methods
.method public constructor <init>(LX/F73;)V
    .locals 1

    .prologue
    .line 2200993
    iput-object p1, p0, LX/F6y;->d:LX/F73;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 2200994
    new-instance v0, LX/F6x;

    invoke-direct {v0, p0}, LX/F6x;-><init>(LX/F6y;)V

    iput-object v0, p0, LX/F6y;->c:Ljava/util/Comparator;

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2200995
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2200996
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2200997
    :goto_0
    return-object v5

    .line 2200998
    :cond_0
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    iget-object v0, v0, LX/F73;->x:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2200999
    if-nez v0, :cond_5

    .line 2201000
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    move-object v1, v0

    .line 2201001
    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 2201002
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/F6y;->a:Ljava/util/List;

    .line 2201003
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    iget-object v0, v0, LX/F73;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201004
    invoke-virtual {v0}, LX/F7L;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, LX/F6y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2201005
    iget-object v4, p0, LX/F6y;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2201006
    :cond_2
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/F6y;->b:Ljava/util/List;

    .line 2201007
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    iget-object v0, v0, LX/F73;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201008
    invoke-virtual {v0}, LX/F7L;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, LX/F6y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2201009
    iget-object v4, p0, LX/F6y;->b:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2201010
    :cond_4
    iget-object v0, p0, LX/F6y;->a:Ljava/util/List;

    iget-object v1, p0, LX/F6y;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2201011
    iget-object v0, p0, LX/F6y;->b:Ljava/util/List;

    iget-object v1, p0, LX/F6y;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 2201012
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2201013
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    iget-object v1, p0, LX/F6y;->d:LX/F73;

    iget-object v1, v1, LX/F73;->e:Ljava/util/List;

    .line 2201014
    iput-object v1, v0, LX/F73;->k:Ljava/util/List;

    .line 2201015
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    iget-object v1, p0, LX/F6y;->d:LX/F73;

    iget-object v1, v1, LX/F73;->f:Ljava/util/List;

    .line 2201016
    iput-object v1, v0, LX/F73;->l:Ljava/util/List;

    .line 2201017
    :goto_0
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    const v1, 0x38fe457c

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2201018
    return-void

    .line 2201019
    :cond_0
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    iget-object v1, p0, LX/F6y;->a:Ljava/util/List;

    .line 2201020
    iput-object v1, v0, LX/F73;->k:Ljava/util/List;

    .line 2201021
    iget-object v0, p0, LX/F6y;->d:LX/F73;

    iget-object v1, p0, LX/F6y;->b:Ljava/util/List;

    .line 2201022
    iput-object v1, v0, LX/F73;->l:Ljava/util/List;

    .line 2201023
    goto :goto_0
.end method
