.class public final LX/EzF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5P5;


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/PersonYouMayKnow;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic e:LX/2iS;


# direct methods
.method public constructor <init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2186961
    iput-object p1, p0, LX/EzF;->e:LX/2iS;

    iput-object p2, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iput-object p3, p0, LX/EzF;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p4, p0, LX/EzF;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p5, p0, LX/EzF;->d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2186962
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2186963
    iget-object v0, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iget-object v1, p0, LX/EzF;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2186964
    iput-object v1, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2186965
    iget-object v0, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iget-object v1, p0, LX/EzF;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2186966
    iget-object v0, p0, LX/EzF;->e:LX/2iS;

    iget-object v1, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/2iS;->b(LX/2iS;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2186967
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v1, p0, LX/EzF;->d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v1, p0, LX/EzF;->d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2186968
    :cond_0
    iget-object v0, p0, LX/EzF;->e:LX/2iS;

    iget-object v0, v0, LX/2iS;->n:LX/2ia;

    iget-object v1, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/2ia;->a(J)V

    .line 2186969
    :cond_1
    :goto_0
    return-void

    .line 2186970
    :cond_2
    iget-object v0, p0, LX/EzF;->e:LX/2iS;

    iget-object v1, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/2iS;->a(LX/2iS;J)Lcom/facebook/fbui/widget/contentview/ContentView;

    move-result-object v0

    .line 2186971
    instance-of v1, v0, Lcom/facebook/friending/common/list/FriendListItemView;

    if-eqz v1, :cond_1

    .line 2186972
    iget-object v1, p0, LX/EzF;->e:LX/2iS;

    iget-object v2, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-static {v1, v0, v2}, LX/2iS;->b(LX/2iS;Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 2186973
    iget-object v1, p0, LX/EzF;->e:LX/2iS;

    check-cast v0, Lcom/facebook/friending/common/list/FriendListItemView;

    iget-object v2, p0, LX/EzF;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-static {v1, v0, v2}, LX/2iS;->a$redex0(LX/2iS;Lcom/facebook/friending/common/list/FriendListItemView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    goto :goto_0
.end method
