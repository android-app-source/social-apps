.class public LX/Gv6;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Gv6;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2404782
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2404783
    sget-object v0, LX/0ax;->cc:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->NEARBY_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2404784
    sget-object v0, LX/0ax;->cd:Ljava/lang/String;

    const-class v1, Lcom/facebook/katana/activity/nearby/NearbySearchActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2404785
    return-void
.end method

.method public static a(LX/0QB;)LX/Gv6;
    .locals 3

    .prologue
    .line 2404770
    sget-object v0, LX/Gv6;->a:LX/Gv6;

    if-nez v0, :cond_1

    .line 2404771
    const-class v1, LX/Gv6;

    monitor-enter v1

    .line 2404772
    :try_start_0
    sget-object v0, LX/Gv6;->a:LX/Gv6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2404773
    if-eqz v2, :cond_0

    .line 2404774
    :try_start_1
    new-instance v0, LX/Gv6;

    invoke-direct {v0}, LX/Gv6;-><init>()V

    .line 2404775
    move-object v0, v0

    .line 2404776
    sput-object v0, LX/Gv6;->a:LX/Gv6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2404777
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2404778
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2404779
    :cond_1
    sget-object v0, LX/Gv6;->a:LX/Gv6;

    return-object v0

    .line 2404780
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2404781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2404769
    const/4 v0, 0x1

    return v0
.end method
