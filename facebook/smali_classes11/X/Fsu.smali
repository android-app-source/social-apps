.class public final LX/Fsu;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FsE;

.field public final synthetic b:Ljava/lang/ref/WeakReference;

.field public final synthetic c:LX/Fsw;


# direct methods
.method public constructor <init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .prologue
    .line 2297714
    iput-object p1, p0, LX/Fsu;->c:LX/Fsw;

    iput-object p2, p0, LX/Fsu;->a:LX/FsE;

    iput-object p3, p0, LX/Fsu;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2297715
    iget-object v0, p0, LX/Fsu;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297716
    iput-boolean v1, v0, LX/FsE;->g:Z

    .line 2297717
    iget-object v0, p0, LX/Fsu;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/TimelineFragment;

    .line 2297718
    if-eqz v0, :cond_0

    .line 2297719
    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->O()V

    .line 2297720
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2297721
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297722
    iget-object v0, p0, LX/Fsu;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297723
    iput-boolean v1, v0, LX/FsE;->g:Z

    .line 2297724
    iget-object v0, p0, LX/Fsu;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/TimelineFragment;

    .line 2297725
    if-nez v0, :cond_0

    .line 2297726
    :goto_0
    return-void

    .line 2297727
    :cond_0
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2297728
    if-eqz v1, :cond_2

    .line 2297729
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2297730
    check-cast v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2297731
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2297732
    check-cast v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2297733
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2297734
    check-cast v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2297735
    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-nez v2, :cond_3

    .line 2297736
    :cond_1
    :goto_1
    goto :goto_0

    .line 2297737
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->O()V

    goto :goto_0

    .line 2297738
    :cond_3
    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bg:LX/BQB;

    .line 2297739
    iget-object p0, v2, LX/BQB;->c:LX/BQD;

    const-string p1, "TimelineFetchProfilePicUri"

    invoke-virtual {p0, p1}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2297740
    invoke-static {v1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object p0

    .line 2297741
    if-eqz p0, :cond_1

    .line 2297742
    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bl:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1HI;

    sget-object p1, Lcom/facebook/timeline/TimelineFragment;->an:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p0, p1}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    goto :goto_1
.end method
