.class public final LX/FxU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V
    .locals 0

    .prologue
    .line 2304982
    iput-object p1, p0, LX/FxU;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2304983
    iget-object v0, p0, LX/FxU;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2304984
    :goto_0
    return-void

    .line 2304985
    :cond_0
    iget-object v0, p0, LX/FxU;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-static {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->x(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2304986
    check-cast p1, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    .line 2304987
    iget-object v0, p0, LX/FxU;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2304988
    :goto_0
    return-void

    .line 2304989
    :cond_0
    if-nez p1, :cond_1

    .line 2304990
    iget-object v0, p0, LX/FxU;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-static {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->x(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    goto :goto_0

    .line 2304991
    :cond_1
    new-instance v0, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;-><init>(ILcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;)V

    .line 2304992
    iget-object v1, p0, LX/FxU;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-static {v1, v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->b(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    .line 2304993
    iget-object v0, p0, LX/FxU;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-static {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->u(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V

    goto :goto_0
.end method
