.class public final LX/GKg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GJ4;


# instance fields
.field public final synthetic a:LX/GKj;


# direct methods
.method public constructor <init>(LX/GKj;)V
    .locals 0

    .prologue
    .line 2341244
    iput-object p1, p0, LX/GKg;->a:LX/GKj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2341245
    if-nez p1, :cond_0

    .line 2341246
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    invoke-static {v0}, LX/GKj;->h(LX/GKj;)V

    .line 2341247
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setVisibility(I)V

    .line 2341248
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341249
    iput v2, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    .line 2341250
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    .line 2341251
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->STANDARD:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 2341252
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 2341253
    :goto_0
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    .line 2341254
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2341255
    sget-object v1, LX/GG8;->INVALID_BID_AMOUNT:LX/GG8;

    iget-object v2, p0, LX/GKg;->a:LX/GKj;

    invoke-virtual {v2}, LX/GKj;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2341256
    return-void

    .line 2341257
    :cond_0
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setVisibility(I)V

    .line 2341258
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v1, v0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    invoke-virtual {v0}, LX/GKj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    invoke-static {v0}, LX/GKj;->j(LX/GKj;)I

    move-result v0

    .line 2341259
    :goto_1
    iput v0, v1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    .line 2341260
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    iget-object v1, p0, LX/GKg;->a:LX/GKj;

    iget-object v2, p0, LX/GKg;->a:LX/GKj;

    iget-object v2, v2, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341261
    iget p1, v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    move v2, p1

    .line 2341262
    invoke-static {v1, v2}, LX/GKj;->a$redex0(LX/GKj;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setBidAmount(Ljava/lang/CharSequence;)V

    .line 2341263
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GKg;->a:LX/GKj;

    iget-object v1, v1, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b48

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    .line 2341264
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget-object v0, v0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->NO_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 2341265
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 2341266
    goto :goto_0

    .line 2341267
    :cond_1
    iget-object v0, p0, LX/GKg;->a:LX/GKj;

    iget v0, v0, LX/GKj;->h:I

    goto :goto_1
.end method
