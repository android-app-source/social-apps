.class public LX/FFj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/7HC;

.field private final b:LX/1zC;


# direct methods
.method public constructor <init>(LX/7HC;LX/1zC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2217590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2217591
    iput-object p1, p0, LX/FFj;->a:LX/7HC;

    .line 2217592
    iput-object p2, p0, LX/FFj;->b:LX/1zC;

    .line 2217593
    return-void
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 4
    .param p0    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2217594
    if-eqz p0, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2217595
    :cond_0
    const/4 v1, 0x0

    .line 2217596
    :goto_0
    move-object v1, v1

    .line 2217597
    if-eqz v1, :cond_1

    .line 2217598
    sget-object v2, LX/FFq;->SUPPORTED_LIGHTWEIGHT_ACTIONS:LX/2QP;

    invoke-virtual {v2, v1}, LX/2QP;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 2217599
    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2, v3}, LX/6lp;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v2

    .line 2217600
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 2217601
    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bG()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
