.class public LX/Fbv;
.super LX/FbF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private static volatile b:LX/Fbv;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2261386
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x4c808d5

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, LX/Fbv;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261384
    invoke-direct {p0}, LX/FbF;-><init>()V

    .line 2261385
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbv;
    .locals 3

    .prologue
    .line 2261372
    sget-object v0, LX/Fbv;->b:LX/Fbv;

    if-nez v0, :cond_1

    .line 2261373
    const-class v1, LX/Fbv;

    monitor-enter v1

    .line 2261374
    :try_start_0
    sget-object v0, LX/Fbv;->b:LX/Fbv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261375
    if-eqz v2, :cond_0

    .line 2261376
    :try_start_1
    new-instance v0, LX/Fbv;

    invoke-direct {v0}, LX/Fbv;-><init>()V

    .line 2261377
    move-object v0, v0

    .line 2261378
    sput-object v0, LX/Fbv;->b:LX/Fbv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261379
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261380
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261381
    :cond_1
    sget-object v0, LX/Fbv;->b:LX/Fbv;

    return-object v0

    .line 2261382
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261383
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261365
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261366
    if-eqz v0, :cond_0

    .line 2261367
    invoke-static {v0}, LX/6X3;->b(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2261368
    if-eqz v0, :cond_0

    .line 2261369
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v1

    invoke-static {v1}, LX/A0T;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v1

    .line 2261370
    invoke-static {v0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;)V

    .line 2261371
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261364
    sget-object v0, LX/Fbv;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261362
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2261363
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
