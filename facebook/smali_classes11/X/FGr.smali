.class public final LX/FGr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2219583
    iput-object p1, p0, LX/FGr;->b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FGr;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219584
    iget-object v0, p0, LX/FGr;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 2219585
    iget-object v0, p0, LX/FGr;->b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FGr;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    sget-object v2, LX/FGa;->ENCRYPTING:LX/FGa;

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FGa;)V

    .line 2219586
    iget-object v0, p0, LX/FGr;->b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FGr;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219587
    invoke-static {v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->k(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v2

    .line 2219588
    iget-object p0, v2, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219589
    iget-object p0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    new-instance p1, LX/FGs;

    invoke-direct {p1, v0, v2}, LX/FGs;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V

    invoke-interface {p0, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2219590
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
