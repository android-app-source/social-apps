.class public final LX/Gqg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/GoE;

.field public final synthetic c:LX/CHX;

.field public final synthetic d:LX/Gqh;


# direct methods
.method public constructor <init>(LX/Gqh;ZLX/GoE;LX/CHX;)V
    .locals 0

    .prologue
    .line 2396799
    iput-object p1, p0, LX/Gqg;->d:LX/Gqh;

    iput-boolean p2, p0, LX/Gqg;->a:Z

    iput-object p3, p0, LX/Gqg;->b:LX/GoE;

    iput-object p4, p0, LX/Gqg;->c:LX/CHX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x69f0d255

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2396800
    iget-boolean v1, p0, LX/Gqg;->a:Z

    if-eqz v1, :cond_1

    .line 2396801
    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    iget-object v1, v1, LX/Gqh;->l:LX/Go0;

    iget-object v2, p0, LX/Gqg;->b:LX/GoE;

    invoke-virtual {v1, v2, v5}, LX/Go0;->a(LX/GoE;Ljava/util/Map;)V

    .line 2396802
    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    .line 2396803
    iget-object v2, v1, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 2396804
    move-object v1, v2

    .line 2396805
    sget-object v2, LX/Crd;->CLICK_MEDIA:LX/Crd;

    invoke-interface {v1, v2}, LX/Cre;->a(LX/Crd;)Z

    .line 2396806
    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    invoke-static {v1}, LX/Gqh;->p(LX/Gqh;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    iget-object v1, v1, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    if-eqz v1, :cond_0

    .line 2396807
    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    iget-object v1, v1, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    .line 2396808
    iget-object v2, v1, LX/Cts;->a:LX/Ctg;

    move-object v2, v2

    .line 2396809
    if-eqz v2, :cond_0

    .line 2396810
    iget-object v2, v1, LX/Cts;->a:LX/Ctg;

    move-object v2, v2

    .line 2396811
    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2396812
    :cond_0
    :goto_0
    const v1, -0x694f7812

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2396813
    :cond_1
    iget-object v1, p0, LX/Gqg;->c:LX/CHX;

    if-eqz v1, :cond_2

    .line 2396814
    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    iget-object v1, v1, LX/Gqh;->p:LX/Go7;

    const-string v2, "image_link_click"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/Gqg;->b:LX/GoE;

    invoke-virtual {v4}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/Gqg;->c:LX/CHX;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2396815
    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    iget-object v1, v1, LX/Gqh;->m:LX/GnF;

    iget-object v2, p0, LX/Gqg;->d:LX/Gqh;

    invoke-virtual {v2}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Gqg;->c:LX/CHX;

    iget-object v4, p0, LX/Gqg;->b:LX/GoE;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/GnF;->a(Landroid/content/Context;LX/CHX;LX/GoE;Ljava/util/Map;)V

    goto :goto_0

    .line 2396816
    :cond_2
    iget-object v1, p0, LX/Gqg;->d:LX/Gqh;

    iget-object v1, v1, LX/Gqh;->l:LX/Go0;

    iget-object v2, p0, LX/Gqg;->b:LX/GoE;

    new-instance v3, LX/Gqf;

    invoke-direct {v3, p0}, LX/Gqf;-><init>(LX/Gqg;)V

    invoke-virtual {v1, v2, v3}, LX/Go0;->a(LX/GoE;Ljava/util/Map;)V

    goto :goto_0

    .line 2396817
    :cond_3
    iget-object v2, v1, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a:LX/Ctg;

    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    .line 2396818
    invoke-virtual {v2}, LX/Cqj;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2396819
    invoke-virtual {v1}, LX/Cts;->j()LX/Cqw;

    move-result-object v2

    .line 2396820
    sget-object v3, LX/Cqw;->a:LX/Cqw;

    if-ne v2, v3, :cond_4

    .line 2396821
    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a()V

    goto :goto_0

    .line 2396822
    :cond_4
    sget-object v3, LX/Cqw;->b:LX/Cqw;

    if-ne v2, v3, :cond_0

    .line 2396823
    iget-object v2, v1, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a:LX/Ctg;

    sget-object v3, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v2, v3}, LX/Ctg;->b(LX/Cqw;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2396824
    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a()V

    goto :goto_0

    .line 2396825
    :cond_5
    iget-object v2, v1, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a:LX/Ctg;

    sget-object v3, LX/Cqw;->a:LX/Cqw;

    invoke-interface {v2, v3}, LX/Ctg;->b(LX/Cqw;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2396826
    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->k()V

    goto/16 :goto_0
.end method
