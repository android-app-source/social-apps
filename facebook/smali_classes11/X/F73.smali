.class public LX/F73;
.super LX/1Cv;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements LX/2ht;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Object;

.field private static final c:Ljava/lang/Object;

.field private static final d:Ljava/lang/Object;


# instance fields
.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/F6z;

.field private final j:LX/F70;

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/widget/Filter;

.field private n:I

.field public o:LX/F72;

.field public p:LX/1DI;

.field public final q:LX/17W;

.field private final r:LX/F7A;

.field public final s:LX/9Tk;

.field public final t:LX/F7X;

.field private final u:LX/2do;

.field public final v:LX/0Uh;

.field public final w:LX/2Ku;

.field public final x:Landroid/content/res/Resources;

.field private y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2201125
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/F73;->a:Ljava/lang/Object;

    .line 2201126
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/F73;->b:Ljava/lang/Object;

    .line 2201127
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/F73;->c:Ljava/lang/Object;

    .line 2201128
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/F73;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/17W;LX/F7A;LX/9Tk;LX/F7X;LX/2do;LX/0Uh;LX/0ad;LX/2Ku;Landroid/content/Context;)V
    .locals 3
    .param p9    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2201129
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2201130
    sget-object v0, LX/F72;->DEFAULT:LX/F72;

    iput-object v0, p0, LX/F73;->o:LX/F72;

    .line 2201131
    iput-object p1, p0, LX/F73;->q:LX/17W;

    .line 2201132
    iput-object p2, p0, LX/F73;->r:LX/F7A;

    .line 2201133
    iput-object p3, p0, LX/F73;->s:LX/9Tk;

    .line 2201134
    iput-object p4, p0, LX/F73;->t:LX/F7X;

    .line 2201135
    iput-object p5, p0, LX/F73;->u:LX/2do;

    .line 2201136
    iput-object p6, p0, LX/F73;->v:LX/0Uh;

    .line 2201137
    iput-object p8, p0, LX/F73;->w:LX/2Ku;

    .line 2201138
    invoke-virtual {p9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/F73;->x:Landroid/content/res/Resources;

    .line 2201139
    sget-short v0, LX/2ez;->a:S

    invoke-interface {p7, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/F73;->y:Z

    .line 2201140
    sget-short v0, LX/2ez;->b:S

    invoke-interface {p7, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/F73;->z:Z

    .line 2201141
    new-instance v0, LX/F6z;

    invoke-direct {v0, p0}, LX/F6z;-><init>(LX/F73;)V

    iput-object v0, p0, LX/F73;->i:LX/F6z;

    .line 2201142
    new-instance v0, LX/F70;

    invoke-direct {v0, p0}, LX/F70;-><init>(LX/F73;)V

    iput-object v0, p0, LX/F73;->j:LX/F70;

    .line 2201143
    iget-object v0, p0, LX/F73;->u:LX/2do;

    iget-object v1, p0, LX/F73;->i:LX/F6z;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2201144
    iget-object v0, p0, LX/F73;->u:LX/2do;

    iget-object v1, p0, LX/F73;->j:LX/F70;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2201145
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/F73;->k:Ljava/util/List;

    iput-object v0, p0, LX/F73;->e:Ljava/util/List;

    .line 2201146
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/F73;->l:Ljava/util/List;

    iput-object v0, p0, LX/F73;->f:Ljava/util/List;

    .line 2201147
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/F73;->g:Ljava/util/Map;

    .line 2201148
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/F73;->h:Ljava/util/Set;

    .line 2201149
    iput v2, p0, LX/F73;->n:I

    .line 2201150
    return-void
.end method

.method private a(LX/F71;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2201151
    iget-object v0, p0, LX/F73;->o:LX/F72;

    sget-object v3, LX/F72;->LOADING_MORE:LX/F72;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 2201152
    :goto_0
    sget-object v3, LX/F6w;->a:[I

    invoke-virtual {p1}, LX/F71;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2201153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 2201154
    goto :goto_0

    :pswitch_0
    move v2, v1

    .line 2201155
    :goto_1
    :pswitch_1
    return v2

    .line 2201156
    :pswitch_2
    iget-object v0, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v2, v0, 0x1

    goto :goto_1

    .line 2201157
    :pswitch_3
    iget-object v1, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int v2, v1, v0

    goto :goto_1

    .line 2201158
    :pswitch_4
    iget-object v1, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x1

    goto :goto_1

    .line 2201159
    :pswitch_5
    iget-object v1, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2201160
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306db

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z
    .locals 1

    .prologue
    .line 2201161
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)LX/F71;
    .locals 4

    .prologue
    .line 2201162
    iget-object v0, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 2201163
    iget-object v0, p0, LX/F73;->o:LX/F72;

    sget-object v2, LX/F72;->DEFAULT:LX/F72;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 2201164
    :goto_0
    if-nez p1, :cond_1

    .line 2201165
    sget-object v0, LX/F71;->CONTACTS_HEADER:LX/F71;

    .line 2201166
    :goto_1
    return-object v0

    .line 2201167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2201168
    :cond_1
    if-gt p1, v1, :cond_3

    .line 2201169
    iget-object v0, p0, LX/F73;->k:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201170
    invoke-virtual {v0}, LX/F7L;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-static {v0}, LX/F73;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/F71;->UNRESPONDED_CONTACT:LX/F71;

    goto :goto_1

    :cond_2
    sget-object v0, LX/F71;->CONTACT:LX/F71;

    goto :goto_1

    .line 2201171
    :cond_3
    iget-object v2, p0, LX/F73;->o:LX/F72;

    sget-object v3, LX/F72;->LOADING_MORE:LX/F72;

    if-ne v2, v3, :cond_4

    add-int/lit8 v2, v1, 0x1

    if-ne p1, v2, :cond_4

    .line 2201172
    sget-object v0, LX/F71;->LOADING:LX/F71;

    goto :goto_1

    .line 2201173
    :cond_4
    iget-object v2, p0, LX/F73;->o:LX/F72;

    sget-object v3, LX/F72;->FAILURE:LX/F72;

    if-ne v2, v3, :cond_5

    add-int/lit8 v2, v1, 0x1

    if-ne p1, v2, :cond_5

    .line 2201174
    sget-object v0, LX/F71;->FAILURE:LX/F71;

    goto :goto_1

    .line 2201175
    :cond_5
    add-int/lit8 v2, v1, 0x1

    add-int/2addr v2, v0

    if-ne p1, v2, :cond_6

    .line 2201176
    sget-object v0, LX/F71;->MANAGE_CONTACTS:LX/F71;

    goto :goto_1

    .line 2201177
    :cond_6
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2201178
    if-ne p1, v0, :cond_7

    .line 2201179
    sget-object v0, LX/F71;->PYMK_HEADER:LX/F71;

    goto :goto_1

    .line 2201180
    :cond_7
    iget-object v1, p0, LX/F73;->l:Ljava/util/List;

    sub-int v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201181
    invoke-virtual {v0}, LX/F7L;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-static {v0}, LX/F73;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, LX/F71;->UNRESPONDED_PYMK:LX/F71;

    goto :goto_1

    :cond_8
    sget-object v0, LX/F71;->PYMK:LX/F71;

    goto :goto_1
.end method

.method private static d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;
    .locals 2

    .prologue
    .line 2201182
    new-instance v0, Lcom/facebook/friending/common/list/FriendListItemView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;-><init>(Landroid/content/Context;)V

    .line 2201183
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2201184
    return-object v0
.end method

.method private static e(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendRequestItemView;
    .locals 2

    .prologue
    .line 2201253
    new-instance v0, Lcom/facebook/friending/common/list/FriendRequestItemView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;-><init>(Landroid/content/Context;)V

    .line 2201254
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2201255
    return-object v0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2201185
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/F73;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 2201186
    :goto_0
    return p1

    :cond_0
    add-int/lit8 p1, p1, -0x2

    goto :goto_0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2201187
    sget-object v0, LX/F6w;->a:[I

    invoke-static {}, LX/F71;->values()[LX/F71;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2201188
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2201189
    :pswitch_0
    invoke-static {p2}, LX/F73;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2201190
    const v1, 0x7f0833c5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2201191
    move-object v0, v0

    .line 2201192
    :goto_0
    return-object v0

    .line 2201193
    :pswitch_1
    iget-boolean v0, p0, LX/F73;->z:Z

    if-eqz v0, :cond_0

    .line 2201194
    invoke-static {p2}, LX/F73;->e(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendRequestItemView;

    move-result-object v0

    goto :goto_0

    .line 2201195
    :cond_0
    :pswitch_2
    invoke-static {p2}, LX/F73;->d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;

    move-result-object v0

    goto :goto_0

    .line 2201196
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306d6

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2201197
    goto :goto_0

    .line 2201198
    :pswitch_4
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306d8

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2201199
    new-instance v1, LX/F6u;

    invoke-direct {v1, p0}, LX/F6u;-><init>(LX/F73;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201200
    move-object v0, v0

    .line 2201201
    goto :goto_0

    .line 2201202
    :pswitch_5
    invoke-static {p2}, LX/F73;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2201203
    const v1, 0x7f0833c6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2201204
    move-object v0, v0

    .line 2201205
    goto :goto_0

    .line 2201206
    :pswitch_6
    iget-boolean v0, p0, LX/F73;->y:Z

    if-eqz v0, :cond_1

    .line 2201207
    invoke-static {p2}, LX/F73;->e(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendRequestItemView;

    move-result-object v0

    goto :goto_0

    .line 2201208
    :cond_1
    :pswitch_7
    invoke-static {p2}, LX/F73;->d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2201209
    iget-object v0, p0, LX/F73;->u:LX/2do;

    iget-object v1, p0, LX/F73;->i:LX/F6z;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2201210
    iget-object v0, p0, LX/F73;->u:LX/2do;

    iget-object v1, p0, LX/F73;->j:LX/F70;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2201211
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 2201212
    sget-object v0, LX/F6w;->a:[I

    invoke-static {}, LX/F71;->values()[LX/F71;

    move-result-object v1

    aget-object v1, v1, p4

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2201213
    :goto_0
    :pswitch_0
    return-void

    .line 2201214
    :pswitch_1
    iget-boolean v0, p0, LX/F73;->z:Z

    if-eqz v0, :cond_0

    .line 2201215
    check-cast p2, LX/F7L;

    .line 2201216
    iget-object v0, p0, LX/F73;->r:LX/F7A;

    check-cast p3, Lcom/facebook/friending/common/list/FriendRequestItemView;

    invoke-virtual {p2}, LX/F7L;->a()J

    move-result-wide v2

    .line 2201217
    new-instance v1, LX/F6v;

    invoke-direct {v1, p0, v2, v3}, LX/F6v;-><init>(LX/F73;J)V

    move-object v1, v1

    .line 2201218
    invoke-static {v0, p3, p2}, LX/F7A;->a(LX/F7A;Lcom/facebook/fbui/widget/contentview/ContentView;LX/83X;)V

    .line 2201219
    const v2, 0x7f080f7b

    invoke-static {v0, v2}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v2

    const v3, 0x7f080f7c

    invoke-static {v0, v3}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Lcom/facebook/friending/common/list/FriendRequestItemView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2201220
    const v2, 0x7f0833cb

    invoke-static {v0, v2}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2201221
    new-instance v2, LX/F77;

    invoke-direct {v2, v0, p2}, LX/F77;-><init>(LX/F7A;LX/83X;)V

    invoke-virtual {p3, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201222
    invoke-virtual {p3, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201223
    goto :goto_0

    .line 2201224
    :cond_0
    :pswitch_2
    iget-object v0, p0, LX/F73;->r:LX/F7A;

    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    check-cast p2, LX/F7L;

    invoke-virtual {v0, p3, p2}, LX/F7A;->a(Lcom/facebook/friending/common/list/FriendListItemView;LX/83X;)V

    goto :goto_0

    .line 2201225
    :pswitch_3
    iget-boolean v0, p0, LX/F73;->y:Z

    if-eqz v0, :cond_1

    .line 2201226
    iget-object v0, p0, LX/F73;->r:LX/F7A;

    check-cast p3, Lcom/facebook/friending/common/list/FriendRequestItemView;

    check-cast p2, LX/F7L;

    .line 2201227
    invoke-static {v0, p3, p2}, LX/F7A;->a(LX/F7A;Lcom/facebook/fbui/widget/contentview/ContentView;LX/83X;)V

    .line 2201228
    const v1, 0x7f080f7b

    invoke-static {v0, v1}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f080f7c

    invoke-static {v0, v2}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2201229
    const v1, 0x7f080f78

    invoke-static {v0, v1}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2201230
    new-instance v1, LX/F75;

    invoke-direct {v1, v0, p2}, LX/F75;-><init>(LX/F7A;LX/83X;)V

    invoke-virtual {p3, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201231
    new-instance v1, LX/F76;

    invoke-direct {v1, v0, p2}, LX/F76;-><init>(LX/F7A;LX/83X;)V

    invoke-virtual {p3, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201232
    goto/16 :goto_0

    .line 2201233
    :cond_1
    :pswitch_4
    iget-object v0, p0, LX/F73;->r:LX/F7A;

    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    check-cast p2, LX/F7L;

    invoke-virtual {v0, p3, p2}, LX/F7A;->a(Lcom/facebook/friending/common/list/FriendListItemView;LX/83X;)V

    goto/16 :goto_0

    .line 2201234
    :pswitch_5
    check-cast p3, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto/16 :goto_0

    .line 2201235
    :pswitch_6
    check-cast p3, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v0, p0, LX/F73;->x:Landroid/content/res/Resources;

    const v1, 0x7f08003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/F73;->p:LX/1DI;

    invoke-virtual {p3, v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/F72;)V
    .locals 1

    .prologue
    .line 2201236
    iget-object v0, p0, LX/F73;->o:LX/F72;

    if-eq p1, v0, :cond_0

    .line 2201237
    iput-object p1, p0, LX/F73;->o:LX/F72;

    .line 2201238
    const v0, 0x59a4db29

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2201239
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2201118
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2201119
    :cond_0
    :goto_0
    return-void

    .line 2201120
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201121
    iget-object v2, p0, LX/F73;->g:Ljava/util/Map;

    invoke-virtual {v0}, LX/F7L;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2201122
    iget-object v2, p0, LX/F73;->h:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2201123
    :cond_2
    iget-object v0, p0, LX/F73;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2201124
    const v0, 0x6d32f7

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2201240
    invoke-direct {p0, p1}, LX/F73;->b(I)LX/F71;

    move-result-object v2

    .line 2201241
    if-nez p2, :cond_3

    .line 2201242
    invoke-static {p3}, LX/F73;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 2201243
    check-cast v0, Landroid/widget/TextView;

    sget-object v3, LX/F71;->PYMK_HEADER:LX/F71;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/F71;->UNRESPONDED_PYMK:LX/F71;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/F71;->PYMK:LX/F71;

    if-ne v2, v3, :cond_2

    :cond_0
    const v2, 0x7f0833c6

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2201244
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 2201245
    iget v0, p0, LX/F73;->n:I

    if-nez v0, :cond_1

    .line 2201246
    const/4 p1, 0x0

    .line 2201247
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2201248
    invoke-static {p1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    .line 2201249
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    move v0, v0

    .line 2201250
    iput v0, p0, LX/F73;->n:I

    .line 2201251
    :cond_1
    return-object v1

    .line 2201252
    :cond_2
    const v2, 0x7f0833c5

    goto :goto_1

    :cond_3
    move-object v1, p2

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2201115
    iget-object v0, p0, LX/F73;->u:LX/2do;

    iget-object v1, p0, LX/F73;->i:LX/F6z;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2201116
    iget-object v0, p0, LX/F73;->u:LX/2do;

    iget-object v1, p0, LX/F73;->j:LX/F70;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2201117
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2201109
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2201110
    :cond_0
    :goto_0
    return-void

    .line 2201111
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201112
    iget-object v2, p0, LX/F73;->g:Ljava/util/Map;

    invoke-virtual {v0}, LX/F7L;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2201113
    :cond_2
    iget-object v0, p0, LX/F73;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2201114
    const v0, 0x7b5e6753

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2201108
    iget-object v0, p0, LX/F73;->x:Landroid/content/res/Resources;

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2201107
    iget-object v0, p0, LX/F73;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2201082
    iget v0, p0, LX/F73;->n:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2201106
    iget-object v0, p0, LX/F73;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 2

    .prologue
    .line 2201105
    invoke-direct {p0, p1}, LX/F73;->b(I)LX/F71;

    move-result-object v0

    sget-object v1, LX/F71;->PYMK_HEADER:LX/F71;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2201101
    iget-object v1, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    .line 2201102
    iget-object v2, p0, LX/F73;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    .line 2201103
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 2201104
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v1, 0x1

    iget-object v1, p0, LX/F73;->o:LX/F72;

    sget-object v4, LX/F72;->DEFAULT:LX/F72;

    if-eq v1, v4, :cond_1

    const/4 v1, 0x1

    :goto_1
    add-int/2addr v1, v3

    if-eqz v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    move v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, LX/F73;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 2201098
    iget-object v0, p0, LX/F73;->m:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 2201099
    new-instance v0, LX/F6y;

    invoke-direct {v0, p0}, LX/F6y;-><init>(LX/F73;)V

    iput-object v0, p0, LX/F73;->m:Landroid/widget/Filter;

    .line 2201100
    :cond_0
    iget-object v0, p0, LX/F73;->m:Landroid/widget/Filter;

    return-object v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2201087
    invoke-direct {p0, p1}, LX/F73;->b(I)LX/F71;

    move-result-object v0

    .line 2201088
    invoke-direct {p0, v0}, LX/F73;->a(LX/F71;)I

    move-result v1

    sub-int v1, p1, v1

    .line 2201089
    sget-object v2, LX/F6w;->a:[I

    invoke-virtual {v0}, LX/F71;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2201090
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2201091
    :pswitch_0
    sget-object v0, LX/F73;->a:Ljava/lang/Object;

    .line 2201092
    :goto_0
    return-object v0

    .line 2201093
    :pswitch_1
    iget-object v0, p0, LX/F73;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2201094
    :pswitch_2
    sget-object v0, LX/F73;->d:Ljava/lang/Object;

    goto :goto_0

    .line 2201095
    :pswitch_3
    sget-object v0, LX/F73;->c:Ljava/lang/Object;

    goto :goto_0

    .line 2201096
    :pswitch_4
    sget-object v0, LX/F73;->b:Ljava/lang/Object;

    goto :goto_0

    .line 2201097
    :pswitch_5
    iget-object v0, p0, LX/F73;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2201086
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2201085
    invoke-direct {p0, p1}, LX/F73;->b(I)LX/F71;

    move-result-object v0

    invoke-virtual {v0}, LX/F71;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2201084
    invoke-static {}, LX/F71;->values()[LX/F71;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2201083
    const/4 v0, 0x0

    return v0
.end method
