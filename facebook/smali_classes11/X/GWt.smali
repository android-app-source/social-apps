.class public LX/GWt;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/GWq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7j6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7iP;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2362793
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2362794
    const/4 p1, 0x0

    .line 2362795
    const-class v0, LX/GWt;

    invoke-static {v0, p0}, LX/GWt;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2362796
    const v0, 0x7f031036

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2362797
    invoke-virtual {p0}, LX/GWt;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2362798
    const v0, 0x7f0d26de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GWt;->d:Landroid/widget/TextView;

    .line 2362799
    const v0, 0x7f0d26df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/GWt;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2362800
    iget-object v0, p0, LX/GWt;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/1P0;

    invoke-virtual {p0}, LX/GWt;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p1, p1}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2362801
    iget-object v0, p0, LX/GWt;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/GWn;

    invoke-direct {v2, v1}, LX/GWn;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2362802
    iget-object v0, p0, LX/GWt;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/GWt;->a:LX/GWq;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2362803
    return-void
.end method

.method public static a(LX/15i;I)LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFilteredList"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2362804
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2362805
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    new-instance v1, LX/GWs;

    invoke-direct {v1}, LX/GWs;-><init>()V

    invoke-static {v0, v1}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 2362806
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2362807
    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GWt;

    new-instance v1, LX/GWq;

    const/16 v0, 0x18c5

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-direct {v1, v0}, LX/GWq;-><init>(LX/0Ot;)V

    move-object v1, v1

    check-cast v1, LX/GWq;

    invoke-static {p0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object p0

    check-cast p0, LX/7j6;

    iput-object v1, p1, LX/GWt;->a:LX/GWq;

    iput-object p0, p1, LX/GWt;->b:LX/7j6;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2362808
    check-cast p1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    const/4 v2, 0x0

    .line 2362809
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->k()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel$CommerceStoreModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2362810
    iget-object v0, p0, LX/GWt;->d:Landroid/widget/TextView;

    new-instance v1, LX/GWr;

    invoke-direct {v1, p0, p1}, LX/GWr;-><init>(LX/GWt;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2362811
    iget-object v0, p0, LX/GWt;->d:Landroid/widget/TextView;

    const v1, 0x7f0207eb

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2362812
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/GWt;->a:LX/GWq;

    invoke-static {v1, v0}, LX/GWt;->a(LX/15i;I)LX/0Px;

    move-result-object v0

    .line 2362813
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    iput-object v1, v2, LX/GWq;->b:LX/0Px;

    .line 2362814
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2362815
    iget-object v0, p0, LX/GWt;->a:LX/GWq;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2362816
    return-void

    .line 2362817
    :cond_0
    iget-object v0, p0, LX/GWt;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2362818
    iget-object v0, p0, LX/GWt;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
