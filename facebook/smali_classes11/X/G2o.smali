.class public LX/G2o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jW;


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2314514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314515
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/G2o;->c:Ljava/lang/String;

    .line 2314516
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/G2o;->a:Ljava/util/ArrayList;

    .line 2314517
    iput-object p2, p0, LX/G2o;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2314518
    return-void
.end method


# virtual methods
.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2314519
    iget-object v0, p0, LX/G2o;->c:Ljava/lang/String;

    return-object v0
.end method
