.class public final enum LX/G5Q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G5Q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G5Q;

.field public static final enum APP_BACKGROUNDED:LX/G5Q;

.field public static final enum BACK_PRESSED:LX/G5Q;

.field public static final enum SEARCH_EXIT_PRESSED:LX/G5Q;

.field public static final enum USER_CLICKED_INLINE_CALL:LX/G5Q;

.field public static final enum USER_CLICKED_RESULT:LX/G5Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2318721
    new-instance v0, LX/G5Q;

    const-string v1, "APP_BACKGROUNDED"

    invoke-direct {v0, v1, v2}, LX/G5Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5Q;->APP_BACKGROUNDED:LX/G5Q;

    .line 2318722
    new-instance v0, LX/G5Q;

    const-string v1, "BACK_PRESSED"

    invoke-direct {v0, v1, v3}, LX/G5Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5Q;->BACK_PRESSED:LX/G5Q;

    .line 2318723
    new-instance v0, LX/G5Q;

    const-string v1, "SEARCH_EXIT_PRESSED"

    invoke-direct {v0, v1, v4}, LX/G5Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5Q;->SEARCH_EXIT_PRESSED:LX/G5Q;

    .line 2318724
    new-instance v0, LX/G5Q;

    const-string v1, "USER_CLICKED_INLINE_CALL"

    invoke-direct {v0, v1, v5}, LX/G5Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5Q;->USER_CLICKED_INLINE_CALL:LX/G5Q;

    .line 2318725
    new-instance v0, LX/G5Q;

    const-string v1, "USER_CLICKED_RESULT"

    invoke-direct {v0, v1, v6}, LX/G5Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5Q;->USER_CLICKED_RESULT:LX/G5Q;

    .line 2318726
    const/4 v0, 0x5

    new-array v0, v0, [LX/G5Q;

    sget-object v1, LX/G5Q;->APP_BACKGROUNDED:LX/G5Q;

    aput-object v1, v0, v2

    sget-object v1, LX/G5Q;->BACK_PRESSED:LX/G5Q;

    aput-object v1, v0, v3

    sget-object v1, LX/G5Q;->SEARCH_EXIT_PRESSED:LX/G5Q;

    aput-object v1, v0, v4

    sget-object v1, LX/G5Q;->USER_CLICKED_INLINE_CALL:LX/G5Q;

    aput-object v1, v0, v5

    sget-object v1, LX/G5Q;->USER_CLICKED_RESULT:LX/G5Q;

    aput-object v1, v0, v6

    sput-object v0, LX/G5Q;->$VALUES:[LX/G5Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2318727
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G5Q;
    .locals 1

    .prologue
    .line 2318728
    const-class v0, LX/G5Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G5Q;

    return-object v0
.end method

.method public static values()[LX/G5Q;
    .locals 1

    .prologue
    .line 2318729
    sget-object v0, LX/G5Q;->$VALUES:[LX/G5Q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G5Q;

    return-object v0
.end method
