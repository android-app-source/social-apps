.class public LX/G1I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protiles/model/ProtileModel;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/G1M;

.field private final c:LX/G1N;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2311048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2311049
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/G1I;->a:Ljava/util/List;

    .line 2311050
    new-instance v0, LX/G1M;

    invoke-direct {v0}, LX/G1M;-><init>()V

    iput-object v0, p0, LX/G1I;->b:LX/G1M;

    .line 2311051
    new-instance v0, LX/G1N;

    invoke-direct {v0}, LX/G1N;-><init>()V

    iput-object v0, p0, LX/G1I;->c:LX/G1N;

    .line 2311052
    return-void
.end method

.method public static a(LX/0QB;)LX/G1I;
    .locals 3

    .prologue
    .line 2311037
    const-class v1, LX/G1I;

    monitor-enter v1

    .line 2311038
    :try_start_0
    sget-object v0, LX/G1I;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2311039
    sput-object v2, LX/G1I;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2311040
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2311041
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2311042
    new-instance v0, LX/G1I;

    invoke-direct {v0}, LX/G1I;-><init>()V

    .line 2311043
    move-object v0, v0

    .line 2311044
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2311045
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G1I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2311046
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2311047
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/G1I;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)Lcom/facebook/timeline/protiles/model/ProtileModel;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2311034
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2311035
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 2311036
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2311023
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2311024
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2311025
    :goto_0
    return-object v0

    .line 2311026
    :cond_0
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    .line 2311027
    invoke-virtual {p0}, LX/G1I;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2311028
    if-nez v0, :cond_1

    .line 2311029
    iget-object v0, p0, LX/G1I;->b:LX/G1M;

    goto :goto_0

    .line 2311030
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 2311031
    :cond_2
    if-nez v0, :cond_3

    iget-object v1, p0, LX/G1I;->c:LX/G1N;

    invoke-virtual {v1}, LX/BPB;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2311032
    iget-object v0, p0, LX/G1I;->c:LX/G1N;

    goto :goto_0

    .line 2311033
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid index:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2311020
    iget-object v0, p0, LX/G1I;->b:LX/G1M;

    sget-object v1, LX/G1L;->COMPLETED:LX/G1L;

    .line 2311021
    iput-object v1, v0, LX/G1M;->a:LX/G1L;

    .line 2311022
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;ZJ)V
    .locals 7

    .prologue
    .line 2310999
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2311000
    :cond_0
    return-void

    .line 2311001
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 2311002
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2311003
    new-instance v4, Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-direct {v4, v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;-><init>(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;)V

    .line 2311004
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 2311005
    iput-object v0, v4, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    .line 2311006
    iput-boolean p3, v4, Lcom/facebook/timeline/protiles/model/ProtileModel;->g:Z

    .line 2311007
    invoke-virtual {v4, p4, p5}, Lcom/facebook/graphql/model/BaseFeedUnit;->a(J)V

    .line 2311008
    const/4 v0, 0x0

    move v5, v0

    :goto_1
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_3

    .line 2311009
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v6

    if-ne v0, v6, :cond_2

    .line 2311010
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0, v5, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2311011
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2311012
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 2311013
    :cond_3
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v5

    .line 2311014
    sget-object v6, LX/G1H;->a:[I

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->ordinal()I

    move-result p2

    aget v6, v6, p2

    packed-switch v6, :pswitch_data_0

    .line 2311015
    iget-object v6, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    :goto_3
    move v5, v6

    .line 2311016
    invoke-interface {v0, v5, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2

    .line 2311017
    :pswitch_0
    iget-object v6, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 p2, 0x0

    invoke-static {v6, p2}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_3

    .line 2311018
    :pswitch_1
    iget-object v6, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 p2, 0x1

    invoke-static {v6, p2}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_3

    .line 2311019
    :pswitch_2
    iget-object v6, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 p2, 0x2

    invoke-static {v6, p2}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()LX/G1N;
    .locals 1

    .prologue
    .line 2310998
    iget-object v0, p0, LX/G1I;->c:LX/G1N;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2310991
    iget-object v0, p0, LX/G1I;->b:LX/G1M;

    invoke-virtual {v0}, LX/G1M;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/G1I;->b:LX/G1M;

    invoke-virtual {v0}, LX/G1M;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 2310992
    iget-object v0, p0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2310993
    invoke-virtual {p0}, LX/G1I;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2310994
    add-int/lit8 v0, v0, 0x1

    .line 2310995
    :cond_0
    iget-object v1, p0, LX/G1I;->c:LX/G1N;

    invoke-virtual {v1}, LX/BPB;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2310996
    add-int/lit8 v0, v0, 0x1

    .line 2310997
    :cond_1
    return v0
.end method
