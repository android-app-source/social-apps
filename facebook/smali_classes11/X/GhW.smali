.class public final LX/GhW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9o7;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;LX/9o7;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2384354
    iput-object p1, p0, LX/GhW;->c:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    iput-object p2, p0, LX/GhW;->a:LX/9o7;

    iput-object p3, p0, LX/GhW;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x5f516809

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2384355
    new-instance v1, LX/89k;

    invoke-direct {v1}, LX/89k;-><init>()V

    iget-object v2, p0, LX/GhW;->a:LX/9o7;

    invoke-interface {v2}, LX/9o7;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2384356
    iput-object v2, v1, LX/89k;->b:Ljava/lang/String;

    .line 2384357
    move-object v1, v1

    .line 2384358
    iget-object v2, p0, LX/GhW;->a:LX/9o7;

    invoke-interface {v2}, LX/9o7;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2384359
    iput-object v2, v1, LX/89k;->c:Ljava/lang/String;

    .line 2384360
    move-object v1, v1

    .line 2384361
    invoke-virtual {v1}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    .line 2384362
    iget-object v2, p0, LX/GhW;->c:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    iget-object v2, v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/GhW;->c:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    iget-object v3, v3, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->e:LX/0hy;

    invoke-interface {v3, v1}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    iget-object v3, p0, LX/GhW;->b:LX/1Pn;

    invoke-interface {v3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2384363
    const v1, 0x104a903b

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
