.class public LX/EzT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/End;


# instance fields
.field public final a:LX/2dk;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/2h7;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/Ene;

.field private final f:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2dk;Ljava/util/concurrent/ExecutorService;LX/Ene;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # LX/Ene;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2187228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187229
    iput-object p1, p0, LX/EzT;->a:LX/2dk;

    .line 2187230
    iput-object p2, p0, LX/EzT;->b:Ljava/util/concurrent/ExecutorService;

    .line 2187231
    iput-object p3, p0, LX/EzT;->e:LX/Ene;

    .line 2187232
    iput-object p4, p0, LX/EzT;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2187233
    if-eqz p5, :cond_1

    const-string v0, "section_type"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2187234
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2187235
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->c:LX/0am;

    .line 2187236
    :goto_1
    if-eqz p5, :cond_5

    const-string v0, "end_cursor"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2187237
    :goto_2
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2187238
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->g:LX/0am;

    .line 2187239
    :goto_3
    if-eqz p5, :cond_0

    const-string v0, "friending_location"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2187240
    :cond_0
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2187241
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->d:LX/0am;

    .line 2187242
    :goto_4
    return-void

    :cond_1
    move-object v0, v1

    .line 2187243
    goto :goto_0

    .line 2187244
    :cond_2
    const-string v2, "friend_requests"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "pymk"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2187245
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->c:LX/0am;

    goto :goto_1

    .line 2187246
    :cond_4
    const/4 v2, 0x0

    goto :goto_5

    :cond_5
    move-object v0, v1

    .line 2187247
    goto :goto_2

    .line 2187248
    :cond_6
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->g:LX/0am;

    goto :goto_3

    .line 2187249
    :cond_7
    const-string v0, "friending_location"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2h7;->valueOf(Ljava/lang/String;)LX/2h7;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->d:LX/0am;

    goto :goto_4
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/2h7;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2187223
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2187224
    const-string v1, "section_type"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187225
    const-string v1, "end_cursor"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187226
    const-string v1, "friending_location"

    invoke-virtual {p2}, LX/2h7;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187227
    return-object v0
.end method

.method public static a$redex0(LX/EzT;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 1

    .prologue
    .line 2187219
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2187220
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->g:LX/0am;

    .line 2187221
    :goto_0
    return-void

    .line 2187222
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EzT;->g:LX/0am;

    goto :goto_0
.end method

.method private b()Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2187218
    const-string v0, "friend_requests"

    iget-object v1, p0, LX/EzT;->c:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2187215
    iget-object v0, p0, LX/EzT;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EzT;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2187216
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2187217
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/EzT;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, LX/EzT;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, LX/EzT;->d:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2h7;

    invoke-static {v0, v1, v2}, LX/EzT;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Enq;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2187202
    sget-object v0, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v0, :cond_0

    .line 2187203
    iget-object v0, p0, LX/EzT;->e:LX/Ene;

    invoke-virtual {v0, p1, p2}, LX/Ene;->a(LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2187204
    :goto_0
    return-object v0

    .line 2187205
    :cond_0
    iget-object v0, p0, LX/EzT;->e:LX/Ene;

    invoke-virtual {v0, p1, p2}, LX/Ene;->b(LX/Enq;I)LX/En3;

    move-result-object v0

    .line 2187206
    invoke-virtual {v0}, LX/En2;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/EzT;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2187207
    :cond_1
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2187208
    :cond_2
    invoke-direct {p0}, LX/EzT;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2187209
    iget-object v0, p0, LX/EzT;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2187210
    iget-object v2, p0, LX/EzT;->a:LX/2dk;

    iget-object v1, p0, LX/EzT;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1, p2, v0}, LX/2dk;->a(Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/EzR;

    invoke-direct {v2, p0, p2}, LX/EzR;-><init>(LX/EzT;I)V

    iget-object p1, p0, LX/EzT;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2187211
    goto :goto_0

    .line 2187212
    :cond_3
    iget-object v0, p0, LX/EzT;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2187213
    iget-object p1, p0, LX/EzT;->a:LX/2dk;

    iget-object v1, p0, LX/EzT;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, LX/EzT;->d:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2h7;

    iget-object v2, v2, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual {p1, v1, p2, v2, v0}, LX/2dk;->b(Ljava/lang/String;ILX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/EzS;

    invoke-direct {v2, p0, p2}, LX/EzS;-><init>(LX/EzT;I)V

    iget-object p1, p0, LX/EzT;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2187214
    goto :goto_0
.end method

.method public final a(LX/Enq;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2187197
    sget-object v1, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v1, :cond_1

    .line 2187198
    iget-object v0, p0, LX/EzT;->e:LX/Ene;

    invoke-virtual {v0, p1}, LX/Ene;->a(LX/Enq;)Z

    move-result v0

    .line 2187199
    :cond_0
    :goto_0
    return v0

    .line 2187200
    :cond_1
    iget-object v1, p0, LX/EzT;->e:LX/Ene;

    invoke-virtual {v1, p1}, LX/Ene;->a(LX/Enq;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2187201
    iget-object v1, p0, LX/EzT;->c:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EzT;->d:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EzT;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
