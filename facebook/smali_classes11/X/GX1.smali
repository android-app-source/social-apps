.class public LX/GX1;
.super LX/0gG;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2362984
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2362985
    iput-object p1, p0, LX/GX1;->a:Landroid/content/Context;

    .line 2362986
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2362987
    const/4 v0, -0x2

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2362988
    new-instance v1, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;

    iget-object v0, p0, LX/GX1;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;-><init>(Landroid/content/Context;)V

    .line 2362989
    iget-object v0, p0, LX/GX1;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2362990
    iget-object v2, v1, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object p2, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2362991
    iget-object v0, p0, LX/GX1;->c:Landroid/view/View$OnClickListener;

    .line 2362992
    iget-object v2, v1, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2362993
    iget-object v3, v1, Lcom/facebook/commerce/productdetails/ui/userinteractions/ProductGroupUserInteractionsImageItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    if-nez v0, :cond_0

    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v3, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2362994
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2362995
    return-object v1

    .line 2362996
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2362997
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2362998
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2362999
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2363000
    iget-object v0, p0, LX/GX1;->b:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GX1;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/GX1;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
