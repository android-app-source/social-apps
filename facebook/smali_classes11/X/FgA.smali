.class public final LX/FgA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/CwB;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/CwW;

.field public final synthetic e:LX/8ci;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:LX/FgF;


# direct methods
.method public constructor <init>(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;LX/CwW;LX/8ci;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2269673
    iput-object p1, p0, LX/FgA;->g:LX/FgF;

    iput-object p2, p0, LX/FgA;->a:Ljava/lang/String;

    iput-object p3, p0, LX/FgA;->b:LX/CwB;

    iput-object p4, p0, LX/FgA;->c:Ljava/lang/String;

    iput-object p5, p0, LX/FgA;->d:LX/CwW;

    iput-object p6, p0, LX/FgA;->e:LX/8ci;

    iput-object p7, p0, LX/FgA;->f:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 3

    .prologue
    .line 2269674
    iget-object v0, p0, LX/FgA;->g:LX/FgF;

    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->RESULTS_DATA_LOADER_ERROR:LX/3Ql;

    invoke-direct {v1, v2, p1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/Throwable;)V

    invoke-static {v0, v1}, LX/FgF;->a$redex0(LX/FgF;LX/7C4;)V

    .line 2269675
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2269676
    iget-object v0, p0, LX/FgA;->g:LX/FgF;

    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->RESULTS_DATA_LOADER_ERROR:LX/3Ql;

    invoke-direct {v1, v2, p1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/Throwable;)V

    invoke-static {v0, v1}, LX/FgF;->a$redex0(LX/FgF;LX/7C4;)V

    .line 2269677
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2269678
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2269679
    iget-object v0, p0, LX/FgA;->g:LX/FgF;

    iget-object v1, p0, LX/FgA;->a:Ljava/lang/String;

    iget-object v2, p0, LX/FgA;->b:LX/CwB;

    iget-object v3, p0, LX/FgA;->c:Ljava/lang/String;

    .line 2269680
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2269681
    check-cast v4, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;

    iget-object v5, p0, LX/FgA;->d:LX/CwW;

    iget-object v6, p0, LX/FgA;->e:LX/8ci;

    iget-object v7, p0, LX/FgA;->f:Ljava/lang/String;

    .line 2269682
    invoke-static/range {v0 .. v7}, LX/FgF;->a$redex0(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;LX/CwW;LX/8ci;Ljava/lang/String;)V

    .line 2269683
    return-void
.end method
