.class public final LX/FM4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Un;


# direct methods
.method public constructor <init>(LX/2Un;)V
    .locals 0

    .prologue
    .line 2228298
    iput-object p1, p0, LX/FM4;->a:LX/2Un;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2228299
    const/4 v8, 0x0

    .line 2228300
    :try_start_0
    iget-object v0, p0, LX/FM4;->a:LX/2Un;

    .line 2228301
    invoke-static {v0}, LX/2Un;->m(LX/2Un;)LX/0Rf;

    move-result-object v2

    .line 2228302
    invoke-virtual {v2}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2228303
    :goto_0
    iget-object v0, p0, LX/FM4;->a:LX/2Un;

    iget-object v0, v0, LX/2Un;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Kr;->aa:LX/0Tn;

    iget-object v2, p0, LX/FM4;->a:LX/2Un;

    iget-object v2, v2, LX/2Un;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x240c8400

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2228304
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2228305
    :goto_1
    return-object v0

    .line 2228306
    :catch_0
    move-exception v0

    .line 2228307
    iget-object v1, p0, LX/FM4;->a:LX/2Un;

    iget-object v1, v1, LX/2Un;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3Kr;->aa:LX/0Tn;

    iget-object v3, p0, LX/FM4;->a:LX/2Un;

    iget-object v3, v3, LX/2Un;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2228308
    sget-object v1, LX/2Un;->a:Ljava/lang/String;

    const-string v2, "Failed to sync sms business numbers"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2228309
    new-instance v0, LX/2YS;

    invoke-direct {v0, v8}, LX/2YS;-><init>(Z)V

    goto :goto_1

    .line 2228310
    :cond_0
    iget-object v1, v0, LX/2Un;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6jK;

    invoke-virtual {v1, v2}, LX/6jK;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method
