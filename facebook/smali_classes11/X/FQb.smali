.class public LX/FQb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/FQa;

.field private static volatile g:LX/FQb;


# instance fields
.field public final b:LX/0s6;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/8A2;

.field public final e:LX/0Zb;

.field public final f:LX/14z;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2239483
    new-instance v0, LX/FQa;

    invoke-direct {v0, v1, v1}, LX/FQa;-><init>(ZZ)V

    sput-object v0, LX/FQb;->a:LX/FQa;

    return-void
.end method

.method public constructor <init>(LX/0s6;LX/0Or;LX/8A2;LX/0Zb;LX/14z;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0s6;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/8A2;",
            "LX/0Zb;",
            "LX/14z;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239477
    iput-object p1, p0, LX/FQb;->b:LX/0s6;

    .line 2239478
    iput-object p2, p0, LX/FQb;->c:LX/0Or;

    .line 2239479
    iput-object p3, p0, LX/FQb;->d:LX/8A2;

    .line 2239480
    iput-object p4, p0, LX/FQb;->e:LX/0Zb;

    .line 2239481
    iput-object p5, p0, LX/FQb;->f:LX/14z;

    .line 2239482
    return-void
.end method

.method public static a(LX/0QB;)LX/FQb;
    .locals 9

    .prologue
    .line 2239463
    sget-object v0, LX/FQb;->g:LX/FQb;

    if-nez v0, :cond_1

    .line 2239464
    const-class v1, LX/FQb;

    monitor-enter v1

    .line 2239465
    :try_start_0
    sget-object v0, LX/FQb;->g:LX/FQb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2239466
    if-eqz v2, :cond_0

    .line 2239467
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2239468
    new-instance v3, LX/FQb;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/8A2;->a(LX/0QB;)LX/8A2;

    move-result-object v6

    check-cast v6, LX/8A2;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/14y;->a(LX/0QB;)LX/14z;

    move-result-object v8

    check-cast v8, LX/14z;

    invoke-direct/range {v3 .. v8}, LX/FQb;-><init>(LX/0s6;LX/0Or;LX/8A2;LX/0Zb;LX/14z;)V

    .line 2239469
    move-object v0, v3

    .line 2239470
    sput-object v0, LX/FQb;->g:LX/FQb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2239471
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2239472
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2239473
    :cond_1
    sget-object v0, LX/FQb;->g:LX/FQb;

    return-object v0

    .line 2239474
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2239475
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
