.class public final LX/FBm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2209764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/0ad;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2209765
    invoke-static {p1}, LX/FBm;->a(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0831ee

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0831ed

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;LX/0hx;LX/10M;LX/2lP;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2209758
    const-string v0, "logout"

    sget-object v1, LX/CIq;->c:Ljava/lang/String;

    sget-object v2, LX/CIp;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2209759
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2209760
    invoke-virtual {p2, p4}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2209761
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2209762
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p3, p0, v0}, LX/2lP;->a(Landroid/app/Activity;Z)V

    .line 2209763
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0hx;LX/0jh;LX/6G2;)V
    .locals 2

    .prologue
    .line 2209766
    sget-object v0, LX/CIp;->k:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209767
    invoke-static {p0, p2}, LX/FBm;->a(Landroid/content/Context;LX/0jh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2209768
    invoke-virtual {p3, p0}, LX/6G2;->a(Landroid/content/Context;)V

    .line 2209769
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;LX/2lM;Lcom/facebook/katana/urimap/IntentHandlerUtil;LX/0gh;)V
    .locals 6

    .prologue
    .line 2209744
    sget-object v0, LX/FQR;->ONAVO_PROTECT:LX/FQR;

    .line 2209745
    :try_start_0
    iget-object v1, p1, LX/2lM;->d:Landroid/content/pm/PackageManager;

    const-string v2, "com.onavo.spaceship"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2209746
    if-eqz v1, :cond_1

    .line 2209747
    iget-object v2, p1, LX/2lM;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p1, LX/2lM;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2209748
    const/4 v1, 0x1

    .line 2209749
    :goto_0
    move v1, v1

    .line 2209750
    if-nez v1, :cond_0

    .line 2209751
    sget-object v0, LX/0ax;->ev:Ljava/lang/String;

    invoke-virtual {p2, p0, v0}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2209752
    sget-object v0, LX/FQR;->PROMOTION:LX/FQR;

    .line 2209753
    :cond_0
    const-string v1, "bookmarks_menu"

    sget-object v2, LX/CIq;->c:Ljava/lang/String;

    sget-object v3, LX/CIp;->m:Ljava/lang/String;

    const-string v4, "target"

    invoke-virtual {v0}, LX/FQR;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-virtual {p3, v1, v2, v3, v0}, LX/0gh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2209754
    return-void

    .line 2209755
    :catch_0
    move-exception v1

    .line 2209756
    iget-object v2, p1, LX/2lM;->c:LX/03V;

    const-string v3, "Onavo_bookmark_click"

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2209757
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0ad;)Z
    .locals 2

    .prologue
    .line 2209743
    sget-short v0, LX/EgI;->a:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static a(LX/0tK;)Z
    .locals 1

    .prologue
    .line 2209741
    invoke-virtual {p0}, LX/0tK;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0tK;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LX/0jh;)Z
    .locals 2

    .prologue
    .line 2209742
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/features/bugreporter/annotations/BugReportingNotRequired;

    invoke-virtual {p1, v0, v1}, LX/0jh;->a(Ljava/lang/reflect/AnnotatedElement;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
