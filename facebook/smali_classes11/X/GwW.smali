.class public final LX/GwW;
.super LX/4hh;
.source ""


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2407068
    invoke-direct {p0}, LX/4hh;-><init>()V

    return-void
.end method

.method private f()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407078
    new-instance v0, LX/GwO;

    invoke-direct {v0, p0}, LX/GwO;-><init>(LX/GwW;)V

    return-object v0
.end method

.method private g()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407077
    new-instance v0, LX/GwP;

    invoke-direct {v0, p0}, LX/GwP;-><init>(LX/GwW;)V

    return-object v0
.end method

.method private h()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2407076
    new-instance v0, LX/GwQ;

    invoke-direct {v0, p0}, LX/GwQ;-><init>(LX/GwW;)V

    return-object v0
.end method

.method private i()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407075
    new-instance v0, LX/GwR;

    invoke-direct {v0, p0}, LX/GwR;-><init>(LX/GwW;)V

    return-object v0
.end method

.method private j()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407074
    new-instance v0, LX/GwS;

    invoke-direct {v0, p0}, LX/GwS;-><init>(LX/GwW;)V

    return-object v0
.end method

.method private k()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407073
    new-instance v0, LX/GwT;

    invoke-direct {v0, p0}, LX/GwT;-><init>(LX/GwW;)V

    return-object v0
.end method

.method private l()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407072
    new-instance v0, LX/GwU;

    invoke-direct {v0, p0}, LX/GwU;-><init>(LX/GwW;)V

    return-object v0
.end method

.method private m()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407071
    new-instance v0, LX/GwV;

    invoke-direct {v0, p0}, LX/GwV;-><init>(LX/GwW;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2407070
    const-string v2, "com.facebook.platform.extra.IS_USER_MESSAGE_OPTIONAL"

    const-class v4, Ljava/lang/Boolean;

    invoke-direct {p0}, LX/GwW;->f()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.PLACE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->g()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.facebook.platform.extra.FRIENDS"

    invoke-direct {p0}, LX/GwW;->h()LX/4hg;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v3, v1}, LX/4hh;->a(Landroid/content/Intent;Ljava/lang/String;ZLX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.LINK"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->i()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.IMAGE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->j()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.TITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->k()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.SUBTITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->l()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.DESCRIPTION"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->m()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2407069
    const-string v2, "IS_USER_MESSAGE_OPTIONAL"

    const-class v4, Ljava/lang/Boolean;

    invoke-direct {p0}, LX/GwW;->f()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "PLACE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->g()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "FRIENDS"

    invoke-direct {p0}, LX/GwW;->h()LX/4hg;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v3, v1}, LX/4hh;->a(Landroid/os/Bundle;Ljava/lang/String;ZLX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "link"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->i()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "IMAGE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->j()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "TITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->k()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "SUBTITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->l()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "DESCRIPTION"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/GwW;->m()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
