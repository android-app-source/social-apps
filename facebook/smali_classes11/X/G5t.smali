.class public final LX/G5t;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:Landroid/app/Activity;

.field public final synthetic c:LX/5SB;

.field public final synthetic d:LX/BQ1;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/wem/ProfileComposerLauncher;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/ProfileComposerLauncher;Landroid/os/Bundle;Landroid/app/Activity;LX/5SB;LX/BQ1;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2319465
    iput-object p1, p0, LX/G5t;->f:Lcom/facebook/wem/ProfileComposerLauncher;

    iput-object p2, p0, LX/G5t;->a:Landroid/os/Bundle;

    iput-object p3, p0, LX/G5t;->b:Landroid/app/Activity;

    iput-object p4, p0, LX/G5t;->c:LX/5SB;

    iput-object p5, p0, LX/G5t;->d:LX/BQ1;

    iput-object p6, p0, LX/G5t;->e:Ljava/lang/String;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2319455
    if-nez p1, :cond_0

    .line 2319456
    :goto_0
    return-void

    .line 2319457
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/G5t;->f:Lcom/facebook/wem/ProfileComposerLauncher;

    invoke-static {v0, p1}, Lcom/facebook/wem/ProfileComposerLauncher;->a$redex0(Lcom/facebook/wem/ProfileComposerLauncher;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v1

    .line 2319458
    iget-object v0, p0, LX/G5t;->f:Lcom/facebook/wem/ProfileComposerLauncher;

    iget-object v0, v0, Lcom/facebook/wem/ProfileComposerLauncher;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74n;

    invoke-static {v1, v0}, LX/7kv;->a(Landroid/net/Uri;LX/74n;)LX/7kv;

    move-result-object v0

    invoke-virtual {v0}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v5

    .line 2319459
    iget-object v0, p0, LX/G5t;->f:Lcom/facebook/wem/ProfileComposerLauncher;

    iget-object v1, p0, LX/G5t;->a:Landroid/os/Bundle;

    iget-object v2, p0, LX/G5t;->b:Landroid/app/Activity;

    iget-object v3, p0, LX/G5t;->c:LX/5SB;

    iget-object v4, p0, LX/G5t;->d:LX/BQ1;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 2319460
    invoke-static/range {v0 .. v5}, Lcom/facebook/wem/ProfileComposerLauncher;->a$redex0(Lcom/facebook/wem/ProfileComposerLauncher;Landroid/os/Bundle;Landroid/app/Activity;LX/5SB;LX/BQ1;LX/0Px;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2319461
    goto :goto_0

    .line 2319462
    :catch_0
    iget-object v0, p0, LX/G5t;->f:Lcom/facebook/wem/ProfileComposerLauncher;

    iget-object v0, v0, Lcom/facebook/wem/ProfileComposerLauncher;->c:LX/03V;

    const-class v1, Lcom/facebook/wem/ProfileComposerLauncher;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to save bitmap for url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/G5t;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2319463
    iget-object v0, p0, LX/G5t;->f:Lcom/facebook/wem/ProfileComposerLauncher;

    iget-object v0, v0, Lcom/facebook/wem/ProfileComposerLauncher;->c:LX/03V;

    const-class v1, Lcom/facebook/wem/ProfileComposerLauncher;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to download url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/G5t;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319464
    return-void
.end method
