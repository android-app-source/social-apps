.class public LX/Fba;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0RL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RL",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/FbB;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0cA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cA",
            "<",
            "LX/CzO;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RL",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;",
            "LX/0Ot",
            "<+",
            "LX/FbA",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
            "+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fbi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2260522
    new-instance v0, LX/FbZ;

    invoke-direct {v0}, LX/FbZ;-><init>()V

    sput-object v0, LX/Fba;->a:LX/0RL;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/FbC;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/FbC;",
            "LX/0Ot",
            "<",
            "LX/Fbi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbu;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fby;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2260457
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    iput-object v1, p0, LX/Fba;->d:LX/0cA;

    .line 2260458
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/Fba;->e:Ljava/util/Map;

    .line 2260459
    iput-object p1, p0, LX/Fba;->c:LX/0Ot;

    .line 2260460
    iput-object p3, p0, LX/Fba;->f:LX/0Ot;

    .line 2260461
    sget-object v1, LX/D0F;->a:LX/CzO;

    invoke-direct {p0, v1, p6}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260462
    sget-object v1, LX/D0F;->b:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260463
    sget-object v1, LX/D0F;->l:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260464
    sget-object v1, LX/D0F;->g:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260465
    sget-object v1, LX/D0F;->k:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260466
    sget-object v1, LX/D0F;->i:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260467
    sget-object v1, LX/D0F;->m:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260468
    sget-object v1, LX/D0F;->n:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260469
    sget-object v1, LX/D0F;->o:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260470
    sget-object v1, LX/D0F;->p:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260471
    sget-object v1, LX/D0F;->q:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260472
    sget-object v1, LX/D0F;->s:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260473
    sget-object v1, LX/D0F;->u:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260474
    sget-object v1, LX/D0F;->j:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260475
    sget-object v1, LX/D0F;->h:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260476
    sget-object v1, LX/D0F;->c:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260477
    sget-object v1, LX/D0F;->d:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260478
    sget-object v1, LX/D0F;->e:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260479
    sget-object v1, LX/D0F;->f:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260480
    sget-object v1, LX/D0F;->t:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260481
    sget-object v1, LX/D0F;->r:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260482
    sget-object v1, LX/D0F;->v:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260483
    sget-object v1, LX/D0F;->x:LX/CzO;

    move-object/from16 v0, p21

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260484
    sget-object v1, LX/D0F;->y:LX/CzO;

    move-object/from16 v0, p21

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260485
    sget-object v1, LX/D0F;->w:LX/CzO;

    move-object/from16 v0, p22

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260486
    sget-object v1, LX/D0F;->A:LX/CzO;

    invoke-direct {p0, v1, p7}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260487
    sget-object v1, LX/D0F;->B:LX/CzO;

    invoke-direct {p0, v1, p8}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260488
    sget-object v1, LX/D0F;->Y:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260489
    sget-object v1, LX/D0F;->C:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260490
    sget-object v1, LX/D0F;->D:LX/CzO;

    invoke-direct {p0, v1, p11}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260491
    sget-object v1, LX/D0F;->E:LX/CzP;

    move-object/from16 v0, p24

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260492
    sget-object v1, LX/D0F;->F:LX/CzP;

    invoke-direct {p0, v1, p10}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260493
    sget-object v1, LX/D0F;->G:LX/CzO;

    invoke-direct {p0, v1, p9}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260494
    sget-object v1, LX/D0F;->H:LX/CzO;

    invoke-direct {p0, v1, p4}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260495
    sget-object v1, LX/D0F;->I:LX/CzO;

    invoke-direct {p0, v1, p4}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260496
    sget-object v1, LX/D0F;->J:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260497
    sget-object v1, LX/D0F;->K:LX/CzP;

    invoke-direct {p0, v1, p12}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260498
    sget-object v1, LX/D0F;->L:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260499
    sget-object v1, LX/D0F;->T:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260500
    sget-object v1, LX/D0F;->M:LX/CzO;

    move-object/from16 v0, p13

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260501
    sget-object v1, LX/D0F;->N:LX/CzO;

    move-object/from16 v0, p14

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260502
    sget-object v1, LX/D0F;->O:LX/CzP;

    move-object/from16 v0, p15

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260503
    sget-object v1, LX/D0F;->P:LX/CzP;

    move-object/from16 v0, p16

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260504
    sget-object v1, LX/D0F;->R:LX/CzP;

    move-object/from16 v0, p20

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260505
    sget-object v1, LX/D0F;->S:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260506
    sget-object v1, LX/D0F;->U:LX/CzO;

    move-object/from16 v0, p18

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260507
    sget-object v1, LX/D0F;->V:LX/CzO;

    move-object/from16 v0, p19

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260508
    sget-object v1, LX/D0F;->W:LX/CzO;

    move-object/from16 v0, p18

    invoke-direct {p0, v1, v0}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260509
    sget-object v1, LX/D0F;->X:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260510
    sget-object v1, LX/D0F;->z:LX/CzO;

    invoke-direct {p0, v1, p5}, LX/Fba;->a(LX/CzO;LX/0Ot;)V

    .line 2260511
    iget-object v1, p0, LX/Fba;->e:Ljava/util/Map;

    sget-object v2, LX/Fba;->a:LX/0RL;

    move-object/from16 v0, p23

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2260512
    iget-object v1, p0, LX/Fba;->d:LX/0cA;

    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    sget-object v2, LX/Fba;->a:LX/0RL;

    invoke-static {v1, v2}, LX/FbC;->a(LX/0Rf;LX/0RL;)LX/FbB;

    move-result-object v1

    iput-object v1, p0, LX/Fba;->b:LX/FbB;

    .line 2260513
    return-void
.end method

.method public static a(LX/0QB;)LX/Fba;
    .locals 3

    .prologue
    .line 2260514
    const-class v1, LX/Fba;

    monitor-enter v1

    .line 2260515
    :try_start_0
    sget-object v0, LX/Fba;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2260516
    sput-object v2, LX/Fba;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2260517
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2260518
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Fba;->b(LX/0QB;)LX/Fba;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2260519
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fba;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260520
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2260521
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/CzO;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">(",
            "LX/CzO",
            "<TF;>;",
            "LX/0Ot",
            "<+",
            "LX/FbA",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
            "TF;>;>;)V"
        }
    .end annotation

    .prologue
    .line 2260453
    iget-object v0, p0, LX/Fba;->d:LX/0cA;

    invoke-virtual {v0, p1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2260454
    iget-object v0, p0, LX/Fba;->e:Ljava/util/Map;

    iget-object v1, p1, LX/CzO;->d:LX/0RL;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2260455
    return-void
.end method

.method private static b(LX/0QB;)LX/Fba;
    .locals 27

    .prologue
    .line 2260451
    new-instance v2, LX/Fba;

    const/16 v3, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class v4, LX/FbC;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/FbC;

    const/16 v5, 0x3319

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3326

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x330a

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3302

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x330f

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x330e

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3303

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x330b

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3310

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x331a

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x331b

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x331c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x331d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x331e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x3321

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x3325

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x3323

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x331f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x3311

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x3309

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x3329

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x3314

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-direct/range {v2 .. v26}, LX/Fba;-><init>(LX/0Ot;LX/FbC;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2260452
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
            ")",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2260434
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2260435
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2260436
    :goto_0
    return-object v0

    .line 2260437
    :cond_0
    iget-object v0, p0, LX/Fba;->b:LX/FbB;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-static {p1}, LX/Fbf;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/FbB;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/0RL;

    move-result-object v0

    .line 2260438
    iget-object v1, p0, LX/Fba;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    .line 2260439
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/Fby;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DISCOVERY_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2260440
    iget-object v0, p0, LX/Fba;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fbi;

    invoke-virtual {v0, p1}, LX/FbF;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2260441
    :cond_1
    if-nez v0, :cond_2

    .line 2260442
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2260443
    goto :goto_0

    .line 2260444
    :cond_2
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FbA;

    invoke-interface {v0, p1}, LX/FbA;->a(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2260445
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_4

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    .line 2260446
    invoke-virtual {v1}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2260447
    iget-object v2, p0, LX/Fba;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    sget-object v5, LX/3Ql;->MISSING_LOGGING_UNIT_ID:LX/3Ql;

    invoke-virtual {v5}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "Unit of type \'"

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "\' missing logging unit id"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/03V;->a(LX/0VG;)V

    .line 2260448
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2260449
    :cond_4
    move-object v0, v0

    .line 2260450
    goto/16 :goto_0
.end method
