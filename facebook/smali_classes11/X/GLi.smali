.class public final enum LX/GLi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GLi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GLi;

.field public static final enum FACEBOOK_URL_ERROR:LX/GLi;

.field public static final enum FORMAT_ERROR:LX/GLi;

.field public static final enum VALID:LX/GLi;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2343517
    new-instance v0, LX/GLi;

    const-string v1, "FORMAT_ERROR"

    invoke-direct {v0, v1, v2}, LX/GLi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GLi;->FORMAT_ERROR:LX/GLi;

    .line 2343518
    new-instance v0, LX/GLi;

    const-string v1, "FACEBOOK_URL_ERROR"

    invoke-direct {v0, v1, v3}, LX/GLi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GLi;->FACEBOOK_URL_ERROR:LX/GLi;

    .line 2343519
    new-instance v0, LX/GLi;

    const-string v1, "VALID"

    invoke-direct {v0, v1, v4}, LX/GLi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GLi;->VALID:LX/GLi;

    .line 2343520
    const/4 v0, 0x3

    new-array v0, v0, [LX/GLi;

    sget-object v1, LX/GLi;->FORMAT_ERROR:LX/GLi;

    aput-object v1, v0, v2

    sget-object v1, LX/GLi;->FACEBOOK_URL_ERROR:LX/GLi;

    aput-object v1, v0, v3

    sget-object v1, LX/GLi;->VALID:LX/GLi;

    aput-object v1, v0, v4

    sput-object v0, LX/GLi;->$VALUES:[LX/GLi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2343521
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GLi;
    .locals 1

    .prologue
    .line 2343522
    const-class v0, LX/GLi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GLi;

    return-object v0
.end method

.method public static values()[LX/GLi;
    .locals 1

    .prologue
    .line 2343523
    sget-object v0, LX/GLi;->$VALUES:[LX/GLi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GLi;

    return-object v0
.end method
