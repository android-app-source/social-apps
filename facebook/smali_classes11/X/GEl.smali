.class public LX/GEl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/GJk;


# direct methods
.method public constructor <init>(LX/GJk;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332050
    iput-object p1, p0, LX/GEl;->b:LX/GJk;

    .line 2332051
    iput-object p2, p0, LX/GEl;->a:LX/0ad;

    .line 2332052
    return-void
.end method

.method public static a(LX/0QB;)LX/GEl;
    .locals 3

    .prologue
    .line 2332053
    new-instance v2, LX/GEl;

    .line 2332054
    new-instance v1, LX/GJk;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/GJk;-><init>(Landroid/content/res/Resources;)V

    .line 2332055
    move-object v0, v1

    .line 2332056
    check-cast v0, LX/GJk;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/GEl;-><init>(LX/GJk;LX/0ad;)V

    .line 2332057
    move-object v0, v2

    .line 2332058
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332059
    const v0, 0x7f03004d

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    .line 2332060
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v0, 0x0

    .line 2332061
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2332062
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v1, v1

    .line 2332063
    if-eqz v1, :cond_0

    .line 2332064
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v1, v1

    .line 2332065
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/EventSpecModel;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2332066
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/GEl;->a:LX/0ad;

    sget-short v2, LX/8wM;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()LX/GHg;
    .locals 1

    .prologue
    .line 2332067
    iget-object v0, p0, LX/GEl;->b:LX/GJk;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332068
    sget-object v0, LX/8wK;->BOOST_TYPE:LX/8wK;

    return-object v0
.end method
