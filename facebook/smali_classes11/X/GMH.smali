.class public LX/GMH;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public b:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

.field private c:LX/GG6;

.field public d:Ljava/text/NumberFormat;

.field public e:LX/GN0;

.field private final f:LX/GKy;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:LX/GMm;

.field public j:LX/17W;


# direct methods
.method public constructor <init>(LX/GN0;LX/GG6;LX/GKy;LX/GMm;LX/17W;)V
    .locals 0
    .param p1    # LX/GN0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2344147
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2344148
    iput-object p2, p0, LX/GMH;->c:LX/GG6;

    .line 2344149
    iput-object p1, p0, LX/GMH;->e:LX/GN0;

    .line 2344150
    iput-object p3, p0, LX/GMH;->f:LX/GKy;

    .line 2344151
    iput-object p4, p0, LX/GMH;->i:LX/GMm;

    .line 2344152
    iput-object p5, p0, LX/GMH;->j:LX/17W;

    .line 2344153
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2344154
    invoke-super {p0}, LX/GHg;->a()V

    .line 2344155
    const/4 v0, 0x0

    iput-object v0, p0, LX/GMH;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    .line 2344156
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 10

    .prologue
    .line 2344157
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    .line 2344158
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344159
    iput-object p1, p0, LX/GMH;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    .line 2344160
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setColumnsActive(Z)V

    .line 2344161
    iget-boolean v0, p0, LX/GMH;->h:Z

    if-nez v0, :cond_0

    .line 2344162
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setColumnsActive(Z)V

    .line 2344163
    iget-object v0, p0, LX/GMH;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setSpentText(Ljava/lang/String;)V

    .line 2344164
    iget-object v0, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2344165
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344166
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2344167
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->u()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x3

    invoke-virtual {v2, v1, v3}, LX/15i;->j(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v0, v1

    .line 2344168
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/GMH;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setPaidReach(Ljava/lang/String;)V

    .line 2344169
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2344170
    iget-boolean v2, p0, LX/GMH;->h:Z

    if-eqz v2, :cond_1

    .line 2344171
    const v2, 0x7f080aa1

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/GMH;->g:Ljava/lang/String;

    invoke-virtual {p1, v2, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344172
    :cond_1
    const v2, 0x7f080ad6

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2344173
    const-string v6, ""

    .line 2344174
    iget-object v7, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v7}, LX/GMU;->k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 2344175
    iget-object v6, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v6}, LX/GMU;->k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v6

    iget-object v7, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v7}, LX/GMU;->k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v7

    invoke-static {v7}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v7

    invoke-virtual {v7}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v8

    iget-object v7, p0, LX/GMH;->d:Ljava/text/NumberFormat;

    invoke-static {v6, v8, v9, v7}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v6

    .line 2344176
    :cond_2
    move-object v4, v6

    .line 2344177
    invoke-virtual {p1, v2, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344178
    const/4 v2, 0x0

    .line 2344179
    iget-object v4, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    instance-of v4, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v4, :cond_3

    .line 2344180
    iget-object v2, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344181
    iget-object v4, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v4

    .line 2344182
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2344183
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v2, p0, LX/GMH;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v4, v5, v2}, LX/GG6;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 2344184
    const v4, 0x7f080ad4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344185
    const/4 v3, 0x0

    .line 2344186
    const/4 v0, 0x0

    .line 2344187
    iget-object v1, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->FINISHED:LX/GGB;

    if-ne v1, v2, :cond_5

    .line 2344188
    :cond_4
    sget-object v1, LX/GMm;->c:LX/01T;

    move-object v1, v1

    .line 2344189
    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-ne v1, v2, :cond_5

    .line 2344190
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2344191
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2344192
    sget-short v2, LX/GDK;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    :cond_5
    move v0, v0

    .line 2344193
    if-eqz v0, :cond_6

    .line 2344194
    new-instance v0, LX/GMG;

    invoke-direct {v0, p0}, LX/GMG;-><init>(LX/GMH;)V

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setSecondActionButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2344195
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080b71

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setSecondActionButtonText(Ljava/lang/String;)V

    .line 2344196
    invoke-virtual {p1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setSecondActionButtonVisibility(I)V

    .line 2344197
    :cond_6
    iget-object v0, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->FINISHED:LX/GGB;

    if-ne v0, v1, :cond_a

    :cond_7
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2344198
    if-eqz v0, :cond_8

    .line 2344199
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080b72

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setCreateNewButtonText(Ljava/lang/String;)V

    .line 2344200
    iget-object v0, p0, LX/GMH;->e:LX/GN0;

    .line 2344201
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2344202
    iget-object v2, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v2}, LX/GN0;->a(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setCreateNewButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2344203
    invoke-virtual {p1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setCreateNewButtonVisibility(I)V

    .line 2344204
    :cond_8
    iget-object v0, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;->j()Z

    move-result v0

    if-eqz v0, :cond_9

    if-eqz p2, :cond_9

    .line 2344205
    const v0, 0x7f080ab7

    new-instance v1, LX/GMF;

    invoke-direct {v1, p0}, LX/GMF;-><init>(LX/GMH;)V

    invoke-virtual {p2, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(ILandroid/view/View$OnClickListener;)V

    .line 2344206
    :cond_9
    return-void

    :cond_a
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 4

    .prologue
    .line 2344207
    invoke-static {p1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, LX/GMH;->d:Ljava/text/NumberFormat;

    .line 2344208
    iput-object p1, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2344209
    iget-object v0, p0, LX/GMH;->f:LX/GKy;

    invoke-virtual {v0}, LX/GKy;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/GMH;->h:Z

    .line 2344210
    iget-object v0, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GMU;->h(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v0

    iget-object v1, p0, LX/GMH;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2344211
    check-cast v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344212
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2344213
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->D()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v1, v2

    .line 2344214
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    iget-object v1, p0, LX/GMH;->d:Ljava/text/NumberFormat;

    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GMH;->g:Ljava/lang/String;

    .line 2344215
    return-void
.end method
