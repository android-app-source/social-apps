.class public final enum LX/FOZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FOZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FOZ;

.field public static final enum FAN:LX/FOZ;

.field public static final enum FOF:LX/FOZ;

.field public static final enum FRIEND:LX/FOZ;

.field public static final enum ME:LX/FOZ;

.field public static final enum NONE:LX/FOZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2235238
    new-instance v0, LX/FOZ;

    const-string v1, "ME"

    invoke-direct {v0, v1, v2}, LX/FOZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOZ;->ME:LX/FOZ;

    .line 2235239
    new-instance v0, LX/FOZ;

    const-string v1, "FRIEND"

    invoke-direct {v0, v1, v3}, LX/FOZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOZ;->FRIEND:LX/FOZ;

    .line 2235240
    new-instance v0, LX/FOZ;

    const-string v1, "FOF"

    invoke-direct {v0, v1, v4}, LX/FOZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOZ;->FOF:LX/FOZ;

    .line 2235241
    new-instance v0, LX/FOZ;

    const-string v1, "FAN"

    invoke-direct {v0, v1, v5}, LX/FOZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOZ;->FAN:LX/FOZ;

    .line 2235242
    new-instance v0, LX/FOZ;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, LX/FOZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOZ;->NONE:LX/FOZ;

    .line 2235243
    const/4 v0, 0x5

    new-array v0, v0, [LX/FOZ;

    sget-object v1, LX/FOZ;->ME:LX/FOZ;

    aput-object v1, v0, v2

    sget-object v1, LX/FOZ;->FRIEND:LX/FOZ;

    aput-object v1, v0, v3

    sget-object v1, LX/FOZ;->FOF:LX/FOZ;

    aput-object v1, v0, v4

    sget-object v1, LX/FOZ;->FAN:LX/FOZ;

    aput-object v1, v0, v5

    sget-object v1, LX/FOZ;->NONE:LX/FOZ;

    aput-object v1, v0, v6

    sput-object v0, LX/FOZ;->$VALUES:[LX/FOZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2235234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FOZ;
    .locals 1

    .prologue
    .line 2235237
    const-class v0, LX/FOZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FOZ;

    return-object v0
.end method

.method public static values()[LX/FOZ;
    .locals 1

    .prologue
    .line 2235236
    sget-object v0, LX/FOZ;->$VALUES:[LX/FOZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FOZ;

    return-object v0
.end method


# virtual methods
.method public final toMinRelationshipInput()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ContactMatchRelationship;
    .end annotation

    .prologue
    .line 2235235
    invoke-virtual {p0}, LX/FOZ;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
