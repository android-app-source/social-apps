.class public LX/GLH;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

.field public b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field private f:Landroid/text/Spanned;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field public i:LX/GG6;

.field private final j:Landroid/content/Context;

.field private k:LX/GMm;


# direct methods
.method public constructor <init>(LX/GG6;Landroid/content/Context;LX/GMm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2342579
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2342580
    iput-object p1, p0, LX/GLH;->i:LX/GG6;

    .line 2342581
    iput-object p2, p0, LX/GLH;->j:Landroid/content/Context;

    .line 2342582
    iput-object p3, p0, LX/GLH;->k:LX/GMm;

    .line 2342583
    return-void
.end method

.method public static a(LX/0QB;)LX/GLH;
    .locals 1

    .prologue
    .line 2342578
    invoke-static {p0}, LX/GLH;->b(LX/0QB;)LX/GLH;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/text/Spanned;
    .locals 8

    .prologue
    .line 2342567
    iget-object v0, p0, LX/GLH;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2342568
    const v1, 0x7f080a5b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 2342569
    const v2, 0x7f080a5c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2342570
    const v3, 0x7f0a0124

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2342571
    new-instance v4, LX/47x;

    invoke-direct {v4, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2342572
    invoke-virtual {v4, p1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2342573
    new-instance v5, LX/47x;

    invoke-direct {v5, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2342574
    invoke-virtual {v5, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2342575
    const-string v0, "[[budget_learn_more_link]]"

    iget-object v1, p0, LX/GLH;->k:LX/GMm;

    const-string v6, "https://m.facebook.com/business/help/190490051321426"

    iget-object v7, p0, LX/GLH;->j:Landroid/content/Context;

    invoke-virtual {v1, v6, v3, v7}, LX/GMm;->a(Ljava/lang/String;ILandroid/content/Context;)Landroid/text/style/ClickableSpan;

    move-result-object v1

    const/16 v3, 0x21

    invoke-virtual {v5, v0, v2, v1, v3}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2342576
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, " "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {v5}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    iput-object v0, p0, LX/GLH;->f:Landroid/text/Spanned;

    .line 2342577
    iget-object v0, p0, LX/GLH;->f:Landroid/text/Spanned;

    return-object v0
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342548
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2342549
    iput-object p1, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    .line 2342550
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080a5a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GLH;->g:Ljava/lang/String;

    .line 2342551
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080a5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GLH;->h:Ljava/lang/String;

    .line 2342552
    iget-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    iget-object v1, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342553
    iget-object p1, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, p1

    .line 2342554
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->setScheduleOptions(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;)V

    .line 2342555
    iput-object p2, p0, LX/GLH;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2342556
    iget-object v0, p0, LX/GLH;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/GLH;->a(Ljava/lang/String;)Landroid/text/Spanned;

    .line 2342557
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/GLH;->d:Ljava/lang/Integer;

    .line 2342558
    iget-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    iget-object v1, p0, LX/GLH;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->d(I)V

    .line 2342559
    invoke-static {p0}, LX/GLH;->e(LX/GLH;)V

    .line 2342560
    iget-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    new-instance v1, LX/GLC;

    invoke-direct {v1, p0}, LX/GLC;-><init>(LX/GLH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->setOnCheckChangedListener(LX/Bc9;)V

    .line 2342561
    iget-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    new-instance v1, LX/GLE;

    invoke-direct {v1, p0}, LX/GLE;-><init>(LX/GLH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->setDateOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342562
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2342563
    invoke-static {p0}, LX/GLH;->c(LX/GLH;)V

    .line 2342564
    :cond_0
    invoke-direct {p0}, LX/GLH;->d()V

    .line 2342565
    invoke-direct {p0}, LX/GLH;->b()V

    .line 2342566
    return-void
.end method

.method public static a$redex0(LX/GLH;II)V
    .locals 3

    .prologue
    .line 2342541
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/GLH;->d:Ljava/lang/Integer;

    .line 2342542
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GLH;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2342543
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2342544
    new-instance v1, LX/GFM;

    iget-object v2, p0, LX/GLH;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, LX/GFM;-><init>(I)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2342545
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/GLH;->c:Ljava/lang/Integer;

    .line 2342546
    invoke-static {p0}, LX/GLH;->e(LX/GLH;)V

    .line 2342547
    return-void
.end method

.method public static b(LX/0QB;)LX/GLH;
    .locals 4

    .prologue
    .line 2342539
    new-instance v3, LX/GLH;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v0

    check-cast v0, LX/GG6;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v2

    check-cast v2, LX/GMm;

    invoke-direct {v3, v0, v1, v2}, LX/GLH;-><init>(LX/GG6;Landroid/content/Context;LX/GMm;)V

    .line 2342540
    return-object v3
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2342533
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342534
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2342535
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->LIFETIME_BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->t()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2342536
    :cond_0
    iget-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c()V

    .line 2342537
    iget-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->b()V

    .line 2342538
    :cond_1
    return-void
.end method

.method public static c(LX/GLH;)V
    .locals 4

    .prologue
    .line 2342518
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2342519
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2342520
    sget-object v1, LX/GG8;->UNEDITED_DATA:LX/GG8;

    iget-object v2, p0, LX/GLH;->i:LX/GG6;

    iget-object v3, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2, v3}, LX/GG6;->c(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2342521
    :cond_0
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342522
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2342523
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->LIFETIME_BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-eq v0, v1, :cond_1

    .line 2342524
    :goto_0
    return-void

    .line 2342525
    :cond_1
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2342526
    iget-object v1, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2342527
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_2

    iget-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080b3f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 2342528
    :goto_1
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v1

    .line 2342529
    sget-object v3, LX/GG8;->INVALID_BUDGET:LX/GG8;

    if-nez v0, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v2, v3, v1}, LX/GCE;->a(LX/GG8;Z)V

    .line 2342530
    iget-object v1, p0, LX/GLH;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2342531
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2342532
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2342475
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2342476
    new-instance v1, LX/GLF;

    invoke-direct {v1, p0}, LX/GLF;-><init>(LX/GLH;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2342477
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2342478
    new-instance v1, LX/GLG;

    invoke-direct {v1, p0}, LX/GLG;-><init>(LX/GLH;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2342479
    return-void
.end method

.method public static e(LX/GLH;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 2342502
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    .line 2342503
    if-nez v4, :cond_0

    .line 2342504
    iget-object v0, p0, LX/GLH;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2342505
    :goto_0
    return-void

    .line 2342506
    :cond_0
    invoke-static {v4}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2342507
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2342508
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2342509
    :goto_1
    int-to-long v6, v0

    div-long/2addr v2, v6

    .line 2342510
    :cond_1
    iget-object v0, p0, LX/GLH;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v5, LX/GG5;->CONTINUOUS:LX/GG5;

    invoke-virtual {v5}, LX/GG5;->getDuration()I

    move-result v5

    if-ne v0, v5, :cond_3

    .line 2342511
    iget-object v0, p0, LX/GLH;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GLH;->h:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v4

    iget-object v5, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v5}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v5

    invoke-static {v4, v2, v3, v5}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2342512
    :cond_2
    iget-object v0, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    goto :goto_1

    .line 2342513
    :cond_3
    iget-object v0, p0, LX/GLH;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 2342514
    iget-object v0, p0, LX/GLH;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f080a5d

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v4

    iget-object v5, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v5}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v5

    invoke-static {v4, v2, v3, v5}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2342515
    :cond_4
    iget-object v0, p0, LX/GLH;->g:Ljava/lang/String;

    iget-object v1, p0, LX/GLH;->d:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v4

    iget-object v5, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v5}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v5

    invoke-static {v4, v2, v3, v5}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2342516
    iget-object v1, p0, LX/GLH;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-direct {p0, v0}, LX/GLH;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2342517
    iget-object v0, p0, LX/GLH;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterMovementMethod(Landroid/text/method/MovementMethod;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2342499
    invoke-super {p0}, LX/GHg;->a()V

    .line 2342500
    const/4 v0, 0x0

    iput-object v0, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    .line 2342501
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2342493
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2342494
    const-string v0, "adinterfaces_schedule"

    iget-object v1, p0, LX/GLH;->c:Ljava/lang/Integer;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2342495
    const-string v0, "adinterfaces_schedule_date"

    iget-object v1, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    .line 2342496
    iget-object p0, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    move-object v1, p0

    .line 2342497
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2342498
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342492
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-direct {p0, p1, p2}, LX/GLH;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2342489
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342490
    iput-object p1, p0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342491
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342480
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2342481
    if-nez p1, :cond_1

    .line 2342482
    :cond_0
    :goto_0
    return-void

    .line 2342483
    :cond_1
    const-string v0, "adinterfaces_schedule_date"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2342484
    if-eqz v0, :cond_2

    .line 2342485
    iget-object v1, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->setDate(Ljava/lang/Long;)V

    .line 2342486
    :cond_2
    const-string v0, "adinterfaces_schedule"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2342487
    if-eqz v0, :cond_0

    .line 2342488
    iget-object v1, p0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c(I)V

    goto :goto_0
.end method
