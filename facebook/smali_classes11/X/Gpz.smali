.class public LX/Gpz;
.super LX/Cod;
.source ""

# interfaces
.implements LX/Gpv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpR;",
        ">;",
        "LX/Gpv;"
    }
.end annotation


# instance fields
.field public a:LX/Go0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Landroid/widget/FrameLayout;

.field public d:LX/GoE;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GnF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2396046
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396047
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2396048
    iput-object v0, p0, LX/Gpz;->e:LX/0Ot;

    .line 2396049
    const-class v0, LX/Gpz;

    invoke-static {v0, p0}, LX/Gpz;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396050
    const v0, 0x7f0d188a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Gpz;->c:Landroid/widget/FrameLayout;

    .line 2396051
    const v0, 0x7f0d188b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gpz;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2396052
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/Gpz;

    invoke-static {v2}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v1

    check-cast v1, LX/Go0;

    const/16 p0, 0x252f

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/Gpz;->a:LX/Go0;

    iput-object v2, p1, LX/Gpz;->e:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2396053
    iget-object v0, p0, LX/Gpz;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2396054
    iget-object v0, p0, LX/Gpz;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2396055
    return-void
.end method

.method public final a(LX/CHX;)V
    .locals 2

    .prologue
    .line 2396056
    iget-object v0, p0, LX/Gpz;->b:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Gpy;

    invoke-direct {v1, p0, p1}, LX/Gpy;-><init>(LX/Gpz;LX/CHX;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396057
    return-void
.end method
