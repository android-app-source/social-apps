.class public LX/GEr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GJz;


# direct methods
.method public constructor <init>(LX/GJz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332124
    iput-object p1, p0, LX/GEr;->a:LX/GJz;

    .line 2332125
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332128
    const v0, 0x7f030068

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 1

    .prologue
    .line 2332129
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2332130
    :cond_0
    const/4 v0, 0x0

    .line 2332131
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332127
    iget-object v0, p0, LX/GEr;->a:LX/GJz;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332126
    sget-object v0, LX/8wK;->INSIGHTS_ENGAGEMENT:LX/8wK;

    return-object v0
.end method
