.class public LX/Fiu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275943
    return-void
.end method

.method public static a(LX/0QB;)LX/Fiu;
    .locals 3

    .prologue
    .line 2275944
    const-class v1, LX/Fiu;

    monitor-enter v1

    .line 2275945
    :try_start_0
    sget-object v0, LX/Fiu;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275946
    sput-object v2, LX/Fiu;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275947
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275948
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2275949
    new-instance v0, LX/Fiu;

    invoke-direct {v0}, LX/Fiu;-><init>()V

    .line 2275950
    move-object v0, v0

    .line 2275951
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275952
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fiu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275953
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
