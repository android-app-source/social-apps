.class public LX/FVb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public a:LX/1ZF;

.field public b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Ei6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ei6",
            "<",
            "Lcom/facebook/saved/controller/SavedDashboardTitleBarController$SavedSectionsListener;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/content/res/Resources;

.field public e:LX/FWL;

.field public f:LX/79a;

.field public final g:LX/16H;

.field public final h:LX/2dD;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/Ei6;LX/FWL;LX/79a;LX/16H;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250915
    iput-object p1, p0, LX/FVb;->d:Landroid/content/res/Resources;

    .line 2250916
    iput-object p3, p0, LX/FVb;->e:LX/FWL;

    .line 2250917
    iput-object p4, p0, LX/FVb;->f:LX/79a;

    .line 2250918
    iput-object p5, p0, LX/FVb;->g:LX/16H;

    .line 2250919
    iput-object p2, p0, LX/FVb;->c:LX/Ei6;

    .line 2250920
    new-instance v0, LX/FVY;

    invoke-direct {v0, p0}, LX/FVY;-><init>(LX/FVb;)V

    iput-object v0, p0, LX/FVb;->h:LX/2dD;

    .line 2250921
    return-void
.end method

.method public static a(LX/0QB;)LX/FVb;
    .locals 9

    .prologue
    .line 2250934
    const-class v1, LX/FVb;

    monitor-enter v1

    .line 2250935
    :try_start_0
    sget-object v0, LX/FVb;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2250936
    sput-object v2, LX/FVb;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2250937
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250938
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2250939
    new-instance v3, LX/FVb;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    .line 2250940
    new-instance v5, LX/Ei6;

    invoke-direct {v5}, LX/Ei6;-><init>()V

    .line 2250941
    move-object v5, v5

    .line 2250942
    move-object v5, v5

    .line 2250943
    check-cast v5, LX/Ei6;

    invoke-static {v0}, LX/FWL;->a(LX/0QB;)LX/FWL;

    move-result-object v6

    check-cast v6, LX/FWL;

    invoke-static {v0}, LX/79a;->a(LX/0QB;)LX/79a;

    move-result-object v7

    check-cast v7, LX/79a;

    invoke-static {v0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v8

    check-cast v8, LX/16H;

    invoke-direct/range {v3 .. v8}, LX/FVb;-><init>(Landroid/content/res/Resources;LX/Ei6;LX/FWL;LX/79a;LX/16H;)V

    .line 2250944
    move-object v0, v3

    .line 2250945
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2250946
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FVb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2250947
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2250948
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/FVb;LX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2250949
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250950
    iget-object v1, p0, LX/FVb;->f:LX/79a;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2250951
    iget-object p1, v0, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v0, p1

    .line 2250952
    invoke-virtual {v1, v0}, LX/79a;->a(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)LX/0am;

    move-result-object v0

    sget-object v1, LX/79a;->a:LX/79Z;

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Z;

    iget v0, v0, LX/79Z;->c:I

    .line 2250953
    :goto_0
    iget-object v1, p0, LX/FVb;->a:LX/1ZF;

    invoke-interface {v1, v0}, LX/1ZF;->x_(I)V

    .line 2250954
    return-void

    .line 2250955
    :cond_0
    sget-object v0, LX/79a;->a:LX/79Z;

    iget v0, v0, LX/79Z;->c:I

    goto :goto_0
.end method

.method public static a$redex0(LX/FVb;LX/79Y;)V
    .locals 14

    .prologue
    .line 2250922
    iget-object v0, p0, LX/FVb;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250923
    iget-object v1, p0, LX/FVb;->g:LX/16H;

    iget-object v0, p0, LX/FVb;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2250924
    iget-object v2, v0, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v0, v2

    .line 2250925
    iget-object v2, p1, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v2, v2

    .line 2250926
    const/4 v13, 0x0

    .line 2250927
    iget-object v11, v1, LX/16H;->b:LX/0gh;

    const-string v12, "saved_dashboard"

    const-string v3, "action_name"

    const-string v4, "saved_dashboard_filter_section_clicked"

    const-string v5, "current_section_type"

    const-string v7, "next_section_type"

    const-string v9, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v6, v0

    move-object v8, v2

    invoke-static/range {v3 .. v10}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    invoke-virtual {v11, v12, v13, v13, v3}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2250928
    :cond_0
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/FVb;->b:LX/0am;

    .line 2250929
    iget-object v0, p0, LX/FVb;->c:LX/Ei6;

    .line 2250930
    iget-object v1, v0, LX/Ei6;->a:Ljava/util/Set;

    move-object v0, v1

    .line 2250931
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVU;

    .line 2250932
    invoke-virtual {v0, p1}, LX/FVU;->a(LX/79Y;)V

    goto :goto_0

    .line 2250933
    :cond_1
    return-void
.end method
