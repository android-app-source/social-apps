.class public LX/Fry;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/BPs;

.field public final c:LX/0ad;

.field public d:Z


# direct methods
.method public constructor <init>(LX/0tX;LX/BPs;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296473
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fry;->d:Z

    .line 2296474
    iput-object p1, p0, LX/Fry;->a:LX/0tX;

    .line 2296475
    iput-object p2, p0, LX/Fry;->b:LX/BPs;

    .line 2296476
    iput-object p3, p0, LX/Fry;->c:LX/0ad;

    .line 2296477
    return-void
.end method


# virtual methods
.method public final a(LX/BP0;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2296478
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2296479
    iget-object v0, p0, LX/Fry;->c:LX/0ad;

    sget-short v3, LX/0wf;->aW:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2296480
    invoke-virtual {p1}, LX/5SB;->d()Z

    move-result v2

    .line 2296481
    :cond_0
    :goto_0
    move v0, v2

    .line 2296482
    if-eqz v0, :cond_1

    .line 2296483
    iget-wide v2, p1, LX/5SB;->b:J

    move-wide v0, v2

    .line 2296484
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 2296485
    iget-object v1, p1, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v1, v1

    .line 2296486
    iget-object v2, p0, LX/Fry;->b:LX/BPs;

    new-instance v3, LX/BPr;

    invoke-direct {v3, v1, v0}, LX/BPr;-><init>(Landroid/os/ParcelUuid;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2296487
    new-instance v1, LX/5zM;

    invoke-direct {v1}, LX/5zM;-><init>()V

    move-object v1, v1

    .line 2296488
    const-string v2, "input"

    .line 2296489
    new-instance v3, LX/4K7;

    invoke-direct {v3}, LX/4K7;-><init>()V

    .line 2296490
    const-string p1, "profile_id"

    invoke-virtual {v3, p1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2296491
    move-object v3, v3

    .line 2296492
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/5zM;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2296493
    iget-object v2, p0, LX/Fry;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2296494
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fry;->d:Z

    .line 2296495
    :cond_1
    return-void

    .line 2296496
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p2, v0, :cond_3

    invoke-virtual {p1}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v2

    .line 2296497
    :goto_1
    iget-boolean v3, p0, LX/Fry;->d:Z

    if-nez v3, :cond_4

    if-nez v0, :cond_0

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 2296498
    goto :goto_1
.end method
