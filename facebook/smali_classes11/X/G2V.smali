.class public LX/G2V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/G2V;


# instance fields
.field public final a:LX/G2T;

.field public final b:LX/0ad;

.field public final c:LX/0rq;

.field public final d:LX/0se;

.field public final e:LX/0sX;

.field public final f:LX/G2L;


# direct methods
.method public constructor <init>(LX/G2T;LX/0ad;LX/0rq;LX/0se;LX/0sX;LX/G2L;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314111
    iput-object p1, p0, LX/G2V;->a:LX/G2T;

    .line 2314112
    iput-object p2, p0, LX/G2V;->b:LX/0ad;

    .line 2314113
    iput-object p3, p0, LX/G2V;->c:LX/0rq;

    .line 2314114
    iput-object p4, p0, LX/G2V;->d:LX/0se;

    .line 2314115
    iput-object p5, p0, LX/G2V;->e:LX/0sX;

    .line 2314116
    iput-object p6, p0, LX/G2V;->f:LX/G2L;

    .line 2314117
    return-void
.end method

.method public static a(LX/0QB;)LX/G2V;
    .locals 10

    .prologue
    .line 2314118
    sget-object v0, LX/G2V;->g:LX/G2V;

    if-nez v0, :cond_1

    .line 2314119
    const-class v1, LX/G2V;

    monitor-enter v1

    .line 2314120
    :try_start_0
    sget-object v0, LX/G2V;->g:LX/G2V;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2314121
    if-eqz v2, :cond_0

    .line 2314122
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2314123
    new-instance v3, LX/G2V;

    invoke-static {v0}, LX/G2T;->a(LX/0QB;)LX/G2T;

    move-result-object v4

    check-cast v4, LX/G2T;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v6

    check-cast v6, LX/0rq;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v7

    check-cast v7, LX/0se;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v8

    check-cast v8, LX/0sX;

    invoke-static {v0}, LX/G2L;->a(LX/0QB;)LX/G2L;

    move-result-object v9

    check-cast v9, LX/G2L;

    invoke-direct/range {v3 .. v9}, LX/G2V;-><init>(LX/G2T;LX/0ad;LX/0rq;LX/0se;LX/0sX;LX/G2L;)V

    .line 2314124
    move-object v0, v3

    .line 2314125
    sput-object v0, LX/G2V;->g:LX/G2V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314126
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2314127
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2314128
    :cond_1
    sget-object v0, LX/G2V;->g:LX/G2V;

    return-object v0

    .line 2314129
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2314130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
