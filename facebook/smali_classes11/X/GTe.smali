.class public LX/GTe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/GTe;


# instance fields
.field private final b:LX/0aG;

.field public final c:Landroid/content/Context;

.field public final d:LX/0aU;

.field private final e:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2355744
    const-class v0, LX/GTe;

    sput-object v0, LX/GTe;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;Landroid/content/Context;LX/0aU;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2355738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2355739
    iput-object p1, p0, LX/GTe;->b:LX/0aG;

    .line 2355740
    iput-object p2, p0, LX/GTe;->c:Landroid/content/Context;

    .line 2355741
    iput-object p3, p0, LX/GTe;->d:LX/0aU;

    .line 2355742
    iput-object p4, p0, LX/GTe;->e:Ljava/util/concurrent/ExecutorService;

    .line 2355743
    return-void
.end method

.method public static a(LX/0QB;)LX/GTe;
    .locals 7

    .prologue
    .line 2355725
    sget-object v0, LX/GTe;->f:LX/GTe;

    if-nez v0, :cond_1

    .line 2355726
    const-class v1, LX/GTe;

    monitor-enter v1

    .line 2355727
    :try_start_0
    sget-object v0, LX/GTe;->f:LX/GTe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2355728
    if-eqz v2, :cond_0

    .line 2355729
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2355730
    new-instance p0, LX/GTe;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v5

    check-cast v5, LX/0aU;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4, v5, v6}, LX/GTe;-><init>(LX/0aG;Landroid/content/Context;LX/0aU;Ljava/util/concurrent/ExecutorService;)V

    .line 2355731
    move-object v0, p0

    .line 2355732
    sput-object v0, LX/GTe;->f:LX/GTe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2355733
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2355734
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2355735
    :cond_1
    sget-object v0, LX/GTe;->f:LX/GTe;

    return-object v0

    .line 2355736
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2355737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/GTe;LX/GTf;LX/8DK;LX/0P1;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GTf;",
            "LX/8DK;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2355720
    new-instance v0, Lcom/facebook/nux/status/UpdateNuxStatusParams;

    const-string v1, "location_upsell_wizard"

    const-string v2, "1631"

    invoke-virtual {p1}, LX/GTf;->name()Ljava/lang/String;

    move-result-object v3

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/nux/status/UpdateNuxStatusParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8DK;LX/0P1;)V

    .line 2355721
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2355722
    const-string v2, "updateNuxStatusParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2355723
    iget-object v0, p0, LX/GTe;->b:LX/0aG;

    const-string v2, "update_nux_status"

    const v3, -0x724b718d

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2355724
    new-instance v1, LX/GTd;

    invoke-direct {v1, p0}, LX/GTd;-><init>(LX/GTe;)V

    iget-object v2, p0, LX/GTe;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
