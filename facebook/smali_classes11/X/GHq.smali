.class public final LX/GHq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/GHx;


# direct methods
.method public constructor <init>(LX/GHx;)V
    .locals 0

    .prologue
    .line 2335294
    iput-object p1, p0, LX/GHq;->a:LX/GHx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 11

    .prologue
    .line 2335295
    if-nez p2, :cond_0

    .line 2335296
    iget-object v0, p0, LX/GHq;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->u:LX/GHw;

    sget-object v1, LX/GHw;->UNCHANGED:LX/GHw;

    invoke-virtual {v0, v1}, LX/GHw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2335297
    :cond_0
    :goto_0
    return-void

    .line 2335298
    :cond_1
    iget-object v0, p0, LX/GHq;->a:LX/GHx;

    .line 2335299
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2335300
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2335301
    iget-object v1, p0, LX/GHq;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v7, 0x0

    .line 2335302
    const-string v4, "change_flow_option"

    invoke-static {v1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v5, "edit"

    :goto_1
    const-string v6, "ad_text"

    const/4 v10, 0x1

    move-object v2, v0

    move-object v3, v1

    move-object v8, v7

    move-object v9, v7

    invoke-static/range {v2 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2335303
    iget-object v0, p0, LX/GHq;->a:LX/GHx;

    sget-object v1, LX/GHw;->UNCHANGED:LX/GHw;

    .line 2335304
    iput-object v1, v0, LX/GHx;->u:LX/GHw;

    .line 2335305
    goto :goto_0

    .line 2335306
    :cond_2
    const-string v5, "create"

    goto :goto_1
.end method
