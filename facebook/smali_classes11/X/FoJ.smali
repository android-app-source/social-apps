.class public LX/FoJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2289304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;)I
    .locals 1

    .prologue
    .line 2289305
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2289306
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2289307
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;)Z
    .locals 2

    .prologue
    .line 2289308
    invoke-static {p0}, LX/FoJ;->a(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;)I

    move-result v0

    const v1, -0x4e6785e3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
