.class public final enum LX/GcD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GcD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GcD;

.field public static final enum ALL_LARGE:LX/GcD;

.field public static final enum ALL_SMALL:LX/GcD;

.field public static final enum SMALL_AND_SCROLL:LX/GcD;

.field public static final enum SMALL_LIST:LX/GcD;

.field public static final enum UNSET:LX/GcD;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2371317
    new-instance v0, LX/GcD;

    const-string v1, "ALL_LARGE"

    invoke-direct {v0, v1, v2}, LX/GcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcD;->ALL_LARGE:LX/GcD;

    .line 2371318
    new-instance v0, LX/GcD;

    const-string v1, "SMALL_LIST"

    invoke-direct {v0, v1, v3}, LX/GcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcD;->SMALL_LIST:LX/GcD;

    .line 2371319
    new-instance v0, LX/GcD;

    const-string v1, "ALL_SMALL"

    invoke-direct {v0, v1, v4}, LX/GcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcD;->ALL_SMALL:LX/GcD;

    .line 2371320
    new-instance v0, LX/GcD;

    const-string v1, "SMALL_AND_SCROLL"

    invoke-direct {v0, v1, v5}, LX/GcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcD;->SMALL_AND_SCROLL:LX/GcD;

    .line 2371321
    new-instance v0, LX/GcD;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v6}, LX/GcD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcD;->UNSET:LX/GcD;

    .line 2371322
    const/4 v0, 0x5

    new-array v0, v0, [LX/GcD;

    sget-object v1, LX/GcD;->ALL_LARGE:LX/GcD;

    aput-object v1, v0, v2

    sget-object v1, LX/GcD;->SMALL_LIST:LX/GcD;

    aput-object v1, v0, v3

    sget-object v1, LX/GcD;->ALL_SMALL:LX/GcD;

    aput-object v1, v0, v4

    sget-object v1, LX/GcD;->SMALL_AND_SCROLL:LX/GcD;

    aput-object v1, v0, v5

    sget-object v1, LX/GcD;->UNSET:LX/GcD;

    aput-object v1, v0, v6

    sput-object v0, LX/GcD;->$VALUES:[LX/GcD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2371314
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GcD;
    .locals 1

    .prologue
    .line 2371316
    const-class v0, LX/GcD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GcD;

    return-object v0
.end method

.method public static values()[LX/GcD;
    .locals 1

    .prologue
    .line 2371315
    sget-object v0, LX/GcD;->$VALUES:[LX/GcD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GcD;

    return-object v0
.end method
