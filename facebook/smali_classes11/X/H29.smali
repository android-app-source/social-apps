.class public LX/H29;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Lcom/facebook/nearby/model/MapTile;

.field public final b:J

.field public final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/model/MapTile;JLjava/util/Set;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/model/MapTile;",
            "J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2416602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416603
    iput-object p1, p0, LX/H29;->a:Lcom/facebook/nearby/model/MapTile;

    .line 2416604
    iput-wide p2, p0, LX/H29;->b:J

    .line 2416605
    invoke-static {p4}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/H29;->c:LX/0Rf;

    .line 2416606
    invoke-static {p5}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/H29;->d:LX/0Rf;

    .line 2416607
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/nearby/model/MapTile;
    .locals 1

    .prologue
    .line 2416608
    iget-object v0, p0, LX/H29;->a:Lcom/facebook/nearby/model/MapTile;

    return-object v0
.end method
