.class public final LX/H6f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/H83;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/H6g;


# direct methods
.method public constructor <init>(LX/H6g;)V
    .locals 0

    .prologue
    .line 2427100
    iput-object p1, p0, LX/H6f;->a:LX/H6g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2427101
    check-cast p1, LX/H83;

    check-cast p2, LX/H83;

    .line 2427102
    iget-object v0, p0, LX/H6f;->a:LX/H6g;

    iget-object v0, v0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-boolean v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->m:Z

    if-eqz v0, :cond_0

    .line 2427103
    invoke-virtual {p1}, LX/H83;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2}, LX/H83;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    .line 2427104
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, LX/H83;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, LX/H83;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    goto :goto_0
.end method
