.class public LX/Fam;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

.field private final b:LX/Fal;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1bf;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;I)V
    .locals 1
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259156
    iput-object p1, p0, LX/Fam;->a:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    .line 2259157
    const/16 v0, 0xf0

    if-ge p2, v0, :cond_0

    .line 2259158
    sget-object v0, LX/Fal;->MDPI:LX/Fal;

    .line 2259159
    :goto_0
    move-object v0, v0

    .line 2259160
    iput-object v0, p0, LX/Fam;->b:LX/Fal;

    .line 2259161
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fam;->d:Z

    .line 2259162
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Fam;->c:Ljava/util/Map;

    .line 2259163
    return-void

    .line 2259164
    :cond_0
    const/16 v0, 0x140

    if-ge p2, v0, :cond_1

    .line 2259165
    sget-object v0, LX/Fal;->HDPI:LX/Fal;

    goto :goto_0

    .line 2259166
    :cond_1
    const/16 v0, 0x1e0

    if-ge p2, v0, :cond_2

    .line 2259167
    sget-object v0, LX/Fal;->XHDPI:LX/Fal;

    goto :goto_0

    .line 2259168
    :cond_2
    sget-object v0, LX/Fal;->XXHDPI:LX/Fal;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15i;I)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImageUri"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2259149
    sget-object v0, LX/Fak;->a:[I

    iget-object v1, p0, LX/Fam;->b:LX/Fal;

    invoke-virtual {v1}, LX/Fal;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2259150
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2259151
    :pswitch_0
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2259152
    :pswitch_1
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2259153
    :pswitch_2
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2259154
    :pswitch_3
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2259111
    sget-object v0, LX/Fak;->a:[I

    iget-object v1, p0, LX/Fam;->b:LX/Fal;

    invoke-virtual {v1}, LX/Fal;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2259112
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2259113
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2259114
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2259115
    :pswitch_2
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2259116
    :pswitch_3
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)V
    .locals 6

    .prologue
    .line 2259129
    if-nez p1, :cond_0

    .line 2259130
    :goto_0
    return-void

    .line 2259131
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    .line 2259132
    if-nez v0, :cond_2

    .line 2259133
    :goto_1
    iget-object v0, p0, LX/Fam;->a:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    iget-object v1, p0, LX/Fam;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 2259134
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1bf;

    .line 2259135
    iget-object v4, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->d:Ljava/util/Map;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259136
    iget-object v4, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->b:LX/1HI;

    sget-object v5, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v2, v5}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v4

    .line 2259137
    new-instance v5, LX/FaO;

    invoke-direct {v5, v0, v2}, LX/FaO;-><init>(Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;LX/1bf;)V

    .line 2259138
    sget-object v2, LX/1fo;->a:LX/1fo;

    move-object v2, v2

    .line 2259139
    invoke-interface {v4, v5, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    .line 2259140
    :cond_1
    goto :goto_0

    .line 2259141
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2259142
    iget-object v4, p0, LX/Fam;->c:Ljava/util/Map;

    invoke-virtual {p0, v3, v2}, LX/Fam;->a(LX/15i;I)Ljava/lang/String;

    move-result-object v5

    .line 2259143
    invoke-virtual {p0, v3, v2}, LX/Fam;->a(LX/15i;I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object p1

    move-object v2, p1

    .line 2259144
    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 2259145
    :cond_3
    iget-object v1, p0, LX/Fam;->c:Ljava/util/Map;

    invoke-virtual {p0, v0}, LX/Fam;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)Ljava/lang/String;

    move-result-object v2

    .line 2259146
    invoke-virtual {p0, v0}, LX/Fam;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v3

    move-object v0, v3

    .line 2259147
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259148
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fam;->d:Z

    goto :goto_1
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2259117
    iget-boolean v0, p0, LX/Fam;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 2259118
    :goto_0
    return v0

    .line 2259119
    :cond_0
    iget-object v0, p0, LX/Fam;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    .line 2259120
    iget-object v3, p0, LX/Fam;->a:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    .line 2259121
    if-eqz v0, :cond_3

    .line 2259122
    if-eqz v0, :cond_2

    iget-object v4, v3, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->b:LX/1HI;

    .line 2259123
    iget-object v5, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v5, v5

    .line 2259124
    invoke-virtual {v4, v5}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_2
    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2259125
    if-nez v4, :cond_3

    iget-object v4, v3, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_3
    const/4 v4, 0x1

    :goto_2
    move v0, v4

    .line 2259126
    if-nez v0, :cond_1

    move v0, v1

    .line 2259127
    goto :goto_0

    .line 2259128
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    goto :goto_1
.end method
