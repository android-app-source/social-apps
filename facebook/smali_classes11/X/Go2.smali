.class public LX/Go2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private a:LX/Go0;

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(LX/Go0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2394008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2394009
    iput-object p1, p0, LX/Go2;->a:LX/Go0;

    .line 2394010
    return-void
.end method

.method public static a(LX/0QB;)LX/Go2;
    .locals 4

    .prologue
    .line 2393991
    const-class v1, LX/Go2;

    monitor-enter v1

    .line 2393992
    :try_start_0
    sget-object v0, LX/Go2;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2393993
    sput-object v2, LX/Go2;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2393994
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393995
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2393996
    new-instance p0, LX/Go2;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v3

    check-cast v3, LX/Go0;

    invoke-direct {p0, v3}, LX/Go2;-><init>(LX/Go0;)V

    .line 2393997
    move-object v0, p0

    .line 2393998
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2393999
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Go2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2394000
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2394001
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2394005
    iget-boolean v0, p0, LX/Go2;->g:Z

    if-eqz v0, :cond_0

    .line 2394006
    :goto_0
    return-void

    .line 2394007
    :cond_0
    iget-wide v0, p0, LX/Go2;->c:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LX/Go2;->b:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/Go2;->c:J

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2394002
    invoke-virtual {p0}, LX/Go2;->a()V

    .line 2394003
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Go2;->g:Z

    .line 2394004
    return-void
.end method
