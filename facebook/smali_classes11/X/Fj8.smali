.class public final LX/Fj8;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Fj8;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Fj6;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Fj9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2276350
    const/4 v0, 0x0

    sput-object v0, LX/Fj8;->a:LX/Fj8;

    .line 2276351
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Fj8;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2276381
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2276382
    new-instance v0, LX/Fj9;

    invoke-direct {v0}, LX/Fj9;-><init>()V

    iput-object v0, p0, LX/Fj8;->c:LX/Fj9;

    .line 2276383
    return-void
.end method

.method public static c(LX/1De;)LX/Fj6;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2276369
    new-instance v1, LX/Fj7;

    invoke-direct {v1}, LX/Fj7;-><init>()V

    .line 2276370
    sget-object v2, LX/Fj8;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fj6;

    .line 2276371
    if-nez v2, :cond_0

    .line 2276372
    new-instance v2, LX/Fj6;

    invoke-direct {v2}, LX/Fj6;-><init>()V

    .line 2276373
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Fj6;->a$redex0(LX/Fj6;LX/1De;IILX/Fj7;)V

    .line 2276374
    move-object v1, v2

    .line 2276375
    move-object v0, v1

    .line 2276376
    return-object v0
.end method

.method public static declared-synchronized q()LX/Fj8;
    .locals 2

    .prologue
    .line 2276377
    const-class v1, LX/Fj8;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Fj8;->a:LX/Fj8;

    if-nez v0, :cond_0

    .line 2276378
    new-instance v0, LX/Fj8;

    invoke-direct {v0}, LX/Fj8;-><init>()V

    sput-object v0, LX/Fj8;->a:LX/Fj8;

    .line 2276379
    :cond_0
    sget-object v0, LX/Fj8;->a:LX/Fj8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2276380
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2276363
    check-cast p2, LX/Fj7;

    .line 2276364
    iget-object v0, p2, LX/Fj7;->a:Ljava/lang/String;

    iget-object v1, p2, LX/Fj7;->b:Ljava/lang/String;

    const/4 p2, 0x6

    const/4 v5, 0x0

    const/4 p0, 0x1

    .line 2276365
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0097

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0e0123

    invoke-static {p1, v5, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b17d1

    invoke-interface {v3, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2276366
    return-object v0

    :cond_0
    const v2, 0x7f0e0123

    invoke-static {p1, v5, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-static {p1}, LX/39p;->a(LX/1De;)LX/39s;

    move-result-object v4

    sget-object v5, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    invoke-virtual {v4, v5}, LX/39s;->a(LX/1Wk;)LX/39s;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Di;->a(LX/1n6;)LX/1Di;

    move-result-object v2

    .line 2276367
    const v4, -0x3178f1b1

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2276368
    invoke-interface {v2, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b0060

    invoke-interface {v2, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b17d1

    invoke-interface {v2, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2276352
    invoke-static {}, LX/1dS;->b()V

    .line 2276353
    iget v0, p1, LX/1dQ;->b:I

    .line 2276354
    packed-switch v0, :pswitch_data_0

    .line 2276355
    :goto_0
    return-object v2

    .line 2276356
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2276357
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2276358
    check-cast v1, LX/Fj7;

    .line 2276359
    iget-object p0, v1, LX/Fj7;->c:Landroid/view/View$OnClickListener;

    .line 2276360
    if-nez p0, :cond_0

    .line 2276361
    :goto_1
    goto :goto_0

    .line 2276362
    :cond_0
    invoke-interface {p0, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x3178f1b1
        :pswitch_0
    .end packed-switch
.end method
