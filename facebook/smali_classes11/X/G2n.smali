.class public LX/G2n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/G2o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/G2n;
    .locals 3

    .prologue
    .line 2314499
    const-class v1, LX/G2n;

    monitor-enter v1

    .line 2314500
    :try_start_0
    sget-object v0, LX/G2n;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314501
    sput-object v2, LX/G2n;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314502
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314503
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2314504
    new-instance v0, LX/G2n;

    invoke-direct {v0}, LX/G2n;-><init>()V

    .line 2314505
    move-object v0, v0

    .line 2314506
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314507
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G2n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314508
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2314510
    if-lez p1, :cond_0

    .line 2314511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "PeopleYouMayKnowData can only be of size 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2314512
    :cond_0
    iget-object v0, p0, LX/G2n;->a:LX/G2o;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2314513
    iget-object v0, p0, LX/G2n;->a:LX/G2o;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
