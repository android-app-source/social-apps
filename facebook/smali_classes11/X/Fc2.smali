.class public LX/Fc2;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261718
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261719
    return-void
.end method

.method public static a(LX/0QB;)LX/Fc2;
    .locals 3

    .prologue
    .line 2261720
    const-class v1, LX/Fc2;

    monitor-enter v1

    .line 2261721
    :try_start_0
    sget-object v0, LX/Fc2;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261722
    sput-object v2, LX/Fc2;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261723
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261724
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261725
    new-instance v0, LX/Fc2;

    invoke-direct {v0}, LX/Fc2;-><init>()V

    .line 2261726
    move-object v0, v0

    .line 2261727
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261728
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fc2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261729
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuantity;)Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLQuantity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261731
    if-nez p0, :cond_0

    .line 2261732
    const/4 v0, 0x0

    .line 2261733
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/A2K;

    invoke-direct {v0}, LX/A2K;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuantity;->j()D

    move-result-wide v2

    .line 2261734
    iput-wide v2, v0, LX/A2K;->b:D

    .line 2261735
    move-object v0, v0

    .line 2261736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuantity;->a()Ljava/lang/String;

    move-result-object v1

    .line 2261737
    iput-object v1, v0, LX/A2K;->a:Ljava/lang/String;

    .line 2261738
    move-object v0, v0

    .line 2261739
    invoke-virtual {v0}, LX/A2K;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261740
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261741
    if-nez v0, :cond_0

    .line 2261742
    const/4 v0, 0x0

    .line 2261743
    :goto_0
    return-object v0

    .line 2261744
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2261745
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2261746
    move-object v1, v1

    .line 2261747
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x44790698

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2261748
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261749
    move-object v1, v1

    .line 2261750
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    .line 2261751
    iput-object v2, v1, LX/8dX;->ad:Ljava/lang/String;

    .line 2261752
    move-object v1, v1

    .line 2261753
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    .line 2261754
    if-nez v2, :cond_1

    .line 2261755
    const/4 v3, 0x0

    .line 2261756
    :goto_1
    move-object v2, v3

    .line 2261757
    iput-object v2, v1, LX/8dX;->aa:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    .line 2261758
    move-object v1, v1

    .line 2261759
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->li()Ljava/lang/String;

    move-result-object v2

    .line 2261760
    iput-object v2, v1, LX/8dX;->bg:Ljava/lang/String;

    .line 2261761
    move-object v1, v1

    .line 2261762
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->la()Lcom/facebook/graphql/model/GraphQLWeatherCondition;

    move-result-object v2

    .line 2261763
    if-nez v2, :cond_2

    .line 2261764
    const/4 v3, 0x0

    .line 2261765
    :goto_2
    move-object v2, v3

    .line 2261766
    iput-object v2, v1, LX/8dX;->be:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    .line 2261767
    move-object v1, v1

    .line 2261768
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lb()LX/0Px;

    move-result-object v2

    .line 2261769
    if-nez v2, :cond_3

    .line 2261770
    const/4 v4, 0x0

    .line 2261771
    :goto_3
    move-object v2, v4

    .line 2261772
    iput-object v2, v1, LX/8dX;->bf:LX/0Px;

    .line 2261773
    move-object v1, v1

    .line 2261774
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    .line 2261775
    if-nez v0, :cond_6

    .line 2261776
    const/4 v2, 0x0

    .line 2261777
    :goto_4
    move-object v0, v2

    .line 2261778
    iput-object v0, v1, LX/8dX;->q:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 2261779
    move-object v0, v1

    .line 2261780
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2261781
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2261782
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2261783
    move-object v0, v1

    .line 2261784
    goto :goto_0

    :cond_1
    new-instance v3, LX/8di;

    invoke-direct {v3}, LX/8di;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLocation;->j()Ljava/lang/String;

    move-result-object p0

    .line 2261785
    iput-object p0, v3, LX/8di;->c:Ljava/lang/String;

    .line 2261786
    move-object v3, v3

    .line 2261787
    invoke-virtual {v3}, LX/8di;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v3, LX/A2I;

    invoke-direct {v3}, LX/A2I;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->a()Ljava/lang/String;

    move-result-object p0

    .line 2261788
    iput-object p0, v3, LX/A2I;->a:Ljava/lang/String;

    .line 2261789
    move-object v3, v3

    .line 2261790
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object p0

    invoke-static {p0}, LX/Fc2;->a(Lcom/facebook/graphql/model/GraphQLQuantity;)Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object p0

    .line 2261791
    iput-object p0, v3, LX/A2I;->c:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    .line 2261792
    move-object v3, v3

    .line 2261793
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-static {p0}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    .line 2261794
    iput-object p0, v3, LX/A2I;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2261795
    move-object v3, v3

    .line 2261796
    invoke-virtual {v3}, LX/A2I;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v3

    goto :goto_2

    .line 2261797
    :cond_3
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2261798
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2261799
    goto :goto_3

    .line 2261800
    :cond_4
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2261801
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_5
    if-ge v5, v7, :cond_5

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;

    .line 2261802
    new-instance v8, LX/A2J;

    invoke-direct {v8}, LX/A2J;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->a()Ljava/lang/String;

    move-result-object v9

    .line 2261803
    iput-object v9, v8, LX/A2J;->a:Ljava/lang/String;

    .line 2261804
    move-object v8, v8

    .line 2261805
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v9

    invoke-static {v9}, LX/Fc2;->a(Lcom/facebook/graphql/model/GraphQLQuantity;)Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v9

    .line 2261806
    iput-object v9, v8, LX/A2J;->c:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    .line 2261807
    move-object v8, v8

    .line 2261808
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    invoke-static {v9}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v9

    .line 2261809
    iput-object v9, v8, LX/A2J;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2261810
    move-object v8, v8

    .line 2261811
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->l()J

    move-result-wide v10

    .line 2261812
    iput-wide v10, v8, LX/A2J;->d:J

    .line 2261813
    move-object v4, v8

    .line 2261814
    invoke-virtual {v4}, LX/A2J;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    move-result-object v4

    .line 2261815
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2261816
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 2261817
    :cond_5
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_3

    :cond_6
    new-instance v2, LX/8fv;

    invoke-direct {v2}, LX/8fv;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    .line 2261818
    if-nez v3, :cond_7

    .line 2261819
    const/4 v4, 0x0

    .line 2261820
    :goto_6
    move-object v3, v4

    .line 2261821
    iput-object v3, v2, LX/8fv;->a:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel$PhotoModel;

    .line 2261822
    move-object v2, v2

    .line 2261823
    invoke-virtual {v2}, LX/8fv;->a()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v2

    goto/16 :goto_4

    :cond_7
    new-instance v4, LX/8fw;

    invoke-direct {v4}, LX/8fw;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2261824
    iput-object v0, v4, LX/8fw;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2261825
    move-object v4, v4

    .line 2261826
    invoke-virtual {v4}, LX/8fw;->a()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel$PhotoModel;

    move-result-object v4

    goto :goto_6
.end method
