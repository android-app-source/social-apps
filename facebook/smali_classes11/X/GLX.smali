.class public final LX/GLX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

.field public final synthetic b:LX/GLb;


# direct methods
.method public constructor <init>(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V
    .locals 0

    .prologue
    .line 2343043
    iput-object p1, p0, LX/GLX;->b:LX/GLb;

    iput-object p2, p0, LX/GLX;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    .line 2343044
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, LX/GLX;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GLX;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2343045
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 2343046
    sget-object v3, LX/8wL;->AUDIENCE_MANAGEMENT:LX/8wL;

    const v4, 0x7f080b80

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2343047
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v4, v5, :cond_0

    .line 2343048
    const-string v4, "data_extra"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2343049
    :goto_0
    move-object v0, v3

    .line 2343050
    iget-object v1, p0, LX/GLX;->b:LX/GLb;

    .line 2343051
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2343052
    new-instance v2, LX/GFS;

    const/16 v3, 0xc

    const/4 v4, 0x1

    invoke-direct {v2, v0, v3, v4}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2343053
    const/4 v0, 0x0

    return v0

    .line 2343054
    :cond_0
    invoke-static {v0}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v4

    .line 2343055
    iget-object v5, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v5, v5

    .line 2343056
    invoke-static {v5}, LX/A96;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;)LX/A96;

    move-result-object v5

    .line 2343057
    iput-object v2, v5, LX/A96;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2343058
    move-object v5, v5

    .line 2343059
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v6

    .line 2343060
    iput-object v6, v5, LX/A96;->A:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 2343061
    move-object v5, v5

    .line 2343062
    invoke-virtual {v5}, LX/A96;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v5

    .line 2343063
    iput-object v5, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2343064
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v8

    invoke-static {v0, v5, v6, v7, v8}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2343065
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v6, v6

    .line 2343066
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 2343067
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p2

    const/4 v7, 0x0

    move v8, v7

    :goto_1
    if-ge v8, p2, :cond_2

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2343068
    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-eq v1, v0, :cond_1

    .line 2343069
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343070
    :cond_1
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_1

    .line 2343071
    :cond_2
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    move-object v6, v7

    .line 2343072
    iput-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    .line 2343073
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->n()Ljava/lang/String;

    move-result-object v6

    .line 2343074
    iput-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    .line 2343075
    invoke-virtual {v4, v5}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2343076
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(LX/0Px;)V

    .line 2343077
    const-string v5, "data_extra"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_0
.end method
