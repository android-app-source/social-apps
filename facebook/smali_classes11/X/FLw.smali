.class public LX/FLw;
.super LX/6lf;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6lf",
        "<",
        "LX/FLv;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field public b:LX/FLy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2228241
    invoke-direct {p0}, LX/6lf;-><init>()V

    .line 2228242
    iput-object p1, p0, LX/FLw;->a:Landroid/content/Context;

    .line 2228243
    return-void
.end method


# virtual methods
.method public final b(Landroid/view/ViewGroup;)LX/6le;
    .locals 3

    .prologue
    .line 2228244
    iget-object v0, p0, LX/FLw;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2228245
    const v1, 0x7f030a7d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2228246
    new-instance v1, LX/FLv;

    invoke-direct {v1, v0}, LX/FLv;-><init>(Landroid/view/View;)V

    return-object v1
.end method
