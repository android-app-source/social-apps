.class public final LX/G4z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/G4y;


# instance fields
.field public final a:LX/Fso;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/Fsp;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/G5A;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2318382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318383
    sget-object v0, LX/G5A;->LOADING:LX/G5A;

    iput-object v0, p0, LX/G4z;->c:LX/G5A;

    .line 2318384
    iput-object v1, p0, LX/G4z;->a:LX/Fso;

    .line 2318385
    iput-object v1, p0, LX/G4z;->b:LX/Fsp;

    .line 2318386
    return-void
.end method

.method public constructor <init>(LX/Fso;)V
    .locals 1
    .param p1    # LX/Fso;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2318387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318388
    sget-object v0, LX/G5A;->LOADING:LX/G5A;

    iput-object v0, p0, LX/G4z;->c:LX/G5A;

    .line 2318389
    iput-object p1, p0, LX/G4z;->a:LX/Fso;

    .line 2318390
    const/4 v0, 0x0

    iput-object v0, p0, LX/G4z;->b:LX/Fsp;

    .line 2318391
    return-void
.end method

.method public constructor <init>(LX/Fsp;)V
    .locals 1
    .param p1    # LX/Fsp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2318392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318393
    sget-object v0, LX/G5A;->LOADING:LX/G5A;

    iput-object v0, p0, LX/G4z;->c:LX/G5A;

    .line 2318394
    iput-object p1, p0, LX/G4z;->b:LX/Fsp;

    .line 2318395
    const/4 v0, 0x0

    iput-object v0, p0, LX/G4z;->a:LX/Fso;

    .line 2318396
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2318397
    iget-object v0, p0, LX/G4z;->c:LX/G5A;

    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
