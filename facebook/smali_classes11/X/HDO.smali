.class public final LX/HDO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public final synthetic c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

.field public final synthetic d:LX/HDF;

.field public final synthetic e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

.field public final synthetic f:LX/HDQ;


# direct methods
.method public constructor <init>(LX/HDQ;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;LX/HDF;Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;)V
    .locals 0

    .prologue
    .line 2441163
    iput-object p1, p0, LX/HDO;->f:LX/HDQ;

    iput-wide p2, p0, LX/HDO;->a:J

    iput-object p4, p0, LX/HDO;->b:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    iput-object p5, p0, LX/HDO;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    iput-object p6, p0, LX/HDO;->d:LX/HDF;

    iput-object p7, p0, LX/HDO;->e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x623dfb85

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2441164
    iget-object v0, p0, LX/HDO;->f:LX/HDQ;

    iget-wide v2, p0, LX/HDO;->a:J

    iget-object v1, p0, LX/HDO;->b:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    iget-object v4, p0, LX/HDO;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    invoke-static {v0, v2, v3, v1, v4}, LX/HDQ;->a$redex0(LX/HDQ;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2441165
    iget-object v0, p0, LX/HDO;->d:LX/HDF;

    iget-object v1, p0, LX/HDO;->f:LX/HDQ;

    iget-object v1, v1, LX/HDQ;->l:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    iget-wide v2, p0, LX/HDO;->a:J

    iget-object v4, p0, LX/HDO;->e:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/HDH;->EDIT:LX/HDH;

    new-instance v6, LX/HDN;

    invoke-direct {v6, p0}, LX/HDN;-><init>(LX/HDO;)V

    invoke-interface/range {v0 .. v6}, LX/HDE;->a(Landroid/app/Activity;JLjava/lang/String;LX/HDH;LX/HBj;)V

    .line 2441166
    const v0, 0x1f66019c

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
