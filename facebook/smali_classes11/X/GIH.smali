.class public LX/GIH;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336255
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336256
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2336266
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336267
    const/4 v0, 0x0

    iput-object v0, p0, LX/GIH;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2336268
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336260
    check-cast p1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2336261
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336262
    iput-object p1, p0, LX/GIH;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2336263
    iget-object v0, p0, LX/GIH;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2336264
    iget-object v0, p0, LX/GIH;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080b95

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2336265
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2336257
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336258
    iput-object p1, p0, LX/GIH;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336259
    return-void
.end method
