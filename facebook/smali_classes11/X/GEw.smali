.class public LX/GEw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GKb;

.field private b:LX/0ad;


# direct methods
.method public constructor <init>(LX/GKb;LX/0ad;)V
    .locals 0
    .param p1    # LX/GKb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332180
    iput-object p1, p0, LX/GEw;->a:LX/GKb;

    .line 2332181
    iput-object p2, p0, LX/GEw;->b:LX/0ad;

    .line 2332182
    return-void
.end method

.method public static a(LX/GGB;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2332171
    if-nez p0, :cond_0

    .line 2332172
    :goto_0
    return v0

    .line 2332173
    :cond_0
    sget-object v1, LX/GEv;->a:[I

    invoke-virtual {p0}, LX/GGB;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2332174
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332175
    const v0, 0x7f030070

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2332176
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    invoke-static {v1}, LX/GEw;->a(LX/GGB;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2332177
    iget-object v1, p0, LX/GEw;->b:LX/0ad;

    sget-short v2, LX/GDK;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2332178
    :cond_0
    return v0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332169
    iget-object v0, p0, LX/GEw;->a:LX/GKb;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332170
    sget-object v0, LX/8wK;->OVERVIEW:LX/8wK;

    return-object v0
.end method
