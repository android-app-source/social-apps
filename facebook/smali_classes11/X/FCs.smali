.class public LX/FCs;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:D


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2210730
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FCs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2210731
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2210728
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/FCs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2210729
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2210725
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2210726
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/FCs;->a:D

    .line 2210727
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 2210716
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V

    .line 2210717
    invoke-virtual {p0}, LX/FCs;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, LX/FCs;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 2210718
    invoke-virtual {p0}, LX/FCs;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v1, v0

    .line 2210719
    int-to-double v2, v1

    iget-wide v4, p0, LX/FCs;->a:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    add-int/2addr v0, v1

    .line 2210720
    invoke-virtual {p0}, LX/FCs;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/FCs;->setMeasuredDimension(II)V

    .line 2210721
    return-void
.end method

.method public setProgress(D)V
    .locals 1

    .prologue
    .line 2210722
    iput-wide p1, p0, LX/FCs;->a:D

    .line 2210723
    invoke-virtual {p0}, LX/FCs;->requestLayout()V

    .line 2210724
    return-void
.end method
