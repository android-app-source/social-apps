.class public final LX/EzB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/PersonYouMayKnow;

.field public final synthetic b:LX/2iS;


# direct methods
.method public constructor <init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 0

    .prologue
    .line 2186936
    iput-object p1, p0, LX/EzB;->b:LX/2iS;

    iput-object p2, p0, LX/EzB;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x3261ae40

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186930
    iget-object v1, p0, LX/EzB;->b:LX/2iS;

    iget-object v1, v1, LX/2iS;->g:LX/2hb;

    iget-object v2, p0, LX/EzB;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1, v2}, LX/2hb;->b(Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 2186931
    iget-object v1, p0, LX/EzB;->b:LX/2iS;

    iget-object v1, v1, LX/2iS;->j:LX/2hd;

    iget-object v2, p0, LX/EzB;->b:LX/2iS;

    iget-object v2, v2, LX/2iS;->o:Ljava/lang/String;

    iget-object v3, p0, LX/EzB;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 2186932
    iget-object v4, v3, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    move-object v3, v4

    .line 2186933
    iget-object v4, p0, LX/EzB;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v4}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    iget-object v6, p0, LX/EzB;->b:LX/2iS;

    iget-object v6, v6, LX/2iS;->l:LX/2h7;

    iget-object v6, v6, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->b(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2186934
    iget-object v1, p0, LX/EzB;->b:LX/2iS;

    iget-object v2, p0, LX/EzB;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-static {v1, v2}, LX/2iS;->a$redex0(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 2186935
    const v1, 0xccbd7af

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
