.class public interface abstract LX/GN0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GCE;",
            "TT;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/GCE;",
            "TT;)",
            "Landroid/widget/CompoundButton$OnCheckedChangeListener;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/view/View;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/GCE;",
            "TT;)V"
        }
    .end annotation
.end method

.method public abstract b(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GCE;",
            "TT;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation
.end method

.method public abstract b(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/GCE;",
            "TT;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation
.end method

.method public abstract c(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/GCE;",
            "TT;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation
.end method

.method public abstract d(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/GCE;",
            "TT;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation
.end method

.method public abstract e(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/GCE;",
            "TT;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation
.end method
