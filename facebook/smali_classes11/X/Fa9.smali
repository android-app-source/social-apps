.class public final LX/Fa9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;",
        ">;>;",
        "LX/0Px",
        "<",
        "LX/CwV;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FaA;


# direct methods
.method public constructor <init>(LX/FaA;)V
    .locals 0

    .prologue
    .line 2258419
    iput-object p1, p0, LX/Fa9;->a:LX/FaA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2258420
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2258421
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2258422
    check-cast v0, Ljava/util/List;

    .line 2258423
    const/4 v1, 0x0

    .line 2258424
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2258425
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;

    .line 2258426
    invoke-virtual {v2}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;->a()Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;

    move-result-object v2

    .line 2258427
    if-eqz v2, :cond_0

    .line 2258428
    invoke-virtual {v2}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->a()LX/2uF;

    move-result-object v2

    .line 2258429
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2258430
    const-class p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$SearchCategorySubTopicModel;

    invoke-virtual {v6, v2, v1, p0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$SearchCategorySubTopicModel;

    .line 2258431
    if-eqz v2, :cond_1

    .line 2258432
    new-instance v6, LX/CwV;

    invoke-virtual {v2}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$SearchCategorySubTopicModel;->j()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$SearchCategorySubTopicModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$SearchCategorySubTopicModel;->k()Z

    move-result v2

    invoke-direct {v6, p0, p1, v2, v1}, LX/CwV;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2258433
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2258434
    move-object v0, v2

    .line 2258435
    return-object v0
.end method
