.class public final LX/GZ6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

.field public final synthetic b:LX/GZ8;


# direct methods
.method public constructor <init>(LX/GZ8;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;)V
    .locals 0

    .prologue
    .line 2365951
    iput-object p1, p0, LX/GZ6;->b:LX/GZ8;

    iput-object p2, p0, LX/GZ6;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x6b23ba8d

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2365952
    iget-object v1, p0, LX/GZ6;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GZ6;->b:LX/GZ8;

    iget-wide v2, v2, LX/GZ8;->g:J

    iget-object v4, p0, LX/GZ6;->b:LX/GZ8;

    iget-boolean v4, v4, LX/GZ8;->h:Z

    invoke-static {v1, v2, v3, v4}, LX/7iS;->a(Ljava/lang/String;JZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2365953
    iget-object v2, p0, LX/GZ6;->b:LX/GZ8;

    iget-object v2, v2, LX/GZ8;->c:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2365954
    iget-object v1, p0, LX/GZ6;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GZ6;->b:LX/GZ8;

    iget-boolean v2, v2, LX/GZ8;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sget-object v3, LX/7iO;->COLLECTION_GRID:LX/7iO;

    invoke-static {v1, v2, v3}, LX/7iS;->b(Ljava/lang/String;Ljava/lang/Boolean;LX/7iO;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2365955
    iget-object v2, p0, LX/GZ6;->b:LX/GZ8;

    iget-object v2, v2, LX/GZ8;->c:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2365956
    iget-object v1, p0, LX/GZ6;->b:LX/GZ8;

    iget-boolean v1, v1, LX/GZ8;->h:Z

    if-eqz v1, :cond_0

    .line 2365957
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, LX/GZ6;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2365958
    iget-object v2, p0, LX/GZ6;->b:LX/GZ8;

    iget-object v2, v2, LX/GZ8;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/GZ6;->b:LX/GZ8;

    iget-object v3, v3, LX/GZ8;->f:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2365959
    const v1, -0x49b32d7d

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2365960
    :goto_0
    return-void

    .line 2365961
    :cond_0
    iget-object v1, p0, LX/GZ6;->b:LX/GZ8;

    iget-object v1, v1, LX/GZ8;->a:LX/7j6;

    iget-object v2, p0, LX/GZ6;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/GZ6;->b:LX/GZ8;

    iget-object v3, v3, LX/GZ8;->j:LX/7iP;

    invoke-virtual {v1, v2, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2365962
    const v1, -0x2dd9ab22

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
