.class public final LX/Fa8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$FetchSearchCategoryTopicsModel;",
        ">;",
        "LX/0Px",
        "<",
        "LX/CwV;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FaA;


# direct methods
.method public constructor <init>(LX/FaA;)V
    .locals 0

    .prologue
    .line 2258410
    iput-object p1, p0, LX/Fa8;->a:LX/FaA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2258411
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2258412
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2258413
    check-cast v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$FetchSearchCategoryTopicsModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$FetchSearchCategoryTopicsModel;->a()LX/0Px;

    move-result-object v2

    .line 2258414
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2258415
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;

    .line 2258416
    new-instance v5, LX/CwV;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->k()Z

    move-result v0

    const/4 v8, 0x1

    invoke-direct {v5, v6, v7, v0, v8}, LX/CwV;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2258417
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2258418
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
