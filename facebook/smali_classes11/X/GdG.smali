.class public LX/GdG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2373438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2373439
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2373440
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2373441
    :cond_0
    const/4 v0, 0x0

    .line 2373442
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2373431
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 2373432
    if-nez v1, :cond_1

    .line 2373433
    :cond_0
    :goto_0
    return-object v0

    .line 2373434
    :cond_1
    invoke-virtual {v1}, LX/0lF;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2373435
    invoke-virtual {v1}, LX/0lF;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2373436
    :cond_2
    invoke-virtual {v1}, LX/0lF;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2373437
    :try_start_0
    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public static d(LX/0lF;Ljava/lang/String;)Ljava/util/Date;
    .locals 2

    .prologue
    .line 2373420
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2373421
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2373422
    :cond_0
    const/4 v0, 0x0

    .line 2373423
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2373424
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string p0, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, p0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2373425
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2373426
    :goto_1
    move-object v0, v1

    .line 2373427
    goto :goto_0

    .line 2373428
    :catch_0
    move-exception v1

    .line 2373429
    const-string p0, "AutoUpdater"

    const-string p1, "Failed to parse publish date"

    invoke-static {p0, p1, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2373430
    const/4 v1, 0x0

    goto :goto_1
.end method
