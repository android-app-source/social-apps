.class public LX/FfZ;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FfZ;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2268652
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f08231d

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268653
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2268654
    iput-object v0, p0, LX/FfZ;->a:LX/0Ot;

    .line 2268655
    return-void
.end method

.method public static a(LX/0QB;)LX/FfZ;
    .locals 4

    .prologue
    .line 2268637
    sget-object v0, LX/FfZ;->b:LX/FfZ;

    if-nez v0, :cond_1

    .line 2268638
    const-class v1, LX/FfZ;

    monitor-enter v1

    .line 2268639
    :try_start_0
    sget-object v0, LX/FfZ;->b:LX/FfZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268640
    if-eqz v2, :cond_0

    .line 2268641
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268642
    new-instance p0, LX/FfZ;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/FfZ;-><init>(Landroid/content/res/Resources;)V

    .line 2268643
    const/16 v3, 0x1032

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2268644
    iput-object v3, p0, LX/FfZ;->a:LX/0Ot;

    .line 2268645
    move-object v0, p0

    .line 2268646
    sput-object v0, LX/FfZ;->b:LX/FfZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268647
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268648
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268649
    :cond_1
    sget-object v0, LX/FfZ;->b:LX/FfZ;

    return-object v0

    .line 2268650
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CwB;Ljava/lang/String;)LX/CwH;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268631
    invoke-virtual {p0, p1, p2}, LX/FfQ;->b(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7BG;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2268632
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2268633
    move-object v0, v0

    .line 2268634
    return-object v0
.end method

.method public final b()LX/Fdv;
    .locals 3

    .prologue
    .line 2268636
    iget-object v0, p0, LX/FfZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->ay:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "graph_search_results_page_commerce"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsFragment;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "graph_search_results_page_commerce"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->b(Ljava/lang/String;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic c(LX/CwB;Ljava/lang/String;)LX/CwA;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268635
    invoke-virtual {p0, p1, p2}, LX/FfQ;->a(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    return-object v0
.end method
