.class public final LX/FIm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

.field private final b:Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;)V
    .locals 0

    .prologue
    .line 2222618
    iput-object p1, p0, LX/FIm;->a:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222619
    iput-object p2, p0, LX/FIm;->b:Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;

    .line 2222620
    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2222621
    iget-object v0, p0, LX/FIm;->a:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->a:LX/18V;

    iget-object v1, p0, LX/FIm;->a:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->b:LX/FIW;

    iget-object v2, p0, LX/FIm;->b:Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;

    iget-object v3, p0, LX/FIm;->a:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    iget-object v3, v3, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->c:LX/14U;

    invoke-virtual {v0, v1, v2, v3}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;

    return-object v0
.end method
