.class public LX/G0q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G0r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2310377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310378
    return-void
.end method

.method public static a(LX/0QB;)LX/G0q;
    .locals 6

    .prologue
    .line 2310386
    const-class v1, LX/G0q;

    monitor-enter v1

    .line 2310387
    :try_start_0
    sget-object v0, LX/G0q;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2310388
    sput-object v2, LX/G0q;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2310389
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2310390
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2310391
    new-instance v3, LX/G0q;

    invoke-direct {v3}, LX/G0q;-><init>()V

    .line 2310392
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v4

    const/16 v5, 0xf2f

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x369f

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2310393
    iput-object v4, v3, LX/G0q;->a:LX/0Or;

    iput-object v5, v3, LX/G0q;->b:LX/0Or;

    iput-object p0, v3, LX/G0q;->c:LX/0Or;

    .line 2310394
    move-object v0, v3

    .line 2310395
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2310396
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G0q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2310397
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2310398
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/G0q;Ljava/lang/String;LX/1bf;LX/0Px;Ljava/util/List;)Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1bf;",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;",
            "Ljava/util/List",
            "<+",
            "LX/5vn;",
            ">;)",
            "Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;"
        }
    .end annotation

    .prologue
    .line 2310399
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2310400
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    .line 2310401
    if-eqz v0, :cond_0

    .line 2310402
    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2310403
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2310404
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2310405
    invoke-static {v0}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v0

    .line 2310406
    sget-object v1, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 2310407
    new-instance p1, LX/0Pz;

    invoke-direct {p1}, LX/0Pz;-><init>()V

    .line 2310408
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_2

    .line 2310409
    invoke-virtual {p3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1U8;

    .line 2310410
    invoke-interface {p4, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5vn;

    .line 2310411
    iget-object v3, p0, LX/G0q;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G0r;

    .line 2310412
    invoke-interface {v1}, LX/1U8;->d()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, v1, v2}, LX/G0r;->b(LX/1U8;LX/5vn;)LX/1Fb;

    move-result-object v1

    invoke-static {p2, v1}, LX/FwM;->a(Ljava/lang/String;LX/1Fb;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v1

    .line 2310413
    invoke-virtual {p1, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2310414
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 2310415
    :cond_2
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2310416
    invoke-virtual {v0, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1af;Landroid/graphics/Rect;LX/1U8;LX/5vn;LX/0Px;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1af;",
            "Landroid/graphics/Rect;",
            "LX/1U8;",
            "LX/5vn;",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;",
            "Ljava/util/List",
            "<+",
            "LX/5vn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2310379
    invoke-interface {p4}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v2

    .line 2310380
    iget-object v0, p0, LX/G0q;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G0r;

    .line 2310381
    invoke-virtual {v0, p4, p5}, LX/G0r;->a(LX/1U8;LX/5vn;)LX/1bf;

    move-result-object v3

    .line 2310382
    iget-object v0, p0, LX/G0q;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    .line 2310383
    iget-object v1, p0, LX/G0q;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 2310384
    invoke-static {p0, v2, v3, p6, p7}, LX/G0q;->a(LX/G0q;Ljava/lang/String;LX/1bf;LX/0Px;Ljava/util/List;)Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v4

    invoke-static {v2, p1, p2, p3, v3}, LX/FwM;->a(Ljava/lang/String;Landroid/view/View;LX/1af;Landroid/graphics/Rect;LX/1bf;)LX/9hN;

    move-result-object v2

    invoke-interface {v0, v1, v4, v2}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2310385
    return-void
.end method
