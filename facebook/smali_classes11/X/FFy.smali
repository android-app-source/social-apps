.class public LX/FFy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/FFv;

.field private final b:LX/0So;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2218146
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FFy;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FFv;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2218084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218085
    iput-object p1, p0, LX/FFy;->a:LX/FFv;

    .line 2218086
    iput-object p2, p0, LX/FFy;->b:LX/0So;

    .line 2218087
    return-void
.end method

.method public static a(LX/0QB;)LX/FFy;
    .locals 8

    .prologue
    .line 2218088
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2218089
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2218090
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2218091
    if-nez v1, :cond_0

    .line 2218092
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2218093
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2218094
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2218095
    sget-object v1, LX/FFy;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2218096
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2218097
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2218098
    :cond_1
    if-nez v1, :cond_4

    .line 2218099
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2218100
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2218101
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2218102
    new-instance p0, LX/FFy;

    invoke-static {v0}, LX/FFv;->a(LX/0QB;)LX/FFv;

    move-result-object v1

    check-cast v1, LX/FFv;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-direct {p0, v1, v7}, LX/FFy;-><init>(LX/FFv;LX/0So;)V

    .line 2218103
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2218104
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2218105
    if-nez v1, :cond_2

    .line 2218106
    sget-object v0, LX/FFy;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFy;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2218107
    :goto_1
    if-eqz v0, :cond_3

    .line 2218108
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2218109
    :goto_3
    check-cast v0, LX/FFy;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2218110
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2218111
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2218112
    :catchall_1
    move-exception v0

    .line 2218113
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2218114
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2218115
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2218116
    :cond_2
    :try_start_8
    sget-object v0, LX/FFy;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFy;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 2218117
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2218118
    if-nez v0, :cond_0

    .line 2218119
    sget-object v1, LX/1nY;->OTHER:LX/1nY;

    const-string v2, "Params can\'t be null."

    invoke-static {v1, v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    .line 2218120
    :cond_0
    const-string v1, "FetchThreadUsersParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadUsersParams;

    .line 2218121
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2218122
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadUsersParams;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2218123
    iget-object v5, p0, LX/FFy;->a:LX/FFv;

    .line 2218124
    if-nez v0, :cond_4

    .line 2218125
    const/4 v6, 0x0

    .line 2218126
    :cond_1
    :goto_1
    move-object v0, v6

    .line 2218127
    if-eqz v0, :cond_2

    .line 2218128
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2218129
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2218130
    :cond_3
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadUsersResult;

    sget-object v1, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-object v3, p0, LX/FFy;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/facebook/messaging/service/model/FetchThreadUsersResult;-><init>(LX/0ta;JLjava/util/List;)V

    .line 2218131
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2218132
    :cond_4
    iget-object v6, v5, LX/FFv;->j:LX/2Oi;

    invoke-virtual {v6, v0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v6

    .line 2218133
    if-eqz v6, :cond_6

    .line 2218134
    :cond_5
    :goto_2
    move-object v6, v6

    .line 2218135
    if-nez v6, :cond_1

    .line 2218136
    invoke-static {v5, v0}, LX/FFv;->c(LX/FFv;Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v6

    goto :goto_1

    .line 2218137
    :cond_6
    iget-object p1, v5, LX/FFv;->c:LX/3fX;

    invoke-virtual {p1, v0}, LX/3fX;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object p1

    .line 2218138
    if-eqz p1, :cond_5

    .line 2218139
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    .line 2218140
    invoke-static {p1}, LX/6Ok;->a(Lcom/facebook/contacts/graphql/Contact;)Lcom/facebook/user/model/User;

    move-result-object v6

    goto :goto_2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2218141
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v0

    .line 2218142
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 2218143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2218144
    :pswitch_0
    const-string v2, "fetch_users"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 2218145
    :pswitch_1
    invoke-direct {p0, p1}, LX/FFy;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x357a8443
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
