.class public final enum LX/F4t;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F4t;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F4t;

.field public static final enum FETCH_PAGE_NAME:LX/F4t;

.field public static final enum FETCH_PAGE_VC:LX/F4t;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2197748
    new-instance v0, LX/F4t;

    const-string v1, "FETCH_PAGE_NAME"

    invoke-direct {v0, v1, v2}, LX/F4t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F4t;->FETCH_PAGE_NAME:LX/F4t;

    .line 2197749
    new-instance v0, LX/F4t;

    const-string v1, "FETCH_PAGE_VC"

    invoke-direct {v0, v1, v3}, LX/F4t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F4t;->FETCH_PAGE_VC:LX/F4t;

    .line 2197750
    const/4 v0, 0x2

    new-array v0, v0, [LX/F4t;

    sget-object v1, LX/F4t;->FETCH_PAGE_NAME:LX/F4t;

    aput-object v1, v0, v2

    sget-object v1, LX/F4t;->FETCH_PAGE_VC:LX/F4t;

    aput-object v1, v0, v3

    sput-object v0, LX/F4t;->$VALUES:[LX/F4t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2197746
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F4t;
    .locals 1

    .prologue
    .line 2197747
    const-class v0, LX/F4t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F4t;

    return-object v0
.end method

.method public static values()[LX/F4t;
    .locals 1

    .prologue
    .line 2197745
    sget-object v0, LX/F4t;->$VALUES:[LX/F4t;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F4t;

    return-object v0
.end method
