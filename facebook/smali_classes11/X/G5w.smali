.class public final LX/G5w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2319508
    iput-object p1, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    iput-object p2, p0, LX/G5w;->a:Landroid/view/View;

    iput-object p3, p0, LX/G5w;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2319509
    iget-object v0, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    invoke-virtual {v0}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->finish()V

    .line 2319510
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2319511
    const-string v1, "show_footer"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2319512
    const-string v1, "short_name"

    iget-object v2, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    iget-object v2, v2, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319513
    const-string v1, "footer_text"

    iget-object v2, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    const v3, 0x7f081565

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    iget-object v5, v5, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->y:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319514
    const-string v1, "footer_link_text"

    iget-object v2, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    const v3, 0x7f081566

    invoke-virtual {v2, v3}, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319515
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    iget-object v3, v3, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->z:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2319516
    iget-object v2, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    iget-object v2, v2, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->r:LX/17W;

    iget-object v3, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    invoke-virtual {v2, v3, v1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2319517
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2319518
    iget-object v0, p0, LX/G5w;->a:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2319519
    iget-object v0, p0, LX/G5w;->c:Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;

    iget-object v0, v0, Lcom/facebook/wem/sahayta/SahaytaChecklistActivity;->p:LX/G62;

    iget-object v1, p0, LX/G5w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/G62;->a(Ljava/lang/String;)V

    .line 2319520
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2319521
    invoke-direct {p0}, LX/G5w;->a()V

    return-void
.end method
