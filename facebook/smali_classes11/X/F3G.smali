.class public final LX/F3G;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;)V
    .locals 0

    .prologue
    .line 2193909
    iput-object p1, p0, LX/F3G;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2193910
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2193911
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2193912
    iget-object v0, p0, LX/F3G;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->k:LX/F2r;

    .line 2193913
    iget-object v2, v0, LX/F2r;->a:LX/F2L;

    .line 2193914
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v5, v3

    .line 2193915
    check-cast v5, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193916
    if-nez v5, :cond_1

    .line 2193917
    :goto_0
    iget-object v1, p0, LX/F3G;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    .line 2193918
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2193919
    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193920
    if-eqz v0, :cond_0

    .line 2193921
    iput-object v0, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193922
    iget-object v2, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->k:LX/F2r;

    .line 2193923
    iget-object v3, v2, LX/F2r;->a:LX/F2L;

    iget-object v1, v2, LX/F2r;->r:LX/F2f;

    invoke-virtual {v3, v0, v1}, LX/F2L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;LX/F2f;)V

    .line 2193924
    invoke-static {v2, v0}, LX/F2r;->b(LX/F2r;Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)V

    .line 2193925
    :cond_0
    return-void

    .line 2193926
    :cond_1
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v9

    .line 2193927
    invoke-virtual {v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2193928
    new-instance v4, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2193929
    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v6, v3

    .line 2193930
    iget-wide v7, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    invoke-direct/range {v4 .. v9}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2193931
    iget-object v3, v2, LX/F2L;->g:LX/1My;

    iget-object v6, v2, LX/F2L;->k:LX/0TF;

    invoke-virtual {v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v6, v5, v4}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0
.end method
