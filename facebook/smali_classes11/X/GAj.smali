.class public LX/GAj;
.super Ljava/io/FilterOutputStream;
.source ""

# interfaces
.implements LX/GAh;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/GAU;",
            "LX/GAk;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/GAX;

.field private final c:J

.field private d:J

.field private e:J

.field private f:J

.field private g:LX/GAk;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;LX/GAX;Ljava/util/Map;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/OutputStream;",
            "LX/GAX;",
            "Ljava/util/Map",
            "<",
            "LX/GAU;",
            "LX/GAk;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2326107
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 2326108
    iput-object p2, p0, LX/GAj;->b:LX/GAX;

    .line 2326109
    iput-object p3, p0, LX/GAj;->a:Ljava/util/Map;

    .line 2326110
    iput-wide p4, p0, LX/GAj;->f:J

    .line 2326111
    invoke-static {}, LX/GAK;->h()J

    move-result-wide v0

    iput-wide v0, p0, LX/GAj;->c:J

    .line 2326112
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2326113
    iget-wide v0, p0, LX/GAj;->d:J

    iget-wide v2, p0, LX/GAj;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 2326114
    iget-object v0, p0, LX/GAj;->b:LX/GAX;

    .line 2326115
    iget-object v1, v0, LX/GAX;->f:Ljava/util/List;

    move-object v0, v1

    .line 2326116
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GA5;

    .line 2326117
    instance-of v2, v0, LX/GAW;

    if-eqz v2, :cond_0

    .line 2326118
    iget-object v2, p0, LX/GAj;->b:LX/GAX;

    .line 2326119
    iget-object v3, v2, LX/GAX;->b:Landroid/os/Handler;

    move-object v2, v3

    .line 2326120
    check-cast v0, LX/GAW;

    .line 2326121
    if-eqz v2, :cond_0

    .line 2326122
    new-instance v3, Lcom/facebook/ProgressOutputStream$1;

    invoke-direct {v3, p0, v0}, Lcom/facebook/ProgressOutputStream$1;-><init>(LX/GAj;LX/GAW;)V

    const v0, 0x5ee31c6a

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 2326123
    :cond_1
    iget-wide v0, p0, LX/GAj;->d:J

    iput-wide v0, p0, LX/GAj;->e:J

    .line 2326124
    :cond_2
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 2326101
    iget-object v0, p0, LX/GAj;->g:LX/GAk;

    if-eqz v0, :cond_0

    .line 2326102
    iget-object v0, p0, LX/GAj;->g:LX/GAk;

    invoke-virtual {v0, p1, p2}, LX/GAk;->a(J)V

    .line 2326103
    :cond_0
    iget-wide v0, p0, LX/GAj;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/GAj;->d:J

    .line 2326104
    iget-wide v0, p0, LX/GAj;->d:J

    iget-wide v2, p0, LX/GAj;->e:J

    iget-wide v4, p0, LX/GAj;->c:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-wide v0, p0, LX/GAj;->d:J

    iget-wide v2, p0, LX/GAj;->f:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 2326105
    :cond_1
    invoke-direct {p0}, LX/GAj;->a()V

    .line 2326106
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(LX/GAU;)V
    .locals 1

    .prologue
    .line 2326125
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/GAj;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAk;

    :goto_0
    iput-object v0, p0, LX/GAj;->g:LX/GAk;

    .line 2326126
    return-void

    .line 2326127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 2326096
    invoke-super {p0}, Ljava/io/FilterOutputStream;->close()V

    .line 2326097
    iget-object v0, p0, LX/GAj;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAk;

    .line 2326098
    invoke-virtual {v0}, LX/GAk;->a()V

    goto :goto_0

    .line 2326099
    :cond_0
    invoke-direct {p0}, LX/GAj;->a()V

    .line 2326100
    return-void
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 2326093
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 2326094
    const-wide/16 v0, 0x1

    invoke-direct {p0, v0, v1}, LX/GAj;->a(J)V

    .line 2326095
    return-void
.end method

.method public final write([B)V
    .locals 2

    .prologue
    .line 2326087
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 2326088
    array-length v0, p1

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, LX/GAj;->a(J)V

    .line 2326089
    return-void
.end method

.method public final write([BII)V
    .locals 2

    .prologue
    .line 2326090
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 2326091
    int-to-long v0, p3

    invoke-direct {p0, v0, v1}, LX/GAj;->a(J)V

    .line 2326092
    return-void
.end method
