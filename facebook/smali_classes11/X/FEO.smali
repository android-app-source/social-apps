.class public LX/FEO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/11S;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/11S;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216249
    iput-object p1, p0, LX/FEO;->a:Landroid/content/Context;

    .line 2216250
    iput-object p2, p0, LX/FEO;->b:LX/11S;

    .line 2216251
    return-void
.end method

.method public static c(LX/11S;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2216247
    sget-object v0, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {p0, v0, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/11S;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2216260
    sget-object v0, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {p0, v0, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILX/FE8;)V
    .locals 4

    .prologue
    .line 2216252
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2216253
    const/16 v1, 0xc

    .line 2216254
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 2216255
    rem-int/lit8 v2, v2, 0x1e

    rsub-int/lit8 v2, v2, 0x1e

    move v2, v2

    .line 2216256
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2216257
    new-instance v1, LX/FEN;

    iget-object v2, p0, LX/FEO;->a:Landroid/content/Context;

    iget-object v3, p0, LX/FEO;->b:LX/11S;

    invoke-direct {v1, v2, v3}, LX/FEN;-><init>(Landroid/content/Context;LX/11S;)V

    .line 2216258
    new-instance v2, LX/FEH;

    invoke-direct {v2, p0, p2}, LX/FEH;-><init>(LX/FEO;LX/FE8;)V

    invoke-virtual {v1, p1, v0, v2}, LX/FEN;->a(ILjava/util/Calendar;LX/FEH;)V

    .line 2216259
    return-void
.end method
