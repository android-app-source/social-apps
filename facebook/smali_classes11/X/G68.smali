.class public final LX/G68;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/wem/watermark/WatermarkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V
    .locals 0

    .prologue
    .line 2319768
    iput-object p1, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 12

    .prologue
    .line 2319740
    iget-object v0, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->r:LX/G6G;

    iget-object v1, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v2, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v3, v2, Lcom/facebook/wem/watermark/WatermarkActivity;->z:LX/5QV;

    iget-object v2, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    invoke-virtual {v2}, Lcom/facebook/wem/watermark/WatermarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "show_footer"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iget-object v2, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v2, v2, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    .line 2319741
    iget-object v5, v2, LX/G6Q;->b:Ljava/util/HashMap;

    invoke-static {v5}, LX/2oC;->a(Ljava/util/Map;)LX/2oC;

    move-result-object v5

    move-object v5, v5

    .line 2319742
    move-object v2, p1

    .line 2319743
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v6

    invoke-static {v3}, LX/B5P;->a(LX/5QV;)Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v7

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setEditedUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v6

    sget-object v7, LX/G6G;->a:Landroid/graphics/RectF;

    invoke-static {v7}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v6

    .line 2319744
    new-instance v7, LX/5Ru;

    invoke-direct {v7}, LX/5Ru;-><init>()V

    .line 2319745
    iput-object v6, v7, LX/5Ru;->d:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 2319746
    move-object v6, v7

    .line 2319747
    sget-object v7, LX/G6G;->a:Landroid/graphics/RectF;

    .line 2319748
    iput-object v7, v6, LX/5Ru;->f:Landroid/graphics/RectF;

    .line 2319749
    move-object v6, v6

    .line 2319750
    iput-object v2, v6, LX/5Ru;->a:Landroid/net/Uri;

    .line 2319751
    move-object v6, v6

    .line 2319752
    invoke-interface {v3}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v7

    .line 2319753
    iput-object v7, v6, LX/5Ru;->g:Ljava/lang/String;

    .line 2319754
    move-object v6, v6

    .line 2319755
    iget-object v7, v0, LX/G6G;->h:Ljava/lang/String;

    .line 2319756
    iput-object v7, v6, LX/5Ru;->c:Ljava/lang/String;

    .line 2319757
    move-object v7, v6

    .line 2319758
    const-string v6, "new_profile_picture_base"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2319759
    iput-object v6, v7, LX/5Ru;->e:Ljava/lang/String;

    .line 2319760
    move-object v6, v7

    .line 2319761
    invoke-virtual {v6}, LX/5Ru;->a()Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    move-result-object v7

    .line 2319762
    iget-object v6, v0, LX/G6G;->c:LX/5vm;

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const-string v11, "photo_selector"

    invoke-virtual {v5, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual/range {v6 .. v11}, LX/5vm;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2319763
    const-string v7, "show_watermark_footer"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2319764
    const-string v7, "analytics_params"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2319765
    iget-object v7, v0, LX/G6G;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v7, v6, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2319766
    iget-object v0, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    invoke-virtual {v0}, Lcom/facebook/wem/watermark/WatermarkActivity;->finish()V

    .line 2319767
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2319736
    iget-object v0, p0, LX/G68;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 2319737
    iget-object p0, v0, LX/G6Q;->b:Ljava/util/HashMap;

    const-string p1, "reason"

    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2319738
    const-string p0, "fb4a_watermark_photo_creation_failed"

    iget-object p1, v0, LX/G6Q;->b:Ljava/util/HashMap;

    invoke-static {v0, p0, p1}, LX/G6Q;->a(LX/G6Q;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 2319739
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2319735
    check-cast p1, Landroid/net/Uri;

    invoke-direct {p0, p1}, LX/G68;->a(Landroid/net/Uri;)V

    return-void
.end method
