.class public LX/GaC;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/7ix;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7iw;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

.field public e:Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

.field public f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2368031
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2368032
    const-class v0, LX/GaC;

    invoke-static {v0, p0}, LX/GaC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2368033
    const v0, 0x7f0313ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2368034
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/GaC;->setOrientation(I)V

    .line 2368035
    const v0, 0x7f0d2da9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/GaC;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2368036
    const v0, 0x7f0d2daa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    iput-object v0, p0, LX/GaC;->d:Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    .line 2368037
    const v0, 0x7f0d2dab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    iput-object v0, p0, LX/GaC;->e:Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    .line 2368038
    const v0, 0x7f0d2dac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/GaC;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2368039
    const v0, 0x7f0d2da8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2368040
    iget-object v1, p0, LX/GaC;->a:LX/7ix;

    invoke-virtual {v1, v0}, LX/7ix;->a(Lcom/facebook/fbui/widget/contentview/ContentView;)LX/7iw;

    move-result-object v0

    iput-object v0, p0, LX/GaC;->b:LX/7iw;

    .line 2368041
    invoke-virtual {p0}, LX/GaC;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, LX/GaC;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, LX/GaC;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, LX/GaC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0b0060

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, LX/GaC;->setPadding(IIII)V

    .line 2368042
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/GaC;

    const-class p0, LX/7ix;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/7ix;

    iput-object v1, p1, LX/GaC;->a:LX/7ix;

    return-void
.end method
