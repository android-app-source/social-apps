.class public final LX/GOg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:[Ljava/lang/CharSequence;

.field public final synthetic b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

.field public final synthetic c:Lcom/facebook/common/locale/Country;

.field public final synthetic d:I

.field public final synthetic e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;[Ljava/lang/CharSequence;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;I)V
    .locals 0

    .prologue
    .line 2347928
    iput-object p1, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iput-object p2, p0, LX/GOg;->a:[Ljava/lang/CharSequence;

    iput-object p3, p0, LX/GOg;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iput-object p4, p0, LX/GOg;->c:Lcom/facebook/common/locale/Country;

    iput p5, p0, LX/GOg;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 2347929
    iget-object v0, p0, LX/GOg;->a:[Ljava/lang/CharSequence;

    aget-object v0, v0, p2

    check-cast v0, Ljava/lang/String;

    .line 2347930
    iget-object v1, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    const v2, 0x7f0827c3

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2347931
    iget-object v0, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->n:LX/ADW;

    const-string v1, "payments_new_credit_card_selected"

    iget-object v2, p0, LX/GOg;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2}, LX/ADW;->a(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2347932
    iget-object v0, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/GOg;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, LX/GOg;->c:Lcom/facebook/common/locale/Country;

    invoke-static {v0, v1, v2}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    .line 2347933
    iget v1, p0, LX/GOg;->d:I

    if-ne v1, v3, :cond_0

    .line 2347934
    iget-object v1, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iget-object v1, v1, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2347935
    :goto_0
    return-void

    .line 2347936
    :cond_0
    iget-object v1, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iget-object v1, v1, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iget v2, p0, LX/GOg;->d:I

    iget-object v3, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-virtual {v3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2347937
    :cond_1
    iget-object v0, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->n:LX/ADW;

    sget-object v1, LX/ADV;->ADD_PAYPAL_STATE:LX/ADV;

    iget-object v2, p0, LX/GOg;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2}, LX/ADW;->a(LX/ADV;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    .line 2347938
    iget-object v0, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->n:LX/ADW;

    const-string v1, "payments_new_paypal_selected"

    iget-object v2, p0, LX/GOg;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2}, LX/ADW;->a(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2347939
    iget-object v0, p0, LX/GOg;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347940
    iget-object v1, v0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v0, v1

    .line 2347941
    iget-object v1, p0, LX/GOg;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2347942
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2347943
    invoke-static {v0, v1}, LX/GPx;->a(LX/6xg;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2347944
    iget v1, p0, LX/GOg;->d:I

    if-ne v1, v3, :cond_2

    .line 2347945
    iget-object v1, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iget-object v1, v1, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2347946
    :cond_2
    iget-object v1, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    iget-object v1, v1, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iget v2, p0, LX/GOg;->d:I

    iget-object v3, p0, LX/GOg;->e:Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-virtual {v3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method
