.class public LX/FLq;
.super LX/6lf;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6lf",
        "<",
        "LX/FLp;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1Ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/1Ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;",
            "LX/1Ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2228213
    invoke-direct {p0}, LX/6lf;-><init>()V

    .line 2228214
    iput-object p1, p0, LX/FLq;->a:Landroid/content/Context;

    .line 2228215
    iput-object p2, p0, LX/FLq;->b:LX/0Ot;

    .line 2228216
    iput-object p3, p0, LX/FLq;->c:LX/1Ad;

    .line 2228217
    return-void
.end method


# virtual methods
.method public final b(Landroid/view/ViewGroup;)LX/6le;
    .locals 3

    .prologue
    .line 2228218
    iget-object v0, p0, LX/FLq;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2228219
    const v1, 0x7f0308d4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2228220
    new-instance v1, LX/FLp;

    invoke-direct {v1, v0}, LX/FLp;-><init>(Landroid/view/View;)V

    return-object v1
.end method
