.class public LX/Fcx;
.super LX/1Cv;
.source ""


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:Z

.field private final d:Landroid/graphics/drawable/Drawable;

.field private final e:Ljava/lang/String;

.field public f:Lcom/facebook/search/results/protocol/filters/FilterValue;

.field public g:Landroid/text/SpannableString;

.field public h:Ljava/lang/String;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2262960
    const-string v0, "city"

    const v1, 0x7f020964

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "employer"

    const v3, 0x7f02078b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "school"

    const v5, 0x7f02084d

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/Fcx;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;LX/0wM;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2262961
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2262962
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2262963
    iput-object v0, p0, LX/Fcx;->i:LX/0Px;

    .line 2262964
    invoke-static {p2}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fcx;->e:Ljava/lang/String;

    .line 2262965
    iput-object p3, p0, LX/Fcx;->h:Ljava/lang/String;

    .line 2262966
    iput-object p4, p0, LX/Fcx;->b:Landroid/content/Context;

    .line 2262967
    sget-object v0, LX/Fcx;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/Fcx;->c:Z

    .line 2262968
    iget-boolean v0, p0, LX/Fcx;->c:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/Fcx;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, -0xa76f01

    invoke-virtual {p5, v0, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/Fcx;->d:Landroid/graphics/drawable/Drawable;

    .line 2262969
    return-void

    .line 2262970
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2262971
    iget-object v0, p0, LX/Fcx;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312b2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/facebook/search/results/protocol/filters/FilterValue;
    .locals 1

    .prologue
    .line 2262972
    iget-object v0, p0, LX/Fcx;->i:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2262973
    check-cast p2, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2262974
    check-cast p3, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2262975
    iget-object v0, p0, LX/Fcx;->f:Lcom/facebook/search/results/protocol/filters/FilterValue;

    if-ne p2, v0, :cond_0

    .line 2262976
    iget-object v0, p0, LX/Fcx;->g:Landroid/text/SpannableString;

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262977
    iget-object v0, p0, LX/Fcx;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2262978
    const v0, 0x7f0e0896

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2262979
    :goto_0
    return-void

    .line 2262980
    :cond_0
    iget-object v0, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2262981
    if-eqz v0, :cond_1

    .line 2262982
    iget-object v0, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2262983
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262984
    iget-object v0, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2262985
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2262986
    const v0, 0x7f0e0893

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    goto :goto_0

    .line 2262987
    :cond_1
    iget-object v0, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2262988
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262989
    sget-object v0, LX/Fcx;->a:LX/0P1;

    iget-object v1, p0, LX/Fcx;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2262990
    const v0, 0x7f0e0893

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2262991
    iget-object v0, p0, LX/Fcx;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2262992
    invoke-virtual {p0, p1}, LX/Fcx;->a(I)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2262993
    const-wide/16 v0, 0x0

    return-wide v0
.end method
