.class public LX/Fqv;
.super LX/1qS;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Fqv;


# instance fields
.field private final a:LX/4VP;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/Fr0;LX/4VP;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294657
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "timeline_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 2294658
    iput-object p5, p0, LX/Fqv;->a:LX/4VP;

    .line 2294659
    return-void
.end method

.method public static a(LX/0QB;)LX/Fqv;
    .locals 9

    .prologue
    .line 2294644
    sget-object v0, LX/Fqv;->b:LX/Fqv;

    if-nez v0, :cond_1

    .line 2294645
    const-class v1, LX/Fqv;

    monitor-enter v1

    .line 2294646
    :try_start_0
    sget-object v0, LX/Fqv;->b:LX/Fqv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2294647
    if-eqz v2, :cond_0

    .line 2294648
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2294649
    new-instance v3, LX/Fqv;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v5

    check-cast v5, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v6

    check-cast v6, LX/1qU;

    invoke-static {v0}, LX/Fr0;->a(LX/0QB;)LX/Fr0;

    move-result-object v7

    check-cast v7, LX/Fr0;

    invoke-static {v0}, LX/4VP;->a(LX/0QB;)LX/4VP;

    move-result-object v8

    check-cast v8, LX/4VP;

    invoke-direct/range {v3 .. v8}, LX/Fqv;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/Fr0;LX/4VP;)V

    .line 2294650
    move-object v0, v3

    .line 2294651
    sput-object v0, LX/Fqv;->b:LX/Fqv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294652
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2294653
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2294654
    :cond_1
    sget-object v0, LX/Fqv;->b:LX/Fqv;

    return-object v0

    .line 2294655
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2294656
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()J
    .locals 6

    .prologue
    .line 2294661
    iget-object v0, p0, LX/Fqv;->a:LX/4VP;

    const-wide/32 v2, 0x200000

    const-wide/32 v4, 0x600000

    invoke-virtual {v0, v2, v3, v4, v5}, LX/4VP;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2294660
    const v0, 0xc800

    return v0
.end method
