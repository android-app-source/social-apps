.class public final LX/Fxw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/2h8;

.field public final synthetic c:LX/2hC;

.field public final synthetic d:LX/5P2;

.field public final synthetic e:LX/Fy4;


# direct methods
.method public constructor <init>(LX/Fy4;JLX/2h8;LX/2hC;LX/5P2;)V
    .locals 0

    .prologue
    .line 2306163
    iput-object p1, p0, LX/Fxw;->e:LX/Fy4;

    iput-wide p2, p0, LX/Fxw;->a:J

    iput-object p4, p0, LX/Fxw;->b:LX/2h8;

    iput-object p5, p0, LX/Fxw;->c:LX/2hC;

    iput-object p6, p0, LX/Fxw;->d:LX/5P2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    .line 2306164
    iget-object v0, p0, LX/Fxw;->e:LX/Fy4;

    iget-wide v2, p0, LX/Fxw;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306165
    iget-object v0, p0, LX/Fxw;->e:LX/Fy4;

    iget-boolean v0, v0, LX/Fy4;->n:Z

    if-nez v0, :cond_0

    .line 2306166
    iget-object v0, p0, LX/Fxw;->e:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hZ;

    const v7, 0x7f081594

    iget-object v1, p0, LX/Fxw;->e:LX/Fy4;

    iget-wide v2, p0, LX/Fxw;->a:J

    iget-object v4, p0, LX/Fxw;->b:LX/2h8;

    iget-object v5, p0, LX/Fxw;->c:LX/2hC;

    iget-object v6, p0, LX/Fxw;->d:LX/5P2;

    invoke-static/range {v1 .. v6}, LX/Fy4;->a(LX/Fy4;JLX/2h8;LX/2hC;LX/5P2;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, p1, v7, v1}, LX/2hZ;->a(Ljava/lang/Throwable;ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2306167
    :cond_0
    iget-object v0, p0, LX/Fxw;->e:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->lk_()V

    .line 2306168
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2306169
    iget-object v0, p0, LX/Fxw;->e:LX/Fy4;

    iget-wide v2, p0, LX/Fxw;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306170
    return-void
.end method
