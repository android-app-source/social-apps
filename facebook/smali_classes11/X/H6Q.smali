.class public final LX/H6Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/H83;

.field public final synthetic c:LX/H6T;


# direct methods
.method public constructor <init>(LX/H6T;Landroid/view/View;LX/H83;)V
    .locals 0

    .prologue
    .line 2426621
    iput-object p1, p0, LX/H6Q;->c:LX/H6T;

    iput-object p2, p0, LX/H6Q;->a:Landroid/view/View;

    iput-object p3, p0, LX/H6Q;->b:LX/H83;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, 0x7eb0bcec

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2426622
    iget-object v1, p0, LX/H6Q;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2426623
    iget-object v2, p0, LX/H6Q;->b:LX/H83;

    invoke-virtual {v2}, LX/H83;->E()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2426624
    iget-object v3, p0, LX/H6Q;->c:LX/H6T;

    iget-object v3, v3, LX/H6T;->q:LX/17d;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 2426625
    iget-object v3, p0, LX/H6Q;->c:LX/H6T;

    iget-object v3, v3, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2426626
    const v1, 0x19a4c55e

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
