.class public final LX/Fpr;
.super LX/1L8;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;)V
    .locals 0

    .prologue
    .line 2291940
    iput-object p1, p0, LX/Fpr;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-direct {p0}, LX/1L8;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 10

    .prologue
    .line 2291941
    check-cast p1, LX/1Za;

    .line 2291942
    iget-object v0, p0, LX/Fpr;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    const/4 v4, 0x0

    .line 2291943
    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v7

    .line 2291944
    iget-object v1, p1, LX/1Za;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2291945
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 2291946
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/1Za;->c:Ljava/lang/String;

    invoke-virtual {v7, v1, v2}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;)LX/16n;

    move-result-object v1

    .line 2291947
    if-nez v1, :cond_0

    .line 2291948
    iget-object v1, v0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v2, "timeline_page_like_fail"

    const-string v3, "Could not find a unit in SectionData to modify"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2291949
    :goto_0
    return-void

    .line 2291950
    :cond_0
    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_1

    .line 2291951
    iget-object v1, v0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v2, "timeline_page_like_fail"

    const-string v3, "Found unit is not a story"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2291952
    :cond_1
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2291953
    iget-object v2, p1, LX/1Za;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2291954
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 2291955
    if-ne v2, v1, :cond_2

    iget-object v3, p1, LX/1Za;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2291956
    :goto_1
    iget-object v1, v0, Lcom/facebook/timeline/BaseTimelineFragment;->B:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/967;

    iget-object v2, p1, LX/1Za;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "newsfeed_ufi"

    const/4 v8, 0x1

    new-instance v9, LX/Fpm;

    invoke-direct {v9, v0, v7}, LX/Fpm;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;LX/G4x;)V

    move-object v7, v4

    invoke-virtual/range {v1 .. v9}, LX/967;->a(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/1L9;)V

    goto :goto_0

    .line 2291957
    :cond_2
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    goto :goto_1
.end method
