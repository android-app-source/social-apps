.class public LX/FHp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FHp;


# instance fields
.field private final a:LX/0kb;

.field private final b:LX/2MM;

.field private final c:LX/2Mo;

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(LX/0kb;LX/2MM;LX/2Mo;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221109
    iput-object p1, p0, LX/FHp;->a:LX/0kb;

    .line 2221110
    iput-object p2, p0, LX/FHp;->b:LX/2MM;

    .line 2221111
    iput-object p3, p0, LX/FHp;->c:LX/2Mo;

    .line 2221112
    iput-object p4, p0, LX/FHp;->d:LX/0W3;

    .line 2221113
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 2221107
    const/4 v0, 0x2

    return v0
.end method

.method public static a(LX/0QB;)LX/FHp;
    .locals 7

    .prologue
    .line 2221023
    sget-object v0, LX/FHp;->e:LX/FHp;

    if-nez v0, :cond_1

    .line 2221024
    const-class v1, LX/FHp;

    monitor-enter v1

    .line 2221025
    :try_start_0
    sget-object v0, LX/FHp;->e:LX/FHp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221026
    if-eqz v2, :cond_0

    .line 2221027
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2221028
    new-instance p0, LX/FHp;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {v0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v4

    check-cast v4, LX/2MM;

    invoke-static {v0}, LX/2Mo;->a(LX/0QB;)LX/2Mo;

    move-result-object v5

    check-cast v5, LX/2Mo;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FHp;-><init>(LX/0kb;LX/2MM;LX/2Mo;LX/0W3;)V

    .line 2221029
    move-object v0, p0

    .line 2221030
    sput-object v0, LX/FHp;->e:LX/FHp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221031
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221032
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221033
    :cond_1
    sget-object v0, LX/FHp;->e:LX/FHp;

    return-object v0

    .line 2221034
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221035
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/FHp;J)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 2221090
    const/high16 v0, 0x100000

    iget-object v2, p0, LX/FHp;->d:LX/0W3;

    sget-wide v4, LX/0X5;->kr:J

    invoke-interface {v2, v4, v5, v1}, LX/0W4;->a(JI)I

    move-result v2

    mul-int/2addr v0, v2

    .line 2221091
    int-to-long v2, v0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_0

    move v0, v1

    .line 2221092
    :goto_0
    return v0

    .line 2221093
    :cond_0
    iget-object v2, p0, LX/FHp;->c:LX/2Mo;

    invoke-virtual {v2}, LX/2Mo;->b()LX/CMt;

    move-result-object v2

    .line 2221094
    iget-object v3, v2, LX/CMt;->b:LX/CMs;

    .line 2221095
    iget-object v2, v2, LX/CMt;->a:LX/CMv;

    .line 2221096
    sget-object v4, LX/CMs;->HIGH:LX/CMs;

    invoke-virtual {v3, v4}, LX/CMs;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 2221097
    invoke-virtual {v2}, LX/CMv;->name()Ljava/lang/String;

    invoke-virtual {v3}, LX/CMs;->name()Ljava/lang/String;

    .line 2221098
    if-eqz v4, :cond_1

    .line 2221099
    iget-object v3, p0, LX/FHp;->a:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2221100
    :goto_1
    sget-object v3, LX/FHo;->a:[I

    invoke-virtual {v2}, LX/CMv;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2221101
    :cond_1
    iget-object v0, p0, LX/FHp;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    const-wide/32 v2, 0xa00000

    cmp-long v0, p1, v2

    if-gtz v0, :cond_3

    move v0, v1

    .line 2221102
    goto :goto_0

    .line 2221103
    :cond_2
    const/high16 v0, 0x1900000

    goto :goto_1

    .line 2221104
    :pswitch_0
    int-to-long v2, v0

    cmp-long v0, p1, v2

    if-gtz v0, :cond_1

    move v0, v1

    .line 2221105
    goto :goto_0

    .line 2221106
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 2221078
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    cmp-long v0, v0, v4

    if-gtz v0, :cond_0

    move v0, v2

    .line 2221079
    :goto_0
    return v0

    .line 2221080
    :cond_0
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 2221081
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    .line 2221082
    iget-object v0, p0, LX/FHp;->b:LX/2MM;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2MM;->b(Landroid/net/Uri;)J

    move-result-wide v0

    .line 2221083
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    move v0, v2

    .line 2221084
    goto :goto_0

    .line 2221085
    :cond_1
    iget-object v3, p0, LX/FHp;->d:LX/0W3;

    sget-wide v4, LX/0X5;->kp:J

    invoke-interface {v3, v4, v5}, LX/0W4;->a(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2221086
    invoke-virtual {p1}, Lcom/facebook/ui/media/attachments/MediaResource;->c()I

    move-result v2

    int-to-float v2, v2

    iget-wide v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    long-to-float v3, v4

    div-float/2addr v2, v3

    .line 2221087
    long-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-long v0, v0

    .line 2221088
    invoke-static {p0, v0, v1}, LX/FHp;->a(LX/FHp;J)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2221089
    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;IZ)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2221044
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 2221045
    :goto_0
    return v0

    .line 2221046
    :cond_0
    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    .line 2221047
    const/4 v3, 0x0

    .line 2221048
    if-nez p3, :cond_1

    .line 2221049
    if-nez v0, :cond_1

    const/4 v3, 0x1

    .line 2221050
    :cond_1
    move v3, v3

    .line 2221051
    if-eqz v3, :cond_2

    move v0, v2

    .line 2221052
    goto :goto_0

    .line 2221053
    :cond_2
    if-eqz v0, :cond_3

    move v0, v1

    .line 2221054
    goto :goto_0

    .line 2221055
    :cond_3
    iget-wide v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 2221056
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_4

    move v0, v1

    .line 2221057
    goto :goto_0

    .line 2221058
    :cond_4
    int-to-long v6, p2

    cmp-long v0, v6, v4

    if-ltz v0, :cond_5

    move v0, v2

    .line 2221059
    goto :goto_0

    .line 2221060
    :cond_5
    const-wide/32 v6, 0x100000

    cmp-long v0, v4, v6

    if-gtz v0, :cond_6

    move v0, v2

    .line 2221061
    goto :goto_0

    .line 2221062
    :cond_6
    if-lez p2, :cond_7

    .line 2221063
    int-to-long v6, p2

    sub-long v6, v4, v6

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    div-long/2addr v6, v4

    long-to-int v0, v6

    .line 2221064
    const/16 v3, 0xa

    if-ge v0, v3, :cond_7

    move v0, v2

    .line 2221065
    goto :goto_0

    .line 2221066
    :cond_7
    iget-object v0, p0, LX/FHp;->c:LX/2Mo;

    invoke-virtual {v0}, LX/2Mo;->b()LX/CMt;

    move-result-object v0

    .line 2221067
    iget-object v3, v0, LX/CMt;->b:LX/CMs;

    .line 2221068
    iget-object v6, v0, LX/CMt;->a:LX/CMv;

    .line 2221069
    sget-object v0, LX/CMs;->HIGH:LX/CMs;

    invoke-virtual {v3, v0}, LX/CMs;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2221070
    invoke-virtual {v6}, LX/CMv;->name()Ljava/lang/String;

    invoke-virtual {v3}, LX/CMs;->name()Ljava/lang/String;

    .line 2221071
    if-eqz v0, :cond_8

    .line 2221072
    iget-object v0, p0, LX/FHp;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->h()Z

    move-result v0

    if-eqz v0, :cond_9

    const/high16 v0, 0x100000

    .line 2221073
    :goto_1
    sget-object v3, LX/FHo;->a:[I

    invoke-virtual {v6}, LX/CMv;->ordinal()I

    move-result v6

    aget v3, v3, v6

    packed-switch v3, :pswitch_data_0

    :cond_8
    move v0, v1

    .line 2221074
    goto :goto_0

    .line 2221075
    :cond_9
    const/high16 v0, 0x1000000

    goto :goto_1

    .line 2221076
    :pswitch_0
    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_8

    move v0, v2

    .line 2221077
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 2221036
    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 2221037
    :goto_0
    return v0

    .line 2221038
    :cond_0
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 2221039
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    .line 2221040
    iget-object v0, p0, LX/FHp;->b:LX/2MM;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2MM;->b(Landroid/net/Uri;)J

    move-result-wide v0

    .line 2221041
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    move v0, v2

    .line 2221042
    goto :goto_0

    .line 2221043
    :cond_1
    invoke-static {p0, v0, v1}, LX/FHp;->a(LX/FHp;J)Z

    move-result v0

    goto :goto_0
.end method
