.class public final LX/Ftw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2299312
    iput-object p1, p0, LX/Ftw;->b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    iput-object p2, p0, LX/Ftw;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2299313
    iget-object v0, p0, LX/Ftw;->b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->j:Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;

    iget-object v1, p0, LX/Ftw;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/favmediapicker/utils/FavoriteMediaSuggestionsDataFetcher;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
