.class public LX/Fc1;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261589
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261590
    return-void
.end method

.method public static a(LX/0QB;)LX/Fc1;
    .locals 3

    .prologue
    .line 2261591
    const-class v1, LX/Fc1;

    monitor-enter v1

    .line 2261592
    :try_start_0
    sget-object v0, LX/Fc1;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261593
    sput-object v2, LX/Fc1;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261594
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261595
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261596
    new-instance v0, LX/Fc1;

    invoke-direct {v0}, LX/Fc1;-><init>()V

    .line 2261597
    move-object v0, v0

    .line 2261598
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261599
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fc1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261600
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261601
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .locals 11
    .param p0    # Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261602
    if-nez p0, :cond_0

    .line 2261603
    const/4 v0, 0x0

    .line 2261604
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/5C5;

    invoke-direct {v0}, LX/5C5;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;->a()LX/0Px;

    move-result-object v1

    .line 2261605
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2261606
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2261607
    :goto_1
    move-object v1, v2

    .line 2261608
    iput-object v1, v0, LX/5C5;->a:LX/0Px;

    .line 2261609
    move-object v0, v0

    .line 2261610
    invoke-virtual {v0}, LX/5C5;->a()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    goto :goto_0

    .line 2261611
    :cond_1
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2261612
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    .line 2261613
    new-instance v6, LX/5C6;

    invoke-direct {v6}, LX/5C6;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->j()J

    move-result-wide v8

    .line 2261614
    iput-wide v8, v6, LX/5C6;->b:J

    .line 2261615
    move-object v6, v6

    .line 2261616
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->a()I

    move-result v7

    .line 2261617
    iput v7, v6, LX/5C6;->a:I

    .line 2261618
    move-object v6, v6

    .line 2261619
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->k()I

    move-result v7

    .line 2261620
    iput v7, v6, LX/5C6;->c:I

    .line 2261621
    move-object v6, v6

    .line 2261622
    invoke-virtual {v6}, LX/5C6;->a()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;

    move-result-object v6

    move-object v2, v6

    .line 2261623
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2261624
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2261625
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2261626
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2261627
    if-nez v1, :cond_1

    .line 2261628
    :cond_0
    :goto_0
    return-object v0

    .line 2261629
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2261630
    if-eqz v2, :cond_0

    .line 2261631
    new-instance v0, LX/8dV;

    invoke-direct {v0}, LX/8dV;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->j()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    .line 2261632
    iput-object v3, v0, LX/8dV;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2261633
    move-object v0, v0

    .line 2261634
    new-instance v3, LX/8dX;

    invoke-direct {v3}, LX/8dX;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    .line 2261635
    iput-object v4, v3, LX/8dX;->L:Ljava/lang/String;

    .line 2261636
    move-object v3, v3

    .line 2261637
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    .line 2261638
    iput-object v4, v3, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261639
    move-object v3, v3

    .line 2261640
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bF()J

    move-result-wide v4

    .line 2261641
    iput-wide v4, v3, LX/8dX;->r:J

    .line 2261642
    move-object v3, v3

    .line 2261643
    iput-object v2, v3, LX/8dX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2261644
    move-object v2, v3

    .line 2261645
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dN()Z

    move-result v3

    .line 2261646
    iput-boolean v3, v2, LX/8dX;->I:Z

    .line 2261647
    move-object v2, v2

    .line 2261648
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dR()I

    move-result v3

    .line 2261649
    iput v3, v2, LX/8dX;->J:I

    .line 2261650
    move-object v2, v2

    .line 2261651
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ez()Z

    move-result v3

    .line 2261652
    iput-boolean v3, v2, LX/8dX;->T:Z

    .line 2261653
    move-object v2, v2

    .line 2261654
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fh()I

    move-result v3

    .line 2261655
    iput v3, v2, LX/8dX;->Z:I

    .line 2261656
    move-object v2, v2

    .line 2261657
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 2261658
    if-nez v3, :cond_2

    .line 2261659
    const/4 v4, 0x0

    .line 2261660
    :goto_1
    move-object v3, v4

    .line 2261661
    iput-object v3, v2, LX/8dX;->ah:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 2261662
    move-object v2, v2

    .line 2261663
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->gC()I

    move-result v3

    .line 2261664
    iput v3, v2, LX/8dX;->ar:I

    .line 2261665
    move-object v2, v2

    .line 2261666
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->gE()I

    move-result v3

    .line 2261667
    iput v3, v2, LX/8dX;->as:I

    .line 2261668
    move-object v2, v2

    .line 2261669
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v3

    .line 2261670
    iput-object v3, v2, LX/8dX;->ab:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2261671
    move-object v2, v2

    .line 2261672
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->lf()I

    move-result v3

    .line 2261673
    iput v3, v2, LX/8dX;->bh:I

    .line 2261674
    move-object v2, v2

    .line 2261675
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->eJ()Z

    move-result v3

    .line 2261676
    iput-boolean v3, v2, LX/8dX;->V:Z

    .line 2261677
    move-object v2, v2

    .line 2261678
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hr()Ljava/lang/String;

    move-result-object v3

    .line 2261679
    iput-object v3, v2, LX/8dX;->ax:Ljava/lang/String;

    .line 2261680
    move-object v2, v2

    .line 2261681
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ee()I

    move-result v3

    .line 2261682
    iput v3, v2, LX/8dX;->O:I

    .line 2261683
    move-object v2, v2

    .line 2261684
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ef()I

    move-result v3

    .line 2261685
    iput v3, v2, LX/8dX;->P:I

    .line 2261686
    move-object v2, v2

    .line 2261687
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->eg()I

    move-result v3

    .line 2261688
    iput v3, v2, LX/8dX;->Q:I

    .line 2261689
    move-object v2, v2

    .line 2261690
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iR()Ljava/lang/String;

    move-result-object v3

    .line 2261691
    iput-object v3, v2, LX/8dX;->aK:Ljava/lang/String;

    .line 2261692
    move-object v2, v2

    .line 2261693
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iQ()Ljava/lang/String;

    move-result-object v3

    .line 2261694
    iput-object v3, v2, LX/8dX;->aJ:Ljava/lang/String;

    .line 2261695
    move-object v2, v2

    .line 2261696
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iS()I

    move-result v3

    .line 2261697
    iput v3, v2, LX/8dX;->aL:I

    .line 2261698
    move-object v2, v2

    .line 2261699
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iP()D

    move-result-wide v4

    .line 2261700
    iput-wide v4, v2, LX/8dX;->aI:D

    .line 2261701
    move-object v2, v2

    .line 2261702
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iO()D

    move-result-wide v4

    .line 2261703
    iput-wide v4, v2, LX/8dX;->aH:D

    .line 2261704
    move-object v2, v2

    .line 2261705
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dJ()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v1

    invoke-static {v1}, LX/Fc1;->a(Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v1

    .line 2261706
    iput-object v1, v2, LX/8dX;->H:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 2261707
    move-object v1, v2

    .line 2261708
    invoke-virtual {v1}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v1

    .line 2261709
    iput-object v1, v0, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2261710
    move-object v0, v0

    .line 2261711
    goto/16 :goto_0

    :cond_2
    new-instance v4, LX/A4w;

    invoke-direct {v4}, LX/A4w;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v5

    .line 2261712
    iput-boolean v5, v4, LX/A4w;->b:Z

    .line 2261713
    move-object v4, v4

    .line 2261714
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    .line 2261715
    iput-object v5, v4, LX/A4w;->c:Ljava/lang/String;

    .line 2261716
    move-object v4, v4

    .line 2261717
    invoke-virtual {v4}, LX/A4w;->a()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v4

    goto/16 :goto_1
.end method
