.class public final enum LX/FOD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FOD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FOD;

.field public static final enum FAB:LX/FOD;

.field public static final enum FLOWER_MESSAGE_PICKER:LX/FOD;

.field public static final enum MONTAGE_VIEWER:LX/FOD;

.field public static final enum OMNI_PICKER:LX/FOD;

.field public static final enum OTHER:LX/FOD;

.field public static final enum ROOM_CREATE:LX/FOD;

.field public static final enum ROOM_CREATE_WITH_SHARE_SHEET:LX/FOD;

.field public static final enum SINGLE_PICKER:LX/FOD;

.field public static final enum THREAD_SETTINGS:LX/FOD;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2234886
    new-instance v0, LX/FOD;

    const-string v1, "THREAD_SETTINGS"

    invoke-direct {v0, v1, v3}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->THREAD_SETTINGS:LX/FOD;

    .line 2234887
    new-instance v0, LX/FOD;

    const-string v1, "SINGLE_PICKER"

    invoke-direct {v0, v1, v4}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->SINGLE_PICKER:LX/FOD;

    .line 2234888
    new-instance v0, LX/FOD;

    const-string v1, "MONTAGE_VIEWER"

    invoke-direct {v0, v1, v5}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->MONTAGE_VIEWER:LX/FOD;

    .line 2234889
    new-instance v0, LX/FOD;

    const-string v1, "OMNI_PICKER"

    invoke-direct {v0, v1, v6}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->OMNI_PICKER:LX/FOD;

    .line 2234890
    new-instance v0, LX/FOD;

    const-string v1, "FLOWER_MESSAGE_PICKER"

    invoke-direct {v0, v1, v7}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->FLOWER_MESSAGE_PICKER:LX/FOD;

    .line 2234891
    new-instance v0, LX/FOD;

    const-string v1, "FAB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->FAB:LX/FOD;

    .line 2234892
    new-instance v0, LX/FOD;

    const-string v1, "ROOM_CREATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->ROOM_CREATE:LX/FOD;

    .line 2234893
    new-instance v0, LX/FOD;

    const-string v1, "ROOM_CREATE_WITH_SHARE_SHEET"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->ROOM_CREATE_WITH_SHARE_SHEET:LX/FOD;

    .line 2234894
    new-instance v0, LX/FOD;

    const-string v1, "OTHER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/FOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOD;->OTHER:LX/FOD;

    .line 2234895
    const/16 v0, 0x9

    new-array v0, v0, [LX/FOD;

    sget-object v1, LX/FOD;->THREAD_SETTINGS:LX/FOD;

    aput-object v1, v0, v3

    sget-object v1, LX/FOD;->SINGLE_PICKER:LX/FOD;

    aput-object v1, v0, v4

    sget-object v1, LX/FOD;->MONTAGE_VIEWER:LX/FOD;

    aput-object v1, v0, v5

    sget-object v1, LX/FOD;->OMNI_PICKER:LX/FOD;

    aput-object v1, v0, v6

    sget-object v1, LX/FOD;->FLOWER_MESSAGE_PICKER:LX/FOD;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FOD;->FAB:LX/FOD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FOD;->ROOM_CREATE:LX/FOD;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FOD;->ROOM_CREATE_WITH_SHARE_SHEET:LX/FOD;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FOD;->OTHER:LX/FOD;

    aput-object v2, v0, v1

    sput-object v0, LX/FOD;->$VALUES:[LX/FOD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2234896
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FOD;
    .locals 1

    .prologue
    .line 2234897
    const-class v0, LX/FOD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FOD;

    return-object v0
.end method

.method public static values()[LX/FOD;
    .locals 1

    .prologue
    .line 2234898
    sget-object v0, LX/FOD;->$VALUES:[LX/FOD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FOD;

    return-object v0
.end method
