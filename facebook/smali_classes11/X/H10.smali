.class public LX/H10;
.super LX/H0w;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/H0w",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V
    .locals 1

    .prologue
    .line 2414694
    invoke-direct/range {p0 .. p6}, LX/H0w;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    .line 2414695
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414696
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2414697
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2414689
    invoke-super {p0, p1, p2}, LX/H0w;->a(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 2414690
    const v0, 0x7f0d1c12

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    .line 2414691
    const/16 v1, 0x1002

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setInputType(I)V

    .line 2414692
    return-void
.end method

.method public final b(LX/4gq;)Z
    .locals 2

    .prologue
    .line 2414693
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->o(J)Z

    move-result v0

    return v0
.end method

.method public final c(LX/4gq;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2414684
    iget-wide v0, p0, LX/H0w;->h:J

    .line 2414685
    invoke-virtual {p1, v0, v1}, LX/4gq;->o(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2414686
    invoke-virtual {p1, v0, v1}, LX/4gq;->p(J)J

    move-result-wide v2

    .line 2414687
    :goto_0
    move-wide v0, v2

    .line 2414688
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p1, v0, v1}, LX/4gq;->q(J)J

    move-result-wide v2

    goto :goto_0
.end method

.method public final d(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414683
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->p(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final e(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414682
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->q(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final f(LX/4gq;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2414677
    iget-wide v0, p0, LX/H0w;->h:J

    .line 2414678
    iget-object v2, p1, LX/4gq;->c:LX/0W4;

    if-eqz v2, :cond_0

    .line 2414679
    iget-object v2, p1, LX/4gq;->c:LX/0W4;

    invoke-interface {v2, v0, v1}, LX/0W4;->d(J)J

    move-result-wide v2

    .line 2414680
    :goto_0
    move-wide v0, v2

    .line 2414681
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0, v1}, LX/0ok;->b(J)J

    move-result-wide v2

    goto :goto_0
.end method

.method public final g(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414676
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-static {v0, v1}, LX/0ok;->b(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
