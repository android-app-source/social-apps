.class public LX/H0s;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field public c:LX/H0q;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/H0K;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2414422
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2414423
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/H0s;->b:Z

    .line 2414424
    iput-object p1, p0, LX/H0s;->a:Landroid/content/Context;

    .line 2414425
    return-void
.end method

.method public static c(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 3

    .prologue
    .line 2414414
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    move-object v0, v0

    .line 2414415
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2414416
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2414417
    if-eqz v0, :cond_0

    .line 2414418
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2414419
    const/16 v2, 0x96

    if-le v0, v2, :cond_0

    .line 2414420
    new-instance v0, LX/Gzs;

    const-string v1, "At least one field overflowed"

    invoke-direct {v0, v1}, LX/Gzs;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2414421
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2414397
    sget-object v0, LX/H0r;->TITLE:LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2414398
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a8

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414399
    new-instance v0, LX/H0Q;

    invoke-direct {v0, v1}, LX/H0Q;-><init>(Landroid/view/View;)V

    .line 2414400
    :goto_0
    return-object v0

    .line 2414401
    :cond_0
    sget-object v0, LX/H0r;->DESCRIPTION:LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2414402
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a6

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414403
    new-instance v0, LX/H0R;

    invoke-direct {v0, v1}, LX/H0R;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2414404
    :cond_1
    sget-object v0, LX/H0r;->FIELD_LABEL:LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2414405
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a2

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414406
    new-instance v0, LX/H0R;

    invoke-direct {v0, v1}, LX/H0R;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2414407
    :cond_2
    sget-object v0, LX/H0r;->FIELD_EDIT_TEXT:LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 2414408
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a3

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414409
    new-instance v0, LX/H0k;

    invoke-direct {v0, p0, v1}, LX/H0k;-><init>(LX/H0s;Landroid/view/View;)V

    goto :goto_0

    .line 2414410
    :cond_3
    sget-object v0, LX/H0r;->FIELD_BUTTON:LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_4

    .line 2414411
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03079f

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414412
    new-instance v0, LX/H0m;

    invoke-direct {v0, p0, v1}, LX/H0m;-><init>(LX/H0s;Landroid/view/View;)V

    goto :goto_0

    .line 2414413
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2414348
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2414349
    sget-object v1, LX/H0r;->TITLE:LX/H0r;

    invoke-virtual {v1}, LX/H0r;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2414350
    check-cast p1, LX/H0Q;

    .line 2414351
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08151f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414352
    invoke-virtual {p1, v0, v6}, LX/H0Q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2414353
    :goto_0
    return-void

    .line 2414354
    :cond_0
    sget-object v1, LX/H0r;->DESCRIPTION:LX/H0r;

    invoke-virtual {v1}, LX/H0r;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2414355
    check-cast p1, LX/H0R;

    .line 2414356
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081520

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414357
    new-array v1, v7, [Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/H0R;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2414358
    :cond_1
    sget-object v1, LX/H0r;->FIELD_LABEL:LX/H0r;

    invoke-virtual {v1}, LX/H0r;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2414359
    check-cast p1, LX/H0R;

    .line 2414360
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081521

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414361
    iget-object v1, p0, LX/H0s;->c:LX/H0q;

    invoke-virtual {v1, p2}, LX/H0q;->c(I)I

    move-result v1

    .line 2414362
    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v8

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/H0R;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2414363
    :cond_2
    sget-object v1, LX/H0r;->FIELD_EDIT_TEXT:LX/H0r;

    invoke-virtual {v1}, LX/H0r;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_7

    move-object v0, p1

    .line 2414364
    check-cast v0, LX/H0j;

    .line 2414365
    iget-object v1, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081522

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2414366
    iget-object v1, p0, LX/H0s;->c:LX/H0q;

    invoke-virtual {v1, p2}, LX/H0q;->c(I)I

    move-result v1

    .line 2414367
    iget-object v2, p0, LX/H0s;->c:LX/H0q;

    invoke-virtual {v2, v1}, LX/H0q;->b(I)Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    move-result-object v2

    .line 2414368
    iget-object v4, v2, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2414369
    iget-object v5, p0, LX/H0s;->c:LX/H0q;

    .line 2414370
    iget-object p1, v5, LX/H0q;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    move v5, p1

    .line 2414371
    if-le v5, v7, :cond_4

    move v5, v7

    .line 2414372
    :goto_1
    if-eqz v5, :cond_5

    move-object v5, v6

    .line 2414373
    :goto_2
    iput v1, v0, LX/H0j;->l:I

    .line 2414374
    iput-object v2, v0, LX/H0j;->p:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2414375
    iget-object p1, v0, LX/H0j;->m:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {p1, v3}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2414376
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_9

    .line 2414377
    iget-object p1, v0, LX/H0j;->m:Lcom/facebook/widget/text/BetterEditTextView;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2414378
    :goto_3
    iget-object p1, v0, LX/H0j;->m:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {p1, v5}, Lcom/facebook/widget/text/BetterEditTextView;->setRightDrawableVisibility(Ljava/lang/Boolean;)V

    .line 2414379
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 2414380
    invoke-virtual {v0, v2}, LX/H0j;->d(I)V

    .line 2414381
    iget-boolean v2, p0, LX/H0s;->b:Z

    if-eqz v2, :cond_6

    if-ne v1, v7, :cond_6

    move v1, v7

    .line 2414382
    :goto_4
    if-eqz v1, :cond_3

    iget-object v1, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08152c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2414383
    :cond_3
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2414384
    iget-object v1, v0, LX/H0j;->n:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2414385
    :goto_5
    goto/16 :goto_0

    :cond_4
    move v5, v8

    .line 2414386
    goto :goto_1

    .line 2414387
    :cond_5
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_2

    :cond_6
    move v1, v8

    .line 2414388
    goto :goto_4

    .line 2414389
    :cond_7
    sget-object v1, LX/H0r;->FIELD_BUTTON:LX/H0r;

    invoke-virtual {v1}, LX/H0r;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_8

    .line 2414390
    check-cast p1, LX/H0l;

    .line 2414391
    iget-object v0, p0, LX/H0s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081523

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414392
    iget-object v1, p1, LX/H0l;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2414393
    goto/16 :goto_0

    .line 2414394
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2414395
    :cond_9
    iget-object p1, v0, LX/H0j;->m:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {p1, v4}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2414396
    :cond_a
    iget-object v1, v0, LX/H0j;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method public final a(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 2

    .prologue
    .line 2414343
    iput-object p1, p0, LX/H0s;->d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2414344
    new-instance v0, LX/H0q;

    iget-object v1, p0, LX/H0s;->d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2414345
    iget-object p1, v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    move-object v1, p1

    .line 2414346
    invoke-direct {v0, v1}, LX/H0q;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LX/H0s;->c:LX/H0q;

    .line 2414347
    return-void
.end method

.method public final b(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 4

    .prologue
    .line 2414327
    iget-object v0, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    move-object v1, v0

    .line 2414328
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2414329
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2414330
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2414331
    const-string v3, ""

    .line 2414332
    iput-object v3, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    .line 2414333
    goto :goto_0

    .line 2414334
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2414335
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2414336
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2414337
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/H0s;->b:Z

    .line 2414338
    return-void

    .line 2414339
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/H0s;->b:Z

    .line 2414340
    iget-object v0, p0, LX/H0s;->e:LX/H0K;

    if-eqz v0, :cond_4

    .line 2414341
    iget-object v0, p0, LX/H0s;->e:LX/H0K;

    invoke-virtual {v0}, LX/H0K;->a()V

    .line 2414342
    :cond_4
    new-instance v0, LX/Gzr;

    const-string v1, "At least Question 1 is required"

    invoke-direct {v0, v1}, LX/Gzr;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2414323
    iget-object v0, p0, LX/H0s;->c:LX/H0q;

    invoke-virtual {v0, p1}, LX/H0q;->a(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2414324
    iget-object v0, p0, LX/H0s;->c:LX/H0q;

    if-nez v0, :cond_0

    .line 2414325
    const/4 v0, 0x0

    .line 2414326
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/H0s;->c:LX/H0q;

    invoke-virtual {v0}, LX/H0q;->a()I

    move-result v0

    goto :goto_0
.end method
