.class public final LX/GTN;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/auth/reauth/ReauthActivity;

.field public final synthetic b:LX/GTO;


# direct methods
.method public constructor <init>(LX/GTO;Lcom/facebook/auth/reauth/ReauthActivity;)V
    .locals 0

    .prologue
    .line 2355458
    iput-object p1, p0, LX/GTN;->b:LX/GTO;

    iput-object p2, p0, LX/GTN;->a:Lcom/facebook/auth/reauth/ReauthActivity;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2355459
    iget-object v0, p0, LX/GTN;->a:Lcom/facebook/auth/reauth/ReauthActivity;

    invoke-virtual {v0}, Lcom/facebook/auth/reauth/ReauthActivity;->b()V

    .line 2355460
    const-string v1, ""

    .line 2355461
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 2355462
    instance-of v2, v0, LX/2Oo;

    if-eqz v2, :cond_0

    .line 2355463
    check-cast v0, LX/2Oo;

    .line 2355464
    invoke-virtual {v0}, LX/2Oo;->b()Ljava/lang/String;

    move-result-object v1

    .line 2355465
    invoke-virtual {v0}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v0

    .line 2355466
    :goto_0
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/GTN;->a:Lcom/facebook/auth/reauth/ReauthActivity;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080177

    new-instance v2, LX/GTM;

    invoke-direct {v2, p0}, LX/GTM;-><init>(LX/GTN;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2355467
    return-void

    .line 2355468
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2355469
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2355470
    iget-object v0, p0, LX/GTN;->a:Lcom/facebook/auth/reauth/ReauthActivity;

    invoke-virtual {v0}, Lcom/facebook/auth/reauth/ReauthActivity;->finish()V

    .line 2355471
    const-class v0, Lcom/facebook/auth/component/ReauthResult;

    invoke-virtual {p1, v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/facebook/auth/component/ReauthResult;

    .line 2355472
    new-instance v0, LX/GTP;

    .line 2355473
    iget-object v1, v4, Lcom/facebook/auth/component/ReauthResult;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2355474
    iget-wide v7, v4, Lcom/facebook/auth/component/ReauthResult;->b:J

    move-wide v2, v7

    .line 2355475
    iget-wide v7, v4, Lcom/facebook/auth/component/ReauthResult;->c:J

    move-wide v4, v7

    .line 2355476
    iget-object v6, p0, LX/GTN;->b:LX/GTO;

    iget-object v6, v6, LX/GTO;->c:LX/0SG;

    invoke-direct/range {v0 .. v6}, LX/GTP;-><init>(Ljava/lang/String;JJLX/0SG;)V

    .line 2355477
    iget-object v1, p0, LX/GTN;->b:LX/GTO;

    iget-object v1, v1, LX/GTO;->f:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2355478
    return-void
.end method
