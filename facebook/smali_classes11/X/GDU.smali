.class public final LX/GDU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/A8v;

.field public final synthetic b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic c:LX/GDY;


# direct methods
.method public constructor <init>(LX/GDY;LX/A8v;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 0

    .prologue
    .line 2329862
    iput-object p1, p0, LX/GDU;->c:LX/GDY;

    iput-object p2, p0, LX/GDU;->a:LX/A8v;

    iput-object p3, p0, LX/GDU;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 11

    .prologue
    .line 2329863
    iget-object v0, p0, LX/GDU;->c:LX/GDY;

    iget-object v0, v0, LX/GDY;->d:LX/GG3;

    iget-object v1, p0, LX/GDU;->a:LX/A8v;

    iget-object v2, p0, LX/GDU;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2329864
    sget-object v3, LX/GG2;->a:[I

    invoke-virtual {v1}, LX/A8v;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2329865
    :goto_0
    iget-object v0, p0, LX/GDU;->c:LX/GDY;

    invoke-virtual {v0, p1}, LX/GDY;->a(Ljava/lang/Throwable;)V

    .line 2329866
    return-void

    .line 2329867
    :pswitch_0
    const/4 v9, 0x0

    .line 2329868
    const-string v7, "client_pause_fail"

    const-string v8, "edit"

    move-object v5, v0

    move-object v6, v2

    move-object v10, v9

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329869
    goto :goto_0

    .line 2329870
    :pswitch_1
    const/4 v9, 0x0

    .line 2329871
    const-string v7, "client_resume_fail"

    const-string v8, "edit"

    move-object v5, v0

    move-object v6, v2

    move-object v10, v9

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329872
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2329873
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2329874
    iget-object v0, p0, LX/GDU;->c:LX/GDY;

    iget-object v0, v0, LX/GDY;->d:LX/GG3;

    iget-object v1, p0, LX/GDU;->a:LX/A8v;

    iget-object v2, p0, LX/GDU;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2329875
    sget-object v3, LX/GG2;->a:[I

    invoke-virtual {v1}, LX/A8v;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2329876
    :goto_0
    iget-object v0, p0, LX/GDU;->c:LX/GDY;

    invoke-virtual {v0, p1}, LX/GDY;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2329877
    return-void

    .line 2329878
    :pswitch_0
    const/4 v9, 0x0

    .line 2329879
    const-string v7, "client_pause_success"

    const-string v8, "edit"

    move-object v5, v0

    move-object v6, v2

    move-object v10, v9

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329880
    goto :goto_0

    .line 2329881
    :pswitch_1
    const/4 v9, 0x0

    .line 2329882
    const-string v7, "client_resume_success"

    const-string v8, "edit"

    move-object v5, v0

    move-object v6, v2

    move-object v10, v9

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329883
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
