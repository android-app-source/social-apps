.class public LX/Gzb;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2412160
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Gzb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2412161
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2412158
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Gzb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2412159
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2412153
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2412154
    const v0, 0x7f030f02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2412155
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gzb;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2412156
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gzb;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2412157
    return-void
.end method
