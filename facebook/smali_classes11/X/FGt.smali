.class public final LX/FGt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2219603
    iput-object p1, p0, LX/FGt;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FGt;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p3, p0, LX/FGt;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2219604
    sget-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v1, "GetFbid from server failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/FGt;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v4, v4, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2219605
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219593
    check-cast p1, Ljava/lang/String;

    .line 2219594
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2219595
    if-eqz v0, :cond_1

    .line 2219596
    iget-object v0, p0, LX/FGt;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->g:LX/2ML;

    iget-object v1, p0, LX/FGt;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/2ML;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    .line 2219597
    iget-object v0, p0, LX/FGt;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/6ed;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 2219598
    iget-object v1, p0, LX/FGt;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->G:LX/0QI;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2219599
    iget-object v0, p0, LX/FGt;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FGt;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v2, p0, LX/FGt;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    new-instance v3, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    invoke-direct {v3, p1}, Lcom/facebook/ui/media/attachments/MediaUploadResult;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/FH8;->SKIPPED_FROM_SERVER:LX/FH8;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FH8;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2219600
    iget-object v0, p0, LX/FGt;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    iget-object v1, p0, LX/FGt;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/2MT;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2219601
    :cond_0
    :goto_1
    return-void

    .line 2219602
    :cond_1
    iget-object v0, p0, LX/FGt;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v0, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->g:LX/2ML;

    iget-object v1, p0, LX/FGt;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/2ML;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
