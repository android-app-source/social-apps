.class public final enum LX/F71;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F71;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F71;

.field public static final enum CONTACT:LX/F71;

.field public static final enum CONTACTS_HEADER:LX/F71;

.field public static final enum FAILURE:LX/F71;

.field public static final enum LOADING:LX/F71;

.field public static final enum MANAGE_CONTACTS:LX/F71;

.field public static final enum PYMK:LX/F71;

.field public static final enum PYMK_HEADER:LX/F71;

.field public static final enum UNRESPONDED_CONTACT:LX/F71;

.field public static final enum UNRESPONDED_PYMK:LX/F71;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2201062
    new-instance v0, LX/F71;

    const-string v1, "CONTACTS_HEADER"

    invoke-direct {v0, v1, v3}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->CONTACTS_HEADER:LX/F71;

    .line 2201063
    new-instance v0, LX/F71;

    const-string v1, "UNRESPONDED_CONTACT"

    invoke-direct {v0, v1, v4}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->UNRESPONDED_CONTACT:LX/F71;

    .line 2201064
    new-instance v0, LX/F71;

    const-string v1, "CONTACT"

    invoke-direct {v0, v1, v5}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->CONTACT:LX/F71;

    .line 2201065
    new-instance v0, LX/F71;

    const-string v1, "MANAGE_CONTACTS"

    invoke-direct {v0, v1, v6}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->MANAGE_CONTACTS:LX/F71;

    .line 2201066
    new-instance v0, LX/F71;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v7}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->LOADING:LX/F71;

    .line 2201067
    new-instance v0, LX/F71;

    const-string v1, "FAILURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->FAILURE:LX/F71;

    .line 2201068
    new-instance v0, LX/F71;

    const-string v1, "PYMK_HEADER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->PYMK_HEADER:LX/F71;

    .line 2201069
    new-instance v0, LX/F71;

    const-string v1, "UNRESPONDED_PYMK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->UNRESPONDED_PYMK:LX/F71;

    .line 2201070
    new-instance v0, LX/F71;

    const-string v1, "PYMK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/F71;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F71;->PYMK:LX/F71;

    .line 2201071
    const/16 v0, 0x9

    new-array v0, v0, [LX/F71;

    sget-object v1, LX/F71;->CONTACTS_HEADER:LX/F71;

    aput-object v1, v0, v3

    sget-object v1, LX/F71;->UNRESPONDED_CONTACT:LX/F71;

    aput-object v1, v0, v4

    sget-object v1, LX/F71;->CONTACT:LX/F71;

    aput-object v1, v0, v5

    sget-object v1, LX/F71;->MANAGE_CONTACTS:LX/F71;

    aput-object v1, v0, v6

    sget-object v1, LX/F71;->LOADING:LX/F71;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/F71;->FAILURE:LX/F71;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/F71;->PYMK_HEADER:LX/F71;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/F71;->UNRESPONDED_PYMK:LX/F71;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/F71;->PYMK:LX/F71;

    aput-object v2, v0, v1

    sput-object v0, LX/F71;->$VALUES:[LX/F71;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2201072
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F71;
    .locals 1

    .prologue
    .line 2201073
    const-class v0, LX/F71;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F71;

    return-object v0
.end method

.method public static values()[LX/F71;
    .locals 1

    .prologue
    .line 2201074
    sget-object v0, LX/F71;->$VALUES:[LX/F71;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F71;

    return-object v0
.end method
