.class public LX/FYJ;
.super LX/FXy;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255965
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255966
    iput-object p1, p0, LX/FYJ;->a:Landroid/content/Context;

    .line 2255967
    iput-object p2, p0, LX/FYJ;->b:LX/0Ot;

    .line 2255968
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2255954
    iget-object v0, p0, LX/FYJ;->c:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->POST:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    if-ne v0, v1, :cond_0

    const v0, 0x7f020984

    :goto_0
    return v0

    :cond_0
    invoke-static {}, LX/10A;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 1

    .prologue
    .line 2255961
    invoke-interface {p1}, LX/BO1;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    move-result-object v0

    iput-object v0, p0, LX/FYJ;->c:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    .line 2255962
    invoke-static {p1}, LX/FZ3;->b(LX/BO1;)Z

    move-result v0

    .line 2255963
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255964
    return-object p0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2255958
    iget-object v0, p0, LX/FYJ;->c:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->POST:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    if-ne v0, v1, :cond_0

    .line 2255959
    const v0, 0x7f081acc

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2255960
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f081acd

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255957
    const-string v0, "view_post"

    return-object v0
.end method

.method public final b(LX/BO1;)Z
    .locals 5

    .prologue
    .line 2255955
    iget-object v0, p0, LX/FYJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FZ3;

    iget-object v1, p0, LX/FYJ;->a:Landroid/content/Context;

    invoke-interface {p1}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/BO1;->y()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/BO1;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/FZ3;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2255956
    const/4 v0, 0x1

    return v0
.end method
