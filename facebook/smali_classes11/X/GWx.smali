.class public LX/GWx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0am;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2362920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2362921
    iput-object p1, p0, LX/GWx;->a:LX/0am;

    .line 2362922
    iput-object p2, p0, LX/GWx;->b:LX/0am;

    .line 2362923
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2362924
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/GWx;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2362925
    :cond_1
    :goto_0
    return v0

    .line 2362926
    :cond_2
    if-eq p0, p1, :cond_1

    .line 2362927
    check-cast p1, LX/GWx;

    .line 2362928
    iget-object v2, p0, LX/GWx;->a:LX/0am;

    .line 2362929
    iget-object v3, p1, LX/GWx;->a:LX/0am;

    move-object v3, v3

    .line 2362930
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GWx;->b:LX/0am;

    .line 2362931
    iget-object v3, p1, LX/GWx;->b:LX/0am;

    move-object v3, v3

    .line 2362932
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2362933
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/GWx;->a:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/GWx;->b:LX/0am;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
