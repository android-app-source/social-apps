.class public LX/F0J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/F0J;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F0H;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0SG;

.field public c:LX/F0K;

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/0ad;

.field public f:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackSettingsGraphQLModels$ThrowbackSettingsSubscriptionMutationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:LX/F0I;


# direct methods
.method public constructor <init>(LX/F0K;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2188431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2188432
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/F0J;->a:Ljava/util/List;

    .line 2188433
    new-instance v0, LX/F0G;

    invoke-direct {v0, p0}, LX/F0G;-><init>(LX/F0J;)V

    iput-object v0, p0, LX/F0J;->f:LX/0Vd;

    .line 2188434
    iput-object p1, p0, LX/F0J;->c:LX/F0K;

    .line 2188435
    iput-object p2, p0, LX/F0J;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2188436
    iput-object p3, p0, LX/F0J;->b:LX/0SG;

    .line 2188437
    iput-object p4, p0, LX/F0J;->e:LX/0ad;

    .line 2188438
    return-void
.end method

.method public static a(LX/0QB;)LX/F0J;
    .locals 7

    .prologue
    .line 2188391
    sget-object v0, LX/F0J;->h:LX/F0J;

    if-nez v0, :cond_1

    .line 2188392
    const-class v1, LX/F0J;

    monitor-enter v1

    .line 2188393
    :try_start_0
    sget-object v0, LX/F0J;->h:LX/F0J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2188394
    if-eqz v2, :cond_0

    .line 2188395
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2188396
    new-instance p0, LX/F0J;

    .line 2188397
    new-instance v5, LX/F0K;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {v5, v3, v4}, LX/F0K;-><init>(LX/0tX;LX/1Ck;)V

    .line 2188398
    move-object v3, v5

    .line 2188399
    check-cast v3, LX/F0K;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/F0J;-><init>(LX/F0K;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0ad;)V

    .line 2188400
    move-object v0, p0

    .line 2188401
    sput-object v0, LX/F0J;->h:LX/F0J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2188402
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2188403
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2188404
    :cond_1
    sget-object v0, LX/F0J;->h:LX/F0J;

    return-object v0

    .line 2188405
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2188406
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(ZLX/F0H;)V
    .locals 7

    .prologue
    .line 2188407
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F0J;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2188408
    iget-object v0, p0, LX/F0J;->c:LX/F0K;

    iget-object v1, p0, LX/F0J;->f:LX/0Vd;

    .line 2188409
    iget-object v2, v0, LX/F0K;->b:LX/1Ck;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2188410
    if-eqz p1, :cond_0

    .line 2188411
    const-string v4, "SUBSCRIBED_ALL"

    .line 2188412
    :goto_0
    new-instance v5, LX/4Jn;

    invoke-direct {v5}, LX/4Jn;-><init>()V

    .line 2188413
    const-string v6, "subscription_status"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2188414
    move-object v4, v5

    .line 2188415
    new-instance v5, LX/399;

    .line 2188416
    new-instance v6, LX/F0b;

    invoke-direct {v6}, LX/F0b;-><init>()V

    move-object v6, v6

    .line 2188417
    const-string p2, "input"

    invoke-virtual {v6, p2, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/F0b;

    invoke-direct {v5, v4}, LX/399;-><init>(LX/0zP;)V

    .line 2188418
    iget-object v4, v0, LX/F0K;->a:LX/0tX;

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v4, v4

    .line 2188419
    invoke-virtual {v2, v3, v4, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2188420
    monitor-exit p0

    return-void

    .line 2188421
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2188422
    :cond_0
    const-string v4, "UNSUBSCRIBED"

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 2188423
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F0J;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/F0C;->b:LX/0Tn;

    iget-object v2, p0, LX/F0J;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2188424
    monitor-exit p0

    return-void

    .line 2188425
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 6

    .prologue
    .line 2188426
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F0J;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/F0C;->b:LX/0Tn;

    const-wide/high16 v2, -0x8000000000000000L

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 2188427
    iget-object v2, p0, LX/F0J;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    const-wide v4, 0x9a7ec800L

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2188428
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2188429
    iget-object v0, p0, LX/F0J;->g:LX/F0I;

    sget-object v1, LX/F0I;->STATUS_SUBSCRIBED_ALL:LX/F0I;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/F0J;->g:LX/F0I;

    sget-object v1, LX/F0I;->STATUS_SUBSCRIBED_HIGHLIGHTS:LX/F0I;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 2188430
    iget-object v0, p0, LX/F0J;->e:LX/0ad;

    sget-short v1, LX/1Cs;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
