.class public final LX/GgO;
.super Landroid/view/View;
.source ""


# instance fields
.field private final A:Z

.field private B:LX/GgP;

.field private a:Landroid/graphics/Paint;

.field public b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:Landroid/graphics/Paint;

.field private g:I

.field private final h:[I

.field public i:[I

.field private final j:[Landroid/graphics/Rect;

.field public final k:Landroid/graphics/Rect;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:F

.field private r:I

.field public s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field public z:I


# direct methods
.method private static a(II)I
    .locals 2

    .prologue
    .line 2379246
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 2379247
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2379248
    sparse-switch v1, :sswitch_data_0

    .line 2379249
    :goto_0
    :sswitch_0
    return p0

    .line 2379250
    :sswitch_1
    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :sswitch_2
    move p0, v0

    .line 2379251
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 2379244
    invoke-virtual {p0}, LX/GgO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0814df

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/GgO;->i:[I

    aget v4, v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    add-int/lit8 v4, p1, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/GgO;->i:[I

    aget v4, v4, p1

    int-to-double v4, v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    iget v6, p0, LX/GgO;->z:I

    int-to-double v6, v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2379245
    return-object v0
.end method

.method public final b(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2379243
    iget-object v0, p0, LX/GgO;->j:[Landroid/graphics/Rect;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2379240
    iget-object v0, p0, LX/GgO;->B:LX/GgP;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GgO;->B:LX/GgP;

    invoke-virtual {v0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2379241
    const/4 v0, 0x1

    .line 2379242
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final getPaddingBottom()I
    .locals 2

    .prologue
    .line 2379239
    invoke-super {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iget v1, p0, LX/GgO;->l:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getPaddingEnd()I
    .locals 2

    .prologue
    .line 2379238
    invoke-super {p0}, Landroid/view/View;->getPaddingEnd()I

    move-result v0

    iget v1, p0, LX/GgO;->l:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getPaddingLeft()I
    .locals 2

    .prologue
    .line 2379237
    invoke-super {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iget v1, p0, LX/GgO;->l:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getPaddingRight()I
    .locals 2

    .prologue
    .line 2379236
    invoke-super {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    iget v1, p0, LX/GgO;->l:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getPaddingStart()I
    .locals 2

    .prologue
    .line 2379149
    invoke-super {p0}, Landroid/view/View;->getPaddingStart()I

    move-result v0

    iget v1, p0, LX/GgO;->l:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getPaddingTop()I
    .locals 2

    .prologue
    .line 2379235
    invoke-super {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    iget v1, p0, LX/GgO;->l:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    .line 2379210
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2379211
    iget v0, p0, LX/GgO;->x:I

    int-to-float v1, v0

    .line 2379212
    iget v0, p0, LX/GgO;->x:I

    int-to-float v3, v0

    .line 2379213
    const/4 v0, 0x5

    move v6, v0

    :goto_0
    if-lez v6, :cond_3

    .line 2379214
    rsub-int/lit8 v0, v6, 0x5

    iget v2, p0, LX/GgO;->r:I

    mul-int v7, v0, v2

    .line 2379215
    add-int/lit8 v8, v6, -0x1

    .line 2379216
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget v2, p0, LX/GgO;->t:I

    int-to-float v2, v2

    iget v4, p0, LX/GgO;->u:I

    add-int/2addr v4, v7

    int-to-float v4, v4

    iget-object v5, p0, LX/GgO;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2379217
    iget-object v0, p0, LX/GgO;->c:Landroid/graphics/drawable/Drawable;

    iget v2, p0, LX/GgO;->v:I

    iget v4, p0, LX/GgO;->w:I

    add-int/2addr v4, v7

    iget v5, p0, LX/GgO;->v:I

    iget-object v9, p0, LX/GgO;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    add-int/2addr v5, v9

    iget v9, p0, LX/GgO;->w:I

    add-int/2addr v9, v7

    iget-object v10, p0, LX/GgO;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v0, v2, v4, v5, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2379218
    iget-object v0, p0, LX/GgO;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2379219
    iget-object v0, p0, LX/GgO;->f:Landroid/graphics/Paint;

    iget-object v2, p0, LX/GgO;->h:[I

    aget v2, v2, v8

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2379220
    iget-object v0, p0, LX/GgO;->i:[I

    aget v9, v0, v8

    .line 2379221
    if-lez v9, :cond_0

    iget v0, p0, LX/GgO;->q:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    iget v0, p0, LX/GgO;->s:I

    if-lez v0, :cond_0

    .line 2379222
    iget v0, p0, LX/GgO;->q:F

    int-to-float v2, v9

    mul-float/2addr v0, v2

    iget v2, p0, LX/GgO;->s:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 2379223
    :goto_1
    iget-boolean v2, p0, LX/GgO;->A:Z

    if-eqz v2, :cond_1

    .line 2379224
    iget v2, p0, LX/GgO;->x:I

    int-to-float v2, v2

    add-float v3, v2, v0

    .line 2379225
    :goto_2
    iget v0, p0, LX/GgO;->y:I

    add-int/2addr v0, v7

    int-to-float v2, v0

    iget v0, p0, LX/GgO;->y:I

    add-int/2addr v0, v7

    int-to-float v4, v0

    iget-object v5, p0, LX/GgO;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2379226
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 2379227
    iget-boolean v0, p0, LX/GgO;->A:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/GgO;->p:I

    int-to-float v0, v0

    add-float/2addr v0, v3

    iget v4, p0, LX/GgO;->g:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 2379228
    :goto_3
    int-to-float v0, v0

    iget v4, p0, LX/GgO;->u:I

    add-int/2addr v4, v7

    int-to-float v4, v4

    iget-object v5, p0, LX/GgO;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2379229
    iget-object v0, p0, LX/GgO;->j:[Landroid/graphics/Rect;

    aget-object v0, v0, v8

    const/4 v2, 0x0

    iget v4, p0, LX/GgO;->w:I

    iget v5, p0, LX/GgO;->n:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    add-int/2addr v4, v7

    invoke-virtual {p0}, LX/GgO;->getMeasuredWidth()I

    move-result v5

    iget v8, p0, LX/GgO;->u:I

    iget v9, p0, LX/GgO;->n:I

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    add-int/2addr v7, v8

    invoke-virtual {v0, v2, v4, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 2379230
    add-int/lit8 v0, v6, -0x1

    move v6, v0

    goto/16 :goto_0

    .line 2379231
    :cond_0
    const v0, 0x3a83126f    # 0.001f

    goto :goto_1

    .line 2379232
    :cond_1
    iget v1, p0, LX/GgO;->x:I

    int-to-float v1, v1

    sub-float/2addr v1, v0

    goto :goto_2

    .line 2379233
    :cond_2
    iget v0, p0, LX/GgO;->p:I

    int-to-float v0, v0

    sub-float v0, v1, v0

    iget v4, p0, LX/GgO;->g:I

    int-to-float v4, v4

    sub-float/2addr v0, v4

    float-to-int v0, v0

    goto :goto_3

    .line 2379234
    :cond_3
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 2379196
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 2379197
    iget-object v0, p0, LX/GgO;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 2379198
    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v1

    .line 2379199
    iget-boolean v2, p0, LX/GgO;->A:Z

    if-eqz v2, :cond_0

    .line 2379200
    iput v1, p0, LX/GgO;->t:I

    .line 2379201
    iget v1, p0, LX/GgO;->t:I

    iget v2, p0, LX/GgO;->m:I

    add-int/2addr v1, v2

    iget v2, p0, LX/GgO;->d:I

    add-int/2addr v1, v2

    iput v1, p0, LX/GgO;->v:I

    .line 2379202
    iget v1, p0, LX/GgO;->v:I

    add-int/2addr v0, v1

    iget v1, p0, LX/GgO;->e:I

    add-int/2addr v0, v1

    iget v1, p0, LX/GgO;->p:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, LX/GgO;->x:I

    .line 2379203
    :goto_0
    invoke-virtual {p0}, LX/GgO;->getPaddingTop()I

    move-result v0

    iget v1, p0, LX/GgO;->n:I

    add-int/2addr v0, v1

    iput v0, p0, LX/GgO;->u:I

    .line 2379204
    invoke-virtual {p0}, LX/GgO;->getPaddingTop()I

    move-result v0

    iput v0, p0, LX/GgO;->w:I

    .line 2379205
    invoke-virtual {p0}, LX/GgO;->getPaddingTop()I

    move-result v0

    iget v1, p0, LX/GgO;->n:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, LX/GgO;->y:I

    .line 2379206
    return-void

    .line 2379207
    :cond_0
    invoke-virtual {p0}, LX/GgO;->getWidth()I

    move-result v2

    sub-int v1, v2, v1

    iput v1, p0, LX/GgO;->t:I

    .line 2379208
    iget v1, p0, LX/GgO;->t:I

    iget v2, p0, LX/GgO;->m:I

    sub-int/2addr v1, v2

    iget v2, p0, LX/GgO;->d:I

    sub-int/2addr v1, v2

    sub-int v0, v1, v0

    iput v0, p0, LX/GgO;->v:I

    .line 2379209
    iget v0, p0, LX/GgO;->v:I

    iget v1, p0, LX/GgO;->e:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/GgO;->p:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, LX/GgO;->x:I

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    .line 2379170
    invoke-virtual {p0}, LX/GgO;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, LX/GgO;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 2379171
    iget v1, p0, LX/GgO;->n:I

    mul-int/lit8 v1, v1, 0x5

    add-int/2addr v0, v1

    .line 2379172
    iget v1, p0, LX/GgO;->o:I

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2379173
    invoke-virtual {p0}, LX/GgO;->getPaddingStart()I

    move-result v1

    .line 2379174
    iget v2, p0, LX/GgO;->m:I

    add-int/2addr v1, v2

    .line 2379175
    iget v2, p0, LX/GgO;->d:I

    add-int/2addr v1, v2

    .line 2379176
    iget-object v2, p0, LX/GgO;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v1, v2

    .line 2379177
    iget v2, p0, LX/GgO;->e:I

    add-int/2addr v1, v2

    .line 2379178
    iget v2, p0, LX/GgO;->p:I

    add-int/2addr v1, v2

    .line 2379179
    iget v2, p0, LX/GgO;->g:I

    add-int/2addr v1, v2

    .line 2379180
    const/4 v3, 0x0

    .line 2379181
    move v2, v3

    move v4, v3

    .line 2379182
    :goto_0
    const/4 v5, 0x5

    if-ge v2, v5, :cond_0

    .line 2379183
    iget-object v5, p0, LX/GgO;->i:[I

    aget v5, v5, v2

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 2379184
    invoke-static {v5}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v6

    .line 2379185
    iget-object v7, p0, LX/GgO;->b:Landroid/graphics/Paint;

    iget-object v8, p0, LX/GgO;->k:Landroid/graphics/Rect;

    invoke-virtual {v7, v5, v3, v6, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2379186
    iget-object v5, p0, LX/GgO;->k:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 2379187
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2379188
    :cond_0
    move v2, v4

    .line 2379189
    add-int/2addr v1, v2

    .line 2379190
    invoke-virtual {p0}, LX/GgO;->getPaddingEnd()I

    move-result v2

    add-int/2addr v1, v2

    .line 2379191
    invoke-static {v1, p1}, LX/GgO;->a(II)I

    move-result v2

    invoke-static {v0, p2}, LX/GgO;->a(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, LX/GgO;->setMeasuredDimension(II)V

    .line 2379192
    invoke-virtual {p0}, LX/GgO;->getMeasuredWidth()I

    move-result v0

    .line 2379193
    if-le v0, v1, :cond_1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    :goto_1
    iput v0, p0, LX/GgO;->q:F

    .line 2379194
    return-void

    .line 2379195
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    .line 2379155
    check-cast p1, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;

    .line 2379156
    invoke-virtual {p1}, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2379157
    iget-object v0, p1, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->a:[I

    iput-object v0, p0, LX/GgO;->i:[I

    .line 2379158
    const/4 v2, 0x0

    .line 2379159
    iget-object v0, p0, LX/GgO;->i:[I

    aget v1, v0, v2

    .line 2379160
    iget-object v0, p0, LX/GgO;->i:[I

    aget v0, v0, v2

    iput v0, p0, LX/GgO;->z:I

    .line 2379161
    const/4 v0, 0x1

    move p1, v0

    move v0, v1

    move v1, p1

    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 2379162
    iget v2, p0, LX/GgO;->z:I

    iget-object v3, p0, LX/GgO;->i:[I

    aget v3, v3, v1

    add-int/2addr v2, v3

    iput v2, p0, LX/GgO;->z:I

    .line 2379163
    iget-object v2, p0, LX/GgO;->i:[I

    aget v2, v2, v1

    if-ge v0, v2, :cond_0

    .line 2379164
    iget-object v0, p0, LX/GgO;->i:[I

    aget v0, v0, v1

    .line 2379165
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2379166
    :cond_1
    iput v0, p0, LX/GgO;->s:I

    .line 2379167
    move v0, v0

    .line 2379168
    iput v0, p0, LX/GgO;->s:I

    .line 2379169
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 2379150
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2379151
    new-instance v1, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2379152
    iget-object v0, p0, LX/GgO;->i:[I

    .line 2379153
    iput-object v0, v1, Lcom/facebook/fig/starrating/FigStackedRatingView$SavedState;->a:[I

    .line 2379154
    return-object v1
.end method
