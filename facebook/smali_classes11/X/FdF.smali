.class public final LX/FdF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/FdH;


# direct methods
.method public constructor <init>(LX/FdH;)V
    .locals 0

    .prologue
    .line 2263326
    iput-object p1, p0, LX/FdF;->a:LX/FdH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x7cdad4c6

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263327
    iget-object v1, p0, LX/FdF;->a:LX/FdH;

    iget-object v1, v1, LX/FdH;->d:LX/Fce;

    .line 2263328
    iget-object v3, v1, LX/Fce;->a:LX/FdU;

    iget-object v4, v1, LX/Fce;->b:LX/5uu;

    .line 2263329
    iget-object v5, v3, LX/FdU;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v5, v5, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8cd;

    invoke-interface {v4}, LX/5uu;->bl_()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4}, LX/5uu;->c()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 2263330
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object p1, v5, LX/8cd;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/ComponentName;

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object p1

    const-string v1, "target_fragment"

    sget-object v4, LX/0cQ;->SEARCH_FILTER_TYPEAHEAD_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object p1

    const-string v1, "search_filter_id"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const-string v1, "search_filter_name"

    invoke-virtual {p1, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const-string v1, "search_filter_text"

    invoke-virtual {p1, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const-string v1, "search_filter_free_text_filter_string"

    invoke-virtual {p1, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    move-object v6, p1

    .line 2263331
    iget-object v5, v3, LX/FdU;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v5, v5, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->o:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const/4 v7, 0x0

    iget-object v8, v3, LX/FdU;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    invoke-interface {v5, v6, v7, v8}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2263332
    const v1, -0x53632840

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
