.class public LX/Fcg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FcI;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FcI",
        "<",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        "LX/FdH;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "LX/FdH;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 2262656
    new-instance v0, LX/Fcc;

    invoke-direct {v0}, LX/Fcc;-><init>()V

    sput-object v0, LX/Fcg;->a:LX/FcE;

    .line 2262657
    new-instance v0, LX/Fcd;

    invoke-direct {v0}, LX/Fcd;-><init>()V

    sput-object v0, LX/Fcg;->b:LX/FcE;

    .line 2262658
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "rp_commerce_price_sort"

    const v2, 0x7f021869

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "friends"

    const v2, 0x7f0208a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "rp_chrono_sort"

    const v2, 0x7f021869

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "rp_author"

    const v2, 0x7f0208a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "city"

    const v2, 0x7f020965

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "rp_location"

    const v2, 0x7f020965

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "employer"

    const v2, 0x7f02078b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "school"

    const v2, 0x7f02084d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "rp_commerce_location"

    const v2, 0x7f020911

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "rp_commerce_source"

    const v2, 0x7f020920

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "set_search_sort"

    const v2, 0x7f021869

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "rp_creation_time"

    const v2, 0x7f0207ab

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Fcg;->c:LX/0P1;

    .line 2262659
    const-string v0, "friends"

    const-string v1, "rp_author"

    const-string v2, "city"

    const-string v3, "rp_location"

    const-string v4, "employer"

    const-string v5, "school"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Fcg;->d:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262587
    iput-object p3, p0, LX/Fcg;->e:LX/0wM;

    .line 2262588
    return-void
.end method

.method public static a(LX/0QB;)LX/Fcg;
    .locals 6

    .prologue
    .line 2262589
    const-class v1, LX/Fcg;

    monitor-enter v1

    .line 2262590
    :try_start_0
    sget-object v0, LX/Fcg;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2262591
    sput-object v2, LX/Fcg;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2262592
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262593
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2262594
    new-instance p0, LX/Fcg;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-direct {p0, v3, v4, v5}, LX/Fcg;-><init>(Landroid/content/Context;LX/0ad;LX/0wM;)V

    .line 2262595
    move-object v0, p0

    .line 2262596
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2262597
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fcg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262598
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2262599
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/5uu;LX/FdH;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 14

    .prologue
    .line 2262600
    invoke-interface {p1}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v6

    .line 2262601
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2262602
    const/4 v4, 0x0

    .line 2262603
    const/4 v3, 0x1

    .line 2262604
    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v5, v2

    :goto_0
    if-ge v5, v9, :cond_1

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262605
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v10

    if-eqz v10, :cond_4

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 2262606
    new-instance v10, LX/FdE;

    invoke-virtual/range {p2 .. p2}, LX/FdH;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, LX/FdE;-><init>(Landroid/content/Context;)V

    .line 2262607
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p3 .. p3}, LX/CyH;->c()LX/4FP;

    move-result-object v12

    invoke-virtual {v12}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v12

    const-string v13, "value"

    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2262608
    const/4 v3, 0x0

    .line 2262609
    const/4 v11, 0x1

    invoke-virtual {v10, v11}, LX/FdE;->setChecked(Z)V

    .line 2262610
    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/FdE;->setTag(Ljava/lang/Object;)V

    .line 2262611
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, LX/FdE;->setTitle(Ljava/lang/CharSequence;)V

    .line 2262612
    invoke-virtual {v7, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262613
    add-int/lit8 v2, v4, 0x1

    .line 2262614
    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v2

    goto :goto_0

    .line 2262615
    :cond_0
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, LX/FdE;->setChecked(Z)V

    goto :goto_1

    .line 2262616
    :cond_1
    if-eqz v3, :cond_2

    .line 2262617
    new-instance v2, LX/FdE;

    invoke-virtual/range {p2 .. p2}, LX/FdH;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/FdE;-><init>(Landroid/content/Context;)V

    .line 2262618
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/FdE;->setChecked(Z)V

    .line 2262619
    invoke-virtual/range {p3 .. p3}, LX/CyH;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/FdE;->setTitle(Ljava/lang/CharSequence;)V

    .line 2262620
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262621
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/FdH;->setRadioButtons(LX/0Px;)V

    .line 2262622
    sget-object v2, LX/Fcg;->d:LX/0Rf;

    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, LX/5uu;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, LX/5uu;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2262623
    new-instance v2, LX/Fce;

    move-object/from16 v0, p4

    invoke-direct {v2, p0, v0, p1}, LX/Fce;-><init>(LX/Fcg;LX/FdU;LX/5uu;)V

    invoke-interface {p1}, LX/5uu;->k()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, LX/FdH;->a(LX/Fce;Ljava/lang/String;)V

    .line 2262624
    :cond_3
    new-instance v2, LX/Fcf;

    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-direct {v2, p0, v6, v0, v1}, LX/Fcf;-><init>(LX/Fcg;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;LX/FdQ;LX/CyH;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/FdH;->setOnRadioButtonSelectedListener(LX/Fcf;)V

    .line 2262625
    return-void

    :cond_4
    move v2, v4

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2262626
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    move-object v2, v1

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262627
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2262628
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2262629
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2262630
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2262631
    :cond_0
    new-instance v0, LX/CyH;

    invoke-direct {v0, p1, v2, v1}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public final a()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262632
    sget-object v0, LX/Fcg;->a:LX/FcE;

    return-object v0
.end method

.method public final a(LX/CyH;)LX/FcT;
    .locals 5

    .prologue
    .line 2262633
    iget-object v0, p1, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262634
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "name"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2262635
    sget-object v1, LX/Fcg;->c:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2262636
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unimplemented filter "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2262637
    :cond_0
    new-instance v1, LX/FcT;

    .line 2262638
    iget-object v2, p1, LX/CyH;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2262639
    iget-object v3, p0, LX/Fcg;->e:LX/0wM;

    sget-object v4, LX/Fcg;->c:LX/0P1;

    invoke-virtual {v4, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v4, -0xa76f01

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2262640
    iget-object v3, p1, LX/CyH;->c:LX/4FP;

    move-object v3, v3

    .line 2262641
    invoke-direct {v1, v2, v0, v3}, LX/FcT;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;LX/4FP;)V

    return-object v1
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .locals 4

    .prologue
    .line 2262642
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2262643
    invoke-interface {p1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262644
    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v2

    .line 2262645
    if-nez v2, :cond_0

    .line 2262646
    :goto_0
    return-void

    .line 2262647
    :cond_0
    sget-object v0, LX/Fcg;->c:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2262648
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unimplemented filter "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2262649
    :cond_1
    iget-object v0, p3, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262650
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2262651
    if-eqz v0, :cond_2

    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const v0, -0x6e685d

    move v1, v0

    .line 2262652
    :goto_1
    iget-object v3, p0, LX/Fcg;->e:LX/0wM;

    sget-object v0, LX/Fcg;->c:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2262653
    :cond_3
    const v0, -0xa76f01

    move v1, v0

    goto :goto_1
.end method

.method public final bridge synthetic a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 6

    .prologue
    .line 2262654
    move-object v2, p2

    check-cast v2, LX/FdH;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/Fcg;->a(LX/5uu;LX/FdH;LX/CyH;LX/FdU;LX/FdQ;)V

    return-void
.end method

.method public final b()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "LX/FdH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262655
    sget-object v0, LX/Fcg;->b:LX/FcE;

    return-object v0
.end method
