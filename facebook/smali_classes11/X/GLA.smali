.class public final LX/GLA;
.super LX/GFk;
.source ""


# instance fields
.field public final synthetic a:LX/GLB;


# direct methods
.method public constructor <init>(LX/GLB;)V
    .locals 0

    .prologue
    .line 2342155
    iput-object p1, p0, LX/GLA;->a:LX/GLB;

    invoke-direct {p0}, LX/GFk;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2342156
    check-cast p1, LX/GFj;

    .line 2342157
    iget-object v0, p1, LX/GFj;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2342158
    iget-object v1, p1, LX/GFj;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2342159
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2342160
    :goto_0
    return-void

    .line 2342161
    :cond_0
    iget-object v0, p0, LX/GLA;->a:LX/GLB;

    iget-object v0, v0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342162
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2342163
    if-eqz v0, :cond_1

    .line 2342164
    iget-object v0, p0, LX/GLA;->a:LX/GLB;

    iget-object v0, v0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    const/4 v1, 0x0

    .line 2342165
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2342166
    iget-object v0, p0, LX/GLA;->a:LX/GLB;

    iget-object v0, v0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2342167
    iget-object v0, p0, LX/GLA;->a:LX/GLB;

    iget-object v0, v0, LX/GLB;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    sget v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a:I

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c(I)V

    .line 2342168
    iget-object v0, p0, LX/GLA;->a:LX/GLB;

    iget-object v0, v0, LX/GLB;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0}, LX/GMo;->a(Landroid/view/View;)V

    .line 2342169
    iget-object v0, p0, LX/GLA;->a:LX/GLB;

    .line 2342170
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2342171
    new-instance v1, LX/GF8;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-direct {v1, v2}, LX/GF8;-><init>(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2342172
    :cond_1
    iget-object v0, p0, LX/GLA;->a:LX/GLB;

    invoke-static {v0}, LX/GLB;->d(LX/GLB;)V

    goto :goto_0
.end method
