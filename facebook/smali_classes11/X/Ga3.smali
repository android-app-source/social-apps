.class public LX/Ga3;
.super LX/3x6;
.source ""


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 2367838
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 2367839
    iput p1, p0, LX/Ga3;->a:I

    .line 2367840
    iput p2, p0, LX/Ga3;->b:I

    .line 2367841
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 3

    .prologue
    .line 2367842
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)I

    move-result v0

    iget v1, p0, LX/Ga3;->a:I

    sub-int/2addr v0, v1

    .line 2367843
    if-gez v0, :cond_0

    .line 2367844
    :goto_0
    return-void

    .line 2367845
    :cond_0
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0d0f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 2367846
    float-to-int v2, v1

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 2367847
    iget v2, p0, LX/Ga3;->b:I

    rem-int v2, v0, v2

    if-nez v2, :cond_1

    .line 2367848
    float-to-int v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2367849
    float-to-int v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 2367850
    :cond_1
    iget v2, p0, LX/Ga3;->b:I

    rem-int/2addr v0, v2

    iget v2, p0, LX/Ga3;->b:I

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_2

    .line 2367851
    float-to-int v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2367852
    float-to-int v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 2367853
    :cond_2
    float-to-int v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2367854
    float-to-int v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method
