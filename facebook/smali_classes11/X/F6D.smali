.class public LX/F6D;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/5pX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Landroid/app/Activity;

.field public final c:LX/1Kf;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Yb;


# direct methods
.method public constructor <init>(LX/5pX;Landroid/app/Activity;LX/1Kf;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .param p1    # LX/5pX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pX;",
            "Landroid/app/Activity;",
            "LX/1Kf;",
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200299
    iput-object p1, p0, LX/F6D;->a:LX/5pX;

    .line 2200300
    iput-object p2, p0, LX/F6D;->b:Landroid/app/Activity;

    .line 2200301
    iput-object p3, p0, LX/F6D;->c:LX/1Kf;

    .line 2200302
    iput-object p4, p0, LX/F6D;->d:LX/0Ot;

    .line 2200303
    iput-object p5, p0, LX/F6D;->e:LX/0Ot;

    .line 2200304
    iput-object p6, p0, LX/F6D;->f:LX/0Ot;

    .line 2200305
    iput-object p7, p0, LX/F6D;->g:LX/0Ot;

    .line 2200306
    iput-object p8, p0, LX/F6D;->h:LX/0Ot;

    .line 2200307
    iget-object v0, p0, LX/F6D;->a:LX/5pX;

    if-eqz v0, :cond_0

    .line 2200308
    iget-object v0, p0, LX/F6D;->a:LX/5pX;

    new-instance v1, LX/F69;

    invoke-direct {v1, p0}, LX/F69;-><init>(LX/F6D;)V

    invoke-virtual {v0, v1}, LX/5pX;->a(LX/5pQ;)V

    .line 2200309
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 5

    .prologue
    .line 2200310
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    const-string v1, "reactGroups"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2200311
    iput-object p1, v1, LX/89I;->c:Ljava/lang/String;

    .line 2200312
    move-object v1, v1

    .line 2200313
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "group_composer"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/F6D;LX/F6C;JLandroid/os/Bundle;)V
    .locals 4
    .param p2    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2200289
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 2200290
    :cond_0
    :goto_0
    return-void

    .line 2200291
    :cond_1
    iget-object v0, p0, LX/F6D;->a:LX/5pX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F6D;->a:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2200292
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2200293
    const-string v0, "groupID"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200294
    const-string v0, "status"

    invoke-virtual {p1}, LX/F6C;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200295
    if-eqz p4, :cond_2

    .line 2200296
    const-string v0, "optimisticStory"

    invoke-static {p4}, LX/5op;->a(Landroid/os/Bundle;)LX/5pH;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2200297
    :cond_2
    iget-object v0, p0, LX/F6D;->a:LX/5pX;

    const-class v2, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v2}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v2, "postComposerEvent"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/F6D;LX/F6C;J)V
    .locals 2

    .prologue
    .line 2200201
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/F6D;->a(LX/F6D;LX/F6C;JLandroid/os/Bundle;)V

    .line 2200202
    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 9

    .prologue
    .line 2200213
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 2200214
    :goto_0
    return-void

    .line 2200215
    :cond_0
    const-string v0, "publishPostParams"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2200216
    iget-object v1, p0, LX/F6D;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v1, p2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2200217
    const-string v1, "is_uploading_media"

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2200218
    new-instance v3, LX/F6B;

    invoke-direct {v3, p0, v0}, LX/F6B;-><init>(LX/F6D;Lcom/facebook/composer/publish/common/PublishPostParams;)V

    iget-object v1, p0, LX/F6D;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2200219
    :cond_1
    const-string v1, "extra_optimistic_feed_story"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2200220
    const-string v1, "extra_optimistic_feed_story"

    invoke-static {p2, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2200221
    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 2200222
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2200223
    const-string v2, "id"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200224
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2200225
    const-string v2, "hasOptimisticAttachments"

    invoke-virtual {v4, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2200226
    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2200227
    const-string v5, "text"

    invoke-static {v1}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200228
    const-string v5, "message"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200229
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2200230
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 2200231
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 2200232
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 2200233
    :cond_3
    const-string v6, "text"

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200234
    const-string v2, "title"

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200235
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 2200236
    if-eqz v2, :cond_8

    .line 2200237
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2200238
    const-string v6, "id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200239
    const-string v6, "does_viewer_like"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2200240
    const-string v6, "can_viewer_comment"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2200241
    const-string v6, "legacy_api_post_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200242
    const-string v6, "can_viewer_like"

    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2200243
    const-string v6, "can_viewer_react"

    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2200244
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v6

    .line 2200245
    if-eqz v6, :cond_4

    .line 2200246
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2200247
    const-string v8, "count"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v6

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2200248
    const-string v6, "likers"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200249
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v6

    .line 2200250
    if-eqz v6, :cond_5

    .line 2200251
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2200252
    const-string v8, "count"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v6

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2200253
    const-string v6, "reactors"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200254
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v6

    .line 2200255
    if-eqz v6, :cond_6

    .line 2200256
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2200257
    const-string v8, "count"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->a()I

    move-result v6

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2200258
    const-string v6, "seen_by"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200259
    :cond_6
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    .line 2200260
    if-eqz v2, :cond_7

    .line 2200261
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2200262
    const-string v7, "count"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a()I

    move-result v2

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2200263
    const-string v2, "top_level_comments"

    invoke-virtual {v5, v2, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200264
    :cond_7
    const-string v2, "feedback"

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200265
    :cond_8
    invoke-static {v1}, LX/17E;->x(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 2200266
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2200267
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2200268
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2200269
    const-string v7, "uri"

    invoke-static {v1}, LX/17E;->x(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200270
    const-string v7, "photo_large"

    invoke-virtual {v5, v7, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200271
    const-string v6, "media"

    invoke-virtual {v2, v6, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2200272
    new-array v5, p1, [Landroid/os/Bundle;

    aput-object v2, v5, v3

    .line 2200273
    const-string v2, "attachments"

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 2200274
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    .line 2200275
    if-eqz v5, :cond_b

    .line 2200276
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 2200277
    new-array v7, v6, [Landroid/os/Bundle;

    .line 2200278
    :goto_2
    if-ge v3, v6, :cond_a

    .line 2200279
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2200280
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2200281
    const-string p1, "id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v8, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200282
    const-string p1, "name"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200283
    aput-object v8, v7, v3

    .line 2200284
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2200285
    :cond_a
    const-string v2, "actors"

    invoke-virtual {v4, v2, v7}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 2200286
    :cond_b
    move-object v1, v4

    .line 2200287
    sget-object v2, LX/F6C;->CREATED:LX/F6C;

    iget-wide v4, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {p0, v2, v4, v5, v1}, LX/F6D;->a(LX/F6D;LX/F6C;JLandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2200288
    :cond_c
    sget-object v1, LX/F6C;->CREATED:LX/F6C;

    iget-wide v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {p0, v1, v2, v3}, LX/F6D;->a$redex0(LX/F6D;LX/F6C;J)V

    goto/16 :goto_0

    :cond_d
    const-string v2, ""

    goto/16 :goto_1
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 2200203
    iget-object v0, p0, LX/F6D;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ay;

    .line 2200204
    iget-object v4, p0, LX/F6D;->b:Landroid/app/Activity;

    new-instance v5, LX/8AA;

    sget-object v6, LX/8AB;->GROUP:LX/8AB;

    invoke-direct {v5, v6}, LX/8AA;-><init>(LX/8AB;)V

    sget-object v6, LX/21D;->GROUP_FEED:LX/21D;

    const-string v7, "groupPhotoReact"

    invoke-static {v6, v7}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    new-instance v7, LX/89I;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    sget-object v10, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v7, v8, v9, v10}, LX/89I;-><init>(JLX/2rw;)V

    .line 2200205
    iput-object p2, v7, LX/89I;->c:Ljava/lang/String;

    .line 2200206
    move-object v7, v7

    .line 2200207
    invoke-virtual {v7}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    .line 2200208
    iput-object v6, v5, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2200209
    move-object v5, v5

    .line 2200210
    invoke-static {v4, v5}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v4

    move-object v1, v4

    .line 2200211
    const/16 v2, 0x6dc

    iget-object v3, p0, LX/F6D;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1, v2, v3}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2200212
    return-void
.end method
