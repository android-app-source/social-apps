.class public LX/GRD;
.super LX/6ly;
.source ""


# instance fields
.field public final a:LX/GRZ;

.field public final b:LX/GTB;


# direct methods
.method public constructor <init>(LX/0ja;LX/GTB;LX/GRZ;)V
    .locals 0
    .param p3    # LX/GRZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351435
    invoke-direct {p0, p1}, LX/6ly;-><init>(LX/0ja;)V

    .line 2351436
    iput-object p3, p0, LX/GRD;->a:LX/GRZ;

    .line 2351437
    iput-object p2, p0, LX/GRD;->b:LX/GTB;

    .line 2351438
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2351434
    iget-object v0, p0, LX/GRD;->a:LX/GRZ;

    invoke-virtual {v0, p1}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 2351428
    check-cast p1, Lcom/facebook/appinvites/ui/AppInviteContentView;

    .line 2351429
    iget-object v0, p0, LX/GRD;->b:LX/GTB;

    iget-object v1, p0, LX/GRD;->a:LX/GRZ;

    invoke-virtual {v0, p1, v1, p3}, LX/GTB;->a(Lcom/facebook/appinvites/ui/AppInviteContentView;LX/GRZ;I)V

    .line 2351430
    new-instance v0, LX/GRC;

    invoke-direct {v0, p0, p3}, LX/GRC;-><init>(LX/GRD;I)V

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setMessageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2351431
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2351433
    iget-object v0, p0, LX/GRD;->a:LX/GRZ;

    invoke-virtual {v0}, LX/GRZ;->b()I

    move-result v0

    return v0
.end method

.method public final d()LX/2eR;
    .locals 1

    .prologue
    .line 2351432
    new-instance v0, LX/GRB;

    invoke-direct {v0, p0}, LX/GRB;-><init>(LX/GRD;)V

    return-object v0
.end method
