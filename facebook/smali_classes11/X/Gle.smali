.class public final LX/Gle;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)V
    .locals 1

    .prologue
    .line 2391155
    iput-object p1, p0, LX/Gle;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 2391156
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gle;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1

    .prologue
    .line 2391135
    invoke-super {p0, p1, p2}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 2391136
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gle;->b:Z

    .line 2391137
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    .line 2391138
    invoke-super {p0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 2391139
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/3wu;

    .line 2391140
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v1

    .line 2391141
    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    .line 2391142
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v2

    .line 2391143
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    .line 2391144
    sub-int/2addr v0, v2

    .line 2391145
    iget-object v3, p0, LX/Gle;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    iget-object v3, v3, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->j:LX/Glk;

    .line 2391146
    iget p1, v3, LX/Glk;->b:I

    move v3, p1

    .line 2391147
    mul-int/lit8 v3, v3, 0x2

    .line 2391148
    add-int/2addr v0, v2

    sub-int v2, v1, v3

    if-lt v0, v2, :cond_1

    if-lez v1, :cond_1

    .line 2391149
    iget-boolean v0, p0, LX/Gle;->b:Z

    if-nez v0, :cond_0

    .line 2391150
    iget-object v0, p0, LX/Gle;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    .line 2391151
    invoke-virtual {v0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->e()LX/Gkk;

    move-result-object v1

    invoke-static {v0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->r(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->n()LX/GkK;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/Gkk;->a(ILX/GkK;)V

    .line 2391152
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gle;->b:Z

    .line 2391153
    :goto_0
    return-void

    .line 2391154
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gle;->b:Z

    goto :goto_0
.end method
