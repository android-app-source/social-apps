.class public LX/Fb5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0iA;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259568
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259569
    iput-object v0, p0, LX/Fb5;->c:LX/0Ot;

    .line 2259570
    return-void
.end method

.method public static b(LX/Fb5;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2259571
    iget-object v0, p0, LX/Fb5;->b:LX/0iA;

    sget-object v2, LX/FaI;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/FaI;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13G;

    .line 2259572
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2259573
    :goto_0
    return-object v0

    .line 2259574
    :cond_0
    iget-object v2, p0, LX/Fb5;->a:Landroid/content/Context;

    invoke-interface {v0, v2}, LX/13G;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 2259575
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2259576
    goto :goto_0

    .line 2259577
    :cond_1
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "qp_definition"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    goto :goto_0
.end method
