.class public final LX/GHk;
.super LX/GFd;
.source ""


# instance fields
.field public final synthetic a:LX/GHx;


# direct methods
.method public constructor <init>(LX/GHx;)V
    .locals 0

    .prologue
    .line 2335219
    iput-object p1, p0, LX/GHk;->a:LX/GHx;

    invoke-direct {p0}, LX/GFd;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2335220
    check-cast p1, LX/GFc;

    .line 2335221
    iget-boolean v0, p1, LX/GFc;->a:Z

    move v0, v0

    .line 2335222
    if-eqz v0, :cond_1

    .line 2335223
    iget-object v0, p0, LX/GHk;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    const v1, 0x7f080afc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineLabelText(Ljava/lang/Integer;)V

    .line 2335224
    iget-object v0, p0, LX/GHk;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setMaxHeadlineLength(I)V

    .line 2335225
    iget-object v0, p1, LX/GFc;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2335226
    if-eqz v0, :cond_0

    .line 2335227
    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineText(Ljava/lang/CharSequence;)V

    .line 2335228
    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    invoke-static {v1, v0}, LX/GHx;->b$redex0(LX/GHx;Ljava/lang/CharSequence;)V

    .line 2335229
    :cond_0
    :goto_0
    return-void

    .line 2335230
    :cond_1
    iget-object v0, p0, LX/GHk;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 2335231
    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    const v2, 0x7f080afb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineLabelText(Ljava/lang/Integer;)V

    .line 2335232
    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setMaxHeadlineLength(I)V

    .line 2335233
    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335234
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2335235
    const/4 v2, 0x0

    iget-object v3, p0, LX/GHk;->a:LX/GHx;

    iget-object v3, v3, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335236
    iget-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->b:Ljava/lang/String;

    move-object v3, p1

    .line 2335237
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2335238
    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setHeadlineText(Ljava/lang/CharSequence;)V

    .line 2335239
    iget-object v1, p0, LX/GHk;->a:LX/GHx;

    invoke-static {v1, v0}, LX/GHx;->b$redex0(LX/GHx;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
