.class public LX/FrA;
.super LX/1Cv;
.source ""


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fr4;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FrJ;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/Fr2;

.field public final g:LX/FrH;

.field private final h:Landroid/view/View$OnClickListener;

.field private final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2295115
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FrA;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FrH;LX/Fr2;Landroid/view/View$OnClickListener;Ljava/lang/String;LX/03V;LX/0Or;LX/0Or;)V
    .locals 1
    .param p1    # LX/FrH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Fr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FrH;",
            "LX/Fr2;",
            "Landroid/view/View$OnClickListener;",
            "Ljava/lang/String;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/Fr4;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FrJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2295106
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2295107
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FrH;

    iput-object v0, p0, LX/FrA;->g:LX/FrH;

    .line 2295108
    iput-object p3, p0, LX/FrA;->h:Landroid/view/View$OnClickListener;

    .line 2295109
    iput-object p4, p0, LX/FrA;->i:Ljava/lang/String;

    .line 2295110
    iput-object p5, p0, LX/FrA;->c:LX/03V;

    .line 2295111
    iput-object p6, p0, LX/FrA;->d:LX/0Or;

    .line 2295112
    iput-object p7, p0, LX/FrA;->e:LX/0Or;

    .line 2295113
    iput-object p2, p0, LX/FrA;->f:LX/Fr2;

    .line 2295114
    return-void
.end method

.method private static a(I)LX/G4n;
    .locals 2

    .prologue
    .line 2295099
    invoke-static {}, LX/Fr9;->values()[LX/Fr9;

    move-result-object v0

    aget-object v0, v0, p0

    .line 2295100
    sget-object v1, LX/Fr8;->a:[I

    invoke-virtual {v0}, LX/Fr9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2295101
    :pswitch_0
    sget-object v0, LX/G4n;->WHOLE:LX/G4n;

    :goto_0
    return-object v0

    .line 2295102
    :pswitch_1
    sget-object v0, LX/G4n;->BEGIN:LX/G4n;

    goto :goto_0

    .line 2295103
    :pswitch_2
    sget-object v0, LX/G4n;->MIDDLE:LX/G4n;

    goto :goto_0

    .line 2295104
    :pswitch_3
    sget-object v0, LX/G4n;->END:LX/G4n;

    goto :goto_0

    .line 2295105
    :pswitch_4
    sget-object v0, LX/G4n;->LOAD_MORE_INDICATOR:LX/G4n;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2295088
    iget-object v0, p0, LX/FrA;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 2295089
    invoke-static {}, LX/Fr9;->values()[LX/Fr9;

    move-result-object v1

    aget-object v1, v1, p1

    .line 2295090
    sget-object v2, LX/Fr8;->a:[I

    invoke-virtual {v1}, LX/Fr9;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2295091
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown item type for TimelineContextualInfoAdapter of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2295092
    :pswitch_0
    sget-short v1, LX/0wf;->aw:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2295093
    new-instance v0, LX/CEA;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CEA;-><init>(Landroid/content/Context;)V

    .line 2295094
    :goto_0
    return-object v0

    .line 2295095
    :cond_0
    new-instance v0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2295096
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314ca

    invoke-virtual {v0, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2295097
    new-instance v1, LX/637;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0543

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, LX/635;->BOTTOM:LX/635;

    invoke-direct {v1, v0, v4, v2, v3}, LX/637;-><init>(Landroid/view/View;IILX/635;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->a(LX/636;)V

    goto :goto_0

    .line 2295098
    :pswitch_2
    new-instance v0, LX/FyX;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/FrA;->i:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/FyX;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 10

    .prologue
    .line 2295116
    const/4 v3, 0x0

    .line 2295117
    const/4 v2, 0x0

    .line 2295118
    const/4 v5, 0x0

    .line 2295119
    instance-of v4, p5, LX/1a8;

    if-eqz v4, :cond_c

    .line 2295120
    check-cast p5, LX/1a8;

    invoke-interface {p5}, LX/1a8;->getViewDiagnostics()LX/1Rn;

    move-result-object v2

    .line 2295121
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/1Rn;->c()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, LX/1Rn;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2295122
    :cond_0
    const/4 v3, 0x1

    .line 2295123
    const-string v4, "renderTimelineContextItemView"

    const v6, -0x6e85622b

    invoke-static {v4, v6}, LX/02m;->a(Ljava/lang/String;I)V

    :cond_1
    move-object v6, v2

    move v7, v3

    .line 2295124
    :goto_0
    :try_start_0
    instance-of v2, p2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    if-eqz v2, :cond_2

    .line 2295125
    move-object v0, p2

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    move-object v2, v0

    .line 2295126
    sget-object v3, LX/Fr8;->b:[I

    iget-object v4, p0, LX/FrA;->f:LX/Fr2;

    invoke-virtual {v4}, LX/Fr2;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2295127
    iget-object v2, p0, LX/FrA;->c:LX/03V;

    const-string v3, "TimelineContextualInfoAdapter.unknownRenderingStyle"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Unsupported rendering style: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, LX/FrA;->f:LX/Fr2;

    invoke-virtual {v8}, LX/Fr2;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    move v3, v5

    .line 2295128
    :goto_1
    :try_start_1
    sget-object v2, LX/FrA;->b:Ljava/lang/Object;

    if-ne p2, v2, :cond_b

    instance-of v2, p3, LX/FyX;

    if-eqz v2, :cond_b

    .line 2295129
    move-object v0, p3

    check-cast v0, LX/FyX;

    move-object v2, v0

    iget-object v4, p0, LX/FrA;->g:LX/FrH;

    .line 2295130
    iget v0, v4, LX/BPB;->e:I

    move v4, v0

    .line 2295131
    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    :goto_2
    iget-object v5, p0, LX/FrA;->h:Landroid/view/View$OnClickListener;

    .line 2295132
    if-eqz v4, :cond_d

    .line 2295133
    const/4 v5, 0x0

    .line 2295134
    iget-object v0, v2, LX/FyX;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2295135
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2295136
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, v2, LX/FyX;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2295137
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, v2, LX/FyX;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2295138
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, LX/FyX;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2295139
    const v0, 0x7f0a0048

    invoke-virtual {v2, v0}, LX/FyX;->setBackgroundResource(I)V

    .line 2295140
    iget v0, v2, LX/FyX;->f:I

    iget v4, v2, LX/FyX;->g:I

    invoke-virtual {v2, v5, v0, v5, v4}, LX/FyX;->setPadding(IIII)V

    .line 2295141
    const/4 v0, 0x1

    move v0, v0

    .line 2295142
    :goto_3
    move v3, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2295143
    move v4, v3

    .line 2295144
    :goto_4
    const-wide/16 v2, 0x0

    .line 2295145
    if-eqz v7, :cond_3

    .line 2295146
    const v2, 0x28cb31f4

    invoke-static {v2}, LX/02m;->b(I)J

    move-result-wide v2

    .line 2295147
    :cond_3
    if-eqz v7, :cond_7

    if-eqz v4, :cond_7

    .line 2295148
    invoke-virtual {v6, v2, v3}, LX/1Rn;->c(J)V

    .line 2295149
    invoke-virtual {v6, p3}, LX/1Rn;->a(Landroid/view/View;)V

    .line 2295150
    :cond_4
    :goto_5
    return-void

    .line 2295151
    :pswitch_0
    :try_start_2
    iget-object v3, p0, LX/FrA;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Fr4;

    move-object v0, p3

    check-cast v0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    move-object v4, v0

    new-instance v8, LX/Fr7;

    invoke-static {p4}, LX/FrA;->a(I)LX/G4n;

    move-result-object v9

    invoke-direct {v8, v2, v9}, LX/Fr7;-><init>(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;LX/G4n;)V

    invoke-virtual {v3, v4, v8}, LX/Fr3;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/Fr7;)Z

    move-result v5

    move v3, v5

    .line 2295152
    goto/16 :goto_1

    .line 2295153
    :pswitch_1
    instance-of v3, p3, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    if-eqz v3, :cond_5

    .line 2295154
    iget-object v3, p0, LX/FrA;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FrJ;

    move-object v0, p3

    check-cast v0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    move-object v4, v0

    new-instance v8, LX/Fr7;

    invoke-static {p4}, LX/FrA;->a(I)LX/G4n;

    move-result-object v9

    invoke-direct {v8, v2, v9}, LX/Fr7;-><init>(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;LX/G4n;)V

    invoke-virtual {v3, v4, v8}, LX/Fr3;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/Fr7;)Z

    move-result v5

    move v3, v5

    goto/16 :goto_1

    .line 2295155
    :cond_5
    instance-of v3, p3, LX/CEA;

    if-eqz v3, :cond_2

    .line 2295156
    iget-object v3, p0, LX/FrA;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FrJ;

    move-object v0, p3

    check-cast v0, LX/CEA;

    move-object v4, v0

    new-instance v8, LX/Fr7;

    invoke-static {p4}, LX/FrA;->a(I)LX/G4n;

    move-result-object v9

    invoke-direct {v8, v2, v9}, LX/Fr7;-><init>(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;LX/G4n;)V

    .line 2295157
    iget-object v0, v8, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    if-nez v0, :cond_10

    .line 2295158
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, LX/CEA;->setVisibility(I)V

    .line 2295159
    const/4 v0, 0x0

    .line 2295160
    :goto_6
    move v5, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2295161
    move v3, v5

    goto/16 :goto_1

    .line 2295162
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 2295163
    :cond_7
    if-eqz v6, :cond_4

    .line 2295164
    const/4 v2, 0x0

    .line 2295165
    iput-boolean v2, v6, LX/1Rn;->C:Z

    .line 2295166
    goto :goto_5

    .line 2295167
    :catchall_0
    move-exception v2

    move v3, v5

    :goto_7
    const-wide/16 v4, 0x0

    .line 2295168
    if-eqz v7, :cond_8

    .line 2295169
    const v4, -0x160dbfab

    invoke-static {v4}, LX/02m;->b(I)J

    move-result-wide v4

    .line 2295170
    :cond_8
    if-eqz v7, :cond_a

    if-eqz v3, :cond_a

    .line 2295171
    invoke-virtual {v6, v4, v5}, LX/1Rn;->c(J)V

    .line 2295172
    invoke-virtual {v6, p3}, LX/1Rn;->a(Landroid/view/View;)V

    .line 2295173
    :cond_9
    :goto_8
    throw v2

    .line 2295174
    :cond_a
    if-eqz v6, :cond_9

    .line 2295175
    const/4 v3, 0x0

    .line 2295176
    iput-boolean v3, v6, LX/1Rn;->C:Z

    .line 2295177
    goto :goto_8

    .line 2295178
    :catchall_1
    move-exception v2

    goto :goto_7

    :cond_b
    move v4, v3

    goto/16 :goto_4

    :cond_c
    move-object v6, v2

    move v7, v3

    goto/16 :goto_0

    :cond_d
    :try_start_3
    const/4 v4, 0x0

    .line 2295179
    iget-object v0, v2, LX/FyX;->i:Ljava/lang/String;

    if-nez v0, :cond_f

    .line 2295180
    iget-object v0, v2, LX/FyX;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2295181
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2295182
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, v2, LX/FyX;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2295183
    iget v0, v2, LX/FyX;->g:I

    invoke-virtual {v2, v4, v4, v4, v0}, LX/FyX;->setPadding(IIII)V

    .line 2295184
    :goto_9
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2295185
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2295186
    :cond_e
    invoke-virtual {v2, v5}, LX/FyX;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2295187
    const v0, 0x7f021943

    invoke-virtual {v2, v0}, LX/FyX;->setBackgroundResource(I)V

    .line 2295188
    const/4 v0, 0x1

    move v0, v0

    .line 2295189
    goto/16 :goto_3

    .line 2295190
    :cond_f
    iget-object v0, v2, LX/FyX;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2295191
    iget-object v0, v2, LX/FyX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2295192
    iget-object v0, v2, LX/FyX;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, v2, LX/FyX;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2295193
    iget v0, v2, LX/FyX;->h:I

    invoke-virtual {v2, v4, v4, v4, v0}, LX/FyX;->setPadding(IIII)V

    goto :goto_9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2295194
    :cond_10
    new-instance v0, LX/Fr6;

    invoke-direct {v0, v3, v8}, LX/Fr6;-><init>(LX/Fr3;LX/Fr7;)V

    invoke-virtual {v4, v0}, LX/CEA;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2295195
    iget-object v0, v8, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-static {v0}, LX/Fr3;->c(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2295196
    iget-object v0, v8, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, v8, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-virtual {v4, v0}, LX/CEA;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2295197
    const/4 v0, 0x1

    goto/16 :goto_6

    .line 2295198
    :cond_11
    const/4 v0, 0x0

    goto :goto_a

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2295073
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2295074
    const/4 v0, 0x0

    .line 2295075
    :goto_0
    return v0

    .line 2295076
    :cond_0
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    .line 2295077
    iget v1, v0, LX/BPB;->d:I

    move v0, v1

    .line 2295078
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 2295079
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    .line 2295080
    iget v1, v0, LX/FrH;->f:I

    move v1, v1

    .line 2295081
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr5;

    .line 2295082
    iget-object p0, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, p0

    .line 2295083
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2295084
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2295085
    :cond_1
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr5;

    .line 2295086
    iget-object v1, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, v1

    .line 2295087
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2295057
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2295058
    invoke-virtual {p0}, LX/FrA;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2295059
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    .line 2295060
    iget v1, v0, LX/BPB;->d:I

    move v0, v1

    .line 2295061
    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    .line 2295062
    iget v1, v0, LX/FrH;->f:I

    move v1, v1

    .line 2295063
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr5;

    .line 2295064
    iget-object v2, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, v2

    .line 2295065
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 2295066
    sget-object v0, LX/FrA;->b:Ljava/lang/Object;

    .line 2295067
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 2295068
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2295069
    goto :goto_1

    .line 2295070
    :cond_2
    iget-object v0, p0, LX/FrA;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr5;

    .line 2295071
    iget-object v1, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, v1

    .line 2295072
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2295056
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 6

    .prologue
    .line 2295041
    invoke-virtual {p0, p1}, LX/FrA;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 2295042
    sget-object v0, LX/Fr9;->UNKNOWN:LX/Fr9;

    .line 2295043
    instance-of v2, v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    if-eqz v2, :cond_1

    .line 2295044
    invoke-virtual {p0}, LX/FrA;->getCount()I

    move-result v0

    .line 2295045
    add-int/lit8 v2, v0, -0x1

    if-eq p1, v2, :cond_0

    add-int/lit8 v0, v0, -0x2

    if-ne p1, v0, :cond_4

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, LX/FrA;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, LX/FrA;->b:Ljava/lang/Object;

    if-ne v0, v2, :cond_4

    .line 2295046
    :cond_0
    sget-object v0, LX/Fr9;->CONTEXTUAL_ITEM_BOTTOM:LX/Fr9;

    .line 2295047
    :cond_1
    :goto_0
    sget-object v2, LX/FrA;->b:Ljava/lang/Object;

    if-ne v1, v2, :cond_2

    .line 2295048
    sget-object v0, LX/Fr9;->LOAD_MORE_INDICATOR:LX/Fr9;

    .line 2295049
    :cond_2
    sget-object v2, LX/Fr9;->UNKNOWN:LX/Fr9;

    if-ne v0, v2, :cond_3

    .line 2295050
    iget-object v2, p0, LX/FrA;->c:LX/03V;

    const-string v3, "TimelineContextualInfoAdapter.unknown_viewtype"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown view type for postition: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2295051
    :cond_3
    invoke-virtual {v0}, LX/Fr9;->ordinal()I

    move-result v0

    return v0

    .line 2295052
    :cond_4
    if-nez p1, :cond_5

    .line 2295053
    sget-object v0, LX/Fr9;->CONTEXTUAL_ITEM_TOP:LX/Fr9;

    goto :goto_0

    .line 2295054
    :cond_5
    sget-object v0, LX/Fr9;->CONTEXTUAL_ITEM_MIDDLE:LX/Fr9;

    goto :goto_0

    .line 2295055
    :cond_6
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2295040
    invoke-static {}, LX/Fr9;->values()[LX/Fr9;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
