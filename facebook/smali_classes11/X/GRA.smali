.class public LX/GRA;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GRA;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351398
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2351399
    sget-object v0, LX/0ax;->fn:Ljava/lang/String;

    const-string v1, "{%s}"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "invite_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/appinvites/activity/AppInvitesActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2351400
    sget-object v0, LX/0ax;->fn:Ljava/lang/String;

    const-class v1, Lcom/facebook/appinvites/activity/AppInvitesActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2351401
    return-void
.end method

.method public static a(LX/0QB;)LX/GRA;
    .locals 3

    .prologue
    .line 2351402
    sget-object v0, LX/GRA;->a:LX/GRA;

    if-nez v0, :cond_1

    .line 2351403
    const-class v1, LX/GRA;

    monitor-enter v1

    .line 2351404
    :try_start_0
    sget-object v0, LX/GRA;->a:LX/GRA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2351405
    if-eqz v2, :cond_0

    .line 2351406
    :try_start_1
    new-instance v0, LX/GRA;

    invoke-direct {v0}, LX/GRA;-><init>()V

    .line 2351407
    move-object v0, v0

    .line 2351408
    sput-object v0, LX/GRA;->a:LX/GRA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2351409
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2351410
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2351411
    :cond_1
    sget-object v0, LX/GRA;->a:LX/GRA;

    return-object v0

    .line 2351412
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2351413
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
