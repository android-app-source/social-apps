.class public final enum LX/GDR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GDR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GDR;

.field public static final enum TASK_GEOCODE_ADDRESS:LX/GDR;

.field public static final enum TASK_REVERSE_GEOCODE:LX/GDR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2329816
    new-instance v0, LX/GDR;

    const-string v1, "TASK_GEOCODE_ADDRESS"

    invoke-direct {v0, v1, v2}, LX/GDR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GDR;->TASK_GEOCODE_ADDRESS:LX/GDR;

    .line 2329817
    new-instance v0, LX/GDR;

    const-string v1, "TASK_REVERSE_GEOCODE"

    invoke-direct {v0, v1, v3}, LX/GDR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GDR;->TASK_REVERSE_GEOCODE:LX/GDR;

    .line 2329818
    const/4 v0, 0x2

    new-array v0, v0, [LX/GDR;

    sget-object v1, LX/GDR;->TASK_GEOCODE_ADDRESS:LX/GDR;

    aput-object v1, v0, v2

    sget-object v1, LX/GDR;->TASK_REVERSE_GEOCODE:LX/GDR;

    aput-object v1, v0, v3

    sput-object v0, LX/GDR;->$VALUES:[LX/GDR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2329819
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GDR;
    .locals 1

    .prologue
    .line 2329820
    const-class v0, LX/GDR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GDR;

    return-object v0
.end method

.method public static values()[LX/GDR;
    .locals 1

    .prologue
    .line 2329821
    sget-object v0, LX/GDR;->$VALUES:[LX/GDR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GDR;

    return-object v0
.end method
