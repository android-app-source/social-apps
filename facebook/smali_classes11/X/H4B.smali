.class public final LX/H4B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/H4G;


# direct methods
.method public constructor <init>(LX/H4G;)V
    .locals 0

    .prologue
    .line 2422397
    iput-object p1, p0, LX/H4B;->a:LX/H4G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1f2b8df

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2422398
    iget-object v1, p0, LX/H4B;->a:LX/H4G;

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 2422399
    new-instance v7, LX/3Af;

    iget-object v3, v1, LX/H4G;->c:Landroid/content/Context;

    invoke-direct {v7, v3}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2422400
    new-instance v8, LX/7TY;

    iget-object v3, v1, LX/H4G;->c:Landroid/content/Context;

    invoke-direct {v8, v3}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2422401
    iput-boolean v4, v8, LX/7TY;->g:Z

    .line 2422402
    iget-object v3, v1, LX/H4G;->g:LX/0wM;

    const v5, 0x7f0207d6

    const p0, -0xa76f01

    invoke-virtual {v3, v5, p0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move v3, v4

    .line 2422403
    :goto_0
    sget-object v5, LX/H4G;->h:[I

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 2422404
    invoke-static {}, LX/H4F;->values()[LX/H4F;

    move-result-object v5

    aget-object p0, v5, v3

    .line 2422405
    sget-object v5, LX/H4G;->h:[I

    aget v5, v5, v3

    invoke-virtual {v8, v5}, LX/34c;->e(I)LX/3Ai;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object p1

    iget-object v5, v1, LX/H4G;->d:LX/H4F;

    if-ne p0, v5, :cond_0

    move v5, v6

    :goto_1
    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v5

    new-instance p1, LX/H4C;

    invoke-direct {p1, v1, p0}, LX/H4C;-><init>(LX/H4G;LX/H4F;)V

    invoke-interface {v5, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2422406
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move v5, v4

    .line 2422407
    goto :goto_1

    .line 2422408
    :cond_1
    invoke-virtual {v7, v8}, LX/3Af;->a(LX/1OM;)V

    .line 2422409
    invoke-virtual {v7}, LX/3Af;->show()V

    .line 2422410
    const v1, 0x787e6e0e

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
