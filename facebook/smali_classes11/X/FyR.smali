.class public final LX/FyR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V
    .locals 0

    .prologue
    .line 2306578
    iput-object p1, p0, LX/FyR;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0xaf9bd99

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2306579
    iget-object v1, p0, LX/FyR;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    invoke-static {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v1

    invoke-virtual {v1}, LX/D2H;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2306580
    iget-object v1, p0, LX/FyR;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    .line 2306581
    iget-object v3, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-virtual {v3}, LX/5SB;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2306582
    iget-object v3, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->o:LX/FyN;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    iget-object v6, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->w()Z

    move-result v6

    invoke-static {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->r(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v7

    invoke-static {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v8

    invoke-virtual {v8}, LX/D2H;->b()Z

    move-result v8

    iget-object v9, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v9}, LX/BQ1;->x()Z

    move-result v9

    new-instance v10, LX/FyS;

    invoke-direct {v10, v1}, LX/FyS;-><init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    invoke-virtual/range {v3 .. v10}, LX/FyN;->a(Landroid/content/Context;LX/5SB;ZZZZLandroid/view/MenuItem$OnMenuItemClickListener;)LX/5OM;

    move-result-object v3

    .line 2306583
    iget-object v4, v1, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    invoke-virtual {v3, v4}, LX/0ht;->a(Landroid/view/View;)V

    .line 2306584
    :goto_0
    const v1, 0x72f967ea

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2306585
    :cond_0
    iget-object v1, p0, LX/FyR;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    invoke-static {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->p(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    goto :goto_0

    .line 2306586
    :cond_1
    invoke-static {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->q(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V

    goto :goto_0
.end method
