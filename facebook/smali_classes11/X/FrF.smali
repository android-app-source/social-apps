.class public LX/FrF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FrC;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4V;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BPp;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:LX/BQ9;

.field private final e:LX/FrV;

.field private final f:LX/5SB;

.field public final g:LX/FrH;

.field private final h:LX/BQ1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BQ9;LX/5SB;LX/FrH;LX/FrV;LX/BQ1;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BQ9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/FrH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/FrV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BQ9;",
            "LX/5SB;",
            "LX/FrH;",
            "LX/FrV;",
            "LX/BQ1;",
            "LX/0Or",
            "<",
            "LX/G4V;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BPp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2295244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2295245
    iput-object p1, p0, LX/FrF;->c:Landroid/content/Context;

    .line 2295246
    iput-object p2, p0, LX/FrF;->d:LX/BQ9;

    .line 2295247
    iput-object p3, p0, LX/FrF;->f:LX/5SB;

    .line 2295248
    iput-object p4, p0, LX/FrF;->g:LX/FrH;

    .line 2295249
    iput-object p5, p0, LX/FrF;->e:LX/FrV;

    .line 2295250
    iput-object p6, p0, LX/FrF;->h:LX/BQ1;

    .line 2295251
    iput-object p7, p0, LX/FrF;->a:LX/0Or;

    .line 2295252
    iput-object p8, p0, LX/FrF;->b:LX/0Or;

    .line 2295253
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 2295254
    iget-object v0, p0, LX/FrF;->e:LX/FrV;

    if-eqz v0, :cond_0

    .line 2295255
    iget-object v0, p0, LX/FrF;->e:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->a()LX/Fra;

    move-result-object v0

    new-instance v1, LX/FrE;

    invoke-direct {v1, p0}, LX/FrE;-><init>(LX/FrF;)V

    .line 2295256
    iget-object v2, v0, LX/Fra;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FsG;

    iget-object v3, v0, LX/Fra;->b:LX/BP0;

    .line 2295257
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2295258
    iget-object v3, v0, LX/Fra;->b:LX/BP0;

    .line 2295259
    iget-object v6, v3, LX/BP0;->b:LX/0am;

    move-object v3, v6

    .line 2295260
    new-instance v6, LX/5w6;

    invoke-direct {v6}, LX/5w6;-><init>()V

    move-object v6, v6

    .line 2295261
    const-string v7, "profile_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v6

    :goto_0
    invoke-virtual {v7, v8, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    const-string v7, "render_location"

    const-string v8, "ANDROID_PROFILE_TILE"

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "top_context_item_type"

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v8, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/5w6;

    move-object v6, v6

    .line 2295262
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    sget-object v7, LX/0zS;->c:LX/0zS;

    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    .line 2295263
    iget-object v7, v2, LX/FsG;->a:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    move-object v2, v6

    .line 2295264
    invoke-static {v0}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/Fs2;->a(LX/1Zp;)V

    .line 2295265
    iget-object v3, v0, LX/Fra;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x1a002a

    invoke-interface {v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2295266
    invoke-static {v2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/FrZ;

    invoke-direct {v3, v0, v1}, LX/FrZ;-><init>(LX/Fra;LX/0Ve;)V

    iget-object v4, v0, LX/Fra;->h:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2295267
    iget-object v0, p0, LX/FrF;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->g()V

    .line 2295268
    iget-object v0, p0, LX/FrF;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BPp;

    new-instance v1, LX/BPI;

    invoke-direct {v1}, LX/BPI;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2295269
    :cond_0
    return-void

    :cond_1
    sget-object v6, LX/0wB;->a:LX/0wC;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;Z)V
    .locals 13
    .param p1    # Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2295270
    if-nez p1, :cond_0

    .line 2295271
    :goto_0
    return-void

    .line 2295272
    :cond_0
    iget-object v0, p0, LX/FrF;->f:LX/5SB;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    iget-object v1, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v3, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-static {v0, v1, v3}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v0

    .line 2295273
    iget-object v1, p0, LX/FrF;->d:LX/BQ9;

    iget-object v3, p0, LX/FrF;->f:LX/5SB;

    .line 2295274
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2295275
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2295276
    iget-object v6, v1, LX/BQ9;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0gh;

    const-string v7, "timeline_context_item"

    invoke-virtual {v6, v7}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2295277
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v10

    .line 2295278
    :goto_1
    const-string v7, "profile_context_item_click"

    move-object v6, v1

    move-wide v8, v4

    move-object v11, v0

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2295279
    invoke-static {v6, p1}, LX/BQ9;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V

    .line 2295280
    iget-object v7, v1, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2295281
    if-eqz p2, :cond_1

    .line 2295282
    iget-object v1, p0, LX/FrF;->d:LX/BQ9;

    iget-object v3, p0, LX/FrF;->f:LX/5SB;

    .line 2295283
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2295284
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2295285
    iget-object v6, v1, LX/BQ9;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0gh;

    const-string v7, "timeline_context_item"

    invoke-virtual {v6, v7}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2295286
    const/4 v10, 0x0

    const-string v12, "context_item_view_click"

    move-object v7, v1

    move-wide v8, v4

    move-object v11, v0

    invoke-static/range {v7 .. v12}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2295287
    if-eqz v6, :cond_1

    .line 2295288
    invoke-static {v6, p1}, LX/BQ9;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V

    .line 2295289
    iget-object v7, v1, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2295290
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v3

    .line 2295291
    iget-object v0, p0, LX/FrF;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4V;

    iget-object v4, p0, LX/FrF;->c:Landroid/content/Context;

    new-instance v5, LX/G4Y;

    invoke-direct {v5}, LX/G4Y;-><init>()V

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 2295292
    :goto_2
    iput v1, v5, LX/G4Y;->a:I

    .line 2295293
    move-object v5, v5

    .line 2295294
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2295295
    :goto_3
    iput-object v1, v5, LX/G4Y;->b:Ljava/lang/String;

    .line 2295296
    move-object v5, v5

    .line 2295297
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->e()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->e()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 2295298
    :goto_4
    iput-object v1, v5, LX/G4Y;->c:Ljava/lang/String;

    .line 2295299
    move-object v1, v5

    .line 2295300
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->c()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->c()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2295301
    :cond_2
    iput-object v2, v1, LX/G4Y;->d:Ljava/lang/String;

    .line 2295302
    move-object v1, v1

    .line 2295303
    sget-object v2, LX/74S;->TIMELINE_CONTEXT_ITEM:LX/74S;

    .line 2295304
    iput-object v2, v1, LX/G4Y;->e:LX/74S;

    .line 2295305
    move-object v1, v1

    .line 2295306
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v2

    .line 2295307
    iput-object v2, v1, LX/G4Y;->f:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 2295308
    move-object v1, v1

    .line 2295309
    iget-object v2, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v2

    .line 2295310
    iput-object v2, v1, LX/G4Y;->g:Ljava/lang/String;

    .line 2295311
    move-object v1, v1

    .line 2295312
    iget-object v2, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v2

    .line 2295313
    iput-object v2, v1, LX/G4Y;->h:Ljava/lang/String;

    .line 2295314
    move-object v1, v1

    .line 2295315
    iget-object v2, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v2

    .line 2295316
    iput-object v2, v1, LX/G4Y;->i:Ljava/lang/String;

    .line 2295317
    move-object v1, v1

    .line 2295318
    iget-object v2, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->Y()LX/2rX;

    move-result-object v2

    .line 2295319
    iput-object v2, v1, LX/G4Y;->j:LX/2rX;

    .line 2295320
    move-object v1, v1

    .line 2295321
    const-string v2, "timeline_context_item"

    .line 2295322
    iput-object v2, v1, LX/G4Y;->k:Ljava/lang/String;

    .line 2295323
    move-object v1, v1

    .line 2295324
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2295325
    iput-object v2, v1, LX/G4Y;->l:Ljava/lang/String;

    .line 2295326
    move-object v1, v1

    .line 2295327
    iget-object v2, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2295328
    iput-object v2, v1, LX/G4Y;->m:Ljava/lang/String;

    .line 2295329
    move-object v1, v1

    .line 2295330
    iget-object v2, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2295331
    iput-object v2, v1, LX/G4Y;->n:Ljava/lang/String;

    .line 2295332
    move-object v1, v1

    .line 2295333
    iget-object v2, p0, LX/FrF;->h:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v2

    .line 2295334
    iput-object v2, v1, LX/G4Y;->o:Ljava/lang/String;

    .line 2295335
    move-object v1, v1

    .line 2295336
    invoke-virtual {v1}, LX/G4Y;->a()LX/G4Z;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, LX/G4V;->a(Landroid/content/Context;LX/G4Z;)V

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_4
    move-object v1, v2

    goto/16 :goto_3

    :cond_5
    move-object v1, v2

    goto/16 :goto_4

    .line 2295337
    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_1
.end method
