.class public LX/G9p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G96;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/G9m;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field private final d:[I

.field private final e:LX/G93;


# direct methods
.method public constructor <init>(LX/G96;LX/G93;)V
    .locals 1

    .prologue
    .line 2323858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2323859
    iput-object p1, p0, LX/G9p;->a:LX/G96;

    .line 2323860
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/G9p;->b:Ljava/util/List;

    .line 2323861
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, LX/G9p;->d:[I

    .line 2323862
    iput-object p2, p0, LX/G9p;->e:LX/G93;

    .line 2323863
    return-void
.end method

.method private static a([II)F
    .locals 3

    .prologue
    .line 2323857
    const/4 v0, 0x4

    aget v0, p0, v0

    sub-int v0, p1, v0

    const/4 v1, 0x3

    aget v1, p0, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x2

    aget v1, p0, v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method private static a(LX/G9p;[IIIZ)Z
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 2323780
    aget v0, p1, v2

    aget v1, p1, v3

    add-int/2addr v0, v1

    aget v1, p1, v7

    add-int/2addr v0, v1

    const/4 v1, 0x3

    aget v1, p1, v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    aget v1, p1, v1

    add-int/2addr v0, v1

    .line 2323781
    invoke-static {p1, p3}, LX/G9p;->a([II)F

    move-result v1

    .line 2323782
    float-to-int v4, v1

    aget v5, p1, v7

    invoke-direct {p0, p2, v4, v5, v0}, LX/G9p;->b(IIII)F

    move-result v4

    .line 2323783
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_7

    .line 2323784
    float-to-int v1, v1

    float-to-int v5, v4

    aget v6, p1, v7

    invoke-direct {p0, v1, v5, v6, v0}, LX/G9p;->c(IIII)F

    move-result v5

    .line 2323785
    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz p4, :cond_2

    float-to-int v1, v4

    float-to-int v6, v5

    aget v7, p1, v7

    .line 2323786
    invoke-static {p0}, LX/G9p;->a(LX/G9p;)[I

    move-result-object v9

    .line 2323787
    const/4 v8, 0x0

    .line 2323788
    :goto_0
    if-lt v1, v8, :cond_0

    if-lt v6, v8, :cond_0

    iget-object v10, p0, LX/G9p;->a:LX/G96;

    sub-int p1, v6, v8

    sub-int p2, v1, v8

    invoke-virtual {v10, p1, p2}, LX/G96;->a(II)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2323789
    const/4 v10, 0x2

    aget p1, v9, v10

    add-int/lit8 p1, p1, 0x1

    aput p1, v9, v10

    .line 2323790
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 2323791
    :cond_0
    if-lt v1, v8, :cond_1

    if-ge v6, v8, :cond_9

    .line 2323792
    :cond_1
    const/4 v8, 0x0

    .line 2323793
    :goto_1
    move v1, v8

    .line 2323794
    if-eqz v1, :cond_7

    .line 2323795
    :cond_2
    int-to-float v0, v0

    const/high16 v1, 0x40e00000    # 7.0f

    div-float v6, v0, v1

    move v1, v2

    .line 2323796
    :goto_2
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2323797
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    .line 2323798
    const/4 v7, 0x0

    .line 2323799
    iget v8, v0, LX/G92;->b:F

    move v8, v8

    .line 2323800
    sub-float v8, v4, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v6

    if-gtz v8, :cond_4

    .line 2323801
    iget v8, v0, LX/G92;->a:F

    move v8, v8

    .line 2323802
    sub-float v8, v5, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v6

    if-gtz v8, :cond_4

    .line 2323803
    iget v8, v0, LX/G9m;->a:F

    sub-float v8, v6, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    .line 2323804
    const/high16 v9, 0x3f800000    # 1.0f

    cmpg-float v9, v8, v9

    if-lez v9, :cond_3

    iget v9, v0, LX/G9m;->a:F

    cmpg-float v8, v8, v9

    if-gtz v8, :cond_4

    :cond_3
    const/4 v7, 0x1

    .line 2323805
    :cond_4
    move v7, v7

    .line 2323806
    if-eqz v7, :cond_8

    .line 2323807
    iget-object v2, p0, LX/G9p;->b:Ljava/util/List;

    .line 2323808
    iget v7, v0, LX/G9m;->b:I

    add-int/lit8 v7, v7, 0x1

    .line 2323809
    iget v8, v0, LX/G9m;->b:I

    int-to-float v8, v8

    .line 2323810
    iget v9, v0, LX/G92;->a:F

    move v9, v9

    .line 2323811
    mul-float/2addr v8, v9

    add-float/2addr v8, v5

    int-to-float v9, v7

    div-float/2addr v8, v9

    .line 2323812
    iget v9, v0, LX/G9m;->b:I

    int-to-float v9, v9

    .line 2323813
    iget v10, v0, LX/G92;->b:F

    move v10, v10

    .line 2323814
    mul-float/2addr v9, v10

    add-float/2addr v9, v4

    int-to-float v10, v7

    div-float/2addr v9, v10

    .line 2323815
    iget v10, v0, LX/G9m;->b:I

    int-to-float v10, v10

    iget p1, v0, LX/G9m;->a:F

    mul-float/2addr v10, p1

    add-float/2addr v10, v6

    int-to-float p1, v7

    div-float/2addr v10, p1

    .line 2323816
    new-instance p1, LX/G9m;

    invoke-direct {p1, v8, v9, v10, v7}, LX/G9m;-><init>(FFFI)V

    move-object v0, p1

    .line 2323817
    invoke-interface {v2, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    .line 2323818
    :cond_5
    if-nez v2, :cond_6

    .line 2323819
    new-instance v0, LX/G9m;

    invoke-direct {v0, v5, v4, v6}, LX/G9m;-><init>(FFF)V

    .line 2323820
    iget-object v1, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    move v2, v3

    .line 2323821
    :cond_7
    return v2

    .line 2323822
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 2323823
    :cond_9
    :goto_3
    if-lt v1, v8, :cond_a

    if-lt v6, v8, :cond_a

    iget-object v10, p0, LX/G9p;->a:LX/G96;

    sub-int p1, v6, v8

    sub-int p2, v1, v8

    invoke-virtual {v10, p1, p2}, LX/G96;->a(II)Z

    move-result v10

    if-nez v10, :cond_a

    const/4 v10, 0x1

    aget v10, v9, v10

    if-gt v10, v7, :cond_a

    .line 2323824
    const/4 v10, 0x1

    aget p1, v9, v10

    add-int/lit8 p1, p1, 0x1

    aput p1, v9, v10

    .line 2323825
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 2323826
    :cond_a
    if-lt v1, v8, :cond_b

    if-lt v6, v8, :cond_b

    const/4 v10, 0x1

    aget v10, v9, v10

    if-le v10, v7, :cond_c

    .line 2323827
    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 2323828
    :cond_c
    :goto_4
    if-lt v1, v8, :cond_d

    if-lt v6, v8, :cond_d

    iget-object v10, p0, LX/G9p;->a:LX/G96;

    sub-int p1, v6, v8

    sub-int p2, v1, v8

    invoke-virtual {v10, p1, p2}, LX/G96;->a(II)Z

    move-result v10

    if-eqz v10, :cond_d

    const/4 v10, 0x0

    aget v10, v9, v10

    if-gt v10, v7, :cond_d

    .line 2323829
    const/4 v10, 0x0

    aget p1, v9, v10

    add-int/lit8 p1, p1, 0x1

    aput p1, v9, v10

    .line 2323830
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 2323831
    :cond_d
    const/4 v8, 0x0

    aget v8, v9, v8

    if-le v8, v7, :cond_e

    .line 2323832
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 2323833
    :cond_e
    iget-object v8, p0, LX/G9p;->a:LX/G96;

    .line 2323834
    iget v10, v8, LX/G96;->b:I

    move v10, v10

    .line 2323835
    iget-object v8, p0, LX/G9p;->a:LX/G96;

    .line 2323836
    iget p1, v8, LX/G96;->a:I

    move p1, p1

    .line 2323837
    const/4 v8, 0x1

    .line 2323838
    :goto_5
    add-int p2, v1, v8

    if-ge p2, v10, :cond_f

    add-int p2, v6, v8

    if-ge p2, p1, :cond_f

    iget-object p2, p0, LX/G9p;->a:LX/G96;

    add-int p3, v6, v8

    add-int p4, v1, v8

    invoke-virtual {p2, p3, p4}, LX/G96;->a(II)Z

    move-result p2

    if-eqz p2, :cond_f

    .line 2323839
    const/4 p2, 0x2

    aget p3, v9, p2

    add-int/lit8 p3, p3, 0x1

    aput p3, v9, p2

    .line 2323840
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 2323841
    :cond_f
    add-int p2, v1, v8

    if-ge p2, v10, :cond_10

    add-int p2, v6, v8

    if-lt p2, p1, :cond_11

    .line 2323842
    :cond_10
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 2323843
    :cond_11
    :goto_6
    add-int p2, v1, v8

    if-ge p2, v10, :cond_12

    add-int p2, v6, v8

    if-ge p2, p1, :cond_12

    iget-object p2, p0, LX/G9p;->a:LX/G96;

    add-int p3, v6, v8

    add-int p4, v1, v8

    invoke-virtual {p2, p3, p4}, LX/G96;->a(II)Z

    move-result p2

    if-nez p2, :cond_12

    const/4 p2, 0x3

    aget p2, v9, p2

    if-ge p2, v7, :cond_12

    .line 2323844
    const/4 p2, 0x3

    aget p3, v9, p2

    add-int/lit8 p3, p3, 0x1

    aput p3, v9, p2

    .line 2323845
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 2323846
    :cond_12
    add-int p2, v1, v8

    if-ge p2, v10, :cond_13

    add-int p2, v6, v8

    if-ge p2, p1, :cond_13

    const/4 p2, 0x3

    aget p2, v9, p2

    if-lt p2, v7, :cond_14

    .line 2323847
    :cond_13
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 2323848
    :cond_14
    :goto_7
    add-int p2, v1, v8

    if-ge p2, v10, :cond_15

    add-int p2, v6, v8

    if-ge p2, p1, :cond_15

    iget-object p2, p0, LX/G9p;->a:LX/G96;

    add-int p3, v6, v8

    add-int p4, v1, v8

    invoke-virtual {p2, p3, p4}, LX/G96;->a(II)Z

    move-result p2

    if-eqz p2, :cond_15

    const/4 p2, 0x4

    aget p2, v9, p2

    if-ge p2, v7, :cond_15

    .line 2323849
    const/4 p2, 0x4

    aget p3, v9, p2

    add-int/lit8 p3, p3, 0x1

    aput p3, v9, p2

    .line 2323850
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 2323851
    :cond_15
    const/4 v8, 0x4

    aget v8, v9, v8

    if-lt v8, v7, :cond_16

    .line 2323852
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 2323853
    :cond_16
    const/4 v8, 0x0

    aget v8, v9, v8

    const/4 v10, 0x1

    aget v10, v9, v10

    add-int/2addr v8, v10

    const/4 v10, 0x2

    aget v10, v9, v10

    add-int/2addr v8, v10

    const/4 v10, 0x3

    aget v10, v9, v10

    add-int/2addr v8, v10

    const/4 v10, 0x4

    aget v10, v9, v10

    add-int/2addr v8, v10

    .line 2323854
    sub-int/2addr v8, v0

    .line 2323855
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    mul-int/lit8 v10, v0, 0x2

    if-ge v8, v10, :cond_17

    .line 2323856
    invoke-static {v9}, LX/G9p;->a([I)Z

    move-result v8

    if-eqz v8, :cond_17

    const/4 v8, 0x1

    goto/16 :goto_1

    :cond_17
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method public static a([I)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/high16 v6, 0x40400000    # 3.0f

    const/4 v0, 0x0

    .line 2323764
    move v2, v0

    move v3, v0

    .line 2323765
    :goto_0
    const/4 v4, 0x5

    if-ge v2, v4, :cond_2

    .line 2323766
    aget v4, p0, v2

    .line 2323767
    if-nez v4, :cond_1

    .line 2323768
    :cond_0
    :goto_1
    return v0

    .line 2323769
    :cond_1
    add-int/2addr v3, v4

    .line 2323770
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2323771
    :cond_2
    const/4 v2, 0x7

    if-lt v3, v2, :cond_0

    .line 2323772
    int-to-float v2, v3

    const/high16 v3, 0x40e00000    # 7.0f

    div-float/2addr v2, v3

    .line 2323773
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v2, v3

    .line 2323774
    aget v4, p0, v0

    int-to-float v4, v4

    sub-float v4, v2, v4

    .line 2323775
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v3

    if-gez v4, :cond_0

    aget v4, p0, v1

    int-to-float v4, v4

    sub-float v4, v2, v4

    .line 2323776
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v3

    if-gez v4, :cond_0

    mul-float v4, v6, v2

    const/4 v5, 0x2

    aget v5, p0, v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 2323777
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float v5, v6, v3

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    const/4 v4, 0x3

    aget v4, p0, v4

    int-to-float v4, v4

    sub-float v4, v2, v4

    .line 2323778
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v3

    if-gez v4, :cond_0

    const/4 v4, 0x4

    aget v4, p0, v4

    int-to-float v4, v4

    sub-float/2addr v2, v4

    .line 2323779
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/G9p;)[I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2323758
    iget-object v0, p0, LX/G9p;->d:[I

    aput v2, v0, v2

    .line 2323759
    iget-object v0, p0, LX/G9p;->d:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 2323760
    iget-object v0, p0, LX/G9p;->d:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 2323761
    iget-object v0, p0, LX/G9p;->d:[I

    const/4 v1, 0x3

    aput v2, v0, v1

    .line 2323762
    iget-object v0, p0, LX/G9p;->d:[I

    const/4 v1, 0x4

    aput v2, v0, v1

    .line 2323763
    iget-object v0, p0, LX/G9p;->d:[I

    return-object v0
.end method

.method private b(IIII)F
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v0, 0x7fc00000    # NaNf

    .line 2323536
    iget-object v2, p0, LX/G9p;->a:LX/G96;

    .line 2323537
    iget v1, v2, LX/G96;->b:I

    move v3, v1

    .line 2323538
    invoke-static {p0}, LX/G9p;->a(LX/G9p;)[I

    move-result-object v4

    move v1, p1

    .line 2323539
    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2323540
    const/4 v5, 0x2

    aget v6, v4, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v4, v5

    .line 2323541
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2323542
    :cond_0
    if-gez v1, :cond_2

    .line 2323543
    :cond_1
    :goto_1
    return v0

    .line 2323544
    :cond_2
    :goto_2
    if-ltz v1, :cond_3

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-nez v5, :cond_3

    aget v5, v4, v8

    if-gt v5, p3, :cond_3

    .line 2323545
    aget v5, v4, v8

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v8

    .line 2323546
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 2323547
    :cond_3
    if-ltz v1, :cond_1

    aget v5, v4, v8

    if-gt v5, p3, :cond_1

    .line 2323548
    :goto_3
    if-ltz v1, :cond_4

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_4

    aget v5, v4, v7

    if-gt v5, p3, :cond_4

    .line 2323549
    aget v5, v4, v7

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v7

    .line 2323550
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 2323551
    :cond_4
    aget v1, v4, v7

    if-gt v1, p3, :cond_1

    .line 2323552
    add-int/lit8 v1, p1, 0x1

    .line 2323553
    :goto_4
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2323554
    const/4 v5, 0x2

    aget v6, v4, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v4, v5

    .line 2323555
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2323556
    :cond_5
    if-eq v1, v3, :cond_1

    .line 2323557
    :goto_5
    if-ge v1, v3, :cond_6

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-nez v5, :cond_6

    aget v5, v4, v9

    if-ge v5, p3, :cond_6

    .line 2323558
    aget v5, v4, v9

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v9

    .line 2323559
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2323560
    :cond_6
    if-eq v1, v3, :cond_1

    aget v5, v4, v9

    if-ge v5, p3, :cond_1

    .line 2323561
    :goto_6
    if-ge v1, v3, :cond_7

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_7

    aget v5, v4, v10

    if-ge v5, p3, :cond_7

    .line 2323562
    aget v5, v4, v10

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v10

    .line 2323563
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2323564
    :cond_7
    aget v2, v4, v10

    if-ge v2, p3, :cond_1

    .line 2323565
    aget v2, v4, v7

    aget v3, v4, v8

    add-int/2addr v2, v3

    const/4 v3, 0x2

    aget v3, v4, v3

    add-int/2addr v2, v3

    aget v3, v4, v9

    add-int/2addr v2, v3

    aget v3, v4, v10

    add-int/2addr v2, v3

    .line 2323566
    sub-int/2addr v2, p4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    mul-int/lit8 v3, p4, 0x2

    if-ge v2, v3, :cond_1

    .line 2323567
    invoke-static {v4}, LX/G9p;->a([I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v4, v1}, LX/G9p;->a([II)F

    move-result v0

    goto/16 :goto_1
.end method

.method private c(IIII)F
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v0, 0x7fc00000    # NaNf

    .line 2323726
    iget-object v2, p0, LX/G9p;->a:LX/G96;

    .line 2323727
    iget v1, v2, LX/G96;->a:I

    move v3, v1

    .line 2323728
    invoke-static {p0}, LX/G9p;->a(LX/G9p;)[I

    move-result-object v4

    move v1, p1

    .line 2323729
    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {v2, v1, p2}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2323730
    const/4 v5, 0x2

    aget v6, v4, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v4, v5

    .line 2323731
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2323732
    :cond_0
    if-gez v1, :cond_2

    .line 2323733
    :cond_1
    :goto_1
    return v0

    .line 2323734
    :cond_2
    :goto_2
    if-ltz v1, :cond_3

    invoke-virtual {v2, v1, p2}, LX/G96;->a(II)Z

    move-result v5

    if-nez v5, :cond_3

    aget v5, v4, v8

    if-gt v5, p3, :cond_3

    .line 2323735
    aget v5, v4, v8

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v8

    .line 2323736
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 2323737
    :cond_3
    if-ltz v1, :cond_1

    aget v5, v4, v8

    if-gt v5, p3, :cond_1

    .line 2323738
    :goto_3
    if-ltz v1, :cond_4

    invoke-virtual {v2, v1, p2}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_4

    aget v5, v4, v7

    if-gt v5, p3, :cond_4

    .line 2323739
    aget v5, v4, v7

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v7

    .line 2323740
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 2323741
    :cond_4
    aget v1, v4, v7

    if-gt v1, p3, :cond_1

    .line 2323742
    add-int/lit8 v1, p1, 0x1

    .line 2323743
    :goto_4
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, v1, p2}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2323744
    const/4 v5, 0x2

    aget v6, v4, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v4, v5

    .line 2323745
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2323746
    :cond_5
    if-eq v1, v3, :cond_1

    .line 2323747
    :goto_5
    if-ge v1, v3, :cond_6

    invoke-virtual {v2, v1, p2}, LX/G96;->a(II)Z

    move-result v5

    if-nez v5, :cond_6

    aget v5, v4, v9

    if-ge v5, p3, :cond_6

    .line 2323748
    aget v5, v4, v9

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v9

    .line 2323749
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2323750
    :cond_6
    if-eq v1, v3, :cond_1

    aget v5, v4, v9

    if-ge v5, p3, :cond_1

    .line 2323751
    :goto_6
    if-ge v1, v3, :cond_7

    invoke-virtual {v2, v1, p2}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_7

    aget v5, v4, v10

    if-ge v5, p3, :cond_7

    .line 2323752
    aget v5, v4, v10

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v10

    .line 2323753
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2323754
    :cond_7
    aget v2, v4, v10

    if-ge v2, p3, :cond_1

    .line 2323755
    aget v2, v4, v7

    aget v3, v4, v8

    add-int/2addr v2, v3

    const/4 v3, 0x2

    aget v3, v4, v3

    add-int/2addr v2, v3

    aget v3, v4, v9

    add-int/2addr v2, v3

    aget v3, v4, v10

    add-int/2addr v2, v3

    .line 2323756
    sub-int/2addr v2, p4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    if-ge v2, p4, :cond_1

    .line 2323757
    invoke-static {v4}, LX/G9p;->a([I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v4, v1}, LX/G9p;->a([II)F

    move-result v0

    goto/16 :goto_1
.end method

.method private static c(LX/G9p;)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 2323710
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 2323711
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    .line 2323712
    iget v7, v0, LX/G9m;->b:I

    move v7, v7

    .line 2323713
    const/4 v8, 0x2

    if-lt v7, v8, :cond_4

    .line 2323714
    add-int/lit8 v3, v3, 0x1

    .line 2323715
    iget v7, v0, LX/G9m;->a:F

    move v0, v7

    .line 2323716
    add-float/2addr v0, v1

    move v1, v3

    :goto_1
    move v3, v1

    move v1, v0

    .line 2323717
    goto :goto_0

    .line 2323718
    :cond_0
    const/4 v0, 0x3

    if-ge v3, v0, :cond_2

    .line 2323719
    :cond_1
    :goto_2
    return v4

    .line 2323720
    :cond_2
    int-to-float v0, v5

    div-float v3, v1, v0

    .line 2323721
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    .line 2323722
    iget v6, v0, LX/G9m;->a:F

    move v0, v6

    .line 2323723
    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    add-float/2addr v2, v0

    .line 2323724
    goto :goto_3

    .line 2323725
    :cond_3
    const v0, 0x3d4ccccd    # 0.05f

    mul-float/2addr v0, v1

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_1

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    move v0, v1

    move v1, v3

    goto :goto_1
.end method

.method private static d(LX/G9p;)[LX/G9m;
    .locals 11

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x3

    .line 2323677
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 2323678
    if-ge v5, v7, :cond_0

    .line 2323679
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2323680
    throw v0

    .line 2323681
    :cond_0
    if-le v5, v7, :cond_3

    .line 2323682
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    .line 2323683
    iget v10, v0, LX/G9m;->a:F

    move v0, v10

    .line 2323684
    add-float/2addr v3, v0

    .line 2323685
    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    move v1, v0

    .line 2323686
    goto :goto_0

    .line 2323687
    :cond_1
    int-to-float v0, v5

    div-float/2addr v3, v0

    .line 2323688
    int-to-float v0, v5

    div-float v0, v1, v0

    mul-float v1, v3, v3

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 2323689
    iget-object v1, p0, LX/G9p;->b:Ljava/util/List;

    new-instance v5, LX/G9o;

    invoke-direct {v5, v3}, LX/G9o;-><init>(F)V

    invoke-static {v1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2323690
    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v5

    move v1, v4

    .line 2323691
    :goto_1
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v7, :cond_3

    .line 2323692
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    .line 2323693
    iget v6, v0, LX/G9m;->a:F

    move v0, v6

    .line 2323694
    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_2

    .line 2323695
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2323696
    add-int/lit8 v1, v1, -0x1

    .line 2323697
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2323698
    :cond_3
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v7, :cond_5

    .line 2323699
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    .line 2323700
    iget v3, v0, LX/G9m;->a:F

    move v0, v3

    .line 2323701
    add-float/2addr v2, v0

    .line 2323702
    goto :goto_2

    .line 2323703
    :cond_4
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 2323704
    iget-object v1, p0, LX/G9p;->b:Ljava/util/List;

    new-instance v2, LX/G9n;

    invoke-direct {v2, v0}, LX/G9n;-><init>(F)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2323705
    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    iget-object v1, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v7, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2323706
    :cond_5
    new-array v1, v7, [LX/G9m;

    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    .line 2323707
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    aput-object v0, v1, v4

    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    .line 2323708
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    aput-object v0, v1, v8

    iget-object v0, p0, LX/G9p;->b:Ljava/util/List;

    .line 2323709
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9m;

    aput-object v0, v1, v9

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/util/Map;)LX/G9q;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/G8t;",
            "*>;)",
            "LX/G9q;"
        }
    .end annotation

    .prologue
    .line 2323568
    if-eqz p1, :cond_3

    sget-object v0, LX/G8t;->TRY_HARDER:LX/G8t;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v2, v0

    .line 2323569
    :goto_0
    if-eqz p1, :cond_4

    sget-object v0, LX/G8t;->PURE_BARCODE:LX/G8t;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 2323570
    :goto_1
    iget-object v1, p0, LX/G9p;->a:LX/G96;

    .line 2323571
    iget v3, v1, LX/G96;->b:I

    move v6, v3

    .line 2323572
    iget-object v1, p0, LX/G9p;->a:LX/G96;

    .line 2323573
    iget v3, v1, LX/G96;->a:I

    move v7, v3

    .line 2323574
    mul-int/lit8 v1, v6, 0x3

    div-int/lit16 v1, v1, 0xe4

    .line 2323575
    const/4 v3, 0x3

    if-lt v1, v3, :cond_0

    if-eqz v2, :cond_1

    .line 2323576
    :cond_0
    const/4 v1, 0x3

    .line 2323577
    :cond_1
    const/4 v4, 0x0

    .line 2323578
    const/4 v2, 0x5

    new-array v8, v2, [I

    .line 2323579
    add-int/lit8 v3, v1, -0x1

    move v5, v1

    :goto_2
    if-ge v3, v6, :cond_d

    if-nez v4, :cond_d

    .line 2323580
    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v8, v1

    .line 2323581
    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v8, v1

    .line 2323582
    const/4 v1, 0x2

    const/4 v2, 0x0

    aput v2, v8, v1

    .line 2323583
    const/4 v1, 0x3

    const/4 v2, 0x0

    aput v2, v8, v1

    .line 2323584
    const/4 v1, 0x4

    const/4 v2, 0x0

    aput v2, v8, v1

    .line 2323585
    const/4 v1, 0x0

    .line 2323586
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v7, :cond_b

    .line 2323587
    iget-object v9, p0, LX/G9p;->a:LX/G96;

    invoke-virtual {v9, v2, v3}, LX/G96;->a(II)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2323588
    and-int/lit8 v9, v1, 0x1

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    .line 2323589
    add-int/lit8 v1, v1, 0x1

    .line 2323590
    :cond_2
    aget v9, v8, v1

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v1

    .line 2323591
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2323592
    :cond_3
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 2323593
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2323594
    :cond_5
    and-int/lit8 v9, v1, 0x1

    if-nez v9, :cond_a

    .line 2323595
    const/4 v9, 0x4

    if-ne v1, v9, :cond_9

    .line 2323596
    invoke-static {v8}, LX/G9p;->a([I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2323597
    invoke-static {p0, v8, v3, v2, v0}, LX/G9p;->a(LX/G9p;[IIIZ)Z

    move-result v1

    .line 2323598
    if-eqz v1, :cond_7

    .line 2323599
    const/4 v5, 0x2

    .line 2323600
    iget-boolean v1, p0, LX/G9p;->c:Z

    if-eqz v1, :cond_6

    .line 2323601
    invoke-static {p0}, LX/G9p;->c(LX/G9p;)Z

    move-result v1

    .line 2323602
    :goto_5
    const/4 v4, 0x0

    .line 2323603
    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 2323604
    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 2323605
    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 2323606
    const/4 v9, 0x3

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 2323607
    const/4 v9, 0x4

    const/4 v10, 0x0

    aput v10, v8, v9

    move v11, v4

    move v4, v1

    move v1, v11

    .line 2323608
    goto :goto_4

    .line 2323609
    :cond_6
    const/4 p1, 0x1

    const/4 v10, 0x0

    .line 2323610
    iget-object v1, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 2323611
    if-gt v1, p1, :cond_f

    move v1, v10

    .line 2323612
    :goto_6
    move v1, v1

    .line 2323613
    const/4 v9, 0x2

    aget v9, v8, v9

    if-le v1, v9, :cond_e

    .line 2323614
    const/4 v2, 0x2

    aget v2, v8, v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x2

    add-int v2, v3, v1

    .line 2323615
    add-int/lit8 v1, v7, -0x1

    :goto_7
    move v3, v2

    move v2, v1

    move v1, v4

    .line 2323616
    goto :goto_5

    .line 2323617
    :cond_7
    const/4 v1, 0x0

    const/4 v9, 0x2

    aget v9, v8, v9

    aput v9, v8, v1

    .line 2323618
    const/4 v1, 0x1

    const/4 v9, 0x3

    aget v9, v8, v9

    aput v9, v8, v1

    .line 2323619
    const/4 v1, 0x2

    const/4 v9, 0x4

    aget v9, v8, v9

    aput v9, v8, v1

    .line 2323620
    const/4 v1, 0x3

    const/4 v9, 0x1

    aput v9, v8, v1

    .line 2323621
    const/4 v1, 0x4

    const/4 v9, 0x0

    aput v9, v8, v1

    .line 2323622
    const/4 v1, 0x3

    .line 2323623
    goto :goto_4

    .line 2323624
    :cond_8
    const/4 v1, 0x0

    const/4 v9, 0x2

    aget v9, v8, v9

    aput v9, v8, v1

    .line 2323625
    const/4 v1, 0x1

    const/4 v9, 0x3

    aget v9, v8, v9

    aput v9, v8, v1

    .line 2323626
    const/4 v1, 0x2

    const/4 v9, 0x4

    aget v9, v8, v9

    aput v9, v8, v1

    .line 2323627
    const/4 v1, 0x3

    const/4 v9, 0x1

    aput v9, v8, v1

    .line 2323628
    const/4 v1, 0x4

    const/4 v9, 0x0

    aput v9, v8, v1

    .line 2323629
    const/4 v1, 0x3

    goto/16 :goto_4

    .line 2323630
    :cond_9
    add-int/lit8 v1, v1, 0x1

    aget v9, v8, v1

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v1

    goto/16 :goto_4

    .line 2323631
    :cond_a
    aget v9, v8, v1

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v1

    goto/16 :goto_4

    .line 2323632
    :cond_b
    invoke-static {v8}, LX/G9p;->a([I)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2323633
    invoke-static {p0, v8, v3, v7, v0}, LX/G9p;->a(LX/G9p;[IIIZ)Z

    move-result v1

    .line 2323634
    if-eqz v1, :cond_c

    .line 2323635
    const/4 v1, 0x0

    aget v5, v8, v1

    .line 2323636
    iget-boolean v1, p0, LX/G9p;->c:Z

    if-eqz v1, :cond_c

    .line 2323637
    invoke-static {p0}, LX/G9p;->c(LX/G9p;)Z

    move-result v4

    .line 2323638
    :cond_c
    add-int/2addr v3, v5

    goto/16 :goto_2

    .line 2323639
    :cond_d
    invoke-static {p0}, LX/G9p;->d(LX/G9p;)[LX/G9m;

    move-result-object v0

    .line 2323640
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2323641
    aget-object v1, v0, v6

    aget-object v2, v0, v7

    invoke-static {v1, v2}, LX/G92;->a(LX/G92;LX/G92;)F

    move-result v1

    .line 2323642
    aget-object v2, v0, v7

    aget-object v3, v0, v8

    invoke-static {v2, v3}, LX/G92;->a(LX/G92;LX/G92;)F

    move-result v2

    .line 2323643
    aget-object v3, v0, v6

    aget-object v4, v0, v8

    invoke-static {v3, v4}, LX/G92;->a(LX/G92;LX/G92;)F

    move-result v3

    .line 2323644
    cmpl-float v4, v2, v1

    if-ltz v4, :cond_13

    cmpl-float v4, v2, v3

    if-ltz v4, :cond_13

    .line 2323645
    aget-object v3, v0, v6

    .line 2323646
    aget-object v2, v0, v7

    .line 2323647
    aget-object v1, v0, v8

    .line 2323648
    :goto_8
    iget v4, v3, LX/G92;->a:F

    .line 2323649
    iget v5, v3, LX/G92;->b:F

    .line 2323650
    iget v9, v1, LX/G92;->a:F

    sub-float/2addr v9, v4

    iget v10, v2, LX/G92;->b:F

    sub-float/2addr v10, v5

    mul-float/2addr v9, v10

    iget v10, v1, LX/G92;->b:F

    sub-float v5, v10, v5

    iget v10, v2, LX/G92;->a:F

    sub-float v4, v10, v4

    mul-float/2addr v4, v5

    sub-float v4, v9, v4

    move v4, v4

    .line 2323651
    const/4 v5, 0x0

    cmpg-float v4, v4, v5

    if-gez v4, :cond_15

    .line 2323652
    :goto_9
    aput-object v1, v0, v6

    .line 2323653
    aput-object v3, v0, v7

    .line 2323654
    aput-object v2, v0, v8

    .line 2323655
    new-instance v1, LX/G9q;

    invoke-direct {v1, v0}, LX/G9q;-><init>([LX/G9m;)V

    return-object v1

    :cond_e
    move v1, v2

    move v2, v3

    goto/16 :goto_7

    .line 2323656
    :cond_f
    const/4 v1, 0x0

    .line 2323657
    iget-object v9, p0, LX/G9p;->b:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-object v9, v1

    :cond_10
    :goto_a
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G9m;

    .line 2323658
    iget v12, v1, LX/G9m;->b:I

    move v12, v12

    .line 2323659
    const/4 v13, 0x2

    if-lt v12, v13, :cond_10

    .line 2323660
    if-nez v9, :cond_11

    move-object v9, v1

    .line 2323661
    goto :goto_a

    .line 2323662
    :cond_11
    iput-boolean p1, p0, LX/G9p;->c:Z

    .line 2323663
    iget v10, v9, LX/G92;->a:F

    move v10, v10

    .line 2323664
    iget v11, v1, LX/G92;->a:F

    move v11, v11

    .line 2323665
    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    .line 2323666
    iget v11, v9, LX/G92;->b:F

    move v9, v11

    .line 2323667
    iget v11, v1, LX/G92;->b:F

    move v1, v11

    .line 2323668
    sub-float v1, v9, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v10, v1

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    goto/16 :goto_6

    :cond_12
    move v1, v10

    .line 2323669
    goto/16 :goto_6

    .line 2323670
    :cond_13
    cmpl-float v2, v3, v2

    if-ltz v2, :cond_14

    cmpl-float v1, v3, v1

    if-ltz v1, :cond_14

    .line 2323671
    aget-object v3, v0, v7

    .line 2323672
    aget-object v2, v0, v6

    .line 2323673
    aget-object v1, v0, v8

    goto :goto_8

    .line 2323674
    :cond_14
    aget-object v3, v0, v8

    .line 2323675
    aget-object v2, v0, v6

    .line 2323676
    aget-object v1, v0, v7

    goto/16 :goto_8

    :cond_15
    move-object v9, v1

    move-object v1, v2

    move-object v2, v9

    goto :goto_9
.end method
