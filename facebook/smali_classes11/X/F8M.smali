.class public LX/F8M;
.super LX/1OM;
.source ""

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Landroid/widget/Filterable;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F8P;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/F8j;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0W9;

.field public final e:LX/F6f;

.field private final f:Landroid/content/Context;

.field public g:I

.field public h:Z

.field public i:LX/F8L;

.field public j:LX/1DI;

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F8P;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/widget/Filter;


# direct methods
.method public constructor <init>(LX/F8k;LX/F6f;LX/0W9;LX/89v;Landroid/content/Context;)V
    .locals 8
    .param p4    # LX/89v;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2203962
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2203963
    iput v0, p0, LX/F8M;->g:I

    .line 2203964
    iput-boolean v0, p0, LX/F8M;->h:Z

    .line 2203965
    sget-object v0, LX/F8L;->DEFAULT:LX/F8L;

    iput-object v0, p0, LX/F8M;->i:LX/F8L;

    .line 2203966
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2203967
    new-instance v2, LX/F8j;

    invoke-static {p1}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    const-class v4, LX/F8T;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/F8T;

    move-object v5, p4

    move-object v6, p0

    move-object v7, v0

    invoke-direct/range {v2 .. v7}, LX/F8j;-><init>(LX/23P;LX/F8T;LX/89v;LX/1OM;Landroid/content/res/Resources;)V

    .line 2203968
    move-object v0, v2

    .line 2203969
    iput-object v0, p0, LX/F8M;->b:LX/F8j;

    .line 2203970
    iput-object p2, p0, LX/F8M;->e:LX/F6f;

    .line 2203971
    iput-object p3, p0, LX/F8M;->d:LX/0W9;

    .line 2203972
    iput-object p5, p0, LX/F8M;->f:Landroid/content/Context;

    .line 2203973
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/F8M;->c:Landroid/content/res/Resources;

    .line 2203974
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/F8M;->k:Ljava/util/List;

    iput-object v0, p0, LX/F8M;->a:Ljava/util/List;

    .line 2203975
    invoke-virtual {p2}, LX/F6f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2203976
    iput v1, p0, LX/F8M;->g:I

    .line 2203977
    iput-boolean v1, p0, LX/F8M;->h:Z

    .line 2203978
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4
    .param p2    # I
        .annotation build Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsAdapter$RowType;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2203998
    packed-switch p2, :pswitch_data_0

    .line 2203999
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not a supported ViewType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2204000
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2204001
    const v1, 0x7f03098f

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2204002
    new-instance v1, LX/F8H;

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {v1, v0}, LX/F8H;-><init>(Lcom/facebook/widget/text/BetterTextView;)V

    move-object v0, v1

    .line 2204003
    :goto_0
    return-object v0

    .line 2204004
    :pswitch_1
    new-instance v0, LX/F8J;

    new-instance v1, LX/F8f;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/F8f;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/F8J;-><init>(LX/F8f;)V

    goto :goto_0

    .line 2204005
    :pswitch_2
    new-instance v1, LX/F8K;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0306d6

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-direct {v1, v0}, LX/F8K;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 10

    .prologue
    .line 2204006
    instance-of v0, p1, LX/F8H;

    if-eqz v0, :cond_1

    .line 2204007
    iget-object v0, p0, LX/F8M;->b:LX/F8j;

    check-cast p1, LX/F8H;

    iget-object v1, p1, LX/F8H;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2204008
    iget-object v2, v0, LX/F8j;->a:Landroid/content/res/Resources;

    const p0, 0x7f08339d

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2204009
    :cond_0
    :goto_0
    return-void

    .line 2204010
    :cond_1
    instance-of v0, p1, LX/F8J;

    if-eqz v0, :cond_2

    .line 2204011
    iget-object v0, p0, LX/F8M;->b:LX/F8j;

    check-cast p1, LX/F8J;

    iget-object v1, p1, LX/F8J;->l:LX/F8f;

    .line 2204012
    iget-object v2, p0, LX/F8M;->k:Ljava/util/List;

    iget p1, p0, LX/F8M;->g:I

    sub-int p1, p2, p1

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/F8P;

    move-object v2, v2

    .line 2204013
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2204014
    const/4 v7, 0x0

    .line 2204015
    const v3, 0x7f08339f

    invoke-static {v0, v3, v1}, LX/F8j;->a(LX/F8j;ILandroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 2204016
    iget-object v3, v2, LX/F8P;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2204017
    sget-object v8, LX/F8i;->a:[I

    .line 2204018
    iget-object v9, v2, LX/F8P;->e:LX/F8O;

    move-object v9, v9

    .line 2204019
    invoke-virtual {v9}, LX/F8O;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    move v6, v5

    .line 2204020
    :goto_1
    iget-object v8, v2, LX/F8P;->b:Ljava/lang/String;

    move-object v8, v8

    .line 2204021
    iget-object v9, v1, LX/F8f;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v9, v8}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2204022
    iget-object v8, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v8, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2204023
    if-eqz v6, :cond_4

    .line 2204024
    iget-object v8, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, LX/F8f;->getContext()Landroid/content/Context;

    move-result-object v9

    const v4, 0x7f0e0207

    invoke-virtual {v8, v9, v4}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 2204025
    iget-object v8, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, LX/F8f;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v4, 0x7f0207a0

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2204026
    :goto_2
    iget-object v8, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v8, v6}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2204027
    if-eqz v5, :cond_5

    .line 2204028
    iget-object v4, v1, LX/F8f;->b:Lcom/facebook/widget/text/BetterTextView;

    sget-object v6, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v4, v3, v6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2204029
    iget-object v4, v1, LX/F8f;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2204030
    :goto_3
    iget-object v3, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2204031
    goto :goto_0

    .line 2204032
    :cond_2
    instance-of v0, p1, LX/F8K;

    if-eqz v0, :cond_0

    .line 2204033
    iget-object v0, p0, LX/F8M;->i:LX/F8L;

    sget-object v1, LX/F8L;->LOADING_MORE:LX/F8L;

    if-ne v0, v1, :cond_3

    .line 2204034
    check-cast p1, LX/F8K;

    iget-object v0, p1, LX/F8K;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto/16 :goto_0

    .line 2204035
    :cond_3
    iget-object v0, p0, LX/F8M;->i:LX/F8L;

    sget-object v1, LX/F8L;->FAILURE:LX/F8L;

    if-ne v0, v1, :cond_0

    .line 2204036
    check-cast p1, LX/F8K;

    iget-object v0, p1, LX/F8K;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/F8M;->c:Landroid/content/res/Resources;

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/F8M;->j:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto/16 :goto_0

    .line 2204037
    :pswitch_0
    new-instance v7, LX/F8g;

    invoke-direct {v7, v0, v2}, LX/F8g;-><init>(LX/F8j;LX/F8P;)V

    goto :goto_1

    .line 2204038
    :pswitch_1
    const/4 p2, 0x0

    .line 2204039
    new-instance v3, Landroid/text/SpannableString;

    iget-object v8, v0, LX/F8j;->a:Landroid/content/res/Resources;

    const v9, 0x7f0833d4

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2204040
    new-instance v8, Landroid/text/SpannableString;

    iget-object v9, v0, LX/F8j;->a:Landroid/content/res/Resources;

    const p0, 0x7f0833d6

    invoke-virtual {v9, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2204041
    new-instance v9, LX/F8h;

    invoke-direct {v9, v0, v2}, LX/F8h;-><init>(LX/F8j;LX/F8P;)V

    .line 2204042
    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result p0

    const/16 p1, 0x21

    invoke-interface {v8, v9, p2, p0, p1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2204043
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/CharSequence;

    aput-object v3, v9, p2

    const/4 v3, 0x1

    const-string p0, " "

    aput-object p0, v9, v3

    const/4 v3, 0x2

    aput-object v8, v9, v3

    invoke-static {v9}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Landroid/text/Spanned;

    move-object v3, v3

    .line 2204044
    move p0, v6

    move v6, v5

    move v5, p0

    .line 2204045
    goto/16 :goto_1

    .line 2204046
    :pswitch_2
    iget-object v3, v0, LX/F8j;->a:Landroid/content/res/Resources;

    const v6, 0x7f0833d4

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move v6, v5

    .line 2204047
    goto/16 :goto_1

    .line 2204048
    :pswitch_3
    const v4, 0x7f0833d3

    invoke-static {v0, v4, v1}, LX/F8j;->a(LX/F8j;ILandroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    move v6, v5

    goto/16 :goto_1

    .line 2204049
    :cond_4
    iget-object v8, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, LX/F8f;->getContext()Landroid/content/Context;

    move-result-object v9

    const v4, 0x7f0e020b

    invoke-virtual {v8, v9, v4}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 2204050
    iget-object v8, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, LX/F8f;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v4, 0x7f02079a

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2204051
    iget-object v8, v1, LX/F8f;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, LX/F8f;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v4, 0x7f0a0107

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    goto/16 :goto_2

    .line 2204052
    :cond_5
    iget-object v4, v1, LX/F8f;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2204053
    iget-object v4, v1, LX/F8f;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/F8L;)V
    .locals 1

    .prologue
    .line 2203991
    iget-object v0, p0, LX/F8M;->i:LX/F8L;

    if-eq p1, v0, :cond_0

    .line 2203992
    iput-object p1, p0, LX/F8M;->i:LX/F8L;

    .line 2203993
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2203994
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/F8P;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2203995
    iget-object v0, p0, LX/F8M;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2203996
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2203997
    return-void
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 2203988
    iget-object v0, p0, LX/F8M;->l:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 2203989
    new-instance v0, LX/F8I;

    invoke-direct {v0, p0}, LX/F8I;-><init>(LX/F8M;)V

    iput-object v0, p0, LX/F8M;->l:Landroid/widget/Filter;

    .line 2203990
    :cond_0
    iget-object v0, p0, LX/F8M;->l:Landroid/widget/Filter;

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 3
    .annotation build Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsAdapter$RowType;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2203980
    if-nez p1, :cond_1

    iget v1, p0, LX/F8M;->g:I

    if-ne v1, v0, :cond_1

    .line 2203981
    const/4 v0, 0x0

    .line 2203982
    :cond_0
    :goto_0
    return v0

    .line 2203983
    :cond_1
    iget v1, p0, LX/F8M;->g:I

    iget-object v2, p0, LX/F8M;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    if-ne p1, v1, :cond_0

    .line 2203984
    iget-object v1, p0, LX/F8M;->i:LX/F8L;

    sget-object v2, LX/F8L;->LOADING_MORE:LX/F8L;

    if-ne v1, v2, :cond_2

    .line 2203985
    const/4 v0, 0x2

    goto :goto_0

    .line 2203986
    :cond_2
    iget-object v1, p0, LX/F8M;->i:LX/F8L;

    sget-object v2, LX/F8L;->FAILURE:LX/F8L;

    if-ne v1, v2, :cond_0

    .line 2203987
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final ij_()I
    .locals 3

    .prologue
    .line 2203979
    iget-object v0, p0, LX/F8M;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/F8M;->g:I

    add-int/2addr v1, v0

    iget-object v0, p0, LX/F8M;->i:LX/F8L;

    sget-object v2, LX/F8L;->DEFAULT:LX/F8L;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
